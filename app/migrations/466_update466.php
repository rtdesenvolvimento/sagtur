<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update466 extends CI_Migration {

    public function up() {

        $this->alter_table_contract_settings();

        $this->db->update('settings',  array('version' => '4.6.6'), array('setting_id' => 1));
    }

    public function alter_table_contract_settings()
    {
        $fields = array(
            'usar_clausulas_site' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'clauses' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_column('contracts', $fields);
    }

    public function down() {}
}
