<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update386 extends CI_Migration {

    public function up() {

        $head_code = "<!-- Google tag4 (gtag.js) PADRAO SAGTUR -->
<script async src='https://www.googletagmanager.com/gtag/js?id=G-QEMX461YX9'></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-QEMX461YX9');
</script>

<!-- Google tag (gtag.js) PADRAO SAGTUR -->
<script async src='https://www.googletagmanager.com/gtag/js?id=UA-165016020-1'></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-165016020-1');
</script>";

        $this->db->update('shop_settings',  array('head_code' => $head_code), array('id' => 1));

        $this->db->update('settings',  array('version' => '3.8.6'), array('setting_id' => 1));

    }

    public function down() {}
}
