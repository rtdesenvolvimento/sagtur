<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif;
            font-size: 12px;
        }

        td {
            padding: 1px;
        }
    </style>
<body>
<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <th colspan="3" align="center">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
            <h5>Lista de Embarque <br/> <?php echo $product->name; ?></h5> <br/>
        </th>
    </tr>
    </thead>
</table>

<table border="1" style="width: 100%;border-collapse:collapse;">
    <?php

    foreach($listOnibus as $onibus){

        $fornecedor         = $onibus->fornecedor;
        $supplier_details 	= $this->site->getCompanyByID($fornecedor);
        $supplier 			= $supplier_details->name;
        $company            = $supplier_details->company;
        ?>

        <thead>
        <tr>
            <th colspan="5" align="center">
                <h3><?php echo $company; ?></h3>
            </th>
        </tr>
        </thead><thead>
        <tr>
            <th align="center" width="10%"> Poltrona</th>
            <th style="text-align: left;width: 40%"> Passageiro</th>
            <th align="center" width="10%">Identidade</th>
            <th align="center" width="30%">Telefone</th>
            <th align="center" width="10%">Tipo</th>
            <th align="center" width="20%">Local sa&iacute;da</th>
        </tr>
        </thead>

        <tbody>
        <?php

        $arquivo = fopen('configuracao.txt', 'r');
        $local_saida_descricao = '';
        while (!feof($arquivo)) :
            $linha          = fgets($arquivo, 1024);
            $local_saida_descricao    = $linha;
        endwhile;
        fclose($arquivo);

        foreach($itens_pedidos as $item){

            $objCustomer 	 = $this->site->getCompanyByID($item->customerClient);
            $totalDeItens    = $this->site->buscarTotalDeItensDeUmaVendaPorVenda($item->sale_id);
            $tipo = '';
            $telefone = '';

            if ($totalDeItens > 1) $tipo = 'Juntos';
            else $tipo = 'Sozinho';

            $nomeCustomer           = $objCustomer->name;
            $rg                     = $objCustomer->cf1;
            $poltronaClient         = $item->poltronaClient;
            $reference_no_variacao  = $item->reference_no_variacao;

            $phone          = $objCustomer->phone;
            $whatsApp       = $objCustomer->cf5;
            $telefone_emergencia    = $objCustomer->telefone_emergencia;

            if ($phone)  $telefone  = $phone;
            if ($whatsApp)  $telefone  = $telefone.' '.$whatsApp;
            if ($telefone_emergencia) $telefone = $telefone.'<br/>Emergência '.$telefone_emergencia;

            if ($item->local_saida) $local_saida = $item->local_saida;
            else $local_saida = $local_saida_descricao;
            ?>

            <?php if ($reference_no_variacao == $onibus->id )  {?>
                <tr>
                    <td align="center"><?php echo $poltronaClient;?></td>
                    <td style="text-align: left;"> <?php echo $nomeCustomer;?></td>
                    <td style="text-align: center;"> <?php echo $rg;?></td>
                    <td align="center"><?php echo $telefone;?></td>
                    <td align="center"><?php echo $tipo;?></td>
                    <td align="center"><?php echo $local_saida;?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    <?php }?>
</table>
</body>
</html>
