<script type="text/javascript">
    $(document).ready(function () {
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });
</script>

<?php echo form_open_multipart("transfer/edit", array('data-toggle' => 'validator', 'role' => 'form')) ?>
    <ul id="myTab" class="nav nav-tabs" style="text-align: center">
        <li class=""><a href="#abageral" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('detalhes_do_pacote') ?></a></li>
        <li class=""><a href="#tarifas" class="tab-grey"><i class="fa fa-users" style="font-size: 20px;"></i><br/><?= lang('tarifario') ?></a></li>
        <li class=""><a href="#origem" class="tab-grey"><i class="fa fa-car" style="font-size: 20px;"></i><br/><?= lang('origem') ?></a></li>
        <li class=""><a href="#destino" class="tab-grey"><i class="fa fa-car" style="font-size: 20px;"></i><br/><?= lang('destino') ?></a></li>
        <li class=""><a href="#faturamento" class="tab-grey"><i class="fa fa-usd" style="font-size: 20px;"></i><br/><?= lang('faturamento') ?></a></li>
        <li class=""><a href="#frequencia" class="tab-grey"><i class="fa fa-calendar" style="font-size: 20px;"></i><br/><?= lang('horario_frequencia') ?></a></li>
        <li class=""><a href="#" id="save" class="tab-grey" style="background: #3c763d;color: #F0F0F0"><i class="fa fa-save" style="font-size: 20px;"></i><br/><?= lang('edit') ?></a></li>
    </ul>

    <div class="tab-content">
        <!--Detalhes do pacote !-->
        <div id="abageral" class="tab-pane fade in">
            <div class="box">
                <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-map-signs"></i><?= lang('edit_transfer'); ?></h2></div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("product_name", "name") ?>
                                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group all">
                                <?= lang("tipo_trajeto", "tipo_trajeto") ?>
                                <?php
                                $cbTrajeto[''] = "";
                                foreach ($tiposTrajeto as $tpTrajeto) $cbTrajeto[$tpTrajeto->id] = $tpTrajeto->name;
                                echo form_dropdown('tipo_trajeto', $cbTrajeto, '', 'class="form-control select" id="tipo_trajeto" required="required" placeholder="' . lang("select") . " " . lang("tipo_trajeto") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group all" id="div_category">
                                <?= lang("color_calendary", "color_calendary") ?>
                                <?php
                                $cor[''] = "";
                                foreach ($colors as $color) $cor[$color->id] = $color->name;
                                echo form_dropdown('color_calendary', $cor, '', 'class="form-control select" id="color_calendary" required="required" placeholder="' . lang("select") . " " . lang("color_calendary") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group all" id="div_category">
                                <?= lang("veiculo", "veiculo") ?>
                                <?php
                                $cbVeiculo[''] = "";
                                foreach ($veiculos as $veiculo) $veiculo[$color->id] = $veiculo->name;
                                echo form_dropdown('veiculo', $cbVeiculo, '', 'class="form-control select" id="veiculo" required="required" placeholder="' . lang("select") . " " . lang("veiculo") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all" id="div_category">
                                <?= lang("tipo_transfer", "tipo_transfer") ?>
                                <?php
                                $cbTipoTransfer[3] = "Ida e Volta   - Transfer IN/OUT";
                                $cbTipoTransfer[1] = "Somente Ida   - Transfer IN";
                                $cbTipoTransfer[2] = "Somente Volta - Transfer OUT";
                                echo form_dropdown('tipo_transfer', $cbTipoTransfer, '', 'class="form-control select" id="veiculo" required="required" placeholder="' . lang("select") . " " . lang("tipo_transfer") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('executivo');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('executivo', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('yes'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('executivo', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('no'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('servico_terceirizado');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('servico_terceirizado', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('yes'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('servico_terceirizado', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('no'); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('disponivel_para_comercializacao');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('publicar_ativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('desativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-comments"></i> <?= lang("product_details", "product_details") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all" id="div_product_details">
                                            <textarea name="product_details" id="product_details"><?php echo (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : ''));?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info-circle"></i>  <?= lang("oqueInclui", "oqueInclui") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <textarea name="oqueInclui" id="oqueInclui"><?php echo (isset($_POST['oqueInclui']) ? $_POST['oqueInclui'] : ($product ? $product->oqueInclui : ''));?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-map-signs"></i> <?= lang("itinerario", "itinerario") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <textarea name="itinerario" id="itinerario"><?php echo (isset($_POST['itinerario']) ? $_POST['itinerario'] : ($product ? $product->itinerario : ''));?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-usd"></i><?= lang("valores_condicoes", "valores_condicoes") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <textarea name="valores_condicoes" id="valores_condicoes"><?php echo (isset($_POST['valores_condicoes']) ? $_POST['valores_condicoes'] : ($product ? $product->valores_condicoes : ''));?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("product_details_for_invoice", "details") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="form-group">
                                <?php echo form_submit('add_transfer', $this->lang->line("add_transfer"), 'id="add_transfer" class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= form_close(); ?>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#save').click(function (event) {
            $('#add_transfer').click();
        });
    });
</script>

