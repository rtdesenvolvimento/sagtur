<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update427 extends CI_Migration {

    public function up() {

        $this->create_table_cupom_desconto();

        $this->db->update('settings',  array('version' => '4.2.7'), array('setting_id' => 1));
    }

    function create_table_cupom_desconto() {
        $fields = array(
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'all_products' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'quantidade_disponibilizada' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'dataInicio' => array('type' => 'DATE', 'null' => TRUE ),
            'dataFinal' => array('type' => 'DATE', 'null' => TRUE ),
        );
        $this->dbforge->add_column('cupom_desconto', $fields);
    }


    public function down() {}
}
