<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update502 extends CI_Migration {

    public function up() {

        $this->alter_table_gallery_item();

        $this->db->update('settings',  array('version' => '5.0.2'), array('setting_id' => 1));
    }

    public function alter_table_gallery_item() {
        $fields = array(
            'photo_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('gallery_item', $fields);
    }

    public function down() {}
}
