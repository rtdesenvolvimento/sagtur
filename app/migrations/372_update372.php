<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update372 extends CI_Migration {

    public function up() {

        $this->create_table_infinitepay();
        $this->alter_table_taxas_venda_configuracao();

        $this->db->update('settings',  array('version' => '3.7.2'), array('setting_id' => 1));
    }

    public function create_table_infinitepay() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'client_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'client_secret' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('infinitepay', TRUE, $attributes);

        $data_infinitepay = array(
            'client_id' => '#',
            'client_secret' => '#',
            'note' => 'Integração SAGTur com Asaas - Boleto, Carnê (Parcelamento) e Pix',
        );
        $this->db->insert('infinitepay', $data_infinitepay);
    }

    public function alter_table_taxas_venda_configuracao() {
        $fields = array(
            'numero_max_parcelas' => array('type' => 'INT', 'constraint' => 1 , 'default' => 12),
            'numero_max_parcelas_sem_juros' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'assumir_juros_parcelamento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('taxas_venda_configuracao', $fields);
    }

    public function down() {}
}
