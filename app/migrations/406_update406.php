<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update406 extends CI_Migration {

    public function up() {

        $this->alter_agenda_viagem();
        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.0.6'), array('setting_id' => 1));
    }

    public function alter_agenda_viagem() {
        $fields = array(
            'habilitar_marcacao_area_cliente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'permitir_marcacao_dependente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'data_inicio_marcacao' => array('type' => 'DATE', 'null' => TRUE ),
            'data_final_marcacao' => array('type' => 'DATE', 'null' => TRUE ),
        );
        $this->dbforge->add_column('agenda_viagem', $fields);
    }


    public function alter_table_shop_settings() {
        $fields = array(
            'usar_email_area_cliente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
