<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update333 extends CI_Migration {

    public function up() {

        $this->alter_table_produts();

        $this->db->update('settings',  array('version' => '2021.4.5'), array('setting_id' => 1));
    }

    function alter_table_produts() {
        $fields = array(
            'permiteVendaMenorIdade' => array('type' => 'INT', 'constraint' => 1 , 'null' => FALSE, 'default' => '0'),
            'permiteVendaClienteDuplicidade' => array('type' => 'INT', 'constraint' => 1 , 'null' => FALSE, 'default' => '0'),
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
