<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update425 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.2.5'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $this->db->update('products',  array('isApenasColetarPagador' => 0));
    }
    public function down() {}
}
