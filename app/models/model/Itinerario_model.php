<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Itinerario_model extends CI_Model
{
    public $veiculo;
    public $guia;
    public $motorista;
    public $itens;

    public function __construct()
    {
        parent::__construct();
    }

    public function adicionarItem($item) {
        $this->itens[count($this->itens) + 1] = $item;
    }

}
