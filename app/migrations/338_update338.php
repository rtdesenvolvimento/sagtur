<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update338 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '2.2022.06.3'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'logo_shop' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => 'background2.jpg' ),
            'filtrar_itens_loja' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
