<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_condicao_pagamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarCondicaoPagamento", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'Ativo' => lang('ativo'),
                    'Inativo' => lang('inativo')
                );
                echo form_dropdown('status', $opts, (isset($_POST['status']) ? $_POST['status'] :  '') , 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('condicao_pagamento_name', 'name'); ?>
                <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] :  '') , 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("intervalo", "intervalo") ?>
                <?php
                $opts = array(
                    'dia' => lang('dia'),
                    'mes' => lang('mes')
                );
                echo form_dropdown('intervalo', $opts, (isset($_POST['intervalo']) ? $_POST['intervalo'] :  '') , 'class="form-control" id="intervalo" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("parcelas", "parcelas"); ?>
                <?php echo form_input('parcelas', '0', 'class="form-control input-tip mask_integer" id="sldiscount"'); ?>
            </div>
            <div class="form-group">
                <?= lang('intervalo_em_dias', 'dias'); ?>
                <?= form_input('dias', (isset($_POST['dias']) ? $_POST['dias'] :  '') , 'class="form-control" id="dias"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarCondicaoPagamento', lang('adicionar_condicao_pagamento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>