<style media="screen">

    #BillerData td:nth-child(1) {display: none;}

    #BillerData td:nth-child(7) {text-align: right;width: 10%;}
    #BillerData td:nth-child(8) {text-align: center;width: 5%;}
</style>

<script>
    $(document).ready(function () {


        function currencyFormatTotal(x, alignment) {

            if (alignment === undefined) alignment = 'text-right';

            if (x != null) {
                return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(x)+'</div>';
            } else {
                return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(0)+'</div>';
            }
        }

        var oTable = $('#BillerData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('commissions/getBillers') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var total_comissao = 0;
                for (var i = 0; i < aaData.length; i++) {
                    if (is_numeric(aaData[aiDisplay[i]][6])) {
                        total_comissao += parseFloat(aaData[aiDisplay[i]][6]);
                    }
                }
                var nCells = nRow.getElementsByTagName('th');

                nCells[6].innerHTML = currencyFormatTotal(parseFloat(total_comissao));
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, null, null, null, null,  {"mRender": currencyFormat}, {"bSortable": false}]
        });
    });
</script>
<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('billers/biller_actions', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('billers_select'); ?></h2>

        <div class="box-icon">

        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="BillerData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= lang("company"); ?></th>
                            <th><?= lang("name"); ?></th>
                            <th><?= lang("vat_no"); ?></th>
                            <th><?= lang("phone"); ?></th>
                            <th><?= lang("email_address"); ?></th>
                            <th><?= lang("commission"); ?></th>
                            <th style="width:85px;"><?= lang("pagar"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= lang("company"); ?></th>
                            <th><?= lang("name"); ?></th>
                            <th><?= lang("vat_no"); ?></th>
                            <th><?= lang("phone"); ?></th>
                            <th><?= lang("email_address"); ?></th>
                            <th><?= lang("commission"); ?></th>
                            <th style="width:85px;text-align: center;"><?= lang("pagar"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>
	

