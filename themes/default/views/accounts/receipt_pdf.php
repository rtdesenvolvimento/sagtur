<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="urn:layr:template">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Recibo</title>
    <link href="<?= $assets ?>styles/recibo.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
</head>
<body style="background-color: white;">
<div id="non-printable" class="actions-div">
    <button class="btn" style="cursor: pointer;" onclick="window.close();">Cancelar</button>
    <button class="btn btn-primary print-btn" style="cursor: pointer;" onclick="window.print();">Imprimir Recibo</button>
</div>
<div id="printable" class="receipt-container">
    <hr class="solid-line">
    <div class="receipt-info-container">
        <div class="container-company-logo">
            <table class="company-logo" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        <img src="<?= base_url(); ?>assets/uploads/logos/sagtur.png" id="act-logo-topo-novo" align="middle">
                    </td>
                    <td>
                        <div class="receipt-info-column-1">
                            <div><span style="font-size: 15px;"><strong>SAGTUR</strong></span></div>
                            <div class="company-info-container">
                                <strong>CNPJ: 38.055.980/0001-00</strong>
                            </div>
                            <div class="company-address-container">
                                (48) 99857-9104 - sistema.sagtur@gmail.com
                                <br/>
                                R. Giovanni Pisano nº 221<br />Ariiru - Pallhoça/SC CEP 88135-432
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr class="solid-line">
    <table style="width: 100%; padding: 30px 0px 5px;">
        <tbody>
        <tr>
            <td style="flex: 1; font-size: 30px;"><strong><span class="condensed">RECIBO DE PAGAMENTO</span></strong></td>
            <td style="flex: 1; text-align: right;">
                <span style="font-size: 38px;" class="condensed semi-bold"><span class="gray condensed semi-bold">R$</span> <?php echo $this->sma->formatNumber($fatura->valorfatura);?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <hr class="dotted-line">
    <div class="receipt-detail-container">
        <div class="receipt-details">Pagamos para <strong>SAGTUR  - 38.055.980/0001-00</strong>, a
            importância de <strong><?php echo $this->sma->formatMoney($fatura->valorfatura);?> (<?php echo  trim($this->sma->extenso($fatura->valorfatura));?>)</strong>
            pago em <strong><?php echo $fatura->tipo_cobranca;?></strong>, referente a <strong>Fatura <?php echo $fatura->reference;?>
               </strong> com pagamento na data <strong><?php echo $this->sma->hrld($fatura->dtultimopagamento);?></strong>.
        </div>
        <div class="receipt-date">Emissão do Recibo Data: <?php echo $this->sma->dataDeHojePorExtensoRetorno();?>.</div>
        <div style="padding: 5px;">  </div>
        __________________________________________________
        <div class="receipt-company">
            Diego Fernando Hoffmann<br>
            Diretor Financeiro
        </div>
    </div>
</div>


</body>
</html>