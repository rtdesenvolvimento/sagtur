<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendamentoFilter_DTO_model extends CI_Model
{

    public $start_date;
    public $end_date;
    public $start_hour;
    public $end_hour;
    public $product;
    public $status;
    public $name;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getStartHour()
    {
        return $this->start_hour;
    }

    /**
     * @param mixed $start_hour
     */
    public function setStartHour($start_hour)
    {
        $this->start_hour = $start_hour;
    }

    /**
     * @return mixed
     */
    public function getEndHour()
    {
        return $this->end_hour;
    }

    /**
     * @param mixed $end_hour
     */
    public function setEndHour($end_hour)
    {
        $this->end_hour = $end_hour;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



}