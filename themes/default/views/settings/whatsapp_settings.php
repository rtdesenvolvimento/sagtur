<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-whatsapp"></i><?= lang('whatsapp_settings'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-6">
                <?=form_open("system_settings/whatsapp_settings/".$whatsapp_settings->id, array('role' => 'form', 'id="whatsapp_settings_form"')); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("activate", "active"); ?>
                                <?php
                                $yn = array('1' => lang("sim"), '0' => lang("nao"));
                                echo form_dropdown('active', $yn, $whatsapp_settings->active, 'class="form-control tip" required="required" id="active"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("apikey", "apikey"); ?>
                                <?=form_input('apikey', $whatsapp_settings->apikey, 'id="apikey" class="form-control"');?>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("phone_whatsapp_sent", "phone"); ?>
                                <?=form_input('phone', $whatsapp_settings->phone, 'id="phone" class="form-control"' , 'number');?>

                            </div>
                        </div>
                    </div>
                    <div style="clear: both; height: 10px;"></div>
                    <div class="form-group">
                        <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                    </div>
                <?=form_close(); ?>
            </div>
            <div class="col-lg-6">
                <h1>Configurar:</h1>
                Você precisa obter a chave API do bot antes de usar a API:<br/><br/>

                1. Adicione o número de telefone <a href="tel:+34644783397">para +34 644 78 33 97</a> em seus Contatos Telefônicos. (Nomeie-o como desejar)<br/>
                2 . Envie esta mensagem "I allow callmebot to send me messages" para o novo contato criado (usando o WhatsApp, é claro) tem quer exatamente essa mensagem!<br/>
                3 . Aguarde até receber a mensagem " API Ativada para seu número de telefone. Sua APIKEY é XXXXX " do bot.<br/>
                <small>Observação: se você não receber a ApiKey em 2 minutos, tente novamente após 24hs.</small><br/>
                4 . Configure aqui no sistema sua APIKEY e o telefone que deseja receber as mensagens.<br/>
                5 . Pronto! Agora com sua APIKEY configurada você vai receber as notificações de compras de clientes.<br/>

                <?php if ($whatsapp_settings->active && $whatsapp_settings->apikey) {?>
                    <br/>
                    <a href="https://api.callmebot.com/whatsapp.php?phone=55<?=$whatsapp_settings->phone;?>&text=Mensagem+de+Teste&apikey=<?=$whatsapp_settings->apikey;?>" target="_blank">Enviar Mensagem no WhatsApp de Teste</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

    });
</script>
