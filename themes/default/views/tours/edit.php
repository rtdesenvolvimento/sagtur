<style>
    .form-control_custom {
        display: block;
        width: 100%;
        height: 25px;
        padding: 0px 0px;
        font-size: 11px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .form-control_data {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>
<?php
if (!empty($transportes)) {
    foreach ($transportes as $transporte) {
        $varsTransporte[] = addslashes($transporte->name);
    }
} else {
    $varsTransporte = array();
}

if (!empty($combo_items)) {


} else {
    $varsFaixaEtariaServicoAdicional = array();
}
?>
<script type="text/javascript">
    $(document).ready(function () {

        $('#controle_estoque_hospedagem').on('ifChecked', function (e) {
            $('.estoque_hospedagem').css('display', '');
        });

        $('#controle_estoque_hospedagem').on('ifUnchecked', function (e) {
            $('.estoque_hospedagem').css('display', 'none');
        });

        $('#isComHospedagem').on('ifChanged', function (e) {
            if ($(this).val() === '1') {
                $('#isHospedagem').iCheck('check');
                $('#isValorPorFaixaEtaria').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').show();
            } else {
                $('#isValorPorFaixaEtaria').iCheck('check');
                $('#isHospedagem').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').hide();
            }
        });

        $('#isSemHospedagem').on('ifChanged', function (e) {
            if ($(this).val() === '1') {
                $('#isHospedagem').iCheck('check');
                $('#isValorPorFaixaEtaria').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').show();
            } else {
                $('#isValorPorFaixaEtaria').iCheck('check');
                $('#isHospedagem').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').hide();
            }
        });

        $('#isHospedagem').on('ifChecked', function (e) {$('#tbHospedagem').slideDown();$('#div_controle_estoque_hospedagem').show();});
        $('#isHospedagem').on('ifUnchecked', function (e) {$('#tbHospedagem').slideUp();$('#div_controle_estoque_hospedagem').hide();});

        $('#isValorPorFaixaEtaria').on('ifChecked', function (e) {$('#tbValorFaixaEtaria').slideDown();$('#div_controle_estoque_hospedagem').hide();});
        $('#isValorPorFaixaEtaria').on('ifUnchecked', function (e) {$('#tbValorFaixaEtaria').slideUp();$('#div_controle_estoque_hospedagem').show();});

        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });

        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                data: scdata
                            });
                        } else {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                                placeholder: "<?= lang('no_subcategory') ?>",
                                data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });

        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        <?php if ($product->controle_estoque_hospedagem) {?>
            $('.estoque_hospedagem').css('display', '');
        <?php } else { ?>
            $('.estoque_hospedagem').css('display', 'none');
        <?php } ?>

         $('.qtd_pessoas_hospedagem').change(function(event){
            let qtdQuartos = $(this).val();
            let tipoQuartoId = $(this).attr('tipo_quarto');
            let qtdAcomodacao = $(this).attr('qtdacomodacao');

            $('#qtd_pessoas_hospedagem'+tipoQuartoId).val(qtdQuartos*qtdAcomodacao);

            total_hospedagem();
        });

        total_hospedagem();
    });

    function total_hospedagem() {

        let total_pessoas_hospedagem = 0;
        let total_quartos = 0;

        $($('.total_pessoas_hospedagem')).each(function( index, pessoas ) {
            total_pessoas_hospedagem += parseInt($(this).val());
        });

        $('.qtd_pessoas_hospedagem').each(function(index, quartos){
            total_quartos += parseInt($(this).val());
        });

        $('#total_quartos').html(total_quartos);
        $('#total_pessoas_hospedagem').html(total_pessoas_hospedagem);
    }
</script>

<?php echo form_open_multipart("products/edit/".$product->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>
<ul id="myTab" class="nav nav-tabs" style="text-align: center">
    <li><a href="#abageral" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('detalhes_do_pacote') ?></a></li>
    <li id="tbValorFaixaEtaria" style="<?php if(!$product->isValorPorFaixaEtaria) echo 'display: none;'?>" ><a href="#valores" class="tab-grey"><i class="fa fa-users" style="font-size: 20px;"></i><br/><?= lang('valor_por_faixa_etaria') ?></a></li>
    <li id="tbHospedagem" style="<?php if(!$product->isHospedagem) echo 'display: none;'?>"><a href="#hospedagem" class="tab-grey"><i class="fa fa-bed" style="font-size: 20px;"></i><br/><?= lang('hospedagem') ?></a></li>
    <li id="tbTransporte" style="<?php if(!$product->isTransporteTuristico) echo 'display: none;'?>"><a href="#transporte" class="tab-grey"><i class="fa fa-bus" style="font-size: 20px;"></i><br/><?= lang('tb_transporte') ?></a></li>
    <li id="tbEmbarques" style="<?php if(!$product->isEmbarque) echo 'display: none;'?>"><a href="#embarque" class="tab-grey"><i class="fa fa-exchange" style="font-size: 20px;"></i><br/><?= lang('embarque') ?></a></li>
    <li id="tbServicosAdicionais" style="<?php if(!$product->isServicosAdicionais) echo 'display: none;'?>"><a href="#servicosopcionais" class="tab-grey"><i class="fa fa-plus" style="font-size: 20px;"></i><br/><?= lang('servicos_opcionais') ?></a></li>
    <li id="tbIntegracaoSite" style="<?php if(!$product->isServicoOnline) echo 'display: none;'?>"><a href="#site" class="tab-grey"><i class="fa fa-sitemap" style="font-size: 20px;"></i><br/><?= lang('integracao_site') ?></a></li>
    <li id="tbComissao" style="<?php if(!$product->isComissao) echo 'display: none;'?>"><a href="#comissao" class="tab-grey"><i class="fa fa-money" style="font-size: 20px;"></i><br/><?= lang('comissao') ?></a></li>
    <li id="tbTaxasComissao" style="<?php if(!$product->isTaxasComissao) echo 'display: none;'?>"><a href="#taxasPagamento" class="tab-grey"><i class="fa fa-usd" style="font-size: 20px;"></i><br/><?= lang('taxas_comissão') ?></a></li>
    <li id="tbDatas"><a href="#datas" class="tab-grey"><i class="fa fa-calendar" style="font-size: 20px;"></i><br/><?= lang('programacao_datas') ?></a></li>
    <li><a href="#" id="save" class="tab-grey" style="background: #3c763d;color: #F0F0F0"><i class="fa fa-save" style="font-size: 20px;"></i><br/><?= lang('edit') ?></a></li>
</ul>
<div class="tab-content">
    <!--Detalhes do pacote !-->
    <div id="abageral" class="tab-pane fade in">
        <div class="box">
            <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-map-signs"></i><?= lang('edit_product'); ?></h2></div>
            <div class="box-content">
                <div class="row">
                    <input type="hidden" id="id" value="<?php echo $product->id;?>">
                    <div class="col-md-12">
                        <div class="col-md-6" style="display: none;">
                            <div class="form-group" id="div_status">
                                <?= lang("situacao_pacote", "status") ?>
                                <?php
                                $opts = array(
                                    'Confirmado' => lang('status_confirmado_para_venda'),
                                    'Montando' => lang('status_montando_pacote'),
                                    'Executado' => lang('status_viagem_executada') ,
                                    'Cancelado' => lang('status_viagem_cancelada')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <div class="form-group all" id="div_category">
                                <?= lang("destino", "destino") ?>
                                <?php
                                $dn[""] = lang("select") . ' ' . lang("local_embarque");
                                foreach ($destinos as $destino) {
                                    $dn[$destino->id] =  $destino->name;
                                }
                                echo form_dropdown('destino', $dn, (isset($_POST['destino']) ? $_POST['destino'] : ($product ? $product->destino : '')), 'id="destino" required="required" name="local_embarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <div class="form-group all">
                                <?= lang("subcategory", "subcategory") ?>
                                <div class="controls" id="subcat_data"> <?php
                                    echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . lang("select_category_to_load") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?= lang("nome_pacote", "name") ?>
                                <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('header_config_valores');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isComHospedagem', '1', ($product ? $product->isHospedagem : ''), 'id="isComHospedagem"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('produto_com_hospedagem'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" <?php echo $product->isValorPorFaixaEtaria ? 'style="display: none;"' : '';?> id="div_controle_estoque_hospedagem">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('controle_estoque_hospedagem', '1', ($product ? $product->controle_estoque_hospedagem : ''), 'id="controle_estoque_hospedagem"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('controle_estoque_hospedagem'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isComHospedagem', '0', ($product ? $product->isValorPorFaixaEtaria : ''), 'id="isSemHospedagem"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('produto_sem_hospedagem'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isValorPorFaixaEtaria', '1', ($product ? $product->isValorPorFaixaEtaria : ''), 'id="isValorPorFaixaEtaria"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('valor_por_faixa_etaria'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isHospedagem', '1', ($product ? $product->isHospedagem : ''), 'id="isHospedagem"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_hospedagem'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('configuracoes_cadastro_produto');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isTransporteTuristico', '1', ($product ? $product->isTransporteTuristico : ''), 'id="isTransporteTuristico"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_transporte_turistico'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isEmbarque', '1', ($product ? $product->isEmbarque : ''), 'id="isEmbarque"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_embarque'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isServicosAdicionais', '1', ($product ? $product->isServicosAdicionais : ''), 'id="isServicosAdicionais"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_servicos_adicionais'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('disponivel_para_comercializacao');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '1', ($product ? $product->active : ''), ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('publicar_ativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '0', ($product ? !$product->active : ''), ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('desativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group all" id="div_category">
                                <?= lang("category", "category") ?>
                                <?php
                                $cat[''] = "";
                                foreach ($categories as $category)$cat[$category->id] = $category->name;
                                echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ($product ? $product->category_id : '1')), 'class="form-control select" id="category" required="required" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('modalidade_para_venda');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('enviar_site', '1', ($product ? $product->enviar_site : ''), 'id="enviar_site"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_reservas_online'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_reservas_online'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isComissao', '1', ($product ? $product->isComissao : ''), 'id="isComissao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_comissao'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isTaxasComissao', '1', ($product ? $product->isTaxasComissao : ''), 'id="isTaxasComissao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('configurar_taxas_comissao'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permiteVendaMenorIdade', '1', ($product ? $product->permiteVendaMenorIdade : ''), 'id="permiteVendaMenorIdade"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_vender_para_menor'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_vender_para_menor'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permiteVendaClienteDuplicidade', '1', ($product ? $product->permiteVendaClienteDuplicidade : ''), 'id="permiteVendaClienteDuplicidade"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_venda_com_cliente_duplicidade'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_venda_com_cliente_duplicidade'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permitirListaEmpera', '1', ($product ? $product->permitirListaEmpera : ''), 'id="permitirListaEmpera"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permitir_lista_de_espera'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('apenas_cotacao', '1', ($product ? $product->apenas_cotacao : ''), 'id="apenas_cotacao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('produto_disponivel_apenas_para_orcamento'); ?></label>  <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_produto_disponivel_apenas_para_orcamento'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isServicoOnline', '1', ($product ? $product->isServicoOnline : ''), 'id="isServicoOnline"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_integracao_site'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('definir_captacao_dados');?></h2>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isApenasColetarPagador', '1', ($product ? $product->isApenasColetarPagador : ''), ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_dados_de_todos_link'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isApenasColetarPagador', '0', ($product ? !$product->isApenasColetarPagador : ''), ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_dados_de_apenas_pagador'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('captarEnderecoLink', '1', ($product ? $product->captarEnderecoLink : ''), 'id="captarEnderecoLink"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_endereco_link'); ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-shopping-cart"></i> <?= lang('informacoes_valores_exibicao_loja'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-6">

                                    <div class="col-md-6">
                                        <div class="form-group all">
                                            <?= lang("valor_pacote", "valor_pacote") ?>
                                            <?= form_input('valor_pacote', (isset($_POST['valor_pacote']) ? $_POST['valor_pacote'] : ($product ? $product->valor_pacote : 'A partir de')), 'class="form-control" required="required" id="valor_pacote" '); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="display: none;">
                                        <div class="form-group all">
                                            <?= lang("simboloMoeda", "simboloMoeda") ?>
                                            <?= form_input('simboloMoeda', (isset($_POST['simboloMoeda']) ? $_POST['simboloMoeda'] : ($product ? $product->simboloMoeda : 'R$')), 'class="form-control" required="required" id="simboloMoeda" '); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group all">
                                            <?= lang("precoExibicaoSite", "precoExibicaoSite") ?>
                                            <?= form_input('precoExibicaoSite', (isset($_POST['precoExibicaoSite']) ? $_POST['precoExibicaoSite'] : ($product ? $product->precoExibicaoSite : '0.00')), 'class="form-control tip mask_money" required="required" id="precoExibicaoSite" '); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <?php echo form_checkbox('promotion', '1', ($product ? $product->promotion : ''), 'id="promotion"'); ?>
                                            <label for="promotion" class="padding05">
                                                <?= lang('promotion'); ?>
                                            </label>
                                        </div>

                                        <div id="promo"<?= $product->promotion ? '' : ' style="display:none;"'; ?>>
                                            <div class="well well-sm">
                                                <div class="form-group">
                                                    <?= lang('promo_price', 'promo_price'); ?>
                                                    <?= form_input('promo_price', set_value('promo_price', $product->promo_price ? $this->sma->formatDecimal($product->promo_price) : '0'), 'class="form-control mask_money tip" id="promo_price"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Imagem de exemplo na loja</label><br/>
                                    <img src="<?= $assets ?>images/exemplo-loja.jpg" style="box-shadow: 0 2px 15px #47525d;">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group all">
                                        <?= lang("alert_stripe", "alert_stripe") ?>
                                        <?= form_input('alert_stripe', (isset($_POST['alert_stripe']) ? $_POST['alert_stripe'] : ($product ? $product->alert_stripe : '')), 'class="form-control" id="alert_stripe" '); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-photo"></i> <?= lang("product_gallery", "product_gallery") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12 alert-info" style="margin-bottom: 20px;">
                                    <h3><i class="fa fa-info-circle"></i> Atencão</h3>
                                    <ul>
                                        <li>Adicione imagens nos formatos (jpg, jpeg ou png) com no máximo 1MB de Tamanho.</li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $product->image ?>" alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group all" id="div_product_details">
                                            <div class="form-group all">
                                                <?= lang("product_image", "product_image") ?>
                                                <input id="product_image" type="file" data-browse-label="<?= lang('browse'); ?>" name="product_image" data-show-upload="false"
                                                       data-show-preview="false" accept="image/*" class="form-control file">
                                            </div>
                                            <div class="form-group all">
                                                <?= lang("product_gallery_images", "images") ?>
                                                <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                                                       data-show-preview="false" class="form-control file" accept="image/*">
                                            </div>
                                            <div id="img-details" ></div>

                                            <div id="multiimages" class="padding10">
                                                <?php if (!empty($images)) {
                                                    echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                    foreach ($images as $ph) {
                                                        echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                        if ($Owner || $Admin || $GP['products-edit']) {
                                                            echo '<a href="#" class="delimg" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                                                        }
                                                        echo '</div>';
                                                    }
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <h3>Imagens do Site</h3>
                                            <div id="multiimages" class="padding10">
                                                <?php if (!empty($imagesFotos)) {
                                                    echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                    foreach ($imagesFotos as $ph) {
                                                        echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="'.$ph->photo.'" style="margin-right:5px;">
                                                        <img class="img-responsive" src="'.$ph->photo.'" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                        if ($Owner || $Admin || $GP['products-edit']) {
                                                            echo '<a href="#" class="delimgsite" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                                                        }
                                                        echo '</div>';
                                                    }
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12 alert-danger">
                            <h3><i class="fa fa-info-circle"></i> Alguns Emoticons não são aceitos pelo sistema.</h3>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-comments"></i> <?= lang("product_details", "product_details") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all" id="div_product_details">
                                        <textarea name="product_details" id="product_details"><?php echo (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-info-circle"></i>  <?= lang("oqueInclui", "oqueInclui") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="oqueInclui" id="oqueInclui"><?php echo (isset($_POST['oqueInclui']) ? $_POST['oqueInclui'] : ($product ? $product->oqueInclui : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-map-signs"></i> <?= lang("itinerario", "itinerario") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="itinerario" id="itinerario"><?php echo (isset($_POST['itinerario']) ? $_POST['itinerario'] : ($product ? $product->itinerario : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-usd"></i><?= lang("valores_condicoes", "valores_condicoes") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="valores_condicoes" id="valores_condicoes"><?php echo (isset($_POST['valores_condicoes']) ? $_POST['valores_condicoes'] : ($product ? $product->valores_condicoes : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("product_details_for_invoice", "details") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display: none;">
                        <div class="form-group">
                            <?php echo form_submit('add_product', $this->lang->line("add_product"), 'id="add_product" class="btn btn-primary"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Valores !-->
    <div id="valores" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('informacoes_vagas_valores_do_produto'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-user"></i> <?= lang('info_valores_por_faixa_etaria'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="attrTableHospedagemFaixaEtaria" class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;">
                                                <thead>
                                                <tr class="active">
                                                    <th style="text-align: center;width: 1%;"></th>
                                                    <th class="col-md-10" style="text-align: left;"><?= lang('tipo') ?></th>
                                                    <th class="col-md-2" style="text-align: right;"><?= lang('price') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($valorFaixas as $valoresFaixa) {
                                                    $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaByFaixaId($product->id, $valoresFaixa->id);
                                                    if ($faixa){
                                                        $ativo = $faixa->status == 'ATIVO' ? true  : false; ?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativarTipoFaixaEtaria[]', $valoresFaixa->id, $ativo, ''); ?></td>
                                                            <td>
                                                                <?= form_input('tipoFaixaEtariaValorConfigure[]',   $faixa->tipo, '', 'hidden') ?>
                                                                <?= form_input('valorFaixaId[]',   $faixa->faixaId, '', 'hidden') ?>
                                                                <span><?php echo $faixa->name;?><br/><small><?php echo $faixa->note;?> </small></span>
                                                            </td>
                                                            <td class="text-right"><?= form_input('valorFaixaEtariaValorConfigure[]', number_format($faixa->valor, 2, ".", ""), 'class="form-control tip mask_money"') ?></td>
                                                        </tr>
                                                    <?php } else {?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativarTipoFaixaEtaria[]', $valoresFaixa->id, FALSE, ''); ?></td>
                                                            <td>
                                                                <?= form_input('tipoFaixaEtariaValorConfigure[]',   $valoresFaixa->tipo, '', 'hidden') ?>
                                                                <?= form_input('valorFaixaId[]',   $valoresFaixa->id, '', 'hidden') ?>
                                                                <span><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span>
                                                            </td>
                                                            <td class="text-right"><?= form_input('valorFaixaEtariaValorConfigure[]',   '0.00', 'class="form-control tip mask_money"') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Transporte !-->
    <div id="transporte" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('adicionar_transporte_rodoviario'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6" style="display: none;">
                        <div class="form-group">
                            <?= lang("tipo_transporte", "tipo_transporte") ?>
                            <?php
                            $opts = array(
                                'semrodoviario' => lang('semrodoviario') ,
                                'rodoviario' => lang('rodoviario') ,
                                'sem_aereo' => lang('sem_aereo'),
                                'com_aereo' => lang('com_aereo'),
                                'nao_exibir' => lang('nao_exibir')
                            );
                            echo form_dropdown('tipo_transporte', $opts, (isset($_POST['tipo_transporte']) ? $_POST['tipo_transporte'] : ($product ? $product->tipo_transporte : '')), 'class="form-control" id="tipo_transporte"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-bus"></i><?= lang('informacoes_tipo_transporte_rodoviario'); ?></div>
                            <div class="panel-body">
                                <div class="control-group table-group">
                                    <div class="controls table-controls">
                                        <div class="table-responsive">
                                            <table id="attrTable" class="table table-bordered table-condensed table-striped table-hover" style="margin-bottom: 20px;">
                                                <thead>
                                                <tr class="active">
                                                    <th style="text-align: center;width: 1%;"></th>
                                                    <th class="col-md-10" style="text-align: left;"><?= lang('transporte') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($tiposTransporte as $tipoTransporte) {
                                                    $tipoTransporteEncontrado =  $this->ProdutoRepository_model->getTransportesRodoviario($product->id, $tipoTransporte->id); ?>

                                                    <?php if (!empty($tipoTransporteEncontrado) ){?>
                                                        <?php foreach ($tipoTransporteEncontrado as $transporte) {
                                                            $ativo = $transporte->status == 'ATIVO' ? true  : false;?>
                                                            <tr>
                                                                <td style="text-align: center;"><?php echo form_checkbox('ativarTipoTransporte[]', $transporte->id, $ativo, ''); ?></td>
                                                                <td><input type="hidden" name="tipoTransporte[]" value="<?php echo $transporte->id;?>"><span><?php echo $transporte->text;?></span></td>
                                                            </tr>
                                                        <?php }?>
                                                    <?php } else {?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativarTipoTransporte[]', $tipoTransporte->id, FALSE, ''); ?></td>
                                                            <td><input type="hidden" name="tipoTransporte[]" value="<?php echo $tipoTransporte->id;?>"><span><?php echo $tipoTransporte->name;?></span></td>
                                                        </tr>
                                                    <?php } ?>

                                                <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Embarque !-->
    <div id="embarque" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('adicionar_transporte_rodoviario'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-bus"></i><?= lang('info_embarque'); ?></div>
                            <div class="panel-body">
                                <div class="control-group table-group">
                                    <div class="controls table-controls">
                                        <table id="tbLocalEmbarque" class="table items table-striped table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th style="width: 1%;"></th>
                                                <th class="col-md-5" style="text-align: left;"><?= lang("local_embarque");?></th>
                                                <th class="col-md-3" style="text-align: left;"><?= lang("note");?></th>
                                                <th class="col-md-2" style="text-align: left;"><?= lang("data_embarque");?></th>
                                                <th class="col-md-2"><?= lang("hora_embarque"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($locais_embarque as $localEmbarque){
                                                $embarques =  $this->ProdutoRepository_model->getLocaisEmbarqueRodoviario($product->id, $localEmbarque->id); ?>
                                                <?php if (!empty($embarques)){?>
                                                    <?php foreach ($embarques as $embarque){
                                                        $ativo = $embarque->status == 'ATIVO' ? true  : false;
                                                        ?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativoLocalEmbarque[]', $embarque->id, $ativo, ''); ?></td>
                                                            <td><input type="hidden" name="localEmbarque[]" value="<?php echo $embarque->id;?>"/> <?php echo $embarque->name;?></td>
                                                            <td><textarea name="noteEmbarque[]" class="skip"><?php echo $embarque->note;?></textarea></td>
                                                            <td><?= form_input('dataEmbarque[]',  $embarque->dataEmbarque, 'class="form-control tip"', 'date') ?></td>
                                                            <td><?= form_input('horaEmbarque[]', $embarque->horaEmbarque, 'class="form-control tip"', 'time') ?></td>
                                                        </tr>
                                                    <?php }?>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo form_checkbox('ativoLocalEmbarque[]', $localEmbarque->id , FALSE, ''); ?></td>
                                                        <td><input type="hidden" name="localEmbarque[]" value="<?php echo $localEmbarque->id;?>"/> <?php echo $localEmbarque->name;?></td>
                                                        <td><textarea name="noteEmbarque[]" class="skip"></textarea></td>
                                                        <td><?= form_input('dataEmbarque[]',  '', 'class="form-control tip"', 'date') ?></td>
                                                        <td><?= form_input('horaEmbarque[]',  '', 'class="form-control tip"', 'time') ?></td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- comissão de vendedores !-->
    <div id="comissao" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('informacoes_vagas_valores_do_produto'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-dollar"></i> <?= lang('informacoes_comissao'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group" id="ui" style="margin-bottom: 0;">
                                        <?= lang("quantidadePessoasViagem", "quantidadePessoasViagem") ?>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding: 2px 5px;">QTD</div>
                                            <?= form_input('quantidadePessoasViagem', (isset($_POST['quantidadePessoasViagem']) ? $_POST['quantidadePessoasViagem'] : ($product ? $this->sma->formatDecimal($product->quantidadePessoasViagem) : '0')), 'class="form-control tip mask_integer" id="quantidadePessoasViagem" required="required" ') ?>
                                        </div>
                                        <span style="color:#F43E61;">OBSERVAÇÃO: Você poderá alterar no Lançamento da Agenda.</span>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group" id="ui" style="margin-bottom: 0;">
                                        <?= lang("price", "price") ?>
                                        <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '0.00')), 'class="form-control tip mask_money" id="price" required="required" ') ?>
                                        <span style="color:#F43E61;">OBSERVAÇÃO: Deixe zero, se for usar o preço por dayuse ou hospedagem.</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="div_status">
                                        <?= lang("tipo_comissao", "tipo_comissao") ?>
                                        <?php
                                        $tipoComissao = array(
                                            'comissao_produto' => lang('status_comissao_produto'),
                                            //'comissao_vendedor' => lang('status_comissao_vendedor'),
                                            //'comissao_categoria' => lang('status_comissao_categoria'),
                                        );
                                        echo form_dropdown('tipoComissao', $tipoComissao, (isset($_POST['tipoComissao']) ? $_POST['tipoComissao'] : ($product ? $product->tipoComissao : '')), 'class="form-control" id="tipoComissao" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="div_status">
                                        <?= lang("tipoCalculoComissao", "tipo_pagamento_comissao") ?>
                                        <?php
                                        $tipoComissao = array(
                                            '1' => lang('pagamento_comissao_percentual'),
                                            '2' => lang('pagamento_comissao_absoluto'),
                                        );
                                        echo form_dropdown('tipoCalculoComissao', $tipoComissao, (isset($_POST['tipoCalculoComissao']) ? $_POST['tipoCalculoComissao'] : ($product ? $product->tipoCalculoComissao : '')), 'class="form-control" id="tipoCalculoComissao" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="margin-bottom: 0;">
                                        <?= lang("comissao", "comissao") ?>
                                        <?= form_input('comissao', (isset($_POST['comissao']) ? $_POST['comissao'] : ($product ? $this->sma->formatDecimal($product->comissao) : '0.00')), 'class="form-control tip mask_money" id="comissao"') ?>
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group">
                                        <?= lang('data_saida', 'data_saida'); ?>
                                        <input type="date" name="data_saida" value="" class="form-control tip" id="data_saida" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('data_retorno', 'data_retorno'); ?>
                                        <input type="date" name="data_retorno" value="" class="form-control tip" id="data_retorno" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('hora_chegada', 'hora_chegada'); ?>
                                        <input type="time" name="hora_chegada" value="" class="form-control tip" id="hora_chegada" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('hora_retorno', 'hora_retorno'); ?>
                                        <input type="time" name="hora_retorno" value="" class="form-control tip" id="hora_retorno" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang("local_retorno", "local_retorno") ?>
                                        <?= form_input('local_retorno', (isset($_POST['local_retorno']) ? $_POST['local_retorno'] : ($product ? $product->local_retorno : '')), 'class="form-control" id="local_retorno" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('tempo_viagem', 'tempo_viagem'); ?>
                                        <input type="number" name="tempo_viagem" value="" class="form-control tip" id="tempo_viagem" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group">
                                        <?= lang('duracao_pacote', 'duracao_pacote'); ?>
                                        <input type="number" name="duracao_pacote" value="<?php  (isset($_POST['duracao_pacote']) ? $_POST['duracao_pacote'] : ($product ? $product->duracao_pacote : '1'));?>" class="form-control tip" id="duracao_pacote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Hospedagem !-->
    <div id="hospedagem" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bed"></i><?= lang('adicionar_hospedagem'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-bed"></i> <?= lang('info_valores_hospedagem'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="attrTableHospedagem" class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;">
                                                        <thead>
                                                        <tr class="active">
                                                            <th style="text-align: center;width: 2%;"></th>
                                                            <th class="col-md-6" style="text-align: left;"><?= lang('tipo_hospedagem') ?></th>
                                                            <th class="col-md-3 estoque_hospedagem" style="text-align: left;"><?= lang('estoque_hospedagem') ?></th>
                                                            <th class="col-md-1 estoque_hospedagem" style="text-align: left;"><?= lang('qtd_pessoas_hospedagem') ?></th>
                                                            <th class="col-md-2" style="text-align: center;width: 5%;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($tiposQuarto as $tipoQuarto) {
                                                            $tiposHspedagemFaixaEtaria = $this->ProdutoRepository_model->getTipoHospedagemRodoviarioByTipoHospedage($product->id, $tipoQuarto->id);
                                                            if (!empty($tiposHspedagemFaixaEtaria)){
                                                                $ativo = $tiposHspedagemFaixaEtaria->status == 'ATIVO' ? true  : false;
                                                                ?>
                                                                <tr>
                                                                    <td style="text-align: center;"><?php echo form_checkbox('ativoTipoHospedagem[]', $tipoQuarto->id, $ativo, ''); ?></td>
                                                                    <td>
                                                                        <?= form_input('tipoHospedagemId[]', $tipoQuarto->id, '', 'hidden') ?>
                                                                        <?= form_input('attr_name[]', $tipoQuarto->name, '', 'hidden') ?>
                                                                        <span>
                                                                            <?php echo $tipoQuarto->name;?>
                                                                            <br/><small>Ocupação para <?php echo $tipoQuarto->ocupacao;?> pessoa(s)</small>
                                                                            <?php if ($tipoQuarto->note) {?>
                                                                                <br/><?php echo $tipoQuarto->note;?>
                                                                            <?php } ?>
                                                                        </span>
                                                                    </td>
                                                                    <td class="estoque_hospedagem"><?= form_input('estoque_hospedagem[]', $tiposHspedagemFaixaEtaria->estoque, 'tipo_quarto='.$tipoQuarto->id.' qtdacomodacao='. $tipoQuarto->ocupacao.' class="form-control tip mask_integer qtd_pessoas_hospedagem"'); ?></td>
                                                                    <td class="estoque_hospedagem"><?= form_input('qtd_pessoas_hospedagem[]', ($tiposHspedagemFaixaEtaria->estoque * $tipoQuarto->ocupacao), 'id="qtd_pessoas_hospedagem'.$tipoQuarto->id.'" class="form-control tip mask_integer total_pessoas_hospedagem" readonly'); ?></td>
                                                                    <td style="text-align: center;width: 5%;"><button class="btn btn-primary" data-toggle="modal" data-target="#aModalHospedagem<?php echo $tipoQuarto->id;?>" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Configurar Valor</button> </td>
                                                                </tr>
                                                            <?php } else { ?>
                                                                <tr>
                                                                    <td style="text-align: center;"><?php echo form_checkbox('ativoTipoHospedagem[]', $tipoQuarto->id, FALSE, ''); ?></td>
                                                                    <td>
                                                                        <?= form_input('tipoHospedagemId[]', $tipoQuarto->id, '', 'hidden') ?>
                                                                        <?= form_input('attr_name[]', $tipoQuarto->name, '', 'hidden') ?>
                                                                        <span>
                                                                        <?php echo $tipoQuarto->name;?>
                                                                        <br/><small><b>Ocupação para <?php echo $tipoQuarto->ocupacao;?> pessoa(s)<b/></small>
                                                                        <?php if ($tipoQuarto->note) {?>
                                                                            <br/><?php echo $tipoQuarto->note;?>
                                                                        <?php } ?>
                                                                    </span>
                                                                    </td>
                                                                    <td class="estoque_hospedagem"><?= form_input('estoque_hospedagem[]', '0', ' tipo_quarto='.$tipoQuarto->id.' qtdacomodacao='. $tipoQuarto->ocupacao.' class="form-control tip mask_integer qtd_pessoas_hospedagem"'); ?></td>
                                                                    <td class="estoque_hospedagem"><?= form_input('qtd_pessoas_hospedagem[]', '0', 'id="qtd_pessoas_hospedagem'.$tipoQuarto->id.'" class="form-control tip mask_integer total_pessoas_hospedagem" readonly'); ?></td>
                                                                    <td style="text-align: center;width: 5%;"><button class="btn btn-primary" data-toggle="modal" data-target="#aModalHospedagem<?php echo $tipoQuarto->id;?>" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Configurar Valor</button> </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php }?>
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td style="text-align: right;">Total</td>
                                                            <td class="estoque_hospedagem" style="text-align: right;"><span id="total_quartos"></span></td>
                                                            <td class="estoque_hospedagem" style="text-align: right;"><span id="total_pessoas_hospedagem"></span></td>
                                                            <td></td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Serviços adicionais !-->
    <div id="servicosopcionais" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('informacoes_servicos_adicionais'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" style="display:block;">
                            <div class="form-group">
                                <?= lang("servicos_opcionais", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                            </div>
                            <div class="control-group table-group">
                                <div class="controls table-controls">
                                    <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover" style="cursor: pointer;">
                                        <thead>
                                        <tr>
                                            <th class="col-md-11" style="text-align: left;"></th>
                                            <th class="col-md-1" style="text-align: center;display: none;"><?= lang("price"); ?></th>
                                            <th class="col-md-1" style="text-align: center;"></th>
                                            <th class="col-md-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Taxas !-->
    <div id="taxasPagamento" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-usd"></i><?= lang('informacoes_taxas_comissoes'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-usd"> </i> <?= lang('info_taxas_tipo_cobranca'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="attrTableHospedagemTaxas" class="table table-bordered table-condensed table-striped table-hover" style="margin-bottom: 20px;cursor: pointer;">
                                            <thead>
                                            <tr class="active">
                                                <th class="col-md-1" style="text-align: center;width: 1%;"><?= lang('ativar') ?></th>
                                                <th class="col-md-4" style="text-align: left;display: none;"><?= lang('forma') ?></th>
                                                <th class="col-md-1" style="text-align: center;"><?= lang('parcelas') ?></th>
                                                <th class="col-md-2" style="text-align: left;"><?= lang('tipo') ?></th>
                                                <th class="col-md-2" style="text-align: left;"></th>
                                                <th class="col-md-2" style="text-align: right;"><?= lang('valor') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($tiposCobranca as $tipoCobranca) {
                                                $contador = 0;
                                                $tb = '';
                                                ?>
                                                <?php foreach ($condicoesPagamento as $condicaoPagamento){?>
                                                    <?php if ($tipoCobranca->tipo == 'cartao' || $tipoCobranca->tipo == 'carne_cartao') {?>
                                                        <?php if ($tipoCobranca->name != $tb){?>
                                                            <tr>
                                                                <td colspan="1"></td>
                                                                <td colspan="5" style="text-align: left;font-size: 16px;"><?php echo $tipoCobranca->name;?></td>
                                                            </tr>
                                                            <?php $tb = $tipoCobranca->name;?>
                                                        <?php }?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativoCondicaoPagamento[]', '1', FALSE, ''); ?></td>
                                                            <td style="display: none;"><input type="hidden" name="tipoCobranca[]" value="<?php echo $tipoCobranca->id;?>" /> <?php echo $tipoCobranca->name;?></td>
                                                            <td style="text-align: center;"><input type="hidden" name="formaPagamento[]" value="<?php echo $condicaoPagamento->id;?>" /><?php echo $condicaoPagamento->name;?></td>
                                                            <td>
                                                                <?php
                                                                $opts = array(
                                                                    'acrescimo' => lang('acrescimo'),
                                                                    'desconto' => lang('desconto')
                                                                );
                                                                echo form_dropdown('acrescimoDescontoCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $opts = array(
                                                                    'absoluto' => lang('absoluto'),
                                                                    'percentual' => lang('em_percentual')
                                                                );
                                                                echo form_dropdown('tipoCobrancaCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?= form_input('precoCondicaoPagamento[]', '0.00', 'class="form-control tip mask_money" required="required"') ?>
                                                            </td>
                                                        </tr>
                                                    <?php } else if ($contador == 0) {?>
                                                        <tr>
                                                            <td colspan="1"></td>
                                                            <td colspan="5" style="text-align: left;font-size: 16px;"><?php echo $tipoCobranca->name;?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo form_checkbox('ativoCondicaoPagamento[]', '1', FALSE, ''); ?></td>
                                                            <td style="display: none;"><input type="hidden" name="tipoCobranca[]" value="<?php echo $tipoCobranca->id;?>" /> <?php echo $tipoCobranca->name;?></td>
                                                            <td style="text-align: center;"><input type="hidden" name="formaPagamento[]" value="<?php echo $condicaoPagamento->id;?>" /><?php echo $condicaoPagamento->name;?></td>
                                                            <td>
                                                                <?php
                                                                $opts = array(
                                                                    'acrescimo' => lang('acrescimo'),
                                                                    'desconto' => lang('desconto')
                                                                );
                                                                echo form_dropdown('acrescimoDescontoCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $opts = array(
                                                                    'absoluto' => lang('absoluto'),
                                                                    'percentual' => lang('em_percentual')
                                                                );
                                                                echo form_dropdown('tipoCobrancaCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?= form_input('precoCondicaoPagamento[]', '0.00', 'class="form-control tip mask_money" required="required"') ?>
                                                            </td>
                                                        </tr>
                                                        <?php $contador++;?>
                                                    <?php }?>
                                                <?php }?>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Agendamento de datas !-->
    <div id="datas" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('add_agenda'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <span id="new-linha-data"></span>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <button type="button" class="btn btn-primary addNewData"  style="width: 100%;"><i class="fa fa-plus-circle"></i> <?= lang('adicionar_uma_nova_data') ?></button>
                    </div>
                </div>
                <div class="row">
                    <?php if ($datasAgendadas) {?>
                        <div class="col-lg-12" style="margin-top: 10px;">
                            <table id="tbDatas" class="table table-bordered table-condensed table-striped" style="cursor: pointer;">
                                <tbody>
                                <?php
                                $mesAno = '';
                                foreach ($datasAgendadas as $dataAgendada) {

                                    $totalReservas = 0;
                                    $totalVendas = 0;

                                    //$dataAgendada = new AgendaViagem_model();
                                    $mesAnoViagem =  strtoupper($this->sma->dataDeHojePorExtensoRetornoMensAno($dataAgendada->getDataSaida()));
                                    ?>
                                    <?php if ($mesAno != $mesAnoViagem){?>
                                        <tr class="active">
                                            <th colspan="2" style="text-align: left;border: 1px solid #ffffff;background: #ffffff;">
                                                <h3> <?php echo $mesAnoViagem;?></h3>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                    <tr class="active">
                                        <th class="col-md-12" colspan="12" style="text-align: left;background: #428bca;color: #ffffff;">
                                            <?php echo strtoupper($this->sma->dataDeHojePorExtensoRetornoComSemana($dataAgendada->getDataSaida()))?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td><?= lang('data_saida') ?><br/><?= form_input('dataSaidaData[]', '' . $this->sma->hrsd($dataAgendada->getDataSaida()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td><?= lang('hora') ?><br/><?= form_input('horaSaidaData[]', '' . $dataAgendada->getHoraSaida() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td><?= lang('data_retorno') ?><br/><?= form_input('dataRetornoData[]', '' . $this->sma->hrsd($dataAgendada->getDataRetorno()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled required="required"') ?></td>
                                        <td><?= lang('hora') ?><br/><?= form_input('horaRetornoData[]', '' . $dataAgendada->getHoraRetorno() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td style="text-align: right;"><?= lang('vagas') ?><br/><?= form_input('vagasData[]', '' . $dataAgendada->getVagas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                                        <td style="text-align: right;"><?= lang('orcamento') ?><br/><?= form_input('', '' . $dataAgendada->getTotalOrcamento() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td style="text-align: right;"><?= lang('faturadas') ?><br/><?= form_input('', '' . $dataAgendada->getTotalVendasFaturas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td style="text-align: right;"><?= lang('disponivel') ?><br/><?= form_input('', '' . $dataAgendada->getTotalDisponvel() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                                        <td style="text-align: right;"><?= lang('espera') ?><br/><?= form_input('', '' . $dataAgendada->getTotalListaEspera() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                        <td class="">
                                            <br/>
                                            <div class="text-center">
                                                <div class="btn-group text-left">
                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle"
                                                            data-toggle="dropdown">Ações <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <?php if ($Owner || $Admin) { ?>
                                                            <li><a href="<?php echo base_url();?>agenda/editByProduto/<?php echo $dataAgendada->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>Editar Agenda</a></li>
                                                        <?php }?>
                                                        <?php if ($Owner || $Admin) { ?>
                                                            <li><a href="<?= site_url('reports/relatorioPassageirosProgramacao/'.$dataAgendada->id)?>"><i class="fa fa-users"></i><?= lang('Relatório Geral da Viagem') ?></a></li>
                                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros_todos/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i> Lista de Passageiros</a></li>
                                                        <?php }?>
                                                        <?php if ($Owner || $Admin) { ?>
                                                            <li class="divider"></li>
                                                            <?php foreach ($transportes as $transporte) {?>
                                                                <?php if ($transporte->status == 'ATIVO'){?>
                                                                    <li>
                                                                        <a href="<?= site_url('sales/montarPoltronas/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>">
                                                                            <i class="fa fa fa-cogs"></i> Configurar Assentos <?php echo $transporte->text;?>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php foreach ($transportes as $transporte) {?>
                                                                <?php if ($transporte->status == 'ATIVO'){
                                                                    $itens = $this->sales_model->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                                    $totalVendasItem = 0;

                                                                    if ($itens) $totalVendasItem = count($itens); ?>
                                                                    <li>
                                                                        <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                                            <i class="fa fa-print"></i>Lista de Embarque <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <li class="divider"></li>
                                                            <?php
                                                                $roomlists = $this->Roomlist_model->getAll($dataAgendada->id);
                                                            ?>
                                                            <?php foreach ($roomlists as $roomlist) {?>
                                                                <li>
                                                                    <a href="<?php echo site_url('roomlist/montar/'.$roomlist->id); ?>">
                                                                        <i class="fa fa fa-cogs"></i><?= lang('configurar').' '.$roomlist->name;?>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                            <?php foreach ($roomlists as $roomlist) {?>
                                                                <li>
                                                                    <a href="<?php echo site_url('roomlist/relatorio/'.$roomlist->id); ?>" target="_blank">
                                                                        <i class="fa fa-print"></i><?= lang('print').' '.$roomlist->name;?>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                            <li>
                                                                <a href="<?php echo site_url('roomlist/adicionarRoomList/'.$dataAgendada->id); ?>" data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-bed"></i><?= lang('adicionar_novo_room_list') ?>
                                                                </a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i><?= lang('relatorio_geral_de_passageiros') ?></a></li>
                                                            <li><a href="<?= site_url('sales/relatorio_seguradora_de_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id) ?>" target="_blank"><i class="fa fa-ambulance"></i><?= lang('relatorio_seguradora_de_passageiros') ?></a></li>
                                                            <?php foreach ($transportes as $transporte) {?>
                                                                <?php if ($transporte->status == 'ATIVO'){
                                                                    $itens = $this->sales_model->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                                    $totalVendasItem = 0;

                                                                    if ($itens) $totalVendasItem = count($itens); ?>
                                                                    <li>
                                                                        <a href="<?= site_url('sales/relatorio_enviado_empresa_onibus/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                                            <i class="fa fa-bus"></i>Lista de Passageiros ANTT <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } ?>

                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                        <?php if ($Owner || $Admin) { ?>
                                            <td class="text-center"><i class="fa fa-times delAgendamento" id="<?php echo $dataAgendada->id;?>" produto="<?php echo $dataAgendada->getProduto();?>" ></i></td>
                                        <?php } else {?>
                                            <td></td>
                                        <?php } ?>
                                    </tr>
                                    <?php $mesAno = $mesAnoViagem; ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <!-- Integracao site !-->
    <div id="site" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('informacoes_integracao_site'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("alertar_polcas_vagas", "alertar_polcas_vagas") ?>
                                <?= form_input('alertar_polcas_vagas', (isset($_POST['alertar_polcas_vagas']) ? $_POST['alertar_polcas_vagas'] : ($product ? $product->alertar_polcas_vagas : '10')), 'class="form-control tip mask_integer" required="required" id="alertar_polcas_vagas" '); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("alertar_ultimas_vagas", "alertar_ultimas_vagas") ?>
                                <?= form_input('alertar_ultimas_vagas', (isset($_POST['alertar_ultimas_vagas']) ? $_POST['alertar_ultimas_vagas'] : ($product ? $product->alertar_ultimas_vagas : '5')), 'class="form-control tip mask_integer" required="required" id="alertar_ultimas_vagas" '); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($tiposQuarto as $tipoQuarto){?>
    <!-- ################################################# -->
    <!-- ############## MODAL DAS HOSPEDAGEM ############# -->
    <!-- ################################################# -->
    <div class="modal" id="aModalHospedagem<?php echo $tipoQuarto->id?>" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="aModalLabel"><?= $tipoQuarto->name;?></h2>
                </div>
                <div class="modal-body" id="pr_popover_content">
                    <input type="hidden" id="aHospedagemId"/>
                    <div id="div_servicos_adicionais">
                        <h3 class="bold"><?= lang('valor_faixa_etaria_hospedagem') ?></h3>
                        <table class="table table-bordered table-striped table-condensed table-hover" id="tbServicosAdicionais">
                            <thead>
                            <tr>
                                <th style="width:2%;text-align: center;"></th>
                                <th style="width:30%;text-align: left;">Tipo</th>
                                <th style="width:10%;text-align: right;">Preço por pessoa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($valorFaixas as $valoresFaixa) {
                                $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemRodoviarioByTipoHospedagemTipoFaixa($product->id, $tipoQuarto->id, $valoresFaixa->id);
                                if ($faixa) {
                                    $ativo = $faixa->status == 'ATIVO' ? true  : false;?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo form_checkbox('ativarValorHospedagem[]', $tipoQuarto->id.'_'. $valoresFaixa->id, $ativo, ''); ?></td>
                                        <td>
                                            <?= form_input('faixaTipoHospedagemId[]',$tipoQuarto->id, '', 'hidden') ?>
                                            <?= form_input('tipoFaixaEtariaValorHospedagem[]',   $valoresFaixa->tipo, '', 'hidden') ?>
                                            <?= form_input('valorFaixaIdHospedagem[]',   $valoresFaixa->id, '', 'hidden') ?>
                                            <span><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span>
                                        </td>
                                        <td class="text-right"><?= form_input('valorFaixaEtariaValorHospedagem[]',   number_format($faixa->valor, 2, ".", "")  , 'class="form-control tip mask_money"') ?></td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo form_checkbox('ativarValorHospedagem[]', $tipoQuarto->id.'_'. $valoresFaixa->id, FALSE, ''); ?></td>
                                        <td>
                                            <?= form_input('faixaTipoHospedagemId[]',$tipoQuarto->id, '', 'hidden') ?>
                                            <?= form_input('tipoFaixaEtariaValorHospedagem[]',   $valoresFaixa->tipo, '', 'hidden') ?>
                                            <?= form_input('valorFaixaIdHospedagem[]',   $valoresFaixa->id, '', 'hidden') ?>
                                            <span><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span>
                                        </td>
                                        <td class="text-right"><?= form_input('valorFaixaEtariaValorHospedagem[]',   '0.00', 'class="form-control tip mask_money"') ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary updateAttr" faixaId="<?php echo $tipoQuarto->id?>"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
<?php }?>

<?= form_close(); ?>

<script type="text/javascript">

    var items = {};
    var row;

    var warehouses = <?= json_encode($warehouses); ?>;
    var fornecedores = <?= json_encode($fornecedores); ?>;
    var transportes = <?= json_encode($transportes); ?>;
    var hospedagens = <?= json_encode($hospedagens); ?>;
    var items_faixa_etaria = <?= json_encode($varsFaixaEtariaServicoAdicional); ?>;

    $(document).ready(function () {

        var $supplier = $('#aFornecedor');

        $supplier.change(function (e) {
            $('#aFornecedor').val($(this).val());
        });

        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };
                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };
                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $('#aFornecedor').select2({
            minimumInputLength: 0,
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        //var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        //var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');

        <?php
        if($combo_items) {
            foreach($combo_items as $item) {
                if($item->code) {
                    echo '
                    
                    var itemj = '. json_encode($item).';
                     
                     if (itemj.valores !== null) {
                         $.each(itemj.valores, function (index, fv) {                         
                             eval("itemj.valor"+fv.faixaId+" = "+fv.valor);
                             eval("itemj.status"+fv.faixaId+" = "+fv.status);
                         });
                         itemj.valores = null;
                     }
                    add_product_item(itemj);';
                }
            }
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");': '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) ? '$("#promotion").iCheck("check");': '' ?>
        $('#promotion').on('ifChecked', function (e) {
            $('#promo').slideDown();
        });
        $('#promotion').on('ifUnchecked', function (e) {
            $('#promo').slideUp();
        });

        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });

        $('#type').change(function () {

            var t = $(this).val();
            if (t !== 'standard') {
                $('.standard').slideDown();
                $('#datas_viagem').show();
                $('#div_status').show();
                $('#div_category').show();
                $('#div_product_details').show();
                $('#track_quantity').iCheck('uncheck');
            } else {
                $('.standard').slideDown();
                $('#datas_viagem').show();
                $('#div_status').show();
                $('#div_category').show();
                $('#div_product_details').show();
                $('#track_quantity').iCheck('check');
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#digital_file').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
            } else {
                $('.digital').slideDown();
                $('#digital_file').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
            }

            if (t !== 'combo') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideUp();
            }

            if (t == 'service') {
                $('.combo').slideUp();
                $('.standard').slideUp();
                $('.digital').slideUp();
                $('#datas_viagem').hide();
                $('#div_status').hide();
                $('#div_category').hide();
                $('#div_product_details').hide();
            }
        });

        $('.delAgendamento').click(function (event) {
            if (confirm('Deseja realmente excluir o lançamento para esta data?')) {
                let agendamentoId = $(this).attr('id');
                let produtoId = $(this).attr('produto');

                window.location = site.base = 'products/excluirProgramacaoData/'+agendamentoId+'/'+produtoId;
            }
        });

        $('.visualizar-informacoes').click(function (event) {

            let id = $(this).attr('id');

            if ($( ".visualizar-informacoes-item-"+id).is( ":visible" ) ) {
                $('.visualizar-informacoes-item-'+id).hide(300);
            } else {
                $('.visualizar-informacoes-item-'+id).show(300);
            }
        });

        $('.addNewData').click(function (event){
            $.ajax({
                url: site.base_url + "agenda/view_lancamento_agenda_data",
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#new-linha-data').append(html);

                $('.removeItemData').click(function (event) {
                    $(this).parent().parent().remove();
                });

                adicionarCamposObrigatorioAoFormulario();
            });
        });

        var t = $('#type').val();
        if (t !== 'standard') {
            $('#track_quantity').iCheck('uncheck');
        } else {
            $('#track_quantity').iCheck('check');
        }
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#digital_file').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
        } else {
            $('.digital').slideDown();
            $('#digital_file').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
        }
        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideUp();
        }

        $("#add_item").autocomplete({
            source: '<?= site_url('products/suggestions_adicionais'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);

                    $('.attrServicosAdicionais').click(function() {

                        row_adicional = $(this).closest("tr");

                        var nomeServicoAdicional = row_adicional.children().children('.rNomeAdicional').val();

                        <?php foreach ($valorFaixas as $valoresFaixa) {?>

                        let rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val();
                        let rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rValorFaixaAdicional<?php echo $valoresFaixa->id;?>').val();
                        let isAtivoServicoAdicional<?php echo $valoresFaixa->id;?> = getBoolean(row_adicional.children().children('.isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>').val());

                        if (rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === undefined ||
                            rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === ''  ||
                            rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === '0') {

                            rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = '0.00';
                        }

                        if (isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>) {
                            $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', true);
                        } else {
                            $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', false);
                        }

                        $('#faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val(rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>);
                        $('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val(rValorFaixaAdicional<?php echo $valoresFaixa->id;?>);
                        <?php } ?>

                        $('#servicoAdicionalId').val(row_adicional.children().eq(0).find('input').val());
                        $('input[type="checkbox"],[type="radio"]').iCheck('update');
                        $('#nmServicoAdicionalTitle').html(nomeServicoAdicional);

                        $('#aModalFaixaEtariaServicoAdicional').appendTo('body').modal('show');

                    });

                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });
        <?php
        if($this->input->post('type') == 'combo') {
            $c = sizeof($_POST['combo_item_code']);
            for ($r = 0; $r <= $c; $r++) {
                if(isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                    $items[] = array('id' => $_POST['combo_item_id'][$r], 'name' => $_POST['combo_item_name'][$r], 'code' => $_POST['combo_item_code'][$r], 'qty' => $_POST['combo_item_quantity'][$r], 'price' => $_POST['combo_item_price'][$r]);
                }
            }
            echo '
            var ci = '.json_encode($items).';
            $.each(ci, function() { add_product_item(this); });
            ';
        }
        ?>

        function add_product_item(item) {

            if (item == null) return false;

            item_id = item.id;

            if (items[item_id]) items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            else  items[item_id] = item;

            $("#prTable tbody").empty();

            $.each(items, function () {

                var row_no  = this.id;
                var newTr   = $('<tr id="row_' + row_no + '" class="item_' + this.id + '"></tr>');

                let tr_html = '' +
                    '<td>' +
                    '   <input name="combo_item_id[]" type="hidden" value="' + this.id + '">' +
                    '   <input name="combo_item_name[]" type="hidden" value="' + this.name + '">' +
                    '   <input name="combo_item_code[]" type="hidden" value="' + this.code + '">' +
                    '   <span id="name_' + row_no + '"><span>' + this.name + ' </span>' +
                    '</td>';

                tr_html += '<td style="display: none;"><input type="text" class="form-control tip mask_money" name="combo_item_price[]" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td><button type="button" class="btn btn-primary attrServicosAdicionais"><i class="fa fa-plus"></i> Configurar Valor Por Faixa</button></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';

                tr_html += '<td style="display: none">' +
                    '<input type="hidden" class="rNomeAdicional"        value="'+this.name +'">' +
                    '<input type="hidden" class="rComissao"             name="servicoAdicionalValorComissao[]"  value="'+getRow(this.comissao)+'">' +
                    '<input type="hidden" class="rFornecedor"           name="servicoAdicionalFornecedor[]"     value="'+getRow(this.fornecedorId)+'">';

                <?php foreach ($valorFaixas as $valoresFaixa) {?>
                tr_html += '<input type="hidden" class="rServicoId<?php echo $valoresFaixa->id;?>"                  name="servicoAdicionalId[]"       value="'+this.id+'">';
                tr_html += '<input type="hidden" class="rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>"    name="faixaIdServicoAdicional[]"  value="<?php echo $valoresFaixa->id;?>">';
                tr_html += '<input type="hidden" class="rValorFaixaAdicional<?php echo $valoresFaixa->id;?>"        name="servicoAdicionalValor[]"    value="'+getRow(this.valor<?php echo $valoresFaixa->id;?>)+'">';
                tr_html += '<input type="hidden" class="isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>"     name="isAtivoServicoAdicional[]"  value="'+getRow(this.status<?php echo $valoresFaixa->id;?>)+'">';
                <?php }?>

                tr_html += '</td>';
                newTr.html(tr_html);
                newTr.prependTo("#prTable");
            });
            return true;
        }

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');

            delete items[id];
            //delete items_faixa_etaria[id];

            $('#attrTableServicosAdicionaisFaixaEtaria'+id).remove();
            $(this).closest('#row_' + id).remove();

            $('#save').click();
        });

        var su = 2;
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                var html = '<div style="clear:both;height:5px;"></div><div class="row"><div class="col-xs-12"><div class="form-group"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);
                    img.src = 'images/' + result[i];
                }
            }
        });

        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });

        $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });

        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
            var valorSugerido = 0;
            $("input[name='attr_totalPreco[]']").each(function(){
                var attr_totalPreco = $(this).val();
                if (attr_totalPreco != '') {
                    attr_totalPreco = parseFloat(attr_totalPreco);
                    valorSugerido = valorSugerido + attr_totalPreco;
                }
            });
        });

        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });

        $('#aModal').on('shown.bs.modal', function () {
            $('#aquantity').focus();
            $(this).keypress(function( e ) {
                if ( e.which === 13 ) {
                    $('#updateAttr').click();
                }
            });
        });


        $('#adicionarFaxiaEtariaServicosAdicionais').click(function() {
            preencherFaixaEtariaServicoAdicionalByForm();
        });

        $('.attrServicosAdicionais').click(function() {

            row_adicional = $(this).closest("tr");

            var nomeServicoAdicional = row_adicional.children().children('.rNomeAdicional').val();

            <?php foreach ($valorFaixas as $valoresFaixa) {?>

            let rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val();
            let rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rValorFaixaAdicional<?php echo $valoresFaixa->id;?>').val();
            let isAtivoServicoAdicional<?php echo $valoresFaixa->id;?> = getBoolean(row_adicional.children().children('.isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>').val());

            if (rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === undefined ||
                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === ''  ||
                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === '0') {

                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = '0.00';
            }

            if (isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>) {
                $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', true);
            } else {
                $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', false);
            }

            $('#faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val(rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>);
            $('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val(rValorFaixaAdicional<?php echo $valoresFaixa->id;?>);
            <?php } ?>


            $('#servicoAdicionalId').val(row_adicional.children().eq(0).find('input').val());
            $('input[type="checkbox"],[type="radio"]').iCheck('update');
            $('#nmServicoAdicionalTitle').html(nomeServicoAdicional);

            $('#aModalFaixaEtariaServicoAdicional').appendTo('body').modal('show');
            $('#add_product').attr('disabled', false);

        });

    });

    <?php if ($product) { ?>
    $(document).ready(function () {

        var t = "<?=$product->type?>";

        if (t !== 'standard') {
            $('.standard').slideUp();
            $('#track_quantity').iCheck('uncheck');
        } else {
            $('.standard').slideDown();
            $('#track_quantity').iCheck('check');
        }

        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#digital_file').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
        } else {
            $('.digital').slideDown();
            $('#digital_file').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
        }

        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideDown();
        }

        $("#code").parent('.form-group').addClass("has-error");
        $("#code").focus();
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");

        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: scdata
                    });
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                        placeholder: "<?= lang('no_subcategory') ?>",
                        data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                    });
                }
            }
        });

        <?php if ($product->supplier1) { ?>
        select_supplier('supplier1', "<?= $product->supplier1; ?>");
        $('#supplier_price').val("<?= $product->supplier1price == 0 ? '' : $this->sma->formatDecimal($product->supplier1price); ?>");
        <?php } ?>

        <?php if ($product->supplier2) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_2', "<?= $product->supplier2; ?>");
        $('#supplier_2_price').val("<?= $product->supplier2price == 0 ? '' : $this->sma->formatDecimal($product->supplier2price); ?>");
        <?php } ?>

        <?php if ($product->supplier3) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_3', "<?= $product->supplier3; ?>");
        $('#supplier_3_price').val("<?= $product->supplier3price == 0 ? '' : $this->sma->formatDecimal($product->supplier3price); ?>");
        <?php } ?>

        <?php if ($product->supplier4) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_4', "<?= $product->supplier4; ?>");
        $('#supplier_4_price').val("<?= $product->supplier4price == 0 ? '' : $this->sma->formatDecimal($product->supplier4price); ?>");
        <?php } ?>

        <?php if ($product->supplier5) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_5', "<?= $product->supplier5; ?>");
        $('#supplier_5_price').val("<?= $product->supplier5price == 0 ? '' : $this->sma->formatDecimal($product->supplier5price); ?>");
        <?php } ?>

        function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= site_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        }
        var whs = $('.wh');
        $.each(whs, function () {
            $(this).val($('#r' + $(this).attr('id')).text());
        });
    });
    <?php } ?>

    $(document).ready(function () {
        $('.change_img').click(function (event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            $('#pr-image').attr('src', img_src);
            return false;
        });
    });

    function adicionarCamposObrigatorioAoFormulario() {

        $('form[data-toggle="validator"]').data('bootstrapValidator', null);
        $('form[data-toggle="validator"]').bootstrapValidator();

        //$('form[data-toggle="validator"]').data('bootstrapValidator').destroy();

        $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

        let fields = $('.form-control');
        $.each(fields, function() {

            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#'+id;

            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                if ($("label[for='" + id + "']").html() !== undefined) {
                    let label =  $("label[for='" + id + "']").html().replace('*', '');
                    $("label[for='" + id + "']").html(label + ' *');
                    $(document).on('change', iid, function () {
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                    });
                }
            }
        });
    }

    function preencherFaixaEtariaServicoAdicionalByForm() {

        let servicoId = $('#servicoAdicionalId').val();
        let fornecedorId = $('#fornecedorServicoAdicional').val();
        let nomeServicoAdicional = $('#nmServicoAdicionalTitle').html();
        let comissao = formatDecimal($('#comissaoServicoAdicional').val());
        let item = items[parseFloat(servicoId)];

        let html =
            ' ' +
            '<input type="hidden" class="rComissao" name="servicoAdicionalValorComissao[]" value="' + comissao + '">' +
            '<input type="hidden" class="rFornecedor" name="servicoAdicionalFornecedor[]" value="' + fornecedorId + '">' +
            '<input type="hidden" class="rNomeAdicional" value="' + nomeServicoAdicional + '">';

        <?php foreach ($valorFaixas as $valoresFaixa) {?>

        let faixaIdServicoAdicional<?php echo $valoresFaixa->id;?> = $('#faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val();
        let valorAdicional<?php echo $valoresFaixa->id;?> = formatDecimal($('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val());
        let statusAdicional<?php echo $valoresFaixa->id;?> = $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').is(":checked");

        html += '<input type="hidden" class="rServicoId<?php echo $valoresFaixa->id;?>"                 name="servicoAdicionalId[]"         value="' + servicoId + '">'
        html += '<input type="hidden" class="rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>"   name="faixaIdServicoAdicional[]"    value="' + faixaIdServicoAdicional<?php echo $valoresFaixa->id;?> +'">';
        html += '<input type="hidden" class="rValorFaixaAdicional<?php echo $valoresFaixa->id;?>"       name="servicoAdicionalValor[]"      value="' + valorAdicional<?php echo $valoresFaixa->id;?> + '">';
        html += '<input type="hidden" class="isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>"    name="isAtivoServicoAdicional[]"    value="' + statusAdicional<?php echo $valoresFaixa->id;?> + '">';

        $('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val('0.00');

        item.faixaId<?php echo $valoresFaixa->id;?> = faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>;
        item.valor<?php echo $valoresFaixa->id;?>   = valorAdicional<?php echo $valoresFaixa->id;?>;
        item.status<?php echo $valoresFaixa->id;?>  = statusAdicional<?php echo $valoresFaixa->id;?>;

        <?php } ?>

        row_adicional.children().eq(4).html(html);

        $('#aModalFaixaEtariaServicoAdicional').modal('hide');
    }

</script>

<!-- #################################################################### -->
<!-- ######## MODAL VALOR POR FAIXA ETARIA SERVICOS OPCIONAIS ########### -->
<!-- #################################################################### -->
<div class="modal" id="aModalFaixaEtariaServicoAdicional" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="nmServicoAdicionalTitle"><?= lang('add_faixa_etaria') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <input type="hidden" value="" id="servicoAdicionalId"/>
                <table class="table table-bordered table-striped table-condensed table-hover" id="tbValorFaixaEtariaServicosAdicionais">
                    <thead>
                    <tr>
                        <th style="width:1%;text-align: center;"><?= lang('ativar') ?></th>
                        <th style="width:30%;text-align: left;">Tipo</th>
                        <th style="width:10%;text-align: right;">Preço por pessoa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($valorFaixas as $valoresFaixa) {?>
                        <tr>
                            <td style="text-align: center;">
                                <?php echo form_checkbox('', $valoresFaixa->id, FALSE, 'id="statusValorFaixaServicoAdicional'.$valoresFaixa->id.'"'); ?>
                                <?= form_input('faixaIdServicoAdicional', $valoresFaixa->id, 'id=faixaIdServicoAdicional'.$valoresFaixa->id, 'hidden') ?>
                            </td>
                            <td><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span></td>
                            <td class="text-right"><?= form_input('',   '0.00', 'id="valorFaixaServicoAdicional'.$valoresFaixa->id.'" class="form-control tip mask_money"') ?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="adicionarFaxiaEtariaServicosAdicionais"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>