<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update497 extends CI_Migration {

    public function up() {
        $this->db->update('settings',  array('enviar_notificacao_whatsapp' => 0), array('setting_id' => 1));
        $this->db->update('settings',  array('version' => '4.9.7'), array('setting_id' => 1));
    }

    public function down() {}
}
