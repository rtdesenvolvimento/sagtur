
<style type="text/css" media="screen">
    #SearhDataCustomer td:nth-child(1) {display: none;}
    #SearhDataCustomer td:nth-child(4) {text-align: center;}
    #SearhDataCustomer td:nth-child(5) {text-align: center;}
    #SearhDataCustomer td:nth-child(6) {text-align: center;}
</style>
<script>
    $(document).ready(function () {
        var cTable = $('#SearhDataCustomer').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": 10,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('customers/getCustomersSearh') ?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "input_search_customer", "value":  $('#input_search_customer').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                $(nRow).on('click', function () {
                    parent.preencher_customer<?=$prefix;?>(aData[0]);
                });
                return nRow;
            },
            "aoColumns":
                [
                    {"bSortable": false, "mRender": checkbox},
                    null,
                    null,
                    null,
                    null,
                    null
                ]
        }).dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('vat_no');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('document_customer');?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('searh_customer'); ?></h4>
        </div>
        <div class="modal-body">
            <p><?= lang('search_terms'); ?></p>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("input_search", "input_search"); ?>
                        <div class="input-group">
                            <?= form_input('input_search_customer', (isset($_POST['input_search_customer']) ? $_POST['input_search_customer'] : ''), 'class="form-control"  placeholder="' . lang("search_terms_customer") . '" id="input_search_customer"'); ?>
                            <div class="input-group-addon no-print" style="padding: 2px 8px;cursor: pointer;" id="buttom_search_customer">
                                <i class="fa fa-search"  style="font-size: 1.2em;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="SearhDataCustomer" cellpadding="0" cellspacing="0" border="0"  style="cursor: pointer;"
                               class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                            <tr class="primary">
                                <th style="text-align: center;width: 1%;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                                <th style="text-align: left;"><?= lang("name"); ?></th>
                                <th style="text-align: left;"><?= lang("email_address"); ?></th>
                                <th><?= lang("phone"); ?></th>
                                <th style="text-align: center;"><?= lang("vat_no"); ?></th>
                                <th style="text-align: center;"><?= lang("document_customer"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $("#input_search_customer").keypress(function (event) {
            if (event.which === 13) {
                $('#SearhDataCustomer').DataTable().fnClearTable();
            }
        });

        $("#buttom_search_customer").click(function (event) {
            $('#SearhDataCustomer').DataTable().fnClearTable();
        });

    });

</script>