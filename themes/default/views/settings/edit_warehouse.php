<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_warehouse'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id'=> 'action-form-warehouse-submit');
        echo form_open_multipart("system_settings/edit_warehouse/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <label class="control-label" for="code"><?php echo $this->lang->line("code"); ?></label>
                <div class="controls"> <?php echo form_input('code', $warehouse->code, 'class="form-control" id="code" required="required"'); ?> </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="company_name"><?php echo $this->lang->line("company_name"); ?></label>
                <div class="controls"> <?php echo form_input('name', $warehouse->name, 'class="form-control" id="company_name" required="required"'); ?> </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="first_name"><?php echo $this->lang->line("first_name"); ?> <small>(<?php echo $this->lang->line("administrator_name"); ?>)</small></label>
                <div class="controls"> <?php echo form_input('first_name', $warehouse->first_name, 'class="form-control" id="first_name" required="required"'); ?></div>
            </div>
            <div class="form-group">
                <label class="control-label" for="last_name"><?php echo $this->lang->line("last_name"); ?> <small>(<?php echo $this->lang->line("administrator_name"); ?>)</small></label>
                <div class="controls"> <?php echo form_input('last_name', $warehouse->last_name, 'class="form-control" id="last_name" required="required"'); ?></div>
            </div>
            <div class="form-group">
                <label class="control-label" for="phone"><?php echo $this->lang->line("phone"); ?></label>
                <div class="controls"> <?php echo form_input('phone', $warehouse->phone, 'class="form-control" id="phone"'); ?> </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="email"><?php echo $this->lang->line("email"); ?></label>
                <div class="controls"> <?php echo form_input('email', $warehouse->email, 'class="form-control" id="email"'); ?> </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="cnpj"><?php echo $this->lang->line("cnpj"); ?></label>
                <div class="controls"> <?php echo form_input('cnpj', $warehouse->cnpj, 'class="form-control" id="cnpj" required="required"'); ?></div>
            </div>
            <div class="form-group">
                <label class="control-label" for="cnpj"><?php echo $this->lang->line("cpf"); ?> <small>(<?php echo $this->lang->line("administrator_name"); ?>)</small></label>
                <div class="controls"> <?php echo form_input('cpf',  $warehouse->cpf, 'class="form-control" id="cpf"'); ?></div>
            </div>
            <div class="form-group">
                <label class="control-label" for="address"><?php echo $this->lang->line("address"); ?></label>
                <div class="controls"> <?php echo form_textarea('address', $warehouse->address, 'class="form-control" id="address" required="required"'); ?> </div>
            </div>
            <div class="form-group">
                <?= lang("warehouse_map", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false" class="form-control file">
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_button('create_tecnospeed_integration', lang('create_tecnospeed_integration'), 'id="create_tecnospeed_integration" class="btn btn-secondary"'); ?>

            <?php echo form_button('create_webhook', lang('create_webhook'), 'id="create_webhook" class="btn btn-secondary"'); ?>

            <?php echo form_submit('edit_warehouse', lang('edit_warehouse'), 'id="edit_warehouse" class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script type="text/javascript">
    $('body').on('click', '#create_tecnospeed_integration', function(e) {
       if (confirm('Deseja relamente criar a integração com a Tecnospeed?')) {
           $('#action-form-warehouse-submit').attr('action', '<?= site_url('contracts/create_company_tecnospeed_plugsign_integration/' . $id) ?>');
           $('#edit_warehouse').click();
       }
    });

    $('body').on('click', '#create_webhook', function(e) {
        if (confirm('Deseja relamente criar o webhook?')) {
            $('#action-form-warehouse-submit').attr('action', '<?= site_url('contracts/create_webhook') ?>');
            $('#edit_warehouse').click();
        }
    });
</script>