<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MessageGroupRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($active = 1) {

        $q = $this->db->get_where('message_groups', array('active' => $active));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getById($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('message_groups');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getGroupByProgramacao($programacaoID)
    {
        $this->db->limit(1);
        $this->db->where("active", 1);
        $this->db->where("programacao_id", $programacaoID);
        $q = $this->db->get('message_groups');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getParticipant($groupId, $phone)
    {
        $this->db->limit(1);
        $this->db->where("message_group_id", $groupId);
        $this->db->where("phone", $phone);
        $this->db->where("active", 1);
        $q = $this->db->get('message_group_participants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function existParticipant($groupId, $phone)
    {
        $q = $this->db->get_where('message_group_participants', array('message_group_id' => $groupId, 'phone' => $phone), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getParticipantByID($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('message_group_participants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function delete_participant($id): bool
    {
        if ($this->db->update("message_group_participants", array('active' => false), array('id' => $id))) {
            return true;
        }
        return false;
    }

    function add_admin_group($id): bool
    {
        if ($this->db->update("message_group_participants", array('administrador' => true), array('id' => $id))) {
            return true;
        }
        return false;
    }

    function editGroup($id, $data): bool
    {
        if ($this->db->update("message_groups", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function admins($groupID)
    {
        $q = $this->db->get_where('message_group_participants', array('message_group_id' => $groupID, 'administrador' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllParticipantsActive($groupID)
    {
        $q = $this->db->get_where('message_group_participants', array('message_group_id' => $groupID, 'active' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}