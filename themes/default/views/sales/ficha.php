<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>


<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
.MsoChpDefault
	{font-size:11.0pt;}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:595.0pt 841.9pt;
	margin:72.0pt 92.0pt 72.0pt 100.0pt;}
div.WordSection1
	{page:WordSection1;}
@page WordSection2
	{size:595.0pt 841.9pt;
	margin:72.0pt 92.0pt 72.0pt 100.0pt;}
div.WordSection2
	{page:WordSection2;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=PT-BR>

<?php foreach ($itens_pedidos as $row) {?>

	<div class=WordSection1 style="margin:2cm;">

	 
	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:181.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'></span></font></p>

	 
	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:177.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>ANEXO </br>MODELO</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:127.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>TARJETA DE ENTRADA/SALIDA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	7.75pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:170.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1era.
	VIA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-42;
	margin-left:-21px;margin-top:15px;width:569px;height:2px'><img width=569
	height=2 src="<?= $assets ?>ficha/image001.gif"></span><span
	style='position:absolute;z-index:-41;margin-left:546px;margin-top:15px;
	width:2px;height:796px'><img width=2 height=796
	src="<?= $assets ?>ficha/image002.gif"></span><span
	style='position:absolute;z-index:-40;margin-left:-21px;margin-top:15px;
	width:2px;height:796px'><img width=2 height=796
	src="<?= $assets ?>ficha/image003.gif"></span><span
	style='position:absolute;z-index:-39;margin-left:-21px;margin-top:809px;
	width:569px;height:2px'><img width=569 height=2
	src="<?= $assets ?>ficha/image001.gif"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	16.5pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:141.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>MERCOSUR/MERCOSUL</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:127.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>TARJETA DE ENTRADA/SALIDA</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:131.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>CART&Atilde;O DE ENTRADA/SA&Iacute;DA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-38;
	margin-left:-1px;margin-top:42px;width:2px;height:40px'><img width=2 height=40
	src="<?= $assets ?>ficha/image004.gif"></span><span
	style='position:absolute;z-index:-37;margin-left:210px;margin-top:42px;
	width:2px;height:40px'><img width=2 height=40
	src="<?= $assets ?>ficha/image004.gif"></span><span
	style='position:absolute;z-index:-36;margin-left:-1px;margin-top:42px;
	width:213px;height:2px'><img width=213 height=2
	src="<?= $assets ?>ficha/image005.gif"></span><span
	style='position:absolute;z-index:-35;margin-left:-1px;margin-top:80px;
	width:213px;height:2px'><img width=213 height=2
	src="<?= $assets ?>ficha/image006.gif"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	12.05pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:1.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>SECUENCIA/SEQUENCIAL</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:relative;z-index:-34'><span
	style='position:absolute;left:190px;top:-5px;width:2px;height:30px'><img
	width=2 height=30
	src="<?= $assets ?>ficha/image007.gif"></span></span><span
	style='position:absolute;z-index:-33;margin-left:-2px;margin-top:53px;
	width:540px;height:97px'><img width=540 height=97
	src="<?= $assets ?>ficha/image008.jpg"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.3pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:31.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>APELLIDO
	Y NOMBRE-/ NOME COMPLETO</span></font></p>

	<?php 
		
		$customer 		= $row->customer;
		$customer_id 	= $row->customer_id;
		
		$cliente 			= $this->site->getCompanyByID($customer_id);
		$data_aniversario	= $cliente->data_aniversario;
		$vat_no				= $cliente->cf1;

        //$customer = substr($customer, 0, 28);
 	?>
	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='text-transform: uppercase;font-size:12.0pt;font-family:"Times New Roman","serif"'>
 	<?php for($d=0;$d< strlen( $customer );$d++): 
		$letra = mb_substr( $customer,$d,1,'UTF-8');
		if ($letra == ' ') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'i') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'I') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'j') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'J') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		?> 
		<?php  echo '&nbsp;'.$letra; ?> 
	<?php endfor;?>
	</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-32;
	margin-left:-1px;margin-top:13px;width:539px;height:153px'><img width=539
	height=153 src="<?= $assets ?>ficha/image009.jpg"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>  </span></font><font
	size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TIPO
	Y NUMERO DE DOCUMENTO<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$vat_no;?> </span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>TIPO
	E NUMERO DE DOCUMENTO</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	18.8pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'> &nbsp;</span></font></p>

	<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
	 style='margin-left:1.0pt;border-collapse:collapse'>
	 <tr height=14 style='height:10.35pt'>
	  <td width=29 height=14 valign=bottom style='width:22.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font>
	 
	  </p>
	  </td>
	  <td width=195 height=14 valign=bottom style='width:146.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>CEDULA
	  DE IDENTIDAD</span></font></p>
	  </td>
	  <td width=41 height=14 valign=bottom style='width:31.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=85 height=14 valign=bottom style='width:64.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>D.N.I.</span></font></p>
	  </td>
	  <td width=56 height=14 valign=bottom style='width:42.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=104 height=14 valign=bottom style='width:78.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PASAPORTE</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=29 height=16 valign=bottom style='width:22.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=195 height=16 valign=bottom style='width:146.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>CARTEIRA
	  DE IDENTIDADE</span></font></p>
	  </td>
	  <td width=41 height=16 valign=bottom style='width:31.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=85 height=16 valign=bottom style='width:64.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>I.E./I.C.</span></font></p>
	  </td>
	  <td width=56 height=16 valign=bottom style='width:42.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=104 height=16 valign=bottom style='width:78.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PASSAPORTE</span></font></p>
	  </td>
	 </tr>
	</table>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	12.65pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
	 style='margin-left:.5pt;border-collapse:collapse'>
	 <tr height=14 style='height:10.45pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border-top:solid windowtext 1.0pt;border-left:solid windowtext 1.0pt;
	  border-bottom:none;border-right:none;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>FECHA
	  DE NACIMIENTO</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=14 valign=bottom style='width:29.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>DIA</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=14 valign=bottom style='width:43.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>MES</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=14 valign=bottom style='width:69.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>A&Ntilde;O</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;border-top:solid windowtext 1.0pt;
	  border-left:none;border-bottom:none;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border-top:solid windowtext 1.0pt;
	  border-left:none;border-bottom:none;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:17.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>SEXO</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.3pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.3pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:9.1pt;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>DATA
	  DE NASCIMENTO</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=14 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'> 
	  <?php 
	  $dia 	= date('d', strtotime($data_aniversario) );
	  
	  $dia_0 = mb_substr( $dia,0,1,'UTF-8');
	  $dia_1 = mb_substr( $dia,1,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;'.$dia_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$dia_1;
	  
	  ?>
	  </span></font></p>
	  </td>
	  <td width=57 colspan=3 height=14 valign=bottom style='width:40.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>
	  
	  <?php 
	  $mes 	= date('m', strtotime($data_aniversario) );
	  
	  $mes_0 = mb_substr( $mes,0,1,'UTF-8');
	  $mes_1 = mb_substr( $mes,1,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$mes_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$mes_1;
	  
	  ?>
	  
	  </span></font></p>
	  </td>
	  <td width=92 colspan=4 height=14 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>
	  
	  <?php 
	  $ano 	= date('Y', strtotime($data_aniversario) );
	  
	  $ano_0 = mb_substr( $ano,2,1,'UTF-8');
	  $ano_1 = mb_substr( $ano,3,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$ano_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$ano_1;
	  
	  ?>
	  
	  </span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=21 style='height:16.1pt'>
	  <td width=135 height=21 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=21 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=21 valign=bottom style='width:55.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=21 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=21 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 rowspan=2 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:7.45pt;margin-bottom:
	  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.
	   &nbsp;&nbsp;&nbsp;&nbsp;M&nbsp;&nbsp;&nbsp;&nbsp;2. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P</span></font></p>
	  </td>
	  <td width=0 height=21 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=9 style='height:6.85pt'>
	  <td width=135 height=9 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=9 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=9 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=9 valign=bottom style='width:29.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=9 valign=bottom style='width:43.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=9 valign=bottom style='width:69.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=9 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=9 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=8 style='height:5.7pt'>
	  <td width=156 colspan=2 height=8 valign=bottom style='width:117.0pt;
	  border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=8 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=8 valign=bottom style='width:29.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=8 valign=bottom style='width:43.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=8 valign=bottom style='width:69.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=8 valign=bottom style='width:11.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=8 valign=bottom style='width:79.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=8 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=17 style='height:12.9pt'>
	  <td width=156 colspan=2 height=17 valign=bottom style='width:117.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=17 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=17 valign=bottom style='width:29.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=17 valign=bottom style='width:43.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=17 valign=bottom style='width:69.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=17 valign=bottom style='width:90.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=17 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.45pt'>
	  <td width=156 colspan=2 height=14 valign=bottom style='width:117.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>NACIONALIDAD</span></font></p>
	  </td>
	  <td width=73 height=14 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=14 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=156 colspan=2 height=16 valign=bottom style='width:117.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>NACIONALIDADE</span></font></p>
	  </td>
	  <td width=73 rowspan=2 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:19.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><b><font size=5 face=Arial><span style='font-size:20.0pt;font-family:
	  "Arial","sans-serif";font-weight:bold'>X</span></font></b></p>
	  </td>
	  <td width=39 colspan=2 height=16 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=16 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=16 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=27 style='height:19.95pt'>
	  <td width=135 height=27 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font></p>
	  </td>
	  <td width=21 height=27 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
	  text-align:right;line-height:normal;text-autospace:none'><font size=1
	  face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=27 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=27 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=27 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:57.0pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>4.</span></font></p>
	  </td>
	  <td width=120 colspan=2 rowspan=2 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:16.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>OTRA/OUTRA</span></font></p>
	  </td>
	  <td width=0 height=27 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=135 height=16 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:39.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>ARGENTINA</span></font></p>
	  </td>
	  <td width=21 height=16 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=16 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:3.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>BRASIL</span></font></p>
	  </td>
	  <td width=96 colspan=5 height=16 valign=bottom style='width:72.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:16.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PARAGUAY</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>URUGUAY</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=2 style='height:1.45pt'>
	  <td width=229 colspan=3 height=2 valign=bottom style='width:172.0pt;
	  border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=2 valign=bottom style='width:29.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=2 valign=bottom style='width:43.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=2 valign=bottom style='width:69.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=2 valign=bottom style='width:90.0pt;
	  border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=2 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=8 style='height:6.2pt'>
	  <td width=229 colspan=3 height=8 valign=bottom style='width:172.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=8 valign=bottom style='width:25.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=8 valign=bottom style='width:11.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=8 valign=bottom style='width:79.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=8 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.45pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PAIS
	  DE RESIDENCIA</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=14 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=15 style='height:11.25pt'>
	  <td width=229 colspan=3 height=15 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PAIS
	  DE RESIDENCIA</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=15 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=15 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=15 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=15 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=15 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=28 style='height:21.1pt'>
	  <td width=135 height=28 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font></p>
	  </td>
	  <td width=21 height=28 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
	  text-align:right;line-height:normal;text-autospace:none'><font size=1
	  face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=73 height=28 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:19.0pt;margin-bottom:.0001pt;line-height:21.1pt;text-autospace:
	  none'><b><font size=5 face=Arial><span style='font-size:20.0pt;font-family:
	  "Arial","sans-serif";font-weight:bold'>X</span></font></b></p>
	  </td>
	  <td width=39 colspan=2 height=28 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=28 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=28 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:57.0pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>4.</span></font></p>
	  </td>
	  <td width=120 colspan=2 rowspan=2 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:16.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>OTRA/OUTRA</span></font></p>
	  </td>
	  <td width=0 height=28 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=135 height=16 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:39.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>ARGENTINA</span></font></p>
	  </td>
	  <td width=21 height=16 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=16 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:3.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>BRASIL</span></font></p>
	  </td>
	  <td width=96 colspan=5 height=16 valign=bottom style='width:72.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:16.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PARAGUAY</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>URUGUAY</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=2 style='height:1.45pt'>
	  <td width=135 height=2 valign=bottom style='width:101.0pt;border-top:none;
	  border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=2 valign=bottom style='width:16.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=2 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=2 valign=bottom style='width:25.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=2 valign=bottom style='width:11.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=2 valign=bottom style='width:79.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=2 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	</table>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	11.1pt;text-autospace:none'><span style='position:relative;z-index:-31'><span
	style='position:absolute;left:450px;top:-192px;width:11px;height:2px'><img
	width=11 height=2
	src="<?= $assets ?>ficha/image010.gif"></span></span><span
	style='position:relative;z-index:-30'><span style='position:absolute;
	left:450px;top:-212px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image011.gif"></span></span><span
	style='position:relative;z-index:-29'><span style='position:absolute;
	left:450px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image012.gif"></span></span><span
	style='position:relative;z-index:-28'><span style='position:absolute;
	left:459px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image013.gif"></span></span><span
	style='position:relative;z-index:-27'><span style='position:absolute;
	left:498px;top:-192px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image010.gif"></span></span><span
	style='position:relative;z-index:-26'><span style='position:absolute;
	left:498px;top:-212px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image011.gif"></span></span><span
	style='position:relative;z-index:-25'><span style='position:absolute;
	left:498px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image012.gif"></span></span><span
	style='position:relative;z-index:-24'><span style='position:absolute;
	left:507px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image013.gif"></span></span><span
	style='position:relative;z-index:-23'><span style='position:absolute;
	left:229px;top:-202px;width:156px;height:2px'><img width=156 height=2
	src="<?= $assets ?>ficha/image014.jpg"></span></span><span
	style='position:relative;z-index:-22'><span style='position:absolute;
	left:86px;top:-125px;width:443px;height:21px'><img width=443 height=21
	src="<?= $assets ?>ficha/image015.jpg"></span></span><span
	style='position:relative;z-index:-21'><span style='position:absolute;
	left:85px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-20'><span style='position:absolute;
	left:85px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-19'><span style='position:absolute;
	left:85px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-18'><span style='position:absolute;
	left:94px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-17'><span style='position:absolute;
	left:181px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-16'><span style='position:absolute;
	left:181px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-15'><span style='position:absolute;
	left:181px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-14'><span style='position:absolute;
	left:190px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-13'><span style='position:absolute;
	left:277px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-12'><span style='position:absolute;
	left:277px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-11'><span style='position:absolute;
	left:286px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-10'><span style='position:absolute;
	left:277px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-9'><span style='position:absolute;left:373px;
	top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-8'><span style='position:absolute;left:373px;
	top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-7'><span style='position:absolute;left:382px;
	top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-6'><span style='position:absolute;left:373px;
	top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:absolute;z-index:-5;margin-left:-1px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image020.gif"></span><span
	style='position:absolute;z-index:-4;margin-left:536px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image021.gif"></span><span
	style='position:absolute;z-index:-3;margin-left:-1px;margin-top:14px;
	width:539px;height:2px'><img width=539 height=2
	src="<?= $assets ?>ficha/image022.gif"></span><span
	style='position:absolute;z-index:-2;margin-left:-1px;margin-top:66px;
	width:539px;height:2px'><img width=539 height=2
	src="<?= $assets ?>ficha/image023.gif"></span><span
	style='position:absolute;z-index:-1;margin-left:363px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image024.gif"></span></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=3 face=Arial><span style='font-size:12.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><b><font size=3 face=Arial><span style='font-size:12.0pt;font-family:
	"Arial","sans-serif";font-weight:bold'>USO OFICIAL</span></font></b></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.3pt;text-autospace:none'><b><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif";font-weight:bold'>&nbsp;</span></font></b></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><b><font size=3 face=Arial><span style='font-size:12.0pt;font-family:
	"Arial","sans-serif";font-weight:bold'>USO OFICIAL</span></font></b></p>

	</div>

	<font size=3 face="Times New Roman"><span style='font-size:12.0pt;font-family:
	"Times New Roman","serif"'><br clear=all style='page-break-before:always'>
	</span></font>

	<div class=WordSection2>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	9.3pt;text-autospace:none'><a name=page2></a><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	</div>

	
	
	
	<!-- 2 VIA -->
	
	<div class=WordSection1 style="margin:2cm;">

	 
	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:181.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'></span></font></p>

	 
	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:177.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>ANEXO </br>MODELO</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:127.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>TARJETA DE ENTRADA/SALIDA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	7.75pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:170.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2era.
	VIA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-42;
	margin-left:-21px;margin-top:15px;width:569px;height:2px'><img width=569
	height=2 src="<?= $assets ?>ficha/image001.gif"></span><span
	style='position:absolute;z-index:-41;margin-left:546px;margin-top:15px;
	width:2px;height:796px'><img width=2 height=796
	src="<?= $assets ?>ficha/image002.gif"></span><span
	style='position:absolute;z-index:-40;margin-left:-21px;margin-top:15px;
	width:2px;height:796px'><img width=2 height=796
	src="<?= $assets ?>ficha/image003.gif"></span><span
	style='position:absolute;z-index:-39;margin-left:-21px;margin-top:809px;
	width:569px;height:2px'><img width=569 height=2
	src="<?= $assets ?>ficha/image001.gif"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	16.5pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:141.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>MERCOSUR/MERCOSUL</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:127.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>TARJETA DE ENTRADA/SALIDA</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:131.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;line-height:99%;font-family:
	"Arial","sans-serif"'>CART&Atilde;O DE ENTRADA/SA&Iacute;DA</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-38;
	margin-left:-1px;margin-top:42px;width:2px;height:40px'><img width=2 height=40
	src="<?= $assets ?>ficha/image004.gif"></span><span
	style='position:absolute;z-index:-37;margin-left:210px;margin-top:42px;
	width:2px;height:40px'><img width=2 height=40
	src="<?= $assets ?>ficha/image004.gif"></span><span
	style='position:absolute;z-index:-36;margin-left:-1px;margin-top:42px;
	width:213px;height:2px'><img width=213 height=2
	src="<?= $assets ?>ficha/image005.gif"></span><span
	style='position:absolute;z-index:-35;margin-left:-1px;margin-top:80px;
	width:213px;height:2px'><img width=213 height=2
	src="<?= $assets ?>ficha/image006.gif"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	12.05pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:1.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:none'><font
	size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>SECUENCIA/SEQUENCIAL</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:relative;z-index:-34'><span
	style='position:absolute;left:190px;top:-5px;width:2px;height:30px'><img
	width=2 height=30
	src="<?= $assets ?>ficha/image007.gif"></span></span><span
	style='position:absolute;z-index:-33;margin-left:-2px;margin-top:53px;
	width:540px;height:97px'><img width=540 height=97
	src="<?= $assets ?>ficha/image008.jpg"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.3pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:31.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>APELLIDO
	Y NOMBRE-/ NOME COMPLETO</span></font></p>

	<?php 
		
		$customer 		= $row->customer;
		$customer_id 	= $row->customer_id;
		
		$cliente 			= $this->site->getCompanyByID($customer_id);
		$data_aniversario	= $cliente->data_aniversario;
		$vat_no				= $cliente->cf1;
 	?>
	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='text-transform: uppercase;font-size:12.0pt;font-family:"Times New Roman","serif"'>
 	<?php for($d=0;$d< strlen( $customer );$d++): 
		$letra = mb_substr( $customer,$d,1,'UTF-8');
		if ($letra == ' ') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'i') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'I') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'j') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		
		if ($letra == 'J') {
			$letra = $letra.'&nbsp;&nbsp;';
		}
		?> 
		<?php  echo '&nbsp;'.$letra; ?> 
	<?php endfor;?>
	</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><span style='position:absolute;z-index:-32;
	margin-left:-1px;margin-top:13px;width:539px;height:153px'><img width=539
	height=153 src="<?= $assets ?>ficha/image009.jpg"></span></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>  </span></font><font
	size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TIPO
	Y NUMERO DE DOCUMENTO<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$vat_no;?> </span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>TIPO
	E NUMERO DE DOCUMENTO</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	18.8pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'> &nbsp;</span></font></p>

	<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
	 style='margin-left:1.0pt;border-collapse:collapse'>
	 <tr height=14 style='height:10.35pt'>
	  <td width=29 height=14 valign=bottom style='width:22.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font>
	 
	  </p>
	  </td>
	  <td width=195 height=14 valign=bottom style='width:146.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>CEDULA
	  DE IDENTIDAD</span></font></p>
	  </td>
	  <td width=41 height=14 valign=bottom style='width:31.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=85 height=14 valign=bottom style='width:64.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>D.N.I.</span></font></p>
	  </td>
	  <td width=56 height=14 valign=bottom style='width:42.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:9.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=104 height=14 valign=bottom style='width:78.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PASAPORTE</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=29 height=16 valign=bottom style='width:22.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=195 height=16 valign=bottom style='width:146.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>CARTEIRA
	  DE IDENTIDADE</span></font></p>
	  </td>
	  <td width=41 height=16 valign=bottom style='width:31.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=85 height=16 valign=bottom style='width:64.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>I.E./I.C.</span></font></p>
	  </td>
	  <td width=56 height=16 valign=bottom style='width:42.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=104 height=16 valign=bottom style='width:78.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:14.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PASSAPORTE</span></font></p>
	  </td>
	 </tr>
	</table>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	10.0pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	12.65pt;text-autospace:none'><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
	 style='margin-left:.5pt;border-collapse:collapse'>
	 <tr height=14 style='height:10.45pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border-top:solid windowtext 1.0pt;border-left:solid windowtext 1.0pt;
	  border-bottom:none;border-right:none;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>FECHA
	  DE NACIMIENTO</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=14 valign=bottom style='width:29.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>DIA</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=14 valign=bottom style='width:43.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>MES</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=14 valign=bottom style='width:69.0pt;
	  border:none;border-top:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>A&Ntilde;O</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;border-top:solid windowtext 1.0pt;
	  border-left:none;border-bottom:none;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border-top:solid windowtext 1.0pt;
	  border-left:none;border-bottom:none;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:17.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>SEXO</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.3pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.3pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:9.1pt;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>DATA
	  DE NASCIMENTO</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=14 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'> 
	  <?php 
	  $dia 	= date('d', strtotime($data_aniversario) );
	  
	  $dia_0 = mb_substr( $dia,0,1,'UTF-8');
	  $dia_1 = mb_substr( $dia,1,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;'.$dia_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$dia_1;
	  
	  ?>
	  </span></font></p>
	  </td>
	  <td width=57 colspan=3 height=14 valign=bottom style='width:40.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>
	  
	  <?php 
	  $mes 	= date('m', strtotime($data_aniversario) );
	  
	  $mes_0 = mb_substr( $mes,0,1,'UTF-8');
	  $mes_1 = mb_substr( $mes,1,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$mes_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$mes_1;
	  
	  ?>
	  
	  </span></font></p>
	  </td>
	  <td width=92 colspan=4 height=14 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:-10.9999pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>
	  
	  <?php 
	  $ano 	= date('Y', strtotime($data_aniversario) );
	  
	  $ano_0 = mb_substr( $ano,2,1,'UTF-8');
	  $ano_1 = mb_substr( $ano,3,1,'UTF-8');
	  
	  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$ano_0.'&nbsp;&nbsp;&nbsp;&nbsp;'.$ano_1;
	  
	  ?>
	  
	  </span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:8.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.3pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=21 style='height:16.1pt'>
	  <td width=135 height=21 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=21 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=21 valign=bottom style='width:55.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=21 valign=bottom style='width:14.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=21 valign=bottom style='width:15.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=21 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=21 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 rowspan=2 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:16.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:7.45pt;margin-bottom:
	  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.
	  &nbsp;&nbsp;&nbsp;&nbsp;M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P</span></font></p>
	  </td>
	  <td width=0 height=21 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:16.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=9 style='height:6.85pt'>
	  <td width=135 height=9 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=9 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=9 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=9 valign=bottom style='width:29.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=9 valign=bottom style='width:43.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=9 valign=bottom style='width:69.0pt;padding:
	  0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=9 valign=bottom style='width:11.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=9 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:6.85pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=8 style='height:5.7pt'>
	  <td width=156 colspan=2 height=8 valign=bottom style='width:117.0pt;
	  border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=8 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=8 valign=bottom style='width:29.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=8 valign=bottom style='width:43.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=8 valign=bottom style='width:69.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=8 valign=bottom style='width:11.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=8 valign=bottom style='width:79.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:4.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=8 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:5.7pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=17 style='height:12.9pt'>
	  <td width=156 colspan=2 height=17 valign=bottom style='width:117.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=17 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=17 valign=bottom style='width:29.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=17 valign=bottom style='width:43.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=17 valign=bottom style='width:69.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=17 valign=bottom style='width:90.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:11.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=17 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.9pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.45pt'>
	  <td width=156 colspan=2 height=14 valign=bottom style='width:117.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>NACIONALIDAD</span></font></p>
	  </td>
	  <td width=73 height=14 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=14 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=156 colspan=2 height=16 valign=bottom style='width:117.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>NACIONALIDADE</span></font></p>
	  </td>
	  <td width=73 rowspan=2 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:19.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><b><font size=5 face=Arial><span style='font-size:20.0pt;font-family:
	  "Arial","sans-serif";font-weight:bold'>X</span></font></b></p>
	  </td>
	  <td width=39 colspan=2 height=16 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=16 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=16 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=27 style='height:19.95pt'>
	  <td width=135 height=27 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font></p>
	  </td>
	  <td width=21 height=27 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
	  text-align:right;line-height:normal;text-autospace:none'><font size=1
	  face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=27 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=27 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=27 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:57.0pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>4.</span></font></p>
	  </td>
	  <td width=120 colspan=2 rowspan=2 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:16.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>OTRA/OUTRA</span></font></p>
	  </td>
	  <td width=0 height=27 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:19.95pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=135 height=16 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:39.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>ARGENTINA</span></font></p>
	  </td>
	  <td width=21 height=16 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=16 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:3.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>BRASIL</span></font></p>
	  </td>
	  <td width=96 colspan=5 height=16 valign=bottom style='width:72.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:16.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PARAGUAY</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>URUGUAY</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=2 style='height:1.45pt'>
	  <td width=229 colspan=3 height=2 valign=bottom style='width:172.0pt;
	  border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=2 valign=bottom style='width:29.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=2 valign=bottom style='width:43.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=2 valign=bottom style='width:69.0pt;border:
	  none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:
	  1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=2 valign=bottom style='width:90.0pt;
	  border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=2 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=8 style='height:6.2pt'>
	  <td width=229 colspan=3 height=8 valign=bottom style='width:172.0pt;
	  border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=8 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=8 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=8 valign=bottom style='width:25.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=8 valign=bottom style='width:11.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=8 valign=bottom style='width:79.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:5.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=8 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:6.2pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=14 style='height:10.45pt'>
	  <td width=229 colspan=3 height=14 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PAIS
	  DE RESIDENCIA</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=14 valign=bottom style='width:14.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=14 valign=bottom style='width:15.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=14 valign=bottom style='width:25.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=14 valign=bottom style='width:11.0pt;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=14 valign=bottom style='width:79.0pt;border:none;
	  border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:9.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=14 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:10.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=15 style='height:11.25pt'>
	  <td width=229 colspan=3 height=15 valign=bottom style='width:172.0pt;
	  border:none;border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PAIS
	  DE RESIDENCIA</span></font></p>
	  </td>
	  <td width=39 colspan=2 height=15 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=15 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=15 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=120 colspan=2 height=15 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=15 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:11.25pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=28 style='height:21.1pt'>
	  <td width=135 height=28 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>1.</span></font></p>
	  </td>
	  <td width=21 height=28 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
	  text-align:right;line-height:normal;text-autospace:none'><font size=1
	  face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>2.</span></font></p>
	  </td>
	  <td width=73 height=28 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:19.0pt;margin-bottom:.0001pt;line-height:21.1pt;text-autospace:
	  none'><b><font size=5 face=Arial><span style='font-size:20.0pt;font-family:
	  "Arial","sans-serif";font-weight:bold'>X</span></font></b></p>
	  </td>
	  <td width=39 colspan=2 height=28 valign=bottom style='width:29.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:6.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>3.</span></font></p>
	  </td>
	  <td width=57 colspan=3 height=28 valign=bottom style='width:43.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=3 face="Times New Roman"><span
	  style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=28 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:57.0pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>4.</span></font></p>
	  </td>
	  <td width=120 colspan=2 rowspan=2 valign=bottom style='width:90.0pt;
	  border:none;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal align=right style='margin-top:0cm;margin-right:16.45pt;
	  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
	  line-height:normal;text-autospace:none'><font size=1 face=Arial><span
	  style='font-size:9.0pt;font-family:"Arial","sans-serif"'>OTRA/OUTRA</span></font></p>
	  </td>
	  <td width=0 height=28 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:21.1pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=16 style='height:12.35pt'>
	  <td width=135 height=16 valign=bottom style='width:101.0pt;border:none;
	  border-left:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:39.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>ARGENTINA</span></font></p>
	  </td>
	  <td width=21 height=16 valign=bottom style='width:16.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=2 face="Times New Roman"><span
	  style='font-size:10.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=16 valign=bottom style='width:55.0pt;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:3.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>BRASIL</span></font></p>
	  </td>
	  <td width=96 colspan=5 height=16 valign=bottom style='width:72.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:16.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>PARAGUAY</span></font></p>
	  </td>
	  <td width=92 colspan=4 height=16 valign=bottom style='width:69.0pt;
	  padding:0cm 0cm 0cm 0cm;height:12.35pt'>
	  <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	  margin-left:15.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	  none'><font size=1 face=Arial><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>URUGUAY</span></font></p>
	  </td>
	  <td width=0 height=16 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:12.35pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	 <tr height=2 style='height:1.45pt'>
	  <td width=135 height=2 valign=bottom style='width:101.0pt;border-top:none;
	  border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
	  border-right:none;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=21 height=2 valign=bottom style='width:16.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=73 height=2 valign=bottom style='width:55.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=19 height=2 valign=bottom style='width:14.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=20 height=2 valign=bottom style='width:15.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=33 height=2 valign=bottom style='width:25.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=15 height=2 valign=bottom style='width:11.0pt;border:none;
	  border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=105 height=2 valign=bottom style='width:79.0pt;border-top:none;
	  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
	  padding:0cm 0cm 0cm 0cm;height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	  <td width=0 height=2 valign=bottom style='width:0cm;padding:0cm 0cm 0cm 0cm;
	  height:1.45pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	  normal;text-autospace:none'><font size=1 face="Times New Roman"><span
	  style='font-size:1.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>
	  </td>
	 </tr>
	</table>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	11.1pt;text-autospace:none'><span style='position:relative;z-index:-31'><span
	style='position:absolute;left:450px;top:-192px;width:11px;height:2px'><img
	width=11 height=2
	src="<?= $assets ?>ficha/image010.gif"></span></span><span
	style='position:relative;z-index:-30'><span style='position:absolute;
	left:450px;top:-212px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image011.gif"></span></span><span
	style='position:relative;z-index:-29'><span style='position:absolute;
	left:450px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image012.gif"></span></span><span
	style='position:relative;z-index:-28'><span style='position:absolute;
	left:459px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image013.gif"></span></span><span
	style='position:relative;z-index:-27'><span style='position:absolute;
	left:498px;top:-192px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image010.gif"></span></span><span
	style='position:relative;z-index:-26'><span style='position:absolute;
	left:498px;top:-212px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image011.gif"></span></span><span
	style='position:relative;z-index:-25'><span style='position:absolute;
	left:498px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image012.gif"></span></span><span
	style='position:relative;z-index:-24'><span style='position:absolute;
	left:507px;top:-212px;width:2px;height:22px'><img width=2 height=22
	src="<?= $assets ?>ficha/image013.gif"></span></span><span
	style='position:relative;z-index:-23'><span style='position:absolute;
	left:229px;top:-202px;width:156px;height:2px'><img width=156 height=2
	src="<?= $assets ?>ficha/image014.jpg"></span></span><span
	style='position:relative;z-index:-22'><span style='position:absolute;
	left:86px;top:-125px;width:443px;height:21px'><img width=443 height=21
	src="<?= $assets ?>ficha/image015.jpg"></span></span><span
	style='position:relative;z-index:-21'><span style='position:absolute;
	left:85px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-20'><span style='position:absolute;
	left:85px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-19'><span style='position:absolute;
	left:85px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-18'><span style='position:absolute;
	left:94px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-17'><span style='position:absolute;
	left:181px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-16'><span style='position:absolute;
	left:181px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-15'><span style='position:absolute;
	left:181px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-14'><span style='position:absolute;
	left:190px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-13'><span style='position:absolute;
	left:277px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-12'><span style='position:absolute;
	left:277px;top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-11'><span style='position:absolute;
	left:286px;top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-10'><span style='position:absolute;
	left:277px;top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:relative;z-index:-9'><span style='position:absolute;left:373px;
	top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image018.gif"></span></span><span
	style='position:relative;z-index:-8'><span style='position:absolute;left:373px;
	top:-20px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image016.gif"></span></span><span
	style='position:relative;z-index:-7'><span style='position:absolute;left:382px;
	top:-39px;width:2px;height:21px'><img width=2 height=21
	src="<?= $assets ?>ficha/image019.gif"></span></span><span
	style='position:relative;z-index:-6'><span style='position:absolute;left:373px;
	top:-39px;width:11px;height:2px'><img width=11 height=2
	src="<?= $assets ?>ficha/image017.gif"></span></span><span
	style='position:absolute;z-index:-5;margin-left:-1px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image020.gif"></span><span
	style='position:absolute;z-index:-4;margin-left:536px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image021.gif"></span><span
	style='position:absolute;z-index:-3;margin-left:-1px;margin-top:14px;
	width:539px;height:2px'><img width=539 height=2
	src="<?= $assets ?>ficha/image022.gif"></span><span
	style='position:absolute;z-index:-2;margin-left:-1px;margin-top:66px;
	width:539px;height:2px'><img width=539 height=2
	src="<?= $assets ?>ficha/image023.gif"></span><span
	style='position:absolute;z-index:-1;margin-left:363px;margin-top:14px;
	width:2px;height:54px'><img width=2 height=54
	src="<?= $assets ?>ficha/image024.gif"></span></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><font size=3 face=Arial><span style='font-size:12.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></font></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><b><font size=3 face=Arial><span style='font-size:12.0pt;font-family:
	"Arial","sans-serif";font-weight:bold'>USO OFICIAL</span></font></b></p>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	2.3pt;text-autospace:none'><b><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif";font-weight:bold'>&nbsp;</span></font></b></p>

	<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
	margin-left:37.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
	none'><b><font size=3 face=Arial><span style='font-size:12.0pt;font-family:
	"Arial","sans-serif";font-weight:bold'>USO OFICIAL</span></font></b></p>

	</div>

	<font size=3 face="Times New Roman"><span style='font-size:12.0pt;font-family:
	"Times New Roman","serif"'><br clear=all style='page-break-before:always'>
	</span></font>

	<div class=WordSection2>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	9.3pt;text-autospace:none'><a name=page2></a><font size=3 face="Times New Roman"><span
	style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></font></p>

	</div>
<?php } ?>
</body>

<script>
	window.print();
</script>
</html>
