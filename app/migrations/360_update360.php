<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update360 extends CI_Migration {

    public function up() {

        $this->db->update('settings',
            array(
                'version' => '3.6.0',
                'smtp_host' => 'mail.sagtur.com.br',
                'smtp_user' => 'voucher@suareservaonline.com.br',
                'default_email' => 'voucher@suareservaonline.com.br'
            ), array('setting_id' => 1));
    }

    public function down() {}
}
