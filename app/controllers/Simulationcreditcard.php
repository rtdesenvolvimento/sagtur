<?php defined('BASEPATH') or exit('No direct script access allowed');

class Simulationcreditcard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('settings_model');
    }

    public function simulation() {
        $this->load->view($this->theme . 'simulation/index', $this->data);
    }

    public function mercadopago() {
        $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();

        $cartao_credito = $this->getTipoCobrancaCartaoCredito('mercadopago');

        if ($cartao_credito) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($cartao_credito->id);
            $this->data['taxaIntermediacao'] = $taxas->taxaIntermediacao;
        }

        $this->load->view($this->theme . 'simulation/mercadopago', $this->data);
    }

    public function pagseguro() {
        $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();

        $cartao_credito = $this->getTipoCobrancaCartaoCredito('pagseguro');

        if ($cartao_credito) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($cartao_credito->id);
            $this->data['taxaIntermediacao'] = $taxas->taxaIntermediacao;
        }

        $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();

        $this->load->view($this->theme . 'simulation/pagseguro', $this->data);
    }

    public function getTipoCobrancaCartaoCredito($integracao)
    {
        $this->db->where('integracao', $integracao);

        if ($integracao == 'mercadopago') {
            $this->db->where('(tipo = "carne_cartao_transparent_mercado_pago" OR tipo = "link_pagamento")');
        } else if($integracao == 'pagseguro') {
            $this->db->where('(tipo = "carne_cartao_transparent" OR tipo = "carne_cartao" OR tipo = "boleto" )');
        }

        $this->db->limit(1);

        $q = $this->db->get("tipo_cobranca");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function simulation_installments() {

        $valePaySettings = $this->settings_model->getValepaySettings();

        if ($valePaySettings->active) {

            $valepay              = new Valepay();
            $valepay->sandbox     = $valePaySettings->sandbox;
            $valepay->key_public  = $valePaySettings->public_key;
            $valepay->key_private = $valePaySettings->private_key;

            $valor_total_taxas = $this->input->get('valor_total_taxas');

            $emmiter = array(
                'amount'            => $valor_total_taxas,
                'markup_type'       => '1',//1 - Multiplo 2-Divisor
                'markup'            => '0',//Porcentagem do markup
                'calculate_markup'  => FALSE,
                'entry_money'       => 0,//valor da entrada
            );

            $this->sma->send_json($valepay->simulation_installments_by_customer($emmiter));

        } else {
            $this->sma->send_json([]);
        }
    }

    public function simulation_agencia_installments() {

        $valePaySettings = $this->settings_model->getValepaySettings();

        if ($valePaySettings->active) {

            $valepay              = new Valepay();
            $valepay->sandbox     = $valePaySettings->sandbox;
            $valepay->key_public  = $valePaySettings->public_key;
            $valepay->key_private = $valePaySettings->private_key;

            $valor_total_taxas = $this->input->get('valor_total_taxas');

            $emmiter = array(
                'amount'            => $valor_total_taxas,
                'markup_type'       => '1',//1 - Multiplo 2-Divisor
                'markup'            => '0',//Porcentagem do markup
                'calculate_markup'  => FALSE,
                'entry_money'       => 0,//valor da entrada
                'product_id'        => 3,
                'installments'      => 12,
            );

            $this->sma->send_json($valepay->simulation_rav($emmiter));

        } else {
            $this->sma->send_json([]);
        }
    }
}