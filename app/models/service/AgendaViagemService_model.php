<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaViagemService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model("repository/ProdutoRepository_model", 'ProdutoRepository_model');

        $this->load->model('model/AgendaViagem_model', 'AgendaViagem_model');
        $this->load->model('model/Quarto_model', 'Quarto_model');
    }

    public function saveAll($programacoes, $produtoId) {
        foreach ($programacoes as $agendaDTO) {
            $agendaDTO->produto = $produtoId;
            $this->salvar($agendaDTO);
        }
    }

    public function salvar($agendaDTO) {
        //$agendaDTO = new AgendaViagemDTO_model();
        try {

            $this->__iniciaTransacao();

            $agenda = $this->toModel($agendaDTO);

            $agendamento = $this->__save('agenda_viagem', $agenda->__toArray());

            $this->adicionarNovoEventoNoCalendario($agendaDTO, $agendamento);

            $this->__confirmaTransacao();

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function editar($agendaDTO, $id) {
        try {

            $this->__iniciaTransacao();

            $agenda = $this->toModel($agendaDTO);

            $this->__delete('calendar', 'programacaoId', $id);
            $this->__update('agenda_viagem', $agenda->__toArray(), 'id', $id);

            $this->adicionarNovoEventoNoCalendario($agendaDTO, $id);

            $this->__confirmaTransacao();
        } catch(Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function editar_cupom_desconto($agendaDTO, $id) {
        try {

            $this->__iniciaTransacao();

            $this->__delete('cupom_desconto_item', 'programacao_id', $id);

            foreach ($agendaDTO->cupons as $cupom) {
                $cupom->setProgramacaoId($id);
                $cupom->setProduto($agendaDTO->produto);

                $this->__save('cupom_desconto_item', $cupom->__toArray());
            }

            $this->__confirmaTransacao();
        } catch(Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function toModel($agendaDTO) {

        //$agendaDTO = new AgendaViagemDTO_model();

        $agenda = new AgendaViagem_model();

        $agenda->setDataSaida($agendaDTO->doDia);
        $agenda->setHoraSaida($agendaDTO->horaSaida);
        $agenda->setDataRetorno($agendaDTO->aoDia);
        $agenda->setHoraRetorno($agendaDTO->horaRetorno);
        $agenda->setVagas($agendaDTO->vagas);
        $agenda->setProduto($agendaDTO->produto);
        $agenda->setHabilitarMarcacaoAreaCliente($agendaDTO->habilitar_marcacao_area_cliente);
        $agenda->setPermitirMarcacaoDependente($agendaDTO->permitir_marcacao_dependente);
        $agenda->setDataInicioMarcacao($agendaDTO->data_inicio_marcacao);
        $agenda->setDataFinalMarcacao($agendaDTO->data_final_marcacao);
        $agenda->setAgendaProgramacaoId($agendaDTO->agenda_programacao_id);

        $agenda->setActive($agendaDTO->active);
        $agenda->setPreco($agendaDTO->preco);
        $agenda->setSinalMinimoPagamento(0);
        $agenda->setPercentualMaximoDesconto(0);

        unset($agenda->produtoModel);

        return $agenda;
    }

    public function excluir($id) {
        try {
            $this->__iniciaTransacao();

            $agenda = $this->getProgramacaoById($id);
            //$agenda = new AgendaViagem_model();

            if ($this->isPossuiVendas($agenda)) throw new Exception("Não é possível realizar a exclusão deste lançamento pois já possui vendas relacionadas.");

            $this->__delete('agenda_viagem', 'id', $id);
            $this->__delete('calendar', 'programacaoId', $id);

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function adicionarNovoEventoNoCalendario($agendaDTO, $id) {

        $produto =  $this->site->getProductByID($agendaDTO->produto);

        $data_event = array(
            'title' 		=> $produto->name,
            'programacaoId' => $id,
            'start'	 		=> $agendaDTO->doDia.' '.$agendaDTO->horaSaida,
            'end' 			=> $agendaDTO->doDia.' '.$agendaDTO->horaSaida,
            'description' 	=> $produto->name,
            'color' 		=>  '#1d7010',//TODO COR DOS DESTINOS VIR DA CATEGORIA
            'user_id' 	    => $this->session->userdata('user_id')
        );
        $this->db->insert('calendar', $data_event);
    }

    public function getAllProgramacao($filter = NULL) {

        $programacoes = $this->AgendaViagemRespository_model->getAllDisponibilidade($filter);

        $configuracaoGeral = $this->site->get_setting();

        $ids = '';
        foreach ($programacoes as $programacao) $ids.= $programacao->programacaoId.',';

        $allVendas =  $this->AgendaViagemRespository_model->getTotalVendaAllByProgramacoes($ids.'0');

        foreach ($programacoes as $programacao) {

            $agenda = $this->__toModel($programacao, new AgendaViagem_model());

            $agenda->setProduto($agenda->produtoId);
            $agenda->setControleEstoqueHospedagem(($agenda->controle_estoque_hospedagem != null ? $agenda->controle_estoque_hospedagem : FALSE));
            $agenda->setQuartos($this->preencher_quartos($agenda));

            $totalVendasFaturadas   = 0;
            $totalOrcamentos        = 0;
            $totalListaEspera       = 0;
            $totalCancelados        = 0;


            if (is_array($allVendas)) {
                foreach ($allVendas as $venda) {
                    if (($programacao->programacaoId == $venda->programacaoId) && ($programacao->produtoId == $venda->produto)) {

                        if ($venda->status == 'faturada') $totalVendasFaturadas = $totalVendasFaturadas + $venda->quantity;
                        if ($venda->status == 'orcamento') $totalOrcamentos = $totalOrcamentos + $venda->quantity;
                        if ($venda->status == 'lista_espera') $totalListaEspera = $totalListaEspera + $venda->quantity;
                        if ($venda->status == 'cancel') $totalCancelados = $totalCancelados + $venda->quantity;

                        $tipoHospedagem = $venda->tipoHospedagem;
                        $programacaoId  = $programacao->programacaoId;

                        $agenda->getQuartos()[$tipoHospedagem] = $this->atualiza_totalizador_controle_estoque_quarto($agenda, $venda, $programacaoId, $tipoHospedagem);
                    }
                }
            }

            $agenda = $this->atualiza_estoque($agenda, $totalVendasFaturadas, $totalOrcamentos, $totalListaEspera);

            if ($configuracaoGeral->ocultarProdutosSemEstoqueLoja && $filter->verificarConfiguracaoOcultarProdutosSemEstoque) {
                if ($agenda->getTotalDisponvel() > 0) {
                    $data[] = $agenda;
                }
            } else {
                $data[] = $agenda;
            }
        }

        return $data;
    }

    public function getAllProgramacaoDestaque($filter = NULL)
    {
        $filter->destaque = true;
        $filter->limit_by_data = false;
        $filter->limit_by_category = false;
        return $this->getAllProgramacao($filter);
    }

    public function getProgramacao($id) {
        $datasAgendadas = $this->AgendaViagemRespository_model->getProgramacao($id);

        foreach ($datasAgendadas as $dataAgendada) {
            $agenda = $this->__toModel($dataAgendada, new AgendaViagem_model());
            $data[] = $this->preencherProgramacao($agenda);
        }

        return $data;
    }

    public function getProgramacaoById($id) {
        try {
            return $this->preencherProgramacao($this->buscarOuFalhar($id));
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    public function vagas($programacaoId, $controle_estoque_hospedagem = FALSE, $tipo_hospedagem = NULL) {

        $agenda = $this->preencherProgramacao($this->buscarOuFalhar($programacaoId));

        if ($controle_estoque_hospedagem) {
            foreach ($agenda->getQuartos() as $quarto) {
                if ($quarto->tipo_hospedagem == $tipo_hospedagem) {
                    return $quarto->estoque_atual;
                }
            }
        }
        return $agenda->getTotalDisponvel();
    }

    public function getControleDeHospedagem($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.controle_estoque_hospedagem');

        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->controle_estoque_hospedagem;
        }
        return FALSE;
    }

    private function preencherProgramacao($agenda) {

        $allVendas =  $this->AgendaViagemRespository_model->getTotalVendaAllByProgramacoes($agenda->id);

        $totalVendasFaturadas   = 0;
        $totalOrcamentos        = 0;
        $totalListaEspera       = 0;
        $totalCancelados        = 0;

        $agenda->setQuartos($this->preencher_quartos($agenda));
        $agenda->setControleEstoqueHospedagem( $this->getControleDeHospedagem($agenda->produto));

        if (is_array($allVendas)) {
            foreach ($allVendas as $venda) {

                if ($venda->produto == $agenda->produto) {

                    $agenda->setControleEstoqueHospedagem(($venda->controle_estoque_hospedagem != null ? $venda->controle_estoque_hospedagem : FALSE));

                    if ($venda->status == 'faturada') {
                        $totalVendasFaturadas = $totalVendasFaturadas + $venda->quantity;
                    }

                    if ($venda->status == 'orcamento') {
                        $totalOrcamentos = $totalOrcamentos + $venda->quantity;
                    }

                    if ($venda->status == 'lista_espera') {
                        $totalListaEspera = $totalListaEspera + $venda->quantity;
                    }

                    if ($venda->status == 'cancel') {
                        $totalCancelados = $totalCancelados + $venda->quantity;
                    }

                    $tipoHospedagem = $venda->tipoHospedagem;
                    $programacaoId = $venda->programacaoId;

                    $agenda->getQuartos()[$tipoHospedagem] = $this->atualiza_totalizador_controle_estoque_quarto($agenda, $venda, $programacaoId, $tipoHospedagem);
                }
            }
        }

        return $this->atualiza_estoque($agenda, $totalVendasFaturadas, $totalOrcamentos, $totalListaEspera);
    }


    public function atualiza_estoque($agenda, $totalVendasFaturadas, $totalOrcamentos, $totalListaEspera) {

        $this->atualizar_estoque_tipo_quarto($agenda);

        $agenda->setTotalVendasFaturas( (int) $totalVendasFaturadas);
        $agenda->setTotalOrcamento($totalOrcamentos);
        $agenda->setTotalListaEspera( (int) $totalListaEspera);
        $agenda->setTotalVendasPendentes(0);


        if ($agenda->getControleEstoqueHospedagem()) {

            $totalEstoqueDisponivel = 0;

            foreach ($agenda->getQuartos() as $quarto) {
                $totalEstoqueDisponivel += $quarto->estoque_atual;
            }

            $agenda->estoque = $totalEstoqueDisponivel;

            $agenda->setTotalDisponvel($totalEstoqueDisponivel);
        } else {
            $totalVendasDisponivel = $agenda->getVagas() - ($totalVendasFaturadas + $totalOrcamentos);

            $agenda->estoque = $totalVendasDisponivel;

            $agenda->setTotalDisponvel($totalVendasDisponivel);
        }

        return $agenda;
    }

    private function atualiza_totalizador_controle_estoque_quarto($programacaoEstoque, $venda, $programcao_id, $tipoHospedagem) {
        if ($programcao_id && $tipoHospedagem) {
            $quarto = array_filter($programacaoEstoque->getQuartos(), function ($obj) use ($programcao_id, $tipoHospedagem) {
                return ($obj->tipo_hospedagem == $tipoHospedagem) && ($obj->programcao_id = $programcao_id);
            });

            if ($quarto != null) {
                if ($venda->status == 'faturada') $quarto[$tipoHospedagem]->total_faturadas         = $quarto[$tipoHospedagem]->total_faturadas + $venda->quantity;
                if ($venda->status == 'orcamento') $quarto[$tipoHospedagem]->total_orcamentos       = $quarto[$tipoHospedagem]->total_orcamentos + $venda->quantity;
                if ($venda->status == 'lista_espera') $quarto[$tipoHospedagem]->total_lista_espera  = $quarto[$tipoHospedagem]->total_lista_espera + $venda->quantity;
                if ($venda->status == 'cancel') $quarto[$tipoHospedagem]->total_canceladas          = $quarto[$tipoHospedagem]->total_canceladas + $venda->quantity;
            }
        }

        return $quarto[$tipoHospedagem];
    }

    function atualizar_estoque_tipo_quarto($agenda) {

        foreach ($agenda->getQuartos() as $quarto) {

            $estoqueFaturada    = $quarto->total_faturadas;
            $estoqueOrcamento   = $quarto->total_orcamentos;
            $vendasRealizadas   = ($estoqueFaturada + $estoqueOrcamento);
            $estoque_atual      = $quarto->estoque_total -  $vendasRealizadas;

            if ($estoque_atual > 0) {//TODO NAO CONSIDERA QUANDO O TIPO ESTA DESABILITADO
                $agenda->getQuartos()[$quarto->tipo_hospedagem]->estoque_atual          = $estoque_atual;

                $estoqueAtualQuartos = ($quarto->estoque_acomodacao - ($vendasRealizadas/$quarto->acomoda));
                $fraction = fmod($estoqueAtualQuartos, 1);

                if ($fraction > 0) {
                    $agenda->getQuartos()[$quarto->tipo_hospedagem]->qtd_quartos_disponivel = ((int) $estoqueAtualQuartos + 1);
                } else {
                    $agenda->getQuartos()[$quarto->tipo_hospedagem]->qtd_quartos_disponivel = ($quarto->estoque_acomodacao - ($vendasRealizadas/$quarto->acomoda));
                }
            }
        }

        return $agenda;
    }

    private function preencher_quartos($agenda) {
        $quartos = $this->ProdutoRepository_model->getTipoHospedagemRodoviario($agenda->produto);

        if (!empty($quartos) && is_array($quartos)) {
            foreach ($quartos as $quarto) {
                $quartoModel = new Quarto_model();
                $quartoModel->tipo_hospedagem = $quarto->id;
                $quartoModel->name = $quarto->name;
                $quartoModel->acomoda = (int)$quarto->acomoda;

                if ($quarto->status == 'ATIVO') {//TODO CONTABILIZA APENAS OS TIPOS HABILITADOS
                    $quartoModel->estoque_acomodacao = (int)$quarto->estoque;
                } else {
                    $quartoModel->estoque_acomodacao = 0;
                }

                $quartoModel->estoque_total = ($quartoModel->estoque_acomodacao * $quartoModel->acomoda);
                $quartoModel->programcao_id = $agenda->id;
                $agenda->adicionarQuarto($quartoModel, $quarto->id);
            }
        }

        return $agenda->getQuartos();
    }

    function buscarOuFalhar($id) {

        if(!$this->__exist('agenda_viagem', $id)) throw new Exception('Agenda não encontrada!');

        return $this->__getByIdToModel('agenda_viagem', $id, new AgendaViagem_model());
    }

    public function isPossuiVendas($agenda) {
        return
            $agenda->getTotalOrcamento() > 0 ||
            $agenda->getTotalVendasFaturas() > 0 ||
            $agenda->getTotalListaEspera() > 0 ||
            $agenda->getTotalVendasPendentes() > 0;
    }
}