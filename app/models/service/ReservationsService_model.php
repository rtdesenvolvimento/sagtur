<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReservationsService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function montar_os($itemid, $localEmbarqueId = NULL, $guia_id = NULL, $motorista_id = NULL, $tipo_transporte_id = NULL) {

        if ($localEmbarqueId) {
            $this->db->update('sale_items', array('localEmbarque' => $localEmbarqueId), array('id' => $itemid));
        }

        if ($guia_id) {
            $this->db->update('sale_items', array('guia' => $guia_id), array('id' => $itemid));
        }

        if ($motorista_id) {
            $this->db->update('sale_items', array('motorista' => $motorista_id), array('id' => $itemid));
        }

        if ($tipo_transporte_id) {
            $this->db->update('sale_items', array('tipoTransporte' => $tipo_transporte_id), array('id' => $itemid));
        }

        return true;
    }
}