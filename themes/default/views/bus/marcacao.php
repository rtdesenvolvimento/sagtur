<?php
$assentosOcupados = $this->products_model->getSaleByPoltronaByItemVendaForMarcacao($tipoTransporte->id, $programacaoId);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Marcaçao de Assentos</title>

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

    <link href="<?= $assets ?>styles/bus/css/bus-map-min-v1.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link href="<?= $assets ?>styles/jquery-ui.css" rel="stylesheet"/>

    <style>

        body{
            margin: 0;
        }
        .change-floors-button {
            height: 18px;
            width: 18px;
            cursor: pointer;
        }

        .busMapAppContainer {
            align-items: center;
            display: flex;
            flex-direction: column;
            margin: 1rem auto 0;
            max-width: 665px;
            width: 100%;
        }

        .top-parent-container {
            padding: 3rem 0 2rem;
        }

        @media (max-width: 680px) {
            .seats-subtitles {
                /* flex-direction: column; */
                margin: 0;
                position: absolute;
                right: 120px;
                top: -15px;
            }
        }

        .box {
            position: fixed;
            top: 0;
            left: 0;
            right: inherit;
            background-color: #f0f0f0;
            padding: 0px;
            color: #34383c;
            border-bottom: 1px solid #dbdee0;
            border-right: 1px solid #dbdee0;
            z-index: 999; /* Certifique-se de que o z-index seja maior que o conteúdo da página para que a div fique acima */
        }

        .box i {
            color: #428bca;
            font-size: 18px;
        }

        .box_div {
            padding: 5px 0px 0px 5px;
            font-size: 25px;
            color: #428bca;
        }

        <?php if ($_GET['inverter']) { ?>
            .scaffold-bus {
                left:50%;
                margin: 15rem 0 0;
                position: absolute;
                transform: translate(-50%) rotate(90deg)
            }

            .bus-seats .seatNumberLabel {
                transform: rotate(-90deg)
            }

            .seat-map {
                height: 680px;
                margin: 0 auto 1rem;
                position: relative;
                width: 100%
            }

            .seats-subtitles {
                /* flex-direction: column; */
                margin: 0;
                position: absolute;
                right: 80px;
                top: -15px;
            }
        <?php } ?>
    </style>

</head>
<body onload="document.body.style.zoom = '<?=$_GET['zoom']?>%'">
    <div id="sticker" style="cursor: all-scroll">
        <div class="box">
            <div class="box_div">
                <i class="fa fa-search-plus" style="cursor: pointer;" onclick="reduceZoomPlus()"></i>
                <i class="fa fa-search-minus" style="cursor: pointer;margin-right: 5px;" onclick="reduceZoomMinus();"></i>
                <i class="fa fa-refresh" style="cursor: pointer;margin-right: 5px;" onclick="recarregar();"></i>
                <?php if ($_GET['inverter']) { ?>
                    <i class="fa fa-exchange" style="cursor: pointer;margin-right: 5px;" onclick="reverter();"></i>
                <?php } else {?>
                    <i class="fa fa-exchange" style="cursor: pointer;margin-right: 5px;" onclick="inverter();"></i>
                <?php } ?>
            </div>
        </div>
        <?php if(!empty($assentos) && !empty($assentos2)) {?>
            <div class="from-group" style="text-align: center;margin-top: 14px;">
                <p><b>Alterne entre os andares:</b></p>
                <span style="margin-right: 50px;">
                <input class="change-floors-button" type="radio" onclick="habilitar(this);" name="floors-number" value="1" id="one-floor-radio" checked="checked">
                <label for="one-floor-radio">1º Andar</label>
            </span>
                <span>
                <input class="change-floors-button" type="radio" onclick="habilitar(this);" name="floors-number" value="2" id="two-floor-radio">
                <label for="two-floor-radio">2º Andar</label>
            </span>
            </div>
        <?php } ?>
        <div class="top-parent-container">
            <div class="busMapAppContainer">
                <div class="busMapApp">
                    <div class="scaffold-bus-container bus-map">
                        <div class="seat-map">
                            <div class="scaffold-bus">
                                <ul class="bus-seats superior-floor active">

                                    <?php if(!empty($assentos)) {?>
                                        <?php foreach ($assentos as $assento) {
                                            $ocupado = 'enabled';
                                            $nome_passageiro = '';
                                            $bloqueado_id = false;
                                            $venda = false;

                                            foreach ($assentosOcupados as $ocup) {
                                                if ($ocup->poltrona == $assento->name) {
                                                    $ocupado = 'disabled';
                                                    $nome_passageiro = $ocup->customer;
                                                    $venda = $ocup->id;
                                                }
                                            }

                                            foreach ($bloqueados as $bloqueado) {
                                                if ($bloqueado->str_assento == $assento->name) {
                                                    if ($bloqueado->cart_lock) {
                                                        $ocupado = 'reservado-bloqueio';
                                                    } else {
                                                        $ocupado = 'reservado';
                                                    }
                                                    $nome_passageiro = $bloqueado->note;
                                                    $bloqueado_id = $bloqueado->id;
                                                }
                                            }

                                            ?>
                                            <li style="<?php if (!$assento->habilitado) echo 'display:none';?>"
                                                onclick="bloquie_assento(this);"
                                                bloqueio="<?=$bloqueado_id;?>"
                                                nome_passageiro="<?=$nome_passageiro;?>"
                                                venda="<?=$venda;?>"
                                                andar="<?=$assento->andar;?>"
                                                habilitado="<?=$assento->habilitado;?>"
                                                data-bs-toggle="modal"
                                                data-bs-target="#axisMapModal"
                                                class="seat andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> seat-<?=$ocupado;?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
                                                data-seat="<?=$assento->assento;?>"
                                                data-status="<?=$ocupado;?>"
                                                data-position-order="<?=$assento->ordem;?>"
                                                data-position-order_name="<?=$assento->name;?>">
                                                <span class="seatNumberLabel" title="<?=$nome_passageiro?>"><?=$assento->name;?></span>
                                            </li>
                                        <?php }?>
                                    <?php } ?>

                                    <?php if(!empty($assentos2)) {?>
                                        <?php foreach ($assentos2 as $assento) {
                                            $ocupado = 'enabled';
                                            $nome_passageiro = '';
                                            $bloqueado_id = false;
                                            $venda = false;

                                            foreach ($assentosOcupados as $ocup) {
                                                if ($ocup->poltrona == $assento->name) {
                                                    $ocupado = 'disabled';
                                                    $nome_passageiro = $ocup->customer;
                                                    $venda = $ocup->id;
                                                }
                                            }

                                            foreach ($bloqueados as $bloqueado) {
                                                if ($bloqueado->str_assento == $assento->name) {
                                                    if ($bloqueado->cart_lock) {
                                                        $ocupado = 'reservado-bloqueio';
                                                    } else {
                                                        $ocupado = 'reservado';
                                                    }
                                                    $nome_passageiro = $bloqueado->note;
                                                    $bloqueado_id = $bloqueado->id;
                                                }
                                            } ?>
                                            <li style="display:none"
                                                onclick="bloquie_assento(this);"
                                                bloqueio="<?=$bloqueado_id;?>"
                                                nome_passageiro="<?=$nome_passageiro;?>"
                                                venda="<?=$venda;?>"
                                                andar="<?=$assento->andar;?>"
                                                habilitado="<?=$assento->habilitado;?>"
                                                data-bs-toggle="modal"
                                                data-bs-target="#axisMapModal"
                                                class="seat andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> seat-<?=$ocupado;?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
                                                data-seat="<?=$assento->assento;?>"
                                                data-status="<?=$ocupado;?>"
                                                data-position-order="<?=$assento->ordem;?>"
                                                data-position-order_name="<?=$assento->name;?>">
                                                <span class="seatNumberLabel" title="<?=$nome_passageiro?>"><?=$assento->name;?></span>
                                            </li>
                                        <?php }?>
                                    <?php } ?>
                                </ul>
                            </div>
                            <ul class="seats-subtitles">
                                <li>
                                    <span class="seat-icon seat-enabled"></span>
                                    <span class="subtitle">Livre</span>
                                </li>
                                <li>
                                    <span class="seat-icon seat-reservado-bloqueio"></span>
                                    <span class="subtitle">Reservado</span>
                                </li>
                                <li>
                                    <span class="seat-icon seat-reservado"></span>
                                    <span class="subtitle">Bloqueado</span>
                                </li>
                                <li>
                                    <span class="seat-icon seat-disabled"></span>
                                    <span class="subtitle">Ocupado</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.js"></script>

<script>

    var site = <?=json_encode(array('base_url' => base_url(), 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
    let zoom = 100;

    $(document).ready(function(){
        $('#sticker').draggable();

        <?php if ($_GET['andar'] == 1) {?>
            $('#one-floor-radio').prop('checked', true);
            $('#two-floor-radio').prop('checked', false);
            habilitar_andar(1)
        <?php } else if ($_GET['andar'] == 2) {?>
            $('#one-floor-radio').prop('checked', false);
            $('#two-floor-radio').prop('checked', true);
            habilitar_andar(2)
        <?php } ?>
    });

    function getAndar() {
        let primeiroAndar = $('#one-floor-radio').prop('checked');

        if (primeiroAndar) {
            if (primeiroAndar) {
                return 1;
            } else {
                return 2;
            }
        }

        return 1;
    }

    function recarregar() {

        let andar = getAndar();

        <?php if ($_GET['inverter']) { ?>
            location.href = '<?php echo base_url()?>products/bus/<?=$product->id;?>/<?=$tipoTransporte->id;?>/<?=$programacaoId;?>?inverter=true&zoom=' + zoom+'&andar=' + andar;
        <?php } else { ?>
            location.href = '<?php echo base_url()?>products/bus/<?=$product->id;?>/<?=$tipoTransporte->id;?>/<?=$programacaoId;?>?zoom=' + zoom+'&andar=' + andar;
        <?php } ?>
    }

    function inverter() {
        let andar = getAndar();

        location.href = '<?php echo base_url()?>products/bus/<?=$product->id;?>/<?=$tipoTransporte->id;?>/<?=$programacaoId;?>?inverter=true&zoom=' + zoom + '&andar=' + andar;
    }

    function reverter() {
        let andar = getAndar();

        location.href = '<?php echo base_url()?>products/bus/<?=$product->id;?>/<?=$tipoTransporte->id;?>/<?=$programacaoId;?>?zoom=' + zoom + '&andar=' + andar;
    }

    function reduceZoomPlus() {
        zoom = zoom + 10;
        document.body.style.zoom = zoom + "%";
    }

    function reduceZoomMinus() {
        zoom = zoom - 10;
        document.body.style.zoom = zoom + "%";
    }

    function abrir_venda(tag) {
        var venda = tag.getAttribute("venda");
        if (venda !== '') {
            let nome_passageiro = tag.getAttribute('nome_passageiro');
            if (confirm('Passageiro '+ nome_passageiro +' \nDeseja abrir os dados da poltrona?')) {
                var url = '<?php echo base_url() ?>sales/edit/' + venda;
                window.open(url,'_blank');
            }
        } else {
            alert('Assento nao atribuido a nenhum passageiro');
        }
    }

   function bloquie_assento(tag) {

       let assento = tag.getAttribute('data-position-order');
       let assento_name = tag.getAttribute('data-position-order_name');
       let status = tag.getAttribute('data-status');
       let nome_passageiro = tag.getAttribute('nome_passageiro');
       let bloqueio = tag.getAttribute('bloqueio');

       if (status === 'enabled') {

           let note  = window.prompt('Descreve uma nota sobre o bloqueio desta asento para confirmar.');

           if (note !== '') {
               $.ajax({
                   type: "get",
                   async: false,
                   data: {
                       tipo_transporte_id: <?=$tipoTransporte->id;?>,
                       programacao_id: <?=$programacaoId;?>,
                       product_id: <?=$product->id;?>,
                       assento: assento,
                       assento_name: assento_name,
                       note: note,
                   },
                   url: site.base_url + "bus/bloqueio_assento",
                   dataType: "html",
                   success: function (data) {
                       location.reload();
                   }
               });
           }

       } else if (status === 'disabled'){
           alert('Este assento contra-se ocupado para ' + nome_passageiro);
           abrir_venda(tag);
       } else if (status === 'reservado') {
           if (confirm('Deseja desbloquear este assento?')) {
               $.ajax({
                   type: "get",
                   async: false,
                   data: {
                       bloqueio: bloqueio
                   },
                   url: site.base_url + "bus/desbloqueio_assento",
                   dataType: "html",
                   success: function (data) {
                       location.reload();
                   }
               });
           }
       }
   }

   function habilitar(tag) {
       habilitar_andar(tag.value);
   }

   function habilitar_andar(andar) {
       $('.andar-1').hide();
       $('.andar-2').hide();

       $('.andar-' + andar + '[habilitado="1"]').show();
   }
</script>