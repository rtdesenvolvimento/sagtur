<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administracao extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('customers', $this->Settings->user_language);
        $this->load->model('companies_model');

        //$this->output->cache(5);
    }

    private function getEmpresa($cnpjempresa) {

        if ($cnpjempresa == '06624498943') $cnpjempresa = 'andrevelho';//TODO TEMPORARIO
        if ($cnpjempresa == 'demonstracao')   $cnpjempresa = 'demonstracaosagtur';//TODO TEMPORARIO

        if ($cnpjempresa == '24072440000112') $cnpjempresa = 'doisforasteiros';//TODO TEMPORARIO
        if ($cnpjempresa == '28203338000160') $cnpjempresa = 'muralhatour';//TODO TEMPORARIO
        if ($cnpjempresa == '29097930000197') $cnpjempresa = 'bmsadventure';//TODO TEMPORARIO
        if ($cnpjempresa == '28935650000148') $cnpjempresa = 'pajeturexcursoesmaraba';//TODO TEMPORARIO
        if ($cnpjempresa == '15536368000146') $cnpjempresa = 'lagunasulturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34400561000108') $cnpjempresa = 'yellowfun';//TODO TEMPORARIO
        if ($cnpjempresa == '26774129000140') $cnpjempresa = 'italiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24500089000113') $cnpjempresa = 'marcosexcursoeseeventos';//TODO TEMPORARIO
        if ($cnpjempresa == '34722503000192') $cnpjempresa = 'tripdajana';//TODO TEMPORARIO
        if ($cnpjempresa == '33619978000195') $cnpjempresa = 'realitytur';//TODO TEMPORARIO
        if ($cnpjempresa == '33166902000151') $cnpjempresa = 'crisandtriptour';//TODO TEMPORARIO
        if ($cnpjempresa == '28203338000160') $cnpjempresa = 'muralhatour';//TODO TEMPORARIO
        if ($cnpjempresa == '30331234000182') $cnpjempresa = 'sicatur';//TODO TEMPORARIO
        if ($cnpjempresa == '17833342000195') $cnpjempresa = 'csviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24005646000120') $cnpjempresa = 'alcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29194452000133') $cnpjempresa = 'penochao';//TODO TEMPORARIO
        if ($cnpjempresa == '19941072000161') $cnpjempresa = 'enjoyviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13338144000102') $cnpjempresa = 'leehtourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '34615832000134') $cnpjempresa = 'augustusturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32581932000161') $cnpjempresa = 'maistravel';//TODO TEMPORARIO
        if ($cnpjempresa == '24246033000184') $cnpjempresa = 'boanovaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30291428000100') $cnpjempresa = 'cksturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40129230000170') $cnpjempresa = 'goldtourtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '28076047000158') $cnpjempresa = 'viajandobrasilafora';//TODO TEMPORARIO
        if ($cnpjempresa == '13241569000190') $cnpjempresa = 'riosexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '20928726000104') $cnpjempresa = 'lpexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '29245296000192') $cnpjempresa = 'semprevix';//TODO TEMPORARIO
        if ($cnpjempresa == '27890670000187') $cnpjempresa = 'mochilaselfie';//TODO TEMPORARIO
        if ($cnpjempresa == '28948910000110') $cnpjempresa = 'uppertrip';//TODO TEMPORARIO
        if ($cnpjempresa == '30891666000148') $cnpjempresa = 'pipaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42122208000123') $cnpjempresa = 'msturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32955843000138') $cnpjempresa = 'hanstour';//TODO TEMPORARIO
        if ($cnpjempresa == '14941063000157') $cnpjempresa = 'amigostur';//TODO TEMPORARIO
        if ($cnpjempresa == '38402047000154') $cnpjempresa = 'vivitour';//TODO TEMPORARIO
        if ($cnpjempresa == '34082358000122') $cnpjempresa = 'viajecomanegaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19288850000165') $cnpjempresa = 'tatatur';//TODO TEMPORARIO
        if ($cnpjempresa == '30439133000120') $cnpjempresa = 'glatur';//TODO TEMPORARIO
        if ($cnpjempresa == '37182808000147') $cnpjempresa = 'stylosturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '17443240000163') $cnpjempresa = 'bellatour';//TODO TEMPORARIO
        if ($cnpjempresa == '29793176000120') $cnpjempresa = 'oliveirastrips';//TODO TEMPORARIO
        if ($cnpjempresa == '27004104000120') $cnpjempresa = 'viajecomigomm';//TODO TEMPORARIO
        if ($cnpjempresa == '33078629000103') $cnpjempresa = 'diversaotour';//TODO TEMPORARIO
        if ($cnpjempresa == '26976469000154') $cnpjempresa = 'excursoesbrunamoraes';//TODO TEMPORARIO
        if ($cnpjempresa == '35424883000141') $cnpjempresa = 'mendesviagenstur';//TODO TEMPORARIO
        if ($cnpjempresa == '26708588000126') $cnpjempresa = 'thaistourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37432064000171') $cnpjempresa = 'magictour';//TODO TEMPORARIO
        if ($cnpjempresa == '35681423000107') $cnpjempresa = 'itaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43759073000174') $cnpjempresa = 'viagensturismosp';//TODO TEMPORARIO
        if ($cnpjempresa == '31014032000170') $cnpjempresa = 'mieventoseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32713811000126') $cnpjempresa = 'libertytur';//TODO TEMPORARIO
        if ($cnpjempresa == '19870536000196') $cnpjempresa = 'loniturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '20777080000101') $cnpjempresa = 'herediaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43348901000181') $cnpjempresa = 'brasiltripstour';//TODO TEMPORARIO
        if ($cnpjempresa == '40070888000153') $cnpjempresa = 'msvviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32305744000100') $cnpjempresa = 'eufuiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31678573000100') $cnpjempresa = 'helenagerraviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37738156000184') $cnpjempresa = 'abrangeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26954715000177') $cnpjempresa = 'felipeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '24729013000164') $cnpjempresa = 'sanmartinviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27288477000170') $cnpjempresa = 'carolviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39274043000109') $cnpjempresa = 'viajeseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35023482000180') $cnpjempresa = 'turismojs';//TODO TEMPORARIO
        if ($cnpjempresa == '26315294000134') $cnpjempresa = 'rbrturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30491098000198') $cnpjempresa = 'turdans';//TODO TEMPORARIO
        if ($cnpjempresa == '31012340000166') $cnpjempresa = 'grazziviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44018494000107') $cnpjempresa = 'aletur';//TODO TEMPORARIO
        if ($cnpjempresa == '26705523000127') $cnpjempresa = 'sergiotur';//TODO TEMPORARIO
        if ($cnpjempresa == '30244458000157') $cnpjempresa = 'luturismoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '44064539000180') $cnpjempresa = 'infinitytripstour';//TODO TEMPORARIO
        if ($cnpjempresa == '29362577000125') $cnpjempresa = 'vemcomagenteviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30599273000165') $cnpjempresa = 'viajandopelomundotur';//TODO TEMPORARIO
        if ($cnpjempresa == '27547096000169') $cnpjempresa = 'barrilviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43399431000185') $cnpjempresa = 'guiademontanha';//TODO TEMPORARIO
        if ($cnpjempresa == '32172434000165') $cnpjempresa = 'riscandoomapa';//TODO TEMPORARIO
        if ($cnpjempresa == '41128510000126') $cnpjempresa = 'prefiroviajar';//TODO TEMPORARIO
        if ($cnpjempresa == '32107957000128') $cnpjempresa = 'roadtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '33917431000176') $cnpjempresa = 'angelacardosoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28452160000190') $cnpjempresa = 'dannytur';//TODO TEMPORARIO
        if ($cnpjempresa == '34388979000139') $cnpjempresa = 'duplabagagem';//TODO TEMPORARIO
        if ($cnpjempresa == '40986189000158') $cnpjempresa = 'trip4fun';//TODO TEMPORARIO
        if ($cnpjempresa == '28526240000143') $cnpjempresa = 'partiutrips';//TODO TEMPORARIO
        if ($cnpjempresa == '33812938000165') $cnpjempresa = 'jalapaoeletrizanteturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41856069000107') $cnpjempresa = 'daviturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38117761000109') $cnpjempresa = 'riosviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07130509000106') $cnpjempresa = 'atmviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '34166438000166') $cnpjempresa = 'paradise';//TODO TEMPORARIO
        if ($cnpjempresa == '26538969000104') $cnpjempresa = 'depalmasturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14073554000123') $cnpjempresa = 'classeviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35671132000120') $cnpjempresa = 'ecoturismodivinopolis';//TODO TEMPORARIO
        if ($cnpjempresa == '39679371000187') $cnpjempresa = 'tfturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52170551000105') $cnpjempresa = 'luluturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28592620000186') $cnpjempresa = 'newdreamstur';//TODO TEMPORARIO
        if ($cnpjempresa == '36974849000103') $cnpjempresa = 'fenixpasseioseaventuras';//TODO TEMPORARIO
        if ($cnpjempresa == '36056364000130') $cnpjempresa = 'excomturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44136973000128') $cnpjempresa = 'codigodeviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '39973743000183') $cnpjempresa = 'enjoyviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33384229000126') $cnpjempresa = 'dnexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '40179754000175') $cnpjempresa = 'limaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41225675000116') $cnpjempresa = 'friendsviagenssp';//TODO TEMPORARIO
        if ($cnpjempresa == '31447413000142') $cnpjempresa = 'trictour';//TODO TEMPORARIO
        if ($cnpjempresa == '25045997000127') $cnpjempresa = 'avesvoandoalto';//TODO TEMPORARIO
        if ($cnpjempresa == '30949801000169') $cnpjempresa = 'saochicoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32366271000151') $cnpjempresa = 'newtimetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '34718011000123') $cnpjempresa = 'brotherstrips';//TODO TEMPORARIO
        if ($cnpjempresa == '25935290000196') $cnpjempresa = 'manniadiviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '12450190000128') $cnpjempresa = 'leletur';//TODO TEMPORARIO
        if ($cnpjempresa == '23923768000132') $cnpjempresa = 'gbtour';//TODO TEMPORARIO
        if ($cnpjempresa == '42980073000137') $cnpjempresa = 'kltourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28065302000167') $cnpjempresa = 'schimyteventos';//TODO TEMPORARIO
        if ($cnpjempresa == '20131237000119') $cnpjempresa = 'daniviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24020259000162') $cnpjempresa = 'natsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45656099000112') $cnpjempresa = 'mtkturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '16862375000109') $cnpjempresa = 'mroyalviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28521221000124') $cnpjempresa = 'paraviverturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33672570000187') $cnpjempresa = 'oliveiraviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43112124000171') $cnpjempresa = 'novetour';//TODO TEMPORARIO
        if ($cnpjempresa == '22997355000130') $cnpjempresa = 'rtdsouza';//TODO TEMPORARIO
        if ($cnpjempresa == '29676117000171') $cnpjempresa = 'excursoestiacleusa';//TODO TEMPORARIO
        if ($cnpjempresa == '34643092000140') $cnpjempresa = 'kirratravel';//TODO TEMPORARIO
        if ($cnpjempresa == '41483075000158') $cnpjempresa = 'terradosolecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28763440000110') $cnpjempresa = 'verdevaleturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13666262000131') $cnpjempresa = 'vaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42756179000151') $cnpjempresa = 'deoliturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47998851000139') $cnpjempresa = 'nenaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27186897000146') $cnpjempresa = 'maisviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32515539000170') $cnpjempresa = 'maeefilhosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32076814000104') $cnpjempresa = 'azevedoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13333777000110') $cnpjempresa = 'vamosdepenaestrada';//TODO TEMPORARIO
        if ($cnpjempresa == '37476039000190') $cnpjempresa = 'viagensorion';//TODO TEMPORARIO
        if ($cnpjempresa == '31659433000187') $cnpjempresa = 'mpwturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '18138398000192') $cnpjempresa = 'originalestradeiros';//TODO TEMPORARIO
        if ($cnpjempresa == '40271677000189') $cnpjempresa = 'cesinhatur';//TODO TEMPORARIO
        if ($cnpjempresa == '30497682000150') $cnpjempresa = 'airbrosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32250898000142') $cnpjempresa = 'eleveviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30320509000182') $cnpjempresa = 'curtaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '46195172000169') $cnpjempresa = 'ltktrip';//TODO TEMPORARIO
        if ($cnpjempresa == '45848368000142') $cnpjempresa = 'agenciaplanettrip';//TODO TEMPORARIO
        if ($cnpjempresa == '27182758000144') $cnpjempresa = 'rosadosventos';//TODO TEMPORARIO
        if ($cnpjempresa == '36293940000163') $cnpjempresa = 'agenciamonteiro';//TODO TEMPORARIO
        if ($cnpjempresa == '42808476000101') $cnpjempresa = 'eliteagtur';//TODO TEMPORARIO
        if ($cnpjempresa == '29144634000108') $cnpjempresa = 'slviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42015101000186') $cnpjempresa = 'sophiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '73379372000144') $cnpjempresa = 'aramastur';//TODO TEMPORARIO
        if ($cnpjempresa == '38452333000124') $cnpjempresa = 'jvexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '40229228000172') $cnpjempresa = 'leotavaresturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '10837034000199') $cnpjempresa = 'peguesuamochila';//TODO TEMPORARIO
        if ($cnpjempresa == '11075549000161') $cnpjempresa = 'nanaturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '09296555000105') $cnpjempresa = 'apviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '12760402000173') $cnpjempresa = 'romeroexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '29830724000144') $cnpjempresa = 'williamviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '39716197000103') $cnpjempresa = 'gturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37711612000100') $cnpjempresa = 'lifestour';//TODO TEMPORARIO
        if ($cnpjempresa == '33159853000120') $cnpjempresa = 'vivavidaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31063525000108') $cnpjempresa = 'acosttaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '29857951000163') $cnpjempresa = 'gpviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '15536368000146') $cnpjempresa = 'lagunasulturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '23429156000198') $cnpjempresa = 'gusmaoviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29122796000137') $cnpjempresa = 'neubauerturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45674415000189') $cnpjempresa = 'familialourencoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43508706000171') $cnpjempresa = 'jptour';//TODO TEMPORARIO
        if ($cnpjempresa == '50361687000169') $cnpjempresa = 'dhtriptour';//TODO TEMPORARIO
        if ($cnpjempresa == '33967010000150') $cnpjempresa = 'vfbturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34764512000146') $cnpjempresa = 'lilexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '42196707000165') $cnpjempresa = 'perollaricoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27897111000107') $cnpjempresa = 'rumocertoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33452360000183') $cnpjempresa = 'roselyviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31493840000167') $cnpjempresa = 'avohaiecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24397104000140') $cnpjempresa = 'ag2g2turismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39310657000190') $cnpjempresa = 'brilhanteturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46719723000146') $cnpjempresa = 'magiatrip';//TODO TEMPORARIO
        if ($cnpjempresa == '20435412000161') $cnpjempresa = 'inesbitencourtturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28940710000110') $cnpjempresa = 'tripforever';//TODO TEMPORARIO
        if ($cnpjempresa == '15382408000142') $cnpjempresa = 'dclasseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '11880764000135') $cnpjempresa = 'sabrinaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '20317795000173') $cnpjempresa = 'marciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '05830342000152') $cnpjempresa = 'transcapturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34645321000165') $cnpjempresa = 'allianzturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35433301000193') $cnpjempresa = 'anayaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '38285051000180') $cnpjempresa = 'viajareprecisope';//TODO TEMPORARIO
        if ($cnpjempresa == '26109186000105') $cnpjempresa = 'locomotivanomade';//TODO TEMPORARIO
        if ($cnpjempresa == '32211281000118') $cnpjempresa = 'triprapviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '15544990000104') $cnpjempresa = 'sntturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29561179000138') $cnpjempresa = 'alegriaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33520971000111') $cnpjempresa = 'plenasviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '38614916000104') $cnpjempresa = 'vogetour';//TODO TEMPORARIO
        if ($cnpjempresa == '38233589000140') $cnpjempresa = 'paccotour';//TODO TEMPORARIO
        if ($cnpjempresa == '47220311000120') $cnpjempresa = 'brturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '78354636000129') $cnpjempresa = 'facisc';//TODO TEMPORARIO
        if ($cnpjempresa == '30395675000148') $cnpjempresa = 'nevesturexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '40498181000142') $cnpjempresa = 'alexturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43627182000138') $cnpjempresa = 'dottaaventura';//TODO TEMPORARIO
        if ($cnpjempresa == '47648941000108') $cnpjempresa = 'entreamigosexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '28122222000104') $cnpjempresa = 'ctljexecutiva';//TODO TEMPORARIO
        if ($cnpjempresa == '13283065000133') $cnpjempresa = 'novoshorizontes';//TODO TEMPORARIO
        if ($cnpjempresa == '32177583000117') $cnpjempresa = 'livreacessoturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '46936366000178') $cnpjempresa = 'mgbusviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '74775701000139') $cnpjempresa = 'bmgturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45519169000190') $cnpjempresa = 'juhtur';//TODO TEMPORARIO
        if ($cnpjempresa == '28350351000141') $cnpjempresa = 'aventour';//TODO TEMPORARIO
        if ($cnpjempresa == '32384712000148') $cnpjempresa = 'todeboaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47859987000168') $cnpjempresa = 'fazamala';//TODO TEMPORARIO
        if ($cnpjempresa == '33022309000131') $cnpjempresa = 'spvturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31834090000140') $cnpjempresa = 'giexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '16892600000141') $cnpjempresa = 'republicasantinha';//TODO TEMPORARIO
        if ($cnpjempresa == '39241477873')    $cnpjempresa = 'hospedagemfunhouse';//TODO TEMPORARIO
        if ($cnpjempresa == '34614194000137') $cnpjempresa = 'lvctravel';//TODO TEMPORARIO
        if ($cnpjempresa == '09084776000101') $cnpjempresa = 'andreytur';//TODO TEMPORARIO
        if ($cnpjempresa == '44673343000192') $cnpjempresa = 'giselleviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31111469000121') $cnpjempresa = 'doneganatour';//TODO TEMPORARIO
        if ($cnpjempresa == '00840324000138') $cnpjempresa = 'navaltur';//TODO TEMPORARIO
        if ($cnpjempresa == '12061304000148') $cnpjempresa = 'viagensciaturismoeventos';//TODO TEMPORARIO
        if ($cnpjempresa == '24042958000103') $cnpjempresa = 'amazonanglersturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39712558000135') $cnpjempresa = 'evancatour';//TODO TEMPORARIO
        if ($cnpjempresa == '32261084000103') $cnpjempresa = 'familyturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40678527000194') $cnpjempresa = 'essenciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '10207342000130') $cnpjempresa = 'cmfviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31895417000193') $cnpjempresa = 'boraaliexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '32321631000107') $cnpjempresa = 'corujaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30250603000102') $cnpjempresa = 'kadoshadonaiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '38296660000134') $cnpjempresa = 'agencianovosdestinos';//TODO TEMPORARIO
        if ($cnpjempresa == '41957971000101') $cnpjempresa = 'lvtour';//TODO TEMPORARIO
        if ($cnpjempresa == '34181741000138') $cnpjempresa = 'plenissimostrips';//TODO TEMPORARIO
        if ($cnpjempresa == '40392109000136') $cnpjempresa = 'santuaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34774052000137') $cnpjempresa = 'dgaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31501093000161') $cnpjempresa = 'turismocerto';//TODO TEMPORARIO
        if ($cnpjempresa == '23167053000105') $cnpjempresa = 'dhagesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28646407000100') $cnpjempresa = 'emersonfelipeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '56673908000110') $cnpjempresa = 'lelynho';//TODO TEMPORARIO
        if ($cnpjempresa == '26117963000163') $cnpjempresa = 'rfturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14554319000173') $cnpjempresa = 'descubrafloripa';//TODO TEMPORARIO
        if ($cnpjempresa == '42461649000150') $cnpjempresa = 'ldviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23335365000172') $cnpjempresa = 'tripsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48293820000145') $cnpjempresa = 'anajuturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14675785000107') $cnpjempresa = 'rturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42526878000105') $cnpjempresa = 'emitripsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47344671000133') $cnpjempresa = 'igturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22426735000114') $cnpjempresa = 'viagens4lovers';//TODO TEMPORARIO
        if ($cnpjempresa == '43006301000135') $cnpjempresa = 'enjoyexperience';//TODO TEMPORARIO
        if ($cnpjempresa == '35443481000194') $cnpjempresa = 'mimatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '41612148000164') $cnpjempresa = 'danviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '13864528000150') $cnpjempresa = 'maxxitours';//TODO TEMPORARIO
        if ($cnpjempresa == '34773592000104') $cnpjempresa = 'cincoamigostour';//TODO TEMPORARIO
        if ($cnpjempresa == '26107426000132') $cnpjempresa = 'karinasturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38385149000109') $cnpjempresa = 'diegotourjalapao';//TODO TEMPORARIO
        if ($cnpjempresa == '36247836000132') $cnpjempresa = 'girltrips';//TODO TEMPORARIO
        if ($cnpjempresa == '21646605000124') $cnpjempresa = 'evoluirviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47667976000186') $cnpjempresa = 'pensetripsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '20988415000122') $cnpjempresa = 'mdiasturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30197759000177') $cnpjempresa = 'jkturismobc';//TODO TEMPORARIO
        if ($cnpjempresa == '28981135000102') $cnpjempresa = 'girlsgoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '40141334000108') $cnpjempresa = 'ticastour';//TODO TEMPORARIO
        if ($cnpjempresa == '48905907000126') $cnpjempresa = 'clafertur';//TODO TEMPORARIO
        if ($cnpjempresa == '20165976000121') $cnpjempresa = 'katturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28559768000119') $cnpjempresa = 'mariliaviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34963378000102') $cnpjempresa = 'heloviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '39753194000131') $cnpjempresa = 'stilostur';//TODO TEMPORARIO
        if ($cnpjempresa == '31748310000112') $cnpjempresa = 'raphatrip';//TODO TEMPORARIO
        if ($cnpjempresa == '49059176000108') $cnpjempresa = 'tursolidario';//TODO TEMPORARIO
        if ($cnpjempresa == '33557911000173') $cnpjempresa = 'msbusviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27493262000191') $cnpjempresa = 'braolitour';//TODO TEMPORARIO
        if ($cnpjempresa == '40714437000101') $cnpjempresa = 'turmadovandre';//TODO TEMPORARIO
        if ($cnpjempresa == '24554827000105') $cnpjempresa = 'remtravel';//TODO TEMPORARIO
        if ($cnpjempresa == '49269308000126') $cnpjempresa = 'lorranturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07123488000193') $cnpjempresa = 'meloretur';//TODO TEMPORARIO
        if ($cnpjempresa == '49185081000130') $cnpjempresa = 'tremdaalegriaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '49010694000137') $cnpjempresa = 'skviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48145185000159') $cnpjempresa = 'f5tour';//TODO TEMPORARIO
        if ($cnpjempresa == '34416896000106') $cnpjempresa = 'mmturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27210941000106') $cnpjempresa = 'ventosdeluzviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '52596973000139') $cnpjempresa = 'reservasangulotravel';//TODO TEMPORARIO
        if ($cnpjempresa == '35286918000123') $cnpjempresa = 'lovefortrips';//TODO TEMPORARIO
        if ($cnpjempresa == '28524458000169') $cnpjempresa = 'jalapoeirosecotour';//TODO TEMPORARIO
        if ($cnpjempresa == '49465179000141') $cnpjempresa = 'navetour';//TODO TEMPORARIO
        if ($cnpjempresa == '38872038000127') $cnpjempresa = 'dritour';//TODO TEMPORARIO
        if ($cnpjempresa == '32514266000149') $cnpjempresa = 'bairestur';//TODO TEMPORARIO
        if ($cnpjempresa == '43069171000180') $cnpjempresa = 'vivantturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27887345000165') $cnpjempresa = 'adventuresturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40932815000123') $cnpjempresa = 'turismoguarulhos';//TODO TEMPORARIO
        if ($cnpjempresa == '47958925000103') $cnpjempresa = 'mulheres100fronteiras';//TODO TEMPORARIO
        if ($cnpjempresa == '35597067000130') $cnpjempresa = 'excursoespontonell';//TODO TEMPORARIO
        if ($cnpjempresa == '32573927000107') $cnpjempresa = 'excursoesabc';//TODO TEMPORARIO
        if ($cnpjempresa == '23998127000147') $cnpjempresa = 'andersonexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '24971929000126') $cnpjempresa = 'azenhaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39448228000184') $cnpjempresa = 'dorinhatrips';//TODO TEMPORARIO
        if ($cnpjempresa == '19720969000165') $cnpjempresa = 'konexaodosambaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33575305000180') $cnpjempresa = 'petrusviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23916973000170') $cnpjempresa = 'teudestino';//TODO TEMPORARIO
        if ($cnpjempresa == '49420809000161') $cnpjempresa = 'peacetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '33878354000192') $cnpjempresa = 'excursoesdabecky';//TODO TEMPORARIO
        if ($cnpjempresa == '48969391000183') $cnpjempresa = 'fdtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '44676714000190') $cnpjempresa = 'alvarengaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '18376031000107') $cnpjempresa = 'novotempoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13285539000186') $cnpjempresa = 'planaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40687114000176') $cnpjempresa = 'cmviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28719264000119') $cnpjempresa = 'gmtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '36535236000170') $cnpjempresa = 'multturismonatal';//TODO TEMPORARIO
        if ($cnpjempresa == '28155974000163') $cnpjempresa = 'vounaviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '26188041000147') $cnpjempresa = 'viajolandiatour';//TODO TEMPORARIO
        if ($cnpjempresa == '36678815000171') $cnpjempresa = 'proximaparadaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '52678636000190') $cnpjempresa = 'girandoomundotour';//TODO TEMPORARIO
        if ($cnpjempresa == '49310740000113') $cnpjempresa = 'alvanevasconcelosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26868644000190') $cnpjempresa = 'princesaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30827727000108') $cnpjempresa = 'tioniaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48758070000130') $cnpjempresa = 'grajatour';//TODO TEMPORARIO
        if ($cnpjempresa == '43258712000118') $cnpjempresa = 'consutoriaeassessoriaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30327658000173') $cnpjempresa = 'araceturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48894830000136') $cnpjempresa = 'monteirodiversoes';//TODO TEMPORARIO
        if ($cnpjempresa == '12241910000145') $cnpjempresa = 'smcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43126837000194') $cnpjempresa = 'rcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42048755000106') $cnpjempresa = 'donatoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46587316000122') $cnpjempresa = 'granturjalapaoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '16588392000191') $cnpjempresa = 'carolviagenseconcursos';//TODO TEMPORARIO
        if ($cnpjempresa == '26466396000150') $cnpjempresa = 'happyviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '32401582000104') $cnpjempresa = 'brasilviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33358225000173') $cnpjempresa = 'soniaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22162022000190') $cnpjempresa = 'bmmturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48955238000105') $cnpjempresa = 'mbviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23485265000122') $cnpjempresa = 'garcabranca';//TODO TEMPORARIO
        if ($cnpjempresa == '47027449000107') $cnpjempresa = 'tripemgalera';//TODO TEMPORARIO
        if ($cnpjempresa == '27585169000107') $cnpjempresa = 'maximatur';//TODO TEMPORARIO
        if ($cnpjempresa == '26015341000124') $cnpjempresa = 'lilaotrip';//TODO TEMPORARIO
        if ($cnpjempresa == '49488167000132') $cnpjempresa = 'cardosocitytour';//TODO TEMPORARIO
        if ($cnpjempresa == '25174268000170') $cnpjempresa = 'bragaturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31163542000109') $cnpjempresa = 'bellaferias';//TODO TEMPORARIO
        if ($cnpjempresa == '15188548000184') $cnpjempresa = 'estanciatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28393373000199') $cnpjempresa = 'wayaventura';//TODO TEMPORARIO
        if ($cnpjempresa == '33824874000112') $cnpjempresa = 'lamarcaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '50134690000140') $cnpjempresa = 'joseniltonviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '34319925000111') $cnpjempresa = 'josatur';//TODO TEMPORARIO
        if ($cnpjempresa == '57522369000181') $cnpjempresa = 'lloydsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42808736000130') $cnpjempresa = 'idealturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41860729000115') $cnpjempresa = 'turismochapada';//TODO TEMPORARIO
        if ($cnpjempresa == '38903517000163') $cnpjempresa = 'vmturismoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32094553000147') $cnpjempresa = 'easytransfer';//TODO TEMPORARIO
        if ($cnpjempresa == '36431606000129') $cnpjempresa = 'lariechrisviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '46837285000110') $cnpjempresa = 'leoturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32368439000168') $cnpjempresa = 'jrturismonovaiguacu';//TODO TEMPORARIO
        if ($cnpjempresa == '35681423000280') $cnpjempresa = 'itaviagensfilial2';//TODO TEMPORARIO
        if ($cnpjempresa == '22307450000164') $cnpjempresa = 'mfviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42300807000190') $cnpjempresa = 'refexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '50800484000121') $cnpjempresa = 'itaviagensfilial3';//TODO TEMPORARIO
        if ($cnpjempresa == '50019981000197') $cnpjempresa = 'vivalavidatripstour';//TODO TEMPORARIO
        if ($cnpjempresa == '17353036000151') $cnpjempresa = 'patotrips';//TODO TEMPORARIO
        if ($cnpjempresa == '03354507000131') $cnpjempresa = 'cpcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33924480000136') $cnpjempresa = 'tripturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33433626000140') $cnpjempresa = 'planetafolia';//TODO TEMPORARIO
        if ($cnpjempresa == '30566842000176') $cnpjempresa = 'chilebratur';//TODO TEMPORARIO
        if ($cnpjempresa == '40697900000154') $cnpjempresa = 'kaiturismobrasil';//TODO TEMPORARIO
        if ($cnpjempresa == '50911487000132') $cnpjempresa = 'flaviustur';//TODO TEMPORARIO
        if ($cnpjempresa == '29713734000108') $cnpjempresa = 'marcinhotur';//TODO TEMPORARIO
        if ($cnpjempresa == '41537685000197') $cnpjempresa = 'pdviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '51166043000182') $cnpjempresa = 'belavitaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33595167000100') $cnpjempresa = 'penseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24657373000106') $cnpjempresa = 'boralaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37578182000192') $cnpjempresa = 'sheyllacrissturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '51003730000187') $cnpjempresa = 'pedrocordeiroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28581000000141') $cnpjempresa = 'gustavotour';//TODO TEMPORARIO
        if ($cnpjempresa == '19427924000105') $cnpjempresa = 'samarraviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '40964847000100') $cnpjempresa = 'javoltour';//TODO TEMPORARIO
        if ($cnpjempresa == '31483792000126') $cnpjempresa = 'professorperegrino';//TODO TEMPORARIO
        if ($cnpjempresa == '28264253000191') $cnpjempresa = 'expressorodriguez';//TODO TEMPORARIO
        if ($cnpjempresa == '02333534000165') $cnpjempresa = 'ituturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32987145000114') $cnpjempresa = 'laxturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27544470000172') $cnpjempresa = 'connectviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '42572981000191') $cnpjempresa = 'kingtripsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35279948000102') $cnpjempresa = 'gktravel';//TODO TEMPORARIO
        if ($cnpjempresa == '38174529000102') $cnpjempresa = 'suncabanaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40710284000124') $cnpjempresa = 'upofiicial';//TODO TEMPORARIO
        if ($cnpjempresa == '29610454000166') $cnpjempresa = 'parceirustur';//TODO TEMPORARIO
        if ($cnpjempresa == '41251615000178') $cnpjempresa = 'amazonnegociostur';//TODO TEMPORARIO
        if ($cnpjempresa == '12362419000172') $cnpjempresa = 'afaiaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '22787632000180') $cnpjempresa = 'agladiadoraturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '51739041000135') $cnpjempresa = 'tuliosilvaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46060234000125') $cnpjempresa = 'lucianotourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '46227865000196') $cnpjempresa = 'dataviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30745067000116') $cnpjempresa = 'micheleturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '08934059000169') $cnpjempresa = 'agencianewstart';//TODO TEMPORARIO
        if ($cnpjempresa == '47650962000150') $cnpjempresa = 'dunamisdojalapao';//TODO TEMPORARIO
        if ($cnpjempresa == '51833792000116') $cnpjempresa = 'rpassaingressos';//TODO TEMPORARIO
        if ($cnpjempresa == '02109322000107') $cnpjempresa = 'voyageturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '12144461000117') $cnpjempresa = 'agtturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34873726000150') $cnpjempresa = 'vivaturismodeexperiencia';//TODO TEMPORARIO
        if ($cnpjempresa == '27797821000157') $cnpjempresa = 'jalapaobrasilto';//TODO TEMPORARIO
        if ($cnpjempresa == '26896133000181') $cnpjempresa = 'lutourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '49298606000144') $cnpjempresa = 'gatodomatoecoadventure';//TODO TEMPORARIO
        if ($cnpjempresa == '42950930000156') $cnpjempresa = 'marizeterodriguesexcursao';//TODO TEMPORARIO
        if ($cnpjempresa == '12192187000151') $cnpjempresa = 'vallendarturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '25199693000114') $cnpjempresa = 'dallonder';//TODO TEMPORARIO
        if ($cnpjempresa == '39811525000142') $cnpjempresa = 'vinitur';//TODO TEMPORARIO
        if ($cnpjempresa == '32322965000197') $cnpjempresa = 'angelvictour';//TODO TEMPORARIO
        if ($cnpjempresa == '05423541000146') $cnpjempresa = 'rotadovale';//TODO TEMPORARIO
        if ($cnpjempresa == '43802649000139') $cnpjempresa = 'asguriastur';//TODO TEMPORARI
        if ($cnpjempresa == '41446324000135') $cnpjempresa = 'alchaarparticipacoes';//TODO TEMPORARIO
        if ($cnpjempresa == '35103468000196') $cnpjempresa = 'dharmaturismo';//TODO TEMPORARIO
        //if ($cnpjempresa == '35838245000177') $cnpjempresa = 'fdvagenciadeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31044704000190') $cnpjempresa = 'charmeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '36723932000100') $cnpjempresa = 'shekinahtrekking';//TODO TEMPORARIO
        if ($cnpjempresa == '46894639000169') $cnpjempresa = 'bigturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41070291000171') $cnpjempresa = 'dutratur';//TODO TEMPORARIO
        if ($cnpjempresa == '46614496000194') $cnpjempresa = 'goncalvesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32258248000143') $cnpjempresa = 'encantoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31962554000101') $cnpjempresa = 'gersonaguiarturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49969273000138') $cnpjempresa = 'sejavipturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19225075000107') $cnpjempresa = 'premiumturismmo';//TODO TEMPORARIO
        if ($cnpjempresa == '46195166000101') $cnpjempresa = 'jdviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28132234000101') $cnpjempresa = 'mantoazulturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33831909000140') $cnpjempresa = 'primeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41810081000172') $cnpjempresa = 'partiuexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '48562448000125') $cnpjempresa = 'voyagevibes';//TODO TEMPORARIO
        if ($cnpjempresa == '38068658000108') $cnpjempresa = 'angelicaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '42860638000142') $cnpjempresa = 'abctripturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52544142000113') $cnpjempresa = 'sendingtur';//TODO TEMPORARIO
        if ($cnpjempresa == '38060954000162') $cnpjempresa = 'liberttravel';//TODO TEMPORARIO
        if ($cnpjempresa == '30600166000100') $cnpjempresa = 'boraviajeiros';//TODO TEMPORARIO
        if ($cnpjempresa == '42639044000106') $cnpjempresa = 'spacetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '51488897000185') $cnpjempresa = 'nessaturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '38377678000160') $cnpjempresa = 'memexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '45802502000174') $cnpjempresa = 'ganeshaadventureviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '41499707000171') $cnpjempresa = 'aureaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24944998000140') $cnpjempresa = 'vmviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43780601000177') $cnpjempresa = 'viajandocomjohnny';//TODO TEMPORARIO
        if ($cnpjempresa == '32868033000144') $cnpjempresa = 'edivanturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38169030000107') $cnpjempresa = 'trilhandoomundo';//TODO TEMPORARIO
        if ($cnpjempresa == '15599774000158') $cnpjempresa = 'safaritur';//TODO TEMPORARIO
        if ($cnpjempresa == '36941996000187') $cnpjempresa = 'poraidemochila';//TODO TEMPORARIO
        if ($cnpjempresa == '38152634000132') $cnpjempresa = 'escarpaspasseiosnauticos';//TODO TEMPORARIO
        if ($cnpjempresa == '31305652000168') $cnpjempresa = 'ketitaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49093187000104') $cnpjempresa = 'fabiotur';//TODO TEMPORARIO
        if ($cnpjempresa == '49245501000127') $cnpjempresa = 'franturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52597653000101') $cnpjempresa = 'lvturismogrupo';//TODO TEMPORARIO
        if ($cnpjempresa == '48752979000180') $cnpjempresa = 'tripzeratur';//TODO TEMPORARIO
        if ($cnpjempresa == '35757018000117') $cnpjempresa = 'jbmexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '14699949000136') $cnpjempresa = 'mrfernandes';//TODO TEMPORARIO
        if ($cnpjempresa == '45704600000179') $cnpjempresa = 'liturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46561439000194') $cnpjempresa = 'levetravel';//TODO TEMPORARIO
        if ($cnpjempresa == '23868679000130') $cnpjempresa = 'vihtour';//TODO TEMPORARIO
        if ($cnpjempresa == '10339582000199') $cnpjempresa = 'caminhosdaterraturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14835354000160') $cnpjempresa = 'silviaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32531513000115') $cnpjempresa = 'sorrisoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44920659000131') $cnpjempresa = 'zeliatur';//TODO TEMPORARIO
        if ($cnpjempresa == '31899301000122') $cnpjempresa = 'vicioviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '39706089000141') $cnpjempresa = 'agenciadeviagensaguiar';//TODO TEMPORARIO
        if ($cnpjempresa == '26869304000183') $cnpjempresa = 'justgotrips';//TODO TEMPORARIO
        if ($cnpjempresa == '52324889000166') $cnpjempresa = 'yestur';//TODO TEMPORARIO
        if ($cnpjempresa == '28703623000140') $cnpjempresa = 'lilianspacetour';//TODO TEMPORARIO
        if ($cnpjempresa == '31644200000100') $cnpjempresa = 'danieleturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30450405000192') $cnpjempresa = 'pedradonegroviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24072157000190') $cnpjempresa = 'conectavips';//TODO TEMPORARIO
        if ($cnpjempresa == '01658738500')    $cnpjempresa = 'alissonviagens_old';//TODO TEMPORARIO
        if ($cnpjempresa == '29640962000197') $cnpjempresa = 'alissonviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '20248627000173') $cnpjempresa = 'vavatur';//TODO TEMPORARIO
        if ($cnpjempresa == '34899233000190') $cnpjempresa = 'wineviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32413081000148') $cnpjempresa = 'estrelastour';//TODO TEMPORARIO
        if ($cnpjempresa == '52103581000190') $cnpjempresa = 'viixeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '21223683000116') $cnpjempresa = 'deboraexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '51488766000106') $cnpjempresa = 'aventuresejalapao';//TODO TEMPORARIO
        if ($cnpjempresa == '53315041000133') $cnpjempresa = 'desrotinetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '43125979000137') $cnpjempresa = 'pretatur';//TODO TEMPORARIO
        if ($cnpjempresa == '22490805000101') $cnpjempresa = 'mitiestours';//TODO TEMPORARIO
        if ($cnpjempresa == '43382120000103') $cnpjempresa = 'keviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '26096072000178') $cnpjempresa = 'calebstourcompany';//TODO TEMPORARIO
        if ($cnpjempresa == '18323819000155') $cnpjempresa = 'jlturturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29722726000110') $cnpjempresa = 'excursoesroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '53266950000129') $cnpjempresa = 'eduviagensexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '50749091000130') $cnpjempresa = 'numanicetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '27400938000155') $cnpjempresa = 'marliturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43755659000160') $cnpjempresa = 'glautour';//TODO TEMPORARIO
        if ($cnpjempresa == '50582521000172') $cnpjempresa = 'tripdavania';//TODO TEMPORARIO
        if ($cnpjempresa == '33560847000180') $cnpjempresa = 'elshadaytur';//TODO TEMPORARIO
        if ($cnpjempresa == '40151742000132') $cnpjempresa = 'imperioturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48360841000136') $cnpjempresa = 'agenciaalohaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '53541141000188') $cnpjempresa = 'tripswei';//TODO TEMPORARIO
        if ($cnpjempresa == '02088829000113') $cnpjempresa = 'pigozzoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39559339000168') $cnpjempresa = 'ksturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19834210000103') $cnpjempresa = 'boraboraviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49568888000152') $cnpjempresa = 'tripslovers';//TODO TEMPORARIO
        if ($cnpjempresa == '49108028000136') $cnpjempresa = 'tawtour';//TODO TEMPORARIO
        if ($cnpjempresa == '10948783000193') $cnpjempresa = 'repturturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37561054000136') $cnpjempresa = 'diadetur';//TODO TEMPORARIO
        if ($cnpjempresa == '52709510000136') $cnpjempresa = 'maravilhatur';//TODO TEMPORARIO
        if ($cnpjempresa == '36661564000112') $cnpjempresa = 'giratour';//TODO TEMPORARIO
        if ($cnpjempresa == '46523909000125') $cnpjempresa = 'arturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45526775000133') $cnpjempresa = 'maldastrips';//TODO TEMPORARIO
        if ($cnpjempresa == '36985188000111') $cnpjempresa = 'franturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '50616315000136') $cnpjempresa = 'betripsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32070091000128') $cnpjempresa = 'jwviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44754101000123') $cnpjempresa = 'guiviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34030630000120') $cnpjempresa = 'jalapao10';//TODO TEMPORARIO
        if ($cnpjempresa == '22800325000191') $cnpjempresa = 'materdeiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '36512111000124') $cnpjempresa = 'riccioviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '14220847000196') $cnpjempresa = 'bellitur';//TODO TEMPORARIO
        if ($cnpjempresa == '34599368000130') $cnpjempresa = 'telesexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '42673262000167') $cnpjempresa = 'tripsdareh';//TODO TEMPORARIO
        if ($cnpjempresa == '15395124000190') $cnpjempresa = 'portalexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '44995985000108') $cnpjempresa = 'seashoreturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '21294464000128') $cnpjempresa = 'federaltur';//TODO TEMPORARIO
        if ($cnpjempresa == '27045508000162') $cnpjempresa = 'betinhotur';//TODO TEMPORARIO
        if ($cnpjempresa == '51377758000184') $cnpjempresa = 'deiaexcursoeseviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43469031000107') $cnpjempresa = 'stonetour';//TODO TEMPORARIO
        if ($cnpjempresa == '46641406000154') $cnpjempresa = 'magnificotur';//TODO TEMPORARIO
        if ($cnpjempresa == '16667689000142') $cnpjempresa = 'raphaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '54181952000188') $cnpjempresa = 'luhexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '12944002000118') $cnpjempresa = 'kilazer';//TODO TEMPORARIO
        if ($cnpjempresa == '18395935000180') $cnpjempresa = 'alextour';//TODO TEMPORARIO
        if ($cnpjempresa == '52965967000101') $cnpjempresa = 'vamoscomigoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30043362000120') $cnpjempresa = 'gtviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '50361687000169') $cnpjempresa = 'vemprocorre';//TODO TEMPORARIO
        if ($cnpjempresa == '42722575000168') $cnpjempresa = 'batsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24122089000127') $cnpjempresa = 'playup';//TODO TEMPORARIO
        if ($cnpjempresa == '11546358000130') $cnpjempresa = 'mtviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '07047078000100') $cnpjempresa = 'triplicetour';//TODO TEMPORARIO
        if ($cnpjempresa == '24480213000126') $cnpjempresa = 'agenciavicktrips';//TODO TEMPORARIO
        if ($cnpjempresa == '35433301000193') $cnpjempresa = 'invistatour';//TODO TEMPORARIO
        if ($cnpjempresa == '17821436000144') $cnpjempresa = 'aguiatur';//TODO TEMPORARIO
        if ($cnpjempresa == '46172157000103') $cnpjempresa = 'dinahturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '14798331000123') $cnpjempresa = 'douratour';//TODO TEMPORARIO
        if ($cnpjempresa == '46977470000100') $cnpjempresa = 'alternativeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '38342425000151') $cnpjempresa = 'allinturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46118182000109') $cnpjempresa = 'girlstrips';//TODO TEMPORARIO
        if ($cnpjempresa == '35109924000105') $cnpjempresa = 'carpediemviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '03404032000140') $cnpjempresa = 'egowtur';//TODO TEMPORARIO
        if ($cnpjempresa == '27147915000180') $cnpjempresa = 'gecioneturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '18845440000105') $cnpjempresa = 'giconect';//TODO TEMPORARIO
        if ($cnpjempresa == '48027215000122') $cnpjempresa = 'danturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32781439000195') $cnpjempresa = 'rssturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49538346000137') $cnpjempresa = 'megacorporativa';//TODO TEMPORARIO
        if ($cnpjempresa == '41995422000121') $cnpjempresa = 'vinhadeluz';//TODO TEMPORARIO
        if ($cnpjempresa == '54932495000116') $cnpjempresa = 'fenixturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07173874000190') $cnpjempresa = 'oliviatour';//TODO TEMPORARIO
        if ($cnpjempresa == '43546540000188') $cnpjempresa = 'brexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '54443081000123') $cnpjempresa = 'gouveiatourgo';//TODO TEMPORARIO
        if ($cnpjempresa == '55008647000151') $cnpjempresa = 'viajandocommaria';//TODO TEMPORARIO
        if ($cnpjempresa == '35259511000107') $cnpjempresa = 'anjosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '36147547000161') $cnpjempresa = 'viagenscamaleao';//TODO TEMPORARIO
        if ($cnpjempresa == '44346398000198') $cnpjempresa = 'dlturismoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '10876079000172') $cnpjempresa = 'caicaraexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '27023439000196') $cnpjempresa = 'pardimturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22521858000134') $cnpjempresa = 'registurismo';//TODO TEMPORARIO
        if ($cnpjempresa == '54536096000136') $cnpjempresa = 'dinamoexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '02878805000168') $cnpjempresa = 'viagensdetrem';//TODO TEMPORARIO
        if ($cnpjempresa == '35998495000174') $cnpjempresa = 'sonicviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30708489000111') $cnpjempresa = 'minasecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32781439000195') $cnpjempresa = 'rssviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24329261000118') $cnpjempresa = 'topdestinosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '15488938000170') $cnpjempresa = 'alkviagenstur';//TODO TEMPORARIO
        if ($cnpjempresa == '21964024000130') $cnpjempresa = 'bemvivertour';//TODO TEMPORARIO
        if ($cnpjempresa == '55190707000108') $cnpjempresa = 'traveltur';//TODO TEMPORARIO
        if ($cnpjempresa == '55351004000106') $cnpjempresa = 'edianetour';//TODO TEMPORARIO
        if ($cnpjempresa == '35411224000170') $cnpjempresa = 'santarotadestinos';//TODO TEMPORARIO
        if ($cnpjempresa == '45247738000196') $cnpjempresa = 'aglaiaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '35554238000143') $cnpjempresa = 'tripshow';//TODO TEMPORARIO
        if ($cnpjempresa == '35176216000197') $cnpjempresa = 'qualityreceptivo';//TODO TEMPORARIO
        if ($cnpjempresa == '34497056000115') $cnpjempresa = 'tatyexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '55437946000101') $cnpjempresa = 'alineviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '55378356000155') $cnpjempresa = 'styllusturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '01679099000162') $cnpjempresa = 'tourmix';//TODO TEMPORARIO
        if ($cnpjempresa == '45711558000113') $cnpjempresa = 'ederturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38237902000119') $cnpjempresa = 'paraisoviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24641996000182') $cnpjempresa = 'noninoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47018522000184') $cnpjempresa = 'rbiturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33019182000100') $cnpjempresa = 'pacotesdetrip';//TODO TEMPORARIO
        if ($cnpjempresa == '54797333000112') $cnpjempresa = 'oasistur';//TODO TEMPORARIO
        if ($cnpjempresa == '49538985000100') $cnpjempresa = 'panatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31881940000160') $cnpjempresa = 'cristianeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30954702000175') $cnpjempresa = 'dhiturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07710851000177') $cnpjempresa = 'cativatur';//TODO TEMPORARIO
        if ($cnpjempresa == '37711299000100') $cnpjempresa = 'happinessintrip';//TODO TEMPORARIO
        if ($cnpjempresa == '51483222000143') $cnpjempresa = 'vibetripviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30742042000169') $cnpjempresa = 'goodtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '55516027000123') $cnpjempresa = 'theworldturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45676346000142') $cnpjempresa = 'itstravel';//TODO TEMPORARIO
        if ($cnpjempresa == '35046011000197') $cnpjempresa = 'enoturbrasilia';//TODO TEMPORARIO
        if ($cnpjempresa == '51710574000194') $cnpjempresa = 'topviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '55778376000113') $cnpjempresa = 'domturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37155315000118') $cnpjempresa = 'alegrotrip';//TODO TEMPORARIO
        if ($cnpjempresa == '32805877000146') $cnpjempresa = 'viagensnacional';//TODO TEMPORARIO
        if ($cnpjempresa == '48823486000194') $cnpjempresa = 'bernardinitrips';//TODO TEMPORARIO
        if ($cnpjempresa == '46022793000140') $cnpjempresa = 'fariaesoaresturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46084930000171') $cnpjempresa = 'morangostour';//TODO TEMPORARIO
        if ($cnpjempresa == '53258852000140') $cnpjempresa = 'renatoturismoadventure';//TODO TEMPORARIO
        if ($cnpjempresa == '35530958000179') $cnpjempresa = 'vivitourltda';//TODO TEMPORARIO
        if ($cnpjempresa == '32496114000160') $cnpjempresa = 'televo';//TODO TEMPORARIO
        if ($cnpjempresa == '23977495000109') $cnpjempresa = 'vibless';//TODO TEMPORARIO
        if ($cnpjempresa == '21286270000180') $cnpjempresa = 'ceumarturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35838245000177') $cnpjempresa = 'vittaltur';//TODO TEMPORARIO
        if ($cnpjempresa == '56052805000133') $cnpjempresa = 'aslexpresso';//TODO TEMPORARIO
        if ($cnpjempresa == '56052805000133') $cnpjempresa = 'aslexpresso';//TODO TEMPORARIO
        if ($cnpjempresa == '51239568000109') $cnpjempresa = 'biaviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44377353000180') $cnpjempresa = 'lamsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27223121000159') $cnpjempresa = 'patyupexcursao';//TODO TEMPORARIO
        if ($cnpjempresa == '55880437000159') $cnpjempresa = 'abepetur';//TODO TEMPORARIO
        if ($cnpjempresa == '40530741000107') $cnpjempresa = 'camargosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48719118000109') $cnpjempresa = 'balcaodeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33164372000102') $cnpjempresa = 'karinaviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '56660599000144') $cnpjempresa = 'raffaviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38260023000108') $cnpjempresa = 'conexaobrasilperu';//TODO TEMPORARIO
        if ($cnpjempresa == '18510915000102') $cnpjempresa = 'julianoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '25342789000190') $cnpjempresa = 'busmar';//TODO TEMPORARIO
        if ($cnpjempresa == '50387887000190') $cnpjempresa = 'stellatours';//TODO TEMPORARIO
        if ($cnpjempresa == '57000277000131') $cnpjempresa = 'domturismomogi';//TODO TEMPORARIO
        if ($cnpjempresa == '56037108000103') $cnpjempresa = 'mgmturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37111616000140') $cnpjempresa = 'jokasturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43483748000103') $cnpjempresa = 'celurturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28456609000199') $cnpjempresa = 'salvadorviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '51929835000161') $cnpjempresa = 'atualviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32875646000109') $cnpjempresa = 'lionstrance';//TODO TEMPORARIO
        if ($cnpjempresa == '37762944000106') $cnpjempresa = 'flyturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '35633004000191') $cnpjempresa = 'havefun';//TODO TEMPORARIO
        if ($cnpjempresa == '51453754000138') $cnpjempresa = 'solariumtur';//TODO TEMPORARIO
        if ($cnpjempresa == '37680710000110') $cnpjempresa = 'ueltrekking';//TODO TEMPORARIO
        if ($cnpjempresa == '51659352000194') $cnpjempresa = 'boanergesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32059493000121') $cnpjempresa = 'scturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32938039000140') $cnpjempresa = 'busaodocabral';//TODO TEMPORARIO
        if ($cnpjempresa == '30751863000161') $cnpjempresa = 'dugeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '57045804000124') $cnpjempresa = 'plenitudeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '56178704000103') $cnpjempresa = 'vamoscomigoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52668950000192') $cnpjempresa = 'travellertur';//TODO TEMPORARIO
        if ($cnpjempresa == '57369672000196') $cnpjempresa = 'viajemaisporai';//TODO TEMPORARIO
        if ($cnpjempresa == '27589649000146') $cnpjempresa = 'gabrielviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33701240000172') $cnpjempresa = 'cenarionativoexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '43351110000100') $cnpjempresa = 'dedessatour';//TODO TEMPORARIO
        if ($cnpjempresa == '27565228000185') $cnpjempresa = 'moserturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37617942000123') $cnpjempresa = 'terraceuviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13659716000147') $cnpjempresa = 'marialuciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28764929000106') $cnpjempresa = 'marituris';//TODO TEMPORARIO
        if ($cnpjempresa == '32676271000158') $cnpjempresa = 'rodzexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '48661022000129') $cnpjempresa = 'ladtur';//TODO TEMPORARIO
        if ($cnpjempresa == '23097837000104') $cnpjempresa = 'qualosegredoaventuras';//TODO TEMPORARIO
        if ($cnpjempresa == '45470017000140') $cnpjempresa = 'tripsdataty';//TODO TEMPORARIO
        if ($cnpjempresa == '20004996000110') $cnpjempresa = 'hollidayviagensetur';//TODO TEMPORARIO
        if ($cnpjempresa == '43002118000161') $cnpjempresa = 'verdeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42443680000168') $cnpjempresa = 'tatytourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '53718437000121') $cnpjempresa = 'jaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44926127000101') $cnpjempresa = 'turismopedregulho';//TODO TEMPORARIO
        if ($cnpjempresa == '57215144000182') $cnpjempresa = 'colinaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22176138000189') $cnpjempresa = 'caetanotur';//TODO TEMPORARIO
        if ($cnpjempresa == '56050478000180') $cnpjempresa = 'mianovichiexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '54220201000123') $cnpjempresa = 'bggturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47633156000173') $cnpjempresa = 'primeturexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '37168505000170') $cnpjempresa = 'aeeviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27225931000144') $cnpjempresa = 'bigbearexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '43310128000164') $cnpjempresa = 'rodribus';//TODO TEMPORARIO
        if ($cnpjempresa == '31364720000160') $cnpjempresa = 'uaitour';//TODO TEMPORARIO
        if ($cnpjempresa == '29798224000172') $cnpjempresa = 'seguetur';//TODO TEMPORARIO
        if ($cnpjempresa == '15163665000193') $cnpjempresa = 'wgexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '53890814000105') $cnpjempresa = 'rpthurturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '36344509000107') $cnpjempresa = 'edinhotur';//TODO TEMPORARIO
        if ($cnpjempresa == '57575788000181') $cnpjempresa = 'conectatour';//TODO TEMPORARIO
        if ($cnpjempresa == '47282508000193') $cnpjempresa = 'maisroteirosviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27755665000161') $cnpjempresa = 'sunset';//TODO TEMPORARIO
        if ($cnpjempresa == '03780520000152') $cnpjempresa = 'samistur';//TODO TEMPORARIO
        if ($cnpjempresa == '31943070000107') $cnpjempresa = 'dsbtur';//TODO TEMPORARIO
        if ($cnpjempresa == '40184621000197') $cnpjempresa = 'constelacaotour';//TODO TEMPORARIO
        if ($cnpjempresa == '42404066000197') $cnpjempresa = 'solartrips';//TODO TEMPORARIO
        if ($cnpjempresa == '45499429000103') $cnpjempresa = 'lumasterturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41865555000183') $cnpjempresa = 'vavdavareturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26026641000109') $cnpjempresa = 'centauroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '05760627000164') $cnpjempresa = 'estrelaguiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34612158000134') $cnpjempresa = 'luviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48385995000182') $cnpjempresa = 'dielyelorenaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47870635000103') $cnpjempresa = 'rotinatrip';//TODO TEMPORARIO
        if ($cnpjempresa == '39448228000184') $cnpjempresa = 'mahila';//TODO TEMPORARIO
        if ($cnpjempresa == '41329070000175') $cnpjempresa = 'alexteleva';//TODO TEMPORARIO
        if ($cnpjempresa == '49427050000149') $cnpjempresa = 'minasgoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '58150252000187') $cnpjempresa = 'zeroonzetour';//TODO TEMPORARIO
        if ($cnpjempresa == '57543888000126') $cnpjempresa = 'nanytrips';//TODO TEMPORARIO
        if ($cnpjempresa == '43731417000137') $cnpjempresa = 'sabinoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '58298825000114') $cnpjempresa = 'bhorizonteviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30875999000183') $cnpjempresa = 'larissaphasseios';//TODO TEMPORARIO
        if ($cnpjempresa == '57426387000160') $cnpjempresa = 'voandoaltoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '53361106000187') $cnpjempresa = 'stevenlindley';//TODO TEMPORARIO
        if ($cnpjempresa == '29165462000140') $cnpjempresa = 'wtorresviagemfamilia';//TODO TEMPORARIO
        if ($cnpjempresa == '25104434000162') $cnpjempresa = 'suetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '61957205000154') $cnpjempresa = 'transgerciturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29751750000187') $cnpjempresa = 'rctt';//TODO TEMPORARIO
        if ($cnpjempresa == '30500686000140') $cnpjempresa = 'deiaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '30025683000100') $cnpjempresa = 'gomestur';//TODO TEMPORARIO

        return $cnpjempresa;
    }

    function index()
    {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('administracao')));
        $meta = array('page_title' => lang('administracao'), 'bc' => $bc);

        $customers = $this->getAllCompanies();

        foreach ($customers as $customer) {

            $cnpj = $customer->vat_no;
            $cg = $this->site->getCustomerGroupByID($customer->customer_group_id);

            $cnpj = str_replace('-', '', $cnpj);
            $cnpj = str_replace('/', '', $cnpj);
            $cnpj = str_replace('.', '', $cnpj);

            $cnpjempresa = $this->getEmpresa($cnpj);

            if ($this->encontrou($cnpjempresa, $cnpj)) {

                $db_config = $this->getConfiguracaoDB($cnpjempresa);

                $db = $this->load->database($db_config, TRUE);

                $configuracaoGeral = $this->get_setting($db);

                $dadosFatura = $this->getDataUltimoVencimentoAberta($customer->id);
                $dataUltimaVenda = $this->getDataultimaVenda($db)->ultimaVenda;
                $totalVendas = $this->getTotalDeVendas($db)->totalVendas;
                $qtdVendas = $this->getQuantidadeVendasAno($db)->qtd;
                $totalUsuarios = $this->getTotalUsuarios($db)->qtd;
                $dataUltimoPagamentoMesCorrente = $this->getDataUltimoVencimentoQuitada($customer->id)->ultimoVencimentoFatura;

                $data[] = array(
                    'name' => $customer->name,
                    'status' => $configuracaoGeral->status,
                    'versao' => $configuracaoGeral->version,
                    'googleAnalytics' => $configuracaoGeral->googleAnalytics,
                    'dataUltimaVenda' => $dataUltimaVenda,
                    'totalVendas' => $totalVendas,
                    'qtdVendas' => $qtdVendas,
                    'totalUsuarios' => $totalUsuarios,
                    'nome_grupo' => $cg->name,
                    'dataUltimoVencimento' =>  $dadosFatura->ultimoVencimentoFatura,
                    'statusUltimoVencimento' => $dadosFatura->status,
                    'dataUltimoPagamentoMesCorrente' => $dataUltimoPagamentoMesCorrente,
                    'cnpj' => $cnpj
                );
            } else {
                $data[] = array(
                    'name' => '(NÃO ENCONTRAMOS) '.$customer->name,
                    'status' => '',
                    'versao' => '',
                    'googleAnalytics' => '',
                    'dataUltimaVenda' => '',
                    'totalVendas' => '',
                    'totalUsuarios' => 0,
                    'nome_grupo' => $cg->name,
                    'dataUltimoVencimento' => '',
                    'statusUltimoVencimento' => 'NAO ENCONTRADO',
                    'dataUltimoPagamentoMesCorrente' => null,
                    'cnpj' => $cnpj
                );
            }
        }

        $this->data['customers'] = $data;

        $this->page_construct('administracao/index', $meta, $this->data);
    }


    function acesso()
    {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('administracao')));
        $meta = array('page_title' => lang('administracao'), 'bc' => $bc);

        $customers = $this->getAllCompanies();

        foreach ($customers as $customer) {
            $cnpj = $customer->vat_no;

            $cnpj = str_replace('-', '', $cnpj);
            $cnpj = str_replace('/', '', $cnpj);
            $cnpj = str_replace('.', '', $cnpj);

            $cnpjempresa = $this->getEmpresa($cnpj);

            if ($this->encontrou($cnpjempresa, $cnpj)) {
                $data[] = array(
                    'name' => $customer->name,
                    'cnpj' => $cnpj
                );
            } else {
                $data[] = array(
                    'name' => '(NÃO ENCONTRAMOS) '.$customer->name,
                    'cnpj' => $cnpj
                );
            }
        }

        $this->data['customers'] = $data;

        $this->page_construct('administracao/acesso', $meta, $this->data);
    }


    public function naoEncontrou($cnpjempresa, $cnpj) {
        return $cnpjempresa == $cnpj;
    }

    public function encontrou($cnpjempresa, $cnpj) {
        return !$this->naoEncontrou($cnpjempresa, $cnpj);
    }

    public function inativar() {

        $cnpj = $this->input->get('cnpj');
        $cnpjempresa = $this->getEmpresa($cnpj);
        $db_config = $this->getConfiguracaoDB($cnpjempresa);

        $db = $this->load->database($db_config, TRUE);
        $configuracaoGeral = $this->get_setting($db);
        $data = array('status' => 0);

        $this->updateStatusSettings($configuracaoGeral->setting_id, $data, $db);
    }

    public function ativar() {

        $cnpj = $this->input->get('cnpj');
        $cnpjempresa = $this->getEmpresa($cnpj);
        $db_config = $this->getConfiguracaoDB($cnpjempresa);

        $db = $this->load->database($db_config, TRUE);

        $configuracaoGeral = $this->get_setting($db);

        $data = array('status' => 1);

        $this->updateStatusSettings($configuracaoGeral->setting_id, $data, $db);
    }

    private function getConfiguracaoDB($cnpjempresa): array
    {
        $db_config = array(
            'dsn'	=> '',
            //'hostname' => 'node83816-sagtur.jelastic.saveincloud.net:12969',
            'hostname' => '10.100.53.149',
            'username' => 'root',
            'password' => 'DOYqah54213',
            'database' => $cnpjempresa,
            'dbdriver' => 'mysqli',
            'dbprefix' => 'sma_',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => TRUE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => FALSE
        );

        return $db_config;
    }

    public function modal_view($cnpj = null)
    {
        $db = $this->getDB($cnpj);
        $configuracaoGeral = $this->get_setting($db);

        $this->data['configuracaoGeral'] = $configuracaoGeral;
        $this->data['nome_empresa'] =  $this->getEmpresa($cnpj);
        $this->data['users'] = $this->getUsers($cnpj);
        $this->load->view($this->theme . 'administracao/modal_view', $this->data);
    }

    public function updateStatusSettings($id, $data = array(), $db = NULL)
    {
        $db->where('setting_id', $id);
        if ($db->update('settings', $data)) {
            return true;
        }
        return false;
    }

    private function get_setting($db) {
        $q = $db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function getUsers($cnpj) {

        $db = $this->getDB($cnpj);

        $db->select("users.group_id as tipo, users.*, companies.*", FALSE);
        $db->join('companies', 'users.biller_id=companies.id');
        $db->where('active', 1);
        $db->where('users.email not in("andre@resultatec.com.br")');
        $db->order_by('name');

        $q = $db->get('users');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;

    }

    public function getDB($cnpj) {
        $cnpjempresa = $this->getEmpresa($cnpj);
        $db_config = $this->getConfiguracaoDB($cnpjempresa);
        $db = $this->load->database($db_config, TRUE);
        return $db;
    }

    public function getDataultimaVenda($db)
    {
        $db->select('max(sma_sales.date) ultimaVenda', false);

        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalDeVendas($db)
    {
        $db->select('sum(sma_sales.grand_total) totalVendas', false);

        $db->where("date_format(date, '%Y') = '".date('Y')."' ");
        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuantidadeVendasAno($db)
    {
        $db->select('count(sma_sales.id) qtd', false);
        $db->where("date_format(date, '%Y') = '".date('Y')."' ");
        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalUsuarios($db)
    {
        $db->select('count(sma_users.id) qtd', false);

        $db->where('active', 1);

        $q = $db->get_where('users', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalAutomoveis($db)
    {
        $db->select('count(sma_automovel.id) qtd', false);

        $q = $db->get_where('automovel', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies() {

        $this->db->order_by('name');

        $q = $this->db->get_where('companies', array('group_name' => 'customer', 'active' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDataUltimoVencimentoAberta($pessoaId)
    {
        $this->db->select('min(sma_fatura.dtvencimento) ultimoVencimentoFatura, max(sma_fatura.status) as status', false);

        $this->db->where('status', 'ABERTA');

        $q = $this->db->get_where('fatura', array('pessoa' => $pessoaId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDataUltimoVencimentoQuitada($pessoaId)
    {
        $this->db->select('max(sma_fatura.dtvencimento) ultimoVencimentoFatura', false);

        $this->db->where('status', 'QUITADA');
        $this->db->where('MONTH(dtvencimento) = MONTH(CURDATE())');
        $this->db->where('YEAR(dtvencimento) = YEAR(CURDATE())');

        $q = $this->db->get_where('fatura', array('pessoa' => $pessoaId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}