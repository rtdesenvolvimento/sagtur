<?php
$v = "&mesAniversario=" . $mesAniversario;
$v .= "&dia=" . $dia;
?>
<style type="text/css" media="screen">
    #CPDData td:nth-child(1) {display: none;}
    #CPDData td:nth-child(2) {width: 15%;}
    #CPDData td:nth-child(5) {text-align: center;}
    #CPDData td:nth-child(6) {text-align: center;}
    #CPDData td:nth-child(7) {text-align: center;}
    #CPDData td:nth-child(8) {text-align: right;}
    #CPDData td:nth-child(9) {text-align: center;}
    #CPDData td:nth-child(10) {width: 25%;}
</style>

<script>
    $(document).ready(function () {
        var oTable = $('#CPDData').dataTable({
            "aaSorting": [[0, "asc"], [1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= site_url('reports/getProductsByCustomers?v=1') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "dias_ultima_compra", "value":  $('#dias_ultima_compra').val() });
                aoData.push({ "name": "product", "value":  $('#product').val() });
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "birthday_of", "value":  $('#birthday_of').val() });
                aoData.push({ "name": "birthday_from", "value":  $('#birthday_from').val() });
            },
            "aoColumns": [
                null,
                null,
                null,
                null,
                {"mRender": fsd},
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": fld},
                null,
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

                var purchases = 0, total = 0;
                for (var i = 0; i < aaData.length; i++) {
                    purchases += parseFloat(aaData[aiDisplay[i]][6]);
                    total += parseFloat(aaData[aiDisplay[i]][7]);
                }

                var nCells = nRow.getElementsByTagName('th');
                nCells[6].innerHTML = parseInt(purchases);
                nCells[7].innerHTML = currencyFormat(parseFloat(total));


            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-heart"></i><?= lang('reports_products_by_customers'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>"><i class="icon fa fa-file-pdf-o"></i></a>
                </li>
                <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                        <i class="icon fa fa-file-excel-o"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?php echo form_open("reports/products_by_customers/"); ?>
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("dias_ultima_compra", "dias_ultima_compra") ?>
                        <?php
                        $cbDiasUltimaCompra = array(
                            '' => lang('select'),
                            '91' => lang('3 meses'),
                            '182' => lang('6 meses'),
                            '273' => lang('9 meses'),
                            '365' => lang('1 ano'),
                            '456' => lang('1 ano e 3 meses'),
                            '547' => lang('1 ano e 6 meses'),
                            '638' => lang('1 ano e 9 meses'),
                            '730' => lang('2 anos'),
                            '821' => lang('2 anos e 3 meses'),
                            '912' => lang('2 anos e 6 meses'),
                            '1003' => lang('2 anos e 9 meses'),
                            '1095' => lang('3 anos'),
                            '1186' => lang('3 anos e 3 meses'),
                            '1277' => lang('3 anos e 6 meses'),
                            '1368' => lang('3 anos e 9 meses'),
                            '1460' => lang('4 anos'),
                        );
                        echo form_dropdown('dias_ultima_compra', $cbDiasUltimaCompra,'', 'class="form-control" id="dias_ultima_compra"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("product", "product") ?>
                        <?php
                        $prod[''] =  lang("select") . " " . lang("product") ;
                        foreach ($products as $produts) {
                            $prod[$produts->id] = $produts->name;
                        }
                        echo form_dropdown('product', $prod, '', 'class="form-control select" id="product" placeholder="' . lang("select") . " " . lang("product") . '"style="width:100%"')
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("start_date_trip", "start_date"); ?>
                        <?php echo form_input('start_date', '', 'class="form-control" id="start_date"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("end_date_trip", "end_date"); ?>
                        <?php echo form_input('end_date', '', 'type="date" class="form-control" id="end_date"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?= lang('birthday_of', 'birthday_of'); ?>
                        <?= form_input('birthday_of', '', 'class="form-control tip mask_integer" id="birthday_of"'); ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?= lang('birthday_from', 'birthday_from'); ?>
                        <?= form_input('birthday_from', '', 'class="form-control tip mask_integer" id="birthday_from"'); ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="CPDData" cellpadding="0" cellspacing="0" border="0"
                               class="table table-bordered table-condensed table-hover table-striped reports-table">
                            <thead>
                            <tr class="primary">
                                <th style="display: none;"></th>
                                <th style="text-align: left;"><?= lang("name"); ?></th>
                                <th style="text-align: left;"><?= lang("phone"); ?></th>
                                <th style="text-align: left;"><?= lang("email_address"); ?></th>
                                <th style="text-align: center;"><?= lang("nasc"); ?></th>
                                <th style="text-align: center;"><i class="fa fa-birthday-cake"></i></th>
                                <th><?= lang("qtd"); ?></th>
                                <th style="text-align: center;"><?= lang("total_amount"); ?></th>
                                <th style="text-align: center;"><?= lang("last_purchase_date"); ?></th>
                                <th style="text-align: left;"><?= lang("product_names"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="display: none;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align: center;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue"><i
                                        class="fa-fw fa fa-bar-chart-o"></i><?= '25 '.lang('popular_destinations');?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="bschart" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue"><i
                                        class="fa-fw fa fa-bar-chart-o"></i><?=lang('popular_destinations_group_date');?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="bschartgropdate" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('total_por_idade'); ?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="pdReportByIdade" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('total_por_sexo'); ?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="pdReportBySexo" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_divulgacao'); ?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="pdMeioDivulgacaoMes" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_by_category'); ?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="pdCategoriaMes" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_biller');?>
                            </h2>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="vVendedor" style="width:100%; height:450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script src="<?= $assets; ?>js/hc/highcharts.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();

            var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
            var product = $('#product').val()  != '' ? $('#product').val() : '0';
            var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
            var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
            var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
            var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

            window.location.href = "<?=site_url('reports/getProductsByCustomers/pdf/0')?>/"+dias_ultima_compra+'/'+product+'/'+start_date+'/'+end_date+'/'+birthday_of+'/'+birthday_from;
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();

            var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
            var product = $('#product').val()  != '' ? $('#product').val() : '0';
            var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
            var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
            var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
            var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

            window.location.href = "<?=site_url('reports/getProductsByCustomers/0/xls')?>/"+dias_ultima_compra+'/'+product+'/'+start_date+'/'+end_date+'/'+birthday_of+'/'+birthday_from;

            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });

        $('#dias_ultima_compra').change(function(event){
            atualizarTabela();
        });

        $('#product').change(function(event){
            atualizarTabela();
        });

        $('#start_date').change(function(event){
            atualizarTabela();
        });

        $('#end_date').change(function(event){
            atualizarTabela();
        });

        $('#birthday_of').change(function(event){
            atualizarTabela();
        });

        $('#birthday_from').change(function(event){
            atualizarTabela();
        });

        getProductsByCustomersGroupByIdade();
        getProductsByCustomersGroupBySex();
        getBestSeller();
        getVendasPorMeioDivulgacao();
        getVendasPorCategoria();
        getVendasPorVendedor();
        getBestSellerGroupDate();
    });

    function atualizarTabela() {
        $('#CPDData').DataTable().fnClearTable();

        getProductsByCustomersGroupByIdade();
        getProductsByCustomersGroupBySex();
        getBestSeller();
        getVendasPorMeioDivulgacao();
        getVendasPorCategoria();
        getVendasPorVendedor();
        getBestSellerGroupDate();
    }

    function getProductsByCustomersGroupBySex() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getProductsByCustomersGroupBySex",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let sexos = [];
                let total = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        sexos.push(datas[i].sexo);
                        total.push(parseInt(datas[i].total_ocorrencias));
                    }
                }

                let chartData = sexos.map(function(sexo, index) {
                    return {
                        name: sexo,
                        y: total[index]
                    };
                });

                $('#pdReportBySexo').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {text: 'Total por Sexo'},
                    credits: {enabled: false},
                    tooltip: {
                        formatter: function () {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + this.y + '</strong> (' + formatNumber(this.percentage) + '%)';
                        },
                        followPointer: true,
                        useHTML: true,
                        borderWidth: 0,
                        shadow: false,
                        valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + this.y + '</b></h3>';
                                },
                                useHTML: true
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo $this->lang->line("stock_value"); ?>',
                        data: chartData,
                    }]
                });

            }

        });
    }

    function getProductsByCustomersGroupByIdade() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getProductsByCustomersGroupByIdade",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let idades = [];
                let total = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        idades.push(parseInt(datas[i].idade));
                        total.push(parseInt(datas[i].total_ocorrencias));
                    }
                }

                let chartData = idades.map(function(idade, index) {
                    return {
                        name: idade + ' anos',
                        y: total[index]
                    };
                });

                $('#pdReportByIdade').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {text: 'Total por Idade'},
                    credits: {enabled: false},
                    tooltip: {
                        formatter: function () {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + this.y + '</strong> (' + formatNumber(this.percentage) + '%)';
                        },
                        followPointer: true,
                        useHTML: true,
                        borderWidth: 0,
                        shadow: false,
                        valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b></h3>';
                                },
                                useHTML: true
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo $this->lang->line("stock_value"); ?>',
                        data: chartData,
                    }]
                });

            }

        });
    }

    function getBestSellerGroupDate() {


        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getBestSellerGroupByDate",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (data) {

                const dates = [...new Set(data.map(item => item.YearMonth))];
                const products = [...new Set(data.map(item => item.name))];

                const seriesData = {};
                products.forEach(product => {
                    seriesData[product] = [];
                    dates.forEach(date => {
                        const dataPoint = data.find(item => item.name === product && item.YearMonth === date);
                        const yValue = dataPoint ? parseFloat(dataPoint.SoldQty) : 0;
                        seriesData[product].push({ name: Highcharts.dateFormat('%b/%Y', new Date(date)), y: yValue });
                    });
                });

                const series = Object.keys(seriesData).map(name => ({ name, data: seriesData[name] }));

                $('#bschartgropdate').highcharts({
                    chart: {type: 'column'},
                    title: {text: 'Pacotes Mais Vendidos por Mês'},
                    credits: {enabled: false},
                    legend: {enabled: false},
                    xAxis: {
                        categories: dates.map(date => Highcharts.dateFormat('%b/%Y', new Date(date))),
                        labels: {
                            rotation: -45,
                            align: 'right'
                        },
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Quantidade Vendida'
                        }
                    },
                    series
                });

            }

        });
    }

    function getBestSeller() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getBestSeller",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let produtos = [];
                let vendas = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        produtos.push(datas[i].name);
                        vendas.push(parseFloat(datas[i].SoldQty));
                    }
                }

                let chartData = produtos.map(function(produto, index) {
                    return {
                        name: produto,
                        y: vendas[index]
                    };
                });

                $('#bschart').highcharts({
                    chart: {type: 'column'},
                    title: {text: ''},
                    credits: {enabled: false},
                    xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                    yAxis: {min: 0, title: {text: ''}},
                    legend: {enabled: false},
                    series: [{
                        name: '<?=lang('sold');?>',
                        data: chartData,
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#000',
                            align: 'right',
                            y: -25,
                            style: {fontSize: '12px'}
                        }
                    }]
                });
            }
        });
    }

    function getVendasPorMeioDivulgacao() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getVendasPorMeioDivulgacao",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let meios = [];
                let vendas = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        meios.push(datas[i].name);
                        vendas.push(parseFloat(datas[i].subtotal));
                    }
                }

                let chartData = meios.map(function(meio, index) {
                    return {
                        name: meio,
                        y: vendas[index]
                    };
                });

                $('#pdMeioDivulgacaoMes').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {text: ''},
                    credits: {enabled: false},
                    tooltip: {
                        formatter: function () {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        },
                        followPointer: true,
                        useHTML: true,
                        borderWidth: 0,
                        shadow: false,
                        valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                                },
                                useHTML: true
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo $this->lang->line("stock_value"); ?>',
                        data: chartData,
                    }]
                });

            }
        });
    }

    function getVendasPorCategoria() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getVendasPorCategoria",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let categorias = [];
                let vendas = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        categorias.push(datas[i].name);
                        vendas.push(parseFloat(datas[i].subtotal));
                    }
                }

                let chartData = categorias.map(function(categoria, index) {
                    return {
                        name: categoria,
                        y: vendas[index]
                    };
                });

                $('#pdCategoriaMes').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {text: ''},
                    credits: {enabled: false},
                    tooltip: {
                        formatter: function () {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        },
                        followPointer: true,
                        useHTML: true,
                        borderWidth: 0,
                        shadow: false,
                        valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                                },
                                useHTML: true
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo $this->lang->line("stock_value"); ?>',
                        data: chartData,
                    }]
                });

            }
        });
    }


    function getVendasPorVendedor() {

        var dias_ultima_compra = $('#dias_ultima_compra').val()  != '' ? $('#dias_ultima_compra').val() : '0';
        var product = $('#product').val()  != '' ? $('#product').val() : '0';
        var start_date = $('#start_date').val()  != '' ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val()  != '' ? $('#end_date').val() : '0';
        var birthday_of = $('#birthday_of').val()  != '' ? $('#birthday_of').val() : '0';
        var birthday_from = $('#birthday_from').val()  != '' ? $('#birthday_from').val() : '0';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>reports/getVendasPorVendedor",
            data: {
                dias_ultima_compra: dias_ultima_compra,
                product: product,
                start_date: start_date,
                end_date: end_date,
                birthday_of: birthday_of,
                birthday_from: birthday_from,
            },
            dataType: 'json',
            success: function (datas) {

                let vendedores = [];
                let vendas = [];

                if (datas != null) {
                    for (let i=0;i<datas.length;i++) {
                        vendedores.push(datas[i].name);
                        vendas.push(parseFloat(datas[i].subtotal));
                    }
                }

                let chartData = vendedores.map(function(vendedor, index) {
                    return {
                        name: vendedor,
                        y: vendas[index]
                    };
                });

                // Ordenar a lista pelo valor das vendas (campo 'y') em ordem decrescente
                chartData.sort(function(a, b) {
                    return b.y - a.y;
                });

                $('#vVendedor').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {text: ''},
                    credits: {enabled: false},
                    tooltip: {
                        formatter: function () {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        },
                        followPointer: true,
                        useHTML: true,
                        borderWidth: 0,
                        shadow: false,
                        valueDecimals: site.settings.decimals,
                        style: {fontSize: '12px', padding: '0', color: '#000000'}
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return '<h3 style="margin:-15px 0 0 0;font-size: 12px;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                                },
                                useHTML: true
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo $this->lang->line("stock_value"); ?>',
                        data: chartData,
                    }]
                });

            }
        });
    }


</script>