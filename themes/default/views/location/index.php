<script>

    function show_status(status) {

        let name = status.split('@')[0];
        let cor = status.split('@')[1];

        return '<div class="text-center" style="text-transform: uppercase;"><span class="label" style="background-color:' + cor + '">' + name + '</span></div>';
    }

    $(document).ready(function () {
        $('#TBLocations').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('location/getLocations') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "fl_data_retirada", "value":  $('#fl_data_retirada').val() });
                aoData.push({ "name": "fl_data_retirada_ate", "value":  $('#fl_data_retirada_ate').val() });
                aoData.push({ "name": "fl_hora_retirada_de", "value":  $('#fl_hora_retirada_de').val() });
                aoData.push({ "name": "fl_hora_retirada_ate", "value":  $('#fl_hora_retirada_ate').val() });
                aoData.push({ "name": "fl_data_devolucao_de", "value":  $('#fl_data_devolucao_de').val() });
                aoData.push({ "name": "fl_data_devolucao_ate", "value":  $('#fl_data_devolucao_ate').val() });
                aoData.push({ "name": "fl_hora_devolucao_de", "value":  $('#fl_hora_devolucao_de').val() });
                aoData.push({ "name": "fl_hora_devolucao_ate", "value":  $('#fl_hora_devolucao_ate').val() });

                aoData.push({ "name": "fl_tipo_contrato", "value":  $('#fl_tipo_contrato').val() });
                aoData.push({ "name": "fl_tipo_veiculo", "value":  $('#fl_tipo_veiculo').val() });
                aoData.push({ "name": "fl_veiculo", "value":  $('#fl_veiculo').val() });
                aoData.push({ "name": "fl_status_disponibilidade", "value":  $('#fl_status_disponibilidade').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"bSortable": false, "mRender": img_hl},
                null,
                null,
                null,
                null,
                {"mRender": show_status},
                null,
                {"mRender": fld},
                {"mRender": fld},
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][11]);
                    paid += parseFloat(aaData[aiDisplay[i]][12]);
                    balance += parseFloat(aaData[aiDisplay[i]][13]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[11].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[12].innerHTML = currencyFormat(parseFloat(paid));
                nCells[13].innerHTML = currencyFormat(parseFloat(balance));
            }
        });
    });
</script>
<?= form_open('financeiro/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-car"></i><?= lang('locations'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('location/add')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_location')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                    <div style="margin-bottom: 20px;" class="divfilters">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("data_retidada_de", "fl_data_retirada_de"); ?>
                                <?php echo form_input('fl_data_retirada', (isset($_POST['fl_data_retirada']) ? $_POST['fl_data_retirada'] : $fl_data_retirada), 'class="form-control" id="fl_data_retirada"', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("data_retirada_ate", "fl_data_retirada_ate"); ?>
                                <?php echo form_input('fl_data_retirada_ate', (isset($_POST['fl_data_retirada_ate']) ? $_POST['fl_data_retirada_ate'] : $fl_data_retirada_ate), 'class="form-control" id="fl_data_retirada_ate"', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <?= lang("hora_retidada_de", "fl_hora_retirada_de"); ?>
                                <?php echo form_input('fl_hora_retirada_de', (isset($_POST['fl_hora_retirada_de']) ? $_POST['fl_hora_retirada_de'] : $fl_hora_retirada_de), 'class="form-control" id="fl_hora_retirada_de"', 'time'); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <?= lang("hora_retirada_ate", "fl_hora_retirada_ate"); ?>
                                <?php echo form_input('fl_hora_retirada_ate', (isset($_POST['fl_hora_retirada_ate']) ? $_POST['fl_hora_retirada_ate'] : $fl_hora_retirada_ate), 'type="date" class="form-control" id="fl_hora_retirada_ate"', 'time'); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("data_devolucao_de", "fl_data_devolucao_de"); ?>
                                <?php echo form_input('fl_data_devolucao_de', (isset($_POST['fl_data_devolucao_de']) ? $_POST['fl_data_devolucao_de'] : $fl_data_devolucao_de), 'class="form-control" id="fl_data_devolucao_de"', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("data_devolucao_ate", "fl_data_devolucao_ate"); ?>
                                <?php echo form_input('fl_data_devolucao_ate', (isset($_POST['fl_data_devolucao_ate']) ? $_POST['fl_data_devolucao_ate'] : $fl_data_devolucao_ate), 'class="form-control" id="fl_data_devolucao_ate"', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <?= lang("hora_devolucao_de", "fl_hora_devolucao_de"); ?>
                                <?php echo form_input('fl_hora_devolucao_de', (isset($_POST['fl_hora_devolucao_de']) ? $_POST['fl_hora_devolucao_de'] : $fl_hora_devolucao_de), 'class="form-control" id="fl_hora_devolucao_de"', 'time'); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <?= lang("hora_devolucao_ate", "fl_hora_devolucao_ate"); ?>
                                <?php echo form_input('fl_hora_devolucao_ate', (isset($_POST['fl_hora_devolucao_ate']) ? $_POST['fl_hora_devolucao_ate'] : $fl_hora_devolucao_ate), 'type="date" class="form-control" id="fl_hora_devolucao_ate"', 'time'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <?= lang("tipo_contrato", "lctipocontrato") ?>
                            <?php
                            $cbTipoContratoLocacao[''] = lang("select") . " " . lang("tipo_contrato") ;
                            foreach ($tiposContrato as $tipoContrato) {
                                $cbTipoContratoLocacao[$tipoContrato->id] = $tipoContrato->name;
                            }
                            echo form_dropdown('fl_tipo_contrato', $cbTipoContratoLocacao, '', 'class="form-control select" id="fl_tipo_contrato" placeholder="' . lang("select") . " " . lang("tipo_contrato") . '"required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <?= lang("tipo_veiculo", "tipo_veiculo") ?>
                            <?php
                            $cbTiposVeiculo[''] = lang("select") . " " . lang("tipo_veiculo") ;
                            foreach ($tiposVeiculo as $tipoVeiculo) {
                                $cbTiposVeiculo[$tipoVeiculo->id] = $tipoVeiculo->name;
                            }
                            echo form_dropdown('fl_tipo_veiculo', $cbTiposVeiculo, '', 'class="form-control select" id="fl_tipo_veiculo" placeholder="' . lang("select") . " " . lang("tipo_contrato") . ' style="width:100%"')
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("veiculo", "fl_veiculo"); ?>
                                <?php echo form_input('fl_veiculo', (isset($_POST['fl_veiculo']) ? $_POST['fl_veiculo'] : ""), 'id="fl_veiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '"class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("status_disponibilidade", "fl_status_disponibilidade"); ?>
                                <?php
                                $cbStatusDisponibilidade[''] = lang("select") . " " . lang("status_disponibilidade") ;
                                foreach ($statusDisponibilidade as $stDisponibilidade) {
                                    $cbStatusDisponibilidade[$stDisponibilidade->id] = $stDisponibilidade->name;
                                }
                                echo form_dropdown('fl_status_disponibilidade', $cbStatusDisponibilidade, '', 'class="form-control select" id="fl_status_disponibilidade" placeholder="' . lang("select") . " " . lang("status_disponibilidade") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table id="TBLocations" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("ck"); ?></th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th><?= $this->lang->line("reference_no"); ?></th>
                            <th><?= $this->lang->line("grupo"); ?></th>
                            <th><?= $this->lang->line("veiculo"); ?></th>
                            <th><?= $this->lang->line("customer"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th><?= $this->lang->line("tipo"); ?></th>
                            <th><?= $this->lang->line("data_retirada"); ?></th>
                            <th><?= $this->lang->line("data_devolucao"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th><?= $this->lang->line("grand_total"); ?></th>
                            <th><?= $this->lang->line("paid"); ?></th>
                            <th><?= $this->lang->line("balance"); ?></th>
                            <th><?= $this->lang->line("situacao"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="16" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('#fl_data_retirada').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_data_retirada_ate').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_hora_retirada_de').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_hora_retirada_ate').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_data_devolucao_de').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_data_devolucao_ate').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_hora_devolucao_de').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_hora_devolucao_ate').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_tipo_contrato').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_tipo_veiculo').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_veiculo').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });
        $('#fl_status_disponibilidade').change(function(event){
            $('#TBLocations').DataTable().fnClearTable()
        });

        $('#fl_veiculo').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "vehicle/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

    });
</script>

