<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CobrancaDTO_model extends CI_Model
{
    public $fatura;
    public $dataVencimento;
    public $descricaoCobranca;
    public $valorVencimento;
    public $numeroParcelas;
    public $tipoCobranca;
    public $pessoa;
    public $instrucoes;


    //fied cc
    public $cardName;
    public $cardNumber;
    public $cardExpiryMonth;
    public $cardExpiryYear;
    public $cvc;

    public $senderHash;
    public $deviceId;
    public $creditCardToken;

    public $cpfTitularCartao;
    public $celularTitularCartao;
    public $cepTitularCartao;
    public $enderecoTitularCartao;
    public $numeroEnderecoTitularCartao;
    public $complementoEnderecoTitularCartao;
    public $bairroTitularCartao;
    public $cidadeTitularCartao;
    public $estadoTitularCartao;

    //pagseguro cc
    public $totalParcelaPagSeguro;

    public $venda_manual;
    public $processa_pagamento_link = false;

    public function __construct() {
        parent::__construct();
    }
}
