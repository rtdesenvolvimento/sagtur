<?php
class Migrate extends CI_Controller {


    public function __construct()
    {
        parent::__construct();

        if ($this->input->get('token') != '') {
            $this->session->set_userdata(array('cnpjempresa' => $this->input->get('token')));
        } else {
            if ($this->session->userdata('cnpjempresa') == '') {
                $this->session->set_userdata(array('cnpjempresa' => $this->uri->segment(2)));
            }
        }
    }

    public function index()
    {
        // load migration library
        $this->load->library('migration');

        $token  = $this->input->get('token');


        if ( ! $this->migration->current($token))
        {
            echo '<pre>Error' . $this->migration->error_string(). ' </pre>';
        } else {
            echo '<pre>Migrations ran successfully!'.$this->session->userdata('cnpjempresa').'</pre>';
        }
    }

    public function current_all()
    {
        $database_padrao = 'demonstracaosagtur';
        $otherdb = $this->load->database($database_padrao, TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();
        $this->current($database_padrao);

        foreach ($dbs as $db_name)
        {
            if ($this->isTableIgnore($db_name)) {
                $this->current($db_name);
            }
        }
    }

    public function current($db) {

        echo 'database.'.$db;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, base_url().'migrate?token='.$db);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        echo $output;
    }

    private function isTableIgnore($db): bool
    {
        return  ($db != 'information_schema' && $db != 'mysql' && $db != 'performance_schema' && $db != 'phpmyadmin' && $db != 'sys' && $db != '_session_database' && $db != 'sagtur_administrativo');
    }

}