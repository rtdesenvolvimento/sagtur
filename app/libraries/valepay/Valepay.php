<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Valepay {

    const VERSION = "v1";

    //public $base_url = 'https://prod_payments.valepaybank.com.br';
    public $base_url = 'https://api.valepaybank.com.br/payments';//NOVA INFRA

    public $base_url_sandbox = 'https://hom-payments.valepay.dev.br';

    public $sandbox = true;
    public $key_public;
    public $key_private;

    public $company_uuid;

    public $user_uuid;

    private $token;

    public function __construct() {}

    public function simulation_installments_by_customer($emmiter = array()) {
        $this->authenticate();

        $emmiter['company_uuid'] = $this->company_uuid;

        return $this->post('/api/product/calculate/emitter', $emmiter);
    }

    public function simulation_installments_by_store($emmiter = array()) {
        $this->authenticate();

        $emmiter['company_uuid'] = $this->company_uuid;

        return $this->post('/api/product/calculate/store', $emmiter);
    }

    public function simulation_rav($rav) {
        $this->authenticate();

        $rav['company_uuid'] = $this->company_uuid;

        return $this->post('/api/product/calculate/rav', $rav);
    }

    public function transaction_start($transaction) {
        $this->authenticate();

        $transaction['company_uuid'] = $this->company_uuid;

        return $this->post('/api/transaction/start', $transaction);
    }


    public function create_pix($dinamyc_qrcode) {
        $this->authenticate();

        $dinamyc_qrcode['company_uuid'] = $this->company_uuid;
        $dinamyc_qrcode['user_uuid'] = $this->user_uuid;

        return $this->post('/api/pix/dynamic-qrcode', $dinamyc_qrcode);
    }

    public function transaction_cancel($transaction_uuid) {
        $this->authenticate();

        return $this->post('/api/transaction/cancel/payment', array('transaction_uuid' => $transaction_uuid));
    }

    public function send_link($transaction_uuid) {
        $this->authenticate();

        return $this->post('/api/transaction/send-link/'.$transaction_uuid, array());
    }

    public function create_client_transaction($client) {
        $this->authenticate();

        return $this->post('/api/transaction/customer', $client);
    }

    public function tokenizing_credit_card($token_card) {
        $this->authenticate();

        return $this->post('/api/transaction/tokenize/card', $token_card);
    }

    public function payment_credit_card($payment) {
        $this->authenticate();

        return $this->post('/api/transaction/payment', $payment);
    }

    private function authenticate() {

        if ($this->token != null) return;

        $response = $this->post('/api/authenticate', array());

        if ($response->status == 'success')  {
            $this->token = $response->data->token->access_token;
            $this->company_uuid = $response->data->company->uuid;
            $this->user_uuid = $response->data->user->uuid;
        }

    }

    private function post($url, $params)
    {
        $params = json_encode($params);
        $ch = curl_init();

        $url_api = ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'. Valepay::VERSION . $url;

        curl_setopt($ch, CURLOPT_URL, $url_api);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        if ($this->token) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->token,
            ));
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "public:". $this->key_public,
                "private:". $this->key_private,
            ));
        }

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->status = 'error';
            $response->message = 'Tivemos um problema ao processar a requisição.';
        }
        return $response;
    }
}