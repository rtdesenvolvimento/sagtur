<?php defined('BASEPATH') or exit('No direct script access allowed');

class Messagegroups extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('messages', $this->Settings->user_language);

        $this->load->model('reports_model');

        //service
        $this->load->model('service/SendMessageService_model', 'SendMessageService_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //dto
        $this->load->model('dto/VendaFilterDTO_model', 'VendaFilterDTO_model');
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        //repository
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/MessageGroupRepository_model', 'MessageGroupRepository_model');

        //model
        $this->load->model('service/MessageGroupService_model', 'MessageGroupService_model');

        $this->load->library('form_validation');
    }

    public function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('message_groups')));
        $meta = array('page_title' => lang('message_groups'), 'bc' => $bc);

        $this->page_construct('message_groups/index', $meta, $this->data);
    }

    function getGroups()
    {

        $groups = $this->MessageGroupRepository_model->getAll();

        foreach ($groups as $group) {
            $this->updateGroupParticipants($group->id);
        }

        $this->load->library('datatables');
        $this->datatables
            ->select("
            id, 
            name,
            total_customers,
            imported_google,
            imported_whatsapp,
            total_customers_whatsapp,
            (total_customers - total_customers_whatsapp) as remaining_customers
            ")
            ->from("message_groups");

        $this->datatables->where("{$this->db->dbprefix('message_groups')}.active ", $this->input->post('status') );

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('messagegroups/participants/$1') . "' title='" . lang("message_group_participants") . "'><i class=\"fa fa-users\"></i></a> <a href='" . site_url('messagegroups/editGroup/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_group") . "'><i class=\"fa fa-edit\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function participants($groupID, $mostrar_vendas_canceladas = false)
    {

        $this->updateGroupParticipants($groupID);

        $group = $this->MessageGroupRepository_model->getById($groupID);

        if ($group->programacao_id == null) {
            $this->session->set_flashdata('error', lang('message_group_not_associated'));
            redirect('messagegroups');
        }

        $vendaFilter = new VendaFilterDTO_model();
        $vendaFilter->setProgramacaoId($group->programacao_id);

        if (!$mostrar_vendas_canceladas) {
            $vendaFilter->sale_status = '(sale_status = "faturada")';
        } else {
            $vendaFilter->sale_status = '(sale_status = "faturada" or sale_status = "cancel")';
        }

        $vendaFilter->order_by = true;

        if ($this->input->post('status_pagamento') != null ) {
            $vendaFilter->setStatusPagamento($this->input->post('status_pagamento'));
        }

        if ($this->input->post('cliente') != null ) {
            $vendaFilter->setClienteId($this->input->post('cliente'));
        }

        $this->data['sales'] = $this->reports_model->getSalesProgramacao($vendaFilter);
        $this->data['admins'] = $this->MessageGroupRepository_model->admins($groupID);
        $this->data['mostrar_vendas_canceladas'] = $mostrar_vendas_canceladas;

        $this->data['group'] = $group;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('message_group_participants')));
        $meta = array('page_title' => lang('message_group_participants'), 'bc' => $bc);

        $this->page_construct('message_groups/participants', $meta, $this->data);
    }

    public function addParticipantGroup($groupID, $ddi = '55', $phone = '')
    {
        try {

            if ($ddi == '') {
                $this->session->set_flashdata('error', lang('ddi_required'));
                redirect('messagegroups/participants/' . $groupID);
            }

            if ($phone == '') {
                $this->session->set_flashdata('error', lang('phone_required'));
                redirect('messagegroups/participants/' . $groupID);
            }

            if ($this->SendMessageService_model->add_participant_group($groupID, [$ddi.$phone])) {
                $this->session->set_flashdata('success', lang('participant_added'));
                redirect('messagegroups/participants/' . $groupID);
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function addParticipantAdminGroup($groupID, $phone = '')
    {
        try {

            if ($phone == '') {
                $this->session->set_flashdata('error', lang('phone_required'));
                redirect('messagegroups/participants/' . $groupID);
            }

            if ($this->SendMessageService_model->add_participant_group($groupID, [$phone])) {

                $this->SendMessageService_model->add_admin_group($groupID, [$phone]);

                $this->session->set_flashdata('success', lang('participant_admin_added'));

                redirect('messagegroups/participants/' . $groupID);
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function removeParticipantGroup($participantID)
    {
        try {

            $participant = $this->MessageGroupRepository_model->getParticipantByID($participantID);

            if ($this->SendMessageService_model->remove_participant_group($participant->message_group_id, [$participant->phone])) {
                $this->session->set_flashdata('success', lang('participant_removed'));
                redirect('messagegroups/participants/' . $participant->message_group_id);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function addGroup()
    {

        $this->form_validation->set_rules('programacaoId', lang("programacaoId"), 'required');
        $this->form_validation->set_rules('phone_admin', lang("phone_admin"), 'required');
        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run() == true) {
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));
            $product = $this->site->getProductByID($programacao->produto);
            $nomeGroup = $product->name.' SAÍDA '.$this->sma->hrsd($programacao->dataSaida);
            $name = $this->input->post('name');
        }

        if ($this->form_validation->run()) {
            try {

                $verificaGroup = $this->MessageGroupRepository_model->getGroupByProgramacao($programacao->id);

                if ($verificaGroup) {
                    $this->session->set_flashdata('error', lang('group_already_created'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($groupID = $this->SendMessageService_model->create_group($nomeGroup, $name, [$this->input->post('phone_admin')], $programacao->id)) {

                    $this->SendMessageService_model->add_admin_group($groupID, [$this->input->post('phone_admin')]);

                    $this->session->set_flashdata('success', lang('group_created_successfully'));
                    redirect('messagegroups/participants/' . $groupID);
                }
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $biller = $this->site->getCompanyByID($this->Settings->default_biller);

            $ddi = '55';

            $phone = str_replace('(', '', str_replace(')', '', $biller->phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($phone);

            $filter = new ProgramacaoFilter_DTO_model();
            $filter->group_data_website = $this->Settings->receptive;
            $filter->enviar_site = false;
            $filter->apenas_agendas_ativa = false;
            $filter->verificarConfiguracaoOcultarProdutosSemEstoque = false;

            $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['programacoes'] = $programacoes;
            $this->data['phone_admin']  = $ddi.$phone;
            $this->data['name']         = $biller->name;

            $this->load->view($this->theme . 'message_groups/addGroup', $this->data);
        }
    }

    public function addAdmin($id)
    {

        $this->form_validation->set_rules('phone_admin', lang("phone_admin"), 'required');
        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run()) {
            try {
                if ($this->SendMessageService_model->addAdmin($id, $this->input->post('name'), $this->input->post('phone_admin'))){
                    $this->session->set_flashdata('success', lang('add_admin_successfully'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $biller = $this->site->getCompanyByID($this->Settings->default_biller);

            $ddi = '55';

            $phone = str_replace('(', '', str_replace(')', '', $biller->phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($phone);

            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['phone_admin']  = $ddi.$phone;
            $this->data['name']         = $biller->name;
            $this->data['group']        = $this->MessageGroupRepository_model->getById($id);

            $this->load->view($this->theme . 'message_groups/addAdmin', $this->data);
        }
    }

    public function addMember($id)
    {
        $this->form_validation->set_rules('phone', lang("phone_admin"), 'required');

        if ($this->form_validation->run()) {
            try {
                if ($this->SendMessageService_model->add_participant_group($id, [$this->input->post('phone')])) {
                    $this->session->set_flashdata('success', lang('add_member_successfully'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['group']        = $this->MessageGroupRepository_model->getById($id);
            $this->load->view($this->theme . 'message_groups/addMember', $this->data);
        }
    }


    public function editGroup($id)
    {

        $this->form_validation->set_rules('active', lang("active"), 'required');

        if ($this->form_validation->run() == true) {

            $active = $this->input->post('active');

            $data = array(
                'active' => $active
            );
        }

        if ($this->form_validation->run() && $this->MessageGroupRepository_model->editGroup($id, $data))  {

            if (!$active && $this->input->post('remover_todos')) {
                $this->SendMessageService_model->remove_all_participants($id);
                $this->session->set_flashdata('success', lang('participants_removed'));
            } else {
                $this->session->set_flashdata('success', lang('group_edited_successfully'));
            }

            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['modal_js']    = $this->site->modal_js();
            $this->data['group'] = $this->MessageGroupRepository_model->getById($id);

            $this->load->view($this->theme . 'message_groups/editGroup', $this->data);
        }
    }

    public function addAllParticipants($groupID)
    {
        try {

            $group = $this->MessageGroupRepository_model->getById($groupID);

            $vendaFilter = new VendaFilterDTO_model();
            $vendaFilter->setProgramacaoId($group->programacao_id);

            $sales  = $this->reports_model->getSalesProgramacao($vendaFilter);
            $admins = $this->MessageGroupRepository_model->admins($groupID);

            $this->SendMessageService_model->add_all_participants($groupID, $sales, $admins);

            $this->session->set_flashdata('success', lang('peoplesadded'));
            redirect('messagegroups/participants/' . $groupID);
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect('messagegroups');
        }

    }

    public function groupMetadata($groupID)
    {
        try {
            $this->SendMessageService_model->group_metadata($groupID);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateGroupParticipants($groupID)
    {
        try {

            $group = $this->MessageGroupRepository_model->getById($groupID);

            $vendaFilter = new VendaFilterDTO_model();
            $vendaFilter->setProgramacaoId($group->programacao_id);

            $sales  = $this->reports_model->getSalesProgramacao($vendaFilter);
            $admins = $this->MessageGroupRepository_model->admins($groupID);

            $this->SendMessageService_model->update_group_participants($groupID, $sales, $admins);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function queue()
    {
        try {
            $this->SendMessageService_model->queue();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getContact($phone)
    {
        try {
            print_r($this->SendMessageService_model->get_contact($phone));
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    public function enviar_convite($participantID)
    {
        try {

            $this->SendMessageService_model->enviar_convite($participantID);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}