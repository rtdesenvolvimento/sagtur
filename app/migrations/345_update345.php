<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update345 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.22.09.01.1'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'assinatura' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => null ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
