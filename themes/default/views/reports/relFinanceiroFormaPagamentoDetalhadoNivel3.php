<?php

$v = "";

if ($this->input->post('paid_by')) {
    $v .= "&paid_by=" . $this->input->post('paid_by');
}

if ($this->input->post('cliente')) {
    $v .= "&cliente=" . $this->input->post('cliente');
}
if ($this->input->post('data_pagamento_de')) {
    $v .= "&data_pagamento_de=" . $this->input->post('data_pagamento_de');
}

if ($this->input->post('data_pagamento_ate')) {
    $v .= "&data_pagamento_ate=" . $this->input->post('data_pagamento_ate');
}

if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}

if ($this->input->post('valor_de')) {
    $v .= "&valor_de=" . $this->input->post('valor_de');
}
if ($this->input->post('valor_ate')) {
    $v .= "&valor_ate=" . $this->input->post('valor_ate');
}

?>

<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($this->input->post('cliente') || $clienteObjId) {

            $cliente = $this->input->post('cliente');
            if (!$cliente) {
                $cliente = $clienteObjId;
            }
            ?>
        $('#cliente').val(<?= $cliente ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/<?= $cliente ?>",
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#cliente').val(<?= $this->input->post('cliente') ?>);
        <?php } else { ?>
            $('#cliente').select2({
                minimumInputLength: 1,
                data: [],
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } ?>

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('relatorio_financeiro_forma_pagamento'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            }
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">

                <li class="dropdown">
                    <a href="#" id="imprimir" class="tip" title="<?= lang('print') ?>">
                        <i class="icon fa fa-print"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="form">
                    <?php echo form_open("reports/relFinanceiroFormaPagamentoDetalhadoNivel3"); ?>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("status_viagem", "status_viagem") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'Aguardando' => lang('Aguardando'),
                                    'Confirmado' => lang('confirmado'),
                                    'Em Viagem' => lang('em_viagem') ,
                                    'Executado' => lang('executado') ,
                                    'Cancelado' => lang('cancelado')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ''), 'class="form-control" id="unit"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                <?php
                                $wh[""] = lang('select').' '.lang('warehouse');
                                $wh["outras_receitas"] = lang('outras_receitas');
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }
                                if ($warehouseObjId) {
                                    echo form_dropdown('warehouse', $wh,   $warehouseObjId, 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                } else {
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                <?php echo form_input('cliente', (isset($_POST['cliente']) ? $_POST['cliente'] : ""), 'class="form-control" id="cliente" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("cliente") . '"'); ?>
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("paying_by", "paid_by_1"); ?>
                                <select name="paid_by" id="paid_by" class="form-control paid_by" required="required">
                                    <option><?php echo lang('todos');?></option>
                                    <?= $this->sma->paid_opts_compact_cash((isset($_POST['paid_by']) ? $_POST['paid_by'] : $formaPagamento)); ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang('data_pagamento_de', 'data_pagamento_de'); ?>
                                <input type="date" name="data_pagamento_de" value="<?php echo (isset($_POST['data_pagamento_de']) ? $_POST['data_pagamento_de'] : '');?>" class="form-control tip" id="data_pagamento_de" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang('data_pagamento_ate', 'data_pagamento_ate'); ?>
                                <input type="date" name="data_pagamento_ate" value="<?php echo (isset($_POST['data_pagamento_ate']) ? $_POST['data_pagamento_ate'] : '');?>" class="form-control tip" id="data_pagamento_ate" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang('valor_de', 'valor_de'); ?>
                                <input type="number" name="valor_de" value="<?php echo (isset($_POST['valor_de']) ? $_POST['valor_de'] : '');?>" class="form-control tip" id="valor_de" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang('valor_ate', 'valor_ate'); ?>
                                <input type="number" name="valor_ate" value="<?php echo (isset($_POST['valor_ate']) ? $_POST['valor_ate'] : '');?>" class="form-control tip" id="valor_ate" data-original-title="" title="">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                            <input type="button" value="Limpar" class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3'">
                            <input type="button" id="voltar" value="Voltar" class="btn btn-primary">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="clearfix"></div>

                <div class="table-responsive" id="divImprimir">
                    <table id=""
                           style="cursor: pointer;"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th style="text-align: left;width: 50%;"><?= lang("passageiro"); ?></th>
                            <th style="text-align: left;"><?= lang("forma_pagamento"); ?></th>
                            <th style="text-align: center;"><?= lang("data_pagamento"); ?></th>
                            <th style="text-align: center;"><?= lang("note"); ?></th>
                            <th style="text-align: right;"><?= lang("pago"); ?></th>
                            <th style="text-align: right;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalPago = 0;
                        foreach ($payments as $payment) {
                            $totalPago = $totalPago + $payment->amount;
                            ?>
                            <tr id="<?php echo $payment->id;?>">
                                <td style="text-align: left;"><?php echo $payment->customer;?></td>
                                <td style="text-align: left;"><?php echo lang($payment->paid_by);?></td>
                                <td style="text-align: center;"><?php echo  $this->sma->hrsd($payment->date);?></td>
                                <td style="text-align: center;"><?php echo $payment->note;?></td>
                                <td style="text-align: right;"><a href="<?php echo base_url();?>sales/payments/<?php echo $payment->sale_id;?>" data-toggle="modal" data-target="#myModal"><?php echo $this->sma->formatMoney($payment->amount);?></a></td>
                                <td class="">
                                    <div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo base_url();?>sales/view/<?php echo $payment->sale_id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>sales/emitir_contrato/<?php echo $payment->sale_id;?>"><i class="fa fa-book"></i> Baixar Contrato</a></li>
                                                <li><a href="<?php echo base_url();?>sales/payments/<?php echo $payment->sale_id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Ver Pagamentos</a></li>
                                                <li><a href="<?php echo base_url();?>sales/add_payment/<?php echo $payment->sale_id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Adicionar Pagamento</a></li>
                                                <li><a href="<?php echo base_url();?>sales/edit/<?php echo $payment->sale_id;?>" class="sledit"><i class="fa fa-edit"></i> Editar Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $payment->sale_id;?>"><i class="fa fa-file-pdf-o"></i> Baixar Voucher</a></li>
                                                <li><a href="<?php echo base_url();?>sales/email/<?php echo $payment->sale_id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> E-mail de Passagem</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo base_url();?>customers/edit/<?php echo $payment->sale_id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Visualizar Dados Passageiro</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th><small>somente são exibidos os responsáveis pela passagem</small></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalPago);?></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#form').show();

        $('#imprimir').click(function (event) {
            event.preventDefault();

            //pega o Html da DIV
            var divElements = document.getElementById('divImprimir').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;

            location.reload();
        });

        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });

        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $('#voltar').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroGeralNivel2/all/"+warehouse+"/"+cliente;
        });

        $('#unit').change(function (event) {
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: site.base_url + "products/getProdutoFilterStatus/"+$('#unit').val(),
                dataType: 'json',
                success: function(produtos)
                {
                    $('#warehouse').empty();
                    $("#warehouse").append('<option value=><?php echo lang('select').' '.lang('warehouse');?></option>');
                    $("#warehouse").append('<option value="outras_receitas"><?php echo lang('outras_receitas');?></option>');
                    $.each(produtos, function( index, produto ) {
                        $("#warehouse").append('<option value='+produto.id+'>'+produto.name+'</option>');
                    });
                    $('#warehouse').select2();
                }
            });
        });
    });
</script>