<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roomlisthospede_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function add($data = array()) {

        $this->db->insert('room_list_hospedagem_hospede', $data);

        if ($this->db->affected_rows() == '1') {
            return $this->db->insert_id('room_list_hospedagem_hospede');
        }
        return FALSE;
    }

    public function getById($id)
    {
        $this->db->select('room_list_hospedagem_hospede.*, tipo_hospedagem.name as name');
        $this->db->join('tipo_hospedagem', 'room_list_hospedagem_hospede.tipo_hospedagem_id=tipo_hospedagem.id', 'left');

        $q = $this->db->get_where('room_list_hospedagem_hospede', array('room_list_hospedagem_hospede.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllHospedes($roomlistId)
    {

        $this->db->select('room_list_hospedagem_hospede.*,sale_items.faixaNome, sale_items.descontarVaga, companies.name as name, companies.social_name, companies.vat_no as documento, companies.data_aniversario as nascimento,   companies.tipoFaixaEtaria, tipo_hospedagem.name as tipoHospedagem, tipo_hospedagem.id as tipoHospedagemId');

        $this->db->join('companies', 'room_list_hospedagem_hospede.customer_id=companies.id', 'left');
        $this->db->join('tipo_hospedagem', 'room_list_hospedagem_hospede.tipo_hospedagem_id=tipo_hospedagem.id', 'left');
        $this->db->join('room_list_hospedagem', 'room_list_hospedagem_hospede.room_list_hospedagem_id=room_list_hospedagem.id', 'left');
        $this->db->join('sale_items', 'sale_items.id=room_list_hospedagem_hospede.itemId');

        $this->db->where('room_list_hospedagem.room_list_id', $roomlistId);

        $this->db->order_by('tipo_hospedagem.id, room_list_hospedagem_id');

        $q = $this->db->get('room_list_hospedagem_hospede');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllHospedes_old($roomlistId)
    {

        $this->db->select('room_list_hospedagem_hospede.*,sale_items.faixaNome, sale_items.descontarVaga, companies.name as name, companies.vat_no as documento, companies.data_aniversario as nascimento,   companies.tipoFaixaEtaria, tipo_hospedagem.name as tipoHospedagem, tipo_hospedagem.id as tipoHospedagemId');

        $this->db->join('companies', 'room_list_hospedagem_hospede.customer_id=companies.id', 'left');
        $this->db->join('tipo_hospedagem', 'room_list_hospedagem_hospede.tipo_hospedagem_id=tipo_hospedagem.id', 'left');
        $this->db->join('room_list_hospedagem', 'room_list_hospedagem_hospede.room_list_hospedagem_id=room_list_hospedagem.id', 'left');
        $this->db->join('sale_items', 'sale_items.id=room_list_hospedagem_hospede.itemId', 'left');

        $this->db->where('room_list_hospedagem.room_list_id', $roomlistId);

        $this->db->order_by('tipo_hospedagem.id, room_list_hospedagem_id');

        $q = $this->db->get('room_list_hospedagem_hospede');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllHospedesByTipoHospedagemRoomList($hospedagem_id)
    {
        $this->db->where('room_list_hospedagem_id', $hospedagem_id);

        $q = $this->db->get('room_list_hospedagem_hospede');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllHospedesByRoomList($hospedagem_id, $tipo_hospedagem_id)
    {

        $this->db->select('room_list_hospedagem_hospede.*,sale_items.faixaNome, sale_items.sale_id ');


        $this->db->where('room_list_hospedagem_id', $hospedagem_id);
        $this->db->where('tipo_hospedagem_id', $tipo_hospedagem_id);

        $this->db->join('sale_items', 'sale_items.id=room_list_hospedagem_hospede.itemId');

        $q = $this->db->get('room_list_hospedagem_hospede');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllHospedesByRoomList_old($hospedagem_id, $tipo_hospedagem_id)
    {

        $this->db->select('room_list_hospedagem_hospede.*,sale_items.faixaNome, sale_items.sale_id ');


        $this->db->where('room_list_hospedagem_id', $hospedagem_id);
        $this->db->where('tipo_hospedagem_id', $tipo_hospedagem_id);

        $this->db->join('sale_items', 'sale_items.id=room_list_hospedagem_hospede.itemId', 'left');

        $q = $this->db->get('room_list_hospedagem_hospede');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function hospedeNaoInseridoNoQuarto($roomlistId, $customerId)
    {

        $this->db->where('room_list.id', $roomlistId);
        $this->db->where('customer_id', $customerId);

        $this->db->join('room_list_hospedagem', 'room_list_hospedagem_hospede.room_list_hospedagem_id=room_list_hospedagem.id', 'left');
        $this->db->join('room_list', 'room_list_hospedagem.room_list_id=room_list.id', 'left');
        $this->db->join('sale_items', 'sale_items.id=room_list_hospedagem_hospede.itemId');

        $q = $this->db->get('room_list_hospedagem_hospede', 1);

        if ($q->num_rows() > 0) {
            return FALSE;
        }
        return TRUE;
    }

    public function hospedeNaoInseridoNoQuarto_old($roomlistId, $customerId)
    {

        $this->db->where('room_list.id', $roomlistId);
        $this->db->where('customer_id', $customerId);

        $this->db->join('room_list_hospedagem', 'room_list_hospedagem_hospede.room_list_hospedagem_id=room_list_hospedagem.id', 'left');
        $this->db->join('room_list', 'room_list_hospedagem.room_list_id=room_list.id', 'left');

        $q = $this->db->get('room_list_hospedagem_hospede', 1);

        if ($q->num_rows() > 0) {
            return FALSE;
        }
        return TRUE;
    }

    public function update($id, $data = array()) {
        if ($this->db->update('room_list_hospedagem_hospede', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('room_list_hospedagem_hospede', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteByItem($itemId) {
        if ($this->db->delete('room_list_hospedagem_hospede', array('itemId' => $itemId))) {
            return true;
        }
        return FALSE;
    }

    public function getHospedagemHospedeByItemVenda($item_id) {
        $q = $this->db->get_where('room_list_hospedagem_hospede', array('itemId' => $item_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

}
