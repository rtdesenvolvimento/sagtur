<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutosAdicionais_model extends CI_Model
{

    public const ADULTO = 'adulto';
    public const CRIANCA = 'crianca';

    public $tipo;
    public $produto;
    public $quantidade;
    public $valor;
    public $faixaId;

    public function __construct() {
        parent::__construct();
    }

}
