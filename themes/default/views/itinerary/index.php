<style media="screen">
    #ITData td:nth-child(1) {display: none;}
    #ITData td:nth-child(2) {text-align: center;width: 10%;}
    #ITData td:nth-child(3) {text-align: center;width: 10%;}
    #ITData td:nth-child(4) {text-align: center;width: 10%;}

    #ITData td:nth-child(9) {text-align: center;width: 5%;}
</style>

<script>

    function status(x) {

        if(x == null) {
            return '';
        } else if(x === 'ABERTO') {
            return '<div class="text-center"><span class="label label-success">Aberta</span></div>';
        } else {
            return '<div class="text-center"><span class="label label-warning">x</span></div>';
        }
    }

    $(document).ready(function () {

        var oTable = $('#ITData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('itinerary/getItineraries')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {},
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterDateFrom", "value":  $('#filter_date').val() });
                aoData.push({ "name": "filterProducts", "value":  $('#filter_tour').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                {"mRender": status},
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {},
        });

    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('tours/tour_actions', 'id="action-form"');
}
?>

<div class="row">

    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue">
                    <i class="fa-fw fa fa-cogs"></i><?=lang('order_service')?>
                </h2>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a href="<?=site_url('itinerary/add')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-arrow-circle-o-up"></i> <?=lang('add_itinerary')?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <?= lang("date", "filter_date") ?>
                            <?= form_input('filter_date', (isset($_POST['filter_date']) ? $_POST['filter_date'] : ''), 'class="form-control" id="filter_date"','date'); ?>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <?= lang("tour", "filter_tour") ?>
                            <?php
                            $prod[''] =  lang("select") . " " . lang("product") ;
                            foreach ($products as $produts) {
                                $prod[$produts->id] = $produts->name;
                            }
                            echo form_dropdown('filter_tour', $prod, (isset($_POST['filter_tour']) ? $_POST['filter_tour'] : ''), 'class="form-control select" id="filter_tour" placeholder="' . lang("select") . " " . lang("tour") . '" style="width:100%"')
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="ITData" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th><?php echo $this->lang->line("date"); ?></th>
                                    <th><?php echo $this->lang->line("fornecedor"); ?></th>
                                    <th><?php echo $this->lang->line("veiculo"); ?></th>
                                    <th><?php echo $this->lang->line("motorista"); ?></th>
                                    <th><?php echo $this->lang->line("guia"); ?></th>
                                    <th><?php echo $this->lang->line("product"); ?></th>
                                    <th><?php echo $this->lang->line("status"); ?></th>
                                    <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr><td colspan="10" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th><?php echo $this->lang->line("date"); ?></th>
                                    <th><?php echo $this->lang->line("fornecedor"); ?></th>
                                    <th><?php echo $this->lang->line("veiculo"); ?></th>
                                    <th><?php echo $this->lang->line("motorista"); ?></th>
                                    <th><?php echo $this->lang->line("guia"); ?></th>
                                    <th><?php echo $this->lang->line("product"); ?></th>
                                    <th><?php echo $this->lang->line("status"); ?></th>
                                    <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#filter_date").change(function (event){
            $('#ITData').DataTable().fnClearTable();
        });

        $("#filter_tour").change(function (event){
            $('#ITData').DataTable().fnClearTable();
        });
    });
</script>
