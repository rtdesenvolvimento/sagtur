<style>
    .selecionado {
        background: #fdf59a;
        border: 2px solid #333;
    }

    .btn_icon {
        padding: 6px 6px 6px 12px;
    }
</style>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_servico_incluso'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/adicionarServicoIncluso", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?php echo lang('name', 'name'); ?>
                <div class="controls">
                    <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ''), 'class="form-control" id="name" required="required"'); ?>
                </div>
            </div>
            <div class="form-group all">
                <input type="hidden" id="image_icon_id" name="image_icon_id" value="">
                <?php foreach ($icons as $icon) {?>
                    <button type="button" id="<?=$icon->id;?>" class="btn btn_icon" icon_id="<?=$icon->id;?>"><?=$icon->icon;?></button>
                <?php }?>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('adicionar_servico_incluso', lang('adicionar_servico_incluso'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script type="text/javascript">

    $(document).ready(function() {

        $('.btn_icon').click(function (evnet){

            var icon_id = $(this).attr('icon_id');

            $('.btn_icon').removeClass('selecionado');
            $('#'+icon_id).addClass('selecionado');

            $('#image_icon_id').val(icon_id);
        });
    });

</script>

