<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model
{
    const STATUS_ABERTA = "ABERTA";
    const STATUS_FATURADA = "FATURADA";
    const STATUS_PARCIAL = "PARCIAL";
    const STATUS_REPARCELADA = "REPARCELADA";
    const STATUS_CANCELADA = "CANCELADA";
    const STATUS_ESTORNADA = 'ESTORNADA';
    const STATUS_QUITADA = "QUITADA";
    const STATUS_VENCIDA = "VENCIDA";
    const  STATUS_PAGO = 'QUITADA';

    const STATUS_SEGUNDA_VIA = "SEGUNDA_VIA";
    const RECEITA = 'receita';
    const DESPESA = 'despesa';

    const TIPO_OPERACAO_CREDITO = 'CREDITO';
    const TIPO_OPERACAO_DEBITO = 'DEBITO';

    public function __construct() {

        parent::__construct();

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');
        $this->load->model('service/FaturaService_model', 'FaturaService_model');

        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository');

        $this->load->model('dto/CobrancaFaturaDTO_model', 'CobrancaFaturaDTO_model');

        $this->load->model('companies_model');
        $this->load->model('sales_model');
        $this->load->model('Products_model');
    }

    public function buscarFaturas($tipo, $parcelaDTO_model) {
        return $this->FinanceiroRepository->buscarFaturas($tipo, $parcelaDTO_model);
    }

    public function buscarFaturasDeDespesas($parcelaDTO_model) {
        return $this->buscarFaturas(Financeiro_model::TIPO_OPERACAO_DEBITO, $parcelaDTO_model);
    }

    public function buscarFaturasDeReceitas($parcelaDTO_model) {
        return $this->buscarFaturas(Financeiro_model::TIPO_OPERACAO_CREDITO, $parcelaDTO_model);
    }

    public function buscarExtrato($mes=NULL, $ano=NULL, $movimentador) {
        return $this->FinanceiroRepository->buscarExtrato($mes, $ano, $movimentador);
    }

    public function adicionarReceita($contaReceber, $parcelasReparcelar=[])
    {
        try {

            $this->__iniciaTransacao();

            $this->db->insert('conta_receber', $contaReceber->data);

            $contaReceber->id = $this->db->insert_id('conta_receber');

            $this->gerarParcelamento($contaReceber, Financeiro_model::RECEITA);

            if (count($parcelasReparcelar) > 1) {
                $this->renegociarDividasContaReceber($parcelasReparcelar, $contaReceber->id);
            }

            $this->__confirmaTransacao();

            return $contaReceber->id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function adicionarDespesa($contaPagar, $parcelasReparcelar=[])
    {
        try {

            $this->__iniciaTransacao();

            $this->db->insert('conta_pagar', $contaPagar->data);

            $contaPagar->id =  $this->db->insert_id('conta_pagar');

            $this->gerarParcelamento($contaPagar, Financeiro_model::DESPESA);

            if (count($parcelasReparcelar) > 1) $this->renegociarDividasContaReceber($parcelasReparcelar, $contaPagar->id);

            $this->__confirmaTransacao();

            return $contaPagar->id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function editarFatura($faturaModel) {
        $this->FaturaService_model->editarFatura($faturaModel);
    }

    private function renegociarDividasContaReceber($parcelasReparcelar, $novaContaReceberId) {

        for($index=0;$index<count($parcelasReparcelar);$index++) {

            $parcela = $this->getParcelaById($parcelasReparcelar[$index]);

            $this->edit('conta_receber', array('conta_origem' => $novaContaReceberId),'id', $parcela->contareceberId);

            $parcelasFatura = $this->getParcelasFaturaByParcela($parcela->id);

            foreach ($parcelasFatura as $parcelaFatura) {
                $this->edit('parcela_fatura', array('status' => Financeiro_model::STATUS_REPARCELADA), 'id', $parcelaFatura->id);
                $this->edit('parcela', array('status' => Financeiro_model::STATUS_REPARCELADA),'id',  $parcelaFatura->parcela);
                $this->edit('fatura', array('status' => Financeiro_model::STATUS_REPARCELADA), 'id', $parcelaFatura->fatura);
            }

            $this->CobrancaService_model->cancelarCobranca($parcela);

            //TODO VERIFICAR COMO SERA FEITO PARA PAGAMENTOS JÁ REALIZADOS
        }
    }

    function adicionarTransferencia($dadosDaConta, $contaOrigem, $contaDestino) {

        $reference_no =  $this->site->getReference('pay');

        $completouSaque = $this->adicionarSaque($dadosDaConta, $contaOrigem, $reference_no,Sales_model::TIPO_DE_PAGAMENTO_TRANSFERENCIA);
        $completouDeposito =  $this->adicionarDeposito($dadosDaConta, $contaDestino, Sales_model::TIPO_DE_PAGAMENTO_TRANSFERENCIA);

        return $completouSaque && $completouDeposito;
    }

    function adicionarSaque($dadosDaConta, $contaOrigem, $reference_no, $tipoDePagamento) {

        $desconto = 0;
        $parcelaReparcelar = [];

        $despesaSaquePadraoId = $this->Settings->despesaTransferencia;
        $pessoaResponsavel = $this->Settings->default_biller;
        $tipoCobrancaId = $this->Settings->tipoCobrancaAVista;
        $conticaoPagamentoAVista = $this->Settings->condicaoPagamentoAVista;
        $formaPagamento = $this->financeiro_model->getFormaPagamentoById($this->Settings->dinheiro);

        $valorTransferencia = $dadosDaConta['valor'];
        $despesa = $dadosDaConta['despesa'];

        if ($despesa != $despesaSaquePadraoId && $tipoDePagamento != Sales_model::TIPO_DE_PAGAMENTO_TRANSFERENCIA) {
            $tipoDePagamento = Sales_model::TIPO_DE_PAGAMENTO_TAXAS;
        }

        $dadosDaConta['pessoa'] = $pessoaResponsavel;
        $dadosDaConta['tipocobranca'] = $tipoCobrancaId;
        $dadosDaConta['condicaopagamento'] = $conticaoPagamentoAVista;

        $payment = array(
            'reference_no' => $reference_no,
            'created_by' => $this->session->userdata('user_id'),
            'date' => $dadosDaConta['dtvencimento'],
            'note' => $dadosDaConta['note'],
            'attachment' => $dadosDaConta['attachment'],
            'formapagamento' => $formaPagamento->id,
            'movimentador' => $contaOrigem,
            'pessoa' => $pessoaResponsavel,
            'amount' => $valorTransferencia,
            'paid_by' => $formaPagamento->name,
            'tipo' => $tipoDePagamento,
            'type' => 'received'
        );

        return $this->adicionarDespesa($dadosDaConta, [$valorTransferencia], [$dadosDaConta['dtvencimento']], [$tipoCobrancaId],[$desconto] , $parcelaReparcelar, $payment);
    }

    function adicionarDeposito($dadosDaConta, $contaOrigem, $tipoPagamento) {

        $desconto = 0;
        $parcelaReparcelar = [];

        $receitaId = $this->Settings->receitaTransferencia;
        $pessoaResponsavel = $this->Settings->default_biller;
        $tipoCobrancaId = $this->Settings->tipoCobrancaAVista;
        $conticaoPagamentoAVista = $this->Settings->condicaoPagamentoAVista;

        $formaPagamento = $this->financeiro_model->getFormaPagamentoById($this->Settings->dinheiro);

        $valorTransferencia = $dadosDaConta['valor'];
        $dadosDaConta['pessoa'] = $pessoaResponsavel;
        $dadosDaConta['tipocobranca'] = $tipoCobrancaId;
        $dadosDaConta['receita'] = $receitaId;
        $dadosDaConta['condicaopagamento'] = $conticaoPagamentoAVista;

        $payment = array(
            'reference_no' => $this->site->getReference('pay'),
            'created_by' => $this->session->userdata('user_id'),
            'date' => $dadosDaConta['dtvencimento'],
            'note' => $dadosDaConta['note'],
            'formapagamento' => $formaPagamento->id,
            'movimentador' => $contaOrigem,
            'attachment' => $dadosDaConta['attachment'],
            'pessoa' => $pessoaResponsavel,
            'amount' => $valorTransferencia,
            'paid_by' => $formaPagamento->name,
            'type' => 'received',
            'tipo' => $tipoPagamento
        );

        $contaReceber = new ContaReceber_model();
        $contaReceber->data = $dadosDaConta;
        $contaReceber->valores = [$valorTransferencia];
        $contaReceber->vencimentos = [$dadosDaConta['dtvencimento']];
        $contaReceber->cobrancas = [$tipoCobrancaId];
        $contaReceber->movimentadores = [$contaOrigem];
        $contaReceber->descontos = [$desconto];
        $contaReceber->pagamentos = $payment;

        return $this->adicionarReceita($contaReceber, $parcelaReparcelar);
    }

    function gerarParcelamento($contaReceber, $tipoConta) {

        $conta = $contaReceber->data;
        $valorVencimento = $contaReceber->valores;
        $vencimentos = $contaReceber->vencimentos;
        $tiposCobranca = $contaReceber->cobrancas;
        $movimentadores = $contaReceber->movimentadores;
        $descontoParcela = $contaReceber->descontos;
        $payments = $contaReceber->pagamentos;

        $condicaoPagamento              = $this->getCondicaoPagamentoById($conta['condicaopagamento']);
        $totalParcelas                  = $condicaoPagamento->parcelas;
        $numeroParcelasCartaoCredito    = 1;
        $tipoCobrancaoObj               = $this->getTipoCobrancaById($conta['tipocobranca']);

        if ($tipoCobrancaoObj->tipo == 'carne_cartao') {//TODO CARNE COM CARTAO DE CREDITO
            $totalParcelas                  = 1;//TODO PARA CARTAO DE CREDITO NAO GERA PARCELAMENTO O RECEBIMENTO E A VISTA O PARCELAMENTO E POR CONTA DO CLIENTE
            $numeroParcelasCartaoCredito    = $condicaoPagamento->parcelas;
        } else if ($this->isCarne($tipoCobrancaoObj)) {
            $contaReceber->numeroParcelas   = $totalParcelas;
        }

        $dataVencimento     = null;
        $cobrancasFatura = new CobrancaFaturaDTO_model();

        $cobrancasFatura->venda_manual      = $contaReceber->venda_manual;

        //field cc
        $cobrancasFatura->senderHash        = $contaReceber->senderHash;
        $cobrancasFatura->creditCardToken   = $contaReceber->creditCardToken;
        $cobrancasFatura->deviceId          = $contaReceber->deviceId;
        $cobrancasFatura->numeroParcelas    = $contaReceber->numeroParcelas;

        $cobrancasFatura->cardName          = $contaReceber->cardName;
        $cobrancasFatura->cardNumber        = $contaReceber->cardNumber;
        $cobrancasFatura->cardExpiryMonth   = $contaReceber->cardExpiryMonth;
        $cobrancasFatura->cardExpiryYear    = $contaReceber->cardExpiryYear;
        $cobrancasFatura->cvc               = $contaReceber->cvc;

        //field cc - titular
        $cobrancasFatura->cpfTitularCartao              = $contaReceber->cpfTitularCartao;
        $cobrancasFatura->celularTitularCartao          = $contaReceber->celularTitularCartao;

        //field cc - endereco
        $cobrancasFatura->cepTitularCartao                  = $contaReceber->cepTitularCartao;
        $cobrancasFatura->enderecoTitularCartao             = $contaReceber->enderecoTitularCartao;
        $cobrancasFatura->numeroEnderecoTitularCartao       = $contaReceber->numeroEnderecoTitularCartao;
        $cobrancasFatura->complementoEnderecoTitularCartao  = $contaReceber->complementoEnderecoTitularCartao;
        $cobrancasFatura->bairroTitularCartao               = $contaReceber->bairroTitularCartao;
        $cobrancasFatura->cidadeTitularCartao               = $contaReceber->cidadeTitularCartao;
        $cobrancasFatura->estadoTitularCartao               = $contaReceber->estadoTitularCartao;
        $cobrancasFatura->totalParcelaPagSeguro             = $contaReceber->totalParcelaPagSeguro;

        for($index=0;$index<$totalParcelas;$index++) {

            $dataDigitada = $vencimentos[$index];
            $tipoCobranca = $tiposCobranca[$index];
            $vlVencimento = $valorVencimento[$index];
            $desconto     = $descontoParcela[$index];
            $movimentador = $movimentadores[$index];

            if ($tipoCobrancaoObj->tipo == 'carne_cartao') {//TODO CARNE COM CARTAO DE CREDITO
                $vlVencimento = $conta['valor'];//PARA ESSA REGRA PEGA O VALOR TOTAL DA CONTA
            }

            if ($dataDigitada == null) {
                if ($dataVencimento == null) { 
                    $dataVencimento = date('Y-m-d');
                } else  {
                    $dataVencimento = date("Y-m-d", strtotime($dataVencimento. " +1 month"));
                }
            } else {
                $dataVencimento = $dataDigitada;
            }

            $parcelaId = $this->getDadosParcela($conta, $contaReceber->id, $index, $totalParcelas , $vlVencimento,
            $dataVencimento, $tipoCobranca,$movimentador, $condicaoPagamento->id, $desconto, $tipoConta, $numeroParcelasCartaoCredito);

            $faturaId = $this->faturar($parcelaId, $payments);

            $cobrancasFatura->adicionarFatura($this->__getById('fatura', $faturaId));//TODO VER ESTE PONTO QUE IRA IMPACTAR O SISTEMA DE FATURAS
        }

        if ($tipoConta == Financeiro_model::RECEITA) {
            $this->CobrancaService_model->geraCobrancasFatura($cobrancasFatura);
        }
    }

    private function isCarne($tipoCobranca) {
        if ($tipoCobranca->tipo == 'carne' ||
            $tipoCobranca->tipo == 'carne_boleto' ||
            $tipoCobranca->tipo == 'carne_cartao' ||
            $tipoCobranca->tipo == 'carne_boleto_cartao') {
            return TRUE;
        }
        return FALSE;
    }

    function getDadosParcela($conta, $contaId, $index, $totalParcelas, $vlVencimento,
     $dataVencimento, $tipoCobranca,$movimentador, $condicaoPagamento, $desconto, $tipoConta, $numeroParcelasCartaoCredito) {

        if ($desconto == '') {
            $desconto = 0;
        }

        if ($tipoConta == Financeiro_model::RECEITA) {
            $dataParcela = array(
                'status'                    => Financeiro_model::STATUS_ABERTA,
                'dtvencimento'              => $dataVencimento,
                'dtcompetencia'             => $dataVencimento,
                'indicecorrecao'            => 0,//TODO AQUI SERA CALCULADO O INDICE DE CORREAÇÃO DAS PARCELAS FINANCIADAS PELO TIPO DE MOEDA COM BASE NO CAMBIO
                'valor'                     => $vlVencimento,
                'acrescimo'                 => 0,
                'desconto'                  => $desconto,
                'valorpago'                 => 0,
                'valorpagar'                => $vlVencimento,
                'numeroParcela'             => ($index + 1),
                'totalParcelas'             => $totalParcelas,
                'numeroParcelaCC'           => $numeroParcelasCartaoCredito,
                'tipocobranca'              => $tipoCobranca,
                'contareceberId'            => $contaId,

                'warehouse'                 => $conta['warehouse'],
                'pessoa'                    => $conta['pessoa'],
                'receita'                   => $conta['receita'],
                'note'                      => $conta['note'],
                'numero_documento'          => $conta['numero_documento'],

                'programacaoId'             => $conta['programacaoId'],
                'sale_id'                   => $conta['sale'],
                'fechamento_comissao_id'    => $conta['fechamento_comissao_id'],
                'fechamento_item_id'        => $conta['fechamento_item_id'],

                'condicaoPagamento'         => $condicaoPagamento,
                'movimentador'              => $movimentador,
                'tipo'                      => Financeiro_model::RECEITA
            );

        } else {
            $dataParcela = array(
                'status'                    => Financeiro_model::STATUS_ABERTA,
                'dtvencimento'              => $dataVencimento,
                'dtcompetencia'             => $dataVencimento,
                'indicecorrecao'            => 0,//TODO AQUI SERA CALCULADO O INDICE DE CORREAÇÃO DAS PARCELAS FINANCIADAS PELO TIPO DE MOEDA COM BASE NO CAMBIO
                'valor'                     => $vlVencimento,
                'acrescimo'                 => 0,
                'desconto'                  => 0,
                'valorpago'                 => 0,
                'valorpagar'                => $vlVencimento,
                'numeroParcela'             => ($index + 1),
                'totalParcelas'             => $totalParcelas,
                'tipocobranca'              => $tipoCobranca,
                'numeroParcelaCC'           => $numeroParcelasCartaoCredito,

                'warehouse'                 => $conta['warehouse'],
                'pessoa'                    => $conta['pessoa'],
                'despesa'                   => $conta['despesa'],
                'note'                      => $conta['note'],
                'numero_documento'          => $conta['numero_documento'],

                'programacaoId'             => $conta['programacaoId'],
                'sale_id'                   => $conta['sale'],
                'fechamento_comissao_id'    => $conta['fechamento_comissao_id'],
                'fechamento_item_id'        => $conta['fechamento_item_id'],

                'condicaoPagamento'         => $condicaoPagamento,
                'movimentador'              => $movimentador,
                'contapagarId'              => $contaId,
                'tipo'                      => Financeiro_model::DESPESA
            );
        }
        return $this->adicionarParcela($dataParcela);
    }

    public function faturar($parcelaId, $payments=[]) {

        $parcela = $this->getParcelaById($parcelaId);

        $valorFatura    = $parcela->valor;
        $totalCredito   = 0;
        $totalDebito    = 0;

        $strReceitas = '';
        $strDespesas = '';

        $strContasReceber = '';
        $strContasPagar = '';

        if ($parcela->tipo == Financeiro_model::RECEITA) {
            $tipoOperacao = Financeiro_model::TIPO_OPERACAO_CREDITO;
            $receita = $this->getReceitaById($parcela->receita);
            $strReceitas = $receita->name;
            $product_name = $strReceitas;

            if ($parcela->programacaoId != null && $parcela->programacaoId > 0) {
                $programacao = $this->AgendaViagemRespository->getProgramacaoById($parcela->programacaoId);
                $product = $this->Products_model->getProductByID($programacao->produto);
                $product_name = $product->name.' '.$this->sma->hrsd($programacao->dataSaida).'<br/>'.$strReceitas;
            } else {
                $product_name = 'MATRIZ<br/>'.$product_name;
            }

            $totalCredito =$parcela->valor;
            $strContasReceber = $parcela->contareceberId;
        } else {
            $tipoOperacao = Financeiro_model::TIPO_OPERACAO_DEBITO;
            $despesa = $this->getDespesaById($parcela->despesa);
            $strDespesas = $despesa->name;
            $product_name = $strDespesas;

            if ($parcela->programacaoId != null && $parcela->programacaoId > 0) {
                $programacao = $this->AgendaViagemRespository->getProgramacaoById($parcela->programacaoId);
                $product = $this->Products_model->getProductByID($programacao->produto);
                $product_name = $product->name.' '.$this->sma->hrsd($programacao->dataSaida).'<br/>'.$strDespesas;
            } else {
                $product_name = 'MATRIZ<br/>'.$product_name;
            }

            $totalDebito =  $parcela->valor;
            $strContasPagar = $parcela->contapagarId;
        }

        $reference = $this->site->getReference('ex');

        $dataFatura = array(
            'reference'                 => $reference,
            'status'                    => Financeiro_model::STATUS_ABERTA,
            'pessoa'                    => $parcela->pessoa,
            'dtVencimento'              => $parcela->dtvencimento,
            'dtfaturamento'             => date('Y-m-d'),
            'tipoCobranca'              => $parcela->tipocobranca,
            'strReceitas'               => $strReceitas,
            'strDespesas'               => $strDespesas,
            'contas_receber'            => $strContasReceber,
            'contas_pagar'              => $strContasPagar,
            'multa'                     => 0,
            'mora'                      => 0,
            'percentualmulta'           => 0,
            'percentualmora'            => 0,
            'tipooperacao'              => $tipoOperacao,
            'totalcredito'              => $totalCredito,
            'totaldebito'               => $totalDebito,
            'valorfatura'               => $valorFatura,
            'valorpagar'                => $valorFatura,
            'valorpago'                 => 0,
            'totalacrescimento'         => 0,
            'totaldesconto'             => 0,
            'warehouse'                 => $parcela->warehouse,
            'receita'                   => $parcela->receita,
            'despesa'                   => $parcela->despesa,
            'condicaoPagamento'         => $parcela->condicaoPagamento,
            'movimentador'              => $parcela->movimentador,
            'note'                      => $parcela->note,
            'numero_documento'          => $parcela->numero_documento,
            'programacaoId'             => $parcela->programacaoId,
            'numero_parcela'            => $parcela->numeroparcela.'/'.$parcela->totalParcelas,
            'numeroParcelaCC'           => $parcela->numeroParcelaCC,
            'product_name'              => $product_name,

            'sale_id'                   => $parcela->sale_id,
            'fechamento_comissao_id'    => $parcela->fechamento_comissao_id,
            'fechamento_item_id'        => $parcela->fechamento_item_id

        );

        $faturaId = $this->adicionarFatura($dataFatura);

        $dataFaturaParcela = array(
            'status'    => Financeiro_model::STATUS_ABERTA,
            'warehouse' => $parcela->warehouse,
            'parcela'   => $parcelaId,
            'fatura'    => $faturaId
        );

        $this->adicionarParcelaFatura($dataFaturaParcela);

        $this->edit('parcela', array('status' => Financeiro_model::STATUS_FATURADA),'id', $parcela->id);

        if ($parcela->tipo == Financeiro_model::RECEITA) {
            $this->edit('conta_receber', array('status' => Financeiro_model::STATUS_FATURADA), 'id', $parcela->contareceberId);
        } else {
            $this->edit('conta_pagar', array('status' => Financeiro_model::STATUS_FATURADA), 'id', $parcela->contapagarId);
        }

        $this->adicionarPagamentoFatura($payments, $faturaId);

        $this->site->updateReference('ex');

        return $faturaId;
    }

    private function adicionarPagamentoFatura($payments, $faturaId) {
        if (count($payments) == 0) return;

        $payments['fatura'] = $faturaId;
        $this->sales_model->addPayment($payments, null, $faturaId);
    }

    function adicionarParcelaFatura($data) {
        $this->db->insert('parcela_fatura', $data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function adicionarParcela($data) {

        $this->db->insert('parcela', $data);

        if ($this->db->affected_rows() == '1') return $idLancamento = $this->db->insert_id('fatura');

        return FALSE;
    }

    function adicionarFatura($data) {

        $this->db->insert('fatura', $data);

        if ($this->db->affected_rows() == '1') {
            return $idLancamento = $this->db->insert_id('fatura');
        }
        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
        return FALSE;
    }

    function getParcelaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('parcela')->row();
    }

    function getParcelaByIdContaReceber($contaReceber){
        $this->db->where('contareceberid',$contaReceber);
        $this->db->limit(1);
        return $this->db->get('parcela')->row();
    }

    function getFaturaById($faturaId){
        $this->db->select('fatura.*, companies.name as nomeCliente');
        $this->db->join('companies', 'companies.id = fatura.pessoa');

        $this->db->where('fatura.id',$faturaId);
        $this->db->limit(1);
        return $this->db->get('fatura')->row();
    }

    function getParcelasFaturaByContaPagar($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_pagar.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');
        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');
        $this->db->where('(fatura.status = "QUITADA" OR fatura.status = "ABERTA" OR  fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelasFaturaAllByContaReceber($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.id as tipoCobrancaId,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_receber.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelasFaturaByContaReceber($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.id as tipoCobrancaId, tipo_cobranca.note as note_tipo_cobranca,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_receber.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');

        $this->db->where('(fatura.status = "QUITADA" OR fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    
    function getParcelasFaturaByContaReceberCanceled($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.id as tipoCobrancaId,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_receber.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');

        $this->db->where("fatura.status", "CANCELADA");

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelaFaturaByFatura($faturaId) {
        $this->db->where('fatura',$faturaId);
        $q = $this->db->get('parcela_fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelaFaturaByParcela($parcelaId) {
        $this->db->select('fatura.*, companies.cf5 as whatsapp');
        $this->db->join('fatura', 'fatura.id = parcela_fatura.fatura');
        $this->db->join('companies', 'companies.id = fatura.pessoa');

        $this->db->where('parcela',$parcelaId);

        $this->db->limit(1);

        return $this->db->get('parcela_fatura')->row();
    }

    function getParcelasFaturaByParcela($parcelaId) {
        $this->db->where('parcela',$parcelaId);
        $q = $this->db->get('parcela_fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelaOneByFatura($faturaId) {
        $this->db->where('fatura',$faturaId);
        $this->db->limit(1);
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');

        return $this->db->get('parcela_fatura')->row();
    }

    function getParcelasByFatura($faturaId) {
        return $this->FinanceiroRepository_model->getParcelasByFatura($faturaId);
    }

    function getParcelasByContaReceberId($contaReceberId) {
        $this->db->where('contareceberId', $contaReceberId);
        $q = $this->db->get('parcela');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getCondicaoPagamentoById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('condicao_pagamento')->row();
    }

    function getCondicaoPagamentoByParcela($numeroParcela){
        $this->db->where('parcelas',$numeroParcela);
        $this->db->limit(1);
        return $this->db->get('condicao_pagamento')->row();
    }

    function getFormaPagamentoById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('forma_pagamento')->row();
    }

    function getTipoCobrancaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('tipo_cobranca')->row();
    }

    function getReceitaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('receita')->row();
    }

    function getDespesaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('despesa')->row();
    }

    function getCobrancaIntegracaoByFatura($id){
        $this->db->where('fatura',$id);
        $this->db->where('(status = "ABERTA" OR status = "QUITADA")');
        $this->db->limit(1);
        return $this->db->get('fatura_cobranca')->row();
    }

    function getCobrancaIntegracaoByCodigo($code){
        $this->db->where('code',$code);
        $this->db->where('status', 'QUITADA');
        $this->db->limit(1);
        return $this->db->get('fatura_cobranca')->row();
    }

    public function getAllReceitasSuperiores()
    {
        $this->db->where('receitasuperior', '');
        $q = $this->db->get('receita');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllDespesasSuperiores()
    {
        $this->db->where('despesasuperior', '');
        $q = $this->db->get('despesa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReceitaByReceitaSuperiorId($id)
    {
        $this->db->where('receitasuperior', $id);
        $q = $this->db->get('receita');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReceitaByDespesaSuperiorId($id)
    {
        $this->db->where('despesasuperior', $id);
        $q = $this->db->get('despesa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobrancaDebito($ocultar_interna = false)
    {
        $this->db->where('status', 'Ativo');
        $this->db->where("tipoExibir", 'receita');

        if ($ocultar_interna) {
            $this->db->where("credito_cliente", 0);
            $this->db->where("reembolso", 0);
        }

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobranca($ocultar_interna = false)
    {
        $this->db->where('status', 'Ativo');

        if ($ocultar_interna) {
            $this->db->where("credito_cliente", 0);
            $this->db->where("reembolso", 0);
        }

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllTiposCobrancaExibirLinkCompra()
    {
        $this->db->where('tipo_cobranca.status', 'Ativo');
        $this->db->where('tipo_cobranca.exibirLinkCompras', 1);
        $this->db->where('taxas_venda_configuracao.ativo', 1);

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = taxas_venda_configuracao.tipoCobrancaId');

        $this->db->order_by('tipo_cobranca.name');
        $this->db->group_by('tipo_cobranca.name');

        $q = $this->db->get('taxas_venda_configuracao');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllFormasPagamentoProdutoExibirLinkCompra($produtoID)
    {
        $this->db->select('tipo_cobranca.id as id, 
        tipo_cobranca.name, 
        tipo_cobranca.tipo, 
        tipo_cobranca.note, 
        tipo_cobranca.conta, 
        tipo_cobranca_produto.diasMaximoPagamentoAntesViagem, 
        tipo_cobranca_produto.exibirNaSemanaDaViagem');

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = tipo_cobranca_produto.tipoCobrancaId');

        $this->db->where('tipo_cobranca.status', 'Ativo');
        $this->db->where('tipo_cobranca.exibirLinkCompras', 1);
        $this->db->where('tipo_cobranca_produto.status', 1);
        $this->db->where('tipo_cobranca_produto.produtoId', $produtoID);

        $this->db->order_by('tipo_cobranca.name');
        $this->db->group_by('tipo_cobranca.name');

        $q = $this->db->get('tipo_cobranca_produto');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCondicoesPagamentoExibirLink()
    {

        $this->db->where('status', 'Ativo');
        $this->db->where('exibirLinkCompras', '1');

        $this->db->order_by('parcelas');
        $q = $this->db->get('condicao_pagamento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCondicoesPagamento()
    {
        $this->db->where('status', 'Ativo');
        $this->db->order_by('parcelas');

        $q = $this->db->get('condicao_pagamento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllContas()
    {
        $this->db->where('status', 'open');
        $q = $this->db->get('pos_register');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPagamentosByFatura($faturaId)
    {
        $this->db->order_by('id', 'asc');

        $this->db->select('payments.*, pos_register.name as conta, tipo_cobranca.name tipoCobranca');
        $this->db->join('pos_register', 'pos_register.id = payments.movimentador', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = payments.tipoCobranca', 'left');

        $q = $this->db->get_where('payments', array('fatura' => $faturaId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getContaReceberById($id)
    {
        $this->db->select('conta_receber.*, companies.name as nomeCliente');
        $this->db->join('companies', 'companies.id = conta_receber.pessoa', 'left');

        $q = $this->db->get_where("conta_receber", array('conta_receber.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function buscarParcelasRenegociacaoDividasByPessoa($pessoaId) {

        $this->db->select('parcela.*,
            conta_receber.id as contareceberId, 
            conta_receber.note,
            companies.id as clienteId,  
            companies.name as nomeCliente,  
            receita.name as receita,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId');
        $this->db->join('companies', 'companies.id = parcela.pessoa');
        $this->db->join('receita', 'receita.id = conta_receber.receita');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca');

        $this->db->where('(parcela.status = "ABERTA" OR parcela.status = "FATURADA" OR parcela.status = "PARCIAL" OR parcela.status = "VENCIDA")');

        $q = $this->db->get_where("parcela", array('parcela.pessoa' => $pessoaId) );
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscarParcelasRenegociacaoDividasByFatura($faturaId) {

        $fatura =  $this->getFaturaById($faturaId);

        $this->db->select('parcela.*,
            conta_receber.id as contareceberId, 
            conta_receber.note,
            companies.id as clienteId,  
            companies.name as nomeCliente,  
            receita.name as receita,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId');
        $this->db->join('companies', 'companies.id = parcela.pessoa');
        $this->db->join('receita', 'receita.id = conta_receber.receita');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca');

        $this->db->where('(parcela.status = "ABERTA" OR parcela.status = "FATURADA" OR parcela.status = "PARCIAL" OR parcela.status = "VENCIDA")');
        $this->db->where('conta_receber.id in ('.$fatura->contas_receber.')');

        $q = $this->db->get("parcela");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) $data[] = $row;
            return $data;
        }
        return FALSE;
    }

    private function buscarHistoricoParcelasByFaturaCredito($fatura) {

        $this->db->select('parcela.*,
            conta_receber.id as contareceberId, 
            conta_receber.conta_origem as contaOriginal, 
            conta_receber.note,
            conta_receber.reference,
            companies.id as clienteId,  
            companies.name as nomeCliente,  
            receita.name as receita,
            tipo_cobranca.id as tipoCobrancaId,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('companies', 'companies.id = parcela.pessoa', 'left');
        $this->db->join('receita', 'receita.id = conta_receber.receita', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca', 'left');
        $this->db->where('conta_receber.id in ('.$fatura->contas_receber.')');

        if ($fatura->contas_pagar != null) $this->db->where('conta_receber.id in ('.$fatura->contas_pagar.')');

        $q = $this->db->get("parcela");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) $data[] = $row;
            return $data;
        }

        return FALSE;
    }

    private function buscarHistoricoParcelasByFaturaDebito($fatura) {

        $this->db->select('parcela.*,
            conta_pagar.id as contareceberId,
            conta_pagar.note,
            conta_pagar.reference,
            companies.id as clienteId,
            companies.name as nomeCliente,
            despesa.name as receita,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId', 'left');
        $this->db->join('companies', 'companies.id = parcela.pessoa', 'left');
        $this->db->join('despesa', 'despesa.id = conta_pagar.despesa', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca', 'left');
        $this->db->where('conta_pagar.id in ('.$fatura->contas_pagar.')');

        $q = $this->db->get("parcela");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) $data[] = $row;

            return $data;
        }

        return FALSE;
    }

    public function buscarHistoricoParcelasByFatura($faturaId) {

        $fatura =  $this->getFaturaById($faturaId);

        if ($fatura->contas_receber != null)  return $this->buscarHistoricoParcelasByFaturaCredito($fatura);
        if ($fatura->contas_pagar != null) return $this->buscarHistoricoParcelasByFaturaDebito($fatura);
    }

    function getMovimentadoSaldoAnterior($dataInicio, $movimentador, $pegarDoDia = FALSE) {

        $this->db->select('sum(saldo) as saldoDoDiaAnterior');
        $this->db->join('pos_register', 'pos_register.id = movimentador_saldo_periodo.movimentador');

        if ($pegarDoDia) $this->db->where('dtmovimento <= "'.$dataInicio.'"');
        else $this->db->where('dtmovimento < "'.$dataInicio.'"');

        $this->db->where('movimentador', $movimentador);
        $this->db->limit(1);
        $encontrouUltimoMovimentoDaConta = $this->db->get('movimentador_saldo_periodo')->row();

        if ($encontrouUltimoMovimentoDaConta->saldoDoDiaAnterior == null) return 0;
        else return $encontrouUltimoMovimentoDaConta->saldoDoDiaAnterior ;
    }

    function getMovimentadorSaldoPeriodo($movimentadorId, $dataInicio, $dataFinal) {

        $this->db->select('sum(entradas) as entradas, sum(saidas) as saidas, sum(saldo) as saldo, pos_register.id as movimentadorId, pos_register.name as movimentador');
        $this->db->join('pos_register', 'pos_register.id = movimentador_saldo_periodo.movimentador');
        $this->db->where('dtmovimento >= "'.$dataInicio.'" AND dtmovimento <= "'.$dataFinal.'"');
        $this->db->group_by('movimentador');
        $this->db->where('movimentador',$movimentadorId);

        return $this->db->get('movimentador_saldo_periodo')->result();
    }

    public function isExibirPagamentoConta($status) {
        return ($status == Financeiro_model::STATUS_ABERTA ||
            $status == Financeiro_model::STATUS_FATURADA ||
            $status == Financeiro_model::STATUS_VENCIDA ||
            $status == Financeiro_model::STATUS_PARCIAL);
    }

    public function confirmacao_pagamento($fatura_cobranca, $code_integracao, $payment_data)
    {
        $this->CobrancaService_model->confirmacao_pagamento($fatura_cobranca, $code_integracao, $payment_data);
    }

    public function dezconfirmacao_pagamento($fatura_cobranca, $code_integracao)
    {
        $this->CobrancaService_model->dezconfirmacao_pagamento($fatura_cobranca, $code_integracao);
    }

}
