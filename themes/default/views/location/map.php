<?php

// Chave de API do OpenWeatherMap
$apiKey = '62c404487c57bf9de0fb059afd0ecc48';

// Cidade para obter o clima
$city = 'Palhoça';

// URL da API para obter o clima
$url = "https://api.openweathermap.org/data/2.5/weather?q={$city}&appid={$apiKey}&units=metric&lang=pt_br";

// Obtém os dados da API
$response = file_get_contents($url);

// Decodifica os dados JSON
$data = json_decode($response);



// URL da API para obter o clima
$url1 = "https://api.openweathermap.org/data/2.5/forecast?q={$city}&appid={$apiKey}&units=metric&lang=pt_br";

// Obtém os dados da API
$response1 = file_get_contents($url1);

// Decodifica os dados JSON
$data1 = json_decode($response1);


// Cria um array com os dados do clima para cada dia
$dias = array();

foreach ($data1->list as $prev) {
    // Obtém a data e hora do registro de previsão
    $timestamp = $prev->dt;

    // Obtém a data da previsão
    $dataPrev = date('Y-m-d', $timestamp);

    // Obtém a hora da previsão
    $horaPrev = date('H:i:s', $timestamp);

    // Verifica se o dia já está no array $dias
    if (!isset($dias[$dataPrev])) {
        // Cria um novo array para o dia
        $dias[$dataPrev] = array(
            'data' => $dataPrev,
            'previsoes' => array(),
        );
    }

    // Adiciona a previsão para o array do dia
    $dias[$dataPrev]['previsoes'][] = array(
        'hora' => $horaPrev,
        'descricao' => $prev->weather[0]->description,
        'icone' => $prev->weather[0]->icon,
        'temperatura' => $prev->main->temp,
    );
}

// Exibe o widget meteorológico
?>

<!DOCTYPE html>
<html>
<head>
    <title>Map</title>
</head>
<body>
<input id="address" type="textbox" value="">
<input id="submit" type="button" value="buscar">

<div id="mapa" style="height: 450px;"> </div>



<!-- Include Editor style. -->
<link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />

<!-- Create a tag that we will use as the editable area. -->
<!-- You can use a div tag as well. -->
<textarea></textarea>

<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
<script>
    new FroalaEditor('textarea');
</script>

<div style="background-color: #ffffff; border: 1px solid #cccccc; padding: 10px; width: 300px;">
    <h2><?php echo $city; ?> Previsão do Tempo</h2>
    <div style="display: flex; align-items: center; margin-bottom: 10px;">
        <div style="width: 33%; text-align: center;"><img src="https://openweathermap.org/img/w/<?php echo $data->weather[0]->icon; ?>.png" alt="<?php echo $data->weather[0]->description; ?>" title="<?php echo $data->weather[0]->description; ?>"></div>
        <div style="width: 33%; text-align: center;"><?php echo $data->weather[0]->description; ?></div>
        <div style="width: 33%; text-align: center;"><?php echo round($data->main->temp); ?>°C</div>
    </div>
    <div style="display: flex; align-items: center;">
        <div style="width: 50%; text-align: center;"><img src="https://openweathermap.org/img/w/01d.png" alt="Previsão do tempo para amanhã" title="Previsão do tempo para amanhã"></div>
        <div style="width: 33%; text-align: center;">para amanhã</div>
        <div style="width: 50%; text-align: center;"><?php echo round($data->main->temp_max); ?>°C / <?php echo round($data->main->temp_min); ?>°C</div>
    </div>
    <a href="https://openweathermap.org/city/<?php echo $data->id; ?>" target="_blank">Ver detalhes da previsão do tempo</a>
</div>



<div style="background-color: #ffffff; border: 1px solid #cccccc; padding: 10px; width: 500px;">
    <h2><?php echo $city; ?> Previsão do Tempo</h2>
    <?php foreach ($dias as $dia) { ?>
        <div style="margin-bottom: 10px;">
            <h3><?php echo date('l', strtotime($dia['data'])); ?></h3>
            <div style="display: flex; align-items: center;">
                <?php foreach ($dia['previsoes'] as $previsao) { ?>
                    <div style="flex: 1; text-align: center;">
                        <div style="font-size: 12px;"><?php echo $previsao['hora']; ?></div>
                        <div style="font-size: 16px;"><img src="https://openweathermap.org/img/w/<?php echo $previsao['icone']; ?>.png" alt="<?php echo $previsao['descricao']; ?>" title="<?php echo $previsao['descricao']; ?>"></div>
                        <div style="font-size: 14px;"><?php echo round($previsao['temperatura']); ?>°C</div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <a href="https://openweathermap.org/city/<?php echo $data->city->id; ?>" target="_blank"> Fonte </a>
</div>


<?php

$date = new DateTime(date('Y-m-d'));
$firstDay = $date->modify('first day of this month')->format('Y-m-d');
$lastDay = $date->modify('last day of this month')->format('Y-m-d');

$dias = $this->db->query("select t.data From sma_tempo t where t.data >= '".$firstDay."' and t.data <= '" . $lastDay . "' GROUP by data");
if ($dias->num_rows() > 0) {
    foreach (($dias->result()) as $row) {
        $dataDias[] = $row;
    }
}

foreach ($dataDias as $dia) {
    $diaArray[] = $this->sma->hrsd($dia->data);
}


$hits = $this->db->query("select t.data, count(r.product_id) as total, r.product_id 
from sma_tempo t left outer join sma_analytical_access_record r on (DATE_FORMAT(r.date, '%Y-%m-%d')  = t.data)
where t.data >= '" . $firstDay . "' and t.data <= '" . $lastDay . "' GROUP by t.data");

if ($hits->num_rows() > 0) {
    foreach (($hits->result()) as $row) {
        $hits_data[] = (double) $row->total;
    }
}



$hits2 = $this->db->query("select t.data, count(r.product_id) as total_acesso, GROUP_CONCAT(r.product_id SEPARATOR '@') as id, GROUP_CONCAT(p.name SEPARATOR '@') as produtos
from sma_tempo t left outer join sma_analytical_access_record r on (DATE_FORMAT(r.date, '%Y-%m-%d')  = t.data)
	             left outer join sma_products p on (r.product_id = p.id)
where t.data >= '" . $firstDay . "' and t.data <= '" . $lastDay . "' GROUP by t.data order by data, p.name");

if ($hits2->num_rows() > 0) {
    foreach (($hits2->result()) as $row) {
        $hits2_data[] = array(
                'data' => $row->data,
                'total_acesso' =>  $row->total_acesso,
                'produtos' =>  $row->produtos,
        );
    }
}


$products = []; // Array para armazenar as séries

// Loop para percorrer os resultados do primeiro conjunto de dados
foreach ($hits2_data as $row) {
    $names = explode('@', $row['produtos']); // Nome da série
    foreach ($names as $name) {
        if (!$name) continue;

        $products[$name] = [
            'name' => $name,
        ];
    }
}

foreach ($products as $product) {
    foreach ($hits2_data as $row) {
        $products[$product['name']]['data'][] = $row['data'];
        $names = explode('@', $row['produtos']); // Nome da série

        $total_acessos = 0;
        foreach ($names as $name) {
            if ($product['name'] == $name) {
                $total_acessos = $total_acessos + 1;
            }
        }

        //$products[$product['name']]['total_acesso'][] = $row['total_acesso'];
        $products[$product['name']]['total_acesso'][] = $total_acessos;

    }
}
?>

<div class="box" style="margin-bottom: 15px;">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('record_store_access_day'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-12">
                <div id="record_store_access_day" style="width:100%; height:450px;"></div>
                <p class="text-center"><?= lang("record_store_access_day"); ?></p>
            </div>
        </div>
    </div>
</div>

<div class="box" style="margin-bottom: 15px;">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('record_store_access_day_by_products'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-12">
                <div id="record_store_access_day_by_products" style="width:100%; height:450px;"></div>
                <p class="text-center"><?= lang("record_store_access_day_by_products"); ?></p>
            </div>
        </div>
    </div>
</div>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>

<script type="text/javascript">

    var data = [
        {type: 'column', name: 'Páginas visualizadas ', data: <?=json_encode($hits_data);?>},
        {type: 'line', name: 'Páginas visualizadas', data: <?=json_encode($hits_data);?>},
    ];

    // Cria o gráfico
    $('#record_store_access_day').highcharts({
        chart: {type: 'line'},
        title: {text: '<?= lang("record_store_access_day"); ?>'},
        xAxis: {
            categories: <?=json_encode($diaArray);?>
        },
        legend: {enabled: false},
        series: data
    });


    // Cria o gráfico
    $('#record_store_access_day_by_products').highcharts({
        chart: {type: 'column'},
        title: {text: '<?= lang("record_store_access_day_by_products"); ?>'},
        xAxis: {
            categories: <?=json_encode($diaArray);?>
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count trophies'
            },
            stackLabels: {
                enabled: true
            }
        },
        legend: {
            align: 'left',
            x: 70,
            verticalAlign: 'top',
            y: 70,
            floating: true,
            backgroundColor: 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [
            <?php foreach($products as $product) {?>
                {
                    name: '<?=$product['name']?>',
                    data:  <?=json_encode($product['total_acesso']);?>,
                },
            <?php } ?>
        ],
    });
</script>

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=XXX&callback=initMap"async defer></script>
!-->

<script>

    var map = null;
    var geocoder = null;

    function initMap() {
        var map = new google.maps.Map(document.getElementById('mapa'), {
            zoom: 10,
        });

        var geocoder = new google.maps.Geocoder();
        document.getElementById('submit').addEventListener('click', function() {
            geocodeAddress(geocoder, map);
        });

        geocodeAddressAberto(geocoder, map);
    }

    function geocodeAddressAberto(geocoder, map) {

        var infowindow = new google.maps.InfoWindow({
            content: 'OLHA SO'
        });

        geocoder.geocode({'address': 'Rua Giovanni Pisano 221, Aririu - Palhoça Santa Catariana 88135432' }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    title: 'TESTE',
                    map: map,
                    animation: google.maps.Animation.DROP,
                    position: results[0].geometry.location,
                });

                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }
        });
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

</script>
</body>
</html>