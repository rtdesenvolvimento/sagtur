<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update349 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '3.4.9'), array('setting_id' => 1));
    }

    public function create_index() {
        try {
            //sma_fatura_cobranca
            $this->db->query('CREATE INDEX indxs_fc_fatura	ON sma_fatura_cobranca (fatura)');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
