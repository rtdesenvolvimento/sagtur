<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CobrancaService_model extends CI_Model
{
    public function __construct() {

        parent::__construct();

        $this->load->model('dto/CobrancaDTO_model', 'CobrancaDTO_model');
        $this->load->model('dto/PessoaCobrancaDTO_model', 'PessoaCobrancaDTO_model');
        $this->load->model('dto/CobrancaFaturaDTO_model', 'CobrancaFaturaDTO_model');

        $this->load->model('service/JunoService_model', 'JunoService_model');
        $this->load->model('service/PagSeguroService_model', 'PagSeguroService_model');
        $this->load->model('service/MercadoPagoService_model', 'MercadoPagoService_model');
        $this->load->model('service/CobreFacilService_model', 'CobreFacilService_model');
        $this->load->model('service/AsaasService_model', 'AsaasService_model');
        $this->load->model('service/ValepayService_model', 'ValepayService_model');

        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');

        $this->load->model('financeiro_model');

        $this->lang->load('financeiro', $this->Settings->user_language);
    }

    public function configurarIntegracao() {
        $this->JunoService_model->configurarIntegracao();
        $this->PagSeguroService_model->configurarIntegracao();
        $this->MercadoPagoService_model->configurarIntegracao();
        $this->CobreFacilService_model->configurarIntegracao();
        $this->AsaasService_model->configurarIntegracao();
        $this->ValepayService_model->configurarIntegracao();
    }

    public function consultaSaldo() {
        $this->configurarIntegracao();

        return $this->JunoService_model->consultaSaldo();
    }

    public  function consultaSaldoAsaas() {
        return $this->AsaasService_model->consultaSaldo();
    }


    public function confirmacao_pagamento($fatura_cobranca, $code_integracao, $payment_data)
    {
        $this->configurarIntegracao();

        if ($this->isAsaas($fatura_cobranca->integracao)) {
            $this->AsaasService_model->confirmacao_pagamento($code_integracao, $payment_data);
        }

    }

    public function dezconfirmacao_pagamento($fatura_cobranca, $code_integracao)
    {
        $this->configurarIntegracao();

        if ($this->isAsaas($fatura_cobranca->integracao)) {
            $this->AsaasService_model->dezconfirmacao_pagamento($code_integracao);
        }

    }

    public  function impressao_carne_asaas($cod_installment) {

        $this->configurarIntegracao();

        return $this->AsaasService_model->getBeefPdf($cod_installment);
    }

    public  function consultar_webhook_cobranca_asaas() {
        return $this->AsaasService_model->consultar_webhook_cobranca();
    }

    public function habilitar_webhook_asaas() {
        return $this->AsaasService_model->habilitar_webhook();
    }

    public function geraCobrancasFatura($cobrancasFatura) {

        $this->configurarIntegracao();

        $retorno = '';

        foreach ($cobrancasFatura->faturas as $fatura) {

            $tipoCobranca   = $this->financeiro_model->getTipoCobrancaById($fatura->tipoCobranca);
            $retorno        = FALSE;

            if ($this->permiteGerarCobrancaPorTipoCobranca($tipoCobranca)) {

                if ($this->isGerarContaNoCarne($cobrancasFatura, $fatura, $tipoCobranca)) {
                    break;//TODO para o fluxo para nao gerar faturas cobranca para as proximas faturas
                } else {
                    $retorno = $this->geraCobrancaPorFatura($cobrancasFatura, $fatura);
                }

            }
        }

        return $retorno;
    }

    private function isGerarContaNoCarne($cobrancasFatura, $fatura, $tipoCobranca): bool
    {

        if (!$this->isCarne($tipoCobranca) ) {
            return FALSE;
        }

        $dtoRetornoCobranca = $this->emitirCobranca($cobrancasFatura, $fatura, $tipoCobranca, null);
        $contador = 0;

        if(!$dtoRetornoCobranca->sucesso) {
            throw new Exception($dtoRetornoCobranca->erro);
        }

        if ($this->isAsaas($tipoCobranca->integracao)) {

            foreach ($dtoRetornoCobranca->datas as $data) {

                $installments   = $this->AsaasService_model->getParcelamento($data['cod_installment']);

                usort($cobrancasFatura->faturas, function($a, $b) {
                    return strcmp($a->dtfaturamento, $b->dtfaturamento);
                });

                usort($installments->data, function($a, $b) {
                    return strcmp($a->dueDate, $b->dueDate);
                });

                foreach ($cobrancasFatura->faturas as $index => $fatura) {
                    $installment = $installments->data[$index];
                    if (!isset($data_invoice[$fatura->id])) {
                        $data_invoice[$fatura->id] = array(
                            'fatura' => $fatura->id,
                            'status' => $data['status'],
                            'tipo' => $data['tipo'],
                            'cod_installment' => $data['cod_installment'],
                            'integracao' => 'asaas',
                            'status_detail' => 'created',
                            'code' => $installment->id,
                            'dueDate' => $installment->dueDate,
                            'installmentLink' => $data['installmentLink'],
                            'link' => $data['installmentLink'],
                            'checkoutUrl' => $data['installmentLink'],
                            'log' => $this->sma->tirarAcentos(print_r($installment, true)),
                        );
                    }
                }

                foreach ($data_invoice as $iData) {

                    $parcelasFatura = $this->financeiro_model->getParcelaFaturaByFatura($iData['fatura']);

                    foreach ($parcelasFatura as $parcelaFatura) {
                        $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
                        $this->adicionaVinculoCobrancaFatura($iData, $parcela);
                    }
                }
            }

        } else {

            foreach ($dtoRetornoCobranca->datas as $data) {

                $data['fatura'] = $cobrancasFatura->faturas[$contador]->id;

                $parcelasFatura = $this->financeiro_model->getParcelaFaturaByFatura($data['fatura']);

                foreach ($parcelasFatura as $parcelaFatura) {
                    $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
                    $this->adicionaVinculoCobrancaFatura($data, $parcela);
                }

                $contador++;
            }
        }

        return TRUE;
    }

    private function geraCobrancaPorFatura($cobrancasFatura, $fatura) {

        $parcelasFatura = $this->financeiro_model->getParcelaFaturaByFatura($fatura->id);

        foreach ($parcelasFatura as $parcelaFatura) {

            $parcela            = $this->__getById('parcela', $parcelaFatura->parcela);
            $tipoCobranca       = $this->financeiro_model->getTipoCobrancaById($parcela->tipocobranca);
            $dtoRetornoCobranca = $this->emitirCobranca($cobrancasFatura, $fatura, $tipoCobranca, $parcela);

            if($this->errorAoProcessarPagamento($dtoRetornoCobranca)) {
                throw new Exception($dtoRetornoCobranca->erro);
            }

            foreach ($dtoRetornoCobranca->datas as $data) {
                return $this->adicionaVinculoCobrancaFatura($data, $parcela);
            }
        }
        return true;
    }

    private function errorAoProcessarPagamento($dtoRetornoCobranca) {
        return !$dtoRetornoCobranca->sucesso;
    }

    private function emitirCobranca($cobrancasFatura, $fatura, $tipoCobranca, $parcela) {

        $condicaoPagamento  = $this->financeiro_model->getCondicaoPagamentoById($fatura->condicaoPagamento);
        $valorPagar         = $fatura->valorpagar;
        $dtVencimento       = date('d/m/Y', strtotime($fatura->dtvencimento));
        $nParcelas          = $parcela == null ? $condicaoPagamento->parcelas : 1;
        $tipoCobrancaTipo   = $tipoCobranca->tipo;
        $integracao         = $tipoCobranca->integracao;
        $pessoaId           = $fatura->pessoa;
        $instrucoes         = $this->escreverInstrucoesCobranca($parcela,$fatura, $tipoCobranca, $nParcelas);
        $cobranca           = $this->preencherCobranca($cobrancasFatura, $fatura, $dtVencimento, $instrucoes, $valorPagar, $nParcelas  , $tipoCobrancaTipo, $pessoaId);

        if ($this->isPagSeguro($integracao)) return $this->PagSeguroService_model->emitirCobranca($cobranca);
        if ($this->isJuno($integracao)) return $this->JunoService_model->emitirCobranca($cobranca);
        if ($this->isMercadoPago($integracao)) return $this->MercadoPagoService_model->emitirCobranca($cobranca);
        if ($this->isCobreFacil($integracao)) return $this->CobreFacilService_model->emitirCobranca($cobranca);
        if ($this->isAsaas($integracao)) return $this->AsaasService_model->emitirCobranca($cobranca);
        if ($this->isValepay($integracao)) return $this->ValepayService_model->emitirCobranca($cobranca);

        return null;
    }

    private function escreverInstrucoesCobranca($parcela, $fatura, $tipoCobranca, $nParcelas) {

        $receita            = $this->financeiro_model->getReceitaById($fatura->receita);
        $reference          = $fatura->reference;
        $tipoCobrancaTipo   =  $tipoCobranca->tipo;
        $instrucoes         = '';

        $itens =  $this->sales_model->getSaleItemByContaReceber($fatura->contas_receber);

        foreach ($itens as $item) {
            $instrucoes .= "Serviço ".$item->product_name;
        }

        if ($fatura->note) {
            $nota = strip_tags($fatura->note);
            $nota = str_replace ( ':', ' - ', $nota);
            $nota = str_replace ( '&', ' - ', $nota);
            $instrucoes .= $nota.' ';
        }

        if ($receita->name) {
            $instrucoes .= ' (' . $receita->name . ') ';
        }

        if ($parcela == null) {
            if ($tipoCobrancaTipo == 'carne_cartao' || $tipoCobrancaTipo == 'cartao' ) {
                $instrucoes .= ' - '.$reference." - Cartão de Crédito";
            } else {
                $instrucoes .= ' - '.$reference." - Carnê em ".$nParcelas."X";
            }
        } else {
            if ($tipoCobrancaTipo == 'carne_cartao' || $tipoCobrancaTipo == 'cartao' ) {
                $instrucoes .= ' - '.$reference." - Cartão de Crédito";
            } else {
                $instrucoes .= ' - '.$reference." - Parcela " . $parcela->numeroparcela . ' de ' . $parcela->totalParcelas;
            }
        }

        return $instrucoes;
    }

    private function adicionaVinculoCobrancaFatura($data, $parcela) {

        if ($data['fatura'] == NULL) {
            return false;
        }

        $this->__edit('fatura', array('numero_transacao'=> $data['code']), 'id', $data['fatura']);

        $this->db->insert('fatura_cobranca', $data);

        $contaReceber  = $this->__getById('conta_receber',  $parcela->contareceberId);

        $tipo = $data['tipo'];

        if ($data['tipo'] == 'boleto') {
            $tipo = 'Boleto';
        } else if ($data['tipo'] == 'carne') {
            $tipo = 'Carnê';
        } else if ($data['tipo'] == 'carne_cartao') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'carne_cartao_transparent') {
            $tipo = 'Cartão de Crédito Transparente';
        } else if ($data['tipo'] == 'carne_cartao_transparent_mercado_pago') {
            $tipo = 'Cartão de Crédito Transparente';
        } else if ($data['tipo'] == 'cartao_credito_transparent_valepay') {
            $tipo = 'Cartão de Crédito Transparente';
        }  else if ($data['tipo'] == 'link_pagamento') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'pix') {
            $tipo = 'PIX';
        } else if ($data['tipo'] == 'boleto_pix') {
            $tipo = 'BOLETO PIX';
        } else if ($data['tipo'] == 'cartao_credito_link') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'cartao_credito_link') {
            $tipo = 'Mensalidade';
        }

        $event = "Criação da Integração de Pagamento: ".strtoupper($data['integracao']).' com '.strtoupper($tipo);

        if ($data['errorProcessamento']) {
            $event .= '<br/>Erro: '.$data['errorProcessamento'];
        }

        $this->SaleEventService_model->transacao_pagamento($contaReceber->sale, $event, $data['status_detail']);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    public function estornarCobranca($pagamentoId) {
        $this->configurarIntegracao();

        $pagamento = $this->__getById('payments', $pagamentoId);

        if ($pagamento->approval_code == null) {
            return true;
        }

        $fatura     = $this->__getById('fatura', $pagamento->fatura);
        $integracao = $this->__getById('tipo_cobranca', $fatura->tipoCobranca)->integracao;
        $cobranca   = $this->financeiro_model->getCobrancaIntegracaoByCodigo($pagamento->approval_code);

        if ($this->isJuno($integracao)) {
            $this->JunoService_model->estornarCobranca($cobranca->code);
        }

        if ($this->isPagSeguro($integracao)){
            //TODO ESSA PARTE DO CANCELAMENTO ESTA COMENTADA POIS FUTURAMENTE VAMOS CRIAR UMA CONFIGURAÇÃO PARA OPTAR
            //TODO EM CANCELAR OU NÃO, POIS TEM AGENCIA QUE NÃO DEVOLVEM O VALOR CANCELADO.
            //$this->PagSeguroService_model->estornarCobranca($cobranca->code);
        }

        return $this->financeiro_model->edit('fatura_cobranca', array('status' => Financeiro_model::STATUS_ESTORNADA),'id', $cobranca->id);
    }

    public function cancelarCobranca($faturaId, $segunda_via = false) {

        $this->configurarIntegracao();

        $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($faturaId);

        if (count($cobranca) == 0) {
            return true;
        }

        $fatura         = $this->__getById('fatura', $faturaId);
        $tipoCobranca   = $this->__getById('tipo_cobranca', $fatura->tipoCobranca);
        $integracao     =  $tipoCobranca->integracao;

        if ($this->isJuno($integracao)) {
            $this->JunoService_model->cancelarCobranca($cobranca->code);
        }

        if ($this->isCobreFacil($integracao)) {
            $this->CobreFacilService_model->cancelarCobranca($cobranca->code);
        }

        if ($this->isAsaas($integracao)) {
            $this->AsaasService_model->cancelarCobranca($cobranca->code);
        }

        if ($this->isValepay($integracao)) {
            $this->ValepayService_model->cancelarCobranca($cobranca->code);
        }

        if ($this->isPagSeguro($integracao)) {
            //TODO ESSA PARTE DO CANCELAMENTO ESTA CANCELADO POIS FUTURAMENTE VAMOS CRIAR UMA CONFIGURAÇÃO PARA OPTAR
            //TODO EM CANCELAR OU NÃO, POIS TEM AGENCIA QUE NÃO DEVOLVEM O VALOR CANCELADO.
            //$this->PagSeguroService_model->cancelarCobranca($cobranca->code);
        }

        if ($segunda_via) {
            return $this->financeiro_model->edit('fatura_cobranca', array('status' => Financeiro_model::STATUS_SEGUNDA_VIA),'id', $cobranca->id);
        } else {
            return $this->financeiro_model->edit('fatura_cobranca', array('status' => Financeiro_model::STATUS_CANCELADA),'id', $cobranca->id);
        }
    }

    public function consultaPagamentoByCode($code) {
        return $this->PagSeguroService_model->buscarPagamentoByCodeTransacao($code);
    }

    public function buscarDadosTransacaoByReference($code) {
        return $this->PagSeguroService_model->buscarDadosTransacaoByReference($code);
    }

    public function baixarPagamentoIntegracaoReference($integracao, $code) {
        $this->configurarIntegracao();

        if ($this->isPagSeguro($integracao)) return $this->PagSeguroService_model->baixarPagamentoIntegracaoReference($code);

        if ($this->isJuno($integracao)) throw new Exception('Não é possível baixar via refernce');
        if ($this->isMercadoPago($integracao))  throw new Exception('Não é possível baixar via refernce');
        if ($this->isCobreFacil($integracao))  throw new Exception('Não é possível baixar via refernce');

        return [];
    }

    public function baixarPagamentoIntegracao($integracao, $code) {
        $this->configurarIntegracao();

        if ($this->isPagSeguro($integracao)) return $this->PagSeguroService_model->buscarPagamentoByCodeTransacao($code);
        if ($this->isJuno($integracao)) return $this->JunoService_model->buscarPagamentoByCodeTransacao($code);
        if ($this->isMercadoPago($integracao)) return $this->MercadoPagoService_model->buscarPagamento($code);
        if ($this->isCobreFacil($integracao)) return $this->CobreFacilService_model->buscarPagamento($code);
        if ($this->isAsaas($integracao)) return $this->AsaasService_model->buscarPagamento($code);
        if ($this->isValepay($integracao)) return $this->ValepayService_model->buscarPagamento($code);

        return [];
    }

    private function isPagSeguro($integracao) {
        return $integracao == 'pagseguro';
    }

    private function isJuno($integracao) {
        return $integracao == 'juno';
    }

    private function isMercadoPago($integracao) {
        return $integracao == 'mercadopago';
    }

    private function isCobreFacil($integracao) {
        return $integracao == 'cobrefacil';
    }

    private function isAsaas($integracao) {
        return $integracao == 'asaas';
    }

    private function isValepay($integracao) {
        return $integracao == 'valepay';
    }

    private function preencherCobranca($cobrancasFatura, $fatura, $dtVencimento, $instrucoes, $valorPagar, $parcelas, $tipoCobranca, $pessoa) {
        $cobranca = new CobrancaDTO_model();

        if ($cobrancasFatura->numeroParcelas != null) {
            $parcelas = $cobrancasFatura->numeroParcelas;
        }

        $cobranca->fatura = $fatura->id;
        $cobranca->dataVencimento = $dtVencimento;
        $cobranca->descricaoCobranca = $instrucoes;
        $cobranca->valorVencimento = $valorPagar;
        $cobranca->numeroParcelas = $parcelas;
        $cobranca->tipoCobranca = $tipoCobranca;
        $cobranca->instrucoes = $instrucoes;

        //field cc
        $cobranca->senderHash = $cobrancasFatura->senderHash;
        $cobranca->deviceId =  $cobrancasFatura->deviceId;
        $cobranca->creditCardToken = $cobrancasFatura->creditCardToken;

        $cobranca->cardName = $cobrancasFatura->cardName;
        $cobranca->cardNumber = $cobrancasFatura->cardNumber;
        $cobranca->cardExpiryMonth = $cobrancasFatura->cardExpiryMonth;
        $cobranca->cardExpiryYear = $cobrancasFatura->cardExpiryYear;
        $cobranca->cvc = $cobrancasFatura->cvc;

        $cobranca->cpfTitularCartao = $cobrancasFatura->cpfTitularCartao;
        $cobranca->celularTitularCartao = $cobrancasFatura->celularTitularCartao;
        $cobranca->cepTitularCartao = $cobrancasFatura->cepTitularCartao;
        $cobranca->enderecoTitularCartao = $cobrancasFatura->enderecoTitularCartao;
        $cobranca->numeroEnderecoTitularCartao = $cobrancasFatura->numeroEnderecoTitularCartao;
        $cobranca->complementoEnderecoTitularCartao = $cobrancasFatura->complementoEnderecoTitularCartao;
        $cobranca->bairroTitularCartao = $cobrancasFatura->bairroTitularCartao;
        $cobranca->cidadeTitularCartao = $cobrancasFatura->cidadeTitularCartao;
        $cobranca->estadoTitularCartao = $cobrancasFatura->estadoTitularCartao;
        $cobranca->totalParcelaPagSeguro = $cobrancasFatura->totalParcelaPagSeguro;

        $cobranca->venda_manual = $cobrancasFatura->venda_manual;

        $cobranca->pessoa = $this->preencherPessoaCobranca($pessoa);

        return $cobranca;
    }

    private function preencherPessoaCobranca($pessoaId) {
        $pessoa = $this->companies_model->getCompanyByID($pessoaId);

        $nome = $pessoa->name;
        $cpf = $pessoa->vat_no;
        $numero = $pessoa->numero;
        $complemento = $pessoa->complemento;
        $bairro = $pessoa->bairro;
        $endereco = $pessoa->address;
        $cidade = $pessoa->city;
        $estado = $pessoa->state;
        $cep = $pessoa->postal_code;
        $telefone = $pessoa->phone;
        $whatsApp = $pessoa->cf5;
        $email = $pessoa->email;
        $tipoPessoa = $pessoa->tipoPessoa;
        $data_aniversario = $pessoa->data_aniversario;
        $telefonePagSeguro = '';
        $sexo = $pessoa->sexo;
        $rg = $pessoa->cf1;

        $cpf = str_replace ( '-', '', str_replace ( '.', '', $cpf));

        if ($this->session->userdata('cnpjempresa') == 'andrevelho') {
            if ($endereco == '') $endereco = ' Rua Giovanni Pisano';
            if ($bairro == '') $bairro = 'Aririu';
            if ($cidade == '') $cidade = 'Palhoça';
            if ($estado == '') $estado = 'SC';
            if ($numero == '') $numero = '21';
            if ($cep == '') $cep = '88135432';

            $telefonePagSeguro = '998235746';
            $dddCelular = '48';
        }

        if ($this->session->userdata('cnpjempresa') == 'demonstracaosagtur') {
            if ($endereco == '') $endereco = ' Rua Giovanni Pisano';
            if ($bairro == '') $bairro = 'Aririu';
            if ($cidade == '') $cidade = 'Palhoça';
            if ($estado == '') $estado = 'SC';
            if ($numero == '') $numero = '21';
            if ($cep == '') $cep = '88135432';

            $telefonePagSeguro = '998235746';
            $dddCelular = '48';
        }

        if ($this->session->userdata('cnpjempresa') == 'tripdajana') {
            if ($endereco == '') $endereco = 'Rua Lamartine Babo';
            if ($bairro == '') $bairro = 'Jardim Santo Onofre';
            if ($cidade == '') $cidade = 'Taboao da Serra';
            if ($estado == '') $estado = 'São Paulo';
            if ($numero == '') $numero = '149';
            if ($cep == '') $cep = '01535000';

            $telefonePagSeguro = '966367987';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'amigostur') {
            if ($endereco == '') $endereco = 'R. DD';
            if ($bairro == '') $bairro = 'Vila Cruzeirinho';
            if ($cidade == '') $cidade = 'Vespa Siano';
            if ($estado == '') $estado = 'MG';
            if ($numero == '') $numero = '16';
            if ($cep == '') $cep = '33205010';

            $telefonePagSeguro = '994934626';
            $dddCelular = '31';
        }

        if ($this->session->userdata('cnpjempresa') == 'muralhatour') {
            if ($endereco == '') $endereco = ' Louis Armstrong';
            if ($bairro == '') $bairro = 'Centreville';
            if ($cidade == '') $cidade = 'Santo André';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '65';
            if ($cep == '') $cep = '09120080';

            $telefonePagSeguro = '975957945';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'plenitude') {
            if ($endereco == '') $endereco = 'Avenida Maria Socorro e Silva Bezerra';
            if ($bairro == '') $bairro = 'Jardim Nova Cidade';
            if ($cidade == '') $cidade = 'Guarulhos';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '1400';
            if ($cep == '') $cep = '07252300';

            $telefonePagSeguro = '949815249';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'lvtour') {
            if ($endereco == '') $endereco = 'Rua Argos';
            if ($bairro == '') $bairro = 'Guadalupe';
            if ($cidade == '') $cidade = 'Rio de Janeiro';
            if ($estado == '') $estado = 'RJ';
            if ($numero == '') $numero = '847';
            if ($cep == '') $cep = '21660050';

            $telefonePagSeguro = '975436279';
            $dddCelular = '21';
        }

        if ($this->session->userdata('cnpjempresa') == 'semprevix') {
            if ($endereco == '') $endereco = 'Rua Belmiro Teixeira Pimenta, 100, loja 03';
            if ($bairro == '') $bairro = 'Jardim Camburi';
            if ($cidade == '') $cidade = 'Vitória';
            if ($estado == '') $estado = 'ES';
            if ($numero == '') $numero = '03';
            if ($cep == '') $cep = '29090600';

            $telefonePagSeguro = '998865954';
            $dddCelular = '27';
        }

        if ($this->session->userdata('cnpjempresa') == 'alcturismo') {
            if ($endereco == '') $endereco = 'Rua José de Souza Porto';
            if ($bairro == '') $bairro = 'Vista Alegre';
            if ($cidade == '') $cidade = 'São Gonçalo';
            if ($estado == '') $estado = 'RJ';
            if ($numero == '') $numero = '104';
            if ($cep == '') $cep = '24723350';

            $telefonePagSeguro = '972074017';
            $dddCelular = '21';
        }

        if ($this->session->userdata('cnpjempresa') == 'livreacessoturismoeviagens') {
            if ($endereco == '') $endereco = 'Rua tadao Inoue';
            if ($bairro == '') $bairro = 'Colônia (Zona Sul)';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '26';
            if ($cep == '') $cep = '04875005';

            $telefonePagSeguro = '970901555';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'penaareia') {
            if ($endereco == '') $endereco = 'R AVIADORA ANESIA PINHEIRO MACHADO';
            if ($bairro == '') $bairro = 'CONJUNTO HABITACIONAL PARQUE VALO VELHO II';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '172';
            if ($cep == '') $cep = '05886610';

            $telefonePagSeguro = '970390460';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'angulotravel') {
            if ($endereco == '') $endereco = 'Estrada dos Mirandas';
            if ($bairro == '') $bairro = 'Jardim Maria Duarte';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '210';
            if ($cep == '') $cep = '05752001';

            $telefonePagSeguro = '988214106';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'mpwturismo') {
            if ($endereco == '') $endereco = 'Rua Amaro Luz';
            if ($bairro == '') $bairro = 'Socorro';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '170';
            if ($cep == '') $cep = '04764010';

            $telefonePagSeguro = '954022924';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'fenixxturismo') {
            if ($endereco == '') $endereco = 'Rua: Sebastião José da Costa';
            if ($bairro == '') $bairro = 'Jardim Rebouças';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '66';
            if ($cep == '') $cep = '05734200';

            $telefonePagSeguro = '996440222';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'cksturismo') {
            if ($endereco == '') $endereco = 'Rua Tibagi';
            if ($bairro == '') $bairro = 'Estância Lago azul';
            if ($cidade == '') $cidade = 'Franco da Rocha';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '459';
            if ($cep == '') $cep = '07866030';

            $telefonePagSeguro = '947469424';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'azevedoadventuretour') {

            if ($endereco == '') $endereco = 'RUA SATÉLITE ARIEL';
            if ($bairro == '') $bairro = 'JARDIM CASA GRANDE';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '52';
            if ($cep == '') $cep = '04858490';

            $telefonePagSeguro = '951082004';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'goldtourtrip') {

            if ($endereco == '') $endereco = 'Av. Dr Sylvio de Campos';
            if ($bairro == '') $bairro = 'Perus';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '1219';
            if ($cep == '') $cep = '05211000';

            $telefonePagSeguro = '986530841';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'bhexpressoturismo') {

            if ($endereco == '') $endereco = 'Rua Mario Pederneirs';
            if ($bairro == '') $bairro = 'Santa Mônica';
            if ($cidade == '') $cidade = 'Belo Horizontes';
            if ($estado == '') $estado = 'MG';
            if ($numero == '') $numero = '129';
            if ($cep == '') $cep = '31525300';

            $telefonePagSeguro = '994717776';
            $dddCelular = '31';
        }

        if ($this->session->userdata('cnpjempresa') == 'vemcomigotrip') {

            if ($endereco == '') $endereco = 'RUA Sabi';
            if ($bairro == '') $bairro = 'Ayrosa';
            if ($cidade == '') $cidade = 'Osasco';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '177';
            if ($cep == '') $cep = '06293040';

            $telefonePagSeguro = '987596594';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'roturismo') {

            if ($endereco == '') $endereco = 'Rua: Antônio Caserta';
            if ($bairro == '') $bairro = 'Jd Apurá';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '88';
            if ($cep == '') $cep = '04470060';

            $telefonePagSeguro = '947453621';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'viajandobrasilafora') {

            if ($endereco == '') $endereco = 'Rua: Rua São Boaventura';
            if ($bairro == '') $bairro = 'Vila Scarpelli';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '115';
            if ($cep == '') $cep = '09050380';

            $telefonePagSeguro = '958878216';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'paulovasconcellos') {

            if ($endereco == '') $endereco = 'Av. Osvaldo José do Amaral - Anexo ao Bistek Via Torres | Sala 05';
            if ($bairro == '') $bairro = 'Bela Vista';
            if ($cidade == '') $cidade = 'São Jose';
            if ($estado == '') $estado = 'SC';
            if ($numero == '') $numero = '00';
            if ($cep == '') $cep = '88110798';

            $telefonePagSeguro = '999197014';
            $dddCelular = '48';
        }

        if ($this->session->userdata('cnpjempresa') == 'riosexcursoes') {

            if ($endereco == '') $endereco = 'Rua: Marechal Deodor 101';
            if ($bairro == '') $bairro = 'Centro';
            if ($cidade == '') $cidade = 'Mairi';
            if ($estado == '') $estado = 'Bahia ';
            if ($numero == '') $numero = '101 ';
            if ($cep == '') $cep = '44630000';

            $telefonePagSeguro = '999314519';
            $dddCelular = '71';
        }

        if ($this->session->userdata('cnpjempresa') == 'mochilaselfie') {

            if ($endereco == '') $endereco = 'Rua Paratiba';
            if ($bairro == '') $bairro = 'Jardim Nordeste';
            if ($cidade == '') $cidade = 'Sao Paulo';
            if ($estado == '') $estado = 'SP ';
            if ($numero == '') $numero = '251 ';
            if ($cep == '') $cep = '03688010';

            $telefonePagSeguro = '970356142';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'uppertrip') {

            if ($endereco == '') $endereco = 'Avenida da Saúde - Sala 2';
            if ($bairro == '') $bairro = 'Santa Cruz';
            if ($cidade == '') $cidade = 'Mogi Mirim';
            if ($estado == '') $estado = 'SP ';
            if ($numero == '') $numero = '206';
            if ($cep == '') $cep = '13800700';

            $telefonePagSeguro = '996055665';
            $dddCelular = '19';
        }

        if ($this->session->userdata('cnpjempresa') == 'italiaturismo') {

            if ($endereco == '') $endereco = 'Rua Cícero Faustino';
            if ($bairro == '') $bairro = 'Centro';
            if ($cidade == '') $cidade = 'Lagoa Seca';
            if ($estado == '') $estado = 'PB';
            if ($numero == '') $numero = '228 ';
            if ($cep == '') $cep = '58117000';

            $telefonePagSeguro = '996181545';
            $dddCelular = '83';
        }

        if ($this->session->userdata('cnpjempresa') == 'manniadiviagem') {

            if ($endereco == '') $endereco = 'Rua Zenon Arantes Galvão';
            if ($bairro == '') $bairro = 'Jardim Bela Vista';
            if ($cidade == '') $cidade = 'Itapetininga';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '234';
            if ($cep == '') $cep = '17207750';

            $telefonePagSeguro = '997958684';
            $dddCelular = '15';
        }

        if ($this->session->userdata('cnpjempresa') == 'tripforever') {

            if ($endereco == '') $endereco = 'Rua Benedito Fernandes  Sala 617 ';
            if ($bairro == '') $bairro = 'Santo Amaro';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '545';
            if ($cep == '') $cep = '04746110';

            $telefonePagSeguro = '966676022';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'tatatur') {

            if ($endereco == '') $endereco = 'Rua São Severiano';
            if ($bairro == '') $bairro = 'Penha';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '43';
            if ($cep == '') $cep = '03613030';

            $telefonePagSeguro = '966911172';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'viajecomanegaturismo') {

            if ($endereco == '') $endereco = 'Rua Tacacazeiro ';
            if ($bairro == '') $bairro = 'Jardim Eliane';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '255';
            if ($cep == '') $cep = '03577030';

            $telefonePagSeguro = '948808213';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'stylosturismoeviagens') {

            if ($endereco == '') $endereco = 'Rua Pedro Américo';
            if ($bairro == '') $bairro = 'Jardim Portinari';
            if ($cidade == '') $cidade = 'Diadema';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '179';
            if ($cep == '') $cep = '09961620';

            $telefonePagSeguro = '952236962';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'doisforasteiros') {

            if ($endereco == '') $endereco = 'RUA CONDE JULIANO';
            if ($bairro == '') $bairro = 'Jardim  VILA HUMAITÁ';
            if ($cidade == '') $cidade = 'SANTO ANDRÉ';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '340';
            if ($cep == '') $cep = '04809200';

            $telefonePagSeguro = '977425934';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'viajacky') {

            if ($endereco == '') $endereco = 'R Coronel Estevam Lopes De Camargo 56';
            if ($bairro == '') $bairro = 'Vila Moreira';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '56';
            if ($cep == '') $cep = '03088050';

            $telefonePagSeguro = '980766151';
            $dddCelular = '11';
        }

        if ($this->session->userdata('cnpjempresa') == 'oliveirastrips') {

            if ($endereco == '') $endereco = 'Rua Eloi Eppinger';
            if ($bairro == '') $bairro = 'Interlago';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '305';
            if ($cep == '') $cep = '04809200';

            $telefonePagSeguro = '985047977';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'happyexcursoes') {

            if ($endereco == '') $endereco = 'Rua Muiraquitã ap 21';
            if ($bairro == '') $bairro = 'Vila Tupi';
            if ($cidade == '') $cidade = 'Praia Grande ';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '44';
            if ($cep == '') $cep = '11703760';

            $telefonePagSeguro = '982186814';
            $dddCelular = '13';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'thaistourviagens') {

            if ($endereco == '') $endereco = 'AV. CEARÁ';
            if ($bairro == '') $bairro = 'CANUDOS';
            if ($cidade == '') $cidade = 'Praia Grande ';
            if ($estado == '') $estado = 'PA';
            if ($numero == '') $numero = '1020';
            if ($cep == '') $cep = '66090460';

            $telefonePagSeguro = '981335946';
            $dddCelular = '91';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'augustusturismo') {

            if ($endereco == '') $endereco = 'RUA CEL JOÃO EVANGELISTA – GALERIA GRÉCIA – LOJA 5';
            if ($bairro == '') $bairro = 'CENTRO';
            if ($cidade == '') $cidade = 'JUAZEIRO';
            if ($estado == '') $estado = 'BA';
            if ($numero == '') $numero = '05';
            if ($cep == '') $cep = '63900209';

            $telefonePagSeguro = '988414493';
            $dddCelular = '74';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'libertytur') {

            if ($endereco == '') $endereco = 'Rua Quinze';
            if ($bairro == '') $bairro = 'Quemil';
            if ($cidade == '') $cidade = 'Birigui';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '11';
            if ($cep == '') $cep = '16202218';

            $telefonePagSeguro = '981745065';
            $dddCelular = '18';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'upexcursoes') {

            if ($endereco == '') $endereco = 'R ELADIO VICENTE FERREIRA';
            if ($bairro == '') $bairro = 'VILA NOVA';
            if ($cidade == '') $cidade = 'CUBATAO';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '175';
            if ($cep == '') $cep = '11525060';

            $telefonePagSeguro = '988125449';
            $dddCelular = '13';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'mieventoseturismo') {

            if ($endereco == '') $endereco = 'Av. Assembléia';
            if ($bairro == '') $bairro = 'Vila Élida';
            if ($cidade == '') $cidade = 'Diadema';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '903';
            if ($cep == '') $cep = '09913130';

            $telefonePagSeguro = '949608828';
            $dddCelular = '13';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'qualityturismo') {

            if ($endereco == '') $endereco = 'R. Mario Rosa Lima';
            if ($bairro == '') $bairro = 'Orlando Dantas';
            if ($cidade == '') $cidade = 'Aracaju';
            if ($estado == '') $estado = 'SE';
            if ($numero == '') $numero = '78';
            if ($cep == '') $cep = '49042320';

            $telefonePagSeguro = '991723545';
            $dddCelular = '79';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'turismojs') {

            if ($endereco == '') $endereco = 'Rua José Coutinho de Carvalho';
            if ($bairro == '') $bairro = 'São Francisco de Assis';
            if ($cidade == '') $cidade = 'Barra Mansa';
            if ($estado == '') $estado = 'RJ';
            if ($numero == '') $numero = '304';
            if ($cep == '') $cep = '27323480';

            $telefonePagSeguro = '988469373';
            $dddCelular = '24';
        }


        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        /*
        if ($this->session->userdata('cnpjempresa') == 'partiutrips') {

            if ($endereco == '') $endereco = 'Vinte um de abril';
            if ($bairro == '') $bairro = 'Centro';
            if ($cidade == '') $cidade = 'Itu';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '194';
            if ($cep == '') $cep = '13300210';

            $telefonePagSeguro = '976465136';
            $dddCelular = '11';
        }
        */

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'depalmasturismo') {

            if ($endereco == '') $endereco = 'Travessa da Confianca';
            if ($bairro == '') $bairro = 'Vila da Penha';
            if ($cidade == '') $cidade = 'Rio de Janeiro';
            if ($estado == '') $estado = 'RJ';
            if ($numero == '') $numero = '35';
            if ($cep == '') $cep = '21211260';

            $telefonePagSeguro = '976126943';
            $dddCelular = '21';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'enjoyviagens') {

            if ($endereco == '') $endereco = 'Rua Angelo Antonelli (Lot Auri-Verde)';
            if ($bairro == '') $bairro = ' Jd Varginha';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '22';
            if ($cep == '') $cep = '04857560';

            $telefonePagSeguro = '955978415';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'brotherstrips') {

            if ($endereco == '') $endereco = 'Avenida Senador Teotônio Vilela 4029';
            if ($bairro == '') $bairro = ' Vila São José';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '4287';
            if ($cep == '') $cep = '04833901';

            $telefonePagSeguro = '994459263';
            $dddCelular = '11';
        }


        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'limaturismo') {

            if ($endereco == '') $endereco = 'Passagem Solimoes (Jd S J Tadeu)';
            if ($bairro == '') $bairro = 'Campanario';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '93';
            if ($cep == '') $cep = '09930545';

            $telefonePagSeguro = '992002384';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'mtkturismo') {

            if ($endereco == '') $endereco = 'Avenida Senador Teotônio Vilela ';
            if ($bairro == '') $bairro = 'Vila São José';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '4287';
            if ($cep == '') $cep = '04833901';

            $telefonePagSeguro = '994459263';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'deoliturismo') {

            if ($endereco == '') $endereco = 'Rua Câmara Junior';
            if ($bairro == '') $bairro = 'Jardim das Américas';
            if ($cidade == '') $cidade = 'Curitiba';
            if ($estado == '') $estado = 'PR';
            if ($numero == '') $numero = '832';
            if ($cep == '') $cep = '81540000';

            $telefonePagSeguro = '996541364';
            $dddCelular = '41';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'msaturismo') {

            if ($endereco == '') $endereco = 'Rua Benedito Fernandes - Sala 617 ';
            if ($bairro == '') $bairro = 'Santo Amaro';
            if ($cidade == '') $cidade = 'São Paulo';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '545';
            if ($cep == '') $cep = '04746110';

            $telefonePagSeguro = '959474043';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'monteirodiversoes') {

            if ($endereco == '') $endereco = 'Rua Borba Gato';
            if ($bairro == '') $bairro = 'Vila Ida';
            if ($cidade == '') $cidade = 'Franco da Rocha';
            if ($estado == '') $estado = 'SP';
            if ($numero == '') $numero = '45';
            if ($cep == '') $cep = '07845220';

            $telefonePagSeguro = '51974202';
            $dddCelular = '11';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'mdiasturismo') {

            if ($endereco == '') $endereco = 'Rua Henrique Couto';
            if ($bairro == '') $bairro = 'Lourival Parente';
            if ($cidade == '') $cidade = 'Teresina';
            if ($estado == '') $estado = 'PI';
            if ($numero == '') $numero = '2611';
            if ($cep == '') $cep = '64023580';

            $telefonePagSeguro = '994824188';
            $dddCelular = '86';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'maxxitours') {

            if ($endereco == '') $endereco = 'RUA BOTAFOGO';
            if ($bairro == '') $bairro = 'Centro';
            if ($cidade == '') $cidade = 'CAXIAS DO SUL';
            if ($estado == '') $estado = 'RS';
            if ($numero == '') $numero = '1792';
            if ($cep == '') $cep = '95040150';

            $telefonePagSeguro = '989086563';
            $dddCelular = '47';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'evoluirviagens') {

            if ($endereco == '') $endereco = 'Rua Engracia Gomes de Alencar';
            if ($bairro == '') $bairro = 'Centro';
            if ($cidade == '') $cidade = 'Quixadá';
            if ($estado == '') $estado = 'CE';
            if ($numero == '') $numero = '134';
            if ($cep == '') $cep = '63901230';

            $telefonePagSeguro = '992940721';
            $dddCelular = '88';
        }

        //TODO por hora sera fixo ate que o link de reserva capture o endereço junto.
        if ($this->session->userdata('cnpjempresa') == 'oasistur') {

            if ($endereco == '') $endereco = 'Rua Professor Monteiro Fonseca';
            if ($bairro == '') $bairro = 'Vila Antonio Narciso';
            if ($cidade == '') $cidade = 'Montes Claros';
            if ($estado == '') $estado = 'MG';
            if ($numero == '') $numero = '1115';
            if ($cep == '') $cep = '39400808';

            $telefonePagSeguro = '97295385';
            $dddCelular = '38';
        }

        if ($this->session->userdata('cnpjempresa') == 'musictravel') {

            if ($endereco == '') $endereco = 'RUA SANTA CRUZ';
            if ($bairro == '') $bairro = 'CENTRO';
            if ($cidade == '') $cidade = 'UBA';
            if ($estado == '') $estado = 'MG';
            if ($numero == '') $numero = '580';
            if ($cep == '') $cep = '36500059';

            $telefonePagSeguro = '988487488';
            $dddCelular = '32';
        }

        $pessoaCobranca = new PessoaCobrancaDTO_model();

        $pessoaCobranca->id = $pessoa->id;
        $pessoaCobranca->nome = $nome;
        $pessoaCobranca->cpfCnpj = $cpf;
        $pessoaCobranca->email = $email;
        $pessoaCobranca->endereco = $endereco;
        $pessoaCobranca->cidade = $cidade;
        $pessoaCobranca->estado = $estado;
        $pessoaCobranca->cep = $cep;
        $pessoaCobranca->telefone = $telefone;
        $pessoaCobranca->numero = $numero;
        $pessoaCobranca->celular = $whatsApp;
        $pessoaCobranca->bairro = $bairro;
        $pessoaCobranca->complemento = $complemento;
        $pessoaCobranca->tipoPessoa = $tipoPessoa;
        $pessoaCobranca->dataNascimento = $data_aniversario;
        $pessoaCobranca->sexo = $sexo;
        $pessoaCobranca->rg = $rg;

        $pessoaCobranca->dddTelefonePagSeguro = $dddCelular;
        $pessoaCobranca->telefonePagSeguro = $telefonePagSeguro;

        return $pessoaCobranca;
    }

    private function isCarne($tipoCobranca): bool
    {
        if ($tipoCobranca->tipo == 'carne' ||
            $tipoCobranca->tipo == 'carne_boleto' ||
            $tipoCobranca->tipo == 'mensalidade' ||
            //$tipoCobranca->tipo == 'carne_cartao' ||
            $tipoCobranca->tipo == 'carne_boleto_cartao') {
            return TRUE;
        }
        return FALSE;
    }

    private function permiteGerarCobrancaPorTipoCobranca($tipoCobranca): bool
    {

        if ($tipoCobranca->integracao != 'nenhuma' &&
            (
                $tipoCobranca->tipo == 'boleto' || //para boletos usado no: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'boleto_pix' || //para boletos usado no: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'mensalidade' || //para boletos usado no: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'cartao_credito_link' || //para boletos usado no: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'cartao' ||//para gerar o link de pagamento: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'boleto_cartao' ||//APENAS PARA JUNO
                $tipoCobranca->tipo == 'carne' ||//APENAS PARA JUNO
                $tipoCobranca->tipo == 'carne_boleto' ||//APENAS PARA JUNO
                $tipoCobranca->tipo == 'carne_cartao' ||//APENAS PARA JUNO
                $tipoCobranca->tipo == 'carne_cartao_transparent' ||//PARA PAGSEGURO
                $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||//PARA MERCADOPAGO
                $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||//PARA VALEPAY
                $tipoCobranca->tipo == 'debito_online' ||//APENAS PARA PAGSEGURO
                $tipoCobranca->tipo == 'link_pagamento' ||//para boletos usado no: JUNO/PAGSEGURO/MERCADOPAGO
                $tipoCobranca->tipo == 'pix' ||//APENAS PARA MERCADO PAGO
                $tipoCobranca->tipo == 'carne_boleto_cartao'//APENAS PARA JUNO
            )) {
            return TRUE;
        }
        return FALSE;
    }
}