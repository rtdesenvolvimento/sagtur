<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FechamentoComissaoService_model extends CI_Model
{
    const STATUS_PENDENTE = 'Pendente';

    const STATUS_EM_REVISAO = 'Em Revisão';
    const STATUS_APROVADA = 'Aprovada';

    const STATUS_REJEITADA = 'Rejeitada';

    const STATUS_CANCELADA = 'Cancelada';

    const STATUS_PAGO = 'Pago';

    const STATUS_CONFIRMADA = 'Confirmada';

    public function __construct()
    {
        parent::__construct();

        //service
        $this->load->model('service/CommissionEventService_model', 'CommissionEventService_model');
    }

    public function addFechamentoComissao($data, $billers)
    {
        if ($this->site->getReference('fc') == $data['reference_no']) {
            //  $this->site->updateReference('fc');
        }

        if ($this->db->insert('close_commissions', $data)) {
            $fechamentoID = $this->db->insert_id();

            $this->FechamentoComissaoRepository_model->adicionar_billers($billers, $fechamentoID);

            return $fechamentoID;
        }
        return false;
    }

    public function editFechamentoComissao($data, $billers, $fechamentoID)
    {
        try {

            $this->__iniciaTransacao();
            $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamentoID);

            if ($fechamento->status == 'Confirmada') {
                throw new Exception('Fechamento já confirmado!');
            }

            if ($this->db->update('close_commissions', $data, array('id' => $fechamentoID))) {

                $this->db->delete('commission_billers', array('commission_id' => $fechamentoID));

                $this->FechamentoComissaoRepository_model->adicionar_billers($billers, $fechamentoID);

                $this->FechamentoComissaoRepository_model->atualizar_status_item_comissao_not_billers($fechamentoID);

                $this->FechamentoComissaoRepository_model->updateTotalComissao($fechamentoID);

                $this->__confirmaTransacao();

                return $fechamentoID;
            }

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }

        return false;
    }

    public function addItemFechamento($fechamentoID, $itemComissaoID, $dataItemFechamento) {

        $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamentoID);

        if ($this->naoExisteComissaoNoFechamento($itemComissaoID) && $fechamento->status == FechamentoComissaoService_model::STATUS_PENDENTE) {

            $dataItemFechamento['close_commission_id'] = $fechamentoID;

            if ($this->db->insert('close_commission_items', $dataItemFechamento)) {
                $this->FechamentoComissaoRepository_model->updateTotalComissao($fechamentoID);
                $this->CommissionsRepository_model->updateStatusItemComissao($itemComissaoID, FechamentoComissaoService_model::STATUS_EM_REVISAO);

                //log da comissao inserida
                $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($itemComissaoID);
                $note = 'Comissão inserida no fechamento ('.$fechamento->reference_no.') '.$fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);
                $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $itemComissaoID, $note, CommissionEventService_model::COMISSAO_INSERIDA_NO_FECHAMENTO, $this->session->userdata('user_id'));

                return true;
            }
        }
        return false;
    }

    public function removeItemFechamento($itemID, $itemComissaoID, $fechamentoID)
    {
        $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamentoID);
        $item = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($itemID);

        if ($item->status == FechamentoComissaoService_model::STATUS_EM_REVISAO ||
            $item->status == FechamentoComissaoService_model::STATUS_APROVADA) {

            $note = 'Comissão removida do fechamento ('.$fechamento->reference_no.') '.$fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);

            if ($item->status == FechamentoComissaoService_model::STATUS_APROVADA) {

                $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');

                $faturaComissaoGerada = $this->FechamentoComissaoRepository_model->getFaturaByCommissionItem($item->id);

                if ($faturaComissaoGerada) {
                    $this->FinanceiroService_model->cancelar($faturaComissaoGerada->id, $note);
                }
            }

            $this->db->delete('close_commission_items', array('id' => $itemID));

            $this->FechamentoComissaoRepository_model->updateTotalComissao($fechamentoID);
            $this->CommissionsRepository_model->updateStatusItemComissao($itemComissaoID, FechamentoComissaoService_model::STATUS_PENDENTE);

            //log da comissao removida
            $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($itemComissaoID);
            $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $itemComissaoID, $note, CommissionEventService_model::COMISSAO_REMOVIDA_DO_FECHAMENTO, $this->session->userdata('user_id'));

            return true;
        }

        return false;
    }

    private function jaExisteComissaoNoFechamento($commission_item_id): bool
    {
        $this->db->where("status in ('Em Revisão','Aprovada','Paga') ");
        $this->db->where('commission_item_id', $commission_item_id);
        $query = $this->db->get('close_commission_items');

        return $query->num_rows() > 0;
    }

    private function naoExisteComissaoNoFechamento($commission_item_id): bool
    {
        return !$this->jaExisteComissaoNoFechamento($commission_item_id);
    }

    public function cancel($fechamentoID): bool
    {
        try {

            $this->__iniciaTransacao();

            $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamentoID);

            if ($fechamento->status == 'Confirmada' && $fechamento->total_commission > 0) {
                throw new Exception('Fechamento já confirmado!');
            }

            $comissoes = $this->FechamentoComissaoRepository_model->getAllItensFechamento($fechamentoID);

            foreach ($comissoes as $comissao) {

                $this->CommissionsRepository_model->updateStatusItemComissao($comissao->commission_item_id, FechamentoComissaoService_model::STATUS_PENDENTE);

                //log da comissao removida
                $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($comissao->commission_item_id);
                $note = 'Comissão removida do fechamento cancelado ('.$fechamento->reference_no.') '.$fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);
                $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $comissao->commission_item_id, $note, CommissionEventService_model::COMISSAO_REMOVIDA_DO_FECHAMENTO_CANCELADO, $this->session->userdata('user_id'));
            }

            $this->FechamentoComissaoRepository_model->updateStatusComissaoFechamento($fechamento->id, FechamentoComissaoService_model::STATUS_CANCELADA);
            $this->FechamentoComissaoRepository_model->updateStatusItemByFechamentoID($fechamento->id, FechamentoComissaoService_model::STATUS_CANCELADA);

            $this->__confirmaTransacao();

            return true;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function fechar($fechamentoDTO, $fechamentoID)
    {
        $this->load->model('financeiro_model');
        $this->load->model('model/ContaPagar_model', 'ContaPagar_model');

        try {

            $this->__iniciaTransacao();
            $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamentoID);

            if ($fechamento->total_commission == 0) {
                throw new Exception('Fechamento sem comissões!');
            }

            if ($fechamento->status == 'Confirmada') {
                throw new Exception('Fechamento já confirmado!');
            }

            $comissoesFechamentoItem = $this->FechamentoComissaoRepository_model->getAllItensFechamento($fechamentoID);

            foreach ($comissoesFechamentoItem as $comissaoFechamentoItem) {

                //adicionar conta pagar comissao
                $this->adicionarDespesa($fechamentoDTO, $comissaoFechamentoItem, $fechamento->id);

                //comission
                $this->CommissionsRepository_model->updateStatusItemComissao($comissaoFechamentoItem->commission_item_id, FechamentoComissaoService_model::STATUS_APROVADA);

                //log da comissao aprovada
                $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($comissaoFechamentoItem->commission_item_id);
                $note = 'Comissão aprovada no fechamento ('.$fechamento->reference_no.') '.
                    $fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);

                $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $comissaoFechamentoItem->commission_item_id,
                    $note, CommissionEventService_model::COMISSAO_APROVADA, $this->session->userdata('user_id'));

            }

            $this->FechamentoComissaoRepository_model->updateStatusItemByFechamentoID($fechamento->id, FechamentoComissaoService_model::STATUS_APROVADA);

            $this->FechamentoComissaoRepository_model->updateStatusComissaoFechamento($fechamento->id, FechamentoComissaoService_model::STATUS_CONFIRMADA);

            $this->__confirmaTransacao();

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function adicionarDespesa($fechamentoDTO, $comissaoFechamentoItem, $fechamentoID): void
    {
        $reference          = $this->site->getReference('ex');

        $totalCobrancas     = count($fechamentoDTO->tiposCobranca);

        $data = array(
            'status'                    => ContaPagar_model::STATUS_ABARTA,
            'reference'                 => $reference,
            'fechamento_comissao_id'    => $fechamentoID,

            'fechamento_item_id'        => $comissaoFechamentoItem->id,
            'pessoa'                    => $comissaoFechamentoItem->biller_id,
            'sale'                      => $comissaoFechamentoItem->sale_id,
            'programacaoId'             => $comissaoFechamentoItem->programacao_id,
            'valor'                     => $comissaoFechamentoItem->commission / $totalCobrancas,

            'tipocobranca'              => $fechamentoDTO->tipo_cobranca_id,
            'condicaopagamento'         => $fechamentoDTO->condicao_pagamento_id,
            'dtvencimento'              => $fechamentoDTO->dtVencimento,
            'dtcompetencia'             => $fechamentoDTO->dtVencimento,
            'note'                      => $fechamentoDTO->note,
            'warehouse'                 => $this->Settings->default_warehouse,
            'despesa'                   => $this->Settings->despesa_fechamento_id,

            'palavra_chave'             => '',
            'numero_documento'          => '',
            'created_by'                => $this->session->userdata('user_id'),
        );

        $contaPagar = ContaPagar_model::create($data, $fechamentoDTO, $comissaoFechamentoItem->commission);

        $this->financeiro_model->adicionarDespesa($contaPagar);
    }

}