var ControllerSales = function () {

    this.onLoad  = onLoad;

    this.isPreencheuFilial = isPreencheuFilial;
    this.eventosCadastrarNovoCliente = eventosCadastrarNovoCliente;
    this.adicionarCamposObrigatorioAoFormulario = adicionarCamposObrigatorioAoFormulario;
    this.getParcelamento = getParcelamento;

    function onLoad() {
        onClick();
        onChange();
        onBlur();
    }

    function onBlur() {

        $('#valor').blur(function (event) {
            getParcelamento();
        });

        $('#previsao_pagamento').change(function (event) {
            getParcelamento();
        });

        $('#tipoCobrancaId').blur(function (event) {
            getParcelamento();
        });

        $("#tipoCobrancaId").select2().on("change", function(e) {
            getParcelamento();
        });

        $("#condicaopagamentoId").select2().on("change", function(e) {
            getParcelamento();
        });
    }

    function onClick() {

        $('#btn-exibir-hospedagem').click(function (event) {
             adicionarNovaHospedagem();
        });

        $('#btn-exibir-passeio').click(function(event) {
            adicionarNovoPasseio();
        });

        $('#adicionar-hospedagem').click(function (event) {
            adicionarNovoItemHospedagem();
        });

        $('#enviar_faturar').click(function (event){
            salvarEhFaturarVenda();
        });

        $('#enviar_orcamento').click(function (event){
           salvarOrcamento();
        });

        $('#enviar_lista_espera').click(function (event){
            salvarListaDeEspera();
        });
    }

    function onChange() {

        $('#slpacote-proprio').select2().on("change", function(e) {
            $('#add_item').val($("#slpacote-proprio option:selected").html());
        });

        $('#tipo_hospedagem').change(function (event) {
            calcularValorProduto();
            $('#pdiscount').keyup();
        });
    }

    function replaceLineStringNewContato(tag) {
        $('#meu-pacote-new-contato').html(tag.val());
    }

    function adicionarNovoPasseio() {
        $('#load-new-passeio').load(site.base_url+"sales/adicionarPasseio", function() {

            window.location.hash ? $('#myTab a[href="' + window.location.hash + '"]').tab('show') : $("#myTabMeusPacotesParametros a:first, #myTabMeusPacotes a").tab("show");

            $('select').select2({minimumResultsForSearch: 7});

            $("#myTabMeusPacotes a, #myTabMeusPacotesParametros a").click(function(t) {
                t.preventDefault();
                $(this).tab("show");
            });

            $('#search-customer').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url+"customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('textarea').not('.skip').redactor({
                buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
                formattingTags: ['p', 'pre', 'h3', 'h4'],
                minHeight: 100,
                changeCallback: function(e) {
                    var editor = this.$editor.next('textarea');
                    if($(editor).attr('required')){
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
                    }
                }
            });

            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            $(function(){
                $('.mask_money').bind('keypress',mask.money);
                $('.mask_money').click(function(){$(this).select();});
            });

            $(function(){
                $('.mask_integer').bind('keypress',mask_integer.money);
                $('.mask_integer').click(function(){$(this).select();});
            });

            eventosCadastrarNovoCliente();
        });
    }

    function adicionarNovaHospedagem() {

        $('#load-new-hospedagem').load(site.base_url+"sales/adicionarHospedagem", function() {

            window.location.hash ? $('#myTab a[href="' + window.location.hash + '"]').tab('show') : $("#myTabMeusPacotesParametros a:first, #myTabMeusPacotes a").tab("show");

            $('select').select2({minimumResultsForSearch: 7});

            $("#myTabMeusPacotes a, #myTabMeusPacotesParametros a").click(function(t) {
                t.preventDefault();
                $(this).tab("show");
            });

            $('#search-customer').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url+"customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('textarea').not('.skip').redactor({
                buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
                formattingTags: ['p', 'pre', 'h3', 'h4'],
                minHeight: 100,
                changeCallback: function(e) {
                    var editor = this.$editor.next('textarea');
                    if($(editor).attr('required')){
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
                    }
                }
            });

            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            $(function(){
                $('.mask_money').bind('keypress',mask.money);
                $('.mask_money').click(function(){$(this).select();});
            });

            $(function(){
                $('.mask_integer').bind('keypress',mask_integer.money);
                $('.mask_integer').click(function(){$(this).select();});
            });

            eventosCadastrarNovoCliente();
        });
    }

    function eventosCadastrarNovoCliente() {

        $('.cpf').mask('999.999.999-99');

        $('#tipo_documento').change(function (e) {
            if ($(this).val() === 'rg') {
                $('#div_vlPassaporteRG').html('<b>Emissão RG</b>');
                $('#div_rgPassaporte').html('<b>R.G</b>');
            } else {
                $('#div_vlPassaporteRG').html('<b>Validade Passaporte</b>');
                $('#div_rgPassaporte').html('<b>Passaporte</b>');
            }
        });

        $('#cf1').change(function (e) {
            $.ajax({
                type: "get",
                url: site.base_url+'customers/validarRG/'+$(this).val(),
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        alert("O R.G. já esta sendo utilizado pelo passageiro "+data[0].name);
                        $('#cf1').val('');
                    }
                }
            });
        });

        $('#vat_no').change(function (e) {
            $.ajax({
                type: "get",
                url: site.base_url+'customers/validarCPF/'+$(this).val(),
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        alert("O CPF/CNPJ já esta sendo utilizado pelo passageiro "+data[0].name);
                        $('#vat_no').val('');
                    }
                }
            });
        });

        $( "#postal_code" ).blur(function() {
            if($.trim($("#postal_code").val()) !== ""){

                var cep = $.trim($("#postal_code").val());
                cep = cep.replace('-','');
                cep = cep.replace('.','');
                cep = cep.replace(' ','');

                var url = 'https://viacep.com.br/ws/' + cep + '/json/';

                $.get(url,
                    function (data) {
                        if(data !== null){

                            var endereco = data.logradouro+" - "+data.bairro;

                            $("#address").val(endereco);
                            $("#city").val(data.localidade);
                            $("#state").val(data.uf);
                        }
                    });
            }
        });

        $('#sName').keyup(function(event){
            replaceLineStringNewContato($(this));
        });

        $('#field-view-endereco-new-contact').click(function(event){
            exibirEndereco();
        });
    }

    function isPreencheuFilial() {
        if (count === 1) {

            slitems = {};
            slfaturas = {};

            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                $('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        return true;
    }

    function adicionarNovoItemHospedagem() {

        if (!isPreencheuFilial() ) return;

        var mid = (new Date).getTime(),
            mcode = '0001',
            mname = $('#slName').val()+' Diária de hospedagem',
            mtax = parseInt($('#mtax').val()),
            mqty = 1,
            mdiscount = 0,
            unit_price = parseFloat('1.00'),
            mtax_rate = {};

        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id === mtax) {
                    mtax_rate = this;
                }
            });

            slitems[mid] = {
                "id": mid,
                "item_id": mid,
                "label": mname,
                "row": {
                    "id": mid,
                    "code": mcode,
                    "name": mname,
                    "quantity": mqty,
                    "price": unit_price,
                    "unit_price": unit_price,
                    "real_unit_price": unit_price,
                    "tax_rate": mtax,
                    "tax_method": 0,
                    "qty": mqty,
                    "type": "manual",
                    "discount": mdiscount,
                    "serial": "",
                    "option": "",
                    "customerClient" :"",
                    "customerClientName" : mname
                },
                "tax_rate": mtax_rate,
                "options": false
            };
            localStorage.setItem('slitems', JSON.stringify(slitems));
            loadItems();

            audio_success.play();
        }
    }

    function exibirEndereco() {
        if ($('#div_endereco').is(':visible')) $('#div_endereco').hide(600);
        else $('#div_endereco').show(600);
    }

    function getParcelamento() {

        var condicaoPagamento = $('#condicaopagamentoId').val();
        var tipoCobranca = $("#tipoCobrancaId  option:selected").val();
        var dtPrimeiroVencimento = $('#previsao_pagamento').val();
        var valorVencimento =  $('#valor').val();

        $('#slTableParcelas tbody').html('');

        if (condicaoPagamento === '' && tipoCobranca === '') return;

        $.ajax({
            type: "GET",
            url: site.base_url + "saleitem/getParcelas",
            //async: true,
            data: {
                condicaoPagamentoId: condicaoPagamento,
                tipoCobrancaId: tipoCobranca,
                dtvencimento:  dtPrimeiroVencimento.split('/').reverse().join('-'),
                valorVencimento: valorVencimento,
            },
            dataType: 'html',
            success: function (html) {
                $('#slTableParcelas tbody').html(html);

                $(function(){
                    $('.mask_money').bind('keypress',mask.money);
                    $('.mask_money').click(function(){$(this).select();});
                });
            }
        });
    }

    function adicionarCamposObrigatorioAoFormulario() {

        $('form[data-toggle="validator"]').data('bootstrapValidator', null);
        $('form[data-toggle="validator"]').bootstrapValidator();

        $('form[data-toggle="validator"]').data('bootstrapValidator').destroy();

        $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

        var fields = $('.form-control');
        $.each(fields, function() {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#'+id;

            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                if ($("label[for='" + id + "']").html() !== undefined) {
                    var label =  $("label[for='" + id + "']").html().replace('*', '');
                    $("label[for='" + id + "']").html(label + ' *');
                    $(document).on('change', iid, function () {
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                    });
                }
            }
        });
    }

    function salvarEhFaturarVenda() {
        bootbox.confirm('Deseja realmente Salvar e Faturar a Venda?<br/><small>Esta operação irá gerar o financeiro automaticamente.</small>', function (result) {
            if (result)  {
                $('#slsale_status').val('faturada').change();
                $('#add_sale').click();
            }
        });
    }

    function salvarOrcamento() {
        $('#slsale_status').val('orcamento').change();
        $('#add_sale').click();
    }

    function salvarListaDeEspera() {
        $('#slsale_status').val('lista_espera').change();
        $('#add_sale').click();
    }

}