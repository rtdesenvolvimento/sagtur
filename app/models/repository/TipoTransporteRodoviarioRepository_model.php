<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TipoTransporteRodoviarioRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getTiposTransporteRodoviario() {
        $q = $this->db->get('tipo_transporte_rodoviario');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getTipoTransporte($id) {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('tipo_transporte_rodoviario');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getTiposTransporteRodoviarioComMapaBase() {

        $this->db->where("automovel_id is not null");
        $this->db->order_by('name');

        $q = $this->db->get('tipo_transporte_rodoviario');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransportesRodoviarioProduto($productID, $tipoTransporte) {

        $this->db->select('tipo_transporte_rodoviario.id, 
            tipo_transporte_rodoviario.automovel_id as automovel_id, 
            tipo_transporte_rodoviario.name as text, tipo_transporte_rodoviario_viagem.status,
            tipo_transporte_rodoviario_viagem.habilitar_selecao_link, 
            tipo_transporte_rodoviario_viagem.configuracao_assento_extra_id');

        $this->db->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=tipo_transporte_rodoviario_viagem.tipoTransporte', 'left');

        $q = $this->db->get_where('tipo_transporte_rodoviario_viagem', array('produto' => $productID, 'tipoTransporte' => $tipoTransporte));

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}