<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'errors/error_404';
$route['translate_uri_dashes'] = FALSE;

$route['users'] = 'auth/users';
$route['users/create_user'] = 'auth/create_user';
$route['users/profile/(:num)'] = 'auth/profile/$1';
$route['login'] = 'auth/login';
$route['login/(:any)'] = 'auth/login/$1';
$route['logout'] = 'auth/logout';
$route['logout/(:any)'] = 'auth/logout/$1';
$route['register'] = 'auth/register';
$route['forgot_password'] = 'auth/forgot_password';
$route['sales/(:num)'] = 'sales/index/$1';
$route['products/(:num)'] = 'products/index/$1';
$route['purchases/(:num)'] = 'purchases/index/$1';
$route['quotes/(:num)'] = 'quotes/index/$1';

//bots
//$route['robots\.txt']           = 'FileRobotsController/getRobots';
//$route['sitemap_(:any).xml']    = 'FileSitemapsController/index/$1';

$empresa    = explode('/', $_SERVER['REQUEST_URI'])[1];
$host       = str_replace('www.', '', $_SERVER['HTTP_HOST']);

switch ($host) {
    case 'sagtur':                  $route = local_host($route); break;
    case 'suareservaonline.com.br': $route = sua_reserva($route, $empresa); break;
    case 'sistema.katatur.com.br':  $route = katatur_exclusive($route); break;

    case 'csviagens.suareservaonline.com.br':
    case 'cincoamigostour.suareservaonline.com.br':
    case 'afaiaviagens.suareservaonline.com.br':
    case 'justgotrips.suareservaonline.com.br':
    case 'alcturismo.suareservaonline.com.br':
    case 'alchaarparticipacoes.suareservaonline.com.br':
    case 'amazonnegociostur.suareservaonline.com.br':
    case 'amigostur.suareservaonline.com.br':
    case 'andersonexcursoes.suareservaonline.com.br':
    case 'angelacardosoviagens.suareservaonline.com.br':
    case 'angelicaexcursoes.suareservaonline.com.br':
    case 'consutoriaeassessoriaviagens.suareservaonline.com.br':
    case 'augustusturismo.suareservaonline.com.br':
    case 'aventuresejalapao.suareservaonline.com.br':
    case 'avesvoandoalto.suareservaonline.com.br':
    case 'avohaiecoturismo.suareservaonline.com.br':
    case 'azenhaturismo.suareservaonline.com.br':
    case 'bragaturviagens.suareservaonline.com.br':
    case 'brasilviagens.suareservaonline.com.br':
    case 'carolviagenseturismo.suareservaonline.com.br':
    case 'charmeturismo.suareservaonline.com.br':
    case 'classeviagemeturismo.suareservaonline.com.br':
    case 'dhagesturismo.suareservaonline.com.br':
    case 'dannytur.suareservaonline.com.br':
    case 'daviturismo.suareservaonline.com.br':
    case 'depalmasturismo.suareservaonline.com.br':
    case 'descubrafloripa.suareservaonline.com.br':
    case 'dgaviagens.suareservaonline.com.br':
    case 'diversaotour.suareservaonline.com.br':
    case 'dnexcursoes.suareservaonline.com.br':
    case 'donatoturismo.suareservaonline.com.br':
    case 'diadetur.suareservaonline.com.br':
    case 'anayaviagens.suareservaonline.com.br':
    case 'aslexpresso.suareservaonline.com.br':
    case 'edivanturismo.suareservaonline.com.br':
    case 'eleveviagens.suareservaonline.com.br':
    case 'essenciaturismo.suareservaonline.com.br':
    case 'excomturismo.suareservaonline.com.br':
    case 'excursoesroturismo.suareservaonline.com.br':
    case 'fdtrip.suareservaonline.com.br':
    case 'felipeexcursoes.suareservaonline.com.br':
    case 'flaviustur.suareservaonline.com.br':
    case 'ganeshaadventureviagens.suareservaonline.com.br':
    case 'gbtour.suareservaonline.com.br':
    case 'gersonaguiarturismo.suareservaonline.com.br':
    case 'giexcursoes.suareservaonline.com.br':
    case 'girandoomundotour.suareservaonline.com.br':
    case 'giselleviagensturismo.suareservaonline.com.br':
    case 'glatur.suareservaonline.com.br':
    case 'glautour.suareservaonline.com.br':
    case 'goncalvesturismo.suareservaonline.com.br':
    case 'grazziviagenseturismo.suareservaonline.com.br':
    case 'gustavotour.suareservaonline.com.br':
    case 'inesbitencourtturismo.suareservaonline.com.br':
    case 'ituturismo.suareservaonline.com.br':
    case 'jalapaobrasilto.suareservaonline.com.br':
    case 'jbmexcursoes.suareservaonline.com.br':
    case 'jrtour.suareservaonline.com.br':
    case 'turismojs.suareservaonline.com.br':
    case 'juhtur.suareservaonline.com.br':
    case 'kingtripsturismo.suareservaonline.com.br':
    case 'kltourviagens.suareservaonline.com.br':
    case 'konexaodosambaviagens.suareservaonline.com.br':
    case 'lamarcaviagens.suareservaonline.com.br':
    case 'ldviagens.suareservaonline.com.br':
    case 'leehtourviagens.suareservaonline.com.br':
    case 'leoturviagens.suareservaonline.com.br':
    case 'viajareprecisope.suareservaonline.com.br':
    case 'levetravel.suareservaonline.com.br':
    case 'liberttravel.suareservaonline.com.br':
    case 'lilaotrip.suareservaonline.com.br':
    case 'locomotivanomade.suareservaonline.com.br':
    case 'lpexcursoes.suareservaonline.com.br':
    case 'luturismoviagens.suareservaonline.com.br':
    case 'marcosexcursoeseeventos.suareservaonline.com.br':
    case 'mbviagens.suareservaonline.com.br':
    case 'mendesviagenstur.suareservaonline.com.br':
    case 'mfviagens.suareservaonline.com.br':
    case 'mgbusviagens.suareservaonline.com.br':
    case 'franturviagens.suareservaonline.com.br':
    case 'betripsviagens.suareservaonline.com.br':
    case 'mimatourviagens.suareservaonline.com.br':
    case 'mochilaselfie.suareservaonline.com.br':
    case 'agenciamonteiro.suareservaonline.com.br':
    case 'mpwturismo.suareservaonline.com.br':
    case 'msvviagens.suareservaonline.com.br':
    case 'mtkturismo.suareservaonline.com.br':
    case 'mulheres100fronteiras.suareservaonline.com.br':
    case 'muralhatour.suareservaonline.com.br':
    case 'nanaturviagens.suareservaonline.com.br':
    case 'natsturismo.suareservaonline.com.br':
    case 'navaltur.suareservaonline.com.br':
    case 'nenaviagens.suareservaonline.com.br':
    case 'novotempoturismo.suareservaonline.com.br':
    case 'pajeturexcursoesmaraba.suareservaonline.com.br':
    case 'peacetrips.suareservaonline.com.br':
    case 'pedrocordeiroturismo.suareservaonline.com.br':
    case 'pipaturismo.suareservaonline.com.br':
    case 'plenissimostrips.suareservaonline.com.br':
    case 'partiuexcursoes.suareservaonline.com.br':
    case 'premiumturismmo.suareservaonline.com.br':
    case 'princesaturismo.suareservaonline.com.br':
    case 'professorperegrino.suareservaonline.com.br':
    case 'rpassaingressos.suareservaonline.com.br':
    case 'raphatrip.suareservaonline.com.br':
    case 'rbrturismo.suareservaonline.com.br':
    case 'realitytur.suareservaonline.com.br':
    case 'riscandoomapa.suareservaonline.com.br':
    case 'romeroexcursoes.suareservaonline.com.br':
    case 'sabrinaviagens.suareservaonline.com.br':
    case 'sanmartinviagens.suareservaonline.com.br':
    case 'saochicoturismo.suareservaonline.com.br':
    case 'semprevix.suareservaonline.com.br':
    case 'sntturismo.suareservaonline.com.br':
    case 'soniaturismo.suareservaonline.com.br':
    case 'sophiaturismo.suareservaonline.com.br':
    case 'spvturismo.suareservaonline.com.br':
    case 'styllusturismo.suareservaonline.com.br':
    case 'stylosturismoeviagens.suareservaonline.com.br':
    case 'terradosolecoturismo.suareservaonline.com.br':
    case 'teudestino.suareservaonline.com.br':
    case 'tioniaturismo.suareservaonline.com.br':
    case 'todeboaviagens.suareservaonline.com.br':
    case 'transcapturismo.suareservaonline.com.br':
    case 'trictour.suareservaonline.com.br':
    case 'tripdavania.suareservaonline.com.br':
    case 'tripturviagens.suareservaonline.com.br':
    case 'tripsturismo.suareservaonline.com.br':
    case 'turismoguarulhos.suareservaonline.com.br':
    case 'turmadovandre.suareservaonline.com.br':
    case 'viagensturismosp.suareservaonline.com.br':
    case 'vfbturismo.suareservaonline.com.br':
    case 'viajecomigomm.suareservaonline.com.br':
    case 'viixeturismo.suareservaonline.com.br':
    case 'vivitour.suareservaonline.com.br':
    case 'voandoaltoturismo.suareservaonline.com.br':
    case 'voyageturismo.suareservaonline.com.br':
    case 'trembaoturismo.suareservaonline.com.br':
    case 'demonstracaosagtur.suareservaonline.com.br':
    case 'guiviagenseturismo.suareservaonline.com.br':
    case 'msaturismo.suareservaonline.com.br':
    case 'brasiltripstour.suareservaonline.com.br':
    case 'jalapaoeletrizanteturismo.suareservaonline.com.br':
    case 'oasistur.suareservaonline.com.br':
    case 'maeefilhosturismo.suareservaonline.com.br':
    case 'deboraexcursoes.suareservaonline.com.br':
    case 'desrotinetrips.suareservaonline.com.br':
    case 'tursolidario.suareservaonline.com.br':
    case 'sergiotur.suareservaonline.com.br':
    case 'mrfernandes.suareservaonline.com.br':
    case 'materdeiviagens.suareservaonline.com.br':
    case 'caminhosdaterraturismo.suareservaonline.com.br':
    case 'msbusviagenseturismo.suareservaonline.com.br':
    case 'pigozzoturismo.suareservaonline.com.br':
    case 'jalapoeirosecotour.suareservaonline.com.br':
    case 'mmturismoeviagens.suareservaonline.com.br':
    case 'riccioviagens.suareservaonline.com.br':
    case 'bellitur.suareservaonline.com.br':
    case 'dunamisexpedicoes.suareservaonline.com.br':
    case 'seashoreturismo.suareservaonline.com.br':
    case 'federaltur.suareservaonline.com.br':
    case 'diegotourjalapao.suareservaonline.com.br':
    case 'deiaexcursoeseviagens.suareservaonline.com.br':
    case 'jrturismonovaiguacu.suareservaonline.com.br':
    case 'keviagens.suareservaonline.com.br':
    case 'stonetour.suareservaonline.com.br':
    case 'raphaviagens.suareservaonline.com.br':
    case 'luhexcursoes.suareservaonline.com.br':
    case 'kilazer.suareservaonline.com.br':
    case 'vamoscomigoturismo.suareservaonline.com.br':
    case 'gtviagenseturismo.suareservaonline.com.br':
    case 'vemprocorre.suareservaonline.com.br':
    case 'batsviagens.suareservaonline.com.br':
    case 'playup.suareservaonline.com.br':
    case 'mtviagens.suareservaonline.com.br':
    case 'agenciavicktrips.suareservaonline.com.br':
    case 'invistatour.suareservaonline.com.br':
    case 'dinahturviagens.suareservaonline.com.br':
    case 'lovefortrips.suareservaonline.com.br':
    case 'domturismo.suareservaonline.com.br':
    case 'brtravel.suareservaonline.com.br':
    case 'itaviagens.suareservaonline.com.br':
    case 'girlstrips.suareservaonline.com.br':
    case 'gecioneturismo.suareservaonline.com.br':
    case 'livreacessoturismoeviagens.suareservaonline.com.br':
    case 'danturismo.suareservaonline.com.br':
    case 'rssturismo.suareservaonline.com.br':
    case 'megacorporativa.suareservaonline.com.br':
    case 'fenixturismo.suareservaonline.com.br':
    case 'oliviatour.suareservaonline.com.br':
    case 'traveltur.suareservaonline.com.br':
    case 'plenasviagens.suareservaonline.com.br':
    case 'brexcursoes.suareservaonline.com.br':
    case 'gouveiatourgo.suareservaonline.com.br':
    case 'cpcturismo.suareservaonline.com.br':
    case 'viajandocommaria.suareservaonline.com.br':
    case 'caicaraexpedicoes.suareservaonline.com.br':
    case 'registurismo.suareservaonline.com.br':
    case 'viagensdetrem.suareservaonline.com.br':
    case 'sonicviagens.suareservaonline.com.br':
    case 'minasecoturismo.suareservaonline.com.br':
    case 'rssviagenseturismo.suareservaonline.com.br':
    case 'edianetour.suareservaonline.com.br':
    case 'aglaiaexcursoes.suareservaonline.com.br':
    case 'tripemgalera.suareservaonline.com.br':
    case 'tourmix.suareservaonline.com.br':
    case 'ederturismo.suareservaonline.com.br':
    case 'paraisoviagensturismo.suareservaonline.com.br':
    case 'noninoviagens.suareservaonline.com.br':
    case 'pacotesdetrip.suareservaonline.com.br':
    case 'panatourviagens.suareservaonline.com.br':
    case 'cristianeturismo.suareservaonline.com.br':
    case 'dhiturismo.suareservaonline.com.br':
    case 'cristiannetur.suareservaonline.com.br':
    case 'cativatur.suareservaonline.com.br':
    case 'goodtrip.suareservaonline.com.br':
    case 'theworldturismo.suareservaonline.com.br':
    case 'enoturbrasilia.suareservaonline.com.br':
    case 'viagensnacional.suareservaonline.com.br':
    case 'bernardinitrips.suareservaonline.com.br':
    case 'fariaesoaresturismo.suareservaonline.com.br':
    case 'morangostour.suareservaonline.com.br':
    case 'renatoturismoadventure.suareservaonline.com.br':
    case 'vivitourltda.suareservaonline.com.br':
    case 'televo.suareservaonline.com.br':
    case 'vibless.suareservaonline.com.br':
    case 'ceumarturismo.suareservaonline.com.br':
    case 'biaviagensturismo.suareservaonline.com.br':
    case 'lamsturismo.suareservaonline.com.br':
    case 'patyupexcursao.suareservaonline.com.br':
    case 'abepetur.suareservaonline.com.br':
    case 'karinaviagensturismo.suareservaonline.com.br':
    case 'conexaobrasilperu.suareservaonline.com.br':
    case 'mariliaviagenseturismo.suareservaonline.com.br':
    case 'busmar.suareservaonline.com.br':
    case 'stellatours.suareservaonline.com.br':
    case 'celurturismo.suareservaonline.com.br':
    case 'salvadorviagenseturismo.suareservaonline.com.br':
    case 'zeroonzetour.suareservaonline.com.br':
    case 'atualviagens.suareservaonline.com.br':
    case 'agenciadeviagensaguiar.suareservaonline.com.br':
    case 'flyturviagens.suareservaonline.com.br':
    case 'solariumtur.suareservaonline.com.br':
    case 'ueltrekking.suareservaonline.com.br':
    case 'scturismoeviagens.suareservaonline.com.br':
    case 'travellertur.suareservaonline.com.br':
    case 'cenarionativoexpedicoes.suareservaonline.com.br':
    case 'terraceuviagemeturismo.suareservaonline.com.br':
    case 'ladtur.suareservaonline.com.br':
    case 'qualosegredoaventuras.suareservaonline.com.br':
    case 'hollidayviagensetur.suareservaonline.com.br':
    case 'verdeviagens.suareservaonline.com.br':
    case 'tatytourviagens.suareservaonline.com.br':
    case 'jaturismo.suareservaonline.com.br':
    case 'turismopedregulho.suareservaonline.com.br':
    case 'colinaturismo.suareservaonline.com.br':
    case 'caetanotur.suareservaonline.com.br':
    case 'mianovichiexcursoes.suareservaonline.com.br':
    case 'marciaturismo.suareservaonline.com.br':
    case 'bigbearexcursoes.suareservaonline.com.br':
    case 'rodribus.suareservaonline.com.br':
    case 'uaitour.suareservaonline.com.br':
    case 'seguetur.suareservaonline.com.br':
    case 'wgexcursoes.suareservaonline.com.br':
    case 'rpthurturismo.suareservaonline.com.br':
    case 'maisroteirosviagens.suareservaonline.com.br':
    case 'sunset.suareservaonline.com.br':
    case 'samistur.suareservaonline.com.br':
    case 'conectatour.suareservaonline.com.br':
    case 'dsbtur.suareservaonline.com.br':
    case 'constelacaotour.suareservaonline.com.br':
    case 'solartrips.suareservaonline.com.br':
    case 'lumasterturismo.suareservaonline.com.br':
    case 'vavdavareturismo.suareservaonline.com.br':
    case 'centauroturismo.suareservaonline.com.br':
    case 'estrelaguiaturismo.suareservaonline.com.br':
    case 'luviagenseturismo.suareservaonline.com.br':
    case 'dielyelorenaviagens.suareservaonline.com.br':
    case 'mahila.suareservaonline.com.br':
    case 'stevenlindley.suareservaonline.com.br':
    case 'nanytrips.suareservaonline.com.br':
    case 'sabinoturismo.suareservaonline.com.br':
    case 'alexteleva.suareservaonline.com.br':
    case 'minasgoturismo.suareservaonline.com.br':
    case 'bhorizonteviagenseturismo.suareservaonline.com.br':
    case 'larissaphasseios.suareservaonline.com.br':
    case 'wtorresviagemfamilia.suareservaonline.com.br':
    case 'suetrips.suareservaonline.com.br':
    case 'transgerciturismo.suareservaonline.com.br':
    case 'rctt.suareservaonline.com.br':
    case 'barrilviagens.suareservaonline.com.br':
    case 'deiaexcursoes.suareservaonline.com.br':
    case 'gomestur.suareservaonline.com.br':
    case 'libertyturismo.suareservaonline.com.br':
    case 'kadoshadonaiviagens.suareservaonline.com.br':
    case 'gratitudeviagens.suareservaonline.com.br':
    case 'rcviagemturismo.suareservaonline.com.br':
    case 'penaestradapasseios.suareservaonline.com.br':
    case 'jldestinosturismo.suareservaonline.com.br':
    case 'patitour.suareservaonline.com.br':
    case 'naestradabauru.suareservaonline.com.br':
    case 'diogotour.suareservaonline.com.br':
    case 'receptivoconecttur.suareservaonline.com.br':
    case 'lifestour.suareservaonline.com.br':
    case 'paratytemporadaoficial.suareservaonline.com.br':
    case 'starstrip.suareservaonline.com.br':
    case 'doisirmaosturismo.suareservaonline.com.br':
    case 'girlsgoviagens.suareservaonline.com.br':
    case 'mceturismo.suareservaonline.com.br':
    case 'agendaturismo.suareservaonline.com.br':
    case 'boraviagensturismo.suareservaonline.com.br':
    case 'agvtur.suareservaonline.com.br':
    case 'letsboratur.suareservaonline.com.br':
    case 'familiarexcursoes.suareservaonline.com.br':
    case 'mundreamviagens.suareservaonline.com.br':
    case 'maxtourviagenseturimo.suareservaonline.com.br':
    case 'turismogess.suareservaonline.com.br':
    case 'kakaturismo.suareservaonline.com.br':
    case 'vmturismoviagens.suareservaonline.com.br':
    case 'mulheresviajantesrj.suareservaonline.com.br':
    case 'ketitaturismo.suareservaonline.com.br':
    case 'nevesturexcursoes.suareservaonline.com.br':
    case 'osfarristastur.suareservaonline.com.br':
    case 'tawtour.suareservaonline.com.br'  : $route = subdomains_suareserva_online($route); break;




    case 'eufuiviagens.com.br':
    case 'penaestradaoficial.com':
    case 'agenciapenochaoexcursoes.com.br':
    case 'aramastur.com.br':
    case 'familyturismo.com.br':
    case 'mieventoseturismo.com':
    case 'reservas.sicatur.com.br':
    case 'partiutrips.com.br':
    case 'excursoesbrunamoraes.com.br':
    case 'reservas.riosexcursoes.com':
    case 'excursoestiacleusa.com.br':
    case 'reservas.dottaaventura.com.br':
    case 'gusmaoviagemeturismo.com.br':
    case 'paraviverturismo.com.br':
    case 'cmdestinoseviagens.com.br':
    case 'abrangeturismo.com.br':
    case 'daniviagens.tur.br':
    case 'agencianovoshorizonte.com.br':
    case 'reservas.thaistour.com.br':
    case 'newdreamstur.com.br':
    case 'curtaviagens.tur.br':
    case 'reservas.turdans.com.br':
    case 'italiaturismo.com.br':
    case 'tripforever.com.br':
    case 'divinopolisecoturismo.com.br':
    case 'agenciaplanettrip.com.br':
    case 'mroyalviagens.com.br':
    case 'ctljexecutiva.com.br':
    case 'reservas.maxxitours.com':
    case 'girltrips.com.br':
    case 'jkturismobc.com.br':
    case 'dclasseturismo.com.br':
    case 'agencianovosdestinos.com.br':
    case 'reservas.agencianovosdestinos.com.br':
    case 'stilostur.com.br':
    case 'tremdaalegriaviagens.com.br':
    case 'f5tour.com.br':
    case 'mmturismoeviagens.com.br':
    case 'reservasangulotravel.com.br':
    case 'excursoespontonell.com.br':
    case 'excursoesdabecky.com.br':
    case 'perollaricoviagens.com.br':
    case 'itaviagens.com.br':
    case 'vounaviagem.com.br':
    case 'reservas.alvanevasconcelosturismo.com':
    case 'penochaoviagenseturismo.com.br':
    case 'emersonfelipeexcursoes.com.br':
    case 'smcturismo.com.br':
    case 'reservas.smcturismo.com.br':
    case 'carolviagens.net':
    case 'gmtrip.com.br':
    case 'allinturismo.com.br':
    case 'estanciatourviagens.com.br':
    case 'azevedotour.com.br':
    case 'lojaviajolandia.com.br':
    case 'viagensorion.com':
    case 'viagensorion.com.br':
    case 'easytransfer.tur.br':
    case 'angulotravel.com.br':
    case 'mdv.tur.br':
    case 'refviagenseturismo.com.br':
    case 'alvarengaexcursoes.com.br':
    case 'euprefiroviajar.com.br':
    case 'reservas.evoluirviagens.com.br':
    case 'marcinhotur.com.br':
    case 'pdviagens.com.br':
    case 'boralaviagens.com.br':
    case 'expressorodriguez.com.br':
    case 'maistravelviagens.com.br':
    case 'acosttaviagens.com.br':
    case 'viagensconnect.com.br':
    case 'bmsturismo.com.br':
    case 'upofiicial.com.br':
    case 'parceirustur.com.br':
    case 'roselyviagens.com.br':
    case 'lucianotourviagens.com.br':
    case 'patotrips.com.br':
    case 'originalestradeiros.com':
    case 'rumocertoturismoperuibe.com.br':
    case 'pensetur.com.br':
    case 'tourgouvea.com.br':
    case 'trip4funn.com.br':
    case 'connecttrips.com.br':
    case 'encantoturismo.tur.br':
    case 'agtturismo.com.br':
    case 'javoltour.com.br':
    case 'tatatur.com.br':
    case 'primeturismobaixada.com.br':
    case 'vemcomagenteviagens.com.br':
    case 'boraviajeiros.com.br':
    case 'nessatur.com.br':
    case 'memexcursoes.com.br':
    case 'ytransfers.com.br':
    case 'bmgturismo.com.br':
    case 'angulotravel.com':
    case 'poraidemochilaoficial.com.br':
    case 'safaritur.com.br':
    case 'escarpaspasseiosnauticos.com.br':
    case 'vicioviagem.com':
    case 'trilhandomundo.com':
    case 'reservas.flviagens.com.br':
    case 'conectashows.com.br':
    case 'grupolvturismo.com.br':
    case 'estrelastour.com.br':
    case 'agenciajptourviagens.com.br':
    case 'tripswei.com.br':
    case 'reservas.boraboraviagenseturismo.com.br':
    case 'reservas.viajandobrasilafora.com.br':
    case 'lojawineviagens.com.br':
    case 'pipaturismo.com.br':
    case 'deboraexcursoes.com.br':
    case 'spvturismo.com.br':
    case 'raphaviagens.com.br':
    case 'luhexcursoes.com.br':
    case 'brotherstrips.com.br':
    case 'raphatrip.com.br':
    case 'douratour.com.br':
    case 'reservas.domturismo.tur.br':
    case 'carpediemviagens.com.br':
    case 'danturismo.com.br':
    case 'brexcursoes.com.br':
    case 'plenasviagens.com.br':
    case 'musictravel.com.br':
    case 'minasecoturismo.com.br':
    case 'caicaraexpedicoes.com':
    case 'caicaraexpedicoes.com.br':
    case 'caicaraexpedicoes.tur.br':
    case 'edianetour.com.br':
    case 'registurismo.com.br':
    case 'alkviagens.com.br':
    case 'reservas.msvturismo.com':
    case 'viajandobrasilafora.com.br':
    case 'dhiturismo.com.br':
    case 'morangostour.com.br':
    case 'andersonexcursoes.com.br':
    case 'marialuciaturismo.com.br':
    case 'reservas.marialuciaturismo.com.br':
    case 'conexaobrasilperu.com.br':
    case 'reservas.busmar.tur.br':
    case 'vibless.com.br':
    case 'mogi.domturismo.tur.br':
    case 'crissturismo.com.br':
    case 'zeroonzetour.com.br':
    case 'andreytur.com.br':
    case 'flyturturismo.com.br':
    case 'ueltrekking.com.br':
    case 'busaodocabral.com.br':
    case 'dugeviagens.com.br':
    case 'plenitudeexcursoes.com.br':
    case 'andreiaviagenseturismo.com.br':
    case 'alineviagens.com.br':
    case 'viajemaisporai.com.br':
    case 'gabrielviagens.com.br':
    case 'dedessatour.com.br':
    case 'entreamigosexcursoes.com.br':
    case 'rodzexcursoes.com.br':
    case 'moserturismo.com.br':
    case 'primeturexcursoes.com.br':
    case 'lucianotouroperadora.com.br':
    case 'lionstrance.com.br':
    case 'tripdajana.com.br':
    case 'edinhotur.com.br':
    case 'rotinatrip.com':
    case 'oliveirastrips.com.br':
    case 'bggturismo.com.br':
    case '2forasteiros.com.br':
    case 'anjosturismorp.com.br':
    case 'agenciaalohaturismo.com.br':
    case 'bellalunatour.com':
    case 'alveselimatur.com.br':
    case 'alternativeviagens.com.br':
    case 'tripmaia.com.br':
    case 'ahmturismo.com.br':
    case 'magiatrip.com.br':
    case 'helloutrip.com.br':
    case 'luluturismo.com.br':
    case 'wineviagens.com.br':
    case 'richardturismo.com.br':
    case 'planejaviagensturismo.com.br':
    case 'fofaoexcursoes.com.br':
    case 'phoenixturismo.com.br':
    case 'kleexcursoes.com.br':
    case 'pensetripsviagens.com.br':
    case 'ericlesturismo.com.br':
    case 'lptur.com':
    case 'camargosturismo.com.br':
    case 'viajarecorrer.com.br':
    case 'gbturismo.com':
    case 'gmturismorealizasonhos.com.br':
    case 'tripsdareh.com.br':
    case 'camaleaoviagens.com.br':
    case 'crturismo.com.br':
    case 'edivanturismo.com.br':
    case 'realizeviagens-es.com.br':
    case 'idealoperadora.com.br':
    case 'goldtourtrip.com.br':
    case 'leoturismobh.com.br':
    case 'sistemabrinquedoteca.com.br': $route = own_domain($route); break;

    default:
        $route[$empresa.'/(:num)']                          = "errors/error_new_page_cart";
        $route[$empresa.'/loja/(:num)']                     = "errors/error_new_page_cart";
        $route[$empresa.'/estoque/(:num)']                  = "errors/error_new_page_cart";
        $route[$empresa.'/carrinho/(:num)/(:num)']          = "errors/error_new_page_cart";
        $route[$empresa.'/carrinho/(:any)/(:num)']          = "errors/error_new_page_cart";
        $route[$empresa.'/pacote/(:any)/(:num)']            = "errors/error_new_page_cart";
        $route[$empresa.'/page/(:any)/(:num)']              = "errors/error_new_page_cart";

        $route['appcompra/(:any)/(:num)/(:num)']            = "errors/error_new_page_cart";
        $route['loja/(:any)/(:num)']                        = "errors/error_new_page_cart";

        $route['default_controller']                        = 'welcome';
        break;
}

function own_domain($route) {

    $route['(:num)']                        = "loja/open/$1";
    $route['loja/(:num)']                   = "loja/open/$1";

    $route['sobre/(:num)']                  = "about/sobre/$1";
    $route['sobre']                         = "about/index";

    $route['contato/(:num)']                = "contact/contato/$1";
    $route['contato']                       = "contact/index";

    $route['promocao/(:num)']               = "promotion/promocao/$1";
    $route['promocao']                      = "promotion/index";

    $route['galeria/(:num)']                = "gallery/galeria/$1";
    $route['galeria']                       = "gallery/index";

    $route['estoque/(:num)']                = "estoque/open/$1";
    $route['estoque/']                      = "estoque/index";

    $route['carrinho/(:any)/(:num)']        = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']          = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']        = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)'] = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)'] = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']            = "page/page/$1/$2";
    $route['page/(:any)']                   = "page/page/$1";

    $route['appcompra/(:num)/(:num)']       = "appview/open/$1/$2";
    $route['admin']                         = 'auth/login';

    $route['rating/(:any)']                 = "ratings/rating/$1";
    $route['rate/(:num)']                   = "ratings/rate/$1";

    $route['simulation']                    = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']        = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']          = "simulationcreditcard/pagseguro";

    $route['myapp']                         = "myapp/index";

    $route['default_controller'] 		    = 'loja/index';

    $route['login']                         =  'errors/error_404';

    return $route;
}

function subdomains_suareserva_online($route) {

    $route['(:num)']                        = "loja/open/$1";
    $route['loja/(:num)']                   = "loja/open/$1";

    $route['sobre/(:num)']                  = "about/sobre/$1";
    $route['sobre']                         = "about/index";

    $route['contato/(:num)']                = "contact/contato/$1";
    $route['contato']                       = "contact/index";

    $route['promocao/(:num)']               = "promotion/promocao/$1";
    $route['promocao']                      = "promotion/index";

    $route['galeria/(:num)']                = "gallery/galeria/$1";
    $route['galeria']                       = "gallery/index";

    $route['estoque/(:num)']                = "estoque/open/$1";
    $route['estoque/']                      = "estoque/index";

    $route['carrinho/(:any)/(:num)']        = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']          = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']        = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)'] = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)'] = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']            = "page/page/$1/$2";
    $route['page/(:any)']                   = "page/page/$1";

    $route['appcompra/(:num)/(:num)']       = "appview/open/$1/$2";
    $route['admin']                         = 'auth/login';

    $route['rating/(:any)']                 = "ratings/rating/$1";
    $route['rate/(:num)']                   = "ratings/rate/$1";

    $route['simulation']                    = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']        = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']          = "simulationcreditcard/pagseguro";

    $route['myapp']                         = "myapp/index";

    $route['default_controller'] 		    = 'loja/index';

    $route['login']                         =  'errors/error_404';

    return $route;
}


function katatur_exclusive($route) {

    $route['(:num)']                        = "loja/open/$1";
    $route['loja/(:num)']                   = "loja/open/$1";

    $route['sobre/(:num)']                  = "about/sobre/$1";
    $route['sobre']                         = "about/index";

    $route['contato/(:num)']                = "contact/contato/$1";
    $route['contato']                       = "contact/index";

    $route['promocao/(:num)']               = "promotion/promocao/$1";
    $route['promocao']                      = "promotion/index";

    $route['galeria/(:num)']                = "gallery/galeria/$1";
    $route['galeria']                       = "gallery/index";

    $route['estoque/(:num)']                = "estoque/open/$1";
    $route['estoque/']                      = "estoque/index";

    $route['carrinho/(:any)/(:num)']        = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']          = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']        = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)'] = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)'] = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']            = "page/page/$1/$2";
    $route['page/(:any)']                   = "page/page/$1";

    $route['appcompra/(:num)/(:num)']       = "appview/open/$1/$2";
    $route['admin']                         = 'auth/login';

    $route['rating/(:any)']                 = "ratings/rating/$1";
    $route['rate/(:num)']                   = "ratings/rate/$1";

    $route['simulation']                    = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']        = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']          = "simulationcreditcard/pagseguro";

    $route['myapp']                         = "myapp/index";

    $route['default_controller']            = 'welcome';

    return $route;
}

function sua_reserva($route, $empresa) {
    $route[$empresa]                                    = "loja/index";
    $route[$empresa.'/(:num)']                          = "loja/open/$1";
    $route[$empresa.'/loja/(:num)']                     = "loja/open/$1";

    $route[$empresa.'/sobre/(:num)']                    = "about/sobre/$1";
    $route[$empresa.'/sobre']                           = "about/index";

    $route[$empresa.'/contato/(:num)']                  = "contact/contato/$1";
    $route[$empresa.'/contato']                         = "contact/index";

    $route[$empresa.'/promocao/(:num)']                 = "promotion/promocao/$1";
    $route[$empresa.'/promocao']                        = "promotion/index";

    $route[$empresa.'/galeria/(:num)']                  = "gallery/galeria/$1";
    $route[$empresa.'/galeria']                         = "gallery/index";

    $route[$empresa.'/estoque/(:num)']                  = "estoque/open/$1";
    $route[$empresa.'/estoque']                         = "estoque/index";

    $route[$empresa.'/carrinho/(:any)/(:num)']          = "appview/product_lookup/$1/$2";
    $route[$empresa.'/pacote/(:any)/(:num)']            = "appview/product_lookup/$1/$2";
    $route[$empresa.'/carrinho/(:num)/(:num)']          = "appview/open/$1/$2";
    $route[$empresa.'/carrinho_compra/(:num)/(:num)']   = "appview/carrinho_compra/$1/$2";
    $route[$empresa.'/carrinho_compra/(:any)/(:num)']   = "appview/shopping_cart/$1/$2";
    $route[$empresa.'/page/(:any)/(:num)']              = "page/page/$1/$2";
    $route[$empresa.'/page/(:any)']                     = "page/page/$1";

    $route[$empresa.'/appcompra/(:num)/(:num)']         = "appview/open/$1/$2";

    $route[$empresa.'/rating/(:any)']                   = "ratings/rating/$1";
    $route[$empresa.'/rate/(:num)']                     = "ratings/rate/$1";

    $route[$empresa.'/simulation']                      = "simulationcreditcard/simulation";
    $route[$empresa.'/simulation/mercadopago']          = "simulationcreditcard/mercadopago";
    $route[$empresa.'/simulation/pagseguro']            = "simulationcreditcard/pagseguro";


    $route[$empresa.'/myapp']                           = "myapp/index";

    $route['login']                                     =  'errors/error_404';

    return $route;
}

function local_host($route) {

    $route['(:num)']                            = "loja/open/$1";
    $route['loja/(:num)']                       = "loja/open/$1";

    $route['sobre/(:num)']                      = "about/sobre/$1";
    $route['sobre']                             = "about/index";

    $route['contato/(:num)']                    = "contact/contato/$1";
    $route['contato']                           = "contact/index";

    $route['promocao/(:num)']                   = "promotion/promocao/$1";
    $route['promocao']                          = "promotion/index";

    $route['galeria/(:num)']                    = "gallery/galeria/$1";
    $route['galeria']                           = "gallery/index";

    $route['estoque/(:num)']                    = "estoque/open/$1";
    $route['estoque/']                          = "estoque/index";

    $route['carrinho/(:any)/(:num)']            = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']              = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']            = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)']     = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)']     = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']                = "page/page/$1/$2";
    $route['page/(:any)']                       = "page/page/$1";

    $route['rating/(:any)']                     = "ratings/rating/$1";
    $route['rate/(:num)']                       = "ratings/rate/$1";

    $route['simulation']                        = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']            = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']              = "simulationcreditcard/pagseguro";

    $route['myapp']                             = "myapp/index";

    $route['admin']                             = 'auth/login';

    return $route;
}