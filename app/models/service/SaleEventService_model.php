<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SaleEventService_model extends CI_Model
{
    const EMAIL_ENVIADO = 'E-MAIL ENVIADO';
    const ACEITE_COMPRA = 'Aceite de Compra';

    public function __construct() {
        parent::__construct();

        $this->load->model('repository/EventSaleRepository_model', 'EventSaleRepository_model');

        $this->load->model('model/EventSale_model', 'EventSale_model');
    }

    public  function cancel_integration($sale_id, $log) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Cancelada");
            $event->setEvent("Venda Cancelada Integração");
            $event->setLog($log);
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function send_mail($sale_id) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus(SaleEventService_model::EMAIL_ENVIADO);
            $event->setEvent('E-mail do voucher foi enviado para o passageiro responsável pela compra.');
            $event->setLog('');
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function cancel($sale_id, $note = NULL) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Cancelada");
            $event->setEvent("Venda Cancelada<br/>".$note);
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function gerar_credito($sale_id, $note = NULL) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Crédito Gerado");
            $event->setEvent("Crédito de Cliente Gerado".$note);
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function gerar_reembolso($sale_id, $note = NULL) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Reembolso Gerado");
            $event->setEvent("Reembolso de Cliente Gerado".$note);
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function archived($sale_id) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Arquivada");
            $event->setEvent("Venda Arquivada");
            $event->setLog('');
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public  function unarchive($sale_id) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $nome_completo = $this->session->userdata('nome_completo');

            if (!$nome_completo) {
                $nome_completo = 'Sistema';
            }

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($this->session->userdata('user_id'));
            $event->setUser($nome_completo);
            $event->setStatus("Desarquivada");
            $event->setEvent("Venda Desarquivada");
            $event->setLog('');
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function addEvent($sale_id, $event_text, $user) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($user->id);
            $event->setUser($user->first_name.' '.$user->last_name);
            $event->setEvent($event_text);
            $event->setStatus("Venda Adicionada");
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function editEvent($sale_id, $event_text, $user) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($user->id);
            $event->setUser($user->first_name.' '.$user->last_name);
            $event->setEvent($event_text);
            $event->setStatus("Venda Editada");
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function pagamento($sale_id, $event_text, $user) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($user->id);

            if ($user) {
                $event->setUser($user->first_name.' '.$user->last_name);
            } else {
                $event->setUser('Sistema');
            }

            $event->setEvent($event_text);
            $event->setStatus("Pagamento Adicionado");
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function transacao_pagamento($sale_id, $event_text, $status) {
        try {

            if (!$status) $status = 'Created';

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId(null);
            $event->setUser('Sistema');
            $event->setEvent($event_text);
            $event->setStatus($status);
            $event->setSaleId($sale_id);
            $event->setLog(print_r($_POST, true));

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function estorno($sale_id, $event_text, $user) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setUserId($user->id);
            $event->setUser($user->first_name.' '.$user->last_name);
            $event->setEvent($event_text);
            $event->setStatus("Estorno");
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function  isEmailEnviado($sale_id) {
        return $this->EventSaleRepository_model->isEmailEnviado($sale_id);
    }

    public function customer_manual_sale_log($sale_id, $event_text, $termos_aceite, $user) {
        try {

            $event = new EventSale_model();

            $this->__iniciaTransacao();

            $event->setUserId($user->id);
            $event->setUser($user->first_name.' '.$user->last_name);
            $event->setEvent($event_text);
            $event->setStatus(SaleEventService_model::ACEITE_COMPRA);
            $event->setLog(print_r($_POST, true));
            $event->setSaleId($sale_id);

            //logs

            // Verificar se o servidor fornece o cabeçalho 'HTTP_X_FORWARDED_FOR'
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            // Verificar se o servidor fornece o cabeçalho 'HTTP_CLIENT_IP'
            elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
            // Caso contrário, obter o endereço IP padrão do servidor
            else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }


            $event->setIpAddress($ip);
            $event->setDate(date('Y-m-d H:i:s'));
            $event->setPageWeb(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
            $event->setHttpMethod($_SERVER['REQUEST_METHOD']);
            $event->setHttpResponseCode($_SERVER['HTTP/1.1']);
            $event->setServerPort($_SERVER['SERVER_PORT']);
            $event->setServerName($_SERVER['SERVER_NAME']);
            $event->setServerSignature($_SERVER['SERVER_SIGNATURE']);
            $event->setServerSoftware($_SERVER['SERVER_SOFTWARE']);
            $event->setHttpReferer($_SERVER['HTTP_REFERER']);
            $event->setRequestTime($_SERVER['REQUEST_TIME']);

            $user_agent = $_SERVER['HTTP_USER_AGENT'];

            $event->setUserAgentInfo($user_agent);

            if (preg_match('/MSIE/i', $user_agent)) {
                $browser = 'Internet Explorer';
                $pattern = '/MSIE (.*?);/';
            } elseif (preg_match('/Firefox/i', $user_agent)) {
                $browser = 'Firefox';
                $pattern = '/Firefox\/(.*?);/';
            } elseif (preg_match('/Chrome/i', $user_agent)) {
                $browser = 'Google Chrome';
                $pattern = '/Chrome\/(.*?);/';
            } elseif (preg_match('/Safari/i', $user_agent)) {
                $browser = 'Safari';
                $pattern = '/Safari\/(.*?);/';
            } elseif (preg_match('/Opera/i', $user_agent)) {
                $browser = 'Opera';
                $pattern = '/Opera\/(.*?);/';
            }

            preg_match($pattern, $user_agent, $matches);
            $version = $matches[1];

            $event->setBrowserInformationName($browser);
            $event->setBrowserInformationVersion($version);

            $browser = get_browser(null, true);
            $os = $browser['platform'];

            $event->setOperatingSystemInformationName($os);
            $event->setInformationCookies(print_r($_COOKIE, true));
            $event->setInformationSession(print_r($_SESSION, true));
            $event->setSessionsId(session_id());
            $event->setTermosAceite($termos_aceite);

            $event_id = $this->__save('sale_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

}