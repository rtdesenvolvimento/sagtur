<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FechamentoComissaoRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($id)
    {
        $q = $this->db->get_where('close_commissions', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItemFechamentoByID($id)
    {
        $q = $this->db->get_where('close_commission_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItemFechamentoByComissaoItemID($comissaoItemID)
    {
        $q = $this->db->get_where('close_commission_items', array('commission_item_id' => $comissaoItemID), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getFechamentoBySaleID($sale_id)
    {
        $this->db->select('close_commissions.*');
        $this->db->join('close_commissions', 'close_commissions.id = close_commission_items.close_commission_id');

        $this->db->where("close_commission_items.status in ('Em Revisão','Aprovada','Parcial','QUITADA') ");

        $q = $this->db->get_where('close_commission_items', array('sale_id' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getFaturaByCommissionItem($itemID)
    {
        $this->db->where('(fatura.status = "QUITADA" OR fatura.status = "ABERTA" OR  fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $q = $this->db->get_where('fatura', array('fechamento_item_id' => $itemID), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllItensFechamento($id, $biller_id = NULL, $sales_id = NULL) {

        $this->db->select('close_commission_items.*, sales.reference_no, sales.date');

        $this->db->join('sales', 'sales.id = close_commission_items.sale_id', 'left');

        if ($biller_id) {
            $this->db->where('close_commission_items.biller_id', $biller_id);
        }

        if ($sales_id) {
            $this->db->where('sales.id', $sales_id);
        }

        $q = $this->db->get_where('close_commission_items', array('close_commission_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateTotalComissao($close_commission_id): void
    {
        $this->db->select_sum('commission');
        $this->db->where('close_commission_id', $close_commission_id);
        $query = $this->db->get('close_commission_items');
        $result = $query->row();
        $total_comissao = 0;

        if ($result->commission) {
            $total_comissao = $result->commission;
        }

        $this->db->set('total_commission', $total_comissao, FALSE);
        $this->db->where('id', $close_commission_id);
        $this->db->update('close_commissions');
    }

    public function updateStatusItemByFechamentoID($fechamentoID, $status)
    {
        $this->db->set('status', $status);
        $this->db->where('close_commission_id', $fechamentoID);
        $this->db->update('close_commission_items');
    }

    public function updateStatusComissaoFechamento($commission_item_id, $status)
    {

        if ($status == 'Confirmada') {
            $this->db->set('dt_fechamento', date('Y-m-d'));
            $this->db->set('updated_by', $this->session->userdata('user_id'));
            $this->db->set('updated_at', date('Y-m-d H:i:s'));
        }

        $this->db->set('status', $status);
        $this->db->where('id', $commission_item_id);
        $this->db->update('close_commissions');
    }

    public function isBillerActive($biller_id, $close_commission_id)
    {
        $q = $this->db->get_where('commission_billers', array('active' => true, 'biller_id' => $biller_id, 'commission_id' => $close_commission_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function adicionar_billers($billers, $fechamento_comissao_id)
    {
        foreach ($billers as $biller) {
            $biller['commission_id'] = $fechamento_comissao_id;
            $this->db->insert('commission_billers', $biller);
        }
    }

    public function atualizar_status_item_comissao_not_billers($fechamentoID)
    {
        $sql = "UPDATE sma_commission_items
                SET status = 'Pendente'
                WHERE id IN
                    (SELECT commission_item_id
                     FROM sma_close_commission_items
                     WHERE sma_close_commission_items.biller_id NOT IN
                         (SELECT cb.biller_id
                          FROM sma_commission_billers cb
                          WHERE cb.commission_id = sma_close_commission_items.close_commission_id)
                       AND sma_close_commission_items.close_commission_id = ". $fechamentoID .")";

        if ($this->db->query($sql)) {
            return $this->delete_item_where_not_billers($fechamentoID);
        } else {
            return false;
        }
    }

    private function delete_item_where_not_billers($fechamentoID): bool
    {

       $sql = "DELETE cci.*
                FROM sma_close_commission_items cci
                LEFT JOIN sma_commission_billers cb ON cb.biller_id = cci.biller_id
                WHERE cb.biller_id IS NULL AND cci.close_commission_id = ?";

        if ($this->db->query($sql, array($fechamentoID))) {
            return true;
        } else {
            return false;
        }
    }

    function getFaturasByFechamento($fechamentoID, $biller_id = NULL) {

        $this->db->select('fatura.*, parcela.numeroparcela, companies.name as biller, tipo_cobranca.id as tipoCobrancaId, tipo_cobranca.note as note_tipo_cobranca,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_pagar.fechamento_comissao_id',$fechamentoID);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contaPagarId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');
        $this->db->join('companies', 'companies.id = fatura.pessoa');

        if ($biller_id) {
            $this->db->where('fatura.pessoa', $biller_id);
        }

        $this->db->where('(fatura.status = "QUITADA" OR fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsByFechamento($fechamentoID, $biller_id = NULL, $ocultar_estorno = FALSE)
    {
        $this->db->select('payments.id as payment_id, payments.date, payments.status, payments.paid_by, payments.amount, payments.taxa, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type, attachment, fatura, note, status, motivo_estorno, companies.name as biller')
            ->join('users', 'users.id=payments.created_by', 'left')
            ->join('companies', 'companies.id=payments.pessoa', 'left');

        if ($biller_id) {
            $this->db->where('payments.pessoa', $biller_id);
        }

        if ($ocultar_estorno) {
            $this->db->where('payments.status IS NULL');
        }

        $q = $this->db->get_where('payments', array('fechamento_comissao_id' => $fechamentoID));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCommissionsFechametoItemToExportExcel($commissionsFechamentoItemID)
    {

        $this->db
            ->select("close_commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    close_commission_items.biller,
                    close_commission_items.customer,
                    close_commission_items.base_value,
                    close_commission_items.commission_percentage,
                    close_commission_items.commission,
                    close_commission_items.paid,
                    (sma_close_commission_items.commission - sma_close_commission_items.paid) as balance,
                    close_commission_items.status,
                    close_commission_items.sale_id")
            ->join('sales', 'sales.id = close_commission_items.sale_id');

        if ($commissionsFechamentoItemID) {
            $this->db->where_in('close_commission_items.id', $commissionsFechamentoItemID);
        }

        if ($this->input->post('flStatusCommission') == 'Aprovada_Parcial') {
            $this->db->where("close_commission_items.status in ('Aprovada','Parcial') ");
        } else if ($this->input->post('flStatusCommission')) {
            $this->db->where('close_commission_items.status', $this->input->post('flStatusCommission'));
        }

        if ($this->input->post('programacaoFilter')) {
            $this->db->where('close_commission_items.programacao_id =', $this->input->post('programacaoFilter'));
        }

        if ($this->input->post('flDataVendaDe')) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $this->input->post('flDataVendaDe'));
        }

        if ($this->input->post('flDataVendaDeAte')) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $this->input->post('flDataVendaDeAte'));
        }

        if ($this->input->post('billerFilter')) {
            $this->db->where('close_commission_items.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('flDataParcelaDe') ||
            $this->input->post('flStatusParcela') ||
            $this->input->post('flTipoCobranca') ||
            $this->input->post('flLiberacaoComissao')) {

            $subqueryf = '(SELECT sma_fatura .sale_id
               FROM sma_fatura WHERE ';

            if ($this->input->post('flStatusParcela')) {

                if ($this->input->post('flStatusParcela') == 'VENCIDA') {
                    $subqueryf .= ' DATEDIFF (sma_fatura.dtVencimento, NOW() ) < 0';
                    $subqueryf .= ' AND sma_fatura.status in ("ABERTA", "PARCIAL")';
                } else {
                    $subqueryf .= ' sma_fatura.status  = "' . $this->input->post('flStatusParcela') . '"';
                }

            } else {
                $subqueryf .= ' sma_fatura.status in ("ABERTA", "PARCIAL", "QUITADA")';
            }

            if ($this->input->post('flDataParcelaDe')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") >= "' . $this->input->post('flDataParcelaDe') . '"';
            }

            if ($this->input->post('flDataParcelaAte')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") <= "' . $this->input->post('flDataParcelaAte') . '"';
            }

            if ($this->input->post('flTipoCobranca')) {
                $subqueryf .= ' AND sma_fatura.tipoCobranca = "' . $this->input->post('flTipoCobranca') . '"';
            }

            if ($this->input->post('flLiberacaoComissao') == 'primeira_parcela_paga') {
                $subqueryf .= ' AND (SELECT COUNT(*) FROM sma_fatura WHERE sma_fatura.sale_id = sma_sales.id AND sma_fatura.status = "QUITADA") > 0';
            }

            $subqueryf .= ')';

            $this->db->where("close_commission_items.sale_id IN $subqueryf");

            if ($this->input->post('flLiberacaoComissao') == 'ultima_parcela_paga') {
                $this->db->where(' (sma_sales.grand_total - sma_sales.paid) <= ',0);
            }
        }

        $this->db->where("sales.payment_status in ('due','partial','paid') ");

        return $this->db->get('close_commission_items');
    }


    public function getCommissionsFechametoToExportExcel($commissionsFechamentoID)
    {

        $this->db
            ->select("close_commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    close_commission_items.biller,
                    close_commission_items.customer,
                    close_commission_items.base_value,
                    close_commission_items.commission_percentage,
                    close_commission_items.commission,
                    close_commission_items.paid,
                    (sma_close_commission_items.commission - sma_close_commission_items.paid) as balance,
                    close_commission_items.status,
                    close_commission_items.sale_id")
            ->join('sales', 'sales.id = close_commission_items.sale_id');

        if ($commissionsFechamentoID) {
            $this->db->where_in('close_commission_items.close_commission_id', $commissionsFechamentoID);
        }

        $this->db->where("sales.payment_status in ('due','partial','paid') ");

        return $this->db->get('close_commission_items');
    }

}