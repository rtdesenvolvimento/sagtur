<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update371 extends CI_Migration {

    public function up() {

        $this->alter_table_fatura_cobranca();

        $this->db->update('settings',  array('version' => '3.7.1'), array('setting_id' => 1));
    }

    public function alter_table_fatura_cobranca() {
        $fields = array(
            'code_sale' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),

            'operadora_cartao' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'installments' => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'valor_financiado' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
        );
        $this->dbforge->add_column('fatura_cobranca', $fields);
    }

    public function down() {}
}
