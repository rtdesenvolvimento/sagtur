<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JunoController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
    }

    function index()
    {
        show_404();
    }

    public function notification()
    {
        $paymentToken = (isset ($_POST['paymentToken']) && trim($_POST['paymentToken']) !== "" ? trim($_POST['paymentToken']) : null);


        echo 'empresa ingracao '.$this->session->userdata('cnpjempresa').'<br/>';
        echo 'paymentToken  '.$paymentToken;
        
        $this->FinanceiroService_model->baixarPagamentoIntegracao('juno', $paymentToken);
        return TRUE;
    }

}