<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update483 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.8.3'), array('setting_id' => 1));
    }


    public function alter_table_shop_settings() {
        $fields = array(
            'pagination_type' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => 'by_date'),
            'number_packages_per_line'   => array('type' => 'INT', 'constraint' => 11, 'default' => 3),

            'is_paginate' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'is_rotate' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
