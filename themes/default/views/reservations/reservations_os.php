<style type="text/css" media="screen">

    #SLData td:nth-child(2) {text-align: center;}
    #SLData td:nth-child(3) {text-align: center;}
    #SLData td:nth-child(4) {text-align: left;}
    #SLData td:nth-child(5) {font-weight: 600;font-size: 12px;}
    #SLData td:nth-child(6) {width: 60%;}

    #SLData td:nth-child(12) {text-align: right;}
    #SLData td:nth-child(13) {text-align: right;}
    #SLData td:nth-child(14) {text-align: right;}

    #SLData td:nth-child(17) {display: none;}

</style>

<script>
    $(document).ready(function () {

        function origem_venda(x) {
            if (x === '1') {
                return '<div class="text-center" title="Venda Vindo do Site"><span class="label label-warning"><i class="fa fa-shopping-cart"></i> Site</span></div>';
            } else {
                return '<div class="text-center" title="Venda Lançada Manualmente"><span class="label label-danger" style="background-color: #428bca;"><i class="fa fa-desktop"></i> Manual</span></div>';
            }
        }

        $('#situacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#billerFilter').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#filterOpcional').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#flDataVendaDe').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#fdivulgacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#flLocalEmbarque').change(function (event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#flTipoTransporte').change(function (event) {
            $('#SLData').DataTable().fnClearTable()
        });

        $('#flDataVendaAte').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#product').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#start_time').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_time').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('reservations/getReservationsOs')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "invoice_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "situacao", "value":  $('#situacao').val() });
                aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                aoData.push({ "name": "filterOpcional", "value":  $('#filterOpcional').val() });
                aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });
                aoData.push({ "name": "flDataVendaAte", "value":  $('#flDataVendaAte').val() });
                aoData.push({ "name": "fdivulgacao", "value":  $('#fdivulgacao').val() });
                aoData.push({ "name": "flLocalEmbarque", "value":  $('#flLocalEmbarque').val() });
                aoData.push({ "name": "flTipoTransporte", "value":  $('#flTipoTransporte').val() });
                aoData.push({ "name": "flGuia", "value":  $('#flGuia').val() });
                aoData.push({ "name": "flMotorista", "value":  $('#flMotorista').val() });
                aoData.push({ "name": "product", "value":  $('#product').val() });
                aoData.push({ "name": "customer", "value":  $('#customer').val() });
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "start_time", "value":  $('#start_time').val() });
                aoData.push({ "name": "end_time", "value":  $('#end_time').val() });
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            },
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                {"mRender": origem_venda},
                null,
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][11]);
                    paid += parseFloat(aaData[aiDisplay[i]][12]);
                    balance += parseFloat(aaData[aiDisplay[i]][13]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[11].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[12].innerHTML = currencyFormat(parseFloat(paid));
                nCells[13].innerHTML = currencyFormat(parseFloat(balance));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('buyer');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('product');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('local_embarque');?>]", filter_type: "text", data: []},
            {column_number: 7, filter_default_label: "[<?=lang('guia');?>]", filter_type: "text", data: []},
            {column_number: 8, filter_default_label: "[<?=lang('motorista');?>]", filter_type: "text", data: []},
            {column_number: 9, filter_default_label: "[<?=lang('tipo_transporte');?>]", filter_type: "text", data: []},
            {column_number: 10, filter_default_label: "[<?=lang('sale_status');?>]", filter_type: "text", data: []},
            {column_number: 14, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer");

        if (localStorage.getItem('remove_slls')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('sdivulgacao')) {
                localStorage.removeItem('sdivulgacao');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            localStorage.removeItem('remove_slls');
        }

        <?php if ($this->session->userdata('remove_slls')) {?>
        if (localStorage.getItem('slitems')) {
            localStorage.removeItem('slitems');
        }
        if (localStorage.getItem('sldiscount')) {
            localStorage.removeItem('sldiscount');
        }
        if (localStorage.getItem('sltax2')) {
            localStorage.removeItem('sltax2');
        }
        if (localStorage.getItem('slref')) {
            localStorage.removeItem('slref');
        }
        if (localStorage.getItem('slshipping')) {
            localStorage.removeItem('slshipping');
        }
        if (localStorage.getItem('slwarehouse')) {
            localStorage.removeItem('slwarehouse');
        }
        if (localStorage.getItem('slnote')) {
            localStorage.removeItem('slnote');
        }
        if (localStorage.getItem('slinnote')) {
            localStorage.removeItem('slinnote');
        }
        if (localStorage.getItem('slcustomer')) {
            localStorage.removeItem('slcustomer');
        }
        if (localStorage.getItem('slbiller')) {
            localStorage.removeItem('slbiller');
        }
        if (localStorage.getItem('sdivulgacao')) {
            localStorage.removeItem('sdivulgacao');
        }
        if (localStorage.getItem('slcurrency')) {
            localStorage.removeItem('slcurrency');
        }
        if (localStorage.getItem('sldate')) {
            localStorage.removeItem('sldate');
        }
        if (localStorage.getItem('slsale_status')) {
            localStorage.removeItem('slsale_status');
        }
        if (localStorage.getItem('slpayment_status')) {
            localStorage.removeItem('slpayment_status');
        }
        if (localStorage.getItem('paid_by')) {
            localStorage.removeItem('paid_by');
        }
        if (localStorage.getItem('amount_1')) {
            localStorage.removeItem('amount_1');
        }
        if (localStorage.getItem('paid_by_1')) {
            localStorage.removeItem('paid_by_1');
        }
        if (localStorage.getItem('pcc_holder_1')) {
            localStorage.removeItem('pcc_holder_1');
        }
        if (localStorage.getItem('pcc_type_1')) {
            localStorage.removeItem('pcc_type_1');
        }
        if (localStorage.getItem('pcc_month_1')) {
            localStorage.removeItem('pcc_month_1');
        }
        if (localStorage.getItem('pcc_year_1')) {
            localStorage.removeItem('pcc_year_1');
        }
        if (localStorage.getItem('pcc_no_1')) {
            localStorage.removeItem('pcc_no_1');
        }
        if (localStorage.getItem('cheque_no_1')) {
            localStorage.removeItem('cheque_no_1');
        }
        if (localStorage.getItem('slpayment_term')) {
            localStorage.removeItem('slpayment_term');
        }

        localStorage.removeItem('previsao_pagamento');
        localStorage.removeItem('valor');
        localStorage.removeItem('tipoCobrancaId');
        localStorage.removeItem('condicaopagamentoId');

        <?php $this->sma->unset_data('remove_slls');}?>

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('reservations/reservations_items_actions', 'id="action-form" target="_blank"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-heart"></i><?=lang('reservations_os')?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('sales/add')?>">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_sale')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="pdf" data-action="export_pdf">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('export_to_pdf')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="reserves_detailed">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('reserves_detailed')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('products/adicionarLocalEmbarque'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('adicionar_local_embarque') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-c.gif"></legend>
            <div style="margin-bottom: 20px;display: none;" class="divfilters">
                <div class="col-sm-4">
                    <?= lang("product", "product") ?>
                    <?php
                    $prod[''] =  lang("select") . " " . lang("product") ;
                    foreach ($products as $produts) {
                        $prod[$produts->id] = $produts->name;
                    }
                    echo form_dropdown('product', $prod, (isset($_POST['product']) ? $_POST['product'] : ''), 'class="form-control select" id="product" placeholder="' . lang("select") . " " . lang("product") . '" style="width:100%"')
                    ?>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("start_date", "start_date"); ?>
                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ''), 'class="form-control" id="start_date"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("end_date", "end_date"); ?>
                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ''), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("start_time", "start_time"); ?>
                        <?php echo form_input('start_time', (isset($_POST['start_time']) ? $_POST['start_time'] : ''), 'class="form-control" id="start_time"', 'time'); ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("end_time", "end_time"); ?>
                        <?php echo form_input('end_time', (isset($_POST['end_time']) ? $_POST['end_time'] : ''), 'type="date" class="form-control" id="end_time"', 'time'); ?>
                    </div>
                </div>
                <div  class="col-sm-4">
                    <?= lang("Status da Venda", "filterStatus") ?>
                    <?php
                    $cbStatus = array(
                        '' => lang('select'),
                        'orcamento' => lang('orcamento'),
                        'faturada' => lang('faturada'),
                        'lista_espera' => lang('lista_espera'),
                        'cancel' => lang('cancel')
                    );
                    echo form_dropdown('filterStatus', $cbStatus,  $ano, 'class="form-control" id="filterStatus"'); ?>
                </div>
                <div  class="col-sm-4">
                    <?= lang("situacao", "situacao") ?>
                    <?php
                    $cbSituacao = array(
                        '' => lang('select'),
                        'due' => lang('due'),
                        'partial' => lang('partial'),
                        'paid' => lang('paid'),
                        'cancel' => lang('cancel'),
                    );
                    echo form_dropdown('situacao', $cbSituacao,  $ano, 'class="form-control" id="situacao"'); ?>
                </div>
                <div class="col-sm-2">
                    <?= lang("biller", "billerFilter"); ?>
                    <?php
                    $bl[""] = lang("select") . ' ' . lang("biller") ;
                    foreach ($billers as $biller) {
                        $bl[$biller->id] = $biller->name;
                    }
                    echo form_dropdown('billerFilter', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("customer", "customer_filter"); ?>
                        <?php
                        echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("meio_divulgacao", "fdivulgacao"); ?>
                        <?php
                        $md[""] = lang("select") . ' ' . lang("meio_divulgacao") ;
                        foreach ($meiosDivulgacao as $divulgacao) {
                            $md[$divulgacao->id] = $divulgacao->name;
                        }
                        echo form_dropdown('fdivulgacao', $md, $inv->meio_divulgacao, 'id="fdivulgacao" name="fdivulgacao" data-placeholder="' . lang("select") . ' ' . lang("meio_divulgacao") . '"class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("data_venda_de", "data_venda_de"); ?>
                        <?php echo form_input('flDataVendaDe', (isset($_POST['flDataVendaDe']) ? $_POST['flDataVendaDe'] : $flDataVendaDe), 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("data_venda_ate", "data_venda_ate"); ?>
                        <?php echo form_input('flDataVendaAte', (isset($_POST['flDataVendaAte']) ? $_POST['flDataVendaAte'] : $flDataVendaAte), 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("local_embarque", "local_embarque"); ?>
                        <?php
                        $cbLocalEmbarque[""] = lang("select") . ' ' . lang("local_embarque") ;
                        foreach ($locais_embarque as $local_embarque) {
                            $cbLocalEmbarque[$local_embarque->id] = $local_embarque->name;
                        }
                        echo form_dropdown('flLocalEmbarque', $cbLocalEmbarque, '', 'id="flLocalEmbarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?= lang("tipo_transporte", "tipo_transporte"); ?>
                        <?php
                        $cbTipoTransporte[""] = lang("select") . ' ' . lang("tipo_transporte") ;
                        foreach ($tipos_transporte as $tipo_transporte) {
                            $cbTipoTransporte[$tipo_transporte->id] = $tipo_transporte->name;
                        }
                        echo form_dropdown('flTipoTransporte', $cbTipoTransporte, '', 'id="flTipoTransporte" data-placeholder="' . lang("select") . ' ' . lang("tipo_transporte") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("guia", "guia"); ?>
                        <?php
                        echo form_input('flGuia', (isset($_POST['flGuia']) ? $_POST['flGuia'] : ""), 'id="flGuia" data-placeholder="' . lang("select") . ' ' . lang("guia") . '" class="form-control input-tip" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("motorista", "motorista"); ?>
                        <?php
                        echo form_input('flMotorista', (isset($_POST['flMotorista']) ? $_POST['flMotorista'] : ""), 'id="flMotorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"');
                        ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-3">
                <div class="form-group">
                    <?= lang("local_embarque", "local_embarque"); ?>
                    <?php
                    $lb[""] = lang("select") . ' ' . lang("local_embarque") ;
                    foreach ($locais_embarque as $local_embarque) {
                        $lb[$local_embarque->id] = $local_embarque->name;
                    }
                    echo form_dropdown('local_embarque', $lb, '', 'id="local_embarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <?= lang("tipo_transporte", "tipo_transporte"); ?>
                    <?php
                    $cbTipoTransporte[""] = lang("select") . ' ' . lang("tipo_transporte") ;
                    foreach ($tipos_transporte as $tipo_transporte) {
                        $cbTipoTransporte[$tipo_transporte->id] = $tipo_transporte->name;
                    }
                    echo form_dropdown('tipo_transporte', $cbTipoTransporte, '', 'id="tipo_transporte" data-placeholder="' . lang("select") . ' ' . lang("tipo_transporte") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= lang("guia", "guia"); ?>
                    <?php
                    echo form_input('guia', (isset($_POST['guia']) ? $_POST['guia'] : ""), 'id="guia" data-placeholder="' . lang("select") . ' ' . lang("guia") . '" class="form-control input-tip" style="width:100%;"');
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= lang("motorista", "motorista"); ?>
                    <?php
                    echo form_input('motorista', (isset($_POST['motorista']) ? $_POST['motorista'] : ""), 'id="motorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"');
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="controls">
                    <input type="button" name="atribuir"  id="atribuir" value="<?=lang('atribuir')?>" class="btn btn-primary">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th><?php echo $this->lang->line("reference_no"); ?></th>
                            <th><?php echo $this->lang->line("biller"); ?></th>
                            <th><?php echo $this->lang->line("customer"); ?></th>
                            <th><?php echo $this->lang->line("product"); ?></th>
                            <th><?php echo $this->lang->line("local_embarque"); ?></th>
                            <th><?php echo $this->lang->line("guia"); ?></th>
                            <th><?php echo $this->lang->line("motorista"); ?></th>
                            <th><?php echo $this->lang->line("tipo_transporte"); ?></th>
                            <th><?php echo $this->lang->line("sale_status"); ?></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th><?php echo $this->lang->line("payment_status"); ?></th>
                            <th><?php echo $this->lang->line("origem"); ?></th>
                            <th style="display: none;"></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="17" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">

    function createFilter(nameClasse) {
        $('.'+nameClasse).click(function() {
            if ($('.div'+nameClasse).is(':visible')) {
                $('.div'+nameClasse).hide(300);
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            } else {
                $('.div'+nameClasse).show(300).fadeIn();
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
            }
        });
    }

    $(document).ready(function () {

        createFilter('filters');
        createFilter('relatorioViagem');
        createFilter('configuracaoViagem');
        createFilter('adicionais');

        $('#atribuir').click(function (event){
            var idsSelecionados = [];

            $('#SLData tbody tr').each(function() {
                // Verifica se o checkbox da linha está marcado
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    debugger;
                    var idLinha = $(this).find('td:nth-last-child(2)').text().trim();
                    idsSelecionados.push(idLinha);

                }
            });

            if (idsSelecionados.length > 0) {

                if (confirm('Deseja realmente atribuir os dados aos item selecionado?')) {
                    $.ajax({
                        type: "get",
                        data: {
                            ids: idsSelecionados ,
                            local_embarque: $('#local_embarque').val(),
                            guia_id: $('#guia').val(),
                            motorista_id: $('#motorista').val(),
                            tipo_transporte_id: $('#tipo_transporte').val(),
                        },
                        async: false,
                        url: site.base_url + "reservations/montar_os/",
                        dataType: "json",
                        success: function (data) {
                            $('#SLData').DataTable().fnClearTable();
                            $('#local_embarque').val(null).trigger('change');
                        }
                    });
                }
            } else {
                alert('Nenhum item selecionado');
            }
        });

        $('#customer').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        $('#guia').select2({
            minimumInputLength: 1,
            data: [],
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        $('#flGuia').select2({
            minimumInputLength: 1,
            data: [],
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        $('#motorista').select2({
            minimumInputLength: 1,
            data: [],
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        $('#flMotorista').select2({
            minimumInputLength: 1,
            data: [],
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        $('body').on('click', '#reservations_os', function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });
    });

</script>