<!DOCTYPE html>
<div data-role="page" data-theme="a" data-url="editarCliente" id="editarCliente">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
        <h2 style="font-weight: bold;text-shadow: none;">Meus Dados<span style="font-size: 10px;"></span></h2>
    </div>
    <div role="main" class="ui-content">
        <form id="formEditarCliente">
            <ul data-role="listview" data-inset="true">
                <li class="ui-field-contain">
                    <input type="text" name="nome" id="nome" value="<?=$customer->name;?>" readonly required="required" data-clear-btn="false">
                </li>
                <li class="ui-field-contain">
                    <label for="cpfCnpj">CPF:</label>
                    <input type="text" name="cpfCnpj" id="cpfCnpj" value="<?=$customer->vat_no;?>" readonly required="required" data-clear-btn="false">
                </li>
                <li class="ui-field-contain">
                    <label for="celular">Celular:</label>
                    <input type="tel" name="celular" id="celular" value="<?=$customer->cf5;?>" readonly data-clear-btn="false">
                </li>
                <li class="ui-field-contain">
                    <label for="telefone">Telefone:</label>
                    <input type="tel" name="telefone" id="telefone" value="<?=$customer->phone;?>" readonly data-clear-btn="false">
                </li>
                <li class="ui-field-contain">
                    <label for="email">E-mail:</label>
                    <input type="email" name="email" id="email" value="<?=$customer->email;?>" readonly data-clear-btn="false">
                </li>
                <li class="ui-field-contain">
                    <label for="email">Nascimento:</label>
                    <input type="date" name="email" id="email" value="<?=$customer->data_aniversario;?>" readonly data-clear-btn="false">
                </li>
                <div data-role="collapsibleset" data-theme="a" data-content-theme="a">
                    <div data-role="collapsible">
                        <h3>Registro Geral</h3>
                        <ul data-role="listview" data-inset="true">
                            <li class="ui-field-contain">
                                <label for="rg">Tipo:</label>
                                <input type="text" name="rg" id="rg" value="<?=strtoupper($customer->tipo_documento);?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="rg">Documento:</label>
                                <input type="text" name="rg" id="rg" value="<?=$customer->cf1;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="orgaoEmissor">Orgão Emissor:</label>
                                <input type="text" name="orgaoEmissor" id="orgaoEmissor" readonly value="<?=$customer->cf3;?>" data-clear-btn="false">
                            </li>
                        </ul>
                    </div>
                </div>
                <div data-role="collapsibleset" data-theme="a" data-content-theme="a">
                    <div data-role="collapsible">
                        <h3>Endereço</h3>
                        <ul data-role="listview" data-inset="true">
                            <li class="ui-field-contain">
                                <label for="cep">CEP:</label>
                                <input type="number" name="cep" id="cep" value="<?=$customer->postal_code;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="rua">Rua:</label>
                                <input type="text" name="rua" id="rua" value="<?=$customer->address;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="numero">Número:</label>
                                <input type="text" name="numero" id="numero" value="<?=$customer->numero;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="complemento">Complemento:</label>
                                <input type="text" name="complemento" id="complemento" value="<?=$customer->complemento;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="bairro">Bairro:</label>
                                <input type="text" name="bairro" id="bairro" value="<?=$customer->bairro;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="cidade">Cidade:</label>
                                <input type="text" name="cidade" id="cidade" value="<?=$customer->city;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="estado">Estado:</label>
                                <input type="text" name="estado" id="estado" value="<?=$customer->state;?>" readonly data-clear-btn="false">
                            </li>
                            <li class="ui-field-contain">
                                <label for="estado">Pais:</label>
                                <input type="text" name="pais" id="pais" value="<?=$customer->country;?>" readonly data-clear-btn="false">
                            </li>
                        </ul>
                    </div>
                </div>
            </ul>
        </form>
    </div>
</div>