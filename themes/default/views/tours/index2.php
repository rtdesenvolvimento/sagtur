<style media="screen">

    #TUData td:nth-child(2) {text-align: center;}
    #TUData td:nth-child(9) {text-align: right;}
    #TUData td:nth-child(10) {text-align: right;}
    #TUData td:nth-child(11) {text-align: right;}

</style>

<script>
    $(document).ready(function () {
        $('#form').hide();
        var oTable = $('#TUData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('tour/getTours')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "invoice_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterProduct", "value":  $('#tour').val() });
                aoData.push({ "name": "filterDateFrom", "value":  $('#dateFrom').val() });
                aoData.push({ "name": "filterDateUp", "value":  $('#dateUp').val() });
                aoData.push({ "name": "filterSatusSale", "value":  $('#status_sale').val() });
                aoData.push({ "name": "filterSatusPayment", "value":  $('#status_payment').val() });
                aoData.push({ "name": "filterBiller", "value":  $('#biller').val() });
                aoData.push({ "name": "filterDateSaleFrom", "value":  $('#dataSaleFrom').val() });
                aoData.push({ "name": "filterDateSaleUp", "value":  $('#dataSaleUp').val() });
                aoData.push({ "name": "filterLocalEmbarque", "value":  $('#local_embarque').val() });

            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][8]);
                    paid += parseFloat(aaData[aiDisplay[i]][9]);
                    balance += parseFloat(aaData[aiDisplay[i]][10]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[8].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[9].innerHTML = currencyFormat(parseFloat(paid));
                nCells[10].innerHTML = currencyFormat(parseFloat(balance));
            },
        }).fnSetFilteringDelay().dtFilter([], "footer");
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('tours/tour_actions', 'id="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-ticket"></i><?=lang('consult_tours')?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip">
                        <i class="icon fa fa-search-minus"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip">
                        <i class="icon fa fa-search-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <p class="introtext"><?= lang('customize_report'); ?> <h3 style="color: red;">EM DESENVOLVIMENTO</h3></p>
        <div id="form">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <?= lang("dateFrom", "dateFrom") ?>
                        <?= form_input('dateFrom', (isset($_POST['dateFrom']) ? $_POST['dateFrom'] : ''), 'class="form-control" id="dateFrom"','date'); ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <?= lang("dateUp", "dateUp") ?>
                        <?= form_input('dateUp', (isset($_POST['dateUp']) ? $_POST['dateUp'] : ''), 'class="form-control" id="dateUp"', 'date'); ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <?= lang("tour", "tour") ?>
                        <?php
                        $prod[''] =  lang("select") . " " . lang("product") ;
                        foreach ($products as $produts) {
                            $prod[$produts->id] = $produts->name;
                        }
                        echo form_dropdown('tour', $prod, (isset($_POST['tour']) ? $_POST['tour'] : ''), 'class="form-control select" id="tour" placeholder="' . lang("select") . " " . lang("tour") . '" required="required" style="width:100%"')
                        ?>
                    </div>
                </div>
                <div  class="col-sm-4">
                    <div class="form-group">
                        <?= lang("status_sale", "status_sale") ?>
                        <?php
                        $cbStatus = array(
                            '' => lang('select'),
                            'orcamento' => lang('orcamento'),
                            'faturada' => lang('faturada'),
                            'lista_espera' => lang('lista_espera'),
                            'cancel' => lang('cancel')
                        );
                        echo form_dropdown('status_sale', $cbStatus,  '', 'class="form-control" id="status_sale"'); ?>
                    </div>
                </div>
                <div  class="col-sm-4">
                    <div class="form-group">
                        <?= lang("status_payment", "status_payment") ?>
                        <?php
                        $cbSituacao = array(
                            '' => lang('select'),
                            'due' => lang('due'),
                            'partial' => lang('partial'),
                            'paid' => lang('paid'),
                            'cancel' => lang('cancel'),
                        );
                        echo form_dropdown('status_payment', $cbSituacao,  '', 'class="form-control" id="status_payment"'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("biller", "biller"); ?>
                        <?php
                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->name;
                        }
                        echo form_dropdown('biller', $bl, '', 'id="biller" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                </div>

                <div class="col-sm-4" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <?= lang("data_sale_from", "dataSaleFrom"); ?>
                        <?php echo form_input('dataSaleFrom', '', 'type="date" class="form-control" id="dataSaleFrom"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("data_venda_ate", "dataSaleUp"); ?>
                        <?php echo form_input('dataSaleUp','', 'type="date" class="form-control" id="dataSaleUp"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("local_embarque", "local_embarque"); ?>
                        <?php
                        $lb[""] = lang("select") . ' ' . lang("local_embarque") ;
                        foreach ($locais_embarque as $local_embarque) {
                            $lb[$local_embarque->id] = $local_embarque->name;
                        }
                        echo form_dropdown('local_embarque', $lb, '', 'id="local_embarque" name="biller" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="TUData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date_exit"); ?></th>
                            <th><?php echo $this->lang->line("pick_up"); ?></th>
                            <th><?php echo $this->lang->line("tour"); ?></th>
                            <th><?php echo $this->lang->line("reference_no"); ?></th>
                            <th><?php echo $this->lang->line("biller"); ?></th>
                            <th><?php echo $this->lang->line("customer"); ?></th>
                            <th><?php echo $this->lang->line("tour_status"); ?></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th><?php echo $this->lang->line("payment_status"); ?></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">
    $(document).ready(function () {

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $('#tour').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#dateFrom').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#dateUp').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#status_sale').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#status_payment').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#biller').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#dataSaleFrom').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#dataSaleUp').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#local_embarque').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });
    });
</script>
