<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update499 extends CI_Migration {

    public function up() {

        $this->alter_table_payments();

        $this->db->update('settings',  array('version' => '4.9.9'), array('setting_id' => 1));
    }

    public function alter_table_payments()
    {
        $fields = array(
            'attachment_storage' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => TRUE),
        );
        $this->dbforge->add_column('payments', $fields);
    }

    public function down() {}
}
