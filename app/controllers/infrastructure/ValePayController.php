<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ValePayController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository_model');

        $this->load->model('financeiro_model');

        if ($this->session->userdata('cnpjempresa') == '')  $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
    }

    function index()
    {
        show_404();
    }


    public function notification()
    {

        $json_body = file_get_contents('php://input');

        $obj = json_decode($json_body);

        if (!empty($obj->uuid)) {

            $uuid = 'pay_'.$obj->uuid;

            $cobrancaSistemaPIX = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($uuid);

            if (!empty($cobrancaSistemaPIX)) {
                $this->financeiro_model->edit('fatura_cobranca', array('log' => $json_body),'id', $cobrancaSistemaPIX->id);
                $this->FinanceiroService_model->baixarPagamentoIntegracao('valepay', $uuid);
            } else {

                $cobrancaSistemaCartaoCredito = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($obj->uuid);

                if (!empty($cobrancaSistemaCartaoCredito)) {

                    if ($this->fatura_aberta($cobrancaSistemaCartaoCredito)) {

                        $uuid = 'pay_cc_'.$obj->uuid;

                        $this->financeiro_model->edit('fatura_cobranca', array('log' => $json_body),'id', $cobrancaSistemaCartaoCredito->id);
                        $this->FinanceiroService_model->baixarPagamentoIntegracao('valepay', $uuid);
                    }

                }
            }

        } else {
            echo '0';
        }

        return TRUE;
    }

    private function fatura_aberta($cobrancaFatura): bool
    {
        return $cobrancaFatura->status !== 'QUITADA' && $cobrancaFatura->status !== 'CANCELADA';
    }

}