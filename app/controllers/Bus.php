<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->lang->load('bus', $this->Settings->user_language);

        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');
        $this->load->model('repository/AutomovelRepository_model', 'AutomovelRepository_model');
        $this->load->model('repository/CorRepository_model', 'CorRepository_model');

        $this->load->model('service/BusService_model', 'BusService_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model('dto/BusDTO_model', 'BusDTO_model');
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $this->load->model('model/Automovel_model', 'Automovel_model');
        $this->load->model('model/AutomovelMapa_model', 'AutomovelMapa_model');

        $this->load->model('products_model');
        $this->load->model('settings_model');

        $this->load->library('form_validation');
    }

    function index()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bus'), 'page' => lang('plantas')), array('link' => '#', 'page' => lang('bus')));
        $meta = array('page_title' => lang('bus'), 'bc' => $bc);
        $this->page_construct('bus/index', $meta, $this->data);
    }

    function acompanhar_marcacao()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bus'), 'page' => lang('plantas')), array('link' => '#', 'page' => lang('acompanhar_marcacao')));
        $meta = array('page_title' => lang('acompanhar_marcacao'), 'bc' => $bc);

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

        $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);

        $this->page_construct('bus/acompanhar_marcacao', $meta, $this->data);
    }

    function getMapas()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, andares, total_assentos")
            ->from("automovel")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('bus/editBus/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_bus") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_bus") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('bus/deletarBus/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addBus() {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $assentos = json_decode($this->input->post('mapa'));
            $name = $this->input->post('name');
            $andares = $this->input->post('numero-andares');
            $note =  $this->input->post('note');

            $automovel = new Automovel_model();
            $automovel->name = $name;
            $automovel->andares = $andares;
            $automovel->note = $note;

            foreach ($assentos as $assento) {
                $assento_automovel = new AutomovelMapa_model();

                $assento_automovel->andar = $assento->andar;
                $assento_automovel->name = $assento->assento_name;
                $assento_automovel->assento = $assento->assento_assento;
                $assento_automovel->habilitado = $assento->assento_status;
                $assento_automovel->note = $assento->assento_note;
                $assento_automovel->ordem = $assento->assento_order;
                $assento_automovel->x = $assento->x;
                $assento_automovel->y = $assento->y;
                $assento_automovel->z = $assento->z;

                $automovel->addAssento($assento_automovel);
            }

        } elseif ($this->input->post('addBus')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("bus/addBus");
        }

        if ($this->form_validation->run() == true && $this->BusService_model->addBus($automovel)) {
            $this->session->set_flashdata('message', lang("bus_adicionado_com_sucesso"));
            redirect("bus");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['automoveis'] = $this->AutomovelRepository_model->getAll();

            $this->load->view($this->theme . 'bus/add', $this->data);
        }
    }

    function editBus($id) {

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $assentos = json_decode($this->input->post('mapa'));
            $name = $this->input->post('name');
            $andares = $this->input->post('numero-andares');
            $note =  $this->input->post('note');

            $automovel = new Automovel_model();
            $automovel->name = $name;
            $automovel->andares = $andares;
            $automovel->note = $note;

            foreach ($assentos as $assento) {
                $assento_automovel = new AutomovelMapa_model();

                $assento_automovel->andar = $assento->andar;
                $assento_automovel->name = $assento->assento_name;
                $assento_automovel->assento = $assento->assento_assento;
                $assento_automovel->habilitado = $assento->assento_status;
                $assento_automovel->note = $assento->assento_note;
                $assento_automovel->ordem = $assento->assento_order;
                $assento_automovel->x = $assento->x;
                $assento_automovel->y = $assento->y;
                $assento_automovel->z = $assento->z;

                $automovel->addAssento($assento_automovel);
            }

        } elseif ($this->input->post('editBus')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("bus/addBus");
        }

        if ($this->form_validation->run() == true && $this->BusService_model->editBus($automovel, $id)) {
            $this->session->set_flashdata('message', lang("bus_editado_com_sucesso"));
            redirect("bus");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['automoveis'] = $this->AutomovelRepository_model->getAll();
            $this->data['automovel'] = $this->AutomovelRepository_model->getById($id);

            $this->load->view($this->theme . 'bus/edit', $this->data);
        }
    }

    function deletarBus($id) {
        if ($this->BusService_model->deletar_bus($id)) {
            echo lang("automovel_deletado_com_sucesso");
        }
    }

    function bus_base()
    {
        $automovelID = $this->input->get('automel_id');

        $this->data['assentos'] = $this->BusRepository_model->getMapaByAutomovel($automovelID, 1);
        $this->data['assentos2'] = $this->BusRepository_model->getMapaByAutomovel($automovelID, 2);

        $this->load->view($this->theme . 'bus/bus_base', $this->data);
    }

    function marcacao($productID, $programacaoID, $tipoTransporteID)
    {
        $product = $this->site->getProductByID($productID);

        if (!$productID || !$product || !$tipoTransporteID || !$programacaoID) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $tipoTransporte = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteID);

        $this->data['tipoTransporte']   = $tipoTransporte;
        $this->data['programacaoId']    = $programacaoID;
        $this->data['product']          = $product;
        $this->data['programacao']      = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoID);
        $this->data['assentos']         = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id, 1);
        $this->data['assentos2']        = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id, 2);
        $this->data['bloqueados']       = $this->BusRepository_model->getAssentosBloqueados($productID, $programacaoID, $tipoTransporte->id);

        $this->data['cobrancaExtraConfig']  = $this->TipoTransporteRodoviarioRepository_model->getTransportesRodoviarioProduto($productID, $tipoTransporteID);

        $this->load->view($this->theme . 'appcompra/bus', $this->data);
    }

    public function bloqueio_assento_site() {

        try {

            $dados = json_decode($this->input->get('dados'));
            $assentos = $dados->assentos;
            $bloqueios_bus = array();

            foreach ($assentos as $assento) {

                $bloqueio_bus = new BusDTO_model();
                $bloqueio_bus->tipo_transporte_id = $dados->tipo_transporte_id;
                $bloqueio_bus->programacao_id =  $dados->programacao_id;
                $bloqueio_bus->product_id = $dados->product_id;

                $bloqueio_bus->assento = $assento->assento;
                $bloqueio_bus->assentoStr = $assento->assento;//VIR
                $bloqueio_bus->note = 'BLOQUEADO VIA LINK #EM RESERVA';//nome do cliente e ip da maquina do cliente

                $bloqueios_bus[] = $bloqueio_bus;
            }

            $this->BusService_model->bloqueio_assentos_site($bloqueios_bus);

            echo $this->sma->send_json(array('ocupado' => false, 'error' => ''));

        } catch (Exception $exception) {
            echo $this->sma->send_json(array('ocupado' => true, 'error' => $exception->getMessage()));
        }
    }

    public function marcacao_assento_area_cliente() {

        try {

            $dados = json_decode($this->input->get('dados'));
            $assentos = $dados->assentos;
            $marcacoes_bus = array();

            foreach ($assentos as $assento) {

                $marcacao_bus = new BusDTO_model();
                $marcacao_bus->tipo_transporte_id = $dados->tipo_transporte_id;
                $marcacao_bus->programacao_id =  $dados->programacao_id;
                $marcacao_bus->product_id = $dados->product_id;

                $marcacao_bus->item_id = $assento->id;
                $marcacao_bus->assento = $assento->assento;
                $marcacao_bus->assentoStr = $assento->assento;
                $marcacao_bus->note = 'MARCACAO VIA LINK #EM RESERVA';

                $marcacoes_bus[] = $marcacao_bus;
            }

            $this->BusService_model->marcacao_assento_area_cliente($marcacoes_bus);

            echo $this->sma->send_json(array('ocupado' => false, 'error' => ''));

        } catch (Exception $exception) {
            echo $this->sma->send_json(array('ocupado' => true, 'error' => $exception->getMessage()));
        }
    }

    public function desbloquear_todos_assentos_session_card() {
        $this->BusService_model->desbloquear_todos_assentos_session_card();
        echo $this->sma->send_json(array('desbloqueado' => true));
    }

    public function bloqueio_assento() {

        $bloqueio_bus = new BusDTO_model();
        $bloqueio_bus->tipo_transporte_id = $this->input->get('tipo_transporte_id');
        $bloqueio_bus->programacao_id = $this->input->get('programacao_id');
        $bloqueio_bus->product_id = $this->input->get('product_id');
        $bloqueio_bus->assento = $this->input->get('assento');
        $bloqueio_bus->assentoStr = $this->input->get('assento_name');
        $bloqueio_bus->note = $this->input->get('note');

        $this->BusService_model->bloqueio_assento($bloqueio_bus);
    }

    public function desbloqueio_assento() {
        $bloqueio_id = $this->input->get('bloqueio');

        $this->BusService_model->desbloqueio_assento($bloqueio_id);
    }

    public function atribuir_todos_ao_automovel() {

        $tipoTransporteID = $this->input->get('tipo_transporte_id');
        $programacaoID = $this->input->get('programacao_id');
        $productID = $this->input->get('product_id');
        $embarqueID = $this->input->get('embarque_id');

        $this->BusService_model->atribuir_todos_ao_automovel($productID, $programacaoID, $tipoTransporteID, $embarqueID);
    }

    public function atribuir_assento() {

        $productID = $this->input->get('product_id');
        $programacaoID = $this->input->get('programacao_id');
        $tipoTransporte = $this->input->get('tipoTransporte');
        $itemId = $this->input->get('itemId');
        $passento = $this->input->get('poltrona');

        if (strpos($passento, 'C') !== false) {
            $assentoFormat = $passento;
        } else {
            $assentoFormat = sprintf('%02d', $passento);
        }

        if ($assentoFormat == '00') $assentoFormat = null;

        $isAssentoOcupado = $this->BusRepository_model->isAssentoMarcado($productID, $programacaoID, $tipoTransporte, $assentoFormat, $itemId);

        if ($isAssentoOcupado) {
            echo $this->sma->send_json(array('ocupado' => true, 'bloqueado' => false, 'passageiro' => $isAssentoOcupado->customerClientName));
        } else {
            $isAssentoBloqueado =  $this->BusRepository_model->isAssentoBloqueado($productID, $programacaoID, $tipoTransporte, $assentoFormat);
            if ($isAssentoBloqueado) {
                echo $this->sma->send_json(array('ocupado' => false, 'bloqueado' => true, 'note' => $isAssentoBloqueado->note));
            } else {
                $this->BusService_model->atribuir_assento($itemId, $assentoFormat, $tipoTransporte);
                echo $this->sma->send_json(array('ocupado' => false, 'bloqueado' => false));
            }
        }
    }

    function cobranca_extra_assento()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('bus'),
                'page' => lang('bus')),
            array('link' => '#', 'page' => lang('configurar_cobranca_extra')));
        $meta = array('page_title' => lang('valor_faixa'), 'bc' => $bc);
        $this->page_construct('bus/cobranca_extra_assento', $meta, $this->data);
    }

    function getCobrancasExtraAssento()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name as name")
            ->from("configuracao_cobranca_extra_assento")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('bus/editarPrecoAssentoExtra/$1') . "'class='tip' title='" . lang("edit_preco_assento_extra") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_cobranca_extra") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('bus/deletarCobrancaExtra/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarPrecoAssentoExtra()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('tipo_transporte', lang("tipo_transporte"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'tipo_transporte_id' => $this->input->post('tipo_transporte'),
                //'note' => $this->input->post('note'),
            );

            $extra_assentos = $this->adicionarValorExtraAssentos();

        } elseif ($this->input->post('adicionarPrecoAssentoExtra')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("bus/adicionarPrecoAssentoExtra");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarPrecoAssentoExtra($data, $extra_assentos)) {
            $this->session->set_flashdata('message', lang("cobranca_extra_assento_addicionada_com_sucesso"));
            redirect("bus/cobranca_extra_assento");
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_assento_extra')));
            $meta = array('page_title' => lang('add_assento_extra'), 'bc' => $bc);

            $this->data['tiposTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviarioComMapaBase();

            $this->page_construct('bus/add_assento_extra', $meta, $this->data);
        }
    }

    function editarPrecoAssentoExtra($id=NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('tipo_transporte', lang("tipo_transporte"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'tipo_transporte_id' => $this->input->post('tipo_transporte'),
                //'note' => $this->input->post('note'),
            );

            $extra_assentos = $this->adicionarValorExtraAssentos();

        } elseif ($this->input->post('adicionarPrecoAssentoExtra')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("bus/adicionarPrecoAssentoExtra");
        }

        if ($this->form_validation->run() == true && $this->settings_model->editarPrecoAssentoExtra($id, $data, $extra_assentos)) {
            $this->session->set_flashdata('message', lang("cobranca_extra_assento_editado_com_sucesso"));
            redirect("bus/editarPrecoAssentoExtra/".$id);
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_preco_assento_extra')));
            $meta = array('page_title' => lang('edit_preco_assento_extra'), 'bc' => $bc);

            $this->data['extra']                    = $this->settings_model->getConfiguracaoExtraAssento($id);
            $this->data['assentosMarcacaoAndar1']   = $this->settings_model->getAssentosCobrancaExtra($id, 1);
            $this->data['assentosMarcacaoAndar2']   = $this->settings_model->getAssentosCobrancaExtra($id, 2);
            $this->data['cores']                    = $this->CorRepository_model->getAll();
            $this->data['tiposTransporte']          = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviarioComMapaBase();

            $this->page_construct('bus/edit_assento_extra', $meta, $this->data);
        }
    }

    private function adicionarValorExtraAssentos() {
        $a = sizeof($_POST ['assento']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['assento'] [$r] )) {
                $extras [] = array (
                    'assento' => $_POST ['assento'] [$r],
                    'valor' => $_POST ['valor_extra'] [$r],
                    'andar' => $_POST ['andar'] [$r],
                    'cor_id' => $_POST ['cor'] [$r],
                );
            }
        }
        return $extras;
    }

    function carregar_assentos() {

        $tipoTransporte = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($this->input->get('tipo_transporte_id'));

        $this->data['assentos']         = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id);
        $this->data['assentos2']        = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id, 2);
        $this->data['cores']            = $this->CorRepository_model->getAll();

        $this->load->view($this->theme . 'bus/carregar_assentos_table', $this->data);
    }

    function deletarCobrancaExtra($id) {
        if ($this->settings_model->deletarCobrancaExtra($id)) {
            echo lang("cobranca_extra_deletado_com_sucesso");
        }
    }

    function bus_assento_cobranca_extra($tipoTransporteId, $id = NULL)
    {

        if (!$tipoTransporteId) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $tipoTransporte = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);

        $this->data['assentosMarcacaoAndar1']   = $this->settings_model->getAssentosCobrancaExtra($id, 1);
        $this->data['assentosMarcacaoAndar2']   = $this->settings_model->getAssentosCobrancaExtra($id, 2);

        $this->data['extra']                    = $this->settings_model->getConfiguracaoExtraAssento($id);
        $this->data['tipoTransporte']           = $tipoTransporte;
        $this->data['assentos']                 = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id);
        $this->data['assentos2']                = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id, 2);

        $this->data['rotulos']                  = $this->BusRepository_model->getlRotuloCobrancaExtra($id);

        if ($tipoTransporte->automovel_id) {
            $this->load->view($this->theme . 'bus/planta_assento_cobranca_extra', $this->data);
        }
    }
}