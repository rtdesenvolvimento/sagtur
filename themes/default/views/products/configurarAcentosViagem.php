<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
    #special {
        width: <?php echo $visao->width;?>px;
        height: <?php echo $visao->height;?>px;
        border:1px ridge #0e90d2;
        background: #0e90d2;
    }
</style>
<body style="text-align: center;">
<h2 id="status2" style="display: none;">0, 0</h2>
<canvas id="special" style="background-image: url('<?php echo base_url().'/plantas/'.$visao->planta; ?>');">Not supported</canvas>
<img id="ocupada" style="display: none;" src="<?php echo base_url();?>themes/default/assets/poltronas_arquivos/images/poltrona-ocupada.png">
<img id="livre" style="display: none;" src="<?php echo base_url();?>themes/default/assets/poltronas_arquivos/images/poltrona-livre.png">
<script>


    jQuery(document).ready(function(){

        var c = document.getElementById("special");
        var elemLeft = c.offsetLeft;
        var elemTop = c.offsetTop;
        var elements = [];

        $('#special').attr('height', $('#special').css('height'));
        $('#special').attr('width', $('#special').css('width'));

        $("#special").click(function(e){
            var x = event.pageX - elemLeft,
                y = event.pageY - elemTop;
            $.each(elements, function(i, element) {
                if ( (x >= element.width  && x <= element.width + 30) &&
                    (y >= element.height  && y <= element.height + 30) ) {

                    var venda = element.venda;
                    if (venda != '') {
                        var customer = element.customer;
                        if (confirm('Passageiro '+customer+' \nDeseja abrir os dados da poltrona?')) {
                            var url = '<?php echo base_url() ?>sales/edit/' + venda;
                            window.open(url,'_blank');
                        }
                    } else {
                        var variacao = element.variacao;
                        var poltrona = element.poltrona;
                        window.parent.setar_poltrona(poltrona, variacao);
                    }
                }
            });
        });

        <?php foreach($posicoes as $posicao){

            $ocupado        = $this->products_model->getSaleByPoltrona($posicao->name, $onibus->id);
            $ocupadoItem    = $this->products_model->getSaleByPoltronaByItemVenda($posicao->name, $onibus->id);?>

            var onibus      = document.getElementById("special");
            var posicao     = onibus.getContext("2d");
            var poltrona    = onibus.getContext("2d");

            <?php if ($ocupado || $ocupadoItem) {

                $customer = null;
                $venda    = null;

                if ($ocupado) {
                    $customer   = $ocupado->customer;
                    $venda      = $ocupado->id;
                } else {
                    $customer   = $ocupadoItem->customer;
                    $venda      = $ocupadoItem->id;

                    if ($customer == 'undefined') {
                        $customer = $ocupadoItem->responsavelPassagem;
                    }
                }
                ?>

                elements.push({
                    width: <?php echo $posicao->client_x;?>,
                    height: <?php echo $posicao->client_y;?>,
                    idVisao : <?php echo $posicao->id;?>,
                    variacao: <?php echo $onibus->id;?>,
                    poltrona: <?php echo $posicao->name; ?>,
                    customer: '<?php echo $customer;?>',
                    venda: <?php echo $venda;?>
                });

                <?php if ($posicao->tipo == 'acento') {?>
                    var img         = document.getElementById("ocupada");
                    poltrona.drawImage(img, <?php echo $posicao->client_x; ?> ,  <?php echo $posicao->client_y; ?>);
                    posicao.fillStyle = "white";
                <?php } else { ?>
                    posicao.fillStyle = "red";
                <?php } ?>

                posicao.beginPath();
                posicao.fill();
                posicao.stroke();
                posicao.font="12px sans-serif";
                posicao.fillText('<?php echo $posicao->name; ?>',<?php echo $posicao->client_x + 10; ?>,  <?php echo $posicao->client_y + 15; ?>);
            <?php } else { ?>
                var onibus   = document.getElementById("special");
                var posicao = onibus.getContext("2d");
                posicao.beginPath();

                <?php if ($posicao->tipo == 'acento') {?>
                    var img         = document.getElementById("livre");
                    poltrona.drawImage(img, <?php echo $posicao->client_x; ?> ,  <?php echo $posicao->client_y; ?>);
                    posicao.fillStyle = "white";
                <?php } else { ?>
                    posicao.fillStyle = "black";
                <?php } ?>

                posicao.fill();
                posicao.stroke();
                posicao.font="12px sans-serif";
                posicao.fillText('<?php echo $posicao->name; ?>',<?php echo $posicao->client_x + 10; ?>,  <?php echo $posicao->client_y + 15; ?>);
            <?php } ?>
        <?php  }?>
    })
</script>

</body>