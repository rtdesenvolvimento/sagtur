
<style type="text/css" media="screen">
    #CobData td:nth-child(1) {
        display: none;
    }


    #CobData td:nth-child(2) {
        text-align: center;
    }

    #CobData td:nth-child(3) {
        text-align: center;
    }

    #CobData td:nth-child(4) {
        text-align: center;
    }

    #CobData td:nth-child(5) {
        text-align: center;
    }


    #CobData td:nth-child(6) {
        display: none;
    }

    #HysData td:nth-child(1) {
        display: none;
    }

    #HysData td:nth-child(2) {
        text-align: center;
    }

    #HysData td:nth-child(3) {
        text-align: center;
    }

    #HysData td:nth-child(4) {
        text-align: center;
    }

    #HysData td:nth-child(5) {
        text-align: center;
    }

</style>

<script>
    $(document).ready(function () {

        function row_status_financeiro(x,  alignment, aaData) {

            let diasAtraso = aaData[5];

            if(x === 'ABERTA') {
                if (diasAtraso < 0) {
                    return '<div class="text-center"><span class="label label-danger">VENCIDA</span></div>';
                } else {
                    return '<div class="text-center"><span class="label label-warning">PENDENTE</span></div>';
                }
            } else if (x === 'QUITADA') {
                return '<div class="text-center"><span class="label label-success">PAGO</span></div>';
            }
        }

        var cTable = $('#CobData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('account/getNextCharges') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                {"mRender": fsd},
                {"mRender": currencyFormat},
                null,
                {"mRender": row_status_financeiro},
                null,
                {"bSortable": false}
            ]
        }).dtFilter([], "footer");


        var cTableh = $('#HysData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('account/getBillingHistory') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                {"mRender": fsd},
                {"mRender": currencyFormat},
                null,
                {"mRender": row_status_financeiro},
                {"bSortable": false}
            ]
        }).dtFilter([], "footer");

    });
</script>

<div class="row">
    <div class="col-lg-2">
        <div class="col-sm-12 text-center">
            <div class="max-width:200px; margin: 0 auto;">
                <img src="<?= base_url(); ?>assets/uploads/logos/sagtur.png" alt="<?php echo $Settings->site_name ;?>">
                <h4>Plano Contratado</h4>
                <p><?= $customer->customer_group_name;?></p>
                <p><?= $this->sma->formatMoney($group->price);?>/mês</p>
            </div>
        </div>
    </div>
    <div class="col-lg-10">
        <ul id="myTab" class="nav nav-tabs" style="text-align: center">
            <li class=""><a href="#next_changes" class="tab-grey"><i class="fa fa-usd" style="font-size: 20px;"></i><br/><?= lang('next_changes') ?></a></li>
            <li class="" id="tbFinanceiro"><a href="#billing_history" class="tab-grey"><i class="fa fa-money" style="font-size: 20px;"></i><br/><?= lang('billing_history') ?></a></li>
            <li class="" id="tbTermosCancelamento"><a href="#termos_cancelamento" class="tab-grey"><i class="fa fa-tasks" style="font-size: 20px;"></i><br/><?= lang('termos_cancelamento') ?></a></li>
            <li class="" id="tbDadosCadastradis"><a href="#seus_dados" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('my_data') ?></a></li>
        </ul>
        <div class="tab-content">
            <div id="next_changes" class="tab-pane fade in">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('next_changes_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="CobData" cellpadding="0" cellspacing="0" border="0"
                                           class="table table-bordered table-condensed table-hover table-striped">
                                        <thead>
                                        <tr class="primary">
                                            <th style="text-align: center;width: 1%;display: none;" ></th>
                                            <th style="text-align: center;"><?= lang("due_date"); ?></th>
                                            <th style="text-align: center;"><?= lang("payment_amount"); ?></th>
                                            <th style="text-align: center;"><?= lang("plan"); ?></th>
                                            <th style="text-align: center;"><?= lang("situacao"); ?></th>
                                            <th style="display: none;"></th>
                                            <th style="text-align: center;"><?= lang("pay"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="min-width:20px !important;" class="text-center"><?= lang("actions"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="billing_history" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('billing_history_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="HysData" cellpadding="0" cellspacing="0" border="0"
                                           class="table table-bordered table-condensed table-hover table-striped">
                                        <thead>
                                        <tr class="primary">
                                            <th style="text-align: center;width: 1%;display: none;" ></th>
                                            <th style="text-align: center;"><?= lang("pay_day"); ?></th>
                                            <th style="text-align: center;"><?= lang("amount_paid"); ?></th>
                                            <th style="text-align: center;"><?= lang("plan"); ?></th>
                                            <th style="text-align: center;"><?= lang("situacao"); ?></th>
                                            <th style="text-align: center;width: 5%;"><?= lang("pay"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="min-width:20px !important;" class="text-center"><?= lang("actions"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="termos_cancelamento" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('termos_cancelamento_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>

                    <div class="box-content">
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 1ª–DO OBJETO DO CONTRATO</strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            O
                            presente instrumento tem como objeto a&nbsp;
                            <strong>LICENÇA
                                DE USO
                            </strong>do sistema de gestão denominado <strong>SAGTUR</strong> para uso exclusivo no
                            segmento de&nbsp;
                            <strong>AGÊNCIAS DE TURISMO</strong>, com objetivo principal a gestão do
                            negócio do
                            <strong>LICENCIADO</strong>.
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEGUNDO: </strong>
                        </p>
                        <p>
                            A <strong>LICENCIANTE</strong> garante ao&nbsp;<strong>LICENCIADO&nbsp;</strong>o acesso ao sistema em até 48
                            horas
                            <strong>(2 DIAS)</strong> após o pagamento da primeira mensalidade, assinatura do
                            contrato, dando início às configurações do sistema e criação dos usuários/senhas
                            conforme o plano contratado pelo
                            <strong>LICENCIADO</strong>.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 2ª–PAGAMENTO DA MENSALIDADE
                                / MUDANÇA DE PLANO / REAJUSTE NA MENSALIDADE
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong> pagará ao&nbsp;<strong>LICENCIANTE&nbsp;</strong>o valor (<strong>MENSAL</strong>)via <strong>PIX/BOLETO</strong>
                            referente ao plano contratado, pela disponibilidade da licença do sistema&nbsp;<strong>SAGTUR</strong>.<a></a>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEGUNDO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO&nbsp;</strong>terá acesso aos boletos
                            do ano corrente em sua área do cliente para acompanhar seus boletos pagos,
                            vencidos e boletos a vencer. A data de vencimento será fixada sempre no
                            dia10/15/20/25/30 conforme a data da assinatura deste contrato.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>TERCEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong>poderá solicitar à primeira
                            mudança do plano a qualquer momento. O valor referente a mudança do novo planocontratado
                            ocorrerá a partir do mês consecutivo. Após a primeira mudança de plano só será
                            possível realizar uma nova mudança de plano a cada 60 dias.
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>QUARTO:</strong>
                        </p>
                        <p>
                            O&nbsp;<strong>LICENCIADO&nbsp;</strong>está ciente que a contratação do sistema é uma assinatura mensal, será
                            cobrado a mensalidade normalmente do
                            <strong>LICENCIADO</strong> utilizando o sistema ou não. Não é uma contratação pré-paga.<strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>QUINTO:</strong>
                        </p>
                        <p>
                            O&nbsp;<strong>LICENCIADO&nbsp;</strong>está ciente que a partir do mês de janeiro de cada ano, haverá um
                            reajuste em nossos planos, mesmo que o
                            <strong>LICENCIADO</strong> contrate um plano a poucos meses do mês de janeiro.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 3ª – BLOQUEIO E
                                CANCELAMENTO DO SISTEMA POR FALTA DE PAGAMENTO
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            Se o&nbsp;<strong>LICENCIADO&nbsp;</strong>estiver com sua mensalidade vencida a <strong>10 DIAS</strong> seu sistema será <strong>(BLOQUEADO
                                AUTOMATICAMENTE),
                            </strong>e caso sua mensalidade esteja a mais de<strong>30 DIAS&nbsp;</strong>em aberto, seu sistema será<strong>(CANCELADO AUTOMATICAMENTE)</strong>ficando com uma
                            restrição em nosso sistema por falta de pagamento.
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEGUNDO:</strong>
                        </p>
                        <p>
                            Se ocorrer o <strong>(CANCELAMENTO
                            </strong><strong>AUTOMÁTICO</strong><strong>)</strong> do sistema por falta de pagamento e sem solicitação do <strong>LICENCIADO</strong>, e sua mensalidade ficar pendente em
                            nosso sistema por mais de
                            <strong>60 DIAS</strong>, sua cobrança será<strong>PROTESTADA</strong> pela <strong>SAGTUR</strong>. A <strong>SAGTUR</strong> se reserva ao direito de <strong>PROTESTAR</strong> o boleto através da plataforma <strong>ASAAS </strong>e de incluir o nome da sua agência em cadastros de inadimplência e/ou
                            negativação. Todas as custas e taxas resultantes deste protesto serão de
                            responsabilidade
                            <strong>EXCLUSIVA</strong> da agência.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 4ª – SOLICITAÇÃO DE
                                CANCELAMENTO DO SISTEMA PELO ART.49 DO CDC
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            Após a contratação,
                            caso o
                            <strong>LICENCIADO </strong>solicite o cancelamento do contrato em até 7 dias corridos, faremos a
                            devolução total da mensalidade paga e realizaremos o cancelamento do sistema e
                            dos boletos já gerados.  Caso seja
                            solicitado o cancelamento após os 7 dias
                            <strong>NÃO HAVERÁ</strong> reembolso da mensalidade paga.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>Artigo 49 do Código de Defesa do
                                Consumidor (CDC):
                            </strong>
                        </p>
                        <p>
                            A
                            cláusula de devolução de um serviço no prazo de 7 dias está prevista no artigo
                            49 do Código de Defesa do Consumidor (CDC): O consumidor pode desistir de um
                            contrato de serviço no prazo de 7 dias corridos. O prazo começa a contar a
                            partir da assinatura do contrato ou do recebimento do serviço. O direito de
                            arrependimento aplica-se a contratações realizadas fora do estabelecimento
                            comercial, como por telefone ou em domicílio. O consumidor não precisa dar
                            explicações sobre o arrependimento
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEGUNDO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO </strong>poderá <strong>CANCELAR</strong> seu plano a qualquer momento sem cobrança de
                            taxa de cancelamento ou multas, desde que ocorra a comunicação (
                            <strong>PRÉVIA ATÉ O DIA DO VENCIMENTO DA
                                MENSALIDADE)
                            </strong>. Caso não o faça e seu
                            boleto já estiver
                            <strong>VENCIDO&nbsp;</strong>será necessário realizar o pagamento do
                            boleto
                            <strong>VENCIDO</strong> e solicitar o <strong>(CANCELAMENTO).</strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 5ª – PROTESTO DE BOLETO
                                VENCIDO
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong> concorda em manter todos os boletos em dia, de acordo com os termos e
                            condições estabelecidos no contrato de serviço assinado digitalmente. Caso o
                            <strong>LICENCIADO</strong> atrase o pagamento do boleto por mais
                            de 60 dias, a
                            <strong>SAGTUR</strong> se reserva ao direito de protestar o boleto através da plataforma <strong>ASAAS</strong> e de incluir o nome do <strong>LICENCIADO</strong> em cadastros de inadimplência e/ou
                            negativação. Todas as custas e taxas resultantes deste protesto serão de
                            responsabilidade exclusiva do
                            <strong>LICENCIADO</strong>.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 6ª–DO SUPORTE</strong><strong></strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            A<strong>LICENCIANTE</strong> prestará suporte por
                            solicitação do
                            <strong>LICENCIADO</strong>, o qual
                            deverá relatar o suporte necessário dentro do horário comercial a ser enviado
                            em nosso grupo de suporte no WhatsApp. O suporte ocorrerá&nbsp;
                            <strong>SEMPRE REMOTAMENTE</strong>, para manter a agilidade e compromisso com o <strong>LICENCIADO</strong>.O <strong>SUPORTE</strong> deverá ser
                            reportado somente a
                            <strong>LICENCIANTE</strong> ou pelos usuários <strong>ADMINISTRADORES</strong>
                            da empresa.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO SEGUNDO:</strong>
                        </p>
                        <p>
                            O treinamento
                            da equipe de vendas não compete a
                            <strong>LICENCIANTE</strong>, devendo ser dirimidas
                            pelo
                            <strong>LICENCIADO</strong> ou <strong>GERENTE</strong> da agência.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>TERCEIRO:</strong>
                        </p>
                        <p>
                            Horário de atendimento/suporte ONLINE:
                        </p>
                        <p>
                            <strong>SEGUNDA A
                                SEXTA
                            </strong>:
                            Das 09:00/12:00 e das 14:00/18:00
                        </p>
                        <p>
                            <strong>SÁBADOS/DOMINGOS/FERIADOS
                                -
                            </strong>Fechado
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEXTO:</strong>
                        </p>
                        <p>
                            A <strong>LICENCIANTE</strong> dará manutenção apenas no
                            que se refere ao
                            <strong>SISTEMA DE GESTÃO
                                SAGTUR,
                            </strong>descrito no objeto deste contrato ficando excluídos as manutenções,
                            suporte e assistência na configuração de equipamentos tais como roteadores,
                            access points, servidores de proxy, servidores de DNS, servidores web,
                            servidores FTP e outros softwares e hardwares estranhos ao sistema e não
                            comercializados pela SAGTUR.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 7ª - DAS OBRIGAÇÕES DALICENCIANTE</strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            Promover as devidas correções no que concerne às
                            falhas e/ou impropriedades do sistema, bem como atualizar o mesmo, por razão de
                            erro não detectado anteriormente.
                        </p>
                        <p>
                            Dar o presente suporte mensal ao <strong>LICENCIADO</strong>, no horário comercial da <strong>SAGTUR.</strong>
                        </p>
                        <p>
                            Não divulgar, transferir, fornecer ou ceder, a qualquer
                            título, quaisquer dados ou informações do
                            <strong>LICENCIADO</strong>
                            e de seus clientes, contidos no banco de dados e/ou obtidos por força do
                            presente instrumento.
                        </p>
                        <p>
                            Os dados inseridos no sistema pelo <strong>LICENCIADO</strong>
                            serãoarmazenados na nuvem e nos servidores do nosso fornecedor a <strong>SAVEINCLOUD</strong>,
                            garantindo uma proteção de dados seguro.
                        </p>
                        <p>
                            Caso o <strong>LICENCIADO</strong> solicite o cancelamento do
                            sistema e queira uma cópia do seu banco de dados, será enviado em uma planilha
                            do
                            <strong>EXCEL</strong> em até 48
                            horas.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 8ª - DAS OBRIGAÇÕES DO LICENCIADO</strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            Remunerar a&nbsp;<strong>LICENCIANTE
                                MENSALMENTE
                            </strong>, nos termos descritos neste contrato.
                        </p>
                        <p>
                            Utilizar o sistema contratado de acordo com suas
                            finalidades e exigências técnicas.
                        </p>
                        <p>
                            Disponibilizar o meio adequado para a utilização do sistema, tais
                            como: hardware, rede, pessoas capacitadas, entre outros.
                        </p>
                        <p>
                            Responsabilizar-se legalmente pelos dados e informações
                            armazenados(digitadas) no sistema.
                        </p>
                        <p>
                            Em hipótese alguma é permitido ao <strong>LICENCIADO</strong> ou
                            a terceiros, de forma geral, copiar, ceder, sublicenciar, vender, reproduzir,
                            doar, alugar, revender, sob quaisquer modalidades, gratuita ou onerosamente,
                            provisória ou permanentemente, o SOFTWARE de objeto deste contrato.
                        </p>
                        <p>
                            <strong>Caso seja identificado que o sistema esteja sendo utilizado para APLICAR GOLPES em clientes, e a SAGTUR identificar
                                este crime, seu sistema será CANCELADO
                                imediatamente, e será aberto um PROCESSO
                                ADMINISTRATIVO CONTRA A AGÊNCIA.
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 9ª - DA PROPRIEDADE INTELECTUAL E
                                CONFIDENCIALIDADE
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>PRIMEIRO:</strong>
                        </p>
                        <p>
                            Todos
                            os direitos e propriedade intelectual do sistema
                            <strong>SAGTUR</strong>, objeto do
                            presente contrato, são e permanecerão exclusivos da&nbsp;
                            <strong>LICENCIANTE.&nbsp;</strong>Em nenhuma hipótese o<strong> LICENCIADO </strong>terá acesso ao código fonte do software ora licenciado.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>SEGUNDO:</strong>
                        </p>
                        <p>
                            Qualquer
                            prejuízo que o
                            <strong>LICENCIADO,</strong> vier a
                            experimentar, pelo uso inadequado ou cadastramento incorreto de dados que não
                            sejam válidos no banco de dados não será de responsabilidade da&nbsp;
                            <strong>LICENCIANTE.</strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>TERCEIRO:</strong>
                        </p>
                        <p>
                            A<strong>LICENCIANTE</strong> não estará obrigada a efetuar
                            qualquer ressarcimento financeiro que venha ser solicitado pelo
                            <strong>LICENCIADO, </strong>por lançamentos incorretos
                            inseridos no sistema.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO </strong><strong>QUARTO:</strong>
                        </p>
                        <p>
                            A<strong>LICENCIANTE</strong>, não terá qualquer
                            responsabilidade perante o
                            <strong>LICENCIADO</strong>
                            e/ou terceiros, no tocante a qualquer ação que resulte de:
                        </p>
                        <p>
                            a)Qualquer
                            violação pelo
                            <strong>LICENCIADO</strong> de suas
                            obrigações descritas neste contrato;
                        </p>
                        <p>
                            b)Mau uso do sistema,
                            caracterizado pelo uso em desacordo com as especificações técnicas aplicáveis;
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 10ª - DO VÍNCULO ENTRE AS
                                PARTES
                            </strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            A presente
                            avença não poderá, sob nenhum aspecto, ser interpretada como uma associação ou
                            um ato de sociedade entre as partes, para todo e qualquer fim de direito.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 11ª – DO FORO</strong>
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            As partes
                            pactuam do foro da Comarca de Palhoça/SC como competente para dirimir conflitos
                            entre
                            <strong>LICENCIANTE</strong> E <strong>LICENCIADO</strong>.
                        </p>
                        <p>
                            <strong><br>
                            </strong>
                        </p>
                        <p>
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 12ª – ASSINATURA DO CONRATO</strong>
                        </p>
                        <p>
                            A afirmação
                            deste contrato será assinada digitalmente através do site
                            <a href="https://tecnospeed.com.br/%20">https://tecnospeed.com.br/</a> onde será
                            enviado para o e-mail de ambas as partes.
                        </p>
                    </div>

                </div>
            </div>
            <div id="seus_dados" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('my_data_info'); ?></h2>
                        <div class="box-icon"></div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <a class="spwHtmlIcoInfo">&nbsp;</a>
                                        <span class="spwTextoRef">AVISO!</span>
                                        <p class="spwTextoRefMensagem">
                                            Se houver inconsistência com seus dados apresentados aqui, entre em contato conosco para ajustar o cadastro.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"><?= lang('info_data') ?></div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo lang('company', 'company'); ?>
                                                    <div class="controls">
                                                        <?php echo form_input('company', $customer->name, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?= lang("vat_no", "vat_no"); ?>
                                                    <?php echo form_input('vat_no', $customer->vat_no, 'class="form-control" disabled'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo lang('responsible', 'responsible'); ?>
                                                    <div class="controls">
                                                        <?php echo form_input('name', $customer->company, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <?php echo lang('phone', 'phone'); ?>
                                                    <div class="controls">
                                                        <input type="tel" class="form-control" disabled value="<?= $customer->phone ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo lang('email', 'email'); ?>
                                                    <input type="email" class="form-control" disabled value="<?= $customer->email ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"><?= lang('info_address') ?></div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang("address", "address"); ?>
                                                        <?php echo form_input('address', $customer->address, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("city", "city"); ?>
                                                        <?php echo form_input('city', $customer->city, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("state", "state"); ?>
                                                        <?php echo form_input('state', $customer->state, 'class="form-control" disabled'); ?>
                                                    </div>

                                                    <div class="form-group">
                                                        <?= lang("postal_code", "postal_code"); ?>
                                                        <?php echo form_input('postal_code',  $customer->postal_code, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("country", "country"); ?>
                                                        <?php echo form_input('country', $customer->country, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	

