<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tour'] 				= "Passeio";
$lang['tours'] 				= "Passeios";
$lang['tour_status']        = "Status";
$lang['pick_up']            = "Origem";
$lang['consult_tours']      = "Consultar Reservas de Passeios e Serviços";
$lang['dateFrom']           = "Data da Atividade";
$lang['dateUp']             = "Até";
$lang['date_exit']          = "Saída";
$lang['local_embarque']     = "Origem";
$lang['status_sale']        = "Status da Venda";
$lang['status_payment']     = 'Situação do pagamento';
$lang['data_sale_from']     = "Data do Lançamento da Venda";
$lang['data_venda_ate']     = "Até";
$lang['customize_report']   = "Por favor, personalizar o relatório abaixo com filtros clicando na lupa";
$lang['edit_sale']          = "Editar Venda";
$lang['cancelar_sale']      = "Cancelar Venda";
$lang['email_sale']         = "Enviar E-mail da Venda";
$lang['compartilhar_voucher_whatsapp']      = "Compartilhar Voucher Atualizado WhatsApp ";
$lang['compartilhar_dados_venda_whatsapp']  = "Compartilhar Venda WhatsApp ";
$lang['whatsapp_send'] = "Abrir Conversa com Cliente no WhatsApp";

$lang['add_tour'] = 'Adicionar Passeio';
$lang['consulta_disponibilidade'] = 'Consultas Disponibilidade';