<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_agendamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agenda/edit/" . $agenda->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <input type="hidden" value="<?php echo $agenda->id; ?>" name="id"/>
            <input type="hidden" value="<?php echo $agenda->produto; ?>" name="produto" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("data_do_dia", "doDia"); ?>
                        <?php echo form_input('doDia', $agenda->dataSaida, 'class="form-control tip" required="required" id="doDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_saida", "horaSaida"); ?>
                        <?php echo form_input('horaSaida', $agenda->horaSaida, 'class="form-control tip" id="horaSaida"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("data_ao_dia", "aoDia"); ?>
                        <?php echo form_input('aoDia', $agenda->dataRetorno, 'class="form-control tip" id="aoDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_retorno", "horaRetorno"); ?>
                        <?php echo form_input('horaRetorno', $agenda->horaRetorno, 'class="form-control tip" id="horaRetorno"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("vagas", "vagas"); ?>
                        <?php echo form_input('vagas', $agenda->vagas, 'class="form-control tip mask_integer" id="vagas" required="required"', 'number'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('active', 'active'); ?>
                        <?php
                        $wm = array('0' => lang('no'), '1' => lang('yes'));
                        echo form_dropdown('active', $wm,$agenda->active, 'class="tip form-control" required="required" id="active" style="width:100%;"');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editar_agendamento', lang('editar_agendamento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
