<?php defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use CodePhix\Asaas\Asaas;
use CodePhix\Asaas\Cobranca;

class Teste2 extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index() {}

    public function cobrefacil() {
    }


    public function buscar_cliente() {
        $app_id = '63a283c0a8cd8';
        $secret_toke = 'f275a1fe0035213be312bea35bfbece43b84a7b6';

        $cobrefacil = new Cobrefacil();
        $cobrefacil->app_id = $app_id;
        $cobrefacil->app_secret = $secret_toke;
        $cobrefacil->sandbox = true;

        $clienteEncontrado =  $cobrefacil->search_customer_by_cpf('06624498943');

        return $clienteEncontrado;
    }

    public function emitir_carne() {
        $app_id = '63a283c0a8cd8';
        $secret_toke = 'f275a1fe0035213be312bea35bfbece43b84a7b6';

        $clienteEncontrado = $this->buscar_cliente();

        $cobrefacil = new Cobrefacil();
        $cobrefacil->app_id = $app_id;
        $cobrefacil->app_secret = $secret_toke;
        $cobrefacil->sandbox = true;

        $cobranca_carne = array(
            'customer_id' => $clienteEncontrado->data[0]->id,
            'customer_address_id' => $clienteEncontrado->data[0]->address->id,
            'reference' => '123',
            'first_due_date' => '2022-12-26',

            'notification_rule_id' => '52XD4W3GE1DZ97JM1R0K',

            'number_installments' => 2,
            //'price_total' => 1000.00,
            //'price_installments' => 1000.00,
            'mode_installment' => 'installment_value',

            'items' => [
                array(
                    'description' => 'Sistema de gestão do turismo',
                    'quantity' => 1,
                    'price' => 1000.00
                )
            ],
            'settings' => array(
                'late_fee' => array(
                    'mode' => 'percentage',
                    'amount' => 2,
                ),
                'interest' => array(
                    'mode' => 'daily_percentage',
                    'amount' => 0.1,
                ),
                'discount' => array(
                    'mode' => 'fixed',
                    'amount' => 9.99,
                    'limit_date' => 5,
                ),
                'warning' => array(
                    'description' => ' Em caso de dúvidas entre em contato com nossa Central de Atendimento.'
                ),
            ),
        );

        $pix_gerado =  $cobrefacil->create_booklets($cobranca_carne);

        print_r($pix_gerado);
    }


    public function subscriptions() {

        $app_id = '63a283c0a8cd8';
        $secret_toke = 'f275a1fe0035213be312bea35bfbece43b84a7b6';

        $cobrefacil = new Cobrefacil();
        $cobrefacil->app_id = $app_id;
        $cobrefacil->app_secret = $secret_toke;
        $cobrefacil->sandbox = true;

        $subscription_data = array(
            'customer_id' => '8J53ROD2JPV82PQ47WG0',
            'notification_rule_id' => '52XD4W3GE1DZ97JM1R0K',
            'contract_number' => 'Numero do contrato',
            'payable_with' => 'bankslip,credit,pix',
            'plans_id' => 'NZM916730P6QJLO8KG52',
            'first_due_date' => '2022-12-29',
            'reference' => 'none',
            'generate_days' => 1,//Número de dias antes do vencimento que será gerado a cobrança
            'interval_type' => 'month',
            'interval_size' => 1,
            'items' => [
                array(
                    'products_services_id' => 'G1X7O835EW38EJRD4LQV',
                    'quantity' => 1,
                    'price' => 150
                )
            ],
            'settings' => array(
                'late_fee' => array(
                    'mode' => 'percentage',
                    'amount' => 1,
                ),
                'interest' => array(
                    'mode' => 'daily_percentage',
                    'amount' => 2,
                ),
                /*
                'discount' => array(
                    'mode' => 'fixed',
                    'amount' => 9.99,
                    'limit_date' => 5,
                ),
                */
                'warning' => array(
                    'description' => 'informações'
                ),
            ),
        );


        $subscriptions_created = $cobrefacil->create_subscriptions($subscription_data);

        print_r($subscriptions_created);

        $this->generate_invoice_subscription($subscriptions_created->data->id);
    }

    public function generate_invoice_subscription($subscription_id) {
        $app_id = '63a283c0a8cd8';
        $secret_toke = 'f275a1fe0035213be312bea35bfbece43b84a7b6';

        $cobrefacil = new Cobrefacil();
        $cobrefacil->app_id = $app_id;
        $cobrefacil->app_secret = $secret_toke;
        $cobrefacil->sandbox = true;

        $invoice_created = $cobrefacil->generate_invoice_subscription($subscription_id);

        print_r($invoice_created);
    }

    function create_cliente()
    {

        $asaas = new Asaas('$aact_YTU5YTE0M2M2N2I4MTliNzk0YTI5N2U5MzdjNWZmNDQ6OjAwMDAwMDAwMDAwMDAyNDYyNTE6OiRhYWNoX2Q2M2MyMjg1LTFjMjUtNGQ4Ni04MGRjLTYwNzI3YzQ1ZGY1OQ==');

        $clientes = $asaas->Cliente();
        $option = 'limit=1&cpfCnpj=52425855017';
        $clienteConsult = $clientes->http->get('/customers', $option);

        // Insere um novo cliente
        $dadosCliente = array(
            "name" => "André Velho",
            "email" => "resultatec@gmail.com",
            "phone" => "48998235746",
            "mobilePhone" => "48998235746",
            "cpfCnpj" => "52425855017",
            "postalCode" => "88135-432",
            //"address" => "Av. Paulista",
            //"addressNumber" => "150",
            //"complement" => "Sala 201",
            //"province" => "Centro",
            "externalReference" => "221",
            "notificationDisabled" => false,
            //"additionalEmails" => "",
            "municipalInscription" => "46683695908",
            "stateInscription"=> "646681195275",
            "observations" => "ótimo pagador, nenhum problema até o momento"
        );

        $clientes = $asaas->Cliente()->create($dadosCliente);
        print_r($clientes);
    }
    public function emitirCobranca($customer_id) {

        $asaas = new Asaas('$aact_YTU5YTE0M2M2N2I4MTliNzk0YTI5N2U5MzdjNWZmNDQ6OjAwMDAwMDAwMDAwMDAyNDYyNTE6OiRhYWNoX2Q2M2MyMjg1LTFjMjUtNGQ4Ni04MGRjLTYwNzI3YzQ1ZGY1OQ==');

        $cobranca = array(
            'customer'             => $customer_id,
            'billingType'          => 'BOLETO',
            'value'                => 100.50,
            'dueDate'              => '2023-12-10',
            'description'          => 'Pagamento de um novo item',
            'externalReference'    => '123',
            'installmentCount'     => 1,
            'installmentValue'     => 100.50,
            'discount'             => '',
            'interest'             => 0,
            'fine'                 => 0,
        );
        $cobranca = $asaas->Cobranca()->create($cobranca);
        print_r($cobranca);
    }

    public function list_customer() {
        $asaas = new Asaas('$aact_YTU5YTE0M2M2N2I4MTliNzk0YTI5N2U5MzdjNWZmNDQ6OjAwMDAwMDAwMDAwMDAyNDYyNTE6OiRhYWNoXzM2MGExNjU5LWUwODEtNGM1OS1iOTg3LWUxYWFlNzE0ZTA3Mg==', 'homologacao');

        // Retorna a listagem de clientes
        $clientes = $asaas->Cliente()->getAll(array());

        print_r($clientes);
    }

    public function extrato() {
        $asaas = new Asaas('$aact_YTU5YTE0M2M2N2I4MTliNzk0YTI5N2U5MzdjNWZmNDQ6OjAwMDAwMDAwMDAwMDAwNDgzMTQ6OiRhYWNoX2U4ZmMxZjM2LTQ5YTgtNDc5Ny1iYjdkLTExMDU3Zjg3Yzk5ZQ==', 'homologacao');

        $extrato = $asaas->Extrato()->getAll(array());

        print_r($extrato);

    }

    public function post($url, $params = array())
    {
        $params = json_encode($params);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "access_token: ".$this->api_key
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if(empty($response)){
            $response = new stdClass();
            $response->error = [];
            $response->error[0] = new stdClass();
            $response->error[0]->description = 'Tivemos um problema ao processar a requisição.';
        }

        return $response;

    }
}