<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update480 extends CI_Migration {

    public function up() {
        $this->db->update('settings',  array('is_email_queue' => 1), array('setting_id' => 1));
        $this->db->update('settings',  array('version' => '4.8.0'), array('setting_id' => 1));
    }

    public function down() {}
}
