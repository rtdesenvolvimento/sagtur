<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParseContractDTO_model extends CI_Model
{

    const TIPO_RETORNO_PARSE_STRING = 'parse_string';
    const TIPO_RETORNO_ARRAY = 'array';

    const ARRAY = 1;
    const MODEL = 2;
    const OBJECT = 3;

    public $contractID;
    public $sale_id;
    public $type = ParseContractDTO_model::ARRAY;
    public $venda = array();
    public $products = array();
    public $appModel;

    public $usarContract = true;

    public $tipo = 'text';

    public $retorno = ParseContractDTO_model::TIPO_RETORNO_PARSE_STRING;

    public function __construct() {
        parent::__construct();
    }

}