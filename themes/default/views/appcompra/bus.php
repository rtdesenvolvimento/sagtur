<?php
$assentosOcupados = $this->products_model->getSaleByPoltronaByItemVendaForMarcacao($tipoTransporte->id, $programacaoId);
$cobrancasExtra = $this->BusRepository_model->getAllCobrancaExtraAssentoByID($cobrancaExtraConfig->configuracao_assento_extra_id);
?>

<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

<link href="<?= $assets ?>styles/bus/css/bus-map-min-v1.css" rel="stylesheet"/>
<link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">

<style>
    .change-floors-button {
        height: 18px;
        width: 18px;
        cursor: pointer;
    }

    .top-parent-container {
        margin: 0 auto;
        padding: 0px 0px 0px 0px;
        width: 70%;
    }

</style>

<?php if(!empty($assentos) && !empty($assentos2)) {?>
    <div class="from-group" style="text-align: center;margin-top: 14px;">
        <p style="color: #10004f;text-transform: uppercase;"><b>Alterne entre os andares:</b></p>
        <span style="margin-right: 50px;">
            <input class="change-floors-button" type="radio" onclick="habilitar(this);" name="floors-number" value="1" id="one-floor-radio" checked="checked">
            <label for="one-floor-radio">1º Andar</label>
        </span>
        <span>
            <input class="change-floors-button" type="radio" onclick="habilitar(this);" name="floors-number" value="2" id="two-floor-radio">
            <label for="two-floor-radio">2º Andar</label>
        </span>
    </div>
<?php } ?>

<div class="top-parent-container">
    <div class="busMapAppContainer">
        <div class="busMapApp">
            <div class="scaffold-bus-container bus-map">
                <div class="seat-map">
                    <div class="scaffold-bus">
                        <ul class="bus-seats superior-floor active">
                            <?php if(!empty($assentos)) {?>
                                <?php foreach ($assentos as $assento) {
                                    $ocupado = 'enabled';
                                    $nome_passageiro = '';
                                    $bloqueado_id = false;
                                    $venda = false;
                                    $assentoNumber = $assento->name;
                                    $valor_extra = 0;
                                    $style_extra = $ocupado;

                                    foreach ($assentosOcupados as $ocup) {
                                        if ($ocup->poltrona == $assento->name) {
                                            $ocupado        = 'occupied';
                                            $style_extra    = $ocupado;

                                            $nome_passageiro = $ocup->customer;
                                            $venda = $ocup->id;
                                            $assentoNumber = '';
                                        }
                                    }

                                    foreach ($bloqueados as $bloqueado) {
                                        if ($bloqueado->str_assento == $assento->name) {
                                            $ocupado        = 'occupied';
                                            $style_extra    = $ocupado;

                                            $nome_passageiro = $bloqueado->note;
                                            $bloqueado_id = $bloqueado->id;
                                            $assentoNumber = '';
                                        }
                                    }

                                    foreach ($cobrancasExtra as $extra) {
                                        if ($extra->assento == $assento->name && $extra->andar == $assento->andar) {
                                            $valor_extra = $extra->valor;

                                            if ($extra->cor && $valor_extra > 0 && $style_extra != 'occupied') {
                                                $style_extra = 'cobranca-extra-'.$extra->id; ?>
                                                <style>
                                                    .seat-cobranca-extra-<?=$extra->id?> {
                                                        background-color: <?=$extra->cor;?>;
                                                        border-radius: 2px 0 0 2px;
                                                        content: "";
                                                        display: block;
                                                        height: 100%;
                                                        width: 90%;
                                                        color: #ffffff;
                                                    }
                                                </style>
                                            <?php }
                                        }
                                    }

                                    ?>
                                    <li style="<?php if (!$assento->habilitado) echo 'display:none';?>"
                                        class="seat seat-<?=$style_extra;?> andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
                                        name_class="seat-<?=$style_extra;?>"
                                        onclick="selecionar_assento(this);"
                                        bloqueio="<?=$bloqueado_id;?>"
                                        nome_passageiro="<?=$nome_passageiro;?>"
                                        venda="<?=$venda;?>"
                                        andar="<?=$assento->andar;?>"
                                        habilitado="<?=$assento->habilitado;?>"
                                        valor_extra="<?=$valor_extra;?>"
                                        data-bs-toggle="modal"
                                        data-bs-target="#axisMapModal"
                                        data-seat="<?=$assento->assento;?>"
                                        data-status="<?=$ocupado;?>"
                                        data-position-order="<?=$assento->ordem;?>"
                                        data-position-order_name="<?=$assento->name;?>">
                                        <span class="seatNumberLabel" title="<?=$nome_passageiro?>"><?=$assentoNumber;?></span>
                                    </li>
                                <?php }?>
                            <?php } ?>

                            <?php if(!empty($assentos2)) {?>
                                <?php foreach ($assentos2 as $assento) {
                                    $ocupado = 'enabled';
                                    $nome_passageiro = '';
                                    $bloqueado_id = false;
                                    $venda = false;
                                    $assentoNumber = $assento->name;
                                    $style_extra = $ocupado;

                                    foreach ($assentosOcupados as $ocup) {
                                        if ($ocup->poltrona == $assento->name) {
                                            $ocupado        = 'occupied';
                                            $style_extra    = $ocupado;

                                            $nome_passageiro = $ocup->customer;
                                            $venda = $ocup->id;
                                            $assentoNumber = '';
                                        }
                                    }

                                    foreach ($bloqueados as $bloqueado) {
                                        if ($bloqueado->str_assento == $assento->name) {
                                            $ocupado        = 'occupied';
                                            $style_extra    = $ocupado;

                                            $nome_passageiro = $bloqueado->note;
                                            $bloqueado_id = $bloqueado->id;
                                            $assentoNumber = '';
                                        }
                                    }

                                    foreach ($cobrancasExtra as $extra) {
                                        if ($extra->assento == $assento->name && $extra->andar == $assento->andar) {
                                            $valor_extra = $extra->valor;

                                            if ($extra->cor && $valor_extra > 0 && $style_extra != 'occupied') {
                                                $style_extra = 'cobranca-extra-'.$extra->id; ?>
                                                <style>
                                                    .seat-cobranca-extra-<?=$extra->id?> {
                                                        background-color: <?=$extra->cor;?>;
                                                        border-radius: 2px 0 0 2px;
                                                        content: "";
                                                        display: block;
                                                        height: 100%;
                                                        width: 90%;
                                                        color: #ffffff;
                                                    }
                                                </style>
                                            <?php }
                                        }
                                    }

                                    ?>
                                    <li style="display:none"
                                        class="seat seat-<?=$style_extra;?> andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
                                        name_class="seat-<?=$style_extra;?>"
                                        onclick="selecionar_assento(this);"
                                        bloqueio="<?=$bloqueado_id;?>"
                                        nome_passageiro="<?=$nome_passageiro;?>"
                                        venda="<?=$venda;?>"
                                        andar="<?=$assento->andar;?>"
                                        habilitado="<?=$assento->habilitado;?>"
                                        valor_extra="<?=$valor_extra;?>"
                                        data-bs-toggle="modal"
                                        data-bs-target="#axisMapModal"
                                        data-seat="<?=$assento->assento;?>"
                                        data-status="<?=$ocupado;?>"
                                        data-position-order="<?=$assento->ordem;?>"
                                        data-position-order_name="<?=$assento->name;?>">
                                        <span class="seatNumberLabel" title="<?=$nome_passageiro?>"><?=$assentoNumber;?></span>
                                    </li>
                                <?php }?>
                            <?php } ?>
                        </ul>
                    </div>
                    <ul class="seats-subtitles">
                        <li>
                            <span class="seat-icon seat-enabled"></span>
                            <span class="subtitle">Livre</span>
                        </li>
                        <li>
                            <span class="seat-icon seat-reservado"></span>
                            <span class="subtitle">Selecionado</span>
                        </li>
                        <li>
                            <span class="seat-icon seat-occupied" style="background-image: url(https://gessicaalbuquerque.excapy.com/images/svg/legend-seat-busy.svg) "></span>
                            <span class="subtitle" style="">Ocupado</span>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var  site = <?=json_encode(array('base_url' => base_url(), 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;

    function selecionar_assento(tag) {

        let status = tag.getAttribute('data-status');
        let name_class = tag.getAttribute('name_class');

        if (status === 'enabled') {

            tag.classList.remove('seat-enabled');
            tag.classList.remove(name_class);

            tag.classList.add('seat-reservado');

            window.parent.selecionar_assento(tag);
        }
    }

    function habilitar(tag) {

        $('.andar-1').hide();
        $('.andar-2').hide();

        $('.andar-' + tag.value + '[habilitado="1"]').show();
    }
</script>