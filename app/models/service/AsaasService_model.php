<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use CodePhix\Asaas\Asaas;
use CodePhix\Asaas\Connection;
use CodePhix\Asaas\Cobranca;
use CodePhix\Asaas\NotaFiscal;

class AsaasService_model extends CI_Model
{
    const MENSAGEM_BAIXA_PAGMENTO = 'Pagamento baixado pelo sistema automaticamente pelo modulo de integração Asaas.<br/>Na data ';

    const  BOLETO = 'BOLETO';
    const  PIX = 'PIX';
    const LINK_PAGAMENTO = 'LINK_PAGAMENTO';
    const CARNE = 'CARNE';

    private $asaas;

    private $config_asaas;

    public function __construct() {
        parent::__construct();
    }
    public function configurarIntegracao() {
        $this->config_asaas = $this->settings_model->getAsaasSettings();

        if ($this->config_asaas->active) {
            if ($this->config_asaas->sandbox) {
                $this->asaas = new Asaas($this->config_asaas->token, 'homologacao');
                $this->atualizar_webhook($this->config_asaas->token, 'homologacao');
            } else {
                $this->asaas = new Asaas($this->config_asaas->token, 'producao');
                $this->atualizar_webhook($this->config_asaas->token, 'producao');
            }
        }
        return null;
    }

    public function habilitar_webhook() {

        $this->config_asaas = $this->settings_model->getAsaasSettings();

        if ($this->config_asaas->active) {

            if ($this->config_asaas->sandbox) {
                $connection = new Connection($this->config_asaas->token, 'homologacao');
            } else {
                $connection = new Connection($this->config_asaas->token, 'producao');
            }

            if ($this->session->userdata('cnpjempresa') == 'katatur') {
                $data_webhook = array(
                    'url' => 'https://sistema.katatur.com.br/infrastructure/AsaasController/notification?empresa='.$this->session->userdata('cnpjempresa'),
                    //'email' => $vendedor->email,
                    'email' => 'conexojava@gmail.com',
                    'apiVersion' => 3,
                    'interrupted' => false,
                    'enabled' => true,
                    'authToken' => '',
                );
            } else {
                $data_webhook = array(
                    'url' => 'https://sistema.sagtur.com.br/infrastructure/AsaasController/notification?empresa='.$this->session->userdata('cnpjempresa'),
                    //'email' => $vendedor->email,
                    'email' => 'conexojava@gmail.com',
                    'apiVersion' => 3,
                    'interrupted' => false,
                    'enabled' => true,
                    'authToken' => '',
                );
            }

            return $connection->post('/webhook',$data_webhook);
        }
        return null;
    }

    public function atualizar_webhook($token, $status = '') {
        $connection = new Connection($token, $status);

        if ($this->session->userdata('cnpjempresa') == 'katatur') {
            $data_webhook = array(
                'url' => 'https://sistema.katatur.com.br/infrastructure/AsaasController/notification?empresa='.$this->session->userdata('cnpjempresa'),
                //'email' => $vendedor->email,
                'email' => 'conexojava@gmail.com',
                'apiVersion' => 3,
                'interrupted' => false,
                'enabled' => true,
                'authToken' => '',
            );
        } else {
            $data_webhook = array(
                'url' => 'https://sistema.sagtur.com.br/infrastructure/AsaasController/notification?empresa='.$this->session->userdata('cnpjempresa'),
                //'email' => $vendedor->email,
                'email' => 'conexojava@gmail.com',
                'apiVersion' => 3,
                'interrupted' => false,
                'enabled' => true,
                'authToken' => '',
            );
        }

        $connection->post('/webhook',$data_webhook);
    }

    public function consultar_webhook_cobranca() {
        $this->config_asaas = $this->settings_model->getAsaasSettings();

        if ($this->config_asaas->active) {
            if ($this->config_asaas->sandbox) {
                $connection = new Connection($this->config_asaas->token, 'homologacao');
            } else {
                $connection = new Connection($this->config_asaas->token, 'producao');
            }

            return $connection->get('/webhook');
        }

        return false;
    }

    public function consultaSaldo() {
        $asaasSettings = $this->settings_model->getAsaasSettings();
        if ($asaasSettings->active) {
            if ($asaasSettings->sandbox) {
                $connection = new Connection($asaasSettings->token, 'homologacao');
            } else {
                $connection = new Connection($asaasSettings->token, 'producao');
            }
            return $connection->get('/finance/balance');
        }

        return null;
    }

    public function emitirCobranca($cobranca) {

        if ($this->config_asaas->active) {
            if ($this->isBoleto($cobranca->tipoCobranca)) {
                $boleto_gerado = $this->boleto($cobranca);
                return $boleto_gerado;
            } else if ($this->isPix($cobranca->tipoCobranca)) {
                $pix_gerado = $this->pix($cobranca);
                return $pix_gerado;
            } else if($this->isLinkPagamento($cobranca->tipoCobranca)) {
                $link_pagamento = $this->link_pagamento($cobranca);
                return $link_pagamento;
            } elseif ($this->isCarne($cobranca->tipoCobranca)) {
                return $this->carne($cobranca);
            }
        }

        return [];
    }

    public function emitirNotaFiscalAgendamento($nfa)
    {
        //$nfa = new NFAgendamento();

        $dados_nf = array(
            'payment' => $nfa->code,
            'observations' => $nfa->observations,
            'externalReference' => $nfa->externalReference,
            'value' => $nfa->valor_nf,
            'deductions' => $nfa->deductions,
            'effectiveDate' => $nfa->effectiveDate,
            'municipalServiceCode' => $nfa->municipalServiceCode,
            'municipalServiceName' => $nfa->municipalServiceName,
            'updatePayment' => $nfa->updatePayment,
            'taxes' => array(
                'retainIss' => false,
                'type' => 2.0100,
            ),
        );

        if ($nfa->status == NFAgendamento::GERADA) {

            $transaction = $this->asaas->NotaFiscal()->create($dados_nf);
            $invoice = $this->handle_errors($transaction);

            if ($invoice->sucesso) {

                $this->db->update('nf_agendamentos', array('status' => NFAgendamento::AGENDADA, 'nf_code' => $invoice->id), array('id' => $nfa->id));

                $transaction = $this->asaas->NotaFiscal()->issueInvoice($invoice->id);
                $authorized = $this->handle_errors($transaction);

                if ($authorized->sucesso) {
                    $this->db->update('nf_agendamentos', array('status' => NFAgendamento::EMITIDA), array('id' => $nfa->id));
                }

            } else {
                throw new Exception($invoice->erro);
            }
        } elseif ($nfa->status == NFAgendamento::AGENDADA) {

            $transaction = $this->asaas->NotaFiscal()->issueInvoice($nfa->nf_code);
            $authorized = $this->handle_errors($transaction);

            if ($authorized->sucesso) {
                $this->db->update('nf_agendamentos', array('status' => NFAgendamento::EMITIDA), array('id' => $nfa->id));
            } else {
                throw new Exception($authorized->erro);
            }

        } else  {
            throw new Exception('Nota fiscal não pode ser emitida no status '.$nfa->status);
        }

    }

    public function cancelarNotaFiscal($nfa)
    {
        $transaction = $this->asaas->NotaFiscal()->cancel($nfa->nf_code);
        $authorized = $this->handle_errors($transaction);

        if ($authorized->sucesso) {
            $this->db->update('nf_agendamentos', array('status' => NFAgendamento::CANCELADA), array('id' => $nfa->id));
        } else {
            throw new Exception($authorized->erro);
        }
    }

    public function getParcelamento($cod_installment)
    {
        $installments = $this->asaas->Parcelamento()->getInstallmentsById($cod_installment);

        return $installments;
    }

    public function getBeefPdf($cod_installment)
    {
        $paymentBook = $this->asaas->Parcelamento()->getBeefPdf($cod_installment);

        return $paymentBook;
    }

    public function confirmacao_pagamento($cod_payment, $payment_data)
    {
        $dtPagamento = date('Y-m-d', strtotime($payment_data['data_pagamento']));

        $payment = array(
            'paymentDate' => $dtPagamento,
            'value' => $payment_data['valor_pago'],
            'notifyCustomer' => true,
        );

        $this->asaas->Cobranca()->confirmacao($cod_payment, $payment);

        return true;
    }

    public function dezconfirmacao_pagamento($cod_payment)
    {
        $this->asaas->Cobranca()->dezconfirmacao($cod_payment, array());

        return true;
    }

    public function cancelarCobranca($code) {
        if ($this->config_asaas->active) {
            $this->asaas->Cobranca()->delete($code);
        }
    }

    private function boleto($cobranca) {
        return $this->create_invoice($cobranca, AsaasService_model::BOLETO);
    }
    private function pix($cobranca) {
        return $this->create_invoice($cobranca, AsaasService_model::PIX);
    }

    private function link_pagamento($cobranca) {
        return $this->create_invoice($cobranca, AsaasService_model::LINK_PAGAMENTO);
    }

    private function carne($cobranca)
    {
        return $this->create_invoice($cobranca, AsaasService_model::CARNE);
    }

    private function create_invoice($cobranca, $type) {

        $setting = $this->settings_model->getSettings();
        $customer = $this->get_customer($cobranca->pessoa);

        $invoice = array(
            'customer'             => $customer->id,
            'value'                => number_format ($cobranca->valorVencimento, 2, '.', '' ),
            'dueDate'              => implode("-",array_reverse(explode("/",$cobranca->dataVencimento))),
            'description'          => $cobranca->instrucoes,
            'externalReference'    => $cobranca->fatura,
            'interest'             => array(
                'value' => $setting->percentualJurosMoraDia,
            ),
            'fine'                 => array(
                'value' => $setting->percentualMulta,
            ),
            'discount' => array(
                'value' => 0,
                'dueDateLimitDays' => 0,
                'type' => 'FIXED',
            )
        );

        if ($this->isBoleto($cobranca->tipoCobranca)) {
            $invoice['billingType'] = 'BOLETO';
            $transaction = $this->asaas->Cobranca()->create($invoice);
        } elseif ($this->isPix($cobranca->tipoCobranca)) {
            $invoice['billingType'] = 'PIX';
            $transaction = $this->asaas->Cobranca()->create($invoice);
        } elseif ($this->isLinkPagamento($cobranca->tipoCobranca)) {
            $pessoaCobranca = $cobranca->pessoa;

            $invoice['name'] = substr($pessoaCobranca->nome, 0, 200);
            $invoice['billingType'] = 'CREDIT_CARD';
            $invoice['chargeType'] = 'INSTALLMENT';
            $invoice['maxInstallmentCount'] = 12;//numero maximo de parcelamento do cartao de credito
            $invoice['dueDateLimitDays'] = 2;//para boleto ate 2 disa para realizar o pagamento

            $transaction = $this->asaas->LinkPagamento()->create($invoice);
        } elseif ($this->isCarne($cobranca->tipoCobranca)) {

            $invoice['billingType']  = 'BOLETO';
            $invoice['totalValue'] = number_format ($cobranca->valorVencimento * $cobranca->numeroParcelas, 2, '.', '' );
            $invoice['installmentCount'] = $cobranca->numeroParcelas;

            $transaction = $this->asaas->Cobranca()->create($invoice);
        }

        return $this->handle_transaction($transaction, $cobranca);
    }

    private function handle_transaction($transaction, $cobranca) {
        $dtoRetornoCobranca = $this->handle_errors($transaction);

        $dtoRetornoCobranca->adicionarFatura($this->write_invoice($cobranca, $transaction, $transaction));

        return $dtoRetornoCobranca;
    }

    public function write_invoice($cobranca, $invoice, $transaction) {

        if ($invoice->dueDate == null) {
            $invoice->dueDate = implode("-",array_reverse(explode("/",$cobranca->dataVencimento)));
        }

        $data_invoice = array(
            'status' => Financeiro_model::STATUS_ABERTA,
            'fatura' => $cobranca->fatura,
            'tipo' => $cobranca->tipoCobranca,
            'integracao' => 'asaas',
            'status_detail' => 'created',
            'code' => $invoice->id,
            'dueDate' => $invoice->dueDate,
            'installmentLink' => $invoice->invoiceUrl,
            'cod_installment' => $invoice->installment,
            'log' => $this->sma->tirarAcentos(print_r($transaction, true)),
        );

        if ($this->isBoleto($cobranca->tipoCobranca) || $this->isPix($cobranca->tipoCobranca)) {
            $data_invoice['link'] = $invoice->invoiceUrl;
            $data_invoice['checkoutUrl'] = $invoice->invoiceUrl;
        } elseif ($this->isLinkPagamento($cobranca->tipoCobranca)) {
            $data_invoice['link'] = $invoice->url;
            $data_invoice['checkoutUrl'] = $invoice->url;
        }

        if ($this->isPix($cobranca->tipoCobranca)) {
            $pix = $this->asaas->Pix()->create($invoice->id);

            if ($pix->success) {
                $data_invoice['qr_code_base64']    = $pix->encodedImage;
                $data_invoice['qr_code']           = $pix->payload;
                $data_invoice['checkoutUrl']       = base_url().'pix/pagamento/'.$invoice->id.'?token='.$this->session->userdata('cnpjempresa');
            }
        }

        return $data_invoice;
    }

    private function handle_errors($transaction) {
        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();

        if (!empty($transaction->errors)) {
            foreach ($transaction->errors as $error) {
                $dtoRetornoCobranca->erro .= $error->description.'<br/>';
            }

            $dtoRetornoCobranca->sucesso = false;
            $dtoRetornoCobranca->data = [];
            return $dtoRetornoCobranca;
        }

        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        return $dtoRetornoCobranca;
    }

    private function get_customer($pessoaCobranca) {
        $customerFound = $this->customer_found($this->find_customer_byCnpjCpf($pessoaCobranca->cpfCnpj));

        if ($customerFound) {
            return $this->update_client($pessoaCobranca, $customerFound->id);
        } else {
            return $this->create_new_client($pessoaCobranca);
        }
    }

    private function update_client($pessoaCobranca, $cliente_id_asaas) {
        $data_client = array(
            "name" => $pessoaCobranca->nome,
            "email" => $pessoaCobranca->email,
            "phone" => $pessoaCobranca->telefone,
            "mobilePhone" => $pessoaCobranca->celular,
            "cpfCnpj" => $pessoaCobranca->cpfCnpj,
            "postalCode" => $pessoaCobranca->cep,
            "address" => $pessoaCobranca->endereco,
            "addressNumber" => $pessoaCobranca->numero,
            "complement" => $pessoaCobranca->complemento,
            "province" => $pessoaCobranca->bairro,
            "externalReference" => $pessoaCobranca->id,
            "notificationDisabled" => false,
            "additionalEmails" => "",
            "municipalInscription" => "",
            "stateInscription"=> "",
            "observations" => '',
            "groupName" => 'PASSAGEIRO'
        );

        $customer_update = $this->asaas->Cliente()->update($cliente_id_asaas, $data_client);

        if (empty($customer_update->error) && empty($customer_update->errors)) {
            if (!empty($customer_update->id)) {
                return $customer_update;
            }
        }

        $message_error = '';

        if (empty($customer_update->error)) {
            $message_error .= $customer_update->error.'<br/>';
        }

        if (!empty($customer_update->errors)) {
            foreach ($customer_update->errors as $error) {
                $message_error .= $error->description.'<br/>';
            }
        }

        throw new Exception('Não foi possível incluir o cliente.<br/>'.$message_error);
    }

    private  function create_new_client($pessoaCobranca) {

        $data_client = array(
            "name" => $pessoaCobranca->nome,
            "email" => $pessoaCobranca->email,
            "phone" => $pessoaCobranca->telefone,
            "mobilePhone" => $pessoaCobranca->celular,
            "cpfCnpj" => $pessoaCobranca->cpfCnpj,
            "postalCode" => $pessoaCobranca->cep,
            "address" => $pessoaCobranca->endereco,
            "addressNumber" => $pessoaCobranca->numero,
            "complement" => $pessoaCobranca->complemento,
            "province" => $pessoaCobranca->bairro,
            "externalReference" => $pessoaCobranca->id,
            "notificationDisabled" => false,
            "additionalEmails" => "",
            "municipalInscription" => "",
            "stateInscription"=> "",
            "observations" => '',
            "groupName" => 'PASSAGEIRO'
        );

        $customer_insert = $this->asaas->Cliente()->create($data_client);

        if (empty($customer_insert->error) && empty($customer_insert->errors)) {
            if (!empty($customer_insert->id)) {
                return $customer_insert;
            }
        }

        $message_error = '';

        if (!empty($customer_insert->error)) {
            foreach ($customer_insert->error as $error) {
                $message_error .= $error->description.'<br/>';
            }
        }

        if (!empty($customer_insert->errors)) {
            foreach ($customer_insert->errors as $error) {
                $message_error .= $error->description.'<br/>';
            }
        }

        throw new Exception('Não foi possível incluir o cliente.<br/>'.$message_error);

    }

    public function buscar_notificacoes($customer_id) {

        $notificacoes = $this->asaas->notificacao->getByCustomer($customer_id);

        if (!empty($notificacoes->data)) {
            foreach ($notificacoes->data as $notificacao) {
                if ($notificacao->event == 'PAYMENT_OVERDUE' && $notificacao->scheduleOffset > 0) {
                    return $notificacao->id;
                }
            }
        }

        return false;
    }
    public function atualizar_notificacao_apos_vencimento($customer_id) {

        $notificacao_id = $this->buscar_notificacoes($customer_id);

        if ($notificacao_id) {

            $notificacao_update = array(
                'enabled' => true,
                'emailEnabledForProvider' => false,
                'smsEnabledForProvider' => false,

                'emailEnabledForCustomer' => true,
                'smsEnabledForCustomer' => true,
                'phoneCallEnabledForCustomer' => false,
                'whatsappEnabledForCustomer' => false,
                'scheduleOffset' => 3,// a cada 3 dias do vencimento

            );
            $resultado = $this->asaas->notificacao->update($notificacao_id, $notificacao_update);
        }
    }

    private function find_customer_byCnpjCpf($cpfCnpj) {
        return $this->asaas->Cliente()->getAll(array('cpfCnpj' => $cpfCnpj));
    }

    public function customer_found($customer) {
        if (empty($customer->error) && empty($customer->errors)) {
            if (!empty($customer->data)) {
                return $customer->data[0];
            }
        }
        return false;
    }

    public function buscarPagamento($code) {

        $this->configurarIntegracao();

        $invoice = $this->asaas->Cobranca()->getById($code);

        if (!$this->handle_errors_payment($invoice)) return [];

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = $invoice->id;
        $fatura->reference = $invoice->externalReference;
        $fatura->valorVencimento = $invoice->value;
        $fatura->dtVencimento = $invoice->paymentDate;
        $fatura->log = $this->sma->tirarAcentos(print_r($invoice, true));
        $baixaCobranca->adicionarFatura($fatura);

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();

        $fees = number_format (($invoice->value - $invoice->netValue), 2, '.', '' );

        $pagamento->id = $invoice->id;
        //$pagamento->status = 'RECEIVED';
        $pagamento->status = $invoice->status;
        $pagamento->dataPagamento = $invoice->paymentDate;
        $pagamento->valorPago = $invoice->value;
        $pagamento->formaPagamento = $invoice->payment_method;
        $pagamento->taxa = $invoice->netValue != null ? $fees :  0;
        $pagamento->observacao = AsaasService_model::MENSAGEM_BAIXA_PAGMENTO . ' ' . date('d/m/Y H:i:s') . '<br/>Foi descontado uma taxa de R$'. $fees .' da agência';
        $fatura->adicionarPagamento($pagamento);

        return $baixaCobranca;
    }

    private function handle_errors_payment($transaction) {

        if (empty($transaction->id)) {
            return false;
        }

        return true;
    }

    private function isBoleto($tipo) {
        if ($tipo == 'boleto') return true;
        return false;
    }

    private function isPix($tipo) {
        if ($tipo == 'pix') return true;
        return false;
    }

    private function isCarne($tipo) {
        if ($tipo == 'carne') return true;
        return false;
    }

    private function isLinkPagamento($tipo) {
        if ($tipo == 'link_pagamento') return true;
        return false;
    }
}