<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update351 extends CI_Migration {

    public function up() {

        $this->create_sale_events();
        $this->alter_table_fatura_cobranca();

        $this->db->update('settings',  array('version' => '3.5.1'), array('setting_id' => 1));
    }

    public function create_sale_events() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'timestamp', 'null' => FALSE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'user_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'user' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'event' => array('type' => 'LONGTEXT', 'default' => '', 'null' => FALSE),
            'status' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'log' => array('type' => 'LONGTEXT', 'default' => '', 'null' => TRUE),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_events', TRUE, $attributes);
    }

    public function alter_table_fatura_cobranca() {
        $fields = array(
            'status_detail' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'log' => array('type' => 'LONGTEXT', 'default' => '', 'null' => TRUE),
        );
        $this->dbforge->add_column('fatura_cobranca', $fields);
    }

    public function down() {}
}
