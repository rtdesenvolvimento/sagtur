<style>
    .iframe_onibus {
        position: fixed;
        top: 95px;
        z-index: 9999;
        height: 810px;
        width: 46%;
        border: 1px solid #dbdee0;
        cursor: crosshair;
        transition: top 0.5s ease-out; /* Adiciona uma transição suave */
    }
</style>
<?php echo form_open_multipart("bus/adicionarPrecoAssentoExtra", array('data-toggle' => 'validator', 'role' => 'form')) ?>
<div class="row">
    <div class="col-lg-5">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?=lang('add_preco_assento_extra');?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?= lang("name", "name") ?>
                            <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($extra ? $extra->name : '')), 'class="form-control" id="name" required="required"'); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?= lang("tipo_transporte", "tipo_transporte") ?>
                            <?php
                            $cbTipoTransporte[''] = lang("select") . " " . lang("tipo_transporte");
                            foreach ($tiposTransporte as $tipoTransporte) {
                                $cbTipoTransporte[$tipoTransporte->id] = $tipoTransporte->name;
                            }
                            echo form_dropdown('tipo_transporte', $cbTipoTransporte, (isset($_POST['tipo_transporte']) ? $_POST['tipo_transporte'] : ''), 'class="form-control select" id="tipo_transporte" required="required" placeholder="' . lang("select") . " " . lang("tipo_transporte") . '"style="width:100%"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table id="tbAssentos" style="cursor: pointer;" class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center">Assento</th>
                                <th style="text-align: right;">Valor Extra</th>
                                <th style="text-align: center">Cor</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('add_assento_extra', $this->lang->line("add_assento_extra"), 'id="add_assento_extra" class="btn btn-primary"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="row">
            <div class="col-lg-12">
                <iframe frameborder="0" scrolling="no"  id="iframe_onibus" class="iframe_onibus"></iframe>
            </div>
        </div>
    </div>
</div>
<?= form_close(); ?>

<script>

    var j_assento_marcado_valor_extra = [];
    var tipo_transporte_selecionado = false;

    $(document).ready(function(){

        $('#tipo_transporte').change(function (event){
            if (!tipo_transporte_selecionado) {
                carregar_assentos();
                tipo_transporte_selecionado = true;
            } else {
                if (confirm('Deseja realmente alterar o Tipo de Transporte? COM ISSO VOCÊ VAI PERDER TODOS OS VALORES JÁ INSERIDOS!')) {
                    carregar_assentos();
                }
            }
        });

        $('.mask_money').change(function (event){

            let assento_id = $(this).attr('assento_id');
            let assento = $(this).attr('assento');
            let cor = $('#cor'+assento_id+' option:selected').attr('cor');

            let valor = $(this).val();

            marcar_assento(assento, valor, cor);
        });

        $('select').change(function (event) {

            let assento_id = $(this).attr('assento_id');
            let assento = $(this).attr('assento');
            let cor = $('#cor'+assento_id+' option:selected').attr('cor');

            let valor = $('#assento_'+assento_id).val();

            marcar_assento(assento, valor, cor);
        })

        $('#iframe_onibus').attr('src', site.base_url + "bus/bus_assento_cobranca_extra/" +  $('#tipo_transporte').val() +'/<?=$extra->id;?>');

        setTimeout(function (){$('select, .select').select2('destroy')}, 1000);
    });

    function carregar_onibus() {
        let = tipoTransporte = $('#tipo_transporte').val();

        $('#iframe_onibus').attr('src', site.base_url + "bus/bus_assento_cobranca_extra/" + tipoTransporte);
    }

    function carregar_assentos() {
        $.ajax({
            type: "GET",
            url: '<?php echo base_url();?>bus/carregar_assentos/',
            data: {
                tipo_transporte_id : $('#tipo_transporte').val(),
            },
            dataType: 'html',
            success: function (data_table) {
                if (data_table != null) {
                    $('#tbAssentos tbody').empty();
                    $('#tbAssentos tbody').append(data_table);

                    $(function(){
                        $('.mask_money').bind('keypress',mask.money);
                        $('.mask_money').click(function(){$(this).select();});

                        $('.mask_money').change(function (event){

                            let assento = $(this).attr('assento');
                            let valor = $(this).val();

                            marcar_assento(assento, valor);
                        });

                        $('select').change(function (event) {

                            let assento_id = $(this).attr('assento_id');
                            let assento = $(this).attr('assento');
                            let cor = $('#cor'+assento_id+' option:selected').attr('cor');
                            let valor = $('#assento_'+assento_id).val();

                            marcar_assento(assento, valor, cor);
                        });
                    });

                    carregar_onibus();
                }
            }
        });
    }

    function marcar_assento(assento, valor, cor) {

        var novo_assento_selecionado = {
            assento: assento,
            valor: valor,
            cor: cor,
        };

        var assento_marcado = j_assento_marcado_valor_extra.find(function(j_assento) {
            return j_assento.assento === assento;
        });

        if (!assento_marcado) {
            j_assento_marcado_valor_extra.push(novo_assento_selecionado);
        } else {
            assento_marcado.assento = assento;
            assento_marcado.valor = valor;
            assento_marcado.cor = cor;
        }

        if (valor > 0) {
            marcar_cor_assento(assento, cor);
        } else {
            desmarcar_cor_assento(assento);
        }
    }

    function marcar_cor_assento(assento, cor) {

        let iframe = $('#iframe_onibus');
        let iframeDocument = iframe.contents();
        let elementoLi = 'ul.bus-seats li[data-position-order_name="' + assento + '"]';
        let elementoDentroDoIframe = iframeDocument.find(elementoLi);

        elementoDentroDoIframe[0].classList.add('seat-reservado');
        elementoDentroDoIframe[0].classList.remove('seat-enabled');

        if (cor !== undefined && cor !== '') {
            elementoDentroDoIframe[0].style.border = "1px solid "+cor;
            elementoDentroDoIframe[0].style.background = "linear-gradient(to bottom, " + cor + " 1%, #ffc107 100%)";
        } else {
            elementoDentroDoIframe[0].style.border = 'none';
            elementoDentroDoIframe[0].style.background  = '#ffc107';
            elementoDentroDoIframe[0].style.border = '4px 4px 4px 4px';
        }
    }

    function desmarcar_cor_assento(assento) {
        let iframe = $('#iframe_onibus');
        let iframeDocument = iframe.contents();
        let elementoLi = 'ul.bus-seats li[data-position-order_name="' + assento + '"]';
        let elementoDentroDoIframe = iframeDocument.find(elementoLi);

        elementoDentroDoIframe[0].classList.remove('seat-reservado');
        elementoDentroDoIframe[0].classList.add('seat-enabled');

        elementoDentroDoIframe[0].style.border = 'none';

        elementoDentroDoIframe[0].style.background  = '#659f07';
        elementoDentroDoIframe[0].style.border = '4px 4px 4px 4px';
    }

</script>