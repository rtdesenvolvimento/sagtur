<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Infinitepay
{

    const VERSION = "v2";

    private $base_url            = 'https://api.infinitepay.io';
    private $base_url_sandbox    = 'https://api-staging.infinitepay.io';

    public $sandbox = true;
    public $client_id;
    public $client_secret;

    private $token;

    public function __construct() {}

    public function authenticate() {

        if ($this->token != null) return;

        $bodyAuth = array(
            "grant_type"    => "client_credentials",
            "client_id"     =>  $this->client_id,
            "client_secret" =>  $this->client_secret,
            "scope"         => 'transactions'
        );

        $response = $this->post('/oauth/token', $bodyAuth);

        if (empty($response->error))  {
            $this->token = $response->access_token;
        }
    }

    private function post($url, $params)
    {
        $params = json_encode($params);
        $ch = curl_init();

        $url_api = ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'. Valepay::VERSION . $url;

        curl_setopt($ch, CURLOPT_URL, $url_api);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        if ($this->token) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->token,
            ));
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "public:". $this->key_public,
                "private:". $this->key_private,
            ));
        }

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->status = 'error';
            $response->message = 'Tivemos um problema ao processar a requisição.';
        }
        return $response;
    }

}