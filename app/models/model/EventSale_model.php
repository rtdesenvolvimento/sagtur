<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EventSale_model extends CI_Model
{

    public $date;
    public $sale_id;
    public $user_id;
    public $user;
    public $event;
    public $status;
    public $log;

    public $sessions_id;
    public $ip_address;
    public $page_web;
    public $http_method;
    public $http_response_code;

    public $browser_information_name;
    public $browser_information_version;

    public $operating_system_information_name;

    public $user_agent_info;

    public $information_cookies;

    public $information_session;

    public $server_port;

    public $server_name;

    public $server_signature;
    public $server_software;

    public $http_referer;

    public $request_time;

    public $termos_aceite;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }



    /**
     * @return mixed
     */
    public function getSaleId()
    {
        return $this->sale_id;
    }

    /**
     * @param mixed $sale_id
     */
    public function setSaleId($sale_id): void
    {
        $this->sale_id = $sale_id;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event): void
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log): void
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSessionsId()
    {
        return $this->sessions_id;
    }

    /**
     * @param mixed $sessions_id
     */
    public function setSessionsId($sessions_id): void
    {
        $this->sessions_id = $sessions_id;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @param mixed $ip_address
     */
    public function setIpAddress($ip_address): void
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @return mixed
     */
    public function getPageWeb()
    {
        return $this->page_web;
    }

    /**
     * @param mixed $page_web
     */
    public function setPageWeb($page_web): void
    {
        $this->page_web = $page_web;
    }

    /**
     * @return mixed
     */
    public function getHttpMethod()
    {
        return $this->http_method;
    }

    /**
     * @param mixed $http_method
     */
    public function setHttpMethod($http_method): void
    {
        $this->http_method = $http_method;
    }

    /**
     * @return mixed
     */
    public function getHttpResponseCode()
    {
        return $this->http_response_code;
    }

    /**
     * @param mixed $http_response_code
     */
    public function setHttpResponseCode($http_response_code): void
    {
        $this->http_response_code = $http_response_code;
    }

    /**
     * @return mixed
     */
    public function getBrowserInformationName()
    {
        return $this->browser_information_name;
    }

    /**
     * @param mixed $browser_information_name
     */
    public function setBrowserInformationName($browser_information_name): void
    {
        $this->browser_information_name = $browser_information_name;
    }

    /**
     * @return mixed
     */
    public function getOperatingSystemInformationName()
    {
        return $this->operating_system_information_name;
    }

    /**
     * @param mixed $operating_system_information_name
     */
    public function setOperatingSystemInformationName($operating_system_information_name): void
    {
        $this->operating_system_information_name = $operating_system_information_name;
    }

    /**
     * @return mixed
     */
    public function getBrowserInformationVersion()
    {
        return $this->browser_information_version;
    }

    /**
     * @param mixed $browser_information_version
     */
    public function setBrowserInformationVersion($browser_information_version): void
    {
        $this->browser_information_version = $browser_information_version;
    }

    /**
     * @return mixed
     */
    public function getUserAgentInfo()
    {
        return $this->user_agent_info;
    }

    /**
     * @param mixed $user_agent_info
     */
    public function setUserAgentInfo($user_agent_info): void
    {
        $this->user_agent_info = $user_agent_info;
    }

    /**
     * @return mixed
     */
    public function getInformationCookies()
    {
        return $this->information_cookies;
    }

    /**
     * @param mixed $information_cookies
     */
    public function setInformationCookies($information_cookies): void
    {
        $this->information_cookies = $information_cookies;
    }

    /**
     * @return mixed
     */
    public function getInformationSession()
    {
        return $this->information_session;
    }

    /**
     * @param mixed $information_session
     */
    public function setInformationSession($information_session): void
    {
        $this->information_session = $information_session;
    }

    /**
     * @return mixed
     */
    public function getServerPort()
    {
        return $this->server_port;
    }

    /**
     * @param mixed $server_port
     */
    public function setServerPort($server_port): void
    {
        $this->server_port = $server_port;
    }

    /**
     * @return mixed
     */
    public function getServerName()
    {
        return $this->server_name;
    }

    /**
     * @param mixed $server_name
     */
    public function setServerName($server_name): void
    {
        $this->server_name = $server_name;
    }

    /**
     * @return mixed
     */
    public function getServerSignature()
    {
        return $this->server_signature;
    }

    /**
     * @param mixed $server_signature
     */
    public function setServerSignature($server_signature): void
    {
        $this->server_signature = $server_signature;
    }

    /**
     * @return mixed
     */
    public function getServerSoftware()
    {
        return $this->server_software;
    }

    /**
     * @param mixed $server_software
     */
    public function setServerSoftware($server_software): void
    {
        $this->server_software = $server_software;
    }

    /**
     * @return mixed
     */
    public function getHttpReferer()
    {
        return $this->http_referer;
    }

    /**
     * @param mixed $http_referer
     */
    public function setHttpReferer($http_referer): void
    {
        $this->http_referer = $http_referer;
    }

    /**
     * @return mixed
     */
    public function getRequestTime()
    {
        return $this->request_time;
    }

    /**
     * @param mixed $request_time
     */
    public function setRequestTime($request_time): void
    {
        $this->request_time = $request_time;
    }

    /**
     * @return mixed
     */
    public function getTermosAceite()
    {
        return $this->termos_aceite;
    }

    /**
     * @param mixed $termos_aceite
     */
    public function setTermosAceite($termos_aceite): void
    {
        $this->termos_aceite = $termos_aceite;
    }

}
