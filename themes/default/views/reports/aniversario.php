<?php
$v = "&mesAniversario=" . $mesAniversario;
$v .= "&dia=" . $dia;
?>
<style type="text/css" media="screen">

    #CusData td:nth-child(1),td:nth-child(7), td:nth-child(8) {display: none;}
    #CusData td:nth-child(6), td:nth-child(7), td:nth-child(8) {text-align: right;}
    #CusData td:nth-child(9) {text-align: center;}

</style>

<script>
    $(document).ready(function () {
        var oTable = $('#CusData').dataTable({
            "aaSorting": [[0, "asc"], [1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= site_url('reports/getCustomersAnivserario?v=1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                null,
                null,
                null,
                {"mRender": decimalFormat, "bSearchable": false},
                {"mRender": currencyFormat, "bSearchable": false},
                {"mRender": currencyFormat, "bSearchable": false},
                {"mRender": currencyFormat, "bSearchable": false},
                {"mRender": fsd}

            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

                var purchases = 0, total = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    purchases += parseFloat(aaData[aiDisplay[i]][4]);
                    total += parseFloat(aaData[aiDisplay[i]][5]);
                    paid += parseFloat(aaData[aiDisplay[i]][6]);
                    balance += parseFloat(aaData[aiDisplay[i]][7]);
                }

                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = decimalFormat(parseFloat(purchases));
                nCells[5].innerHTML = currencyFormat(parseFloat(total));
                nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                nCells[7].innerHTML = currencyFormat(parseFloat(balance));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
            {column_number: 8, filter_default_label: "[<?=lang('nascimento');?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-birthday-cake"></i><?= lang('lista_aniversario'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>"><i
                            class="icon fa fa-file-pdf-o"></i></a></li>
                <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i
                            class="icon fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <?php echo form_open("reports/aniversario/"); ?>
        <div class="box-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" id="div_status">
                        <?= lang("mesAniversario", "mesAniversario") ?>
                        <?php
                        $cbMes = array(
                            '' => lang('select'),
                            '1' => lang('janeiro'),
                            '2' => lang('fevereiro'),
                            '3' => lang('marco'),
                            '4' => lang('abril'),
                            '5' => lang('maio'),
                            '6' => lang('junho'),
                            '7' => lang('julho'),
                            '8' => lang('agosto'),
                            '9' => lang('setembro'),
                            '10' => lang('outubro'),
                            '11' => lang('novembro'),
                            '12' => lang('dezembro'),
                        );
                        echo form_dropdown('mesAniversario', $cbMes, (isset($_POST['mesAniversario']) ? $_POST['mesAniversario'] :  ''), 'class="form-control" id="mesAniversario"');
                        ?>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_sale_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang('dia', 'dia'); ?>
                        <?= form_input('dia', $dia, 'class="form-control tip" id="dia"'); ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="CusData" cellpadding="0" cellspacing="0" border="0"
                               class="table table-bordered table-condensed table-hover table-striped reports-table">
                            <thead>
                            <tr class="primary">
                                <th style="display: none;"><?= lang("company"); ?></th>
                                <th style="text-align: left;"><?= lang("name"); ?></th>
                                <th style="text-align: left;"><?= lang("phone"); ?></th>
                                <th style="text-align: left;"><?= lang("email_address"); ?></th>
                                <th><?= lang("total_sales"); ?></th>
                                <th style="text-align: center;"><?= lang("total_amount"); ?></th>
                                <th style="text-align: center;display: none;"><?= lang("paid"); ?></th>
                                <th style="text-align: center;display: none;"><?= lang("balance"); ?></th>
                                <th style="text-align: center;"><?= lang("nascimento"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="display: none;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-center"><?= lang("total_sales"); ?></th>
                                <th class="text-center" style="text-align: center;"><?= lang("total_amount"); ?></th>
                                <th class="text-center" style="display: none;"><?= lang("paid"); ?></th>
                                <th class="text-center;" style="display: none;"><?= lang("balance"); ?></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getCustomersAnivserario/pdf/0/'.$mesAniversario)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getCustomersAnivserario/0/xls/'.$mesAniversario)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
    });
</script>