<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function addPage($data)
    {
        if ($this->db->insert("page", $data)) {
            return true;
        }
        return false;
    }

    public function editPage($id, $data = array())
    {
        if ($this->db->update("page", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function adicionarMenu($data)
    {
        if ($this->db->insert("menu", $data)) {
            return true;
        }
        return false;
    }

    public function editarMenu($id, $data = array())
    {
        if ($this->db->update("menu", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deletarMenu($id)
    {
        if ($this->db->delete("menu", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarPage($id)
    {
        if ($this->db->delete("page", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function verificaIntegridadeMenu($id)
    {
        $this->db->where('menu_pai', $id);
        return $this->db->count_all_results('menu');
    }
}