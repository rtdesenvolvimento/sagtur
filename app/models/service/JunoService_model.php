<?php defined('BASEPATH') OR exit('No direct script access allowed');

class JunoService_model extends CI_Model
{
    const TOKEN_SAGTUR  = '';
    const TAXA_FIXA_BOLETO = 2.20;
    const ADIANTAR_PARCELAS_CARTAO_CREDITO = TRUE;
    const MENSAGEM_BAIXA_PAGMENTO = 'Pagamento baixado pelo sistema automaticamente pelo modulo de integração JUNO. na data ';

    const BOLETO            = 'BOLETO';
    const CARTAO_CREDITO =   'CREDIT_CARD';
    const CARTAO_E_BOLETO = 'BOLETO,CREDIT_CARD';

    public function __construct() {
        parent::__construct();

        $this->load->model('dto/CobrancaDTO_model', 'CobrancaDTO_model');
        $this->load->model('dto/PessoaCobrancaDTO_model', 'PessoaCobrancaDTO_model');
        $this->load->model('dto/RetornoCobrancaDTO_model','RetornoCobrancaDTO_model');
        $this->load->model('dto/BaixaCobrancaDTO_model', 'BaixaCobrancaDTO_model');
        $this->load->model('dto/BaixaFaturaCobrancaDTO_model', 'BaixaFaturaCobrancaDTO_model');
        $this->load->model('dto/BaixaPagamentoFaturaCobrancaDTO_model', 'BaixaPagamentoFaturaCobrancaDTO_model');

        $this->load->model('settings_model');

        $this->load->library('juno');
    }

    public function configurarIntegracao() {

        $junoSetting = $this->settings_model->getJunoSettings();

        if ($junoSetting->active) {

            if ($junoSetting->sandbox) {
                $token = $junoSetting->account_token_sandbox;
            } else {
                $token = $junoSetting->account_token;
            }

            if ($junoSetting->sandbox == '0') {
                $this->juno->sandbox = false;
            } else { 
                $this->juno->sandbox = true;
            }

            if ($this->session->userdata('cnpjempresa') == 'evoluirviagens') {
                $this->juno->paymentAdvance = FALSE;
            } else {
                $this->juno->paymentAdvance = self::ADIANTAR_PARCELAS_CARTAO_CREDITO;
            }

            $this->juno->token = $token;
        }
    }

    public function emitirCobranca($cobranca) {

        $pessoa = $cobranca->pessoa;

        $vlVencimento = $cobranca->valorVencimento;

        if ($cobranca->tipoCobranca == 'carne_cartao') { 
            $vlVencimento = $vlVencimento / $cobranca->numeroParcelas;
        }

        $this->juno->createCharge($pessoa->nome, $pessoa->cpfCnpj, $cobranca->descricaoCobranca, $vlVencimento, $cobranca->dataVencimento);

        $celularNotFormat =  $pessoa->celular;
        $celularNotFormat =  str_replace ( '-', '', $celularNotFormat);
        $celularNotFormat =  str_replace ( '(', '', $celularNotFormat);
        $celularNotFormat =  str_replace ( ')', '', $celularNotFormat);

        $this->juno->payerEmail = $pessoa->email;
        $this->juno->payerPhone = $celularNotFormat;
        $this->juno->reference = $cobranca->fatura;

        if ($pessoa->dataNascimento != null) {

            if ($cobranca->tipoCobranca == 'carne_cartao') {

                list($ano, $mes, $dia) = explode('-', $pessoa->dataNascimento);

                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                if ($idade < 18) {
                    throw new Exception("Não é possível gerar o link do cartão de crédito para menores de 18 anos.");
                }
            }

            $this->juno->payerBirthDate = $pessoa->data_aniversario;
        }

        $this->juno->notificationUrl = 'https://sistema.sagtur.com.br/infrastructure/JunoController/notification?empresa='.$this->session->userdata('cnpjempresa');

        $this->juno->billingAddressStreet = $pessoa->endereco;
        $this->juno->billingAddressNumber = $pessoa->numero;
        $this->juno->billingAddressComplement = $pessoa->complemento;
        $this->juno->billingAddressNeighborhood = $pessoa->bairro;
        $this->juno->billingAddressCity = $pessoa->cidade;
        $this->juno->billingAddressState = $pessoa->estado;
        $this->juno->billingAddressPostcode = $pessoa->cep;
        $this->juno->referralToken = self::TOKEN_SAGTUR;

        if ($this->session->userdata('cnpjempresa') == 'partiutrips') {
            $this->juno->fine = 0;
            $this->juno->interest = 0;
        }

        if ($cobranca->tipoCobranca == 'boleto') {
            $this->juno->installments = 1;
        } else {
            $this->juno->installments = $cobranca->numeroParcelas;
        }

        $this->setTipoCobranca($cobranca->tipoCobranca);

        $retorno =  $this->juno->issueCharge();

        return $this->tratarEmissaoCobrancaJuno($retorno, $cobranca);
    }

    private function setTipoCobranca($tipo) {
        if ($tipo == 'boleto' || $tipo == 'carne' || $tipo == 'carne_boleto') {
            $this->setFormaPagamento( JunoService_model::BOLETO);
        } elseif ($tipo == 'boleto_cartao' || $tipo == 'carne_boleto_cartao') {
            $this->setFormaPagamento( JunoService_model::CARTAO_E_BOLETO);
        } elseif ($tipo == 'cartao' || $tipo == 'carne_cartao') {
            $this->setFormaPagamento( JunoService_model::CARTAO_CREDITO);
        } elseif ($tipo == 'debito_online') {
            throw new Exception('Não há integração com pagamento online na JUNO.');
        }

        $this->juno->tipoCobranca = $tipo;
    }

    private function setFormaPagamento($formaPagamento) {
        $this->juno->paymentTypes = $formaPagamento;
    }

    private function tratarEmissaoCobrancaJuno($retorno, $cobranca) {

        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();

        $retorno = json_decode($retorno);

        if (!$retorno->success) {
            $dtoRetornoCobranca->sucesso = false;
            $dtoRetornoCobranca->erro = $retorno->errorMessage;
            $dtoRetornoCobranca->data = [];
            return $dtoRetornoCobranca;
        }

        foreach ($retorno->data->charges as $charge) {
            if (property_exists($charge, 'billetDetails')) {
                $dados = array(
                    'status' => Financeiro_model::STATUS_ABERTA,
                    'tipo' => $this->juno->tipoCobranca,
                    'fatura' => $cobranca->fatura,
                    'code' => $charge->code,
                    'dueDate' => $charge->dueDate,
                    'checkoutUrl' => $charge->checkoutUrl,
                    'link' => $charge->link,
                    'installmentLink' => $charge->installmentLink,
                    'payNumber' => $charge->payNumber,
                    'bankAccount' => $charge->billetDetails->bankAccount,
                    'ourNumber' => $charge->billetDetails->ourNumber,
                    'barcodeNumber' => $charge->billetDetails->barcodeNumber,
                    'portfolio' => $charge->billetDetails->portfolio,
                    'integracao' => 'juno',
                    'log' => $this->sma->tirarAcentos(print_r($retorno, true)),
                    'status_detail' => 'created',
                );
            } else {
                $dados = array(
                    'status' => Financeiro_model::STATUS_ABERTA,
                    'fatura' => $cobranca->fatura,
                    'tipo' => $this->juno->tipoCobranca,
                    'code' => $charge->code,
                    'dueDate' => $charge->dueDate,
                    'checkoutUrl' => $charge->checkoutUrl,
                    'link' => $charge->link,
                    'installmentLink' => $charge->installmentLink,
                    'payNumber' => $charge->payNumber,
                    'integracao' => 'juno',
                    'log' => $this->sma->tirarAcentos(print_r($retorno, true)),
                    'status_detail' => 'created',
                );
            }
            $dtoRetornoCobranca->adicionarFatura($dados);
        }

        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        return $dtoRetornoCobranca;
    }

    public function estornarCobranca($code) {/*Na juno nao e possivel estornar pagamentos ja confirmados*/}

    public function cancelarCobranca($code) {
        $retorno = json_decode($this->juno->cancelCharge($code));

        if (!$retorno->success) {
            //TODO FUTURAMENTE CRIAR UM LOG PARA REGISTRAR ESSSAS FALHAS MESMO QUE NAO JOGUE NA TELA DO CLIENTE!!!!!
           // throw new Exception($retorno->errorMessage);//TODO VAMOS DEIXAR COMENTADO POR ENQUANTO POIS TEM CLIENTE QUE RECLAMAN QUE NAO CONSEGUEM CANCELAR POIS CANCELAM ANTES NA JUNO OU A COBRANCA JA ESTA PAGA
        }
    }

    public function consultaSaldo() {
        return $this->juno->fetchBalance();
    }

    public function buscarPagamentoByCodeTransacao($code) {

        $retorno = $this->juno->fetchPaymentDetails($code);
        $datas = json_decode($retorno);

        if (!$datas->success) {
            return [];
        }

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $payment = $datas->data->payment;
        $charge = $datas->data->payment->charge;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = $charge->code;
        $fatura->reference = $charge->reference;
        $fatura->valorVencimento = $charge->amount;
        $fatura->dtVencimento = $charge->dueDate;
        $fatura->log = $this->sma->tirarAcentos(print_r($retorno, true));
        $baixaCobranca->adicionarFatura($fatura);

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $pagamento->id = $payment->id;
        $pagamento->status = $payment->status;
        $pagamento->dataPagamento = $payment->date;
        $pagamento->valorPago = $payment->amount;
        $pagamento->formaPagamento = $payment->type;
        $pagamento->taxa = $payment->fee;
        $pagamento->observacao = JunoService_model::MENSAGEM_BAIXA_PAGMENTO.' '.date('d/m/Y H:i:s');
        $fatura->adicionarPagamento($pagamento);

        return $baixaCobranca;
    }

    public function listarPagamentosNoPeriodo($beginPaymentDate) {

        $retorno = $this->juno->listCharges($beginPaymentDate);
        $datas = json_decode($retorno);

        $baixaCobranca = new BaixaCobrancaDTO_model();

        foreach ($datas as $data) {

            $charges = $data->charges;
            $baixaCobranca ->sucesso = true;

            foreach ($charges as $charge) {

                $fatura = new BaixaFaturaCobrancaDTO_model();
                $fatura->code = $charge->code;
                $fatura->reference = $charge->reference;
                $fatura->valorVencimento = $charge->amount;
                $fatura->dtVencimento = $charge->dueDate;
                $fatura->payNumber = $charge->payNumber;
                $baixaCobranca->adicionarFatura($fatura);

                $payments = $charge->payments;

                if ($payments == null & count($payments) == 0 & $charge->payNumber == 'BOLETO PAGO') {
                    $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
                    $pagamento->status = 'CONFIRMED';
                    $pagamento->dataPagamento = $charge->dueDate;
                    $pagamento->valorPago = $charge->amount;
                    $pagamento->formaPagamento = 'BOLETO';
                    $pagamento->taxa = self::TAXA_FIXA_BOLETO;
                    $pagamento->observacao = JunoService_model::MENSAGEM_BAIXA_PAGMENTO.' '.date('d/m/Y H:i:s');
                    $fatura->adicionarPagamento($pagamento);
                }

                foreach ($payments as $payment) {
                    $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
                    $pagamento->id = $payment->id;
                    $pagamento->status = $payment->status;
                    $pagamento->dataPagamento = $payment->date;
                    $pagamento->valorPago = $payment->amount;
                    $pagamento->formaPagamento = $payment->type;
                    $pagamento->taxa = $payment->fee;
                    $pagamento->observacao = JunoService_model::MENSAGEM_BAIXA_PAGMENTO.' '.date('d/m/Y H:i:s');
                    $fatura->adicionarPagamento($pagamento);
                }
            }
        }
        return $baixaCobranca;
    }
}
