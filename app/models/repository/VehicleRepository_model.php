<?php defined('BASEPATH') OR exit('No direct script access allowed');

class VehicleRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getByID($id)
    {
        $q = $this->db->get_where('veiculo', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function getPhotos($id) {
        $q = $this->db->get_where("veiculo_photos", array('veiculo_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    public function suggestions($term, $limit = 10)
    {
        $this->db->select("id,  name text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('veiculo', array(), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }
}