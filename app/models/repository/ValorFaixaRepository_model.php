<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ValorFaixaRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getById($id) {
        $this->db->limit(1);
        return $this->db->get_where('valor_faixa', array('id' => $id) )->row();
    }

    function getValoreFaixas() {

        $this->db->where('staff', 0);
        $this->db->where('active', 1);

        $this->db->order_by('name');

        $q = $this->db->get('valor_faixa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getStaff() {

        $this->db->where('staff', 1);

        $this->db->order_by('name');

        $q = $this->db->get('valor_faixa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}