<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RatingRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getByID($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('ratings');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getQuestionByID($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('rating_questions');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addQuestion($data)
    {
        if ($this->db->insert("rating_questions", $data)) {
            return true;
        }
        return false;
    }

    public function updateQuestion($id, $data = array())
    {
        if ($this->db->update("rating_questions", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    function getByuuid($uuid)
    {
        $this->db->limit(1);
        $this->db->where("uuid", $uuid);
        $q = $this->db->get('ratings');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAnswers($id) {

        $this->db->where("rating_id", $id);
        $q = $this->db->get('rating_responses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getQuestions() {
        $this->db->order_by('ordem');
        $q = $this->db->get('rating_questions');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getQuestionsActive() {
        $this->db->where("active", 1);
        $this->db->order_by('ordem');
        $q = $this->db->get('rating_questions');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function encontrouAvaliacao($sale_id, $customer_id)
    {
        $this->db->where("sale_id", $sale_id);
        $this->db->where("customer_id", $customer_id);

        $this->db->limit(1);
        $q = $this->db->get('ratings');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function naoEncontrouAvaliacao($sale_id, $customer_id)
    {
        return !$this->encontrouAvaliacao($sale_id, $customer_id);
    }

    public function getAllItensAvaliacao($avaliar_dias = TRUE, $sale_id = NULL) {

        $this->db->select('sale_items.id, sale_items.sale_id, sale_items.customerClient, sale_items.customerClientName, sale_items.programacaoId, sale_items.product_id');

        $this->db->join('products', 'products.id=sale_items.product_id');
        $this->db->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId');
        $this->db->join('sales', 'sma_sales.id=sale_items.sale_id');
        $this->db->join('companies', 'companies.id=sale_items.customerClient', 'left');

        $this->db->where('products.active', 1);//produto ativo
        $this->db->where('agenda_viagem.active', 1);// data ativa
        $this->db->where('sale_items.descontarVaga', 1);// somente quem desconta vaga no onibus
        $this->db->where("sales.payment_status in ('due','partial','paid') ");//vendas ativas

        if ($avaliar_dias) {
            // Calculando a data limite para avaliação
            $this->db->where('DATE_ADD(sma_agenda_viagem.dataRetorno, INTERVAL sma_products.avaliar_dias DAY) = CURDATE()');
        }

        $this->db->where('products.avaliar', 1);//solicitar avaliacao do pacote
        $this->db->where('agenda_viagem.avaliar', 1);//solicitar avaliacao da data

        if ($sale_id) {
            $this->db->where('sales.id', $sale_id);
        }

        //agrupa
        $this->db->group_by('sale_items.sale_id, companies.email');//agrup os itens de venda por e-mail para não enviar o mesmo e-mail varias vezes

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return FALSE;
    }

}
