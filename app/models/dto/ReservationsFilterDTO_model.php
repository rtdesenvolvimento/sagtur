<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReservationsFilterDTO_model extends CI_Model
{

    public $product_id;

    public $data_saida;
    public $data_retorno;

    public $hora_saida;
    public $hora_retorno;

    public $status_sale;
    public $status_payment;

    public $biller;
    public $customer;
    public $meio_divulgacao;
    public $local_embarque;
    public $tipo_transporte;
    public $guia;
    public $motorista;

    public $data_venda_de;
    public $data_venda_ate;

    public $created_by;

    public $idsVenda;


    public $group_by = 'sma_sales.id';

    public function __construct() {
        parent::__construct();
    }

}