<style>
    .select2-results__option {
        padding: 6px;
        user-select: none;
        -webkit-user-select: none;
        font-size: 0.9rem;
        text-transform: capitalize;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #9e9e9e;
        line-height: 28px;
    }
</style>
<!-- Booking Start -->
<div class="container-fluid booking mt-5 pb-0" id="buscar">
    <div class="container pb-5">
        <div class="bg-light shadow" style="padding: 30px;">
            <div class="row align-items-center" style="min-height: 60px;">
                <div class="col-md-10">
                    <div class="row">

                        <?php if ($this->Settings->filtar_local_embarque){ ?>
                            <div class="col-md-3">
                                <label>Embarques:</label>
                                <div class="mb-6 mb-md-0 labelInputContainer">
                                    <select class="custom-select px-4" id="embarque" onchange="buscarDestino();">
                                        <option value="" >De onde sai?</option>
                                        <?php foreach ($embarques as $embarque) {?>
                                            <?php if ($embarque->id == $embarqueFilter){?>
                                                <option value="<?php echo $embarque->id;?>" selected><?php echo $embarque->name?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $embarque->id;?>"><?php echo $embarque->name?></option>
                                            <?php } ?>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="<?=($this->Settings->filtar_local_embarque ? 'col-md-3' : 'col-md-4');?>">
                            <label>Mês:</label>
                            <div class="mb-6 mb-md-0 labelInputContainer">
                                <select class="custom-select px-4" id="mes" onchange="buscarDestino();">
                                    <option value="all" <?php if($mesFilter == 'all') echo ' selected="selected"';?>>Todos os Meses</option>
                                    <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>Janeiro</option>
                                    <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>Fevereiro</option>
                                    <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>Março</option>
                                    <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>Abril</option>
                                    <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>Maio</option>
                                    <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>Junho</option>
                                    <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>Julho</option>
                                    <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>Agosto</option>
                                    <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>Setembro</option>
                                    <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>Outubro</option>
                                    <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>Novembro</option>
                                    <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="<?=($this->Settings->filtar_local_embarque ? 'col-md-3' : 'col-md-4');?>">
                            <label>Destinos:</label>
                            <div class="mb-6 mb-md-0 labelInputContainer">
                                <select class="custom-select px-4" id="destino" onchange="buscarDestino();">
                                    <option value="" >Para onde vai?</option>
                                    <?php
                                    $programacoesFilterByName = array_column($programacoesFilter, 'name');
                                    array_multisort($programacoesFilterByName, SORT_ASC, $programacoesFilter);

                                    $produtoId = null;
                                    $name_product_filter = null;
                                    foreach ($programacoesFilter as $programacao) {
                                        if ($produtoId !== $programacao->produtoId){
                                            $produtoId = $programacao->produtoId; ?>
                                            <?php if ($destinoFilter == $programacao->produtoId){
                                                $name_product_filter = $programacao->name; ?>
                                                <option selected value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                            <?php } ?>
                                        <?php }?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="<?=($this->Settings->filtar_local_embarque ? 'col-md-3' : 'col-md-4');?>">
                            <label>Categorias:</label>
                            <div class="mb-6 mb-md-0 labelInputContainer">
                                <select class="custom-select px-4" id="category" onchange="buscarDestino();">
                                    <option value="">Tipo de experiência</option>
                                    <?php foreach ($categories as $categoria) {?>
                                        <?php if ($categoryFilter == $categoria->id){?>
                                            <option selected value="<?php echo $categoria->id;?>"><?php echo $categoria->name?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $categoria->id;?>"><?php echo $categoria->name?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-primary btn-block" type="button" onclick="buscarDestino()" style="height: 47px; margin-top: -2px;"> <i class="fa fa-search"></i> Buscar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    <?php if ($destinoFilter && $this->Settings->pixelFacebook) {?>
    try{
        // FACEBOOK TRACK EVENT
        fbq('track', 'Search', {
            search_string : '<?php echo $name_product_filter;?>',
            content_ids : [<?php echo $destinoFilter;?>], // top 5-10 search results
            content_type : 'product'
        });
    }
    catch(error){console.log(error)}
    <?php } ?>
</script>
<!-- Booking End -->