<?php defined('BASEPATH') OR exit('No direct script access allowed');

class WelcomeRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getTotalReceberMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpagar) as totalReceberMes");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("{$this->db->dbprefix('fatura')}.status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalRecebimentoMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpago) as totalRecebimentoMes");
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalPagamentoMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpago) as totalPagamentoMes");
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("tipooperacao", 'DEBITO');
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalPagarMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpagar) as totalPagarMes");
        $this->db->where("tipooperacao", 'DEBITO');
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getRelatorioVendasPorHora($startDate, $endDate)
    {
        $hoursArray = [];
        for ($i = 0; $i < 24; $i++) {
            $hoursArray[] = str_pad($i, 2, '0', STR_PAD_LEFT); // Garantindo que a hora seja formatada como "00", "01", ..., "23"
        }

        $myQuery = "SELECT 
            YEAR(date) AS year,
            MONTH(date) AS month,
            DAY(date) AS day,
            HOUR(date) AS sale_hour,
            SUM(grand_total) AS sales
        FROM " . $this->db->dbprefix('sales') . "
        WHERE date BETWEEN ? AND ? 
              AND sale_status IN ('faturada', 'orcamento', 'lista_espera')
        GROUP BY YEAR(date), MONTH(date), DAY(date), HOUR(date)
        ORDER BY YEAR(date), MONTH(date), DAY(date), HOUR(date) ASC";

        $q = $this->db->query($myQuery, array($startDate, $endDate));
        $salesData = $q->result();

        $hourlySales = [];

        foreach ($salesData as $sale) {
            $hourlySales[$sale->year][$sale->month][$sale->day][$sale->sale_hour] = (double) $sale->sales;
        }

        foreach ($hourlySales as $year => $months) {
            foreach ($months as $month => $days) {
                foreach ($days as $day => $hours) {
                    $orderedHours = [];
                    foreach ($hoursArray as $hour) {
                        $orderedHours[(int) $hour] = isset($hours[(int) $hour]) ? $hours[(int) $hour] : 0;
                    }
                    $hourlySales[$year][$month][$day] = $orderedHours;
                }
            }
        }

        $hours = [];
        foreach ($hoursArray as $hour) {
            $hours[] = $hour . 'h';
        }

        return ['hourlySales' => $hourlySales, 'hours' => $hours];
    }

    public function getRelatorioVendasPorDiaSemana($startDate, $endDate)
    {
        $daysOfWeek = [
            0 => 'Domingo',
            1 => 'Segunda-feira',
            2 => 'Terça-feira',
            3 => 'Quarta-feira',
            4 => 'Quinta-feira',
            5 => 'Sexta-feira',
            6 => 'Sábado'
        ];

        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        $end->modify('+1 day');

        $interval = new DateInterval('P1D');
        $period = new DatePeriod($start, $interval, $end);


        $finalLabels = [];
        $defaultSales = [];
        foreach ($period as $date) {
            $dateKey = $date->format('Y-m-d');
            $formattedDate = $date->format('Y/m/d');
            $weekdayNumber = (int)$date->format('w'); // 0 (Domingo) até 6 (Sábado)
            $dayName = $daysOfWeek[$weekdayNumber];
            $finalLabels[$dateKey] = $formattedDate . ' (' . $dayName . ')';
            $defaultSales[$dateKey] = 0;
        }

        $myQuery = "SELECT 
                    DATE(date) AS sale_date, 
                    SUM(grand_total) AS sales
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date BETWEEN ? AND ? 
                      AND sale_status IN ('faturada', 'orcamento', 'lista_espera')
                GROUP BY sale_date
                ORDER BY sale_date ASC";

        $q = $this->db->query($myQuery, array($startDate, $endDate));
        $salesData = $q->result();

        foreach ($salesData as $sale) {
            if (isset($defaultSales[$sale->sale_date])) {
                $defaultSales[$sale->sale_date] = (double)$sale->sales;
            }
        }

        // Preserva a ordem cronológica e retorna os dados para o gráfico
        $labels = array_values($finalLabels);
        $sales  = array_values($defaultSales);

        return ['weeklySales' => $sales, 'labels' => $labels];
    }

    public function getRelatorioVendasPorDespesasSemanal()
    {
        $myQuery = "SELECT S.week,
            COALESCE(S.Sales, 0) as sales,
            COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  YEARWEEK(date, 1) as week,
                SUM(grand_total) Sales
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL 12 MONTH ) AND
                sale_status in ('faturada', 'orcamento', 'lista_espera')
                GROUP BY YEARWEEK(date, 1)) S
        LEFT JOIN ( SELECT  YEARWEEK(dtvencimento, 1) as week,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                         ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                        GROUP BY YEARWEEK(dtvencimento, 1)
        ) FD
        
        ON S.week = FD.week
        GROUP BY S.week
        ORDER BY S.week";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getRelatorioVendasPorDespesas()
    {
        $myQuery = "SELECT S.month,
            COALESCE(S.Sales, 0) as sales,
            COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  date_format(date, '%Y-%m') Month,
                SUM(grand_total) Sales
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL 24 MONTH ) AND
                sale_status in ('faturada', 'orcamento', 'lista_espera')
                GROUP BY date_format(date, '%Y-%m')) S
        LEFT JOIN ( SELECT  date_format(dtvencimento, '%Y-%m') Month,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                         ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                        GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FD
        
        ON S.Month = FD.Month
        GROUP BY S.Month
        ORDER BY S.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBestSeller($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date)  $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $sp = "( SELECT 
            si.product_id, 
            SUM( si.quantity ) soldQty, 
            s.date as sdate
         from " . $this->db->dbprefix('sales') . " s JOIN " . $this->db->dbprefix('sale_items') . " si on s.id = si.sale_id 
         where s.date >= '{$start_date}' and s.date < '{$end_date}' and s.sale_status in ('faturada', 'orcamento', 'lista_espera') group by si.product_id ) PSales";

        $this->db
            ->select("CONCAT(" . $this->db->dbprefix('products') . ".name, ' (', " . $this->db->dbprefix('products') . ".code, ')') as name, COALESCE( PSales.soldQty, 0 ) as SoldQty", FALSE)
            ->from('products', FALSE)
            ->join($sp, 'products.id = PSales.product_id', 'left')
            ->order_by('PSales.soldQty desc')
            ->limit(10);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVendasPorVendedor($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("companies.name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('companies', 'companies.id = sales.biller_id', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('companies.name')
            ->order_by('subtotal', 'DESC')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVendasPorCategoria($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("nmCategoria as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('nmCategoria')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getVendasPorMeioDivulgacao($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("meio_divulgacao.name as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('meio_divulgacao', 'meio_divulgacao.id = sales.meioDivulgacao', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('meio_divulgacao.id')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getVendasPorTipoCobranca($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("tipo_cobranca.name as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = sales.tipoCobrancaId', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('tipo_cobranca.id')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}