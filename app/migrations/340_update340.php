<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update340 extends CI_Migration {

    public function up() {

        $this->update_sales_item();
        $this->update_valor_faixa_etaria();
        $this->update_faixa_etaria_tipo_hospedagem();
        $this->update_faixa_etaria_adicional();
        $this->update_products();

        $this->db->update('settings',  array('version' => '3.22.08.28'), array('setting_id' => 1));
    }

    function update_sales_item() {
        $sql_update = "UPDATE sma_sale_items SET faixaId = 1, faixaNome = 'Adulto', descontarVaga = 1 WHERE customerClient in (select id from sma_companies where tipoFaixaEtaria = 'adulto')";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_sale_items SET faixaId = 2, faixaNome = 'Crianca', descontarVaga = 1 WHERE customerClient in (select id from sma_companies where tipoFaixaEtaria = 'crianca')";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_sale_items SET faixaId = 3, faixaNome = 'Bebê de Colo', descontarVaga = 0 WHERE customerClient in (select id from sma_companies where tipoFaixaEtaria = 'bebe')";
        $query = $this->db->query($sql_update);
    }

    function update_valor_faixa_etaria() {
        $sql_update = "UPDATE sma_valor_faixa_etaria set faixaId = 1 where tipo = 'adulto'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_valor_faixa_etaria set faixaId = 2 where tipo = 'crianca'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_valor_faixa_etaria set faixaId = 3 where tipo = 'bebe'";
        $query = $this->db->query($sql_update);
    }

    function update_faixa_etaria_tipo_hospedagem() {
        $sql_update = "UPDATE sma_faixa_etaria_tipo_hospedagem set faixaId = 1 where tipo = 'adulto'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_faixa_etaria_tipo_hospedagem set faixaId = 2 where tipo = 'crianca'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_faixa_etaria_tipo_hospedagem set faixaId = 3 where tipo = 'bebe'";
        $query = $this->db->query($sql_update);
    }

    function update_faixa_etaria_adicional() {
        $sql_update = "UPDATE sma_faixa_etaria_adicional set faixaId = 1 where tipo = 'adulto'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_faixa_etaria_adicional set faixaId = 2 where tipo = 'crianca'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_faixa_etaria_adicional set faixaId = 3 where tipo = 'bebe'";
        $query = $this->db->query($sql_update);

        $sql_update = "update  sma_faixa_etaria_adicional
                        set valor = (select unit_price as price from sma_combo_items where sma_combo_items.product_id = sma_faixa_etaria_adicional.produto and sma_combo_items.item_id = sma_faixa_etaria_adicional.servico),
                        status = 'ATIVO'
                        where valor = 0 and tipo in ('adulto', 'crianca') and status = 'INATIVO'";
        $query = $this->db->query($sql_update);
    }

    function update_products() {
        $sql_update = "UPDATE sma_products SET unit = 'Confirmado', active = 1 where unit = 'Montando'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_products SET unit = 'Arquivado', active = 0 where unit in ('Executado', 'Cancelado')";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_products SET isEmbarque = 1";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_products SET enviar_site = 1 where enviar_site = 'sim'";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_products SET enviar_site = 0 where enviar_site = 'nao' ";
        $query = $this->db->query($sql_update);

        $sql_update = "UPDATE sma_products SET apenas_cotacao = 0";
        $query = $this->db->query($sql_update);
    }

    public function down() {}
}
