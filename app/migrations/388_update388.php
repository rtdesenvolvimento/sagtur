<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update388 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.8.8'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'meio_divulgacao_default' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'captar_meio_divulgacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
