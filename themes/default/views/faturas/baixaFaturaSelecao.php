<style type="text/css" media="screen">

    .div_sr {
        width: 100%;
        height: 350px;
        overflow: auto;
    }

    #SLDataFatura td:nth-child(1) {display: none;}
    #SLDataFatura td:nth-child(2) {text-align: center;}
    #SLDataFatura td:nth-child(3) {text-align: left;width: 30%;}
    #SLDataFatura td:nth-child(4) {text-align: left;width: 30%;}
    #SLDataFatura td:nth-child(5) {text-align: left;display: none;}
    #SLDataFatura td:nth-child(6) {text-align: left;display: none;}
    #SLDataFatura td:nth-child(7) {text-align: right;}
    #SLDataFatura td:nth-child(8) {text-align: right;}
    #SLDataFatura td:nth-child(9) {text-align: right;}
    #SLDataFatura td:nth-child(10) {text-align: right;}
    #SLDataFatura td:nth-child(11) {text-align: center;display: none;}
    #SLDataFatura td:nth-child(12) {text-align: center;display: none;}
    #SLDataFatura td:nth-child(13) {text-align: center;display: none;}
    #SLDataFatura td:nth-child(14) {text-align: center;display: none;}
</style>


<script>
    $(document).ready(function () {

        function attachDescriptionToName(x, alignment, aaData) {
            if (aaData[15] !== null) {

                let codigo = aaData[17];

                if ( aaData[15] === 'boleto') {
                    x = '<i class="fa fa-barcode"></i> '+x+'<br/><small>Cod. '+codigo+'</small>';
                } else  {
                    x = '<i class="fa fa-credit-card"></i> '+x;
                }
            }

            if (aaData[5] !== null) {
                x += '<br/><small><i class="fa fa-money"></i> '+aaData[5]+'</small>';
            }

            return x;
        }

        var oTable = $('#SLDataFatura').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": -1,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('faturas/getFaturasMalote/'.$malote->id)?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filter_numero_documento", "value":  $('#filter_numero_documento').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterDespesa", "value":  $('#filterDespesa').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                null,
                null,
                null,
                null
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                let total = 0, pago = 0, pagar = 0, status, recebidas = 0, aReceber = 0, vencidas = 0, totalizadores = 0;

                for (var i = 0; i < aaData.length; i++) {
                    status = aaData[aiDisplay[i]][9];

                    total += parseFloat(aaData[aiDisplay[i]][6]);
                    pago += parseFloat(aaData[aiDisplay[i]][7]);
                    pagar += parseFloat(aaData[aiDisplay[i]][8]);

                    if (status === 'ABERTA') aReceber += parseFloat(aaData[aiDisplay[i]][8]);
                    if (status === 'PARCIAL' || status === 'QUITADA') recebidas += parseFloat(aaData[aiDisplay[i]][7]);

                    totalizadores += recebidas + aReceber;
                }

                var nCells = nRow.getElementsByTagName('th');

                nCells[6].innerHTML = currencyFormat(parseFloat(total));
                nCells[7].innerHTML = currencyFormat(parseFloat(pago));
                nCells[8].innerHTML = currencyFormat(parseFloat(pagar));

                $('#recebidas').html(currencyFormat(recebidas));
                $('#aReceber').html(currencyFormat(aReceber));
                $('#vencidas').html(currencyFormat(vencidas));
                $('#totalizador').html(currencyFormat(totalizadores));
            }
        });
    });
</script>

<div class="box">
    <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('baixa_fatura_selecao'); ?></h2></div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <div class="box-header">
                                <h2 class="blue">
                                    <i class="fa-fw fa fa-plus-square-o"></i><?=lang('header_baixa_fatura_selecao')?>
                                </h2>
                            </div>
                            <div class="box-content div_sr">
                                <div class="table-responsive">
                                    <table id="SLDataFatura" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                        <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                            <th><?php echo $this->lang->line("vencimento"); ?></th>
                                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                                            <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                                            <th><?php echo $this->lang->line("total"); ?></th>
                                            <th><?php echo $this->lang->line("pago"); ?></th>
                                            <th><?php echo $this->lang->line("pagar"); ?></th>
                                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                                            <th style="display: none;">Parc.</th>
                                            <th style="display: none;">Ult.</th>
                                            <th style="display: none;">Atras.</th>
                                            <th style="display: none;">Venda.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                            <th><?php echo $this->lang->line("vencimento"); ?></th>
                                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                                            <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                                            <th><?php echo $this->lang->line("total"); ?></th>
                                            <th><?php echo $this->lang->line("pago"); ?></th>
                                            <th><?php echo $this->lang->line("pagar"); ?></th>
                                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                                            <th style="display: none;">Parc.</th>
                                            <th style="display: none;">Ult.</th>
                                            <th style="display: none;">Atras.</th>
                                            <th style="display: none;">Venda.</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php echo form_open_multipart("faturas/baixaFaturaSelecaoPagar", array('data-toggle' => 'validator', 'role' => 'form')); ?>
                    <div class="modal-body">
                        <div id="payments">
                            <div class="well well-sm well_1">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("data_pagamento", "data_pagamento"); ?>
                                                <?php if ($malote->status == 'Aberto') { ?>
                                                    <?= form_input('data_pagamento', (isset($_POST['data_pagamento']) ? $_POST['data_pagamento'] : ''), 'class="form-control datetime" id="date" required="required"'); ?>
                                                <?php } else { ?>
                                                    <?= form_input('data_pagamento', $this->sma->hrld($malote->dt_fechamento), 'class="form-control" id="datecreated_at" required="required" readonly'); ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="payment">
                                                <div class="form-group">
                                                    <?= lang("valor_total_vencimento", "valor_total_vencimento"); ?>
                                                    <input name="valor_total_vencimento" type="text" readonly value="<?= $this->sma->formatMoney($malote->total); ?>" class="pa form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="payment">
                                                <div class="form-group">
                                                    <?= lang("valor_total_pago", "valor_total_pago"); ?>
                                                    <input name="valor_total_pago" type="text" readonly value="<?= $this->sma->formatMoney($malote->total_pago); ?>" class="pa form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="payment">
                                                <div class="form-group">
                                                    <?= lang("valor_total_pagamento", "valor_total_pagamento"); ?>
                                                    <input name="valor_total_pagamento" type="text" readonly value="<?= $this->sma->formatMoney($malote->total - $malote->total_pago);  ?>" class="pa form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang('tipo_cobranca_financeiro', 'tipoCobrancaPagamento'); ?>
                                                <select name="tipoCobrancaPagamento" id="tipoCobrancaPagamento" <?php if ($malote->status != 'Aberto') echo 'disabled';?>  class="form-control input-tip select" data-placeholder="<?=lang("select") . ' ' . lang("tipo_cobranca_financeiro");?>" required="required" style="width: 100%;">
                                                    <option value=""><?= lang('select') . ' ' . lang('tipo_cobranca_financeiro'); ?></option>
                                                    <?php foreach ($tiposCobranca as $tipoCobranca) { ?>
                                                        <?php if ($malote->status == 'Aberto') { ?>
                                                            <?php if ($tipoCobranca->id ==  $tipoCobrancaDefoult->id) {?>
                                                                <option value="<?= $tipoCobranca->id; ?>" selected="selected" movimentador="<?=$tipoCobranca->conta?>" formapagamento="<?=$tipoCobranca->formapagamento?>"><?= $tipoCobranca->name; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?= $tipoCobranca->id; ?>" movimentador="<?=$tipoCobranca->conta?>" formapagamento="<?=$tipoCobranca->formapagamento?>"><?= $tipoCobranca->name; ?></option>
                                                            <?php } ?>
                                                        <?php }  else {?>
                                                            <?php if ($tipoCobranca->id == $malote->tipo_cobranca_id) {?>
                                                                <option value="<?= $tipoCobranca->id; ?>" selected="selected"><?= $tipoCobranca->name; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?= $tipoCobranca->id; ?>" ><?= $tipoCobranca->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang('conta_destino', 'movimentador'); ?>
                                                <?php
                                                $cbContas[''] = lang('select') . ' ' . lang('conta_destino');
                                                foreach ($movimentadores as $movimentador) {
                                                    $cbContas[$movimentador->id] = $movimentador->name;
                                                }
                                                ?>

                                                <?php if ($malote->status == 'Aberto') { ?>
                                                    <?= form_dropdown('movimentador', $cbContas, $tipoCobrancaDefoult->conta, 'class="form-control" required="required" id="movimentador"'); ?>
                                                <?php } else { ?>
                                                    <?= form_dropdown('movimentador', $cbContas, $malote->movimentador_id, 'class="form-control" disabled required="required" id="movimentador" disabled'); ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang("forma_pagamento", "formapagamento"); ?>
                                                <?php
                                                $cbFormaPagamento[''] = lang('select') . ' ' . lang('forma_pagamento');
                                                foreach ($formaspagamento as $formapagamento) {
                                                    $cbFormaPagamento[$formapagamento->id] = $formapagamento->name;
                                                }
                                                ?>

                                                <?php if ($malote->status == 'Aberto') { ?>
                                                    <?= form_dropdown('formapagamento', $cbFormaPagamento, $tipoCobrancaDefoult->formapagamento, 'class="form-control" required="required" id="formapagamento"'); ?>
                                                <?php } else { ?>
                                                    <?= form_dropdown('formapagamento', $cbFormaPagamento, $malote->forma_pagamento_id, 'class="form-control" disabled required="required" id="formapagamento" disabled'); ?>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <?php if ($malote->status == 'Aberto') { ?>
                            <div class="form-group">
                                <?= lang("attachment", "attachment") ?>
                                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                                       class="form-control file">
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <?= lang("note", "note"); ?>
                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $malote->note), 'class="form-control" id="note"'); ?>
                        </div>
                        <?php echo form_hidden('malote_id', $malote->id); ?>

                        <?php if ($malote->status == 'Aberto') { ?>
                            <?php echo form_submit('add_payment_selection', lang('add_payment_selection'), 'class="btn btn-primary"'); ?>
                        <?php } ?>
                    </div>

                    <div class="col-xs-6 pull-right">
                        <div class="well well-sm">
                            <p>
                                <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                                <?= lang("date"); ?>: <?= $this->sma->hrld($malote->created_at); ?>
                            </p>
                            <?php if ($malote->updated_by) { ?>
                                <p>
                                    <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                                    <?= lang("update_at"); ?>: <?= $this->sma->hrld($malote->updated_at); ?>
                                </p>
                            <?php } ?>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        $('#tipoCobrancaPagamento').change(function (evet){
            let movimentador = $('#tipoCobrancaPagamento option:selected').attr('movimentador');
            let formapagamento = $('#tipoCobrancaPagamento option:selected').attr('formapagamento');

            $('#movimentador').val(movimentador).change();
            $('#formapagamento').val(formapagamento).change();
        });

        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });
</script>
