<style type="text/css" media="screen">
    #TBVehicle td:nth-child(1) {display: none;}
    #TBVehicle td:nth-child(2) {}
    #TBVehicle td:nth-child(3) {text-align: center;}
    #TBVehicle td:nth-child(4) {}
    #TBVehicle td:nth-child(5) {}
    #TBVehicle td:nth-child(6) {}
    #TBVehicle td:nth-child(7) {text-align: center;}
    #TBVehicle td:nth-child(8) {text-align: center;}
    #TBVehicle td:nth-child(9) {text-align: center;}
    #TBVehicle td:nth-child(10) {text-align: center;}
    #TBVehicle td:nth-child(11) {text-align: center;}
</style>

<script>
    function show_status_vehicle(status) {

        let name = status.split('@')[0];
        let cor = status.split('@')[1];

        return '<div class="text-center" style="text-transform: uppercase;"><span class="label" style="background-color:' + cor + '">' + name + '</span></div>';
    }

    function placa(placa) {
        return '<div class="text-center" style="text-transform: uppercase;"><span class="label" style="background-color:#0044cc">' + placa + '</span></div>';
    }

    $(document).ready(function () {
        $('#TBVehicle').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('vehicle/getVehicles') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {},
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"bSortable": false, "mRender": img_hl},
                null,
                null,
                null,
                null,
                {"mRender": placa},
                null,
                null,
                null,
                {"mRender": show_status_vehicle},
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {}
        });
    });
</script>
<?= form_open('financeiro/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-car"></i><?= lang('vehicles'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('vehicle/add')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_vehicle')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table id="TBVehicle" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:40px; width: 40px; text-align: center;display: none;"><?php echo $this->lang->line("ck"); ?></th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th><?= $this->lang->line("reference_no"); ?></th>
                            <th><?= $this->lang->line("tipo"); ?></th>
                            <th><?= $this->lang->line("categoria"); ?></th>
                            <th><?= $this->lang->line("veiculo"); ?></th>
                            <th><?= $this->lang->line("placa"); ?></th>
                            <th><?= $this->lang->line("marca"); ?></th>
                            <th><?= $this->lang->line("modelo"); ?></th>
                            <th><?= $this->lang->line("cor"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th colspan="11"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

