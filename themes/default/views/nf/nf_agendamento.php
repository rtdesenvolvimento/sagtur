<style type="text/css" media="screen">

    #nfAgendamentoTable td:nth-child(2) {width: 30%}
    #nfAgendamentoTable td:nth-child(3) {text-align: right;}
    #nfAgendamentoTable td:nth-child(4) {width: 50%}
    #nfAgendamentoTable td:nth-child(5) {text-align: center;width: 5%;}
    #nfAgendamentoTable td:nth-child(6) {text-align: center;width: 5%;}

</style>

<script>
    $(document).ready(function () {

        function st_nf(x) {
            if (x === 'generated') {
                return '<div class="text-center"><span class="label label-warning">Gerada</span></div>'
            } else if (x === 'scheduled') {
                return '<div class="text-center"><span class="label label-default">Agendada</span></div>'
            } else if (x === 'issued') {
                return '<div class="text-center"><span class="label label-success">Emitida</span></div>'
            } else if (x === 'cancel') {
                return '<div class="text-center"><span class="label label-danger">Cancelada</span></div>'
            } else {
                return '<div class="text-center"><span class="label label-danger">' + x + '</span></div>'
            }
        }

        $('#nfAgendamentoTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('notafiscal/getNFAGendamentos') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                {"mRender": currencyFormat},
                null,
                {"mRender": fsd},
                {"mRender": st_nf},
                {"bSortable": false}
            ]
        });
    });
</script>
<?= form_open('nf/expense_nfa_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-leaf"></i><?= lang('nf_agendamento'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="#" id="emitir_notasfiscal" data-action="emitir_notasfiscal">
                                <i class="fa fa-repeat"></i> <?=lang('emitir_notasfiscal')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="nfAgendamentoTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= $this->lang->line("customer"); ?></th>
                            <th><?= $this->lang->line("valor_nf"); ?></th>
                            <th><?= $this->lang->line("descricao_cobranca"); ?></th>
                            <th><?= $this->lang->line("emissao"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= $this->lang->line("customer"); ?></th>
                            <th><?= $this->lang->line("valor_nf"); ?></th>
                            <th><?= $this->lang->line("descricao_cobranca"); ?></th>
                            <th><?= $this->lang->line("emissao"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {
        $('body').on('click', '#emitir_notasfiscal', function(e) {
            e.preventDefault();

            // Obtém todos os checkboxes marcados
            const selected = $('input[name="val[]"]:checked');

            // Verifica se há checkboxes selecionados
            if (selected.length === 0) {
                alert('Por favor, selecione pelo menos um agendamento.');
                return;
            }

            selected.each(function (index, checkbox) {
                setTimeout(() => {
                    const value = $(checkbox).val();
                    $.ajax({
                        url: '<?=base_url()?>notafiscal/emitir_agendamento/' + value,
                        type: 'GET',
                        success: function (response) {
                            console.log(`Agendamento ${value} emitido com sucesso.`);
                            $('#nfAgendamentoTable').DataTable().fnClearTable()
                        },
                        error: function (error) {
                            console.error(`Erro ao emitir agendamento ${value}:`, error);
                        }
                    });
                }, index * 1000); // Adiciona 1 segundo de intervalo entre as chamadas
            });

        });
    });
</script>

