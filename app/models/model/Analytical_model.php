<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Analytical_model extends CI_Model
{

    public $date;

    public $sessions_id;
    public $ip_address;
    public $page_web;
    public $http_method;
    public $http_response_code;

    public $browser_information_name;
    public $browser_information_version;

    public $operating_system_information_name;

    public $user_agent_info;

    public $information_cookies;

    public $information_session;

    public $server_port;

    public $server_name;

    public $server_signature;
    public $server_software;

    public $http_referer;

    public $request_time;

    public $type;

    public $product_id;
    public $programacao_id;

    public $page_current;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getSessionsId()
    {
        return $this->sessions_id;
    }

    /**
     * @param mixed $sessions_id
     */
    public function setSessionsId($sessions_id): void
    {
        if ($sessions_id == null) $sessions_id = '';
        $this->sessions_id = $sessions_id;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @param mixed $ip_address
     */
    public function setIpAddress($ip_address): void
    {
        if ($ip_address == null) $ip_address = '';

        $this->ip_address = $ip_address;
    }

    /**
     * @return mixed
     */
    public function getPageWeb()
    {
        return $this->page_web;
    }

    /**
     * @param mixed $page_web
     */
    public function setPageWeb($page_web): void
    {
        if ($page_web == null) $page_web = '';

        $this->page_web = $page_web;
    }

    /**
     * @return mixed
     */
    public function getHttpMethod()
    {
        return $this->http_method;
    }

    /**
     * @param mixed $http_method
     */
    public function setHttpMethod($http_method): void
    {
        if ($http_method == null) $http_method = '';

        $this->http_method = $http_method;
    }

    /**
     * @return mixed
     */
    public function getHttpResponseCode()
    {
        return $this->http_response_code;
    }

    /**
     * @param mixed $http_response_code
     */
    public function setHttpResponseCode($http_response_code): void
    {
        if ($http_response_code == null)  $http_response_code = '';
        $this->http_response_code = $http_response_code;
    }

    /**
     * @return mixed
     */
    public function getBrowserInformationName()
    {
        return $this->browser_information_name;
    }

    /**
     * @param mixed $browser_information_name
     */
    public function setBrowserInformationName($browser_information_name): void
    {
        if ($browser_information_name == null) $browser_information_name = '';
        $this->browser_information_name = $browser_information_name;
    }

    /**
     * @return mixed
     */
    public function getBrowserInformationVersion()
    {
        return $this->browser_information_version;
    }

    /**
     * @param mixed $browser_information_version
     */
    public function setBrowserInformationVersion($browser_information_version): void
    {
        if ($browser_information_version == null) $browser_information_version = '';

        $this->browser_information_version = $browser_information_version;
    }

    /**
     * @return mixed
     */
    public function getOperatingSystemInformationName()
    {
        return $this->operating_system_information_name;
    }

    /**
     * @param mixed $operating_system_information_name
     */
    public function setOperatingSystemInformationName($operating_system_information_name): void
    {
        if ($operating_system_information_name == null) $operating_system_information_name = '';

        $this->operating_system_information_name = $operating_system_information_name;
    }

    /**
     * @return mixed
     */
    public function getUserAgentInfo()
    {
        return $this->user_agent_info;
    }

    /**
     * @param mixed $user_agent_info
     */
    public function setUserAgentInfo($user_agent_info): void
    {
        if ($user_agent_info == null) $user_agent_info = '';

        $this->user_agent_info = $user_agent_info;
    }

    /**
     * @return mixed
     */
    public function getInformationCookies()
    {
        return $this->information_cookies;
    }

    /**
     * @param mixed $information_cookies
     */
    public function setInformationCookies($information_cookies): void
    {
        if ($information_cookies == null) $information_cookies = '';

        $this->information_cookies = $information_cookies;
    }

    /**
     * @return mixed
     */
    public function getInformationSession()
    {
        return $this->information_session;
    }

    /**
     * @param mixed $information_session
     */
    public function setInformationSession($information_session): void
    {
        if ($information_session == null) $information_session = '';

        $this->information_session = $information_session;
    }

    /**
     * @return mixed
     */
    public function getServerPort()
    {
        return $this->server_port;
    }

    /**
     * @param mixed $server_port
     */
    public function setServerPort($server_port): void
    {
        if ($server_port == null) $server_port = '';

        $this->server_port = $server_port;
    }

    /**
     * @return mixed
     */
    public function getServerName()
    {
        return $this->server_name;
    }

    /**
     * @param mixed $server_name
     */
    public function setServerName($server_name): void
    {
        if ($server_name == null) $server_name = '';

        $this->server_name = $server_name;
    }

    /**
     * @return mixed
     */
    public function getServerSignature()
    {
        return $this->server_signature;
    }

    /**
     * @param mixed $server_signature
     */
    public function setServerSignature($server_signature): void
    {
        if ($server_signature == null) $server_signature = '';

        $this->server_signature = $server_signature;
    }

    /**
     * @return mixed
     */
    public function getServerSoftware()
    {
        return $this->server_software;
    }

    /**
     * @param mixed $server_software
     */
    public function setServerSoftware($server_software): void
    {
        if ($server_software == null) $server_software = '';

        $this->server_software = $server_software;
    }

    /**
     * @return mixed
     */
    public function getHttpReferer()
    {
        return $this->http_referer;
    }

    /**
     * @param mixed $http_referer
     */
    public function setHttpReferer($http_referer): void
    {
        if ($http_referer == null) $http_referer  = '';

        $this->http_referer = $http_referer;
    }

    /**
     * @return mixed
     */
    public function getRequestTime()
    {
        return $this->request_time;
    }

    /**
     * @param mixed $request_time
     */
    public function setRequestTime($request_time): void
    {
        if ($request_time == null) $request_time = '';

        $this->request_time = $request_time;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        if ($date == null) $date = '';
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {

        if ($type == null) $type = '';

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id): void
    {

        if ($product_id == null)  $product_id = '';
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getProgramacaoId()
    {
        return $this->programacao_id;
    }

    /**
     * @param mixed $programacao_id
     */
    public function setProgramacaoId($programacao_id): void
    {
        if ($programacao_id == null) $programacao_id = '';

        $this->programacao_id = $programacao_id;
    }

    /**
     * @return mixed
     */
    public function getPageCurrent()
    {
        return $this->page_current;
    }

    /**
     * @param mixed $page_current
     */
    public function setPageCurrent($page_current): void
    {
        $this->page_current = $page_current;
    }



}