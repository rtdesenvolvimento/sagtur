<script>
    $(document).ready(function () {
        $('#plugsign_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('plugsign_settings'); ?></h2>
        <div class="box-icon">
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('role' => 'form', 'id="plugsign_form"');
                echo form_open("contract_settings/plugsign", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('1' => lang("sim"), '0' => lang("nao"));
                            echo form_dropdown('active', $yn, $plugsign->active, 'class="form-control tip" required="required" id="active"'); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("account_token_plugsign", "account_token_plugsign"); ?>
                            <?=form_textarea('account_token_plugsign', $plugsign->token, 'id="account_token_plugsign" class="form-control kb-text skip" style="height: 100px;"');?>

                        </div>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>