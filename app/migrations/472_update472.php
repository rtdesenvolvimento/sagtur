<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update472 extends CI_Migration {

    public function up() {

        $this->create_document_events();

        $this->db->update('settings',  array('version' => '4.7.2'), array('setting_id' => 1));
    }

    public function create_document_events() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'timestamp', 'null' => FALSE ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'user_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'user' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'event' => array('type' => 'LONGTEXT', 'default' => '', 'null' => FALSE),
            'status' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'link_contract' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => TRUE),
            'log' => array('type' => 'LONGTEXT', 'default' => '', 'null' => TRUE),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_events', TRUE, $attributes);
    }

    public function down() {}
}
