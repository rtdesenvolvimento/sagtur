<style>
    .form-control_aereo {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>
<div class="row">
    <div class="col-md-12" id="form-meu-pacote">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("localizadorPassagemAerea", "slLocalizadorPassagemAerea"); ?>
                    <?php echo form_input('localizadorPassagemAerea', (isset($_POST['localizadorPassagemAerea']) ? $_POST['localizadorPassagemAerea'] : ""), 'class="form-control" id="slLocalizadorPassagemAerea"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("emissaoPassagemAerea", "slEmissaoPassagemAerea"); ?>
                    <?php echo form_input('emissaoPassagemAerea', (isset($_POST['emissaoPassagemAerea']) ? $_POST['emissaoPassagemAerea'] : ""), 'class="form-control input-tip" id="slEmissaoPassagemAerea"', 'date'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group company">
                    <?= lang("tipoDestinoPassagemAerea", "slTipoDestinoPassagemAerea"); ?>
                    <select id="slTipoDestinoPassagemAerea" name="tipoDestinoPassagemAerea" class="form-control">
                        <option value="NACIONAL">NACIONAL</option>
                        <option value="INTERNACIONAL">INTERNACIONAL</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("apresentacaoPassagem", "apresentacaoPassagem"); ?>
                    <?php echo form_input('apresentacaoPassagem', (isset($_POST['apresentacaoPassagem']) ? $_POST['apresentacaoPassagem'] : ""), 'class="form-control autocomplete_origem_destino" id="apresentacaoPassagem"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("dataApresentacao", "dataApresentacao"); ?>
                    <?php echo form_input('dataApresentacao', (isset($_POST['dataApresentacao']) ? $_POST['dataApresentacao'] : ""), 'class="form-control input-tip" id="dataApresentacao"', 'date'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("horaApresentacao", "horaApresentacao"); ?>
                    <?php echo form_input('horaApresentacao', (isset($_POST['horaApresentacao']) ? $_POST['horaApresentacao'] : ""), 'class="form-control input-tip" id="horaApresentacao"', 'time'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <?= lang("consolidadora", "supplierPassagemAerea"); ?>
                    <?php echo form_input('supplierPassagemAerea', (isset($_POST['supplierPassagemAerea']) ? $_POST['supplierPassagemAerea'] : ""), 'class="form-control" id="supplierPassagemAerea"'); ?>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <?= lang("numeroBilhete", "numeroBilhete"); ?>
                    <?php echo form_input('numeroBilhete', (isset($_POST['numeroBilhete']) ? $_POST['numeroBilhete'] : ""), 'class="form-control" id="numeroBilhete"'); ?>
                </div>
            </div>
        </div>
        <h2 class="blue" style="margin-top: 0px;"><?= lang('header.label.itinerario')?></h2><hr/>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group" style="float: left;width: 10%;">
                    <?= lang("companhiaPassagemAerea", "companhiaPassagemAerea"); ?>*
                    <?php echo form_input('companhiaPassagemAerea[]', (isset($_POST['companhiaPassagemAerea']) ? $_POST['companhiaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio" placeholder="CIA"'); ?>
                </div>
                <div class="form-group" style="float: left;width: 16%;">
                    <?= lang("origemPassagemAerea", "slOrigemPassagemAerea"); ?>*
                    <?php echo form_input('slOrigemPassagemAerea[]', (isset($_POST['slOrigemPassagemAerea']) ? $_POST['slOrigemPassagemAerea'] : ""), 'class="form-control_aereo input-tip autocomplete_origem_destino obrigatorio" placeholder="Digite uma origem/aeroporto"'); ?>
                </div>
                <div class="form-group" style="float: left;width: 16%;">
                    <?= lang("destinoPassagemAerea", "slDestinoPassagemAerea"); ?>*
                    <?php echo form_input('destinoPassagemAerea[]', (isset($_POST['destinoPassagemAerea']) ? $_POST['destinoPassagemAerea'] : ""), 'class="form-control_aereo input-tip autocomplete_origem_destino obrigatorio" placeholder="Digite um destino/aeroporto"'); ?>
                </div>
                <div class="form-group" style="float: left;">
                    <?= lang("dataSaidaPassagemAerea", "dataSaidaPassagemAerea"); ?>*
                    <?php echo form_input('dataSaidaPassagemAerea[]', (isset($_POST['dataSaidaPassagemAerea']) ? $_POST['dataSaidaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'date'); ?>
                </div>
                <div class="form-group" style="float: left;">
                    <?= lang("horaSaidaPassagemAerea", "horaSaidaPassagemAerea"); ?>*
                    <?php echo form_input('horaSaidaPassagemAerea[]', (isset($_POST['horaSaidaPassagemAerea']) ? $_POST['horaSaidaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'time'); ?>
                </div>
                <div class="form-group" style="float: left;">
                    <?= lang("dataChegadaPassagemAerea", "dataChegadaPassagemAerea"); ?>*
                    <?php echo form_input('dataChegadaPassagemAerea[]', (isset($_POST['dataChegadaPassagemAerea']) ? $_POST['dataChegadaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'date'); ?>
                </div>
                <div class="form-group" style="float: left;">
                    <?= lang("horaChegadaPassagemAerea", "horaChegadaPassagemAerea"); ?>*
                    <?php echo form_input('horaChegadaPassagemAerea[]', (isset($_POST['horaChegadaPassagemAerea']) ? $_POST['horaChegadaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'time'); ?>
                </div>
                <div class="form-group" style="float: left;width: 6%;">
                    <?= lang("vooPassagemAerea", "slVooPassagemAerea"); ?>*
                    <?php echo form_input('vooPassagemAerea[]', (isset($_POST['vooPassagemAerea']) ? $_POST['vooPassagemAerea'] : ""), 'class="form-control_aereo obrigatorio"'); ?>
                </div>
                <div class="form-group" style="float: left;width: 6%;">
                    <?= lang("assento", "assento"); ?>*
                    <?php echo form_input('assento[]', (isset($_POST['assento']) ? $_POST['assento'] : ""), 'class="form-control_aereo obrigatorio"'); ?>
                </div>
                <div class="form-group" style="float: left;width: 6%;display: none;">
                    <?= lang("bagagem", "bagagem"); ?>
                    <?php echo form_input('bagagem[]', (isset($_POST['bagagem']) ? $_POST['bagagem'] : ""), 'class="form-control_aereo"'); ?>
                </div>
                <div class="form-group" style="float: left;margin-top: 25px;margin-left: 15px;font-size: 20px;">
                    <i class="fa fa-plus addItinerario" style="cursor: pointer;"></i>
                </div>
            </div>
        </div>
        <span id="pacotes-com-aereo-itinerario"></span>
    </div>
</div>
<br/>
<div id="load-new-pacote-aereo"></div>
<div class="clearfix"></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
        <div class="fprom-group">
            <button type="button" class="btn btn-primary" name="adicionar-pacote-operadora" id="adicionar-pacote-aereo"><?= lang('button.label.adiconar.aereo');?></button>
        </div>
    </div>
</div>
