<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update420 extends CI_Migration {

    public function up() {

        $this->create_table_plantao();
        $this->create_table_interesses();
        $this->create_table_assunto();
        $this->create_table_department();
        $this->create_table_etiqueta();
        $this->create_table_dificuldade();
        $this->create_table_prioridade();

        $this->create_table_captacao();
        $this->create_table_contato_captacao();
        $this->crate_table_tarefas();

        $this->db->update('settings',  array('version' => '4.2.0'), array('setting_id' => 1));
    }

    function create_table_captacao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'date' => array('type' => 'DATE', 'null' => TRUE ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => 'ABERTO' ),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => FALSE ),
            'plantao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'interesse_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'meio_divulgacao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'forma_atendimento_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),//todo assunto
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'cell_phone' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'commercial_phone' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'additional_telephone' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'city' => array('type' => 'VARCHAR', 'constraint' => '50' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11, 'null' => FALSE  ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'update_by' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),

        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('captacao', TRUE, $attributes);
    }

    function create_table_contato_captacao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => 'ABERTO' ),

            'captacao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'department_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'etiqueta_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'dificuldade_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'prioridade_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),

            'data_contato' => array('type' => 'DATE', 'null' => TRUE ),
            'hora_contato' => array('type' => 'TIME', 'null' => TRUE ),

            'forma_atendimento_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),//todo assunto
            'retorno' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'data_retorno' => array('type' => 'DATE', 'null' => TRUE ),
            'hora_retorno' => array('type' => 'TIME', 'null' => TRUE ),
            'forma_atendimento_retorno_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),//todo assunto
            'biller_retorno_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'dificuldade_retorno_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'prioridade_retorno_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'tarefa_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('contato_captacao', TRUE, $attributes);
    }

    function crate_table_tarefas()
    {
        $fields = array(

            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => 'ABERTO' ),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//atribuido a
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'prioridade_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'dificuldade_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => FALSE ),

            'data_inicio' => array('type' => 'DATE', 'null' => TRUE ),
            'data_final' => array('type' => 'DATE', 'null' => TRUE ),

            'hora_inicio' => array('type' => 'TIME', 'null' => TRUE ),
            'hora_final' => array('type' => 'TIME', 'null' => TRUE ),

            'etiqueta_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'notificado' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tarefas', TRUE, $attributes);
    }

    function create_table_plantao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('plantao', TRUE, $attributes);

        $this->db->insert('plantao', array('name' => 'Plantão na Agência'));
        $this->db->insert('plantao', array('name' => 'Site'));
    }

    function create_table_department() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('department', TRUE, $attributes);

        $this->db->insert('department', array('name' => 'Vendas'));
    }

    function create_table_etiqueta() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '999' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('etiqueta', TRUE, $attributes);

        $this->db->insert('etiqueta', array('name' => 'Clientes', 'cor' => '#ffff00'));
    }


    function create_table_dificuldade() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'previsao_horas' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '999' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('dificuldade', TRUE, $attributes);

        $this->db->insert('dificuldade', array('name' => 'Baixa', 'reference_no' => 'Baixa'));
        $this->db->insert('dificuldade', array('name' => 'Média', 'reference_no' => 'Média'));
        $this->db->insert('dificuldade', array('name' => 'Alta', 'reference_no' => 'Alta'));

    }

    function create_table_prioridade() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '999' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('prioridade', TRUE, $attributes);

        $this->db->insert('prioridade', array('name' => 'Prioridade Normal'));
        $this->db->insert('prioridade', array('name' => 'Prioridade Moderada'));
        $this->db->insert('prioridade', array('name' => 'Prioridade Alta'));

    }

    function create_table_interesses() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('interesses', TRUE, $attributes);

        $this->db->insert('interesses', array('name' => 'PACOTE TURISTICO'));
    }

    function create_table_assunto() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('assunto', TRUE, $attributes);

        $this->db->insert('assunto', array('name' => 'Chat WhatsApp'));
        $this->db->insert('assunto', array('name' => 'Chat Instagram'));
        $this->db->insert('assunto', array('name' => 'Chat Facebook'));
        $this->db->insert('assunto', array('name' => 'E-mail'));
        $this->db->insert('assunto', array('name' => 'Telefonema'));
        $this->db->insert('assunto', array('name' => 'Visita Agência'));

    }

    public function down() {}
}
