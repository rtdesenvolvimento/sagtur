<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CobreFacilController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
    }

    function index()
    {
        show_404();
    }

    public function notification()
    {
        $defaultPath = (dirname(__FILE__));
        $defaultName = 'CofreFacil.log';
        $fileLocation = $defaultPath . DIRECTORY_SEPARATOR . $defaultName;

        $json_body = file_get_contents('php://input');
        $obj = json_decode($json_body);

        $file = fopen($fileLocation, "a");
        $date_message = "{" . @date("Y/m/d H:i:s", time()) . "}";
        $str = "$date_message $json_body";

        fwrite($file, "$str \r\n");
        fclose($file);

        if (!empty($obj->data)) {
            $this->FinanceiroService_model->baixarPagamentoIntegracao('cobrefacil', $obj->data->id);
            echo $obj->data->id;
        }

        return TRUE;
    }

    public function notification_all()
    {
        $cobrancaEmAberto = $this->FinanceiroService_model->getAllFaturasAbertas('cobrefacil');
        foreach ($cobrancaEmAberto as $cobranca) {
            $this->FinanceiroService_model->baixarPagamentoIntegracao('cobrefacil', $cobranca->code);
        }
        return TRUE;
    }
}