<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update382 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->alter_table_order_ref();

        $this->create_table_product_addresses();
        $this->create_table_product_details();
        $this->create_table_product_seo();
        $this->create_table_product_midia();

        $this->popular_tabelas();

        $this->db->update('settings',  array('version' => '3.8.2'), array('setting_id' => 1));
    }

    public function popular_tabelas() {

        $products = $this->getAllProducts();

        foreach ($products as $product) {
            $product = $this->getProductByID($product->id);

            $midia_data = array(
                'url_video' => '',
            );

            $seo_data = array(
                'tag_title' => $product->name,
                'meta_tag_description' => '',
                'url_product' => '',
                'key_words' => '',
            );

            if ($midia_data) {
                $midia_data['product_id'] = $product->id;
                $this->db->insert('product_midia', $midia_data);
            }

            if ($seo_data) {
                $seo['product_id'] = $product->id;
                $this->db->insert('product_seo', $seo_data);
            }
        }
    }

    public function getProductByID($id) {

        $this->db->select('products.*, 
        product_midia.url_video,
        product_seo.tag_title,
        product_seo.meta_tag_description,
        product_seo.url_product,
        product_seo.key_words,
        product_addresses.ponto_encontro,
        product_addresses.cep, 
        product_addresses.endereco, 
        product_addresses.numero, 
        product_addresses.complemento, 
        product_addresses.bairro, 
        product_addresses.cidade, 
        product_addresses.estado,
        product_addresses.pais');

        $this->db->join('product_midia', 'products.id=product_midia.product_id', 'left');
        $this->db->join('product_addresses', 'products.id=product_addresses.product_id', 'left');
        $this->db->join('product_seo', 'products.id=product_seo.product_id', 'left');

        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllProducts() {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function alter_table_settings() {
        $fields = array(
            'location_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'LOC'),
            'vehicle_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'VL'),
            'use_product_landing_page' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//TODO VER COM O DIEGO
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_order_ref() {
        $fields = array(
            'loc' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
            've' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('order_ref', $fields);
    }

    public function create_table_product_addresses() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'cep' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),

            'endereco' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'bairro' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'pais' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),

            'numero' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'ponto_encontro' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'complemento' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cidade' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'estado' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'usar_localizacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'product_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_addresses', TRUE, $attributes);
    }

    public function create_table_product_details() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'titulo' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'product_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_details', TRUE, $attributes);
    }

    public function create_table_product_seo() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'tag_title' => array('type' => 'VARCHAR', 'constraint' => '70', 'null' => FALSE  ),
            'meta_tag_description' => array('type' => 'VARCHAR', 'constraint' => '250', 'null' => FALSE  ),
            'url_product' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE  ),
            'key_words' => array('type' => 'VARCHAR', 'constraint' => '300'  ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_seo', TRUE, $attributes);
    }

    public function create_table_product_midia() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'url_video' => array('type' => 'VARCHAR', 'constraint' => '300'  ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_midia', TRUE, $attributes);
    }

    public function down() {}
}
