<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update378 extends CI_Migration {

    public function up() {

        $this->alter_table_room_list();

        $this->db->update('settings',  array('version' => '3.7.8'), array('setting_id' => 1));
    }

    public function alter_table_room_list() {
        $fields = array(
            'fornecedor' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
        );
        $this->dbforge->add_column('room_list', $fields);
    }


    public function down() {}
}
