<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update508 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '5.0.8'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $this->db->update('shop_settings',  array('web_page_caching' => 1), array('id' => 1));
        $this->db->update('shop_settings',  array('cache_minutes' => 30), array('id' => 1));//30 minutos
    }

    public function down() {}
}
