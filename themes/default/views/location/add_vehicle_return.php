<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_vehicle_return'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-location-form');
        echo form_open_multipart("location/add_vehicle_return/".$location->id, $attrib); ?>
        <div class="modal-body">
            <?php echo form_input('locacao_veiculo_id', $location->id, '', 'hidden'); ?>
            <?php echo form_input('product_id', $location->product_id, '', 'hidden'); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-calendar-check-o"></i> <?= lang("dados_retirada") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("dataPrevista", "lcdatadevolucao"); ?>
                                        <?php echo form_input('dataDevolucaoPrevista', $location->dataDevolucaoPrevista, 'class="form-control input-tip" id="lcdatadevolucao" readonly required="required"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("horaRetiradaPrevista", "lchoradevolucao"); ?>
                                        <?php echo form_input('horaDevolucaoPrevista', $location->horaDevolucaoPrevista, 'class="form-control input-tip" readonly id="lchoradevolucao" required="required"', 'time'); ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("dataRetiradaRealizada", "lcdevolucaorealizada"); ?>
                                        <?php echo form_input('dataDevolucao', $location->dataDevolucaoPrevista, 'class="form-control input-tip" id="lcdevolucaorealizada" required="required"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("horaRetidadaRealizada", "lchoradevolucaorealizada"); ?>
                                        <?php echo form_input('horaDevolucao', $location->horaDevolucaoPrevista, 'class="form-control input-tip" id="lchoradevolucaorealizada" required="required"', 'time'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <?= lang("local_retirada", "lclocaldevolucao"); ?>
                                        <?php
                                        $cbLocalDevolucaoVeiculo[''] = lang("select") . " " . lang("local_retirada");
                                        foreach ($locais_embarque as $local_embarque) {
                                            $cbLocalDevolucaoVeiculo[$local_embarque->id] = $local_embarque->name;
                                        }
                                        echo form_dropdown('local_devolucao_id', $cbLocalDevolucaoVeiculo, $location->local_retirada_id, 'class="form-control select" readonly id="lclocaldevolucao" placeholder="' . lang("select") . " " . lang("local_retirada") . '" required="required"  style="width:100%"')
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("km_devolucao_veiculo", "lckmdevolucao"); ?>
                                        <?php echo form_input('km_devolucao_veiculo', 0, 'class="form-control input-tip" required="required"  id="lckmdevolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <?= lang("tanque", "lstipotanqueveiculo"); ?>
                                    <div class="form-group">
                                        <?php foreach ($cumbustiveis as $combustivel) {?>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?php echo form_radio('tanque_veiculo_id', $combustivel->id, FALSE, 'id="lstipotanqueveiculo"'); ?>
                                                    <label for="attributes" class="padding05"><?= $combustivel->name; ?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-car"></i> <?= lang("resumo_financeiro") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("veiculo", "lcveiculo"); ?>
                                        <?php echo form_input('veiculo_id', (isset($_POST['veiculo']) ? $_POST['veiculo'] : ""), 'id="lcveiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '"readonly class="form-control input-tip" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <br/>
                                        <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $location->image ?>" alt="<?= $location->name_product ?>" class="img-responsive img-thumbnail"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("diarias", "diarias"); ?>
                                            <?php echo form_input('diarias', $location->qtd_dias_locacao, 'class="form-control input-tip" readonly id="diarias"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("horas", "horas"); ?>
                                            <?php echo form_input('horas', $location->qtd_horas_locacao, 'class="form-control input-tip" readonly id="horas"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("total", "total"); ?>
                                            <?php echo form_input('total', $location->grand_total, 'class="form-control input-tip" readonly id="total"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("paid", "paid"); ?>
                                            <?php echo form_input('paid', $location->paid, 'class="form-control input-tip" readonly id="paid"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("falta_pagar", "falta_pagar"); ?>
                                            <?php echo form_input('falta_pagar', $location->grand_tota - $location->paid, 'class="form-control input-tip" readonly id="falta_pagar"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("note", "note"); ?>
                        <?php echo form_textarea('note', '', 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('add_vehicle_return', lang('add_vehicle_return'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $('#lcveiculo').val('<?=$location->veiculo_id;?>').select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
            $.ajax({
                type: "get", async: false,
                url: site.base_url + "vehicle/getVehicle/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                    callback(data[0]);
                }
            });
        },
        ajax: {
            url: site.base_url + "vehicle/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                }
            }
        }
    });
</script>