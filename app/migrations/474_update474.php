<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update474 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '4.7.4'), array('setting_id' => 1));
    }

    public function create_index()
    {
        $this->db->query('CREATE INDEX idx_sma_documents_document_status ON sma_documents(document_status);');
        $this->db->query('CREATE INDEX idx_sma_documents_document_key ON sma_documents(document_key);');
        $this->db->query('CREATE INDEX idx_sma_document_signatures_document_id ON sma_document_signatures(document_id);');
        $this->db->query('CREATE INDEX idx_sma_document_signatures_customer_id ON sma_document_signatures(document_id, customer_id);');
        $this->db->query('CREATE INDEX idx_sma_document_signatures_public_id ON sma_document_signatures(public_id);');
        $this->db->query('CREATE INDEX idx_sma_document_events_document_id ON sma_document_events(document_id);');
        $this->db->query('CREATE INDEX idx_sma_document_customers_email ON sma_document_customers(email);');
        $this->db->query('CREATE INDEX idx_sma_document_customers_public_id ON sma_document_customers(public_id);');
    }
    public function down() {}
}
