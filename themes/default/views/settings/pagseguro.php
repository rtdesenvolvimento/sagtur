<script>
    $(document).ready(function () {
        $('#pagSeguro_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
        $('#active').change(function () {
            var v = $(this).val();
            if (v == 1) {
                $('#account_email').attr('required', 'required');
                $('#pagSeguro_form').bootstrapValidator('addField', 'account_email');
            } else {
                $('#account_email').removeAttr('required');
                $('#pagSeguro_form').bootstrapValidator('removeField', 'account_email');
            }
        });
        var v = <?=$pagSeguro->active;?>;
        if (v == 1) {
            $('#account_email').attr('required', 'required');
            $('#pagSeguro_form').bootstrapValidator('addField', 'account_email');
        } else {
            $('#account_email').removeAttr('required');
            $('#pagSeguro_form').bootstrapValidator('removeField', 'account_email');
        }
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('pagSeguro_settings'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <ul class="btn-tasks">
                    <li class="dropdown"><a href="<?= site_url('system_settings/juno') ?>" class="toggle_down"><i
                                    class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_juno'); ?></span></a>
                    </li>
                </ul>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('role' => 'form', 'id="pagSeguro_form"');
                echo form_open("system_settings/pagseguro", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('0' => lang("nao"), '1' => lang("sim"));
                            echo form_dropdown('active', $yn, $pagSeguro->active, 'class="form-control tip" required="required" id="active"');
                            ?>
                        </div>
                        <div class="form-group">
                            <?= lang("pagseguro_account_email", "account_email"); ?>
                            <?php echo form_input('account_email', $pagSeguro->account_email, 'class="form-control tip" id="account_email"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("pagseguro_account_token", "account_token"); ?>
                            <?php echo form_input('account_token', $pagSeguro->account_token, 'class="form-control tip" id="account_token"'); ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("sandbox", "sandbox"); ?>
                            <?php
                            $yn = array('1' => 'Yes', '0' => 'No');
                            echo form_dropdown('sandbox', $yn, $pagSeguro->sandbox, 'class="form-control tip" required="required" id="sandbox"');
                            ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("pagseguro_account_token_sandbox", "account_token_sandbox"); ?>
                            <?php echo form_input('account_token_sandbox', $pagSeguro->account_token_sandbox, 'class="form-control tip" id="account_token_sandbox"'); ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label>Como pegar o TOKEN PAGSEGURO para integração</label>
                        <iframe width="383" height="384" src="https://www.youtube.com/embed/FodB0MvTfu4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div>

                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>