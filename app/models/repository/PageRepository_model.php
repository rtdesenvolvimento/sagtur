<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PageRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getById($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('page');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getPageBySlug($titulo)
    {
        $pages = $this->getAllPage();

        foreach ($pages as $page) {
            $titulo_page =  $this->sma->slug($page->titulo);

            if ($titulo == $titulo_page) {
                return $page;
            }
        }

        return false;
    }

    public function getAllPage() {

        $this->db->where("active", 1);
        $q = $this->db->get('page');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}
