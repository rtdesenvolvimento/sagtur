<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['update_contract_settings'] = 'Atualizar';
$lang['monitorar_validade'] = 'Monitorar validade?';
$lang['validade_meses'] = 'Quantos meses de validade este documento possui? <small>Iremos te avisar sobre o vencimento por e-mail, contando da última assinatura!</small>';
$lang['auto_destruir_solicitacao'] = 'Auto destruir solicitação?';
$lang['validade_auto_destruir_dias'] = 'Quando a solicitação deve se cancelar automaticamente?<small>Caso o signatário não assine o documento até a data selecionada, iremos cancelar a mesma automáticamente e te avisaremos por e-mail!</small>';
$lang['solicitar_selfie'] = 'Solicitar selfie?';
$lang['solicitar_documento'] = 'Solicitar documento?';
$lang['signature_type'] = 'Como o(s) signatário(s) devem assinar?';
$lang['all'] = 'De qualquer forma (Podem escolher).';
$lang['text'] = 'Digitando texto (Recomendado).';
$lang['draw'] = 'Desenhando na tela (Não recomendado para computadores).';
$lang['upload'] = 'Envio de imagem da assinatura (Upload de imagem).';
$lang['is_observadores'] = 'Adicionar observadores?';
$lang['observadores'] = 'Digite o(s) e-mail(s) separados por vírgula dos observadores, eles são notificados de todas as etapas do documento';
$lang['enviar_sequencial'] = 'Enviar sequencialmente a solicitação de assinatura?';
$lang['note'] = 'Mensagem ao(s) destinatário(s)';
$lang['note_placeholder'] = 'Ex: Segue o contrato para assinatura...';
$lang['setting_updated'] = 'Configurações atualizadas com sucesso!';
$lang['configurar_plugsign'] = 'Configurar Token PlugSign';
$lang['plugsign_settings'] = 'Configurações PlugSign';
$lang['activate'] = 'Ativar?';
$lang['update_settings'] = 'Atualizar configurações';
$lang['account_token_plugsign'] = 'Token da conta PlugSign';
$lang['plugsign_setting_updated'] = 'Configurações PlugSign atualizadas com sucesso!';
$lang['sim'] = 'Sim';
$lang['nao'] = 'Não';
$lang['gerar_contrato_faturamento_venda'] = 'Gerar contrato ao faturar venda Manual?';
$lang['enviar_contrato_signatarios_venda'] = 'Enviar o contrato para os signatários ao faturar venda?';
$lang['gerar_contrato_faturamento_venda_site'] = 'Gerar contrato das Venda Pelo Link de Reservas (Site)?';
$lang['auto_assinar_biller_contrato'] = 'Assinar automaticamente o contrato como responsável?';
$lang['auto_assinar_customer_contrato'] = 'Assinar automaticamente o contrato como cliente, ao gerar a venda Pelo Link de Reservas?';
$lang['contract'] =  'Contrato Padrão';
$lang['default_biller_contract'] = 'Responsável Padrão do Contrato';
$lang['cancel_contract_by_sale'] = 'Cancelar contrato ao cancelar a venda?';