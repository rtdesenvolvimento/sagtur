<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FinanceiroService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('service/FaturaService_model', 'FaturaService_model');
    }

    public function consultaPagamentoByCode($code) {
        return $this->FaturaService_model->consultaPagamentoByCode($code);
    }

    public function buscarDadosTransacaoByReference($code) {
        return $this->FaturaService_model->buscarDadosTransacaoByReference($code);
    }

    public function baixarPagamentoIntegracaoByReference($integracao, $reference) {
        $this->FaturaService_model->baixarPagamentoIntegracaoByReference($integracao, $reference);
    }

    public function baixarPagamentoIntegracao($integracao, $code) {
        $this->FaturaService_model->baixarPagamentoIntegracao($integracao, $code);
    }

    public function reembolso($contaReceber,$faturaId, $motivoReembolso) {

        //aqui sera gerada a conta pagar do reembolso para pagar o cliente e poder tirar dinheiro do caixa os das contas
        //verificar se o valor do reembolso é maior que zero

        $this->FaturaService_model->reembolso($faturaId, $motivoReembolso);
    }

    public function estornar($pagamentoId, $motivoEstorno) {
        $this->FaturaService_model->estornar($pagamentoId, $motivoEstorno);
    }

    public function cancelar($faturaId, $motivoCancelamento, $cancelarCobranca = true) {
        $this->FaturaService_model->cancelar($faturaId, $motivoCancelamento, $cancelarCobranca);
    }

    public function cancelar_com_multa($faturaId, $motivoCancelamento, $cancelarCobranca = true)
    {
        $this->FaturaService_model->cancelar_com_multa($faturaId, $motivoCancelamento, $cancelarCobranca);

    }
    public function cancelar_com_lancamento_credito($faturaId, $motivoCancelamento, $cancelarCobranca = true) {
        $this->FaturaService_model->cancelar_com_lancamento_credito($faturaId, $motivoCancelamento, $cancelarCobranca);
    }

    public function consultaSaldo() {
        echo $this->FaturaService_model->consultaSaldo();
    }

    public function getAllFaturasAbertas($integradora)
    {
        $this->db->where('integracao', $integradora);
        $this->db->where('(status = "ABERTA" OR status = "PENDENTE")');

        $q = $this->db->get('fatura_cobranca');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    
}
