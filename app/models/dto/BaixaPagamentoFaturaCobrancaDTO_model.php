<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaixaPagamentoFaturaCobrancaDTO_model extends CI_Model
{

    public $id;
    public $valorPago;
    public $dataPagamento;
    public $taxa;
    public $formaPagamento;
    public $status;
    public $observacao;
    public $dataCredito;

    public function __construct() {
        parent::__construct();
    }
}
