<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update319 extends CI_Migration {

    public function up() {

        $this->update_datas_table_parcela_fatura();

        //atualizar versao
        $this->db->update('settings',  array('version' => '2021.1.319'), array('setting_id' => 1));
    }

    function update_datas_table_parcela_fatura() {

        $this->db->select("fatura.id as faturaId,
            fatura.tipooperacao, 
            fatura.strreceitas,
            fatura.strdespesas,
            fatura.programacaoId");

        $this->db->from('fatura');

        $query = $this->db->get();
        $datas = $query->result();

        foreach ($datas as $data) {

            $product_name = '';

            if ($data->tipooperacao == 'CREDITO') {
                $product_name = $data->strreceitas;
            } else if ($data->tipooperacao == 'DEBITO'){
                $product_name = $data->strdespesas;
            }

            if ($data->programacaoId != null && $data->programacaoId > 0) {

                $agendamento = $this->getProgramacaoId($data->programacaoId);
                $product = $this->getProductByID($agendamento->produto);

                $product_name = $product->name.' '
                    .date('d/m/Y', strtotime($agendamento->dataSaida))
                    .'<br/>'.$product_name;
            }

            $update_fatura = array(
                'product_name' => $product_name ,
            );

            $this->edit('fatura', $update_fatura, 'id', $data->faturaId);
        }
    }

    public function getProgramacaoId($id)
    {
        $q = $this->db->get_where('agenda_viagem', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
        return FALSE;
    }

    public function down() {}
}
