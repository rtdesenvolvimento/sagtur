<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_variant'); ?></h4>
        </div>
        <?php echo form_open("system_settings/edit_variant/" . $variant->id); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("name"); ?></label>
                <div class="controls"> <?php echo form_input('name', $variant->name, 'class="form-control" id="name" required="required"'); ?> </div>
            </div>
            
            
            <div class="form-group">
                 	<?= lang("lancamento_type", "lancamento_type") ?>
                    <?php
                    $opts = array(
                        '1' => lang('lancamento_grupo') ,
                        '2' => lang('lancamento_individual'),
                    );
                     echo form_dropdown('lancamento_type', $opts, $variant->lancamento_type, 'class="form-control" id="lancamento_type" required="required"');
                     ?>
            </div>
            <div class="form-group">
	                <?= lang("usar_fornecedor", "usar_fornecedor") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                    echo form_dropdown('usar_fornecedor', $opts, $variant->usar_fornecedor, 'class="form-control" id="usar_fornecedor"');
                     ?>
            </div>
            
           <div class="form-group">
		            <div class="form-group">
		                <label class="control-label" for="customer_group"><?php echo $this->lang->line("default_suppliers_group"); ?></label>
	
	                <div class="controls"> <?php
	                	$cgs[] = '';
	                    foreach ($customer_groups as $customer_group) {
	                        $cgs[$customer_group->id] = $customer_group->name;
	                    }
	                    echo form_dropdown('customer_group', $cgs, $variant->group_id, 'class="form-control tip select" id="customer_group" style="width:100%;" ');
	                    ?>
	                </div>
            </div>
	            
            <div class="form-group">
  	                <?= lang("usar_localizacao", "usar_localizacao") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                    echo form_dropdown('usar_localizacao', $opts,$variant->usar_localizacao, 'class="form-control" id="usar_localizacao"');
                     ?>
            </div>
            
            <div class="form-group" style="display: none;">
  	                <?= lang("is_transporte", "is_transporte") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                    echo form_dropdown('is_transporte', $opts, $variant->is_transporte, 'class="form-control" id="is_transporte"');
                     ?>
            </div>

               <div class="form-group" style="display: none;">
                   <?= lang("planta", "planta") ?>
                   <div class="controls"> <?php
                       $pl[] = '';
                       foreach ($plantas as $planta) {
                           $pl[$planta->id] = $planta->name;
                       }
                       echo form_dropdown('planta', $pl, $variant->planta, 'class="form-control tip select" id="planta" style="width:100%;" ');
                       ?>
                   </div>
               </div>
            
            <div class="form-group" style="display: none;">
  	                <?= lang("transport_type", "transport_type") ?>
                    <?php
                    $opts = array('1' => lang('1_andar'), '2' => lang('2_andar') );
                    echo form_dropdown('transport_type', $opts, $variant->transport_type, 'class="form-control" id="transport_type"');
                     ?>
            </div>
	            
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_variant', lang('edit_variant'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>