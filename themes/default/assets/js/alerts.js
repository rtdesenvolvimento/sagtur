
function operacaoSucesso(texto, tempo) {

    if (tempo === undefined) tempo = 5000;

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": '1000',
        "timeOut": tempo,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr.options.background = "#ff0000";

    Command: toastr["success"](texto, "Sucesso!");
}

function operacaoInfo(texto, tempo) {
    Command: toastr["info"](texto, "Informa&ccedil;&otilde;es!");

    if (tempo === undefined) tempo = 5000;

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": '1000',
        "timeOut": tempo,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}

function operacaowarning(texto, tempo) {
    Command: toastr["warning"](texto, "Alerta!");

    if (tempo === undefined) tempo = 5000;

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "900",
        "hideDuration": '1000',
        "timeOut": tempo,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}

function operacaoerror(texto, tempo) {
    Command: toastr["error"](texto, "Alerta!");
    if (tempo === undefined) tempo = 5000;

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "900",
        "hideDuration": '1000',
        "timeOut": tempo,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}
