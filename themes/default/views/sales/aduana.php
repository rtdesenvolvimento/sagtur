<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<link href="<?= $assets ?>styles/helpers/bootstrap.min2.css" rel="stylesheet"/>
<link href="<?= $assets ?>styles/helpers/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="<?= $assets ?>styles/helpers/matrix-style.css" rel="stylesheet"/>
<link href="<?= $assets ?>styles/helpers/matrix-media.css" rel="stylesheet"/>

<style type="text/css">

    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 7px;
        color: #666;
        background: #ffffff;
        font-weight: bold;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 10px;
        text-align: left;
        vertical-align: top;
        border-top: 0;
    }

</style>

<?php
$contado = 1;
foreach ($itens_pedidos as $row) {

    if ($row->sale_status == 'cancel') continue;

    $customer_id 	    = $row->customerClient;
    $cliente 			= $this->site->getCompanyByID($customer_id);
    $data_aniversario	= $cliente->data_aniversario;
    $vat_no				= $cliente->cf1;
    $sexo				= $cliente->sexo;
    $country            = strtoupper ($cliente->country);
    $nomeCompleto 		= $cliente->name;
    $tipo_documento     = $cliente->tipo_documento; ?>
        <!--############################!-->
        <!--#### VIA DO CLIENTE ########!-->
        <!--############################!-->
        <div class="span7" style="float: left;border-right: 1px dotted;border-bottom: 1px dotted;">
            <div class="invoice-content" style="">
            <table class="table" style="margin-bottom: 0;">
                <tbody>
                <tr>

                    <td colspan="2" style="text-align: center;">
                        MERCOSUR/MERCOSUL<br/>
                        TARJETA DE ENTRADA/SALIDA<br/>
                        CART&Atilde;O DE ENTRADA/SA&Iacute;DA<br/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="table" style="margin-bottom: 0">
                            <tr>
                                <td style="border:1px solid black;height: 50%">
                                    SECUENCIA/SEQUENCIAL&nbsp;<br/>&nbsp;<br/>
                                </td>
                                <td style="border:1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        APELLIDO
                        Y NOMBRE-/ NOME COMPLETO
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table class="table" style="border:1px solid black;">
                            <?php


                            $partesNome = explode(" ", $nomeCompleto);
                            array_pop($partesNome);
                            $primeiroNome = implode(" ", $partesNome);
                            $totalLetrasSobraPrimeiroNome = 30 - strlen( $primeiroNome );

                            $ultimoNome = explode(" ", $nomeCompleto);
                            $ultimoNome = $ultimoNome[count($ultimoNome)-1];
                            $ultimoNome = substr($ultimoNome, 0, 30);
                            $totalLetrasSobraUltimoNome = 30 - strlen( $ultimoNome );
                            ?>

                            <tr>
                                <?php for($d=0;$d< strlen( $ultimoNome );$d++):
                                    $letra = mb_substr( $ultimoNome,$d,1,'UTF-8'); ?>
                                    <td style="border:1px solid black;text-align: center;"><?php echo $letra; ?></td>
                                <?php endfor;?>

                                <?php for($e=0;$e<=$totalLetrasSobraUltimoNome;$e++):?>
                                    <td style="border:1px solid black;text-align: center;">&nbsp;&nbsp;</td>
                                <?php endfor;?>
                            </tr>

                            <tr>
                                <?php for($d=0;$d< strlen( $primeiroNome );$d++):
                                    $letra = mb_substr( $primeiroNome,$d,1,'UTF-8'); ?>
                                    <td style="border:1px solid black;text-align: center;"><?php echo $letra; ?></td>
                                <?php endfor;?>

                                <?php for($e=0;$e<=$totalLetrasSobraPrimeiroNome;$e++):?>
                                    <td style="border:1px solid black;text-align: center;">&nbsp;&nbsp;</td>
                                <?php endfor;?>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table class="table" style="border:1px solid black;">
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    TIPO Y NUMERO DE DOCUMENTO<br/>
                                    TIPO E NUMERO DE DOCUMENTO <br/>
                                </td>
                                <td style="border:1px solid black;text-align: center;margin-top: 10px;"><?php echo $vat_no; ?></td>
                            </tr>
                            <tr>
                                <?php if($tipo_documento == 'rg') {?>
                                    <td style="text-align: justify;">
                                        &nbsp;&nbsp;&nbsp;1. [ X ] CEDULA DE IDENTIDAD<br/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CARTEIRA DE IDENTIDADE
                                    </td>
                                <?php } else { ?>
                                    <td style="text-align: justify;">
                                        &nbsp;&nbsp;&nbsp;1. [  ] CEDULA DE IDENTIDAD<br/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CARTEIRA DE IDENTIDADE
                                    </td>
                                <?php }?>
                                <td style="text-align: justify;">
                                    2. [ ] D.N.I.<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I.E./I.C.
                                </td>
                                <?php if($tipo_documento == 'passaporte') {?>
                                    <td style="text-align: justify;">
                                        3. [ X ] PASAPORTE<br/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PASSAPORTE
                                     </td>
                                <?php } else {?>
                                    <td style="text-align: justify;">
                                        3. [ ] PASAPORTE<br/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PASSAPORTE
                                    </td>
                                <?php }?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
                $dia 	= date('d', strtotime($data_aniversario) );
                $dia_0 = mb_substr( $dia,0,1,'UTF-8');
                $dia_1 = mb_substr( $dia,1,1,'UTF-8');

                $mes 	= date('m', strtotime($data_aniversario) );
                $mes_0 = mb_substr( $mes,0,1,'UTF-8');
                $mes_1 = mb_substr( $mes,1,1,'UTF-8');

                $ano 	= date('Y', strtotime($data_aniversario) );
                $ano_0 = mb_substr( $ano,2,1,'UTF-8');
                $ano_1 = mb_substr( $ano,3,1,'UTF-8');

                ?>
                <tr>
                    <td style="text-align: center;border:1px solid black;">
                        FECHA DE NACIMIENTO <br/>
                        DATA DE NASCIMENTO
                    </td>
                    <td style="text-align: center;border:1px solid black;">
                        DIA
                        MES
                        A&Ntilde;O <br/>
                        <?php echo '|'.$dia_0.'|'.$dia_1.'|'; ?>&nbsp;&nbsp;
                        <?php echo '|'.$mes_0.'|'.$mes_1.'|'; ?>&nbsp;&nbsp;
                        <?php echo '|'.$ano_0.'|'.$ano_1.'|'; ?>
                    </td>
                    <td style="text-align: center;border:1px solid black;">
                        SEXO<br/>
                        <?php if ($sexo == 'MASCULINO') {?>
                            1.[ X ] M &nbsp;&nbsp;2.[ ] F
                        <?php } else { ?>
                            1.[ ] M &nbsp;&nbsp;2.[ X ] F
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table class="table" style="border:1px solid black;">
                            <tr>
                                <td style="text-align: center;">NACIONALIDAD<br/>
                                    NACIONALIDADE
                                </td>
                            </tr>
                            <tr>
                                <?php if ($country == 'ARGENTINA'){?>
                                    <td style="text-align: center;">1. [ X ]<br/>ARGENTINA</td>
                                <?php } else { ?>
                                    <td style="text-align: center;">1. [&nbsp;]<br/>ARGENTINA</td>
                                <?php }?>

                                <?php if ($country == 'BRASIL'){?>
                                    <td style="text-align: center;">2. [ X ]<br/>BRASIL</td>
                                <?php } else { ?>
                                    <td style="text-align: center;">2. [&nbsp;]<br/>BRASIL</td>
                                <?php }?>

                                <?php if ($country == 'PARAGUAY'){?>
                                    <td style="text-align: center;">3. [ X ]<br/>PARAGUAY</td>
                                <?php } else { ?>
                                    <td style="text-align: center;">3. [&nbsp;]<br/>PARAGUAY</td>
                                <?php }?>

                                <?php if ($country == 'URUGUAY'){?>
                                    <td style="text-align: center;">4. [ X ]<br/>URUGUAY</td>
                                <?php } else { ?>
                                    <td style="text-align: center;">4. [&nbsp;]<br/>URUGUAY</td>
                                <?php }?>
                                <td style="text-align: center;">_________________<br/>OTRA/OUTRA</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table class="table" style="border:1px solid black;">
                            <tr>
                                <td style="text-align: center;">
                                    PAIS DE RESIDENCIA<br/>
                                    PAIS DE RESIDENCIA
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1. [&nbsp;]<br/>ARGENTINA</td>
                                <td style="text-align: center;">2. [ X ]<br/>BRASIL</td>
                                <td style="text-align: center;">3. [&nbsp;]<br/>PARAGUAY</td>
                                <td style="text-align: center;">4. [&nbsp;]<br/>URUGUAY</td>
                                <td style="text-align: center;">_________________<br/>OTRA/OUTRA</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border:1px solid black;height: 50%;text-align: center;">
                        <b><p>USO OFICIAL<p/>
                        <p>USO OFICIAL </p></b>
                    </td>
                    <td style="border:1px solid black;" >&nbsp;<br/><br/></td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>


<?php } ?>