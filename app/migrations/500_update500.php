<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update500 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '5.0.0'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'logo_shop_storage' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '' ),
            'slider1_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
            'slider2_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
            'slider3_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
