<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update496 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.9.6'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'enviar_notificacao_whatsapp' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
