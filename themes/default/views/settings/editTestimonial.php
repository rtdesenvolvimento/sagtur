<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_testimonial'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("site_settings/editTestimonial/" . $testimonial->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $testimonial->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('profession', 'profession'); ?>
                <?= form_input('profession', $testimonial->profession, 'class="form-control" id="profession" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("testimonial", "testimonial"); ?>
                <?php echo form_textarea('testimonial', $testimonial->testimonial, 'class="form-control" id="testimonial" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
            <div class="form-group">
                <?= lang("photo", "photo") ?>
                <input id="photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="photo" data-show-upload="false"
                       data-show-preview="false" accept="image/*" class="form-control file">
            </div>
            <?php echo form_hidden('id', $testimonial->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editar_testimonial', lang('editar_testimonial'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>