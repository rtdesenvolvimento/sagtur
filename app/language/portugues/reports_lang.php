<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Reports
 * Language: Brazilian Portuguese Language File
 *
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['profit_estimate'] 			= "Estimativa de Lucro";
$lang['warehouse_stock_heading'] 	= "Valor do Estoque da Viagem por custo e preço. Por favor selecione a Viagem à direita para obter o valor para a Viagem selecionado.";
$lang['alert_quantity'] 			= "Quant. de Alerta";
$lang['customize_report'] 			= "Por favor, personalizar o relatório abaixo";
$lang['start_date'] 				= "Data de Início";
$lang['end_date'] 					= "Data Final";
$lang['purchased_amount'] 			= "Quantia Despesada";
$lang['sold_amount'] 				= "Quantia Vendida";
$lang['profit_loss'] 				= "Lucro e/ou Perda";
$lang['daily_sales_report'] 		= "Relatório de Vendas Diárias";
$lang['reports_calendar_text'] 		= "Você pode alterar o mês, clicando no >> (próximo) ou << (anterior)";
$lang['monthly_sales_report'] 		= "Relatório de Vendas Mensal";
$lang['product_qty'] 				= "Viagem (Qtd.)";
$lang['payment_ref'] 				= "Ref. de Pagamento";
$lang['sale_ref'] 					= "Ref. de Passagem";
$lang['purchase_ref'] 				= "Ref. de Despesa";
$lang['paid_by'] 					= "Pago em";
$lang['view_report'] 				= "Ver Relatório";
$lang['sales_amount'] 				= "Quant. de Vendas";
$lang['total_paid'] 				= "Total Pago";
$lang['due_amount'] 				= "Valor Pagar";
$lang['total_sales'] 				= "Total de Vendas";
$lang['total_quotes'] 				= "Total de Cotação";
$lang['customer_sales_report'] 		= "Relatório de Vendas do Passageiro";
$lang['customer_payments_report'] 	= "Relatório de Pagamentos de Passageiros";
$lang['purchases_amount'] 			= "Quant. de Despesas";
$lang['total_purchases'] 			= "Total de Despesas";
$lang['view_report_customer'] 		= "Por favor, clique em Exibir relatório para verificar o relatório do Passageiro.";
$lang['view_report_supplier'] 		= "Por favor, clique em Exibir relatório para verificar o relatório de Fornecedor.";
$lang['view_report_staff'] 			= "Por favor, clique em Exibir relatório para verificar o relatório da Equipe.";
$lang['staff_purchases_report'] 	= "Despesas de Viagem da Equipe";
$lang['staff_sales_report'] 		= "Relatório de Vendas da Equipe";
$lang['staff_payments_report'] 		= "Relatório de Pagamentos da Equipe";
$lang['group'] 						= "Grupo";
$lang['staff_daily_sales'] 			= "Vendas Diárias";
$lang['staff_monthly_sales'] 		= "Vendas Mensais";
$lang['staff_logins_report'] 		= "Relatório de Entrada da Equipe";
$lang['add_customer'] 				= "Passageiro";
$lang['show_form'] 					= "Exibir Form.";
$lang['hide_form'] 					= "Ocultar Form.";
$lang['view_pl_report'] 			= "Por favor, veja o relatório de lucros e/ou perda e você pode selecionar o intervalo de datas personalizado para o relatório.";
$lang['payments_sent'] 				= "Pagamentos Enviados";
$lang['payments_received'] 			= "Pagamentos Recebidos";
$lang['cheque'] 					= "Cheque";
$lang['cash'] 						= "Dinheiro";
$lang['CC'] 						= "Cartão de Crédito";
$lang['paypal_pro'] 				= "Paypal Pro";
$lang['stripe'] 					= "Stripe";
$lang['cc_slips'] 					= "CC Slips";
$lang['total_cash'] 				= "Total em Dinheiro";
$lang['open_time'] 					= "Aberto em";
$lang['close_time'] 				= "Fechado em";
$lang['cash_in_hand'] 				= "Dinheiro em Caixa";
$lang['Cheques'] 					= "Cheques";
$lang['save_image']                 = "Salvar em Imagem";
$lang['total_quantity']             = "Total Qtd.";
$lang['total_items']                = "Total de Itens";
$lang['transfers_report']           = "Relatório de Transferências";
$lang['transfer_no']                = "Nº da Tranf.";
$lang['sales_return_report']        = "Relatório de Devoluções de Vendas";
$lang['payments_returned']          = "Pagamentos Extornados";
$lang['category_code']              = "Cód. da Categoria";
$lang['category_name']              = "Nome da Categoria";
$lang['total_returns']              = "Total de Reembolsos";
$lang['stock_in_hand']              = "Estoque (Qtd.)";
$lang['expenses_report']            = "Despesas Operacionais";
$lang['cheque_no']                  = "Nº do Cheque";
$lang['transaction_id']             = "Cód. da Transação";
$lang['card_no']                    = "Nº do Cartão (últimos 4 dígitos)";
$lang['net_sales']                  = "Vendas Líquidas";
$lang['net_purchases']              = "Despesas Líquidas";
$lang['subcategory']                = "Sub-categorias";
$lang['select_category_to_load']    = "Por favor selecione uma categoria para ser carregada";
$lang['select_subcategory']         = "Por favor selecione uma sub-categoria para ser carregada";
$lang['no_subcategory']             = "Esta categoria não possui sub-categorias";
$lang['daily_purchases_report']     = "Despesas de Viagem Diárias";
$lang['monthly_purchases_report']   = "Despesas de Viagem Mensais";

$lang['relatorio_financeiro_viagem_geral']      = "Relatório Contas a Receber de Viagens - Nível 1";
$lang['relatorio_forma_pagamento']              = "Pagamentos por Forma de Pagamento";
$lang['relatorio_receitas_passageiros']         = "Relatório de Contas a Receber por Passageiros";
$lang['relatorio_financeiro_forma_pagamento']   = "Relatório de Recebimentos por Forma de Pagamento - Nível 3";

$lang['tipo']                                   = "Tipo";
$lang['viagem']                                 = "Viagem";
$lang['saida']                                  = "Saída";
$lang['retorno']                                = "Retorno";
$lang['preco']                                  = "Preço";
$lang['tipo']                                   = "Tipo";
$lang['product_details'] 		                = "Sobre o Serviço";
$lang['preco_pacote']                           = "Pacote";
$lang['valor']                                  = "Valor Pagar";
$lang['pago']                                   = "Valor Pago";
$lang['valor_pagar']                            = "Pagar";
$lang['falta']                                  = "Valor a Receber";
$lang['data_saida']                             = "Data da Saída";
$lang['data_retorno']                           = "Data do Retorno";
$lang['poltrona']                               = "Polt.";
$lang['passageiro']                             = "Passageiro";
$lang['vencimento']                             = "Vencimento";
$lang['condicoes']                              = "Condições";
$lang['forma_pagamento']                        = "Forma de Pagamento";
$lang['cartao_credito']                         = "CARTÃO";
$lang['data_pagamento']                         = "Data do Pagamento";
$lang['atrasada']                               = "Atrasada";
$lang['todos']                                  = "Todos";
$lang['status_viagem']                          = "Status de Viagens";
$lang['status_pagamento']                       = "Status de Pagamento";
$lang['vencimento_de']                          = "Vencimento De";
$lang['vencimento_ate']                         = "Até";
$lang['data_pagamento_de']                      = "Data do pagamento De";
$lang['data_pagamento_ate']                     = "Até";
$lang['valor_de']                               = "Valor De";
$lang['valor_ate']                              = "Até";
$lang['cash_deposit']                           = "Dinheiro / Deposito";
$lang['somente_cash']                           = "Somente Dinheiro";
$lang['somente_deposit']                        = "Somente Deposito";
$lang['CC_other']                               = "Todos os Cartões";
$lang['somente_CC']                             = "Somente Cartão de Crédito";
$lang['somente_other']                          = "Somente Cartão de Débito";
$lang['outras_receitas']                        = "OUTRAS RECEITAS";
$lang['fornecedor']                             = "Fornecedor";
$lang['grupo']                                  = "Tipo de Despesa";
$lang['falta_pagar']                            = "Falta Pagar";

$lang['relatorio_financeiro_despesas_viagem_geral']     = "Relatório Contas a Pagar de Viagens - Nível 1";
$lang['relatorio_financeiro_despesas_fornecedores']     = "Relatório de Contas a Pagar de Fornecedores - Nível 2";
$lang['relatorio_financeiro_despesa_forma_pagamento']   = "Relatório de Pagamentos por Forma de Pagamento - Nível 3";
$lang['reports_relfinanceiroviagemnivel1']              = "Relatório de Contas a receber";
$lang['relFinanceiroDespesasViagemNivel1']              = "Relatório de Contas a pagar";
$lang['relatorio_comissao']                             = "Relatório de Comissão dos Vendedores";

#Status das parcelas no sistema financeiro
$lang['vencida']    = "Vencida";
$lang['ABERTA']     = "Pagar";
$lang['FATURADA']   = "Pagar";
$lang['QUITADA']    = "Pago";
$lang['PARCIAL']    = "Parcial";
$lang['ARQUIVADA']  = "Arquivada";
$lang['CANCELADA']  = "Cancelada";
$lang['REPARCELADA'] = "Renegociada";
$lang['REEMBOLSO'] = 'Reembolso';

$lang['dinheiro']                             = "Dinheiro";
$lang['boleto']                               = "Boleto bancário";
$lang['cartao']                               = "Cartão de Crédido";
$lang['contravale']                           = "Contra-Vale";
$lang['cheque']                               = "Cheque";
$lang['outros']                               = "Outros Pagamentos";
$lang['carne']                                = "Carnê";
$lang['boleto_cartao']                        = "Boleto e cartão de crédito";
$lang['carne']                                = "Carnê";
$lang['carne_boleto']                         = "Carnê em boletos";
$lang['carne_cartao']                         = "Carnê com cartão de crédito";
$lang['carne_boleto_cartao']                  = "Carnê Boleto e cartão de crédito";
$lang['credito_loja']                         = "Credito com a loja";

$lang['tipo_cobranca'] = "Forma";
$lang['pessoa'] = "Cliente";
$lang['categoria'] = "Categoria";
$lang['vencimento_financeiro'] = "Vencimento";
$lang['status'] = "Status";
$lang['pagar_financeiro'] = "Pagar";
$lang['recebido_financeiro'] = "Recebido";

$lang['financeiro'] = "Financeiro";
$lang['gerenciamento_financeiro'] = "Gerenciamento Financeiro";
$lang['contas_receber'] = "Contas a Receber";
$lang['contas_pagar'] = "Contas a Pagar";
$lang['historico_pagamentos'] = "Histórico de Pagamentos";
$lang['historico_recebimentos'] = "Histórico de Recebimentos";
$lang['pagamento_com'] = "Pagamento com";
$lang['saldo_das_contas'] = "Saldo das contas";
$lang['adicionar_conta_receber'] = "Adicionar Contas a Receber";
$lang['adicionar_conta_pagar'] = "Adicionar Contas a Pagar";
$lang['relatorio_financeiro_conta_receber'] = "Cobranças";
$lang['cancelar_contas_receber'] = "Cancelar Contas por Seleção";

$lang['cliente_conta'] = "Pessoa";
$lang['receita'] = "Categoria";
$lang['data_vencimento'] = "Data do Primeiro Vencimento";
$lang['tipo_cobranca_financeiro'] = "Tipo de Cobrança";
$lang['valor_vencimento'] = "Valor do Vencimento";
$lang['pagar_agora'] = "Pagar agora";
$lang['valor_entrada'] = "Valor da entrada";
$lang['data_pagamento_entrada'] = "Data do pagamento";
$lang['conta_destino'] = "Conta de Destino";
$lang['parcelas'] = "Parcelas";

$lang['renegociar_conta_receber'] = "Renegociar Contas a Receber";
$lang['selecione_parcelas_reparcelar_info'] = "Selecine as parcelas que serão renegociadas com o cliente";
$lang['renegociar_parcelas_selecionadas'] = "Clique aqui para continuar com a negociação";
$lang['selecionar_parcelas'] = "Clique aqui para exibir as parcelas e voltar para a seleção";
$lang['confirmar_renegociacao'] = "Confirmar renegociação";

$lang['negociar_divida'] = "Renegociar toda dívida do cliente";
$lang['negociar_divida_contrato'] = "Renegociar apenas esta dívida";

$lang['valor_vencimento'] = 'Valor vencimento';
$lang['multa'] = "Multa";
$lang['juros'] = "Juros";
$lang['desconto'] = "Desconto";

$lang['historico_de_parcelas']  = "Ver o Histórico das Parcelas";
$lang['segunda_via']            = "Gerar 2º Via";

$lang['valor_vencimento'] = "Valor do Vencimento";
$lang['info_historico_fatura'] = "Histórico de Parcelas da Conta";
$lang['ver_fatura_gerada'] = "Ver Fatura Gerada";
$lang['ver_historico_pagamento'] = "Ver Histórico de Pagamento";
$lang['ver_observacao_fatura'] = "Ver Observações da Fatura";

$lang['contas'] = "Contas";
$lang['contas_saldo_dia_anterior'] = "Saldo do dia anterior";
$lang['contas_entradas'] = "Entradas";
$lang['contas_saidas'] = "Saídas";
$lang['contas_saldo'] = "Saldo";
$lang['contas_saldo_bloqueado'] = "Saldo bloqueado";
$lang['contas_saldo_disponivel'] = "Saldo disponível";

$lang['adicionar_transferencia'] = "Adicionar Transferência";
$lang['adicionar_saque'] = "Adicionar Saque";
$lang['adicioar_deposito'] = "Adicionar Depósito";

$lang['movimentador'] = "Conta";
$lang['data_vencimento_de'] = "Vencimento de";
$lang['data_vencimento_ate'] = "Até";
$lang['motivo_cancelamento'] = "Motivo do cancelamento";
$lang['numero_documento']   = "Nº documento";
$lang['acres'] = 'Acres.';
$lang['desc'] = 'Desc.';
$lang['editar_fatura'] = 'Editar Parcela';
$lang['fatura_editada_com_sucesso_gerado_link_cobranca'] = "Parcela editada com sucesso! <br/>Gerado automaticamente 2º Via do boleto ou link de pagamento do cartão.<br/>";
$lang['fatura_editada_com_sucesso'] = 'Parcela editada com sucesso!';
$lang['taxas'] = 'Taxas';
$lang['view_payments'] 			= "Ver Pagamentos";
$lang['ver_observacoes_pagamento'] = "Ver observações do pagamento";
$lang['tipo_cobranca_reembolso'] = "Tipo de pagamento";
$lang['motivo_reembolso'] = "Motivo do reembolso";
$lang['financeiro_adicionado'] = "Conta a receber adicionada com sucesso!";
$lang['add_payment'] 			= "Adicionar Pagamento";
$lang['pago_financeiro']        = "Recebido";
$lang['valor_pagamento']        = "Valor do Pagamento";
$lang['despesa']                = "Despesa";
$lang['pago_financeiro']        = "Pago";
$lang['nascimento']             = "Nascimento";
$lang['lista_aniversario']      = "Lista de Aniversariantes";
$lang['mesAniversario']         = "Mês";

$lang['relatorio_financeiro_passageiros']       = "Relatório Geral de Viagem";
$lang['todas_as_contas'] = "Todas as Contas";
$lang['info_extrato_bancario'] = "Extrato Bancário";

$lang['roomlist'] = "Configuração de sua Rooming List";
$lang['print_list_room_list'] = "Imprimir Rooming List";
$lang['adicionar_room_list'] = "Adicionar Novo Rooming List";
$lang['room_list_hotel'] = "Nome do Hotel";
$lang['room_list_adicionado_com_sucesso'] = "Rooming List Adicionado com Sucesso";
$lang['create_new_tipo_hospedagem'] = "Clique e Crie um Novo Quarto";
$lang['info_arrastar_puxar_room_list'] = "Puxe e Arraste o Hospede para dentro do Quarto";
$lang['editar_room_list'] = 'Editar Rooming List';
$lang['excluir_room_list'] = "Excluir Rooming List";

$lang['send_message'] = "Abrir Conversa no WhatsApp";
$lang['whatsapp_send'] = "Abrir Conversa com Cliente no WhatsApp";

$lang['start_date_payment'] = 'Data do Pagamento De';
$lang['end_date_payment'] = 'Até';

$lang['start_date_sales'] = 'Data da Venda De';
$lang['end_date_sales'] = 'Até';

$lang['last_purchase_date'] = "Última Compra";
$lang['shopping'] = 'Compras';
$lang['reports_products_by_customers'] = 'Data da Última Compra por Passageiro';

$lang['dias_ultima_compra'] = 'Clientes Que Não Compraram Nós Ultimos';
$lang['start_date_trip'] = 'Data da Viagem De';
$lang['end_date_trip'] = 'Até';

$lang['product_names'] = 'Pacotes';
$lang['qtd'] = 'Qtd';
$lang['nasc'] = 'Nasc:.';

$lang['birthday_of'] = 'Idade De';
$lang['birthday_from'] = 'Até';
$lang['total_por_idade'] = 'Total de Passageiros Por Idade';
$lang['total_por_sexo'] = 'Total de Passageiros Por Sexo';
$lang['popular_destinations'] = 'Destinos Mais Populares';
$lang['popular_destinations_group_date'] = 'Destinos Mais Populares por Mês | Último 12 Meses';

$lang['edit_sale']= "Editar Venda";
$lang['email_sale'] = 'Enviar E-mail da Venda';
$lang['data_venda_de'] = 'Data do Cancelamento';
$lang['sale_status'] 			= "Status";
