<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Commission_model extends CI_Model
{

    const COMISSION_PENDENTE = 'Pendente';
    const COMISSION_CANCELADA = 'Cancelada';

    public $reference_no;
    public $status;

    public $subtotal = 0.00;
    public $total_commission = 0.00;

    public $sale_id;

    //log
    public $date;

    //log de usuarios
    public $created_by;
    public $updated_by;

    //log de datas
    public $created_at;
    public $updated_at;

    public function __construct() {
        parent::__construct();
    }

}
