
<style media="screen">
    #PayRDataTable td:nth-child(1) {text-align: center;width: 10%;}
    #PayRDataTable td:nth-child(2) {text-align: center;}
    #PayRDataTable td:nth-child(3) {text-align: center;}
    #PayRDataTable td:nth-child(4) {width: 30%;}
    #PayRDataTable td:nth-child(5) {text-align: center;}
    #PayRDataTable td:nth-child(6) {text-align: right;}
    #PayRDataTable td:nth-child(7) {width: 5%;}

</style>


<?php
$v = "";
/* if($this->input->post('name')){
  $v .= "&name=".$this->input->post('name');
} */
if ($this->input->post('payment_ref')) {
    $v .= "&payment_ref=" . $this->input->post('payment_ref');
}
if ($this->input->post('sale_ref')) {
    $v .= "&sale_ref=" . $this->input->post('sale_ref');
}

if ($this->input->post('supplier')) {
    $v .= "&supplier=" . $this->input->post('supplier');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('customer')) {
    $v .= "&customer=" . $this->input->post('customer');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('cheque')) {
    $v .= "&cheque=" . $this->input->post('cheque');
}
if ($this->input->post('tid')) {
    $v .= "&tid=" . $this->input->post('tid');
}
if ($this->input->post('card')) {
    $v .= "&card=" . $this->input->post('card');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}

if ($this->input->post('start_date_sales')) {
    $v .= "&start_date_sales=" . $this->input->post('start_date_sales');
}
if ($this->input->post('end_date_sales')) {
    $v .= "&end_date_sales=" . $this->input->post('end_date_sales');
}

if ($this->input->post('filter_programacao_id')) {
    $v .= "&programacao_id=" . $this->input->post('filter_programacao_id');
}

?>
<script>
    $(document).ready(function () {
        var pb = <?= json_encode($pb); ?>;
        function paid_by(x) {
            return x;
        }

        function ref(x) {
            return (x != null) ? x : ' - ';
        }

        function row_status(x) {

            if(x == null) {
                return '';
            } else if(x == 'sent') {
                return '<div class="text-center"><span class="label label-danger">'+lang[x]+'</span></div>';
            } else if(x == 'received') {
                return '<div class="text-center"><span class="label label-success">'+lang[x]+'</span></div>';
            }

        }

        var oTable = $('#PayRDataTable').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('reports/getPaymentsReport/?v=1' . $v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"mRender": fld},
                {"mRender": ref},
                {"mRender": ref},
                {"mRender": ref},
                {"mRender": paid_by}, {
                "mRender": currencyFormat},
                {"mRender": row_status},
                {"bVisible": false}
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[7];
                nRow.className = "invoice_link";
                if (aData[6] == 'sent') {
                    nRow.className = "invoice_link warning";
                } else if (aData[6] == 'returned') {
                    nRow.className = "invoice_link danger";
                }
                return nRow;
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var total = 0;
                for (var i = 0; i < aaData.length; i++) {
                    if (aaData[aiDisplay[i]][6] == 'sent' || aaData[aiDisplay[i]][6] == 'returned')
                        total -= parseFloat(aaData[aiDisplay[i]][5]);
                    else
                        total += parseFloat(aaData[aiDisplay[i]][5]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(total));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('payment_ref');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('sale_ref');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('products');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('paid_by');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
        ], "footer");

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();
        <?php if ($this->input->post('biller')) { ?>
        $('#rbiller').select2({ allowClear: true });
        <?php } ?>
        <?php if ($this->input->post('supplier')) { ?>
        $('#rsupplier').val(<?= $this->input->post('supplier') ?>).select2({
            minimumInputLength: 1,
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= site_url('suppliers/getSupplier') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
        $('#rsupplier').val(<?= $this->input->post('supplier') ?>);
        <?php } ?>
        <?php if ($this->input->post('customer')) { ?>
        $('#rcustomer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= site_url('customers/getCustomer') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('payments_report'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            } ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>">
                        <i class="icon fa fa-file-pdf-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                        <i class="icon fa fa-file-excel-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                        <i class="icon fa fa-file-picture-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('customize_report'); ?></p>

                <div id="form">

                    <?php echo form_open("reports/payments"); ?>
                    <div class="row">
                        <div>
                            <div class="col-sm-10">
                                <?= lang("product", "product"); ?>
                                <?php
                                $pgs[""] = lang('select').' '.lang('product');
                                echo form_dropdown('filter_programacao_id', $pgs,  '', 'class="form-control" id="filter_programacao_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary" style="width: 100%;margin-top: 25px;" data-toggle="modal" data-target="#filterModal"><i class="fa fa-search"></i> <?=$this->lang->line("filter_product");?></button>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang("payment_ref", "payment_ref"); ?>
                                <?php echo form_input('payment_ref', (isset($_POST['payment_ref']) ? $_POST['payment_ref'] : ""), 'class="form-control tip" id="payment_ref"'); ?>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang("sale_ref", "sale_ref"); ?>
                                <?php echo form_input('sale_ref', (isset($_POST['sale_ref']) ? $_POST['sale_ref'] : ""), 'class="form-control tip" id="sale_ref"'); ?>

                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="rcustomer"><?= lang("customer"); ?></label>
                                <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'class="form-control" id="rcustomer" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("customer") . '"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="rbiller"><?= lang("biller"); ?></label>
                                <?php
                                $bl[''] = '';
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="rbiller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("supplier", "rsupplier"); ?>
                                <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'class="form-control" id="rsupplier" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("supplier") . '"'); ?>
                            </div>
                        </div>

                        <div class="col-sm-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("transaction_id", "tid"); ?>
                                <?php echo form_input('tid', (isset($_POST['tid']) ? $_POST['tid'] : ""), 'class="form-control" id="tid"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("card_no", "card"); ?>
                                <?php echo form_input('card', (isset($_POST['card']) ? $_POST['card'] : ""), 'class="form-control" id="card"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("cheque_no", "cheque"); ?>
                                <?php echo form_input('cheque', (isset($_POST['cheque']) ? $_POST['cheque'] : ""), 'class="form-control" id="cheque"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-sm-4" style="display: none;">
                            <div class="form-group">
                                <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                <?php
                                $us[""] = lang('select').' '.lang('user');
                                foreach ($users as $user) {
                                    $us[$user->id] = $user->first_name . " " . $user->last_name;
                                }
                                echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("start_date_sales", "start_date_sales"); ?>
                                <?php echo form_input('start_date_sales', (isset($_POST['start_date_sales']) ? $_POST['start_date_sales'] : ""), 'class="form-control date" id="start_date_sales"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("end_date_sales", "end_date_sales"); ?>
                                <?php echo form_input('end_date_sales', (isset($_POST['end_date_sales']) ? $_POST['end_date_sales'] : ""), 'class="form-control date" id="end_date_sales"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("start_date_payment", "start_date"); ?>
                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control date" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("end_date_payment", "end_date"); ?>
                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control date" id="end_date"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div
                            class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table id="PayRDataTable"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th><?= lang("date"); ?></th>
                            <th><?= lang("payment_ref"); ?></th>
                            <th><?= lang("sale_ref"); ?></th>
                            <th><?= lang("product"); ?></th>
                            <th><?= lang("paid_by"); ?></th>
                            <th><?= lang("amount"); ?></th>
                            <th><?= lang("type"); ?></th>
                            <th><?= lang("id"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Filter Pacote-->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filtros de Pacote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('filter_status_pacote', $opts,  'Confirmado', 'class="form-control" id="filter_status_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('filter_ano_pacote', $opts,  date('Y'), 'class="form-control" id="filter_ano_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('filter_mes_pacote', $opts,  date('m'), 'class="form-control" id="filter_mes_pacote"'); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button"  id="applyFilters" class="btn btn-primary">Aplicar Filtros</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        createFilter('filters');
        buscarProdutos();

        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/pdf/?v=1'.$v)?>";
            return false;
        });

        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/0/xls/?v=1'.$v)?>";
            return false;
        });

        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });

        $('#applyFilters').click(function() {
            buscarProdutos();
        });

    });

    function createFilter(nameClasse) {
        $('.'+nameClasse).click(function() {
            if ($('.div'+nameClasse).is(':visible')) {
                $('.div'+nameClasse).hide(300);
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            } else {
                $('.div'+nameClasse).show(300).fadeIn();
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
            }
        });
    }

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#filter_status_pacote').val(),
                ano: $('#filter_ano_pacote').val(),
                mes: $('#filter_mes_pacote').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {

                $('#filter_programacao_id').empty();
                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#filter_programacao_id').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#filter_programacao_id').append(option);
                });

                $('#filter_programacao_id').select2({minimumResultsForSearch: 7});
                $('#filterModal').modal('hide');
            }
        });
    }

</script>