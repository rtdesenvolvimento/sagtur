<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update393 extends CI_Migration {

    public function up() {

        $this->create_table_analytical_access_record();

        $this->db->update('settings',  array('version' => '3.9.3'), array('setting_id' => 1));
    }


    public function create_table_analytical_access_record() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'ip_address' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),
            'sessions_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'page_web' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'page_current' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'server_name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'server_port' => array('type' => 'VARCHAR', 'constraint' => '30', 'null' => TRUE ),
            'server_signature' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'server_software' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'http_referer' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'http_method' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),
            'http_response_code' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),
            'browser_information_name' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),
            'browser_information_version' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),
            'operating_system_information_name' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),
            'information_cookies' => array('type' => 'VARCHAR', 'constraint' => '999', 'null' => TRUE ),
            'user_agent_info' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),
            'information_session' => array('type' => 'VARCHAR', 'constraint' => '999', 'null' => TRUE ),
            'request_time' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),

            'type' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'product_id' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),
            'programacao_id' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE ),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('analytical_access_record', TRUE, $attributes);
    }

    public function down() {}
}
