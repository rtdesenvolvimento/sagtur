<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#e58800"/>

    <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>toastr/toastr.css" rel="stylesheet"/>

    <!--
    <link rel="manifest" href="<?=base_url();?>manifest.json"/>
    !-->

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <?php if ($Settings->user_rtl) { ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () { $('.pull-right, .pull-left').addClass('flip'); });
        </script>
    <?php } ?>
    <script type="text/javascript">
        $(window).load(function () {
            $("#loading").fadeOut("slow");
        });
    </script>
    <!--
    <script>
        if (typeof navigator.serviceWorker !== 'undefined') {
            navigator.serviceWorker.register('pwabuilder-sw.js', { scope: '/' }).then(() => {
                console.log('Service Worker registrado com sucesso.');
            }).catch(error => {
                console.log('Service Worker falhou:', error);
            });
        }
    </script>
    !-->
</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div id="loading"></div>
<div id="app_wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <!--
            <a class="navbar-brand" href="<?= site_url() ?>">
            	<span class="logo">
            		<?php echo '<img src="' . base_url('assets/uploads/logos/logo.png') . '" alt="' . $Settings->site_name .'" style="margin-bottom:10px;width: 225px;height:30px;" />' ;?>
            	</span>
            </a>
            !-->
            <style>
                .i-logo {
                    margin-right: 0px;
                }
            </style>
            <a class="navbar-brand" href="<?= site_url() ?>"><span class="logo"><i class="fa fa-globe fa-spin i-logo"></i>  <?= $Settings->site_name ?></span></a>
            <div class="btn-group visible-xs pull-right btn-visible-sm">
                <button class="navbar-toggle btn" type="button" data-toggle="collapse" data-target="#sidebar_menu">
                    <span class="fa fa-bars"></span>
                </button>
                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="btn">
                    <span class="fa fa-user"></span>
                </a>
                <a href="<?= site_url('logout'); ?>" class="btn">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? site_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>" class="mini_avatar img-rounded">
                            <div class="user">
                                <span><?= lang('welcome') ?> <?= $this->session->userdata('nome_completo'); ?></span>
                            </div>
                        </a>
                        <?php if ($Owner || $Admin || $GP['sales-index'] || $GP['checkins-index']) { ?>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>">
                                    <i class="fa fa-user"></i> <?= lang('profile'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"><i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= site_url('logout'); ?>">
                                    <i class="fa fa-sign-out"></i> <?= lang('logout'); ?>
                                </a>
                            </li>
                        </ul>
                        <?php } ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-xs"><a class="btn tip" title="<?= lang('dashboard') ?>" data-placement="bottom" href="<?= site_url('welcome') ?>"><i class="fa fa-home"></i></a></li>
                    <?php if ($Owner) { ?>
                        <li class="dropdown hidden-sm" style="display: none;">
                            <a class="btn tip" title="<?= lang('settings') ?>" data-placement="bottom" href="<?= site_url('system_settings') ?>">
                                <i class="fa fa-cogs"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn bdarkGreen  tip" title="<?= lang('shop') ?>" data-placement="bottom" target="_blank" href="<?= $this->Settings->url_site_domain.'/'.$this->session->userdata('biller_id') ?>">
                            <i class="fa fa-shopping-cart"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calculator') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                            <i class="fa fa-calculator"></i>
                        </a>
                        <ul class="dropdown-menu pull-right calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                        <?php if ($info) { ?>
                            <li class="dropdown hidden-sm">
                                <a class="btn tip" title="<?= lang('notifications') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                    <i class="fa fa-info-circle"></i> Recados
                                    <span class="number blightOrange black"><?= sizeof($info) ?></span>
                                </a>
                                <ul class="dropdown-menu pull-right content-scroll">
                                    <li class="dropdown-header"><i class="fa fa-info-circle"></i> <?= lang('notifications'); ?></li>
                                    <li class="dropdown-content">
                                        <div class="scroll-div">
                                            <div class="top-menu-scroll">
                                                <ol class="oe">
                                                    <?php foreach ($info as $n) {
                                                        echo '<li>' . $n->comment . '</li>';
                                                    } ?>
                                                </ol>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>

                        <li class="dropdown hidden-xs">
                            <a class="btn tip" title="<?= lang('detalhes_servico') ?>" data-placement="bottom" href="<?= site_url('agenda/consulta_disponibilidade') ?>">
                                <i class="fa fa-calendar-plus-o"></i> <?= lang('Agenda'); ?>
                            </a>
                        </li>
                    <?php } ?>


                    <!--
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('check_reservations') ?>" data-placement="bottom" href="<?= site_url('reservations/agenda_reservas') ?>">
                            <i class="fa fa-calendar"></i> <?= lang('check_reservations'); ?>
                        </a>
                    </li>
                    !-->

                    <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                        <?php if ($events) { ?>
                            <li class="dropdown hidden-xs">
                                <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                    <i class="fa fa-calendar"></i> <?= lang('commitments') ?>
                                    <span class="number blightOrange black"><?= sizeof($events) ?></span>
                                </a>
                                <ul class="dropdown-menu pull-right content-scroll">
                                    <li class="dropdown-header">
                                    <i class="fa fa-calendar"></i> <?= lang('upcoming_events'); ?>
                                    </li>
                                    <li class="dropdown-content">
                                        <div class="top-menu-scroll">
                                            <ol class="oe">
                                                <?php foreach ($events as $event) {
                                                    echo '<li>' . date($dateFormats['php_ldate'], strtotime($event->start)) . ' <strong>' . $event->title . '</strong><br>'.$event->description.'</li>';
                                                } ?>
                                            </ol>
                                        </div>
                                    </li>
                                    <li class="dropdown-footer">
                                        <a href="<?= site_url('calendar') ?>" class="btn-block link">
                                            <i class="fa fa-calendar"></i> <?= lang('commitments'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } else { ?>
                        <li class="dropdown hidden-xs">
                            <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="<?= site_url('calendar') ?>">
                                <i class="fa fa-calendar"></i> <?= lang('commitments') ?>
                            </a>
                        </li>
                        <?php } ?>
                    <?php } ?>
                    <!--
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="<?= site_url('calendar') ?>">
                            <i class="fa fa-tasks" style="font-size: 14px;"></i> tarefas
                            <span class="number blightGrey black">2</span>
                        </a>
                    </li>
                    !-->
                    <?php if (($Owner || $Admin || $GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts']) &&
                            ($qty_alert_sale_cancel > 0 || $qty_alert_overdue_bills > 0 || $qty_number_of_accounts_winning_per_week > 0 || $get_number_of_accounts_winning_per_day > 0 )) { ?>

                        <li class="dropdown hidden-sm">
                            <a class="btn bred tip" title="<?= lang('alerts') ?>"
                               data-placement="left" data-toggle="dropdown" href="#">
                                <i class="fa fa-exclamation-triangle"></i> Alertas do Dia!<span class="number blightOrange black"><?=($qty_alert_sale_cancel + $qty_alert_overdue_bills + $qty_number_of_accounts_winning_per_week);?></span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="<?= site_url('reports/sales_cancel_alerts') ?>">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_sale_cancel; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('sales_cancel_alerts_day') ?></span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= site_url('reports/overdue_bills_alerts') ?>">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_overdue_bills; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('overdue_bills_alerts_month') ?></span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= site_url('reports/number_of_accounts_winning_per_week') ?>">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_number_of_accounts_winning_per_week; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('number_of_accounts_winning_per_week') ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('reports/number_of_accounts_winning_per_day') ?>">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $get_number_of_accounts_winning_per_day; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('number_of_accounts_winning_per_day') ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--
                            <li class="dropdown hidden-sm" style="display: none;">
                            <a class="btn blightOrange tip" title="<?= lang('alerts') ?>"
                               data-placement="left" data-toggle="dropdown" href="#">
                                <i class="fa fa-exclamation-triangle"></i> Alertas!
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                        <span style="padding-right: 35px;">Contratos vencidos</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                        <span style="padding-right: 35px;">Vendas de hoje</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('quantity_alerts') ?></span>
                                    </a>
                                </li>
                                <?php if ($Settings->product_expiry) { ?>
                                    <li>
                                        <a href="<?= site_url('reports/expiry_alerts') ?>" class="">
                                            <span class="label label-danger pull-right" style="margin-top:3px;"><?= $exp_alert_num; ?></span>
                                            <span style="padding-right: 35px;"><?= lang('expiry_alerts') ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        !-->
                    <?php  } ?>

                    <li class="dropdown hidden-sm">
                        <a class="btn tip" title="<?= lang('styles') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <i class="fa fa-css3"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="bwhite noPadding">
                                <a href="#" id="fixed" class="">
                                    <i class="fa fa-angle-double-left"></i>
                                    <span id="fixedText">Fixed</span>
                                </a>
                                <a href="#" id="cssLight" class="grey">
                                    <i class="fa fa-stop"></i> Grey
                                </a>
                                <a href="#" id="cssBlue" class="blue">
                                    <i class="fa fa-stop"></i> Blue
                                </a>
                                <a href="#" id="cssBlack" class="black">
                                   <i class="fa fa-stop"></i> Black
                               </a>
                           </li>
                        </ul>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('language') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <img src="<?= base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php $scanned_lang_dir = array_map(function ($path) {
                                return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                            foreach ($scanned_lang_dir as $entry) { ?>
                                <li>
                                    <a href="<?= site_url('welcome/language/' . $entry); ?>">
                                        <img src="<?= base_url(); ?>assets/images/<?= $entry; ?>.png" class="language-img">
                                        &nbsp;&nbsp;<?= ucwords($entry); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= site_url('welcome/toggle_rtl') ?>">
                                    <i class="fa fa-align-<?=$Settings->user_rtl ? 'right' : 'left';?>"></i>
                                    <?= lang('toggle_alignment') ?>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <?php if ($_SERVER['SERVER_ADDR'] == '10.100.43.176') {?>
                    <li class="dropdown hidden-sm">
                        <a class="btn blightOrange tip" data-placement="bottom" data-container="body">
                            PC48
                        </a>
                    </li>
                    <?php } else if ($_SERVER['SERVER_ADDR'] == '10.100.31.17') { ?>
                        <li class="dropdown hidden-sm">
                            <a class="btn blightOrange tip" data-placement="bottom" data-container="body">
                                PC49
                            </a>
                        </li>
                    <?php } else {?>
                        <li class="dropdown hidden-sm">
                            <a class="btn blightOrange tip" data-placement="bottom" data-container="body">
                                LC
                            </a>
                        </li>
                    <?php } ?>

                    <?php if ($Owner && $Settings->update) { ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn blightOrange tip" title="<?= lang('update_available') ?>"
                            data-placement="bottom" data-container="body" href="<?= site_url('system_settings/updates') ?>">
                            <i class="fa fa-download"></i>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if ($Owner) { ?>
                        <li class="dropdown" style="display: none;">
                            <a class="btn bdarkGreen tip" id="today_profit" title="<?= lang('today_profit') ?>"
                                data-placement="bottom" data-html="true" href="<?= site_url('reports/profit') ?>"
                                data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-hourglass-2"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($Owner || $Admin) { ?>
                            <!--
						<li class="dropdown hidden-xs" style="display: none;">
							<a class="btn bred tip" title="<?= lang('clear_ls') ?>" data-placement="bottom" id="clearLS" href="#">
								<i class="fa fa-eraser"></i>
							</a>
						</li>
						!-->
                    <?php } ?>
                </ul>
            </div>
        </div>
    </header>

    <div class="container" id="container">
        <div class="row" id="main-con">
        <table class="lt"><tr><td class="sidebar-con">
            <div id="sidebar-left">

                <style>
                    .profile_img {
                        padding: 17px;
                        width: 200px;
                    }
                </style>
                <div class="profile clearfix" style="text-align: center;">
                    <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" alt="<?php echo $Settings->site_name ;?>" class="profile_img">
                </div>

                 <div class="sidebar-nav nav-collapse collapse navbar-collapse" id="sidebar_menu">
                    <ul class="nav main-menu">
                        <li class="mm_welcome">
                            <a href="<?= site_url() ?>">
                                <i class="fa fa-home"></i>
                                <span class="text"> <?= lang('dashboard'); ?></span>
                            </a>
                        </li>
                        <?php if ($Owner || $Admin) {?>

                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-map"></i>
                                    <span class="text"> <?= lang('products'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= site_url('products'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_add">
                                        <a class="submenu" href="<?= site_url('products/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_product'); ?></span>
                                        </a>
                                    </li>
                                    <li id="agenda_index">
                                        <a class="submenu" href="<?= site_url('agenda'); ?>">
                                            <i class="fa fa-calendar-o"></i>
                                            <span class="text"> <?= lang('detalhes_servico'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_arquivados">
                                        <a class="submenu" href="<?= site_url('products/arquivados'); ?>">
                                            <i class="fa fa-archive"></i>
                                            <span class="text"> <?= lang('arquivados'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_locais_embarque">
                                        <a href="<?= site_url('products/locais_embarque') ?>">
                                            <i class="fa fa-bus"></i><span class="text"> <?= lang('locais_embarque'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_tipos_hospedagem">
                                        <a href="<?= site_url('products/tipos_hospedagem') ?>">
                                            <i class="fa fa-bed"></i><span class="text"> <?= lang('tipos_hospedagem'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_servicos_incluso">
                                        <a href="<?= site_url('products/servicos_incluso') ?>">
                                            <i class="fa fa-plus-square-o"></i><span class="text"> <?= lang('servicos_incluso'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_tipostransporte">
                                        <a href="<?= site_url('products/tipostransporte') ?>">
                                            <i class="fa fa-bus"></i><span class="text"> <?= lang('tipos_transporte'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_valores_faixa">
                                        <a href="<?= site_url('products/valores_faixa') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('faixa_valor'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_categories">
                                        <a href="<?= site_url('products/categories') ?>">
                                            <i class="fa fa-folder-open"></i><span class="text"> <?= lang('categories'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_cupons">
                                        <a href="<?= site_url('products/cupons') ?>">
                                            <i class="fa fa-ticket"></i><span class="text"> <?= lang('cupom_desconto'); ?></span>
                                        </a>
                                    </li>

                                    <li id="products_meios_divulgacao">
                                        <a href="<?= site_url('products/meios_divulgacao') ?>">
                                            <i class="fa fa-bullhorn"></i><span class="text"> <?= lang('meios_divulgacao'); ?></span>
                                        </a>
                                    </li>

                                    <li id="products_planta" style="display: none;">
                                        <a href="<?= site_url('products/planta') ?>">
                                            <i class="fa fa-map-pin"></i><span class="text"> <?= lang('plantas'); ?></span>
                                        </a>
                                    </li>
                                    <li id="fluxo_caixa" style="display: none;">
                                        <a class="submenu" href="<?= site_url('products/fluxo_caixa'); ?>">
                                            <i class="fa fa-usd"></i>
                                            <span class="text"> <?= lang('fluxo_caixa'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_tours" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-map-signs"></i>
                                    <span class="text"> <?= lang('tours'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="tours_index">
                                        <a class="submenu" href="<?= site_url('tours'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_tours'); ?></span>
                                        </a>
                                    </li>
                                    <li id="tours_add">
                                        <a class="submenu" href="<?= site_url('tours/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_tour'); ?></span>
                                        </a>
                                    </li>

                                    <li id="tour_tour">
                                        <a class="submenu" href="<?= site_url('tours/tour'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_tour'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_servicoadicional">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-plus-circle"></i>
                                    <span class="text"> <?= lang('Adicionais'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="servicoadicional_index">
                                        <a class="submenu" href="<?= site_url('ServicoAdicional'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_servicos_adicional'); ?></span>
                                        </a>
                                    </li>
                                    <li id="servicoadicional_add">
                                        <a class="submenu" href="<?= site_url('ServicoAdicional/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_servico_adicional'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_transfer" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-car"></i>
                                    <span class="text"> <?= lang('transfer'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                </ul>
                            </li>
                            <li class="mm_transfer" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bus"></i>
                                    <span class="text"> <?= lang('charters'); ?> <span class="label label-info" style="margin-left: 5rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                </ul>
                            </li>
                            <li class="mm_transfer" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-ticket"></i>
                                    <span class="text"> <?= lang('tickets'); ?> <span class="label label-info" style="margin-left: 6rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                            </li>
                            <li class="mm_location mm_vehicle" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-car"></i>
                                    <span class="text"> <?= lang('location'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="location_index">
                                        <a class="submenu" href="<?= site_url('location'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('location_list'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_add">
                                        <a class="submenu" href="<?= site_url('location/add'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_new_location'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_location_reminders">
                                        <a class="submenu" href="<?= site_url('location/location_reminders'); ?>">
                                            <i class="fa fa-calendar-plus-o"></i>
                                            <span class="text"> <?= lang('location_reminders'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_availability_panel">
                                        <a class="submenu" href="<?= site_url('location/availability_panel'); ?>">
                                            <i class="fa fa-search-plus"></i>
                                            <span class="text"> <?= lang('availability_panel'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_availability_panel">
                                        <a class="submenu" href="<?= site_url('location/availability_searh'); ?>">
                                            <i class="fa fa-calendar"></i>
                                            <span class="text"> <?= lang('availability_searh'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_vehicle_fleet_panel">
                                        <a class="submenu" href="<?= site_url('location/booking_map'); ?>">
                                            <i class="fa fa-map"></i>
                                            <span class="text"> <?= lang('booking_map'); ?></span>
                                        </a>
                                    </li>
                                    <li id="location_vehicle_fleet_panel">
                                        <a class="submenu" href="<?= site_url('location/vehicle_fleet_panel'); ?>">
                                            <i class="fa fa-list-ol"></i>
                                            <span class="text"> <?= lang('vehicle_fleet_panel'); ?></span>
                                        </a>
                                    </li>
                                    <li id="vehicle_index">
                                        <a class="submenu" href="<?= site_url('vehicle'); ?>">
                                            <i class="fa fa-list-alt"></i>
                                            <span class="text"> <?= lang('list_vehicle'); ?></span>
                                        </a>
                                    </li>
                                    <li id="vehicle_add">
                                        <a class="submenu" href="<?= site_url('vehicle/add'); ?>">
                                            <i class="fa fa-car"></i>
                                            <span class="text"> <?= lang('add_vehicle'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_agenda">
                                <a class="submenu" href="<?= site_url('agenda/consulta_disponibilidade'); ?>">
                                    <i class="fa fa-calendar"></i><span class="text"> <?= lang('schedule'); ?></span>
                                </a>
                            </li>

                            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'settings' ? '' : 'mm_pos' ?> mm_salesutil">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-heart"></i>
                                    <span class="text"> <?= lang('sales'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="salesutil_salesfilter">
                                        <a class="submenu" href="<?= site_url('salesutil/salesFilter/all/Confirmado'); ?>">
                                            <i class="fa fa-heart"></i>
                                            <span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="salesutil_sale_items">
                                        <a class="submenu" href="<?= site_url('salesutil/sale_items'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('sale_by_items'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= site_url('sales/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_sale'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_canceled">
                                        <a class="submenu" href="<?= site_url('sales/canceled'); ?>">
                                            <i class="fa fa-trash-o"></i>
                                            <span class="text"> <?= lang('list_sales_canceled'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_archived_sales">
                                        <a class="submenu" href="<?= site_url('sales/archived_sales'); ?>">
                                            <i class="fa fa-archive"></i>
                                            <span class="text"> <?= lang('list_sales_archived'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!--
                            <li class="mm_agenda">
                                <a class="dropmenu" href="<?= site_url('agenda/consulta_disponibilidade'); ?>">
                                    <i class="fa fa-calendar"></i>
                                    <span class="text"> <?= lang('schedule'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="agenda_consulta_disponibilidade" style="display: none;">
                                        <a class="submenu" href="<?= site_url('agenda/consulta_disponibilidade'); ?>">
                                            <i class="fa fa-calendar-plus-o"></i>
                                            <span class="text"> <?= lang('consulta_disponibilidade'); ?></span>
                                        </a>
                                    </li>

                                    <li id="agenda_index" style="display: none;">
                                        <a class="submenu" href="<?= site_url('tasks'); ?>">
                                            <i class="fa fa-tasks"></i>
                                            <span class="text"> <?= lang('tasks'); ?></span>
                                        </a>
                                    </li>
                                    <li id="agenda_index" style="display: none;">
                                        <a class="submenu" href="<?= site_url('tasks'); ?>">
                                            <i class="fa fa-comment"></i>
                                            <span class="text"> <?= lang('conversations'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            !-->

                            <?php if ($this->Settings->receptive){?>
                            <li class="mm_reservations">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-map-signs"></i>
                                    <span class="text"> <?= lang('reservations'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;"></span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="reservations_index">
                                        <a class="submenu" href="<?= site_url('reservations'); ?>">
                                            <i class="fa fa-ticket"></i>
                                            <span class="text"> <?= lang('list_reservations'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reservations_reserve_items">
                                        <a class="submenu" href="<?= site_url('reservations/reserve_items'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_reservations_item'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reservations_agenda_reservas">
                                        <a class="submenu" href="<?= site_url('reservations/agenda_reservas'); ?>">
                                            <i class="fa fa-calendar"></i>
                                            <span class="text"> <?= lang('agenda_reserva'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reservations_reservations_os">
                                        <a class="submenu" href="<?= site_url('reservations/reservations_os'); ?>">
                                            <i class="fa fa-cogs"></i>
                                            <span class="text"> <?= lang('list_reservations_os'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php } ?>

                            <li class="mm_contracts mm_folders mm_contract_settings">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-archive"></i>
                                    <span class="text"> <?= lang('contracts'); ?> <span class="label label-info" style="margin-left: 5rem;font-size: 65%;background-color: #428BCA;">BETA</span></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="contracts_index">
                                        <a class="submenu" href="<?= site_url('contracts'); ?>">
                                            <i class="fa fa-files-o"></i>
                                            <span class="text"> <?= lang('my_documents'); ?></span>
                                        </a>
                                    </li>
                                    <li id="folders_index">
                                        <a class="submenu" href="<?= site_url('folders'); ?>">
                                            <i class="fa fa-folder-open-o"></i>
                                            <span class="text"> <?= lang('my_folders'); ?></span>
                                        </a>
                                    </li>
                                    <li id="contracts_contracts">
                                        <a class="submenu" href="<?= site_url('contracts/contracts'); ?>">
                                            <i class="fa fa-file"></i>
                                            <span class="text"> <?= lang('template_contracts'); ?></span>
                                        </a>
                                    </li>
                                    <li id="contract_settings_index">
                                        <a class="submenu" href="<?= site_url('contract_settings'); ?>">
                                            <i class="fa fa fa-cogs"></i>
                                            <span class="text"> <?= lang('contract_settings'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <?php
                            $plugsignSetting = $this->site->getPlugsignSettings();

                            if ($plugsignSetting->active && $plugsignSetting->token) {
                                $plugsign = true;
                            } else {
                                $plugsign = false;
                            }
                            ?>
                            <?php if ($plugsign) {?>
                                <!--
                                <li class="mm_messagegroups">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-whatsapp"></i>
                                        <span class="text"> <?= lang('message_groups'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="messagegroups_index">
                                            <a class="submenu" href="<?= site_url('messagegroups'); ?>">
                                                <i class="fa fa-files-o"></i>
                                                <span class="text"> <?= lang('list_message_groups'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                !-->
                            <?php } ?>

                            <li class="mm_purchases"  style="display:none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="purchases_index">
                                        <a class="submenu" href="<?= site_url('purchases'); ?>">
                                            <i class="fa fa-star"></i>
                                            <span class="text"> <?= lang('list_purchases'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_add">
                                        <a class="submenu" href="<?= site_url('purchases/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_purchase'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_return_purchases">
                                        <a class="submenu" href="<?= site_url('purchases/return_purchases'); ?>">
                                            <i class="fa fa-reply"></i>
                                            <span class="text"> <?= lang('list_return_purchases'); ?></span>
                                        </a>
                                    </li>
									<li id="purchases_expenses">
                                        <a class="submenu" href="<?= site_url('purchases/expenses'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('Outras Despesas'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_add_expense">
                                        <a class="submenu" href="<?= site_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('Adicionar Outras Despesas'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Financeiro !-->
                            <li class="mm_financeiro mm_refunds mm_contapagar mm_contareceber mm_faturas mm_financeiroutil">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-usd"></i>
                                    <span class="text"> <?= lang('financeiro'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="contareceber_index">
                                        <a class="submenu" href="<?= site_url('contareceber'); ?>">
                                            <i class="fa fa-arrow-circle-up"></i>
                                            <span class="text"> <?= lang('contas_receber'); ?></span>
                                        </a>
                                    </li>
                                    <li id="contapagar_index">
                                        <a class="submenu" href="<?= site_url('contapagar'); ?>">
                                            <i class="fa fa-arrow-circle-down"></i>
                                            <span class="text"> <?= lang('contas_pagar'); ?></span>
                                        </a>
                                    </li>

                                    <li id="financeiro_extrato_financeiro">
                                        <a class="submenu" href="<?= site_url('financeiro/extrato_financeiro'); ?>">
                                            <i class="fa fa-table"></i>
                                            <span class="text"> <?= lang('extrato_financeiro'); ?></span>
                                        </a>
                                    </li>

                                    <li id="refunds_index">
                                        <a class="submenu" href="<?= site_url('refunds'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_refunds'); ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="submenu" href="<?= site_url('refunds/add_refund_card'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_refund_card'); ?></span>
                                        </a>
                                    </li>

                                    <li id="financeiro_extrato">
                                        <a class="submenu" href="<?= site_url('financeiro/extrato'); ?>">
                                            <i class="fa fa-money"></i>
                                            <span class="text"> <?= lang('extrato'); ?></span>
                                        </a>
                                    </li>

                                    <li id="financeiro_contas">
                                        <a href="<?= site_url('financeiro/contas') ?>">
                                            <i class="fa fa-bank"></i><span class="text"> <?= lang('cadastro_contas'); ?></span>
                                        </a>
                                    </li>
                                    <li id="financeiroutil_tiposcobranca">
                                        <a href="<?= site_url('financeiroutil/tiposcobranca') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('tipos_cobranca'); ?></span>
                                        </a>
                                    </li>
                                    <li id="financeiroutil_taxas">
                                        <a href="<?= site_url('financeiroutil/taxas') ?>">
                                            <i class="fa fa-usd"></i><span class="text"> <?= lang('configurar_taxas_tipos_cobranca'); ?></span>
                                        </a>
                                    </li>
                                    <li id="financeiro_plano_receitas">
                                        <a href="<?= site_url('financeiro/plano_receitas') ?>">
                                            <i class="fa fa-list"></i><span class="text"> <?= lang('plano_receitas'); ?></span>
                                        </a>
                                    </li>
                                    <li id="financeiro_plano_despesas">
                                        <a href="<?= site_url('financeiro/plano_despesas') ?>">
                                            <i class="fa fa-list"></i><span class="text"> <?= lang('plano_despesas'); ?></span>
                                        </a>
                                    </li>
                                    <!--
                                    <li id="mm_financeiro">
                                        <a class="submenu" href="<?= site_url('financeiro/comissao'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('pagamento_comissao'); ?></span>
                                        </a>
                                    </li>
                                    <li id="mm_financeiro">
                                        <a class="submenu" href="<?= site_url('financeiro/pagamentoFornecedor'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('pagamento_fornecedor'); ?></span>
                                        </a>
                                    </li>
                                    <li id="mm_financeiro">
                                        <a class="submenu" href="<?= site_url('financeiro/pagamentoReceptivo'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('pagamento_receptivo'); ?></span>
                                        </a>
                                    </li>
                                    <li id="mm_financeiro">
                                        <a class="submenu" href="<?= site_url('financeiro/receitasDURAV'); ?>">
                                            <i class="fa fa-money"></i>
                                            <span class="text"> <?= lang('recebimento_fornecedor'); ?></span>
                                        </a>
                                    </li>
                                    !-->
                                    <li id="mm_financeiro" style="display: none;">
                                        <a class="submenu" href="<?= site_url('financeiro/saldo'); ?>">
                                            <i class="fa fa-university"></i>
                                            <span class="text"> <?= lang('saldo_em_conta'); ?></span>
                                        </a>
                                    </li>
                                    <li id="mm_financeiro">
                                        <a class="submenu" style="display: none;" href="<?= site_url('financeiro/fluxocaixadiario'); ?>">
                                            <i class="fa fa-exchange"></i>
                                            <span class="text"> <?= lang('fluxo_caixa'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <?php if ($this->Settings->emitir_nota_fiscal) {?>
                                <!-- Notas Fiscais !-->
                                <li class="mm_notafiscal">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-leaf"></i>
                                        <span class="text"> <?= lang('notasfiscal'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="notafiscal_agendamento">
                                            <a class="submenu" href="<?= site_url('notafiscal/agendamento'); ?>">
                                                <i class="fa fa-list"></i>
                                                <span class="text"> <?= lang('notasfiscal_agendamento'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <!-- Comissões !-->
                            <li class="mm_commissions">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-paypal"></i>
                                    <span class="text"> <?= lang('commissions'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="commissions_index">
                                        <a class="submenu" href="<?= site_url('commissions'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_commissions'); ?></span>
                                        </a>
                                    </li>
                                    <li id="commissions_fechamentos">
                                        <a class="submenu" href="<?= site_url('commissions/fechamentos'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('fechamento_comissao'); ?></span>
                                        </a>
                                    </li>
                                    <li id="commissions_payment_commissions">
                                        <a class="submenu" href="<?= site_url('commissions/payment_commissions'); ?>">
                                            <i class="fa fa-paypal"></i>
                                            <span class="text"> <?= lang('payment_commissions'); ?></span>
                                        </a>
                                    </li>
                                    <li id="commissions_approved">
                                        <a class="submenu" href="<?= site_url('commissions/approved'); ?>">
                                            <i class="fa fa-user-plus"></i>
                                            <span class="text"> <?= lang('list_approved_commissions'); ?></span>
                                        </a>
                                    </li>
                                    <li id="commissions_report_commissions_paid">
                                        <a class="submenu" href="<?= site_url('commissions/report_commissions_paid'); ?>">
                                            <i class="fa fa fa-pie-chart"></i>
                                            <span class="text"> <?= lang('report_commissions_paid'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- End Comissões !-->

                            <li class="mm_itinerary">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-car"></i><span class="text"> <?= lang('itineraries'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="itinerary_index">
                                        <a class="submenu" href="<?= site_url('itinerary'); ?>">
                                            <i class="fa fa-map-marker"></i>
                                            <span class="text"> <?= lang('orders_service'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_gifts">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-credit-card"></i>
                                    <span class="text"> <?= lang('gifts'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="gifts_index">
                                        <a class="submenu" href="<?= site_url('gifts'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_gifts'); ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="submenu" href="<?= site_url('gifts/add_gift_card'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_gift_card'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_checkins">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-qrcode"></i>
                                    <span class="text"> <?= lang('checkins'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="checkins_entrance">
                                        <a class="submenu" href="<?= site_url('checkins'); ?>">
                                            <i class="fa fa-qrcode"></i>
                                            <span class="text"> <?= lang('add_checkins'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php if ($this->Settings->usar_captacao) {?>
                                <li class="mm_captacao mm_departments">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-comments-o"></i>
                                        <span class="text"> <?= lang('captacoes'); ?></span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="captacao_index">
                                            <a class="submenu" href="<?= site_url('captacao'); ?>">
                                                <i class="fa fa-leaf"></i>
                                                <span class="text"> <?= lang('list_captacao'); ?></span>
                                            </a>
                                        </li>
                                        <li id="departments_index">
                                            <a class="submenu" href="<?= site_url('departments/index'); ?>">
                                                <i class="fa fa-qrcode"></i>
                                                <span class="text"> <?= lang('add_departments'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                            <li class="mm_commissions" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-user-plus"></i>
                                    <span class="text"> <?= lang('afiliados'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="commissions_index">
                                        <a class="submenu" href="<?= site_url('afiliados'); ?>">
                                            <i class="fa fa-qrcode"></i>
                                            <span class="text"> <?= lang('list_afiliados'); ?></span>
                                        </a>
                                    </li>
                                    <li id="afiliados_index">
                                        <a class="submenu" href="<?= site_url('afiliados/add'); ?>">
                                            <i class="fa fa-qrcode"></i>
                                            <span class="text"> <?= lang('add_afiliado'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_auth mm_customers mm_suppliers mm_billers mm_vendedores">
                                <a class="dropmenu" href="#">
                                <i class="fa fa-users"></i>
                                <span class="text"> <?= lang('people'); ?> </span>
                                <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($Owner) { ?>
                                    <li id="auth_users">
                                        <a class="submenu" href="<?= site_url('users'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_users'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= site_url('customers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>

                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= site_url('suppliers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_suppliers'); ?></span>
                                        </a>
                                    </li>

                                    <?php if ($Owner) { ?>
                                        <li id="vendedores_index">
                                            <a class="submenu" href="<?= site_url('vendedores'); ?>">
                                                <i class="fa fa-users"></i><span class="text"> <?= lang('list_billers'); ?></span>
                                            </a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </li>

                            <li class="mm_reports">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="reports_index">
                                        <a href="<?= site_url('reports') ?>" style="display: none;">
                                            <i class="fa fa-bars"></i><span class="text"> <?= lang('overview_chart'); ?></span>
                                        </a>
                                    </li>

                                    <li id="reports_warehouse_stock">
                                        <a href="<?= site_url('reports/warehouse_stock') ?>"  style="display: none;">
                                            <i class="fa fa-building"></i><span class="text"> <?= lang('warehouse_stock'); ?></span>
                                        </a>
                                    </li>
                                    <?php if (POS) { ?>
                                    <li id="reports_register">
                                        <a href="<?= site_url('reports/register') ?>"  style="display: none;">
                                            <i class="fa fa-th-large"></i><span class="text"> <?= lang('register_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="reports_quantity_alerts">
                                        <a href="<?= site_url('reports/quantity_alerts') ?>"  style="display: none;">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($Settings->product_expiry) { ?>
                                    <li id="reports_expiry_alerts">
                                        <a href="<?= site_url('reports/expiry_alerts') ?>"  style="display: none;">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="reports_products" style="display: none;">
                                        <a href="<?= site_url('reports/products') ?>">
                                            <i class="fa fa-bus"></i><span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>

                                    <li id="reports_aniversario">
                                        <a href="<?= site_url('reports/aniversario') ?>">
                                            <i class="fa fa-birthday-cake"></i><span class="text"> <?= lang('aniversario_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_products_by_customers">
                                        <a href="<?= site_url('reports/products_by_customers') ?>">
                                            <i class="fa fa-heart"></i><span class="text"> <?= lang('products_by_customers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_overdue_bills_alerts">
                                        <a href="<?= site_url('reports/overdue_bills_alerts') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('overdue_bills'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales_cancel_alerts">
                                        <a href="<?= site_url('reports/sales_cancel_alerts') ?>">
                                            <i class="fa fa-remove"></i><span class="text"> <?= lang('sales_cancel'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_categories" style="display: none;">
                                        <a href="<?= site_url('reports/categories') ?>">
                                            <i class="fa fa-folder-open"></i><span class="text"> <?= lang('categories_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_sales" style="display: none;">
                                        <a href="<?= site_url('reports/daily_sales') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales" style="display: none;">
                                        <a href="<?= site_url('reports/monthly_sales') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales">
                                        <a href="<?= site_url('reports/sales') ?>">
                                            <i class="fa fa-ticket"></i><span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_relfinanceiroviagemnivel1">
                                        <a href="<?= site_url('reports/relFinanceiroViagemNivel1') ?>" style="display: none;">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('receitas_viagem'); ?></span>
                                        </a>
                                    </li>

                                    <li id="reports_payments">
                                        <a href="<?= site_url('reports/payments') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_profit_loss">
                                        <a href="<?= site_url('reports/profit_loss') ?>" style="display: none;">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('profit_and_loss'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_purchases">
                                        <a href="<?= site_url('reports/daily_purchases') ?>"  style="display: none;">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_purchases">
                                        <a href="<?= site_url('reports/monthly_purchases') ?>"  style="display: none;">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_relfinanceirodespesasviagemnivel1">
                                        <a href="<?= site_url('reports/relFinanceiroDespesasViagemNivel1') ?>" style="display: none;">
                                            <i class="fa fa-dollar"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_purchases">
                                        <a href="<?= site_url('reports/purchases') ?>"  style="display: none;">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_expenses">
                                        <a href="<?= site_url('reports/expenses') ?>"  style="display: none;">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_relatoriocomissao">
                                        <a href="<?= site_url('reports/relatoriocomissao') ?>" style="display: none;">
                                            <i class="fa fa-dollar"></i><span class="text"> <?= lang('relatorio_comissao'); ?></span>
                                        </a>
                                    </li>

                                    <li id="reports_customers">
                                        <a href="<?= site_url('reports/customers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_suppliers">
                                        <a href="<?= site_url('reports/suppliers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_users">
                                        <a href="<?= site_url('reports/users') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('staff_report'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php if ($Owner) { ?>

                                <li class="mm_bus">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa fa-bus"></i><span class="text"> <?= lang('bus'); ?></span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="bus_index">
                                            <a href="<?= site_url('bus') ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('bus'); ?></span>
                                            </a>
                                        </li>
                                        <li id="bus_acompanhar_marcacao" style="display: none;">
                                            <a href="<?= site_url('bus/acompanhar_marcacao') ?>">
                                                <i class="fa fa-check-circle"></i><span class="text"> <?= lang('acompanhar_marcacao'); ?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="submenu" href="<?= site_url('bus/addBus'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                                <i class="fa fa-plus-circle"></i>
                                                <span class="text"> <?= lang('newBus'); ?></span>
                                            </a>
                                        </li>


                                        <li id="bus_cobranca_extra_assento">
                                            <a href="<?= site_url('bus/cobranca_extra_assento') ?>">
                                                <i class="fa fa-money"></i><span class="text"> <?= lang('cobranca_extra'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="mm_site_settings mm_shop_settings">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa fa-television"></i><span class="text"> <?= lang('site_settings'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="shop_settings_index">
                                            <a href="<?= site_url('shop_settings') ?>">
                                                <i class="fa fa-cog"></i><span class="text"> <?= lang('shop_settings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="shop_settings_menus">
                                            <a href="<?= site_url('shop_settings/menus') ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('shop_settings_menus'); ?></span>
                                            </a>
                                        </li>
                                        <li id="shop_settings_pages">
                                            <a href="<?= site_url('shop_settings/pages') ?>">
                                                <i class="fa fa-list-alt"></i><span class="text"> <?= lang('shop_settings_pages'); ?></span>
                                            </a>
                                        </li>
                                        <li id="site_settings_testimonial">
                                            <a href="<?= site_url('site_settings/testimonial') ?>">
                                                <i class="fa fa-comments"></i><span class="text"> <?= lang('testimonials'); ?></span>
                                            </a>
                                        </li>
                                        <li id="site_settings_team">
                                            <a href="<?= site_url('site_settings/team') ?>">
                                                <i class="fa fa-users"></i><span class="text"> <?= lang('team'); ?></span>
                                            </a>
                                        </li>
                                        <li id="site_settings_gallery">
                                            <a href="<?= site_url('site_settings/gallery') ?>">
                                                <i class="fa fa-photo"></i><span class="text"> <?= lang('gallery'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="mm_system_settings mm_commission_settings <?= strtolower($this->router->fetch_method()) != 'settings' ? '' : 'mm_pos' ?>">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa fa-cogs"></i><span class="text"> <?= lang('settings'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="system_settings_index">
                                            <a href="<?= site_url('system_settings') ?>">
                                                <i class="fa fa-cog"></i><span class="text"> <?= lang('system_settings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="commission_settings_index">
                                            <a href="<?= site_url('commission_settings') ?>">
                                                <i class="fa fa-cog"></i><span class="text"> <?= lang('commission_settings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_change_logo">
                                            <a href="<?= site_url('system_settings/change_logo') ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-upload"></i><span class="text"> <?= lang('change_logo'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_currencies">
                                            <a href="<?= site_url('system_settings/currencies') ?>" style="display: none;">
                                                <i class="fa fa-money"></i><span class="text"> <?= lang('currencies'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_customer_groups">
                                            <a href="<?= site_url('system_settings/customer_groups') ?>">
                                                <i class="fa fa-chain"></i><span class="text"> <?= lang('customer_groups'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_whatsapp_settings">
                                            <a href="<?= site_url('system_settings/whatsapp_settings') ?>">
                                                <i class="fa fa-whatsapp"></i><span class="text"> <?= lang('whatsapp_settings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_expense_categories">
                                            <a href="<?= site_url('system_settings/expense_categories') ?>" style="display: none;">
                                                <i class="fa fa-folder-open"></i><span class="text"> <?= lang('expense_categories'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_variants">
                                            <a href="<?= site_url('system_settings/variants') ?>" style="display: none;">
                                                <i class="fa fa-tags"></i><span class="text"> <?= lang('variants'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_tax_rates">
                                            <a href="<?= site_url('system_settings/tax_rates') ?>" style="display: none;">
                                                <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('tax_rates'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_warehouses">
                                            <a href="<?= site_url('system_settings/warehouses') ?>" style="display: none;">
                                                <i class="fa fa-building-o"></i><span class="text"> <?= lang('warehouses'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_email_templates" style="display: none;">
                                            <a href="<?= site_url('system_settings/email_templates') ?>">
                                                <i class="fa fa-envelope"></i><span class="text"> <?= lang('email_templates'); ?></span>
                                            </a>
                                        </li>

                                        <li id="system_settings_user_groups">
                                            <a href="<?= site_url('system_settings/user_groups') ?>" style="display: none;">
                                                <i class="fa fa-key"></i><span class="text"> <?= lang('group_permissions'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_backups" style="display: none;">
                                            <a href="<?= site_url('system_settings/backups') ?>">
                                                <i class="fa fa-database"></i><span class="text"> <?= lang('backups'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_updates" style="display: none;">
                                            <a href="<?= site_url('system_settings/updates') ?>">
                                                <i class="fa fa-upload"></i><span class="text"> <?= lang('updates'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($this->Settings->avaliar) {?>
                                <li class="mm_ratings">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-star"></i>
                                        <span class="text"> <?= lang('ratings'); ?></span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="ratings_index">
                                            <a class="submenu" href="<?= site_url('ratings'); ?>">
                                                <i class="fa fa-star"></i>
                                                <span class="text"> <?= lang('list_ratings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="ratings_responses">
                                            <a class="submenu" href="<?= site_url('ratings/responses'); ?>">
                                                <i class="fa fa-star"></i>
                                                <span class="text"> <?= lang('list_responses'); ?></span>
                                            </a>
                                        </li>
                                        <li id="ratings_questions">
                                            <a class="submenu" href="<?= site_url('ratings/questions'); ?>">
                                                <i class="fa fa-question-circle"></i>
                                                <span class="text"> <?= lang('list_questions'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <li class="mm_graphic">
                                <a class="submenu" href="<?= site_url('graphic'); ?>">
                                    <i class="fa fa fa-pie-chart"></i><span class="text"> <?= lang('graphics'); ?></span>
                                </a>
                            </li>
                            <li class="mm_notifications">
                                <a class="submenu" href="<?= site_url('notifications'); ?>">
                                    <i class="fa fa fa-info"></i><span class="text"> <?= lang('notifications'); ?></span>
                                </a>
                            </li>

                            <li class="mm_account">
                                <a class="submenu" href="<?= site_url('account'); ?>">
                                    <i class="fa fa fa-tasks"></i><span class="text"> <?= lang('account'); ?></span>
                                </a>
                            </li>
                        <?php
                        } else { // not owner and not admin
                            ?>
                            <?php if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode']) { ?>
                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bus"></i>
                                    <span class="text"> <?= lang('products'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= site_url('products'); ?>">
                                            <i class="fa fa-map-signs"></i><span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['products-add']) { ?>
										<li id="products_add">
											<a class="submenu" href="<?= site_url('products/add'); ?>">
												<i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_product'); ?></span>
											</a>
										</li>
                                    <?php } ?>


                                    <?php if ($GP['products-edit']) { ?>
										<li id="products_quantity_adjustments">
											<a class="submenu" href="<?= site_url('products/quantity_adjustments'); ?>">
												<i class="fa fa-filter"></i><span class="text"> <?= lang('quantity_adjustments'); ?></span>
											</a>
										</li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards'] || $GP['sales-return_sales']) { ?>
                                <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'settings' ? '' : 'mm_pos' ?> mm_salesutil">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-ticket"></i>
                                        <span class="text"> <?= lang('sales'); ?>
                                        </span> <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="salesutil_salesFilter">
                                            <a class="submenu" href="<?= site_url('salesutil/salesFilter/all/Confirmado'); ?>">
                                                <i class="fa fa-ticket"></i><span class="text"> <?= lang('list_sales'); ?></span>
                                            </a>
                                        </li>
                                        <li id="salesutil_sale_items">
                                            <a class="submenu" href="<?= site_url('salesutil/sale_items'); ?>">
                                                <i class="fa fa-list"></i>
                                                <span class="text"> <?= lang('sale_by_items'); ?></span>
                                            </a>
                                        </li>
                                        <?php if ($GP['sales-add']) { ?>
                                            <li id="sales_add">
                                                <a class="submenu" href="<?= site_url('sales/add'); ?>">
                                                    <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_sale'); ?></span>
                                                </a>
                                            </li>
                                            <li id="sales_add">
                                                <a class="submenu" href="<?= site_url('sales/canceled'); ?>">
                                                    <i class="fa fa-trash-o"></i>
                                                    <span class="text"> <?= lang('list_sales_canceled'); ?></span>
                                                </a>
                                            </li>
                                            <li id="sales_archived_sales">
                                                <a class="submenu" href="<?= site_url('sales/archived_sales'); ?>">
                                                    <i class="fa fa-archive"></i>
                                                    <span class="text"> <?= lang('list_sales_archived'); ?></span>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($GP['sales-deliveries']) { ?>
                                        <li id="sales_deliveries">
                                            <a class="submenu" href="<?= site_url('sales/deliveries'); ?>">
                                                <i class="fa fa-truck"></i><span class="text"> <?= lang('deliveries'); ?></span>
                                            </a>
                                        </li>
                                        <?php }
                                        if ($GP['sales-gift_cards']) { ?>
                                        <li id="sales_gift_cards">
                                            <a class="submenu" href="<?= site_url('sales/gift_cards'); ?>">
                                                <i class="fa fa-gift"></i><span class="text"> <?= lang('gift_cards'); ?></span>
                                            </a>
                                        </li>
                                        <?php }
                                        if ($GP['sales-return_sales']) { ?>
                                        <li id="sales_return_sales">
                                            <a class="submenu" href="<?= site_url('sales/return_sales'); ?>">
                                                <i class="fa fa-reply"></i><span class="text"> <?= lang('list_return_sales'); ?></span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <?php if ($this->Settings->receptive){?>
                                    <li class="mm_reservations">
                                        <a class="dropmenu" href="#">
                                            <i class="fa fa-map-signs"></i>
                                            <span class="text"> <?= lang('reservations'); ?> <span class="label label-info" style="margin-left: 7rem;font-size: 65%;background-color: #428BCA;"></span></span>
                                            <span class="chevron closed"></span>
                                        </a>
                                        <ul>
                                            <li id="reservations_index">
                                                <a class="submenu" href="<?= site_url('reservations'); ?>">
                                                    <i class="fa fa-ticket"></i>
                                                    <span class="text"> <?= lang('list_reservations'); ?></span>
                                                </a>
                                            </li>
                                            <li id="reservations_reserve_items">
                                                <a class="submenu" href="<?= site_url('reservations/reserve_items'); ?>">
                                                    <i class="fa fa-list"></i>
                                                    <span class="text"> <?= lang('list_reservations_item'); ?></span>
                                                </a>
                                            </li>
                                            <li id="reservations_agenda_reservas">
                                                <a class="submenu" href="<?= site_url('reservations/agenda_reservas'); ?>">
                                                    <i class="fa fa-calendar"></i>
                                                    <span class="text"> <?= lang('agenda_reserva'); ?></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>

                                <!-- Comissões !-->
                                <li class="mm_commissions">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-paypal"></i>
                                        <span class="text"> <?= lang('commissions'); ?> <span class="label label-info" style="margin-left: 5rem;font-size: 65%;background-color: #428BCA;">NOVO</span></span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="commissions_index">
                                            <a class="submenu" href="<?= site_url('commissions'); ?>">
                                                <i class="fa fa-list"></i>
                                                <span class="text"> <?= lang('list_commissions'); ?></span>
                                            </a>
                                        </li>
                                        <li id="commissions_approved">
                                            <a class="submenu" href="<?= site_url('commissions/approved'); ?>">
                                                <i class="fa fa-user-plus"></i>
                                                <span class="text"> <?= lang('list_approved_commissions'); ?></span>
                                            </a>
                                        </li>
                                        <li id="commissions_report_commissions_paid">
                                            <a class="submenu" href="<?= site_url('commissions/report_commissions_paid'); ?>">
                                                <i class="fa fa fa-pie-chart"></i>
                                                <span class="text"> <?= lang('report_commissions_paid'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- End Comissões !-->

                            <?php } ?>


                            <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['suppliers-index'] || $GP['suppliers-add']) { ?>

                                <?php if ($this->Settings->usar_captacao) {?>
                                    <li class="mm_captacao">
                                        <a class="dropmenu" href="#">
                                            <i class="fa fa-whatsapp"></i>
                                            <span class="text"> <?= lang('captacoes'); ?></span>
                                            <span class="chevron closed"></span>
                                        </a>
                                        <ul>
                                            <li id="captacao_index">
                                                <a class="submenu" href="<?= site_url('captacao'); ?>">
                                                    <i class="fa fa-leaf"></i>
                                                    <span class="text"> <?= lang('list_captacao'); ?></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>

                                <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="text"> <?= lang('people'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <?php if ($GP['customers-index']) { ?>
                                            <li id="customers_index">
                                                <a class="submenu" href="<?= site_url('customers'); ?>">
                                                    <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($GP['customers-add']) { ?>
                                            <li id="customers_index">
                                                <a class="submenu" href="<?= site_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                                    <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_customer'); ?></span>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($GP['suppliers-index']) { ?>
                                            <li id="suppliers_index">
                                                <a class="submenu" href="<?= site_url('suppliers'); ?>">
                                                    <i class="fa fa-users"></i><span class="text"> <?= lang('list_suppliers'); ?></span>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($GP['suppliers-add']) { ?>
                                            <li id="suppliers_index">
                                                <a class="submenu" href="<?= site_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_supplier'); ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['sales-index']) { ?>
                                <li class="mm_agenda">
                                    <a class="submenu" href="<?= site_url('agenda/consulta_disponibilidade'); ?>">
                                        <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('schedule'); ?></span>
                                    </a>
                                </li>
                                <li class="mm_agenda_abrir">
                                    <a class="submenu" href="<?= site_url('agenda/abrir'); ?>">
                                        <i class="fa fa-calendar"></i>
                                        <span class="text"> Agenda de Viagem</span>
                                    </a>
                                </li>
                            <?php }?>

                            <?php if ($GP['checkins-index']) { ?>
                            <li class="mm_checkins">
                                <a class="submenu" href="<?= site_url('checkins'); ?>">
                                    <i class="fa fa-qrcode"></i>
                                    <span class="text"> <?= lang('add_checkins'); ?></span>
                                </a>
                            </li>
                            <?php }?>

                            <?php if ($GP['quotes-index'] || $GP['quotes-add']) { ?>
                            <li class="mm_quotes">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-ticket-o"></i>
                                    <span class="text"> <?= lang('quotes'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= site_url('quotes'); ?>">
                                            <i class="fa fa-ticket-o"></i><span class="text"> <?= lang('list_quotes'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['quotes-add']) { ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= site_url('quotes/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_quote'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses'] || $GP['purchases-return_purchases']) { ?>
                            <li class="mm_purchases" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>

                                    <?php if ($GP['purchases-add']) { ?>
										<li id="purchases_index">
											<a class="submenu" href="<?= site_url('purchases'); ?>">
												<i class="fa fa-star"></i><span class="text"> <?= lang('list_purchases'); ?></span>
											</a>
										</li>
										<li id="purchases_add">
											<a class="submenu" href="<?= site_url('purchases/add'); ?>">
												<i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_purchase'); ?></span>
											</a>
										</li>
                                    <?php } ?>
                                    <?php if ($GP['purchases-return_purchases']) { ?>
                                    <li id="purchases_return_purchases">
                                        <a class="submenu" href="<?= site_url('purchases/return_purchases'); ?>">
                                            <i class="fa fa-reply"></i>
                                            <span class="text"> <?= lang('list_return_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>


                            <?php if ($GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts'] || $GP['reports-products'] || $GP['reports-monthly_sales'] || $GP['reports-sales'] || $GP['reports-payments'] || $GP['reports-purchases'] || $GP['reports-customers'] || $GP['reports-suppliers'] || $GP['reports-expenses']) { ?>
                            <li class="mm_reports">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($GP['reports-quantity_alerts']) { ?>
                                    <li id="reports_quantity_alerts">
                                        <a href="<?= site_url('reports/quantity_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-expiry_alerts']) { ?>
                                    <?php if ($Settings->product_expiry) { ?>
                                    <li id="reports_expiry_alerts">
                                        <a href="<?= site_url('reports/expiry_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <?php }
                                    if ($GP['reports-products']) { ?>
                                    <li id="reports_products" style="display: none;">
                                        <a href="<?= site_url('reports/products') ?>">
                                            <i class="fa fa-bus"></i><span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-daily_sales']) { ?>
                                    <li id="reports_daily_sales" style="display: none;">
                                        <a href="<?= site_url('reports/daily_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-monthly_sales']) { ?>
                                    <li id="reports_monthly_sales" style="display: none;">
                                        <a href="<?= site_url('reports/monthly_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-sales']) { ?>
                                    <li id="reports_sales">
                                        <a href="<?= site_url('reports/sales') ?>">
                                            <i class="fa fa-ticket"></i><span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-payments']) { ?>
                                    <li id="reports_payments">
                                        <a href="<?= site_url('reports/payments') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-daily_purchases']) { ?>
                                    <li id="reports_daily_purchases">
                                        <a href="<?= site_url('reports/daily_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-monthly_purchases']) { ?>
                                    <li id="reports_monthly_purchases">
                                        <a href="<?= site_url('reports/monthly_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-purchases']) { ?>
                                    <li id="reports_purchases">
                                        <a href="<?= site_url('reports/purchases') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-expenses']) { ?>
                                    <li id="reports_expenses">
                                        <a href="<?= site_url('reports/expenses') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-customers']) { ?>
                                    <li id="reports_customer_report">
                                        <a href="<?= site_url('reports/customers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-suppliers']) { ?>
                                    <li id="reports_supplier_report">
                                        <a href="<?= site_url('reports/suppliers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['sales-index']) { ?>
                                <li id="graphic">
                                    <a class="submenu" href="<?= site_url('graphic'); ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span class="text"> <?= lang('graphics'); ?></span>
                                    </a>
                                </li>
                            <?php }?>
                            <!--
                            <li>
                                <a class="submenu" href="<?= site_url('financeiro/comissao'); ?>">
                                    <i class="fa fa-dollar"></i>
                                    <span class="text"> <?= lang('Comissões'); ?></span>
                                </a>
                            </li>
                            !-->
                        <?php } ?>
                    </ul>
                </div>
                <a href="#" id="main-menu-act" class="full visible-md visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            </div>
            </td><td class="content-con">
            <div id="content">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <ul class="breadcrumb">
                            <?php
                            foreach ($bc as $b) {
                                if ($b['link'] === '#') {
                                    echo '<li class="active">' . $b['page'] . '</li>';
                                } else {
                                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                                }
                            }
                            ?>
                            <li class="right_log hidden-xs">
                              <span class='hidden-sm'><?php echo $this->sma->dataDeHojePorExtensoRetornoComSemana();?></span>

                                <!--
                                <?= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ": " . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . " " . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . " )</span>" ?>
                                !-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!--
                          Essas informações são consultadas no admin do sistema
                        -->
                        <?php
                        if ($Owner || $Admin) {
                            if (!empty($this->data['faturaEmAbertoSistema']) && $this->data['faturaEmAbertoSistema']->dtVencimento != null) {
                                $dias_vencimento = $this->data['faturaEmAbertoSistema']->quantidade_dias_vencimento;
                                $dtVencimento    = $this->data['faturaEmAbertoSistema']->dtVencimento;
                                $urlPagamento = $this->data['faturaCobrancaEmAbertoSistema']->link;

                                ?>
                                <?php if ($dias_vencimento >= 0 && $dias_vencimento <= 5) {?>
                                    <div class="alert alert-info">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <a class="spwHtmlIcoInfo">&nbsp;</a>
                                        <span class="spwTextoRef">AVISO DE VENCIMENTO!</span>
                                        <p class="spwTextoRefMensagem">
                                            Sua mensalidade vence dia <?php echo $this->sma->hrsd($dtVencimento);?>  realize o pagamento no link abaixo. Lembrando que após 7 dias do vencimento seu sistema será bloqueado automaticamente.
                                            <br/><a href="<?=$urlPagamento;?>" target="_blank"> Clique aqui para realizar o pagamento</a>
                                        </p>
                                    </div>
                                <?php } else if ($dias_vencimento < 0) { ?>
                                    <div class="alert alert-danger">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <a class="spwHtmlIcoErro">&nbsp;</a>
                                        <span class="spwTextoRef">FATURA DE COBRANÇA ATRASADA!</span>
                                        <p class="spwTextoRefMensagem">
                                            Sua mensalidade venceu dia <?php echo $this->sma->hrsd($dtVencimento);?>  realize o pagamento no link abaixo. Lembrando que após 7 dias do vencimento seu sistema será bloqueado automaticamente.
                                            <br/><a href="<?=$urlPagamento;?>" target="_blank"> Clique aqui para realizar o pagamento</a>
                                        </p>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>


                        <?php if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoSucesso">&nbsp;</a>
                                <span class="spwTextoRef">Sucesso!</span>
                                <p class="spwTextoRefMensagem"><?php echo $message;?></p>
                            </div>
                        <?php } ?>

                        <?php if ($error) { ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoErro">&nbsp;</a>
                                <span class="spwTextoRef">OPS! ALGO ERRADO!</span>
                                <p class="spwTextoRefMensagem"><?php echo $error;?></p>
                            </div>
                        <?php }?>

                        <?php if ($warning) { ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoAlerta">&nbsp;</a>
                                <span class="spwTextoRef">IMPORTANTE!</span>
                                <p class="spwTextoRefMensagem"><?php echo $warning;?></p>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoInfo">&nbsp;</a>
                                <span class="spwTextoRef">INFORMAÇÕES!</span>
                                <p class="spwTextoRefMensagem"><?php echo $this->session->flashdata('info');?></p>
                            </div>
                        <?php } ?>

                        <?php
                        if ($info) {
                            foreach ($info as $n) {
                                if (!$this->session->userdata('hidden' . $n->id)) {
                                    ?>
                                    <div class="alert alert-info">
                                        <a href="#" id="<?= $n->id ?>" class="close hideComment external"
                                           data-dismiss="alert">&times;</a>
                                        <a class="spwHtmlIcoInfo">&nbsp;</a>
                                        <span class="spwTextoRef">Lembrete!</span>
                                        <p style="margin: 0px;margin-left: 35px;font-size: 14px;" id="<?php echo 'notificacion'.$n->id;?>"><?= $n->comment; ?></p>
                                    </div>
                                    <?php
                                }
                            }
                        } ?>
                        <div class="alerts-con"></div>

