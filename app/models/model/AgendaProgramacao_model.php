<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaProgramacao_model extends CI_Model
{
    const TIPO_DISPONIBILIDADE_PERIODOS = 'PERIODOS';
    const TIPO_DISPONIBILIDADE_DATAS_PONTUAIS = 'DATAS_PONTUAIS';

    const TIPO_DISPONIBILIDADE_DOMINGO = 'DOMINGO';
    const TIPO_DISPONIBILIDADE_SEGUNDA = 'SEGUNDA';
    const TIPO_DISPONIBILIDADE_TERCA = 'TERCA';
    const TIPO_DISPONIBILIDADE_QUARTA = 'QUARTA';
    const TIPO_DISPONIBILIDADE_QUINTA = 'QUINTA';
    const TIPO_DISPONIBILIDADE_SEXTA = 'SEXTA';
    const TIPO_DISPONIBILIDADE_SABADO = 'SABADO';


    public $produto;
    public $dataSaida;
    public $dataRetorno;
    public $horaSaida;
    public $horaRetorno;
    public $titulo = '';
    public $note = '';
    public $isPromocao = false;
    public $isUsarPreco = false;
    public $preco = 0;
    public $tipoDisponibilidade;
    public $vagas;

    public $segunda = false;
    public $terca = false;
    public $quarta = false;
    public $quinta = false;
    public $sexta = false;
    public $sabado = false;
    public $domingo = false;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getAgendaViagemId()
    {
        return $this->agenda_viagem_id;
    }

    /**
     * @param mixed $agenda_viagem_id
     */
    public function setAgendaViagemId($agenda_viagem_id): void
    {
        $this->agenda_viagem_id = $agenda_viagem_id;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto): void
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    /**
     * @param mixed $dataSaida
     */
    public function setDataSaida($dataSaida): void
    {
        $this->dataSaida = $dataSaida;
    }

    /**
     * @return mixed
     */
    public function getDataRetorno()
    {
        return $this->dataRetorno;
    }

    /**
     * @param mixed $dataRetorno
     */
    public function setDataRetorno($dataRetorno): void
    {
        $this->dataRetorno = $dataRetorno;
    }

    /**
     * @return mixed
     */
    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    /**
     * @param mixed $horaSaida
     */
    public function setHoraSaida($horaSaida): void
    {
        $this->horaSaida = $horaSaida;
    }

    /**
     * @return mixed
     */
    public function getHoraRetorno()
    {
        return $this->horaRetorno;
    }

    /**
     * @param mixed $horaRetorno
     */
    public function setHoraRetorno($horaRetorno): void
    {
        $this->horaRetorno = $horaRetorno;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getIsPromocao()
    {
        return $this->isPromocao;
    }

    /**
     * @param mixed $isPromocao
     */
    public function setIsPromocao($isPromocao): void
    {
        $this->isPromocao = $isPromocao;
    }

    /**
     * @return mixed
     */
    public function getIsUsarPreco()
    {
        return $this->isUsarPreco;
    }

    /**
     * @param mixed $isUsarPreco
     */
    public function setIsUsarPreco($isUsarPreco): void
    {
        $this->isUsarPreco = $isUsarPreco;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco): void
    {
        $this->preco = $preco;
    }

    /**
     * @return mixed
     */
    public function getTipoDisponibilidade()
    {
        return $this->tipoDisponibilidade;
    }

    /**
     * @param mixed $tipoDisponibilidade
     */
    public function setTipoDisponibilidade($tipoDisponibilidade): void
    {
        $this->tipoDisponibilidade = $tipoDisponibilidade;
    }

    /**
     * @return mixed
     */
    public function getVagas()
    {
        return $this->vagas;
    }

    /**
     * @param mixed $vagas
     */
    public function setVagas($vagas): void
    {
        $this->vagas = $vagas;
    }

    /**
     * @return mixed
     */
    public function getSegunda()
    {
        return $this->segunda;
    }

    /**
     * @param mixed $segunda
     */
    public function setSegunda($segunda): void
    {
        $this->segunda = $segunda;
    }

    /**
     * @return mixed
     */
    public function getTerca()
    {
        return $this->terca;
    }

    /**
     * @param mixed $terca
     */
    public function setTerca($terca): void
    {
        $this->terca = $terca;
    }

    /**
     * @return mixed
     */
    public function getQuarta()
    {
        return $this->quarta;
    }

    /**
     * @param mixed $quarta
     */
    public function setQuarta($quarta): void
    {
        $this->quarta = $quarta;
    }

    /**
     * @return mixed
     */
    public function getQuinta()
    {
        return $this->quinta;
    }

    /**
     * @param mixed $quinta
     */
    public function setQuinta($quinta): void
    {
        $this->quinta = $quinta;
    }

    /**
     * @return mixed
     */
    public function getSexta()
    {
        return $this->sexta;
    }

    /**
     * @param mixed $sexta
     */
    public function setSexta($sexta): void
    {
        $this->sexta = $sexta;
    }

    /**
     * @return mixed
     */
    public function getSabado()
    {
        return $this->sabado;
    }

    /**
     * @param mixed $sabado
     */
    public function setSabado($sabado): void
    {
        $this->sabado = $sabado;
    }

    /**
     * @return mixed
     */
    public function getDomingo()
    {
        return $this->domingo;
    }

    /**
     * @param mixed $domingo
     */
    public function setDomingo($domingo): void
    {
        $this->domingo = $domingo;
    }

    public function equals($obj) {
        return $this->dataSaida == $obj->dataSaida &&
            $this->dataRetorno == $obj->dataRetorno &&
            $this->horaRetorno == $obj->horaRetorno &&
            $this->horaSaida == $obj->horaSaida &&
            $this->vagas == $obj->vagas &&
            $this->preco == $obj->preco &&
            $this->tipoDisponibilidade == $obj->tipoDisponibilidade;
    }

}