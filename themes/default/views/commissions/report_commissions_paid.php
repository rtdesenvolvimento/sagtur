 <style type="text/css" media="screen">
    #SLData td:nth-child(1) {display: none;}
    #SLData td:nth-child(2) {text-align: center;}
    #SLData td:nth-child(3) {text-align: left;width: 30%;}
    #SLData td:nth-child(4) {text-align: left;width: 30%;}
    #SLData td:nth-child(5) {text-align: left;display: none;}
    #SLData td:nth-child(6) {text-align: left;display: none;}
    #SLData td:nth-child(7) {text-align: right;}
    #SLData td:nth-child(8) {text-align: right;}
    #SLData td:nth-child(9) {text-align: right;}
    #SLData td:nth-child(10) {text-align: right;}
    #SLData td:nth-child(11) {text-align: center;display: none;}
    #SLData td:nth-child(12) {text-align: center;display: none;}
    #SLData td:nth-child(13) {text-align: center;display: none;}
    #SLData td:nth-child(14) {text-align: center;display: none;}
    #SLData td:nth-child(15) {text-align: center;display: none;}
    #SLData td:nth-child(16) {text-align: center;display: none;}
    #SLData td:nth-child(17) {text-align: center;display: none;}
    #SLData td:nth-child(18) {text-align: center;display: none;}
    #SLData td:nth-child(19) {text-align: center;}
</style>

<script>

    function attachDescriptionToName(x, alignment, aaData) {
        if (aaData[15] !== null) {

            let codigo = aaData[17];

            if ( aaData[15] === 'boleto') x = '<a href="'+aaData[14]+'" target="_blank" title="Ver Boleto"><i class="fa fa-barcode"></i> '+x+'</a><br/><small>Cod. '+codigo+'</small> ';
            else  x = '<a href="'+aaData[16]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[5] !== null) x += '<br/><small><i class="fa fa-money"></i> '+aaData[5]+'</small>';

        return x;
    }

    function currencyFormatValuePay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #F43E61;">'+formatMoney(x*-1)+'<br/><small>'+aaData[10]+'</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValuePaid(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let dtUltimoPagamento = aaData[11] != null ? aaData[11] : '&nbsp;';

        if (x != null) {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + dtUltimoPagamento + '</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValueToPay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let diferencaEmDias = aaData[12] != null ? aaData[12] : '&nbsp;';
        let status = aaData[9];

        if (diferencaEmDias < 0 && status !== 'QUITADA') {
            if (diferencaEmDias === -1) return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>' + diferencaEmDias + ' dia</small></div>';
            else return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>' + diferencaEmDias + ' dias</small></div>';
        } else {
            return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>&nbsp;</small></div>';
        }
    }

    function currencyFormatDebit(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #F43E61;!important;">'+formatMoney(x*-1)+'</div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    $(document).ready(function () {

        $('#filter_programacao_id').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterTipoCobranca').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#date_payment_from').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#date_payment_until').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#biller_filter').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('commissions/getComissoesPagas')?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
                aoData.push({ "name": "filterBiller", "value":  $('#biller_filter').val() });
                aoData.push({ "name": "filterDataPagamentoDe", "value":  $('#date_payment_from').val() });
                aoData.push({ "name": "filterDataPagamentoAte", "value":  $('#date_payment_until').val() });
                aoData.push({ "name": "filterCustomer", "value":  $('#customer_filter').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#filter_programacao_id').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormatValuePay},
                {"mRender": currencyFormatValuePaid},
                {"mRender": currencyFormatValueToPay},
                {"mRender": row_status},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[13];
                nRow.className = "invoice_link_fechamento";
                return nRow;
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                let total = 0, pago = 0, pagar = 0, status, recebidas = 0, aReceber = 0, vencidas = 0, totalizadores = 0;

                for (var i = 0; i < aaData.length; i++) {
                    status = aaData[aiDisplay[i]][9];

                    total += parseFloat(aaData[aiDisplay[i]][6]);
                    pago += parseFloat(aaData[aiDisplay[i]][7]);
                    pagar += parseFloat(aaData[aiDisplay[i]][8]);

                    if (status === 'ABERTA') aReceber += parseFloat(aaData[aiDisplay[i]][8]);
                    if (status === 'PARCIAL' || status === 'QUITADA') recebidas += parseFloat(aaData[aiDisplay[i]][7]);

                    totalizadores += recebidas + aReceber;
                }

                var nCells = nRow.getElementsByTagName('th');

                nCells[6].innerHTML = currencyFormatDebit(parseFloat(total));
                nCells[7].innerHTML = currencyFormat(parseFloat(pago));
                nCells[8].innerHTML = currencyFormatDebit(parseFloat(pagar));

                $('#recebidas').html(currencyFormat(recebidas));
                $('#aReceber').html(currencyFormat(aReceber));
                $('#vencidas').html(currencyFormat(vencidas));
                $('#totalizador').html(currencyFormat(totalizadores));

            }
        });
    });

</script>
 <?php  echo form_open('faturas/baixa_selecao_actions', 'id="action-form"');?>
    <div class="box">
        <div class="box-header">
            <h2 class="blue">
                <i class="fa fa-sign-in"></i><?=lang('report_commissions_paid');?>
            </h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div id="form">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?>
                                <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                            <div style="margin-bottom: 20px;"  class="divfilters">

                                <div class="col-sm-10">
                                    <?= lang("product", "product"); ?>
                                    <?php
                                    $pgs[""] = lang('select').' '.lang('product');
                                    echo form_dropdown('filter_programacao_id', $pgs,  '', 'class="form-control" id="filter_programacao_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-primary" style="width: 100%;margin-top: 25px;" data-toggle="modal" data-target="#filterModal"><i class="fa fa-search"></i> <?=$this->lang->line("filter_product");?></button>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("data_vencimento_de", "start_date"); ?>
                                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("data_vencimento_ate", "end_date"); ?>
                                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="filterStatus"><?=lang('payment_status');?></label>
                                        <select class="form-control tip" name="filterStatus" id="filterStatus">
                                            <option value="">Selecione</option>
                                            <option value="PARCIAL"><?=lang('PARCIAL');?></option>
                                            <option value="QUITADA"><?=lang('QUITADA');?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                        <?php
                                        $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                        foreach ($tiposCobranca as $tipoCobranca) {
                                            $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                        }
                                        echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : ""), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("data_pagamento_de", "date_payment_from"); ?>
                                        <?php echo form_input('date_payment_from', (isset($_POST['date_payment_from']) ? $_POST['date_payment_from'] : $date_payment_from), 'class="form-control" id="date_payment_from"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("data_pagamento_ate", "date_payment_until"); ?>
                                        <?php echo form_input('date_payment_until', (isset($_POST['date_payment_until']) ? $_POST['date_payment_until'] : $date_payment_until), 'type="date" class="form-control" id="date_payment_until"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("customer", "customer_filter"); ?>
                                        <?php
                                        echo form_input('customer_filter', (isset($_POST['customer_filter']) ? $_POST['customer_filter'] : ""), 'id="customer_filter" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("biller", "biller_filter"); ?>
                                        <?php
                                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->name;
                                        }
                                        echo form_dropdown('biller_filter', $bl, (isset($_POST['biller_filter']) ? $_POST['biller_filter'] : $biller_filter), 'id="biller_filter" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '"class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                            <thead>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                <th><?php echo $this->lang->line("vencimento"); ?></th>
                                <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                                <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                                <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                                <th><?php echo $this->lang->line("total"); ?></th>
                                <th><?php echo $this->lang->line("pago"); ?></th>
                                <th><?php echo $this->lang->line("pagar"); ?></th>
                                <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                                <th style="display: none;">Parc.</th>
                                <th style="display: none;">Ult.</th>
                                <th style="display: none;">Atras.</th>
                                <th style="display: none;">Venda.</th>
                                <th style="display: none;">Link.</th>
                                <th style="display: none;">Tipo.</th>
                                <th style="display: none;">Cartao.</th>
                                <th style="display: none;">Cod.</th>
                                <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                            </tr>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                <th><?php echo $this->lang->line("vencimento"); ?></th>
                                <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                                <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                                <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                                <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                                <th><?php echo $this->lang->line("total"); ?></th>
                                <th><?php echo $this->lang->line("pago"); ?></th>
                                <th><?php echo $this->lang->line("pagar"); ?></th>
                                <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                                <th style="display: none;">Parc.</th>
                                <th style="display: none;">Ult.</th>
                                <th style="display: none;">Atras.</th>
                                <th style="display: none;">Venda.</th>
                                <th style="display: none;">Link.</th>
                                <th style="display: none;">Tipo.</th>
                                <th style="display: none;">Cartao.</th>
                                <th style="display: none;">Cod.</th>
                                <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

     <div style="display: none;">
         <input type="hidden" name="form_action" value="" id="form_action_baixa_selecao"/>
         <?=form_submit('performAction', 'performAction', 'id="action-form-baixa-selecao-submit"')?>
     </div>
 <?=form_close()?>

 <!-- Modal Filter Pacote-->
 <div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="filterModalLabel">Filtros de Pacote</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <form>
                     <div class="form-group">
                         <?= lang("Status do Serviço", "status_viagem") ?>
                         <?php
                         $opts = array(
                             'Confirmado' => lang('confirmado'),
                             'Inativo' => lang('produto_inativo'),
                             'Arquivado' => lang('arquivado') ,
                         );
                         echo form_dropdown('filter_status_pacote', $opts,  'Confirmado', 'class="form-control" id="filter_status_pacote"'); ?>
                     </div>
                     <div class="form-group">
                         <?= lang("Ano do Serviço", "ano") ?>
                         <?php
                         $opts = array(
                             '2020' => lang('2020'),
                             '2021' => lang('2021'),
                             '2022' => lang('2022'),
                             '2023' => lang('2023'),
                             '2024' => lang('2024'),
                             '2025' => lang('2025'),
                             '2026' => lang('2026'),
                             '2027' => lang('2027'),
                             '2028' => lang('2028'),
                             '2029' => lang('2029'),
                             '2030' => lang('2030'),
                         );
                         echo form_dropdown('filter_ano_pacote', $opts,  date('Y'), 'class="form-control" id="filter_ano_pacote"'); ?>
                     </div>
                     <div class="form-group">
                         <?= lang("Mês do Serviço", "mes") ?>
                         <?php
                         $opts = array(
                             'Todos' => lang('Todos'),
                             '01' => lang('Janeiro'),
                             '02' => lang('Fevereiro'),
                             '03' => lang('Março'),
                             '04' => lang('Abril'),
                             '05' => lang('Maio'),
                             '06' => lang('Junho'),
                             '07' => lang('Julho'),
                             '08' => lang('Agosto'),
                             '09' => lang('Setembro'),
                             '10' => lang('Outubro'),
                             '11' => lang('Novembro'),
                             '12' => lang('Dezembro'),
                         );
                         echo form_dropdown('filter_mes_pacote', $opts,  date('m'), 'class="form-control" id="filter_mes_pacote"'); ?>
                     </div>
                 </form>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                 <button type="button"  id="applyFilters" class="btn btn-primary">Aplicar Filtros</button>
             </div>
         </div>
     </div>
 </div>

 <script type="text/javascript">
     $(document).ready(function () {

         createFilter('filters');
         buscarProdutos();

         $('#applyFilters').click(function() {
             buscarProdutos();
         });

         $('body').on('click', '#baixa_selecao', function(e) {
             e.preventDefault();
             $('#form_action_baixa_selecao').val($(this).attr('data-action'));
             $('#action-form-baixa-selecao-submit').trigger('click');
         });

         $('body').on('click', '.invoice_link_fechamento td:not(:first-child :last-child)', function() {
             $('#myModal').modal({remote: site.base_url + 'salesutil/modal_view/' + $(this).parent('.invoice_link_fechamento').attr('id')});
             $('#myModal').modal('show');
         });

         $('#customer_filter').select2({
             minimumInputLength: 1,
             data: [],
             initSelection: function (element, callback) {
                 $.ajax({
                     type: "get", async: false,
                     url: site.base_url + "customers/getCustomer/" + $(element).val(),
                     dataType: "json",
                     success: function (data) {
                         callback(data[0]);
                     }
                 });
             },
             ajax: {
                 url: site.base_url + "customers/suggestionsAll",
                 dataType: 'json',
                 quietMillis: 15,
                 data: function (term, page) {
                     return {
                         term: term,
                         limit: 10
                     };
                 },
                 results: function (data, page) {
                     if (data.results != null) {
                         return {results: data.results};
                     } else {
                         return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                     }
                 }
             }
         }).on('change', function (e) {
             $('#SLData').DataTable().fnClearTable();
         });
     });

     function buscarProdutos() {

         $.ajax({
             type: "GET",
             url: "<?php echo base_url() ?>apputil/buscarProgramacao",
             data: {
                 status: $('#filter_status_pacote').val(),
                 ano: $('#filter_ano_pacote').val(),
                 mes: $('#filter_mes_pacote').val()
             },
             dataType: 'json',
             async: true,
             success: function (agendamentos) {

                 $('#filter_programacao_id').empty();
                 var option = $('<option/>');

                 option.attr({ 'value': '' }).text('Selecione uma opção');
                 $('#filter_programacao_id').append(option);

                 $(agendamentos).each(function( index, agendamento ) {
                     var option = $('<option/>');

                     option.attr({ 'value': agendamento.id }).text(agendamento.label);
                     $('#filter_programacao_id').append(option);
                 });

                 $('#filter_programacao_id').select2({minimumResultsForSearch: 7});
                 $('#filterModal').modal('hide');
             }
         });
     }

     function createFilter(nameClasse) {
         $('.'+nameClasse).click(function() {
             if ($('.div'+nameClasse).is(':visible')) {
                 $('.div'+nameClasse).hide(300);
                 $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

             } else {
                 $('.div'+nameClasse).show(300).fadeIn();
                 $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
             }
         });
     }
 </script>