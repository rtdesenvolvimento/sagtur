<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update343 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.22.08.31'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'staff' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('valor_faixa', $fields);

        $tipo_faixa_padrao_guia = array(
            'name' => 'Guia',
            'staff' => 1,
            'descontarVaga' => 1,
            'faixaDependente' => 0,
            'obrigaCPF' => 0,
            'captarDataNascimento' => 0,
            'captarDocumento' => 0,
            'captarOrgaoEmissor' => 0,
            'obrigaOrgaoEmissor' => 0,
            'captarEmail' => 0,
            'captarProfissao' => 0,
            'captarInformacoesMedicas' => 0,
            'captarNomeSocial' => 0,
            'captarCPF' => 0,
        );
        $this->db->insert('valor_faixa', $tipo_faixa_padrao_guia);

        $tipo_faixa_padrao_motorista = array(
            'name' => 'Motorista',
            'staff' => 1,
            'descontarVaga' => 0,
            'faixaDependente' => 0,
            'obrigaCPF' => 0,
            'captarDataNascimento' => 0,
            'captarDocumento' => 0,
            'captarOrgaoEmissor' => 0,
            'obrigaOrgaoEmissor' => 0,
            'captarEmail' => 0,
            'captarProfissao' => 0,
            'captarInformacoesMedicas' => 0,
            'captarNomeSocial' => 0,
            'captarCPF' => 0,
        );
        $this->db->insert('valor_faixa', $tipo_faixa_padrao_motorista);
    }

    public function down() {}
}
