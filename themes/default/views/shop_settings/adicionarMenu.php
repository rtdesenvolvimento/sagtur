<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_menu'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("shop_settings/adicionarMenu", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang("active", "active") ?>
                <?php
                $opts = array(
                    '1' => lang('ativo'),
                    '0' => lang('inativo')
                );
                echo form_dropdown('active', $opts, (isset($_POST['active']) ? $_POST['active'] :  ''), 'class="form-control" id="active" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("type_menu", "type") ?>
                <?php
                $opts = array(
                    'url_page' => lang('url_page'),
                    'menu_superior' => lang('menu_superior'),
                    'menu_superior_url' => lang('menu_superior_url'),
                );
                echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] :  ''), 'class="form-control" id="type_menu" required="required"');
                ?>
            </div>
            <div class="form-group" id="div_menu_pai">
            <?= lang('menu_pai', 'menu_pai'); ?>
                <?php
                $cbMenuPai[''] = lang('select').' '.lang('menu_pai');
                foreach ($menusPai as $pai) {
                    $cbMenuPai[$pai->id] = $pai->name;
                } ?>
                <?= form_dropdown('menu_pai', $cbMenuPai, (isset($_POST['menu_pai']) ? $_POST['menu_pai'] :  ''), 'class="form-control" id="menu_pai"'); ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_url_page">
                <?= lang('url_page', 'url_page'); ?>
                <?= form_input('url_page', '', 'class="form-control" id="url_page"'); ?>
            </div>
            <div class="form-group" id="div_new_page">
                <?= lang("new_page", "new_page") ?>
                <?php
                $opts = array(
                    '1' => lang('yes'),
                    '0' => lang('no')
                );
                echo form_dropdown('new_page', $opts, (isset($_POST['new_page']) ? $_POST['new_page'] :  ''), 'class="form-control" id="new_page" required="required"');
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarMenu', lang('adicionar_menu'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">

    $(document).ready(function () {
        $('#type_menu').change(function (event){
            organizar_formulario($(this).val());
        });

        function organizar_formulario(type) {

            $('#url_page').attr('required', 'required');
            $('#div_menu_pai').show();
            $('#div_url_page').show();
            $('#div_new_page').show();

            if (type=== 'menu_superior') {
                $('#url_page').removeAttr('required');

                $('#div_url_page').hide();
                $('#div_menu_pai').hide();
                $('#div_new_page').hide();
            } else if (type === 'menu_superior_url') {
                $('#url_page').removeAttr('required');
                $('#div_menu_pai').hide();
            }
        }
    })
</script>
