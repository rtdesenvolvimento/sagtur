<?php defined('BASEPATH') OR exit('No direct script access allowed');

class  Linkpagamento extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->init();

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');

        $this->load->model('service/LinkpagamentoService_model', 'LinkpagamentoService_model');

        $this->load->model('sales_model');
        $this->load->model('financeiro_model');

    }

    public function index() {show_404();}

    public function view($faturaId, $status) {

        $cobrancaFatura = $this->getCobrancaFaturaByFaturaId($faturaId);

        $fatura = $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);
        $venda = $this->sales_model->getInvoiceByID($fatura->sale_id);

        if (!empty($venda)) {
            $this->data['venda'] =  $venda;
            $this->data['vendedor'] =  $this->site->getCompanyByID($venda->biller_id);
        }

        $this->data['fatura'] =  $fatura;
        $this->data['cobrancaFatura'] = $cobrancaFatura;
        $this->data['cliente'] = $this->site->getCompanyByID($fatura->pessoa);;

         if ($status == 'success') {
            $this->load->view($this->theme . 'link_pagamento/success', $this->data);
        } else if ($status == 'pending') {
            $this->load->view($this->theme . 'link_pagamento/pending', $this->data);
        } else if ($status == 'failure') {
            $this->load->view($this->theme . 'link_pagamento/failure', $this->data);
        } else {
            show_404();
        }
    }

    function success($faturaId) {

        $this->atualizaPagamento($faturaId);

        $this->view($faturaId, 'success');
    }

    function pending($faturaId) {
        $this->atualizaPagamento($faturaId);


        $this->view($faturaId, 'pending');
    }

    function failure($faturaId) {
        $this->atualizaPagamento($faturaId);

        $this->view($faturaId, 'failure');
    }

    function atualizaPagamento($faturaId) {

        $code = $this->input->get('payment_id');

        if ($code) {
            $cobrancaFatura = $this->getCobrancaFaturaByFaturaId($faturaId);

            $cobrancaFaturaAtualizada = $this->LinkpagamentoService_model->consultaPagamento($cobrancaFatura->integracao, $code, $cobrancaFatura->fatura, $cobrancaFatura->id);
            //$this->enviar_email($venda, $cliente->email);

            return true;
        }

        return false;
    }

    private function getCobrancaFaturaByFaturaId($faturaId) {
        $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($faturaId);

        if (empty($cobrancaFatura)) show_404();

        return $cobrancaFatura;
    }

    private function init() {

        if ($this->input->get('token') != ''){
            $this->session->set_userdata(array('cnpjempresa' => $this->input->get('token')));
        } else {
            if ($this->session->userdata('cnpjempresa') == '') {
                $this->session->set_userdata(array('cnpjempresa' => $this->uri->segment(2)));
            }
        }

        $this->Settings = $this->site->get_setting();
        $this->data['Settings'] = $this->Settings;

        if ($this->Settings->status == 0) {
            redirect('login');
        }

        $sd = $this->site->getDateFormat($this->Settings->dateformat);

        $dateFormats = array(
            'js_sdate' => $sd->js,
            'php_sdate' => $sd->php,
            'mysq_sdate' => $sd->sql,
            'js_ldate' => $sd->js . ' hh:ii',
            'php_ldate' => $sd->php . ' H:i',
            'mysql_ldate' => $sd->sql . ' %H:%i'
        );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m']= $this->m;
        $this->data['v'] = $this->v;
    }

}