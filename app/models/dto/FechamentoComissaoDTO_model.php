<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FechamentoComissaoDTO_model extends CI_Model
{

    public $dtVencimento;
    public $tipo_cobranca_id;

    public $condicao_pagamento_id;

    public $note;

    public $vencimentos;
    public $tiposCobranca;
    public $valorVencimento;
    public $movimentadores;
    public $descontoParcela;

    public function __construct()
    {
        parent::__construct();
    }

}