<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update334 extends CI_Migration {

    public function up() {

        $this->alter_table_produts();

        $this->db->update('settings',  array('version' => '2021.4.6'), array('setting_id' => 1));
    }

    function alter_table_produts() {
        $fields = array(
            'contrato' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
