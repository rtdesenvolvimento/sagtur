<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AsaasController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
    }

    function index()
    {
        show_404();
    }

    public function notification()
    {
        $json_body = file_get_contents('php://input');
        $obj = json_decode($json_body);

        if (!empty($obj->payment)) {
            $this->FinanceiroService_model->baixarPagamentoIntegracao('asaas', $obj->payment->id);
        }

        return TRUE;
    }

}