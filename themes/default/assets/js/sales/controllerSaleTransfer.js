var ControllerSaleTransfer = function () {

    this.onLoad = onLoad;

    function onLoad() {
        onClick();
    }

    function onClick() {
        $('#adicionar-formulario-transfer').click(function (event) {

            if (!controllerSales.isPreencheuFilial() ) return;

            adicionarFormularioTransfer();
        });
    }

    function adicionarFormularioTransfer() {

        $('#pacotes-com-transfer').load(site.base_url + "sales/view_transfer", function(event) {

            $('#slTable').hide();

            $('#barra-remover').show();
            $('#sticker').hide();

            $('#descricaoCadastroManual').html('Adicionar Transfer IN-OUT');

            $('#iconCadastroManual').removeClass('fa-plus');
            $('#iconCadastroManual').removeClass('fa-briefcase');
            $('#iconCadastroManual').addClass('fa-exchange');

            buscarFormularioTaxasClienteFornecedor();

            $('#adicionar-transfer').click(function (event) {
                salvarTransfer();
            });
        });
    }

    function buscarFormularioTaxasClienteFornecedor() {

        $('#load-new-transfer').load(site.base_url+"sales/adicionarPasseio", function() {

            autocompleteOrigemDestino();

            $('#supplierTransfer').val(OPERADORA).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url+"suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            setCampoObrigatorio('supplierTransfer');
            setCampoObrigatorio('emissaoTransfer');

            //in
            setCampoObrigatorio('slOrigemTransferIn');
            setCampoObrigatorio('slDataChegadaTransferIn');
            setCampoObrigatorio('slHoraChegadaTransferIn');
            setCampoObrigatorio('slDestinoTransferIn');
            setCampoObrigatorio('slReservaTransferIn');

            //out
            setCampoObrigatorio('slDestinoTransferOut');
            setCampoObrigatorio('slOrigemTransferOut');
            setCampoObrigatorio('slReservaTransferOut');
            setCampoObrigatorio('slDataSaidaTransferOut');
            setCampoObrigatorio('slHoraSaidaTransferOut');

            $('#agendarIn').click(function (event){
                if ($(this).val() === 'SIM') $('#divAgendarIn').show();
                else $('#divAgendarIn').hide();
            });

            $('#agendarOut').click(function (event){
                if ($(this).val() === 'SIM') $('#divAgendarOut').show();
                else $('#divAgendarOut').hide();
            });

            $('#emissaoTransfer').val(controllerSalesPacote.dataAtualFormatada());

            controllerSalesPacote.onLoadCarregarConfiguracaoItem();

         });
    }

    function setCampoObrigatorio(atributo) {
        $('#'+atributo).attr("required", true);
    }

    function setRemoveCampoObrigatorio(atributo) {
        $('#'+atributo).attr("required", false);
    }

    function autocompleteOrigemDestino() {

        $('#tipoTransferOrigemIn').change(function (event){
            autocomplete('tipoTransferOrigemIn', 'slOrigemTransferIn');
        });

        $('#tipoTransferDestinoIn').change(function (event){
            autocomplete('tipoTransferDestinoIn', 'slDestinoTransferIn');
        });

        $('#tipoTransferDestinoOut').change(function (event){
            autocomplete('tipoTransferDestinoOut', 'slDestinoTransferOut');
        });

        $('#tipoTransferOrigemOut').change(function (event){
            autocomplete('tipoTransferOrigemOut', 'slOrigemTransferOut');
        });
    }

    function autocomplete(tipo, campo) {
        let consulta = '';

        if ($('#'+tipo).val() === 'hotel') consulta = 'sales/getHoteis';
        if ($('#'+tipo).val() === 'Aeroporto') consulta = 'sales/getLocais';

        if ($('#'+tipo).val() === 'Outros') {
            $("#"+tipo).autocomplete("destroy");
            return;
        }

        var t = $("#"+campo).autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: site.base_url+consulta,
                    dataType: "json",
                    data: {
                        term : request.term,
                    },
                    success: function (data) {
                        if (data !== '') response(JSON.parse(data));
                        else response([]);
                    }
                });
            },
            minLength: 3,
            autoFocus: false,
            delay: 200,
            placeholder: 'Digite uma origem/aeroporto',
            response: function (event, ui) {},
            select: function (event, ui) {}
        });

        console.log(t);
    }

    function salvarTransfer() {

        if (!controllerSales.isPreencheuFilial() ) return;

        if (!isCamposObrigatoriosPreenchidosAntesSalvar()){
            bootbox.alert(lang.select_above);

            $('#hotelManual').focus();

            return false;
        }

        let mid = (new Date).getTime();

        let mcode = mid;
        let mqty = 1;
        let mtax = 0;

        let nomePassageiro = $('#search-customer').select2('data').text;
        let clienteId = $('#search-customer').val();

        let categoria = 'Transfer';//TODO
        let categoriaId = 12;//TODO

        let fornecedor = $('#supplierTransfer').val();
        let nmFornecedor = $('#supplierTransfer').select2('data').text;
        let dataEmissao = $('#emissaoTransfer').val();

        let nomePacote = 'Transfer';

        if ($('#agendarIn').val() === 'SIM') nomePacote += ' IN';
        if ($('#agendarOut').val() === 'SIM') nomePacote += ' OUT';

        let unit_price = getNumber($('#vlLiquidoCustomer').find('div').html());
        let mdiscount = getNumber($('#slTaxasAdicionaisDescontoCustomer').val());
        let comissao = getNumber($('#slComissaoConsultorAbsolutoSupplier').val());

        let tipoCobrancaFornecedorId = $('#tipoCobrancaFornecedorId').val();
        let condicaoPagamentoFornecedorId = $('#condicaoPagamentoFornecedorId').val();
        let vlTotalPagamentoFornecedorInput = $('#vlTotalPagamentoFornecedorInput').val();

        let mtax_rate = {};

        slitems[mid] = {
            "id": mid,
            "item_id": mid,
            "label": nomePacote,
            "row": {
                "id": mid,
                "code": mcode,
                "name": nomePacote,
                "quantity": mqty,
                "price": unit_price,
                "unit_price": unit_price,
                "real_unit_price": unit_price,
                "tax_rate": mtax,
                "tax_method": 0,
                "qty": mqty,
                "type": "manual",
                "discount": String(mdiscount),
                "serial": "",
                "option": "",
                "customerClient" :clienteId,
                "customerClientName" : nomePassageiro,
                "fornecedor" : fornecedor,
                'nmFornecedor' : nmFornecedor,
                "dataEmissao" : dataEmissao,
                "tipoCobrancaFornecedorId": tipoCobrancaFornecedorId,
                "condicaoPagamentoFornecedorId": condicaoPagamentoFornecedorId,
                "vlTotalPagamentoFornecedorInput": vlTotalPagamentoFornecedorInput,
                "comissao" : comissao,
                "category_name" : categoria,
                "categoria_id": categoriaId,

                "digitado" : '1'
            },
            "tax_rate": mtax_rate,
            "options": false
        };

        if ($('#agendarIn').val() === 'SIM') adicionarTransferIN(slitems[mid]);
        if ($('#agendarOut').val() === 'SIM') adicionarTransferOUT(slitems[mid]);

        controllerSalesPacote.preencherTaxasCliente(slitems[mid]);
        controllerSalesPacote.preencherDadosCliente(slitems[mid]);

        localStorage.setItem('slitems', JSON.stringify(slitems));

        loadItems();

        controllerSalesPacote.limparDadosFormularioPacotes();
        //audio_success.play();
    }

    function adicionarTransferIN(slitems) {

        let mid = (new Date).getTime();

        let tipoTransferOrigem = $('#tipoTransferOrigemIn').val();
        let origem = $('#slOrigemTransferIn').val();
        let dataSaida = $('#slDataChegadaTransferIn').val();
        let horaSaida = $('#slHoraChegadaTransferIn').val();

        let tipoTransferDestino = $('#tipoTransferDestinoIn').val();
        let destino = $('#slDestinoTransferIn').val();
        let reserva = $('#slReservaTransferIn').val();
        let fornecedor = $('#supplierTransfer').val();
        let dataEmissao = $('#emissaoTransfer').val();

        if (slitems.transfers === undefined) slitems.transfers = {}

        slitems.transfers[mid+'_in'] = {
            "mid" : mid,
            "item" : slitems.id,
            "tipoTransfer": 'IN',
            "tipoOrigem": tipoTransferOrigem,
            "origem": origem,
            "data": dataSaida,
            "hora": horaSaida,
            "tipoDestino": tipoTransferDestino,
            "destino": destino,
            "referencia": reserva,
            "fornecedor" : fornecedor,
            "dataEmissao" : dataEmissao,
        }
    }

    function adicionarTransferOUT(slitems) {

        let mid = (new Date).getTime();

        let tipoTransferOrigem = $('#tipoTransferOut').val();
        let origem = $('#slOrigemTransferOut').val();
        let dataSaida = $('#slDataSaidaTransferOut').val();
        let horaSaida = $('#slHoraSaidaTransferOut').val();

        let tipoTransferDestino = $('#tipoTransferDestinoIn').val();
        let destino = $('#slDestinoTransferOut').val();
        let reserva = $('#slReservaTransferOut').val();
        let fornecedor = $('#supplierTransfer').val();
        let dataEmissao = $('#emissaoTransfer').val();

        let dataSaidaVooBus = $('#slDataPartidaTransferOut').val();
        let horaSaidaBooBus = $('#slHoraPartidaTransferOut').val();
        let referenciaVooBus = $('#referenciaVooBusTransferOut').val();

        if (slitems.transfers === undefined) slitems.transfers = {}

        slitems.transfers[mid+'_out'] = {
            "mid" : mid,
            "item" : slitems.id,
            "tipoTransfer": 'OUT',
            "tipoOrigem": tipoTransferOrigem,
            "origem": origem,
            "data": dataSaida,
            "hora": horaSaida,
            "tipoDestino": tipoTransferDestino,
            "destino": destino,
            "referencia": reserva,
            "dataSaidaVooBus" : dataSaidaVooBus,
            "horaSaidaBooBus" : horaSaidaBooBus,
            "referenciaVooBus" : referenciaVooBus,
            "fornecedor" : fornecedor,
            "dataEmissao" : dataEmissao,
        }
    }

    function isCamposObrigatoriosPreenchidosAntesSalvar() {

        var customer =  $('#search-customer').val();
        var supplierTransfer =  $('#supplierTransfer').val();

        return $('form[data-toggle="validator"]').data('bootstrapValidator').validate().isValid() &&
            customer !== '' &&
            supplierTransfer !== '';
    }
}