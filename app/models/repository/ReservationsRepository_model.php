<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReservationsRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllInvoiceItems($filter, $sale_id = false)
    {

        //$filter = new ReservationsFilterDTO_model();

        $this->db->select('
        
            sales.paid,
            sales.shipping,
            sales.order_discount,
            sales.grand_total,            
            sales.sale_status,            

            sale_items.id as itemid,
            sale_items.sale_id as id,
            sale_items.product_name,
            sale_items.customerClientName as customer_name,
            sale_items.subtotal,
            sale_items.customerClient,
            local_embarque.name as localEmbarque,
            sale_items.faixaNome,
            sale_items.product_name,
            sale_items.guia,
            sale_items.motorista,
            sale_items.poltronaClient,
            sale_items.product_name,
            sale_items.faixaNome,

            tipo_transporte_rodoviario.name as tipo_transporte,
            
            products.color_agenda_id,
            categories.name as categoria,
            tipo_trajeto_veiculo.name as tipo_trajeto,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno
            
            ')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('tipo_trajeto_veiculo', 'products.tipo_trajeto_id=tipo_trajeto_veiculo.id', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque','left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte','left')
            ->order_by('agenda_viagem.dataSaida, agenda_viagem.horaSaida, sale_items.sale_id, sale_items.customerClient');

        $this->db->where("sales.sale_status in ('faturada', 'orcamento') ");

        if ($sale_id) {
            $this->db->where('sale_items.sale_id', $sale_id);
        }

        if ($filter->product_id) {
            $this->db->where('sale_items.product_id', $filter->product_id);
        }

        if ($filter->data_saida) {
            $this->db->where('agenda_viagem.dataSaida>=', $filter->data_saida);
        }

        if ($filter->data_retorno) {
            $this->db->where('agenda_viagem.dataSaida<=', $filter->data_retorno);
        }

        if ($filter->customer) {
            $this->db->where('sale_items.customerClient', $filter->customer);
        }

        if ($filter->local_embarque) {
            $this->db->where('sale_items.localEmbarque', $filter->local_embarque);
        }

        if ($filter->tipo_transporte) {
            $this->db->where('sale_items.tipoTransporte', $filter->tipo_transporte);
        }

        if ($filter->guia) {
            $this->db->where('sale_items.guia', $filter->guia);
        }

        if ($filter->motorista) {
            $this->db->where('sale_items.motorista', $filter->motorista);
        }

        if ($filter->hora_saida) {
            $this->db->where('agenda_viagem.horaSaida>=', $filter->hora_saida);
        }

        if ($filter->hora_retorno) {
            $this->db->where('agenda_viagem.horaSaida<=', $filter->hora_retorno);
        }

        if ($filter->status_sale) {
            $this->db->where('sales.sale_status=', $filter->status_sale);
        }

        if ($filter->status_payment) {
            $this->db->where('sales.payment_status=', $filter->status_payment);
        }

        if ($filter->biller_id) {
            $this->db->where('sales.payment_status=', $filter->biller);
        }

        if ($filter->biller) {
            $this->db->where('sales.biller_id', $filter->biller);
        }

        if ($filter->meio_divulgacao) {
            $this->db->where('sales.meioDivulgacao=', $filter->meio_divulgacao);
        }

        if ($filter->data_venda_de) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $filter->data_venda_de);
        }

        if ($filter->data_venda_ate) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $filter->data_venda_ate);
        }

        if ($filter->created_by) {
            $this->db->where('sales.created_by=', $filter->created_by);
        }

        if ($filter->idsVenda) {
            $this->db->where('sma_sales.id IN ('. $filter->idsVenda .') ');
        }

        if ($filter->group_by) {
            $this->db->group_by('sma_sales.id');
        }

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByID($id)
    {
        $this->db->select('sales.*, meio_divulgacao.name as meio_divulgacao_name, condicao_pagamento.name as condicaoPagamento, tipo_cobranca.name as tipoCobranca, sma_companies.name as biller_name ')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('meio_divulgacao', 'meio_divulgacao.id=sales.meioDivulgacao', 'left')
            ->join('sma_companies', 'sma_companies.id=sales.biller_id', 'left');
        $q = $this->db->get_where('sales', array('sales.id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}