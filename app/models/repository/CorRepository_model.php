<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CorRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll() {

        $this->db->where("active", 1);
        $q = $this->db->get('cor');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}