<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update498 extends CI_Migration {

    public function up() {

        $this->create_table_whatsapp();

        $this->db->update('settings',  array('version' => '4.9.8'), array('setting_id' => 1));
    }


    public function create_table_whatsapp() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'type' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'instance' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => TRUE ),
            'apikey' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => TRUE ),
            'client_token' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => TRUE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('whatsapp_settings', TRUE, $attributes);

        $data_plugsign = array(
            'apikey' => '',
            'active' => 0,
            'note' => 'Integração com whatsapp',
        );

        $this->db->insert('whatsapp_settings', $data_plugsign);
    }

    public function down() {}
}
