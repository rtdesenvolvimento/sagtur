<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update383 extends CI_Migration {

    public function up() {

        $this->popular_tabelas();

        $this->db->update('settings',  array('version' => '3.8.3'), array('setting_id' => 1));
    }

    public function popular_tabelas() {

        $this->db->query('delete from sma_product_midia');
        $this->db->query('delete from sma_product_seo');

        $products = $this->getAllProducts();

        foreach ($products as $product) {
            $product = $this->getProductByID($product->id);

            $midia_data = array(
                'url_video' => '',
            );

            $seo_data = array(
                'tag_title' => $product->name,
                'meta_tag_description' => '',
                'url_product' => '',
                'key_words' => '',
            );

            if ($midia_data) {
                $midia_data['product_id'] = $product->id;
                $this->db->insert('product_midia', $midia_data);
            }

            if ($seo_data) {
                $seo_data['product_id'] = $product->id;
                $this->db->insert('product_seo', $seo_data);
            }
        }
    }

    public function getProductByID($id) {

        $this->db->select('products.*, 
        product_midia.url_video,
        product_seo.tag_title,
        product_seo.meta_tag_description,
        product_seo.url_product,
        product_seo.key_words,
        product_addresses.ponto_encontro,
        product_addresses.cep, 
        product_addresses.endereco, 
        product_addresses.numero, 
        product_addresses.complemento, 
        product_addresses.bairro, 
        product_addresses.cidade, 
        product_addresses.estado,
        product_addresses.pais');

        $this->db->join('product_midia', 'products.id=product_midia.product_id', 'left');
        $this->db->join('product_addresses', 'products.id=product_addresses.product_id', 'left');
        $this->db->join('product_seo', 'products.id=product_seo.product_id', 'left');

        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllProducts() {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function down() {}
}
