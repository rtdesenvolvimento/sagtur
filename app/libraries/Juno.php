<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *
 * BoletoFacil é uma solução para emissão de cobranças da BoletoBancario.com
 * Para usar, é necessário ter um cadastro no boleto fácil e gerar um token de integração.
 * Acesse e confira: https://www.boletobancario.com/boleto-facil
 * Documentação: https://www.boletobancario.com/boletofacil/integration/integration.html
 * Criado por: Junior Barros
 *
 */

class Juno {

    public $transferAmount;
    public $description;
    public $reference;
    public $amount;
    public $dueDate;

    public $installments = 1;//total de parcelas

    public $maxOverdueDays = 29;//dias maximo para o pagamento apos o vencimento
    public $paymentAdvance = false;//Define se o pagamento via cartão de crédito será antecipado

    public $fine = 2.0;//valor da multa por atraso
    public $interest = 1.0;//valor do juros mora

    public $discountAmount;
    public $discountDays;
    public $paymentTypes;
    public $tipoCobranca;

    public $payerName;
    public $payerCpfCnpj;
    public $payerEmail;
    public $payerSecondaryEmail;
    public $payerPhone;
    public $payerBirthDate;

    public $billingAddressStreet;
    public $billingAddressNumber;
    public $billingAddressComplement;
    public $billingAddressNeighborhood;
    public $billingAddressCity;
    public $billingAddressState;
    public $billingAddressPostcode;

    public $feeSchemaToken;
    public $referralToken;
    public $notifyPayer = true;
    public $notificationUrl;

    public $token = '';
    public $sandbox = true;

    const PROD_URL = "https://www.boletobancario.com/boletofacil/integration/api/v1/";
    const SANDBOX_URL = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1/";
    const RESPONSE_TYPE = "JSON";

    public function __construct() {}

    public function createCharge($payerName, $payerCpfCnpj, $description, $amount, $dueDate) {
        $this->payerName = $payerName;
        $this->payerCpfCnpj = $payerCpfCnpj;
        $this->description = $description;
        $this->amount = $amount;
        $this->dueDate = $dueDate;
        return $this;
    }

    public function issueCharge() {
        $requestData = array(
            'token'                     =>  $this->token,
            'description'               =>  $this->description,
            'reference'                 =>  $this->reference,
            'amount'                    =>  $this->amount,
            'dueDate'                   =>  $this->dueDate,
            'installments'              =>  $this->installments,
            'maxOverdueDays'            =>  $this->maxOverdueDays,
            'fine'                      =>  $this->fine,
            'interest'                  =>  $this->interest,
            'discountAmount'            =>  $this->discountAmount,
            'discountDays'              =>  $this->discountDays,
            'payerName'                 =>  $this->payerName,
            'payerCpfCnpj'              =>  $this->payerCpfCnpj,
            'payerEmail'                =>  $this->payerEmail,
            'payerSecondaryEmail'       =>  $this->payerSecondaryEmail,
            'payerPhone'                =>  $this->payerPhone,
            'payerBirthDate'            =>  $this->payerBirthDate,
            'billingAddressStreet'      =>  $this->billingAddressStreet,
            'billingAddressNumber'      =>  $this->billingAddressNumber,
            'billingAddressComplement'  =>  $this->billingAddressComplement,
            'billingAddressNeighborhood'=>  $this->billingAddressNeighborhood,
            'billingAddressCity'        =>  $this->billingAddressCity,
            'billingAddressState'       =>  $this->billingAddressState,
            'billingAddressPostcode'    =>  $this->billingAddressPostcode,
            'notifyPayer'               =>  $this->notifyPayer,
            'notificationUrl'           =>  $this->notificationUrl,
            'paymentTypes'              =>  $this->paymentTypes,
            'paymentAdvance'            =>  $this->paymentAdvance,
            'feeSchemaToken'            =>  $this->feeSchemaToken,
            'referralToken'             => $this->referralToken,
            'responseType'              =>  Juno::RESPONSE_TYPE
        );

        return $this->request("issue-charge", $requestData);
    }

    public function fetchPaymentDetails($paymentToken) {
        $requestData = array(
            'paymentToken'   => $paymentToken,
            'responseType'   => Juno::RESPONSE_TYPE
        );

        return $this->request("fetch-payment-details", $requestData);
    }

    public function listCharges($beginPaymentDate) {
        $endPaymentDate = date('Y-m-d', strtotime("+1 days", strtotime($beginPaymentDate)));

        $requestData = array(
            'token'         => $this->token,
            'beginPaymentDate' => date('d/m/Y', strtotime($beginPaymentDate)),
            'endPaymentDate' =>   date('d/m/Y', strtotime($endPaymentDate)),
            'responseType'  => Juno::RESPONSE_TYPE
        );

        return $this->request("list-charges", $requestData);
    }

    public function fetchBalance() {
        $requestData = array(
            'token'         => $this->token,
            'responseType'  => Juno::RESPONSE_TYPE
        );

        return $this->request("fetch-balance", $requestData);
    }

    public function requestTransfer() {
        $requestData = array(
            'token'         => $this->token,
            'responseType'  => Juno::RESPONSE_TYPE,
            'amount'        => $this->transferAmount
        );

        return $this->request("request-transfer", $requestData);
    }

    public function cancelCharge($code) {
        $requestData = array(
            'token'         => $this->token,
            'code'          => $code,
            'responseType'  => Juno::RESPONSE_TYPE
        );
        return $this->request("cancel-charge", $requestData);
    }

    private function request($urlSufix, $data) {
        $ch = curl_init(($this->sandbox ? Juno::SANDBOX_URL : Juno::PROD_URL).$urlSufix);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        // execute!
        $response = curl_exec($ch);

        return $response;
    }
}