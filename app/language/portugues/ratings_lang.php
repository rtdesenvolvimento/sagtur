<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ratings']                = 'Avaliações';
$lang['rating']                 = 'Avaliação';
$lang['average']                = 'Média';
$lang['stars']                  = 'Estrelas';

$lang['responses']  = 'Respostas';
$lang['rating_view'] = 'Visualizar Avaliação';
$lang['question']   = 'Pergunta';

$lang['edit_question']  = 'Editar Pergunta';
$lang['questions'] = 'Perguntas';

$lang['pergunta_atualizada_com_sucesso']= 'Pergunta atualizada com sucesso!';
$lang['ordem'] = 'Ordem';
$lang['type_rating'] = 'Avaliar com Estrelas';
$lang['type_note']  = 'Avaliar com Observação';
$lang['type_question'] = 'Tipo de Pergunta';
$lang['add_question'] = 'Adicionar Pergunta';
$lang['pergunta_adicionada_com_sucesso'] = 'Pergunta adicionada com sucesso!';