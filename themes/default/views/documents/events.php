<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel">
                <?= lang("contract_events"); ?><br/>
                <i class="fa fa-file" style="font-size: 1.2em;"></i> <?php echo $contract->name;?> </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="controls table-controls">
                        <table id="slTableParcelasRenegociacao" class="table items table-striped table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: left;" ><?= lang("date"); ?></th>
                                <th style="text-align: left;"><?= lang('event') ?></th>
                                <th style="text-align: left;"><?= lang('user') ?></th>
                                <th style="text-align: center;"><?= lang('status') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($events as $event){?>
                                <tr>
                                    <td style="text-align: left;"><?= $this->sma->hrld($event->date); ?></td>
                                    <td style="text-align: left;"><?= $event->event; ?></td>
                                    <td style="text-align: left;"><?= $event->user; ?></td>
                                    <td style="text-align: center;">
                                        <?= lang($event->status); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $modal_js ?>