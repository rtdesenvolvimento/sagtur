<!DOCTYPE html>
<div data-role="page" data-theme="a" data-url="minhas-viagens" id="minhas-viagens">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h2 style="font-weight: bold;text-shadow: none;">CHECK-IN</h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <h4 style="text-align: right;"><?=$product->name;?></h4>
        <p style="text-align: right;line-height: 0px;font-weight: 700;font-size: 12px;"><?=$tipoTransporte->name;?></p>
        <p style="text-align: right;font-size: 12px;">Saída <?php echo $this->sma->dataToSite($programacao->getDataSaida());?></p>
        <h5>Clique no(s) assento(s) disponível(is):</h5>
        <?php if ($Settings->instrucoes_planta_onibus){?>
            <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;text-transform: initial;"><?=$Settings->instrucoes_planta_onibus;?></div>
        <?php } ?>
        <div id="client_sel_assentos" style="margin-top: 15px;">
            <input type="hidden" id="product_id" value="<?=$proxima_viagem->product_id;?>"/>
            <input type="hidden" id="programacao_id" value="<?=$proxima_viagem->programacaoId;?>"/>
            <input type="hidden" id="tipo_transporte_id" value="<?=$tipoTransporteID;?>"/>
            <fieldset class="cc_fieldset cc_assentos cc_assento_selecionado" id="cc_assento_dependente<?=$proxima_viagem->id;?>" sel="dependente<?=$proxima_viagem->id;?>">
                <div>
                    <div style="display: flex;text-align: left;">
                        <div class="col-12">
                            <div class="assento_selecionado" style="float: left;">
                                <select required="required" data-role="none"  class="combo_assento combo2" id="select_assento_selecionadodependente<?=$proxima_viagem->id;?>">
                                    <?php if ($proxima_viagem->poltronaClient){?>
                                        <option><?=$proxima_viagem->poltronaClient;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="assento_selecionado_name" style="float:right;">
                                <div class="assento_selecionado_name_inner" id="assento_dependente<?=$proxima_viagem->id;?>"><?=$customer->name;?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <!--MAPA DO ONIBUS!-->
        <iframe width="100%" frameborder="0" scrolling="no" id="iframe_onibus" height="800px" style="margin-top: 15px;"></iframe>
    </div>
    <div   class="botoes_avanca_confirma">

        <div style="display: none;" id="div_assentos_marcacao_confirma">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-12" style="text-align: right;margin-right: 30px;">
                    <button type="button" data-role="none" class="close" onclick="close_marcacao();" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x" style="font-size: 38px;">×</i></button>
                </div>

                <div style="padding: 0px 0px 0px 15px;text-transform: none;">
                    <div class="col-6 colulna_totalizador_left" id="pop_up_seleciona_assento_name" style="text-align: left;text-transform: uppercase;"></div>
                    <div class="col-6 colulna_totalizador_right" id="pop_up_seleciona_assento_poltrona"></div>
                </div>
            </div>

            <div class="div_botao">
                <button onclick="confirmarAssentoMarcado(this);" style="background: none rgb(232, 17, 75);color: #ffffff;text-shadow: none;font-size: 1.125rem;height: 3rem;">CONFIRMAR ASSENTO</button>
            </div>
        </div>

        <div style="display: none;" id="div_assentos_marcacao_desmarca">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-12" style="text-align: right;margin-right: 30px;">
                    <button type="button" data-role="none" class="close"  onclick="close_desmarcar();" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x" style="font-size: 38px;">×</i></button>
                </div>
                <div style="padding: 0px 0px 0px 15px;text-transform: none;">
                    <div class="col-6 colulna_totalizador_left" id="pop_down_seleciona_assento_name" style="text-align: left;text-transform: uppercase;"></div>
                    <div class="col-6 colulna_totalizador_right" id="pop_down_seleciona_assento_poltrona"></div>
                </div>
            </div>
            <div class="div_botao">
                <button onclick="desmarcarAssento(this);" style="background: none rgb(232, 17, 75);color: #ffffff;text-shadow: none;font-size: 1.125rem;height: 3rem;">REMOVER ASSENTO</button>
            </div>
        </div>

        <div data-role="navbar">
            <ul id="botao_confirma">
                <li><button data-icon="check" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;text-shadow: none;" onclick="bloqueio_assento_site();">CONFIRMAR MARCAÇÃO</button></li>
            </ul>
        </div>
    </div>
</div>