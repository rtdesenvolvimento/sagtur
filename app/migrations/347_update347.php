<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update347 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '3.30.09.22.0'), array('setting_id' => 1));
    }

    public function create_index() {
        try {

            //sma_tipo_hospedagem_viagem
            $this->db->query('CREATE INDEX indxs_tipo_hospedagem_viagem_produto_tipoHospedagem	ON sma_tipo_hospedagem_viagem (produto, tipoHospedagem)');
            $this->db->query('CREATE INDEX indxs_tipo_hospedagem_viagem_produto	ON sma_tipo_hospedagem_viagem (produto)');

            //sma_valor_faixa_etaria
            $this->db->query('CREATE INDEX indxs_valor_faixa_etaria_faixaId_produto_status	ON sma_valor_faixa_etaria (faixaId, produto, status)');

            //sma_faixa_etaria_tipo_hospedagem
            $this->db->query('CREATE INDEX indxs_sma_faeth_produto	ON sma_faixa_etaria_tipo_hospedagem (produto)');
            $this->db->query('CREATE INDEX indxs_sma_faeth_produto_tipoHospedagem	ON sma_faixa_etaria_tipo_hospedagem (produto, tipoHospedagem)');
            $this->db->query('CREATE INDEX indxs_sma_faeth_produto_faixaId_status	ON sma_faixa_etaria_tipo_hospedagem (faixaId, produto, status)');
            $this->db->query('CREATE INDEX indxs_sma_faeth_produto_faixaId_tipoHospedagem	ON sma_faixa_etaria_tipo_hospedagem (faixaId, produto, tipoHospedagem)');

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
