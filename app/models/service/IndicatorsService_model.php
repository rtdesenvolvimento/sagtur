<?php defined('BASEPATH') OR exit('No direct script access allowed');

class IndicatorsService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/IndicatorsRepository_model');
    }

    /*
     * ARR (Annual Recurring Revenue): Receita recorrente anualizada. Mostra a previsibilidade da receita ao longo do ano.
     */
    public function ARR_Total($ano)
    {
        return $this->IndicatorsRepository_model->ARR_Total($ano);
    }

    public function ARR_Grafico($ano): array
    {
        return $this->IndicatorsRepository_model->ARR_Grafico($ano);
    }

    public function ARR_AgrupadoPorGrupo($ano)
    {
        return $this->IndicatorsRepository_model->ARR_AgrupadoPorGrupo($ano);
    }

    public function activeCustomers()
    {
        return $this->IndicatorsRepository_model->activeCustomers();
    }

    public function inactiveCustomers()
    {
        return $this->IndicatorsRepository_model->inactiveCustomers();
    }

    public function customersLastYear($ano)
    {
        return $this->IndicatorsRepository_model->customersLastYear($ano);
    }
}