<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update336 extends CI_Migration {

    public function up() {

        $this->alter_table_sales();
        $this->alter_table_sales_item();
        $this->alter_table_customer();
        $this->alter_table_fatura_cobranca();
        $this->alter_table_settings();
        $this->alter_table_roominglist();
        $this->alter_table_room_list_hospedagem_hospede();

        $this->create_table_meio_divulgacao();
        $this->create_table_veiculos();
        $this->create_table_biller_veiculo();
        $this->create_table_itinerario();
        $this->create_table_itinerario_items();
        $this->create_table_mercado_pago();

        $this->create_table_plano_sagtur();
        $this->create_table_cupom_desconto();
        $this->create_table_cupom_desconto_item();

        $this->db->update('settings',  array('version' => '2.2022.01.1', 'decimals_sep' => ',', 'thousands_sep' => '.', 'timezone' => 'America/Sao_Paulo', 'watermark' => '0', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));

    }

    public function alter_table_settings() {
        $fields = array(
            'status' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
            'numero_ususarios' => array('type' => 'INT', 'constraint' => 11, 'default' => 1 ),
            'dia_vencimento_plano' => array('type' => 'INT', 'constraint' => 11, 'default' => 10 ),
            'plano_sagtur' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
            'exibirDadosFaturaVoucher' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
            'informacoesImportantesVoucher' => array('type' => 'LONGTEXT', 'default' => ''),
            'captarMeioDivulgacaoVenda' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
            'ocultarProdutosSemEstoqueLoja' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
            'disponibilizarVoucherReserva' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);

        $dados = 'Para o embarque é obrigatório apresentar RG<br/>CNH ou documento oficial  com foto<br/>podendo ser a certidão de nascimento original para menores de 12 anos.<br/>Limite máximo de tolerância de 15 minutos para o embarque.<br/>Apresentar a carteira de vacinação com o registro da COVID para o embarque.<br/>';

        $this->db->update('settings',  array('informacoesImportantesVoucher' => $dados), array('setting_id' => 1));
    }

    public function alter_table_sales() {
        $fields = array(
            'venda_link' => array('type' => 'INT', 'constraint' => 1,  'default' => 0),
            'meioDivulgacao' => array('type' => 'INT', 'constraint' => 11 , 'default' => 12),
            'indicacao' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => ''),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_sales_item() {

        $fields = array(
            'dataEmbarque' => array('type' => 'DATE', 'null' => TRUE ),
            'fornecedor' => array('type' => 'INT', 'constraint' => 11 ),
            'guia' => array('type' => 'INT', 'constraint' => 11 ),
            'motorista' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('sale_items', $fields);
    }

    public function alter_table_customer() {
        $fields = array(
            'idioma' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => 'portugues'),
            'bloqueado' => array('type' => 'INT', 'constraint' => 1,  'default' => 0),
            'motivo_bloqueio' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_fatura_cobranca() {
        $fields = array(
            'qr_code_base64' => array('type' => 'LONGTEXT', 'default' => ''),
            'qr_code' => array('type' => 'LONGTEXT', 'default' => ''),
            'errorProcessamento' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('fatura_cobranca', $fields);
    }

    function create_table_cupom_desconto() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'codigo' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'tipo_desconto' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => 'PERCENTUAL'),
            'descricao' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cupom_desconto', TRUE, $attributes);
    }

    function create_table_cupom_desconto_item() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'cupom' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),
            'quantidade_disponibilizada' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'dataInicio' => array('type' => 'DATE', 'null' => TRUE ),
            'dataFinal' => array('type' => 'DATE', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cupom_desconto_item', TRUE, $attributes);
    }

    function create_table_meio_divulgacao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'outros' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('meio_divulgacao', TRUE, $attributes);

        $this->db->insert('meio_divulgacao', array('name' => 'Telefone/WhatsApp'));
        $this->db->insert('meio_divulgacao', array('name' => 'Facebook'));
        $this->db->insert('meio_divulgacao', array('name' => 'Instagram'));
        $this->db->insert('meio_divulgacao', array('name' => 'Grupos WhatsApp'));
        $this->db->insert('meio_divulgacao', array('name' => 'Pesquisa no Google'));
        $this->db->insert('meio_divulgacao', array('name' => 'Indicação', 'outros' => 1));
        $this->db->insert('meio_divulgacao', array('name' => 'E-mail'));
        $this->db->insert('meio_divulgacao', array('name' => 'Já sou cliente'));
        $this->db->insert('meio_divulgacao', array('name' => 'Televisão'));
        $this->db->insert('meio_divulgacao', array('name' => 'Rádio'));
        $this->db->insert('meio_divulgacao', array('name' => 'Cartões de visita e panfletos'));
        $this->db->insert('meio_divulgacao', array('name' => 'Outro', 'outros' => 1));
    }

    function create_table_veiculos() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('veiculo', TRUE, $attributes);
    }

    function create_table_biller_veiculo() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'veiculo' => array('type' => 'INT', 'constraint' => 11 ),
            'motorista' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('motorista_veiculo', TRUE, $attributes);
    }

    function create_table_itinerario() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'dataEmbarque' => array('type' => 'DATE', 'null' => TRUE ),
            'horaEmbarque' => array('type' => 'TIME', 'null' => TRUE ),
            'dataRetorno' => array('type' => 'TIME', 'null' => TRUE ),
            'horaRetorno' => array('type' => 'TIME', 'null' => TRUE ),
            'fornecedor' => array('type' => 'INT', 'constraint' => 11 ),
            'motorista' => array('type' => 'INT', 'constraint' => 11 ),
            'guia' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'ABERTO'),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('itinerario', TRUE, $attributes);
    }

    function create_table_itinerario_items() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'itineario' => array('type' => 'INT', 'constraint' => 11 ),
            'sale_item' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('itinerario_items', TRUE, $attributes);
    }

    function create_table_mercado_pago() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'account_token_private' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'account_token_private_sandbox' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'account_token_public' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'account_token_public_sandbox' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('mercado_pago', TRUE, $attributes);

        $data_mercadopago = array(
            'active' => 0,
            'sandbox' => 0
        );
        $this->db->insert('mercado_pago', $data_mercadopago);
    }

    function create_table_plano_sagtur() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'numero_ususarios' => array('type' => 'INT', 'constraint' => 11,'default' => 1 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('plano_sagtur', TRUE, $attributes);

        $this->db->insert('plano_sagtur', array('name' => 'BASIC - 1 Único Usuário', 'numero_ususarios' => 1));
        $this->db->insert('plano_sagtur', array('name' => 'PREMIUM - Até 5 Usuários', 'numero_ususarios' => 5));
        $this->db->insert('plano_sagtur', array('name' => 'GOLD - Usuários Ilimitados', 'numero_ususarios' => 10000));
    }

    public function alter_table_roominglist() {
        $fields = array(
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('room_list', $fields);
    }

    public function alter_table_room_list_hospedagem_hospede() {
        $fields = array(
            'itemId' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('room_list_hospedagem_hospede', $fields);
    }

    public function down() {}
}
