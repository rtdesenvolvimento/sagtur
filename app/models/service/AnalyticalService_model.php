<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AnalyticalService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('model/Analytical_model', 'Analytical_model');
    }

    public function registry_access($analyticalModel) {

        try {
            // Verificar se o servidor fornece o cabeçalho 'HTTP_X_FORWARDED_FOR'
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            // Verificar se o servidor fornece o cabeçalho 'HTTP_CLIENT_IP'
            elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
            // Caso contrário, obter o endereço IP padrão do servidor
            else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $analytical = new Analytical_model();

            $analytical->setIpAddress($ip);
            $analytical->setDate(date('Y-m-d H:i:s'));
            $analytical->setPageWeb(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
            $analytical->setPageCurrent(current_url());
            $analytical->setHttpMethod($_SERVER['REQUEST_METHOD']);
            $analytical->setHttpResponseCode($_SERVER['HTTP/1.1']);
            $analytical->setServerPort($_SERVER['SERVER_PORT']);
            $analytical->setServerName($_SERVER['SERVER_NAME']);
            $analytical->setServerSignature($_SERVER['SERVER_SIGNATURE']);
            $analytical->setServerSoftware($_SERVER['SERVER_SOFTWARE']);
            $analytical->setHttpReferer($_SERVER['HTTP_REFERER']);
            $analytical->setRequestTime($_SERVER['REQUEST_TIME']);

            $user_agent = $_SERVER['HTTP_USER_AGENT'];

            $analytical->setUserAgentInfo($user_agent);

            if (preg_match('/MSIE/i', $user_agent)) {
                $browser = 'Internet Explorer';
                $pattern = '/MSIE (.*?);/';
            } elseif (preg_match('/Firefox/i', $user_agent)) {
                $browser = 'Firefox';
                $pattern = '/Firefox\/(.*?);/';
            } elseif (preg_match('/Chrome/i', $user_agent)) {
                $browser = 'Google Chrome';
                $pattern = '/Chrome\/(.*?);/';
            } elseif (preg_match('/Safari/i', $user_agent)) {
                $browser = 'Safari';
                $pattern = '/Safari\/(.*?);/';
            } elseif (preg_match('/Opera/i', $user_agent)) {
                $browser = 'Opera';
                $pattern = '/Opera\/(.*?);/';
            }

            preg_match($pattern, $user_agent, $matches);
            $version = $matches[1];

            $analytical->setBrowserInformationName($browser);
            $analytical->setBrowserInformationVersion($version);

            $browser = get_browser(null, true);
            $os = $browser['platform'];

            $analytical->setOperatingSystemInformationName($os);
            $analytical->setInformationCookies(print_r($_COOKIE, true));
            $analytical->setInformationSession(print_r($_SESSION, true));
            $analytical->setSessionsId(session_id());

            //$analyticalModel = new AnalyticalDTO_model();

            if ($analyticalModel->type) {
                $analytical->setType($analyticalModel->type);
            }

            if ($analyticalModel->product_id) {
                $analytical->setProductId($analyticalModel->product_id);
            }

            if ($analyticalModel->programacao_id) {
                $analytical->setProgramacaoId($analyticalModel->programacao_id);
            }

            $analytical_id = $this->__save('analytical_access_record', $analytical->__toArray());

            $this->__confirmaTransacao();

            return $analytical_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

}