<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaViagemRespository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getAllDisponibilidade($filter = NULL) {

        //$filter = new ProgramacaoFilter_DTO_model();

        $this->db->select('agenda_viagem.id, 
        agenda_viagem.id as programacaoId, 
        vagas,
        preco, 
        dataSaida, 
        dataRetorno,
        horaSaida, 
        horaRetorno, 
        products.id as produtoId, 
        products.name, 
        products.details,
        products.category_id, 
        categories.use_day as use_day,
        categories.name as category_name,
        images_icon.icon as icon_img,
        products.promotion,  
        products.promo_price,  
        products.valores_condicoes, 
        products.oqueInclui,
        products.alert_stripe, 
        products.product_details, 
        products.itinerario, 
        products.alertar_polcas_vagas, 
        products.alertar_ultimas_vagas, 
        products.image,  
        products.precoExibicaoSite, 
        products.valor_pacote, 
        products.controle_estoque_hospedagem,  
        products.apenas_cotacao, 
        products.permitirListaEmpera,
        product_midia.url_video, 
        product_addresses.ponto_encontro,
        product_addresses.cep, 
        product_addresses.endereco, 
        product_addresses.numero, 
        product_addresses.complemento, 
        product_addresses.bairro, 
        product_addresses.cidade, 
        product_addresses.estado,
        products.color_agenda_id, 
        products.duracao_atividade, 
        products.tipo_duracao_atividade, 
        products.passeio, 
        products.desc_duracao,
        simboloMoeda');

        $this->db->join('products', 'products.id=agenda_viagem.produto');
        $this->db->join('product_midia', 'products.id=product_midia.product_id', 'left');
        $this->db->join('product_addresses', 'products.id=product_addresses.product_id', 'left');
        $this->db->join('categories', 'products.category_id=categories.id');
        $this->db->join('images_icon', 'images_icon.id=categories.image_icon_id', 'left');

        if ($filter->ocultarViagensPassadas) {

            $this->db->where('agenda_viagem.dataSaida >= "' . date('Y-m-d') . '" ');//SO VIAGEM QUE NAO PASSARAM
            $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO NAO PODE ARQUIVADOS

            if ($filter->apenas_agendas_ativa) {
                $this->db->where('agenda_viagem.active', 1);//TODO APENAS AGENDAS ATIVAS
            }

            if ($filter->enviar_site) {
                $this->db->where('products.enviar_site', '1');//APENAS DISPONIVEL NA LOJA
            }
        }

        if ($filter->produto) {
            $this->db->where("products.id", $filter->produto);
        }

        if ($filter->category) {
            $this->db->where("categories.id", $filter->category);
        }

        if ($filter->date_shedule) {
            $this->db->where("dataSaida", $filter->date_shedule);
        }

        if ($filter->mes) {
            $this->db->where("MONTH(dataSaida)", $filter->mes);
        }

        if ($filter->ano) {
            $this->db->where("YEAR(dataSaida)", $filter->ano);
        }

        if ($filter->dia) {
            $this->db->where("DAY(dataSaida)", $filter->dia);
        }

        if ($filter->status) {
            $this->db->where("products.unit", $filter->status);
        } else {
            $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        }

        if ($filter->destaque) {
            $this->db->where("products.destaque", 1);
        }

        if ($filter->promotion) {
            $this->db->where("products.promotion", 1);
        }

        if ($filter->embarque) {
            $this->db->join('local_embarque_viagem', 'products.id=local_embarque_viagem.produto AND local_embarque_viagem.status ="ATIVO"');
            $this->db->where('local_embarque_viagem.localEmbarque', $filter->embarque);
        }

        if ($filter->order_by_custom) {
            $this->db->order_by($filter->order_by_custom);
        } else {
            $this->db->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida');
        }

        if ($filter->group_by_date) {
            $this->db->group_by('agenda_viagem.dataSaida');
        } else if ($filter->group_data_website) {
            $this->db->group_by('YEAR(sma_agenda_viagem.dataSaida), MONTH(sma_agenda_viagem.dataSaida), agenda_viagem.produto');
        }

        if ($filter->limit) {
            $this->db->limit($filter->limit, isset($filter->offset) ? $filter->offset : 0);
        }

        $q = $this->db->get('agenda_viagem');

        if ($filter->limit_by_data) {
            return $this->disponibilidade_limit_data($q);
        } elseif ($filter->limit_by_category) {
            return $this->disponibilidade_limit_categoria($q);
        } else {
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
            return FALSE;
        }
    }

    private function disponibilidade_limit_categoria($q)
    {
        if ($q->num_rows() > 0) {
            $data = [];
            $category_count = [];
            foreach ($q->result() as $row) {
                $category_id = $row->category_id;
                if (!isset($category_count[$category_id])) {
                    $category_count[$category_id] = 0;
                }
                if ($category_count[$category_id] < $this->Settings->max_packages_line) {
                    $data[] = $row;
                    $category_count[$category_id]++;
                }
            }
            return $data;
        }

        return FALSE;
    }

    private function disponibilidade_limit_data($q) {

        if ($q->num_rows() > 0) {
            $data = [];
            $date_time_count = [];

            foreach ($q->result() as $row) {
                $month_year_key = date('Y-m', strtotime($row->dataSaida)); // Formato "YYYY-MM"

                if (!isset($date_time_count[$month_year_key])) {
                    $date_time_count[$month_year_key] = 0;
                }

                if ($date_time_count[$month_year_key] < $this->Settings->max_packages_line) {
                    $data[] = $row;
                    $date_time_count[$month_year_key]++;
                }
            }

            return $data;
        }

        return FALSE;
    }

    public function getTotalVendaAllByProgramacoes($programacoes) {
        $this->db->select('sma_sale_items.quantity as quantity, sale_status as status, products.controle_estoque_hospedagem, sale_items.tipoHospedagem, sale_items.programacaoId, sale_items.product_id as produto')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left');

        $this->db->where('sale_items.descontarVaga', 1);//TODO DESCONTAR VAGA

        $this->db->where('programacaoId in ('.$programacoes.')');

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }


    function getProgramacao($produto) {

        $this->db->select('agenda_viagem.*, products.controle_estoque_hospedagem');
        $this->db->join('products', 'products.id=agenda_viagem.produto');
        $this->db->where('produto',$produto);

        $this->db->order_by('dataSaida', 'asc');
        $this->db->order_by('horaSaida', 'asc');
        $this->db->order_by('dataRetorno', 'asc');
        $this->db->order_by('horaRetorno', 'asc');

        $q = $this->db->get('agenda_viagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProgramacaoById($id)
    {
        $q = $this->db->get_where('agenda_viagem', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProgramacaoByData($produtoId, $data, $hora, $idOrigem = false)
    {

        if ($idOrigem) {
            $this->db->where('id not in ('.$idOrigem.')');
        }

        $q = $this->db->get_where('agenda_viagem', array('produto' => $produtoId, 'dataSaida' => $data, 'horaSaida' => $hora, 'active'=> 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function isExistProgramacaoByData($produtoId, $data, $hora)
    {
        $q = $this->db->get_where('agenda_viagem', array('produto' => $produtoId, 'dataSaida' => $data, 'horaSaida' => $hora), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProximaProgramcaoByData($produtoId)
    {

        $today = date('Y-m-d');

        $this->db->where('dataSaida >=', $today)
            ->where('produto', $produtoId)
            ->where('active', 1)
            ->order_by('dataSaida', 'asc')
            ->limit(1);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalDeVendas($product_id, $status, $aprogramacaoId,  $tipoFaixa=null) {
        $this->db->select('sum(sma_sale_items.quantity) as quantity')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->group_by('sale_items.product_id');

        $this->db->where('sale_items.descontarVaga', 1);//TODO DESCONTAR VAGA

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id, 'sale_status' => $status , 'programacaoId' => $aprogramacaoId ));

        if ($q->num_rows() > 0) return $q->row()->quantity;

        return 0;
    }

}