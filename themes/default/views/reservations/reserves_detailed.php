<?php

function tirarAcentos($string){
    return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
} ?>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 3px;
        }

        th {
            padding: 3px;
            background: #e6e8e8;
            border-bottom: 1px solid #0b0b0b;
            border-top: 1px solid #0b0b0b;
        }
    </style>
<body>
<table style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;">
            <h4>Relatório de Reservas de Passeios e Atividades</h4>
        </td>
    </tr>
    </thead>
</table>

<table style="width: 100%;border-collapse:collapse;margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="2" style="border: 1px solid #0b0b0b;">Filtros da consulta</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($filters->product_id) {
        $product = $this->site->getProductByID($filters->product_id);
        ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Atividade:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$product->name;?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->data_saida) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Data saída:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$this->sma->hrsd($filters->data_saida);?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->data_retorno) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Data retorno:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$this->sma->hrsd($filters->data_retorno);?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->hora_saida) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Hora saída:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$filters->hora_saida;?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->hora_retorno) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Hora retorno:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$filters->hora_retorno;?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->status_sale) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Status da Venda:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=lang($filters->status_sale);?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->status_payment) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Status do Pagamento:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=lang($filters->status_payment);?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->biller) {
        $biller = $this->site->getCompanyByID($filters->biller); ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Vendedor:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$biller->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->customer) {
        $customer = $this->site->getCompanyByID($filters->customer); ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Passageiro:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$customer->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->guia) {
        $guia = $this->site->getCompanyByID($filters->guia); ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Guia:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$guia->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->motorista) {
        $motorista = $this->site->getCompanyByID($filters->motorista); ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Motorista:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$motorista->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->meio_divulgacao) {
        $meio_divulgacao = $this->Meiodivulgacao_model->getById($filters->meio_divulgacao);
        ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Como nós conheceu:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$meio_divulgacao->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->local_embarque) {
        $lc = $this->site->getLocalEmbarqueByID($filters->local_embarque);
        ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Embarque:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$lc->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->tipo_transporte) {
        $tt = $this->site->getTipoTransporteID($filters->tipo_transporte);
        ?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Veículo:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$tt->name;?></td>
        </tr>
    <?php } ?>

    <?php if ($filters->data_venda_de) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Data da Venda De:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$this->sma->hrsd($filters->data_venda_de);?></td>
        </tr>
    <?php } ?>
    <?php if ($filters->data_venda_ate) {?>
        <tr>
            <td style="background: #f5f5f5;text-align: right;width: 20%;border: 1px solid #0b0b0b;">Data da Venda Até:</td>
            <td style="text-align: left;width: 80%;border: 1px solid #0b0b0b;"><?=$this->sma->hrsd($filters->data_venda_ate);?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<table border="0" style="width: 100%;border-collapse:collapse;">
    <thead>
        <tr>
            <th style="width: 6%;text-align: left;">Data</th>
            <th style="width: 4%;text-align: left;">Hora</th>
            <th style="width: 10%;text-align: left;">Pick Up</th>
            <th style="width: 10%;text-align: left;">Atividade</th>
            <th style="width: 10%;text-align: left;">Veículo</th>
            <th style="width: 10%;text-align: left;">Serviço</th>
            <th style="width: 20%;text-align: left;">Pax</th>
            <th style="width: 9%;text-align: left;">Guia</th>
            <th style="width: 9%;text-align: left;">Motorista</th>
            <th style="width: 9%;text-align:right;">Valor</th>
            <th style="width: 9%;text-align:right;">Cobrar</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $faixas             = [];
    $strCobrar          = 0;
    $valorItem          = 0;
    $valorTotalItem     = 0;
    $valorTotalCobrar   = 0;
    $totalAcrescimo     = 0;
    $totalDesconto      = 0;

    foreach ($sales as $sale) {

        $itens = $this->ReservationsRepository_model->getAllInvoiceItems($filters, $sale->id);

        $totalAcrescimo += $sale->shipping;
        $totalDesconto += $sale->order_discount;

        foreach ($itens as $item) {

            $totalPagarItem = $item->subtotal;
            $totalPago      = $sale->paid;
            $acrescimo      = $sale->shipping;
            $desconto       = $sale->order_discount;
            $totalVenda     = $sale->grand_total - $acrescimo + $desconto;

            //calculo
            $totalRealPagoPorDependente = $totalPagarItem * (($sale->paid*100/$totalVenda)/100);
            $totalAberto                = $totalPagarItem - $totalRealPagoPorDependente;

            $valorTotalItem += $totalPagarItem;
            $valorTotalCobrar += $totalAberto;

            if ($totalAberto > 0) {
                $strCobrar      = $this->sma->formatMoney($totalAberto);
            } else {
                $strCobrar      = 'PAGO';
                $totalAberto    = 0;
            }

            $customer   = $this->site->getCompanyByID($item->customerClient);
            $guia       = $this->site->getCompanyByID($item->guia);
            $motorista  = $this->site->getCompanyByID($item->motorista);

            $phone      = $customer->phone;
            $whatsApp   = $customer->cf5;
            $email      = $customer->email;
            $telefoneEmergencia  = $customer->telefone_emergencia;
            $contato = '';

            if ($phone)  {
                $contato = $phone;
            }

            if ($whatsApp) {
                $contato  .= ' '.$whatsApp;
            }

            if ($telefoneEmergencia) {
                $contato .= '<br/>Emergência '.$telefoneEmergencia;
            }

            if ($email) {
                $contato .= '<br/>'.$email;
            }

            if ($sale->local_saida != null) {
                $localEmbarque = $sale->local_saida;
            } else {
                $localEmbarque = $item->localEmbarque;
            }

            if ($isColorBackground) {
                $background = "#f5f5f5";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }

            if ($faixas[$item->faixaNome]->qty == null) {
                $faixas[$item->faixaNome]->qty = 1;
                $faixas[$item->faixaNome]->name = $item->faixaNome;
            } else {
                $faixas[$item->faixaNome]->qty = $faixas[$item->faixaNome]->qty + 1;
            }
            ?>
            <tr>
                <td style="background: <?=$background?>;text-align: left;"><?=date('d/m/Y', strtotime($item->dtSaida))?></td>
                <td style="background: <?=$background?>;text-align: left;"><?=date('H:i', strtotime($item->hrRetorno));?></td>
                <td style="background: <?=$background?>"><?=$localEmbarque;?></td>
                <td style="background: <?=$background?>"><?=$item->product_name;?></td>
                <td style="background: <?=$background?>"><?=$item->tipo_transporte;?></td>
                <td style="background: <?=$background?>">
                    <?php if ($item->tipo_trajeto && $item->tipo_trajeto != 'Não definido') {?>
                        <?=$item->categoria;?><br/><small><?=$item->tipo_trajeto;?></small>
                    <?php } else {?>
                        <?=$item->categoria;?>
                    <?php } ?>
                </td>
                <td style="background: <?=$background?>"><span style="font-weight: bold;"><?=$customer->name.' ('.$item->faixaNome.')</span><br/>'.$contato;?></td>
                <td style="background: <?=$background?>">
                    <?php if ($guia){?>
                        <?=$guia->name;?>
                    <?php } ?>
                </td>
                <td style="background: <?=$background?>">
                    <?php if ($motorista){?>
                        <?=$motorista->name;?>
                    <?php } ?>
                </td>
                <td style="background: <?=$background?>;text-align:right;"><?=$this->sma->formatMoney($totalPagarItem);?></td>
                <td style="background: <?=$background?>;text-align:right;"><?=$strCobrar;?></td>
            </tr>
        <?php }?>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="7" style="text-align: right;">
            <span style="font-weight: bold;">Valor Total:</span>
        </td>
        <td style="text-align: right;" colspan="2">
            <?=$this->sma->formatMoney($valorTotalItem);?>
        </td>
        <td style="text-align: right;" colspan="2">
            <?=$this->sma->formatMoney($valorTotalCobrar);?>
        </td>
    </tr>
    <tr>
        <td colspan="7" style="text-align: right;">
            <span style="font-weight: bold;">Total Acrescimo:</span>
        </td>
        <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalAcrescimo);?></small></td>
        <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalAcrescimo);?></small></td>
    </tr>
    <tr>
        <td colspan="7" style="text-align: right;">
            <span style="font-weight: bold;">Total Desconto:</span>
        </td>
        <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalDesconto);?></small></td>
        <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalDesconto);?></small></td>
    </tr>
    <tr>
        <td colspan="7" style="text-align: right;">
            <span style="font-weight: bold;">Subtotal:</span>
        </td>
        <td style="text-align: right;background: #f5f5f5;" colspan="2"><span style="font-weight: bold;"><?=$this->sma->formatMoney($valorTotalItem+$totalAcrescimo-$totalDesconto);?></span></td>
        <td style="text-align: right;background: #f5f5f5;" colspan="2"><span style="font-weight: bold;"><?=$this->sma->formatMoney($valorTotalCobrar+$totalAcrescimo-$totalDesconto);?></span></td>
    </tr>
    <tr style="border-top: 1px solid #0b0b0b;">
        <td></td>
    </tr>
    <?php
    $total_passageiros = 0;
    foreach ($faixas as $faixa){
        $total_passageiros += $faixa->qty;
        ?>
        <tr>
            <td colspan="2" style="background: #f5f5f5;">
                <span style="font-weight: bold;"><?=strtoupper($faixa->name);?></span>:
            </td>
            <td colspan="1" style="text-align: left;background: #f5f5f5;">
                <?= str_pad($faixa->qty, 2, '0', STR_PAD_LEFT); ?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td colspan="2" style="background: #0b0b0b;color: #FFFFFF;">
            <span style="font-weight: bold;">TOTAL PAX:</span>
        </td>
        <td colspan="1" style="text-align: left;background: #0b0b0b;color: #FFFFFF;font-size: 14px;">
            <?= str_pad($total_passageiros, 2, '0', STR_PAD_LEFT); ?>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>