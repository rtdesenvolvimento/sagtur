<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update511 extends CI_Migration {

    public function up() {
        $this->db->update('settings',  array('is_email_queue' => 1), array('setting_id' => 1));
        $this->db->update('settings',  array('version' => '5.1.1'), array('setting_id' => 1));
    }

    public function down() {}
}
