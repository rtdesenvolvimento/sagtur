<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update364 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '3.6.4'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'receptive' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_products() {
        $fields = array(
            'is_text_from' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//exibir a partir de
            'text_unit' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => 'por pesssoa' ),//unidade de venda exibido
            'highlighted_text' => array('type' => 'VARCHAR', 'constraint' => '300' ),//texto em destaque
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
