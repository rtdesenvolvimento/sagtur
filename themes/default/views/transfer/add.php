<script type="text/javascript">
    $(document).ready(function () {
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });
</script>

<?php echo form_open_multipart("transfer/add", array('data-toggle' => 'validator', 'role' => 'form')) ?>
    <ul id="myTab" class="nav nav-tabs" style="text-align: center">
        <li class=""><a href="#abageral" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('detalhes_do_pacote') ?></a></li>
    </ul>
    <div class="tab-content">
        <!--Detalhes do pacote !-->
        <div id="abageral" class="tab-pane fade in">
            <div class="box">
                <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-map-signs"></i><?= lang('add_transfer'); ?></h2></div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("product_name", "name") ?>
                                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group all">
                                <?= lang("tipo_trajeto", "tipo_trajeto") ?>
                                <?php
                                $cbTrajeto[''] = "";
                                foreach ($tiposTrajeto as $tpTrajeto) $cbTrajeto[$tpTrajeto->id] = $tpTrajeto->name;
                                echo form_dropdown('tipo_trajeto', $cbTrajeto, '', 'class="form-control select" id="tipo_trajeto" required="required" placeholder="' . lang("select") . " " . lang("tipo_trajeto") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group all" id="div_category">
                                <?= lang("color_calendary", "color_calendary") ?>
                                <?php
                                $cor[''] = "";
                                foreach ($colors as $color) $cor[$color->id] = $color->name;
                                echo form_dropdown('color_calendary', $cor, '', 'class="form-control select" id="color_calendary" required="required" placeholder="' . lang("select") . " " . lang("color_calendary") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('disponivel_para_comercializacao');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('publicar_ativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('desativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_submit('next_add_transfer', $this->lang->line("next_add_transfer"), 'id="next_add_transfer" class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= form_close(); ?>

