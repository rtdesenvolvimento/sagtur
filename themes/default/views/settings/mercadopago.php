<script>
    $(document).ready(function () {
        $('#mercadopago_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('mercadopago_settings'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="<?= site_url('system_settings/mercadopago') ?>" class="toggle_down"><i
                            class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_pagseguro'); ?></span></a>
                </li>
                <li class="dropdown"><a href="<?= site_url('system_settings/juno') ?>" class="toggle_down"><i
                                class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_juno'); ?></span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('role' => 'form', 'id="mercadopago_form"');
                echo form_open("system_settings/mercadopago", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('1' => lang("sim"), '0' => lang("nao"));
                            echo form_dropdown('active', $yn, $mercadopago->active, 'class="form-control tip" required="required" id="active"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("account_token_private", "account_token_private"); ?>
                            <?php echo form_input('account_token_private', $mercadopago->account_token_private, 'class="form-control tip" id="account_token_private"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("account_token_public", "account_token_public"); ?>
                            <?php echo form_input('account_token_public', $mercadopago->account_token_public, 'class="form-control tip" id="account_token_public"'); ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("sandbox", "sandbox"); ?>
                            <?php
                            $yn = array('0' => lang("nao"), '1' => lang("sim"));
                            echo form_dropdown('sandbox', $yn, $mercadopago->sandbox, 'class="form-control tip" required="required" id="sandbox"');
                            ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("account_token_private_sandbox", "account_token_private_sandbox"); ?>
                            <?php echo form_input('account_token_private_sandbox', $mercadopago->account_token_private_sandbox, 'class="form-control tip" id="account_token_private_sandbox"'); ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("account_token_public_sandbox", "account_token_public_sandbox"); ?>
                            <?php echo form_input('account_token_public_sandbox', $mercadopago->account_token_public_sandbox, 'class="form-control tip" id="account_token_public_sandbox"'); ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label>Como pegar os dados para integração com MercadoPago</label>
                        <iframe width="383" height="384" src="https://www.youtube.com/embed/QZjRaZXKYsc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>