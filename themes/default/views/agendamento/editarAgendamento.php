<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_agendamento_header'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agendamento/editarAgendamento/" . $agendamento->id, $attrib); ?>
        <div class="modal-body">
            <div class="row" style="<?php if ($agendamento->isBloqueado) echo 'display: none;';?>">
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("status", "status") ?>
                        <?php
                        $opts = array(
                            'PAGO' => lang('PAGO'),
                            'AGUARDANDO_PAGAMENTO' => lang('AGUARDANDO_PAGAMENTO'),
                            'CANCELADA' => lang('CANCELADA'),
                            'BLOQUEADO' => lang('BLOQUEADO'),
                        );
                        echo form_dropdown('status', $opts, $agendamento->status , 'class="form-control" id="status" readonly required="required"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('servico', 'servico'); ?>
                        <div class="input-group">
                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                            </div>
                            <?php
                            $cbServicos[''] = lang('select').' '.lang('servico');
                            foreach ($servicos as $servico) {
                                $cbServicos[$servico->id] = $servico->name;
                            } ?>
                            <?= form_dropdown('produto', $cbServicos,  $agendamento->produto , 'class="form-control" readonly id="produto"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('valor', 'valor'); ?>
                        <?= form_input('valor', $agendamento->valor, 'class="form-control" id="valor" readonly required="required"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('date', 'dataDe'); ?>
                        <?= form_input('data', $agendamento->data, 'class="form-control" id="data" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('dataAte', 'dataAte'); ?>
                        <?= form_input('dataAte', date('Y-m-d'), 'class="form-control" id="dataAte" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('horaInicio', 'horaInicio'); ?>
                        <?= form_input('horaInicio', $agendamento->horaInicio, 'class="form-control" id="horaInicio" required="required"', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('horaTermino', 'horaTermino'); ?>
                        <?= form_input('horaTermino', $agendamento->horaTermino, 'class="form-control" id="horaTermino" required="required"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group all">
                        <?php echo form_checkbox('isBloqueado', '1', $agendamento->isBloqueado, 'id="isBloqueado"'); ?>
                        <label for="attributes" class="padding05"><?= lang('horario_bloqueado'); ?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('nome_completo', 'nomeCompleto'); ?>
                        <?= form_input('nomeCompleto', $agendamento->nomeCompleto, 'class="form-control" id="nomeCompleto" required="required"'); ?>
                    </div>
                </div>
            </div>
            <div id="divDadosCliente" style="<?php if ($agendamento->isBloqueado) echo 'display: none;';?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('celular', 'celular'); ?>
                            <?= form_input('celular', $agendamento->celular, 'class="form-control" id="celular"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('cpf', 'cpf'); ?>
                            <?= form_input('cpf', $agendamento->cpf, 'class="form-control" id="cpf"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('email', 'email'); ?>
                            <?= form_input('email', $agendamento->email, 'class="form-control" id="email"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_hidden('id', $agendamento->id); ?>
            <?php echo form_hidden('dataReserva', $agendamento->dataReserva); ?>
            <?php echo form_hidden('tipoCobranca', $agendamento->tipoCobranca); ?>
            <?php echo form_hidden('contaReceber', $agendamento->contaReceber); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarAgendamento', lang('editar_agendamento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $('#produto').attr('readonly', true);
        $('#status').attr('readonly', true);
        $("#cpf").mask("999.999.999-99");

        $('#isBloqueado').on('ifChecked', function (e) {
            $('#divDadosCliente').slideUp();
        });
        $('#isBloqueado').on('ifUnchecked', function (e) {$('#divDadosCliente').slideDown();});
    });
</script>
