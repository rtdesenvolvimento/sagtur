<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Cobrefacil {

    const VERSION = "v1";
    private $token = false;

    public $base_url = 'https://api.cobrefacil.com.br';
    public $base_url_sandbox = 'https://api.sandbox.cobrefacil.com.br';

    public $sandbox = true;
    public $app_id;
    public $app_secret;

    public function __construct() {}

    public function create_client($data_client) {
        $this->authenticate();

        return $this->post('/customers', $data_client);
    }
    public function update_client($data_client, $client_id) {
        $this->authenticate();

        return $this->put('/customers/'.$client_id, $data_client);
    }
    public function create_invoice($data_cobranca) {
        $this->authenticate();

        return $this->post('/invoices', $data_cobranca);
    }
    public function create_booklets($data_cobranca) {
        $this->authenticate();

        return $this->post('/payment-booklets', $data_cobranca);
    }
    public function search_customer_by_cpf($cpfCnpj) {
        $this->authenticate();

        return $this->get('/customers', '?limit=1&taxpayer_id='.$cpfCnpj);
    }
    public function search_customer_by_ein($cpfCnpj) {
        $this->authenticate();

        return $this->get('/customers', '?limit=1&ein='.$cpfCnpj);
    }
    public function cancel_invoice($invoice_id) {
        $this->authenticate();

        return $this->delete('/invoices/'.$invoice_id);
    }

    public function create_subscriptions($subscription_data) {
        $this->authenticate();

        return $this->post('/subscriptions', $subscription_data);
    }

    public function generate_invoice_subscription($subscription_id) {
        $this->authenticate();

        return $this->post('/subscriptions/'.$subscription_id.'/generate-invoice', array());
    }

    public function cancel_subscription($subscription_id) {
        $this->authenticate();

        return $this->post('/subscriptions/'.$subscription_id.'/cancel', array());
    }

    public function invoice_details_by_id($invoice_id) {
        $this->authenticate();

        return $this->get('/invoices/'.$invoice_id);
    }

    public function authenticate() {
        $params = array(
            'app_id' => $this->app_id,
            'secret' => $this->app_secret,
        );

        $request = $this->post('/authenticate', $params);

        if ($request->success)  $this->token = $request->data->token;
    }

    private function get($url, $option = false, $custom = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'.Cobrefacil::VERSION . $url . $option);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        if (!empty($custom)) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->token
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->error = [];
            $response->error[0] = new stdClass();
            $response->error[0]->description = 'Tivemos um problema ao processar a requisição.';
        }

        return $response;
    }
    private function post($url, $params)
    {
        $params = json_encode($params);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'. Cobrefacil::VERSION . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->token
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->error = [];
            $response->error[0] = new stdClass();
            $response->error[0]->description = 'Tivemos um problema ao processar a requisição.';
        }
        return $response;
    }

    private function put($url, $params)
    {
        $params = json_encode($params);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'. Cobrefacil::VERSION . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->token
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->error = [];
            $response->error[0] = new stdClass();
            $response->error[0]->description = 'Tivemos um problema ao processar a requisição.';
        }
        return $response;
    }

    private function delete($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ($this->sandbox ? $this->base_url_sandbox : $this->base_url) . '/'. Cobrefacil::VERSION . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->token
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if (empty($response)) {
            $response = new stdClass();
            $response->error = [];
            $response->error[0] = new stdClass();
            $response->error[0]->description = 'Tivemos um problema ao processar a requisição.';
        }
        return $response;
    }
}
