<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['hora_saida'] = 'Horário De';
$lang['hora_retorno'] = 'Até';
$lang['data_inicio'] = 'Data De';
$lang['data_final'] = 'Até';

$lang['domingo'] = 'Domingo';
$lang['segunda'] = 'Segunda-feira';
$lang['terca'] = 'Terça-feira';
$lang['quarta'] = 'Quarta-feira';
$lang['quinta'] = 'Quinta-feira';
$lang['sexta'] = 'Sexta-feira';
$lang['sabado'] = 'Sábado';
$lang['datas_pontuais'] = 'Datas Pontuais';
$lang['periodos'] = 'Períodos';
$lang['data_inicio_comercializacao'] = 'Data Início Comercialização';
$lang['data_horario_bloqueado'] = 'Datas e horários bloqueados';

$lang['start_date'] = 'Data De';
$lang['end_date'] = 'Até';

$lang['start_time'] = 'Hora De';
$lang['end_time'] = 'Até';

$lang['list_by_day'] = 'Lista por Dia';
$lang['list_by_week'] = 'Lista por Semana';
$lang['list_by_month'] = 'Lista por Mês';
$lang['list_by_year'] = 'Lista por Ano';

$lang['consulta_disponibilidade'] = 'Consulta Disponibilidade';
$lang['consulta_disponibilidade_header'] = 'Consulta Disponibilidade de Passeios e Atividades';
$lang['disponbilidade_passeio'] = 'Disponibilidade das Atividades';
$lang['data_availability_searh_day'] = 'Consultar';
$lang['select_day'] = 'Selecione o Dia';

$lang['tours'] = "Atividades";

$lang['adicionar_venda'] = 'Adicionar Venda';

$lang['data_saida'] = 'Data da Saída';
$lang['produto_ativo'] = "Pacotes Ativo";
$lang['produto_inativo'] = "Pacotes Inativo";
$lang['arquivado']  = "Arquivados";
$lang['reservas']                             = "Reservas";
$lang['vagas']                                = "Vagas";
$lang['vendas']                               = "Vendas";
$lang['disponivel']                           = "Disponível";
$lang['lista_espera']                         = "Lista Espera";
$lang['espera']                               = "Espera";
$lang['faturadas']                            = "Faturadas";
$lang['cotacao']    = "Cotações";
$lang['colo'] = "Infantil";
$lang['relatorios_servico'] = "Ver Relatórios do Pacote";
$lang['relatorio_geral_passageiros'] = "Relatório Geral de Passageiros";
$lang['lista_passageiros']  = "Lista de Passageiros (PDF)";
$lang['abrir_nova_data_servico'] = "Abrir Nova Data ou Frequência";
$lang['abrir_link_pacote']   = "Abrir Link do Pacote";
$lang['abrir_link_loja']    = "Abrir o Link da Loja";
$lang['exportar_dados_venda_excel'] = "Exportar Dados da Venda para Excel";
$lang['cotacoes'] = "Cotações";
$lang['poltrona'] = "Polt.";
$lang['adicionar_uma_nova_data'] = "ADICIONAR UMA NOVA DATA";
$lang['agendamento_editado_com_sucesso'] = "Agendamento Editado Com Sucesso";
$lang['agendamento_adicionado_com_sucesso'] = "Novo Agendamento Adicionado Com Sucesso";
$lang['configurar_valor'] = "Configurar o Valor";
$lang['detalhes_servico'] = "Detalhes dos Pacotes";
$lang['cupons_desconto'] = "Cupons de Desconto";
$lang['editar_data'] = "Editar Data";
$lang['atribuir_cupom'] = 'Atribuir Cupom ao Pacote';
$lang['cupom_editado_com_sucesso'] = 'Cupom Editado Com Sucesso';
$lang['habilitar_marcacao_assento'] = 'Habilitar Check-In dos Assentos';
$lang['header_type_product'] = 'Qual Tipo de Experiência que você está criando?';

$lang['edit_product'] 			= "Editar Pacote";
$lang['adicionar_cupom'] = "Adicionar Cupom de Desconto";
$lang['info_marcacao_assento'] = 'Configuração para Habilitar Marcação de Assentos na ÁREA DO CLIENTE';
$lang['habilitar_marcacao_area_cliente'] = 'Permitir que o Responsável Pela Compra Faça Marcação dos Assentos';
$lang['permitir_marcacao_dependente'] = 'Permitir que Dependentes Possam Fazer a Marcação dos Assentos de Todos os Passageiros da Venda';

$lang['data_inicio_marcacao'] = 'Data Para Liberar a Marcação';
$lang['data_final_marcacao'] = 'Data Final Para a Marcação';
$lang['data_do_dia']                          = "Saída";
$lang['data_ao_dia']                          = "Retorno";
$lang['editar_agendamento']                   = "Editar Agendamento";
$lang['adicionar_agenda'] = 'Programação de Atividades';
$lang['save_date'] = 'Salvar Data';

$lang['qr_code'] = 'Código QR Code';
$lang['qr_code_open'] = 'Compartilhar Link Via QR Code';
$lang['abrir_link_avaliacao'] = 'Link de Avaliação do Pacote';
$lang['link_avaliacao'] = 'Link de Avaliação do Pacote';
$lang['availability'] = 'Disponibilidade';
$lang['estoque'] = 'Estoque';
$lang['vendidos'] = 'Vendidos';
$lang['restam'] = 'Disponíveis';