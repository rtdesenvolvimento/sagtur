<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update398 extends CI_Migration {

    public function up() {

        $this->alter_table_assento_bloqueio();
        $this->alter_table_tipo_transporte_rodoviario_viagem();

        $this->db->update('settings',  array('version' => '3.9.8'), array('setting_id' => 1));
    }

    public function alter_table_assento_bloqueio() {
        $fields = array(
            'ip_address' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'sessions_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
         );

        $this->dbforge->add_column('assento_bloqueio', $fields);
    }

    public function alter_table_tipo_transporte_rodoviario_viagem() {
        $fields = array(
            'habilitar_selecao_link' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('tipo_transporte_rodoviario_viagem', $fields);
    }

    public function down() {

    }
}
