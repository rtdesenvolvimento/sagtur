<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['agenda_reserva'] = 'Consulta de Reservas';
$lang['reservations'] = 'Reservas';

$lang['start_date'] = 'Data Atividade De';
$lang['end_date'] = 'Até';

$lang['start_time'] = 'Hora De';
$lang['end_time'] = 'Até';

$lang['filters'] = 'Filtros da Consulta Clique Aqui';

$lang['situacao'] = 'Situação de Pagamento';
$lang['biller'] = 'Vendedor';
$lang['meio_divulgacao'] = 'Divulgação';
$lang['data_venda_de'] = 'Data da Venda De';
$lang['data_venda_ate'] = 'Até';

$lang['product'] = 'Atividade';

$lang['sale_status'] = 'Status';
$lang['customer'] = 'Passageiro';
$lang['list_reservations'] = 'Lista de Reservas';

$lang['edit_sale'] = "Editar Venda";
$lang['cancelar_sale'] = "Cancelar Venda";
$lang['whatsapp_send'] = "Abrir Conversa com Cliente no WhatsApp";
$lang['email_sale'] 			= "Enviar E-mail da Venda";

$lang['list_by_day'] = 'Lista por Dia';
$lang['list_by_week'] = 'Lista por Semana';
$lang['list_by_month'] = 'Lista por Mês';
$lang['list_by_year'] = 'Lista por Ano';

$lang['customer_filter'] = 'Passageiro';
$lang['search_reservations'] = 'Agenda de Reservas';
$lang['agenda_reservas'] = 'Agenda de Reservas';

$lang['no_sale_selected'] 		= "Nenhuma Venda selecionada. Por favor, selecione pelo menos uma Venda.";


$lang['buyer'] = 'Responsável';
$lang['orcamento']                     = "Cotação";
$lang['faturada']                      = "Faturada";
$lang['lista_espera'] 				   = "Lista Espera";

$lang['reserves_detailed']  = 'Relatório de Reservas Detalhado';
$lang['local_embarque']  = 'Embarque';
$lang['atribuir'] = 'Atribuir';
$lang['adicionar_local_embarque'] = "Adicionar Local de Embarque";

$lang['reservations_os'] = 'Ordem de Serviço';
$lang['origem'] = 'Origem';
$lang['guia'] = 'Guia';
$lang['motorista'] = 'Motorista';
$lang['tipo_transporte'] = 'Veículo';
$lang['export_sales_excel'] = 'Exportar Reservas Para Excel';