<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update429 extends CI_Migration {

    public function up() {

        $this->update_tb_settings();

        $this->db->update('settings',  array('version' => '4.2.9'), array('setting_id' => 1));
    }

    public function update_tb_settings() {
        $this->db->update('settings',  array('own_domain' => 1), array('setting_id' => 1));
    }

    public function down() {}
}
