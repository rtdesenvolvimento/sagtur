<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContractEventService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('model/EventContract_model', 'EventContract_model');
    }

    public function draft($user, $document_id) {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);
            $contract = $this->getContractByID($document_id);

            $event->setStatus("draft");
            $event->setEvent("Contrato Em Rascunho Criado: ".$contract->name);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function unsigned($user, $document_id, $integradora = 'plugsign', $link_document = '')
    {
        try {

            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $event->setStatus("issued");
            $event->setEvent("Contrato Emitido na ".strtoupper($integradora));
            $event->setDocumentId($document_id);
            $event->setLinkContract($link_document);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function rename_document($user, $document_id, $nomeAntigo, $nomeNovo)
    {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $event->setStatus("rename_document");
            $event->setEvent("Contrato Com Nome Alterado:<br/> De ".$nomeAntigo." para ".$nomeNovo);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function send_document_to($user, $document_id, $contact, $message)
    {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Dados: '.$contact.'<br/>';
            $data .= 'Mensagem: '.$message.'<br/>';

            $event->setStatus("send_document_to");
            $event->setEvent("Documento Reenviado: <br/>".$data);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function send_document_pdf($user, $document_id, $phone, $message)
    {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Telefone: '.$phone.'<br/>';
            $data .= 'Mensagem: '.$message.'<br/>';

            $event->setStatus("send_document_pdf");
            $event->setEvent("Documento Assinado Eviado em PDF Para: <br/>".$data);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function download_document($user, $document_id)
    {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $event->setStatus("viewed_document");
            $event->setEvent("Abriu Documento para Visualização em PDF");
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function to_sign_document($user, $document_id, $signatory) {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Nome: '.$signatory->name.'<br/>';
            $data .= 'Email: '.$signatory->email.'<br/>';
            $data .= 'Telefone: '.$signatory->phone.'<br/>';
            $data .= 'Public ID: '.$signatory->public_id.'<br/>';
            $data .= 'Link: '.$signatory->url_link.'<br/>';

            if ($signatory->is_biller) {
                $data .= 'É Responsável pela Agência: SIM<br/>';
            } else {
                $data .= 'É Responsável pela Agência: NÃO<br/>';
            }

            $event->setStatus("sent_signature");
            $event->setEvent("Documento Enviado para Assinatura: <br/>".$data);
            $event->setDocumentId($document_id);
            $event->setLinkContract($signatory->url_link);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function remove_signatory($user, $document_id, $signatory)
    {
        try {

            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Nome: '.$signatory->name.'<br/>';
            $data .= 'Email: '.$signatory->email.'<br/>';
            $data .= 'Telefone: '.$signatory->phone.'<br/>';
            $data .= 'Public ID: '.$signatory->public_id.'<br/>';

            if ($signatory->is_biller) {
                $data .= 'É Responsável pela Agência: SIM<br/>';
            } else {
                $data .= 'É Responsável pela Agência: NÃO<br/>';
            }

            $event->setStatus("signatory_removed");
            $event->setEvent("Signatário Removido: <br/>".$data);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function signed($user, $document_id, $signatory)
    {
        try {

            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Nome: '.$signatory->name.'<br/>';
            $data .= 'Email: '.$signatory->email.'<br/>';
            $data .= 'Telefone: '.$signatory->phone.'<br/>';
            $data .= 'Public ID: '.$signatory->public_id.'<br/>';

            if ($signatory->is_biller) {
                $data .= 'É Responsável pela Agência: SIM<br/>';
            } else {
                $data .= 'É Responsável pela Agência: NÃO<br/>';
            }

            $event->setStatus("signed");
            $event->setEvent("Documento Assinado Por: <br/>".$data);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function declined($user, $document_id, $signatory)
    {
        try {

            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $data = 'Nome: '.$signatory->name.'<br/>';
            $data .= 'Email: '.$signatory->email.'<br/>';
            $data .= 'Telefone: '.$signatory->phone.'<br/>';
            $data .= 'Public ID: '.$signatory->public_id.'<br/>';

            if ($signatory->is_biller) {
                $data .= 'É Responsável pela Agência: SIM<br/>';
            } else {
                $data .= 'É Responsável pela Agência: NÃO<br/>';
            }

            $event->setStatus("Declined");
            $event->setEvent("Documento Rejeitado Por: <br/>".$data);
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function canceled($user, $document_id)
    {
        try {
            $this->__iniciaTransacao();

            $event = $this->getEvent($user);

            $event->setStatus("canceled");
            $event->setEvent("Contrato Cancelado");
            $event->setDocumentId($document_id);

            $event_id = $this->__save('document_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function error_sale($sale_id, $error)
    {
        try {

            $this->__iniciaTransacao();

            $document = $this->getLastDocumentBySaleID($sale_id);

            if (!empty($document)) {
                $event = $this->getEvent($this->getUser($sale_id));

                $event->setStatus("error");
                $event->setEvent("Erro ao Gerar o Contrato da Venda: <br/> ".$error);
                $event->setDocumentId($document->id);

                $event_id = $this->__save('document_events', $event->__toArray());

                $this->__confirmaTransacao();

                return $event_id;
            }

            return false;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    private function getEvent($user): EventContract_model
    {
        $event = new EventContract_model();
        $nome_completo = $user->username;

        if (!isset($user->username)) {
            $nome_completo = 'Created by System';
        }

        $event->setDate(date('Y-m-d H:i:s'));
        $event->setUserId($user->id);
        $event->setUser($nome_completo);
        $event->setLog(print_r($_POST, true));

        return $event;
    }

    public function getLastDocumentBySaleID($saleID)
    {
        $this->db->select("documents.id as id");
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get_where('documents', array('sale_id' => $saleID), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }


    public function getUser($sale_id)
    {
        if ($this->session->userdata('user_id')) {
            $user = $this->site->getUser($this->session->userdata('user_id'));
            if ($user->biller_id) {
                return $user;
            } elseif ($sale_id) {
                return $this->site->getUserByBiller($this->site->getSaleByID($sale_id)->biller_id);
            } else {
                return $this->site->getUserByBiller($this->Settings->default_biller);
            }
        } elseif ($sale_id) {
            return $this->site->getUserByBiller($this->site->getSaleByID($sale_id)->biller_id);
        }
        return null;
    }

    public function getContractByID($id)
    {
        $q = $this->db->get_where('documents', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}