<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update356 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.5.6'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'theme_site' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => 'theme02' ),
            'view_logomarca_site' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'about' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
