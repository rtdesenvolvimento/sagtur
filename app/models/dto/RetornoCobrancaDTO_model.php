<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RetornoCobrancaDTO_model extends CI_Model
{
    public $erro;
    public $sucesso;
    public $datas = [];

    public function __construct() {
        parent::__construct();
    }

    public function adicionarFatura($dt) {
        $this->datas[count($this->datas)] = $dt;
    }
}
