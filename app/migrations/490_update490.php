<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update490 extends CI_Migration {

    public function up() {

        $this->update_shop_settings();

        $this->db->update('settings',  array('version' => '4.9.0'), array('setting_id' => 1));
    }

    public function update_shop_settings()
    {
        $this->db->update('shop_settings',  array('web_page_caching' => 0), array('id' => 1));
    }

    public function down() {}
}
