<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AutomovelMapa_model extends CI_Model
{

    public $automovel_id;
    public $andar;
    public $name;
    public $assento;
    public $note;
    public $habilitado;
    public $x;
    public $y;
    public $z;
    public $ordem;

    public function __construct() {
        parent::__construct();
    }

}
