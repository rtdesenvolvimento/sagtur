<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update475 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '4.7.5'), array('setting_id' => 1));
    }

    public function create_index()
    {
        $this->db->query('CREATE INDEX idx_sma_sale_events_sale_id ON sma_sale_events(sale_id);');
    }
    public function down() {}
}
