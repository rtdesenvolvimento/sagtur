<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update385 extends CI_Migration {

    public function up() {

        $this->insert_table_shop_settings();

        $this->alter_table_shop_settings();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.8.5'), array('setting_id' => 1));
    }
    public function alter_table_settings() {
        $fields = array(
            'shop_settings_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('shop_settings_id' => 1), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'head_code' => array('type' => 'LONGTEXT', 'default' => ''),
            'body_code' => array('type' => 'LONGTEXT', 'default' => ''),

        );
        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function insert_table_shop_settings() {

        $tipo_faixa_padrao_bebe = array(
            'shop_name' => 'CONFIGURAÇÕES DA LOJA VIRTUAL'
        );
        $this->db->insert('shop_settings', $tipo_faixa_padrao_bebe);
    }

    public function down() {}
}
