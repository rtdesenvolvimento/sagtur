<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update323 extends CI_Migration {

    public function up() {

        $this->alter_table_parcela();
        $this->alter_table_fatura();

        $this->db->update('settings',  array('version' => '2021.3.23'), array('setting_id' => 1));
    }

    function alter_table_parcela() {
        $fields = array(
            'numeroParcelaCC' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('parcela', $fields);
    }

    function alter_table_fatura() {
        $fields = array(
            'numeroParcelaCC' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('fatura', $fields);
    }

    public function down() {}
}
