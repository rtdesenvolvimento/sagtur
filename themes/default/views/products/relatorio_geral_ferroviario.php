<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }
        td {
            padding: 1px;
        }
        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>

<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
            <?php
            $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
            $nomeViagem = strtoupper($product->name).'<br/> Sáida '.$data. ' às '.$this->sma->hf($programacao->horaSaida);
            ?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
            <h4>RELATÓRIO GERAL DE ATIVIDADES DO DIA</h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;text-align: center;font-weight: bold;font-size: 13px;">
            <h5><?=$nomeViagem?></h5>
        </td>
    </tr>
    </thead>
</table>
<table border="0" style="width: 100%;">
    <thead>
    <tr>
        <th style="text-align: center;width: 5%;"><img src="<?php echo $assets;?>/images/assento.svg" style="height: 15px;width: 15px;" ></th>
        <th style="width: 20%;">Passageiro</th>
        <th style="width: 15%;text-align: center;">Nº Voucher.</th>
        <th style="width: 20%;text-align: center;">E-mail</th>
        <th style="width: 15%;text-align: center;">Contato</th>
        <th style="width: 20%;text-align: center;">Atividade</th>
        <th style="width: 6%;text-align: center;">Faixa</th>
        <?php if ($this->Settings->show_payment_report_shipment) {?>
            <th style="width: 10%;text-align: right">Receber</th>
        <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php


    $contador = 1;
    $isColorBackground = false;
    $totalPassageirosPagantes = 0;
    $totalCriancasColo = 0;

    foreach ($itens as $row) {

        $totalAbertoPorDependente = '';

        if ($row->ocultar_faixa_relatorio) {
            continue;
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin) {

            if ($row->payment_status == 'due') {
                continue;
            }
        }

        if ($this->Settings->show_payment_report_shipment) {

            $totalPagarItem = $row->subtotal;
            $totalPago      = $row->paid;
            $acrescimo      = $row->shipping;
            $desconto       = $row->order_discount;
            $totalVenda     = $row->grand_total - $acrescimo + $desconto;

            $totalRealPagoPorDependente = $totalPagarItem * (($row->paid*100/$totalVenda)/100);
            $totalAberto   =  $totalPagarItem - $totalRealPagoPorDependente;

            if ($totalAberto > 0) {
                $totalAbertoPorDependente = $this->sma->formatMoney($totalAberto);

                if ($acrescimo > 0) {
                    $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Acres: '.$this->sma->formatMoney($acrescimo).'</small>';
                }

                if ($desconto > 0) {
                    $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Desc:'.$this->sma->formatMoney($desconto).'</small>';
                }

                if ($acrescimo > 0 || $desconto > 0) {
                    $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Pagar:'.$this->sma->formatMoney($totalAberto + $acrescimo - $desconto).'</small>';
                }

            } else {
                $totalAbertoPorDependente = 'PAGO';
            }

        } else {
            if ($row->customer_id == $row->customerClient) {

                $totalEmAberto = $row->grand_total - $row->paid;

                if ($totalEmAberto > 0) {
                    $totalAbertoPorDependente = $this->sma->formatMoney($totalEmAberto);
                } else {
                    $totalAbertoPorDependente = 'PAGO';
                }
            } else {
                $totalAbertoPorDependente = 'PAGO';
            }
        }

        $objCustomer 	        = $this->site->getCompanyByID($row->customerClient);

        $customer                = $objCustomer->name;
        $poltrona                = $row->poltronaClient;
        $doenca_informar         = $objCustomer->doenca_informar;
        $social_name             = $objCustomer->social_name;

        $endereco               = '';
        $cep                    = $objCustomer->postal_code;
        $rua                    = $objCustomer->address;
        $cidade                 = $objCustomer->city;
        $estado                 = $objCustomer->state;
        $endereco               = '';

        if ($rua)     $endereco .= $rua.''.$cidade.'/'.$estado.' '.$cep;
        if ($cidade)  $endereco .= '<br/>'.$cidade.'/'.$estado.' '.$cep;
        if ($estado)  $endereco .= $estado.' '.$cep;
        if ($cep)     $endereco .= $cep;

        $contato = '';
        $email = $objCustomer->email;
        $whatsApp = $objCustomer->cf5;
        $phone = $objCustomer->phone;
        $telefone_emergencia = $objCustomer->telefone_emergencia;

        if ($whatsApp) {
            $contato = $whatsApp;
        }
        if ($phone) {
            $contato = $contato.' '.$phone;
        }

        if ($social_name) {
            $customer .= '<br/><small>Nome Social: ' . $social_name. '</small>';
        }

        if ($doenca_informar) {
            $customer .= '<br/><small>' . $doenca_informar. '</small>';
        }

        $background = "#eee";

        if ($isColorBackground) {
            $background = "#eee";
            $isColorBackground = false;
        } else {
            $background = "#ffffff";
            $isColorBackground = true;
        }
        ?>
        <tr style="background: <?=$background;?>">
            <?php if (!$row->descontarVaga) {
                $totalCriancasColo++;?>
                <td style="text-align: center;">(C) </td>
            <?php } else {
                $sContador = $contador < 10 ? '0'.$contador : $contador;
                $totalPassageirosPagantes++;
                ?>
                <td style="text-align: center;"><?=$poltrona;?></td>
                <?php $contador++?>
            <?php } ?>
            <td style="text-align: left;">
                <?=$customer;?>
                <?php if ($row->note_item){?>
                    <br/><small><?=strip_tags(html_entity_decode($row->note_item, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));?></small>
                <?php }?>
            </td>
            <td style="text-align: center;"><?=$row->reference_no;?></td>
            <td style="text-align: center;"><?=$email;?></td>
            <?php if ($whatsApp) {
                $phone = str_replace('(', '', str_replace(')', '', $whatsApp));
                $phone = str_replace('-', '', $phone);
                $phone = str_replace(' ', '', $phone); ?>
                <td style="text-align: center;"><a href="https://api.whatsapp.com/send?phone=55<?php echo $phone;?>" target="_blank"> <?php echo $contato;?></a></td>
            <?php } else {?>
                <td style="text-align: center;"><?=$contato;?></td>
            <?php }?>
            <td style="text-align: center;"><?=$row->product_name;?></td>
            <td style="text-align: center;"><?=$row->faixaNome;?></td>
            <?php if ($this->Settings->show_payment_report_shipment) {?>
                <td style="text-align: right;"><?php echo $totalAbertoPorDependente;?></td>
            <?php } ?>
        </tr>
    <?php }?>
    </tbody>

    <tfoot>
    <?php if ($this->Settings->show_payment_report_shipment) {?>
        <?=$col=8;?>
    <?php } else { ?>
        <?=$col=7;?>
    <?php } ?>
    <tr>
        <td colspan="<?=$col;?>"></td>
    </tr>
    <tr style="background: #eee;">
        <td colspan="<?=$col;?>" style="border-top: 1px solid #0b0b0b;">
            TOTAL DE PASSAGEIROS PAGANTES: <?=$totalPassageirosPagantes;?>
        </td>
    </tr>
    <tr style="background: #eee;">
        <td colspan="<?=$col;?>">
            TOTAL NÃO PAGANTES: <?=$totalCriancasColo;?>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>
