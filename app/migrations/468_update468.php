<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update468 extends CI_Migration {

    public function up() {

        $this->alter_table_message_group_participants();

        $this->db->update('settings',  array('version' => '4.6.8'), array('setting_id' => 1));
    }

    public function alter_table_message_group_participants()
    {
        $fields = array(
            'invitation' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('message_group_participants', $fields);
    }

    public function down() {}
}
