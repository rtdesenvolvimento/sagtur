
<div class="row" style="margin-bottom: 15px;">
<?php foreach ($programacoes as $programacao) {
    $transportes = $this->ProdutoRepository_model->getTransportesRodoviario($programacao->produto);
    $product = $this->site->getProductByID($programacao->produto);
    $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
    $nomeViagem = strtoupper($product->name).' | '.$data;
    ?>
    <?php foreach ($transportes as $transporte) {?>
        <?php if ($transporte->status == 'ATIVO'){?>
            <?php foreach ($transportes as $transporte) {?>
                <div class="col-sm-6">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-bus"></i> <span style="font-size: 11px;"><?= $nomeViagem; ?></span>
                            </h2>
                            <div class="box-icon" style="width: 100%;">
                                <div class="form-group all">
                                    <iframe src="<?php echo base_url('products/bus/'.$programacao->produto.'/'.$transporte->id.'/'.$programacao->id);?>" width="100%" frameborder="0" scrolling="no" id="iframe_onibus" height="800px"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>
</div>
