<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update404 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.0.4'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'sections_code' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
