<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update412 extends CI_Migration {

    public function up() {

        $this->alter_table_configuracao_cobranca_extra_assento_marcacao();

        $this->db->update('settings',  array('version' => '4.1.2'), array('setting_id' => 1));
    }

    public function alter_table_configuracao_cobranca_extra_assento_marcacao() {

        $this->db->query('ALTER TABLE sma_configuracao_cobranca_extra_assento DROP cor_id;');

        $fields = array(
            'cor_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );

        $this->dbforge->add_column('configuracao_cobranca_extra_assento_marcacao', $fields);
    }

    public function down() {}
}
