<?php defined('BASEPATH') OR exit('No direct script access allowed');

class HorarioTrabalho_model extends CI_Model
{
    const HORARIO_CAFE_DA_TARDE = '16:00:00';

    public $intervalo;
    public $duracao;
    public $segunda_manha_de;
    public $segunda_manha_ate;
    public $segunda_tarde_de;
    public $segunda_tarde_ate;
    public $terca_manha_de;
    public $terca_manha_ate;
    public $terca_tarde_de;
    public $terca_tarde_ate;
    public $quarta_manha_de;
    public $quarta_manha_ate;
    public $quarta_tarde_de;
    public $quarta_tarde_ate;
    public $quinta_manha_de;
    public $quinta_manha_ate;
    public $quinta_tarde_de;
    public $quinta_tarde_ate;
    public $sexta_manha_de;
    public $sexta_manha_ate;
    public $sexta_tarde_de;
    public $sexta_tarde_ate;
    public $sabado_manha_de;
    public $sabado_manha_ate;
    public $sabado_tarde_de;
    public $sabado_tarde_ate;
    public $domingo_manha_de;
    public $domingo_manha_ate;
    public $domingo_tarde_de;
    public $domingo_tarde_ate;

    public function __construct() {
        parent::__construct();
    }


    public function getHoraInicioManha($data) {

        $diaDaSemana = strftime("%A", strtotime($data));

        if ($diaDaSemana == 'segunda-feira' || $diaDaSemana == 'Monday') return $this->getSegundaManhaDe();
        if ($diaDaSemana == 'terça-feira' || $diaDaSemana == 'Tuesday' ) return $this->getTercaManhaDe();
        if ($diaDaSemana == 'quarta-feira' || $diaDaSemana == 'Wednesday') return $this->getQuartaManhaDe();
        if ($diaDaSemana == 'quinta-feira' || $diaDaSemana == 'Thursday') return $this->getQuintaManhaDe();
        if ($diaDaSemana == 'sexta-feira' || $diaDaSemana == 'Friday') return $this->getSextaManhaDe();
        if ($diaDaSemana == 'sábado' || $diaDaSemana == 'Saturday') return $this->getSabadoManhaDe();
        if ($diaDaSemana == 'domingo' || $diaDaSemana == 'Sunday') return $this->getDomingoManhaDe();

        return null;
    }

    public function getHoraFinalManha($data) {

        $diaDaSemana = strftime("%A", strtotime($data));

        if ($diaDaSemana == 'segunda-feira' || $diaDaSemana == 'Monday') return $this->getSegundaManhaAte();
        if ($diaDaSemana == 'terça-feira' || $diaDaSemana == 'Tuesday' ) return $this->getTercaManhaAte();
        if ($diaDaSemana == 'quarta-feira' || $diaDaSemana == 'Wednesday') return $this->getQuartaManhaAte();
        if ($diaDaSemana == 'quinta-feira' || $diaDaSemana == 'Thursday') return $this->getQuintaManhaAte();
        if ($diaDaSemana == 'sexta-feira' || $diaDaSemana == 'Friday') return $this->getSextaManhaAte();
        if ($diaDaSemana == 'sábado' || $diaDaSemana == 'Saturday') return $this->getSabadoManhaAte();
        if ($diaDaSemana == 'domingo' || $diaDaSemana == 'Sunday') return $this->getDomingoManhaAte();

        return null;
    }

    public function getHoraInicioTarde($data) {

        $diaDaSemana = strftime("%A", strtotime($data));

        if ($diaDaSemana == 'segunda-feira' || $diaDaSemana == 'Monday') return $this->getSegundaTardeDe();
        if ($diaDaSemana == 'terça-feira' || $diaDaSemana == 'Tuesday' ) return $this->getTercaTardeDe();
        if ($diaDaSemana == 'quarta-feira' || $diaDaSemana == 'Wednesday' ) return $this->getQuartaTardeDe();
        if ($diaDaSemana == 'quinta-feira' || $diaDaSemana == 'Thursday') return $this->getQuintaTardeDe();
        if ($diaDaSemana == 'sexta-feira' || $diaDaSemana == 'Friday') return $this->getSextaTardeDe();
        if ($diaDaSemana == 'sábado' || $diaDaSemana == 'Saturday') return $this->getSabadoTardeDe();
        if ($diaDaSemana == 'domingo' || $diaDaSemana == 'Sunday') return $this->getDomingoTardeDe();

        return null;
    }

    public function getHoraFinalTarde($data) {

        $diaDaSemana = strftime("%A", strtotime($data));

        if ($diaDaSemana == 'segunda-feira' || $diaDaSemana == 'Monday') return $this->getSegundaTardeAte();
        if ($diaDaSemana == 'terça-feira' || $diaDaSemana == 'Tuesday' ) return $this->getTercaTardeAte();
        if ($diaDaSemana == 'quarta-feira' || $diaDaSemana == 'Wednesday') return $this->getQuartaTardeAte();
        if ($diaDaSemana == 'quinta-feira' || $diaDaSemana == 'Thursday') return $this->getQuintaTardeAte();
        if ($diaDaSemana == 'sexta-feira' || $diaDaSemana == 'Friday') return $this->getSextaTardeAte();
        if ($diaDaSemana == 'sábado'  || $diaDaSemana == 'Saturday') return $this->getSabadoTardeAte();
        if ($diaDaSemana == 'domingo' || $diaDaSemana == 'Sunday') return $this->getDomingoTardeAte();
        return null;
    }

    /**
     * @return mixed
     */
    public function getIntervalo()
    {
        return $this->intervalo;
    }

    /**
     * @param mixed $intervalo
     */
    public function setIntervalo($intervalo)
    {
        $this->intervalo = $intervalo;
    }

    /**
     * @return mixed
     */
    public function getDuracao()
    {
        return $this->duracao;
    }

    /**
     * @param mixed $duracao
     */
    public function setDuracao($duracao)
    {
        $this->duracao = $duracao;
    }

    /**
     * @return mixed
     */
    public function getSegundaManhaDe()
    {
        return $this->segunda_manha_de;
    }

    /**
     * @param mixed $segunda_manha_de
     */
    public function setSegundaManhaDe($segunda_manha_de)
    {
        $this->segunda_manha_de = $segunda_manha_de;
    }

    /**
     * @return mixed
     */
    public function getSegundaManhaAte()
    {
        return $this->segunda_manha_ate;
    }

    /**
     * @param mixed $segunda_manha_ate
     */
    public function setSegundaManhaAte($segunda_manha_ate)
    {
        $this->segunda_manha_ate = $segunda_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getSegundaTardeDe()
    {
        return $this->segunda_tarde_de;
    }

    /**
     * @param mixed $segunda_tarde_de
     */
    public function setSegundaTardeDe($segunda_tarde_de)
    {
        $this->segunda_tarde_de = $segunda_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getSegundaTardeAte()
    {
        return $this->segunda_tarde_ate;
    }

    /**
     * @param mixed $segunda_tarde_ate
     */
    public function setSegundaTardeAte($segunda_tarde_ate)
    {
        $this->segunda_tarde_ate = $segunda_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getTercaManhaDe()
    {
        return $this->terca_manha_de;
    }

    /**
     * @param mixed $terca_manha_de
     */
    public function setTercaManhaDe($terca_manha_de)
    {
        $this->terca_manha_de = $terca_manha_de;
    }



    /**
     * @return mixed
     */
    public function getTercaTardeDe()
    {
        return $this->terca_tarde_de;
    }

    /**
     * @param mixed $terca_tarde_de
     */
    public function setTercaTardeDe($terca_tarde_de)
    {
        $this->terca_tarde_de = $terca_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getTercaTardeAte()
    {
        return $this->terca_tarde_ate;
    }

    /**
     * @param mixed $terca_tarde_ate
     */
    public function setTercaTardeAte($terca_tarde_ate)
    {
        $this->terca_tarde_ate = $terca_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getQuartaManhaDe()
    {
        return $this->quarta_manha_de;
    }

    /**
     * @param mixed $quarta_manha_de
     */
    public function setQuartaManhaDe($quarta_manha_de)
    {
        $this->quarta_manha_de = $quarta_manha_de;
    }

    /**
     * @return mixed
     */
    public function getQuartaManhaAte()
    {
        return $this->quarta_manha_ate;
    }

    /**
     * @param mixed $quarta_manha_ate
     */
    public function setQuartaManhaAte($quarta_manha_ate)
    {
        $this->quarta_manha_ate = $quarta_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getQuartaTardeDe()
    {
        return $this->quarta_tarde_de;
    }

    /**
     * @param mixed $quarta_tarde_de
     */
    public function setQuartaTardeDe($quarta_tarde_de)
    {
        $this->quarta_tarde_de = $quarta_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getQuartaTardeAte()
    {
        return $this->quarta_tarde_ate;
    }

    /**
     * @param mixed $quarta_tarde_ate
     */
    public function setQuartaTardeAte($quarta_tarde_ate)
    {
        $this->quarta_tarde_ate = $quarta_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getQuintaManhaDe()
    {
        return $this->quinta_manha_de;
    }

    /**
     * @param mixed $quinta_manha_de
     */
    public function setQuintaManhaDe($quinta_manha_de)
    {
        $this->quinta_manha_de = $quinta_manha_de;
    }

    /**
     * @return mixed
     */
    public function getQuintaManhaAte()
    {
        return $this->quinta_manha_ate;
    }

    /**
     * @param mixed $quinta_manha_ate
     */
    public function setQuintaManhaAte($quinta_manha_ate)
    {
        $this->quinta_manha_ate = $quinta_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getQuintaTardeDe()
    {
        return $this->quinta_tarde_de;
    }

    /**
     * @param mixed $quinta_tarde_de
     */
    public function setQuintaTardeDe($quinta_tarde_de)
    {
        $this->quinta_tarde_de = $quinta_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getQuintaTardeAte()
    {
        return $this->quinta_tarde_ate;
    }

    /**
     * @param mixed $quinta_tarde_ate
     */
    public function setQuintaTardeAte($quinta_tarde_ate)
    {
        $this->quinta_tarde_ate = $quinta_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getSextaManhaDe()
    {
        return $this->sexta_manha_de;
    }

    /**
     * @param mixed $sexta_manha_de
     */
    public function setSextaManhaDe($sexta_manha_de)
    {
        $this->sexta_manha_de = $sexta_manha_de;
    }

    /**
     * @return mixed
     */
    public function getSextaManhaAte()
    {
        return $this->sexta_manha_ate;
    }

    /**
     * @param mixed $sexta_manha_ate
     */
    public function setSextaManhaAte($sexta_manha_ate)
    {
        $this->sexta_manha_ate = $sexta_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getSextaTardeDe()
    {
        return $this->sexta_tarde_de;
    }

    /**
     * @param mixed $sexta_tarde_de
     */
    public function setSextaTardeDe($sexta_tarde_de)
    {
        $this->sexta_tarde_de = $sexta_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getSextaTardeAte()
    {
        return $this->sexta_tarde_ate;
    }

    /**
     * @param mixed $sexta_tarde_ate
     */
    public function setSextaTardeAte($sexta_tarde_ate)
    {
        $this->sexta_tarde_ate = $sexta_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getSabadoManhaDe()
    {
        return $this->sabado_manha_de;
    }

    /**
     * @param mixed $sabado_manha_de
     */
    public function setSabadoManhaDe($sabado_manha_de)
    {
        $this->sabado_manha_de = $sabado_manha_de;
    }

    /**
     * @return mixed
     */
    public function getSabadoManhaAte()
    {
        return $this->sabado_manha_ate;
    }

    /**
     * @param mixed $sabado_manha_ate
     */
    public function setSabadoManhaAte($sabado_manha_ate)
    {
        $this->sabado_manha_ate = $sabado_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getSabadoTardeDe()
    {
        return $this->sabado_tarde_de;
    }

    /**
     * @param mixed $sabado_tarde_de
     */
    public function setSabadoTardeDe($sabado_tarde_de)
    {
        $this->sabado_tarde_de = $sabado_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getSabadoTardeAte()
    {
        return $this->sabado_tarde_ate;
    }

    /**
     * @param mixed $sabado_tarde_ate
     */
    public function setSabadoTardeAte($sabado_tarde_ate)
    {
        $this->sabado_tarde_ate = $sabado_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getDomingoManhaDe()
    {
        return $this->domingo_manha_de;
    }

    /**
     * @param mixed $domingo_manha_de
     */
    public function setDomingoManhaDe($domingo_manha_de)
    {
        $this->domingo_manha_de = $domingo_manha_de;
    }

    /**
     * @return mixed
     */
    public function getDomingoManhaAte()
    {
        return $this->domingo_manha_ate;
    }

    /**
     * @param mixed $domingo_manha_ate
     */
    public function setDomingoManhaAte($domingo_manha_ate)
    {
        $this->domingo_manha_ate = $domingo_manha_ate;
    }

    /**
     * @return mixed
     */
    public function getDomingoTardeDe()
    {
        return $this->domingo_tarde_de;
    }

    /**
     * @param mixed $domingo_tarde_de
     */
    public function setDomingoTardeDe($domingo_tarde_de)
    {
        $this->domingo_tarde_de = $domingo_tarde_de;
    }

    /**
     * @return mixed
     */
    public function getDomingoTardeAte()
    {
        return $this->domingo_tarde_ate;
    }

    /**
     * @param mixed $domingo_tarde_ate
     */
    public function setDomingoTardeAte($domingo_tarde_ate)
    {
        $this->domingo_tarde_ate = $domingo_tarde_ate;
    }

    /**
     * @return mixed
     */
    public function getTercaManhaAte()
    {
        return $this->terca_manha_ate;
    }

    /**
     * @param mixed $terca_manha_ate
     */
    public function setTercaManhaAte($terca_manha_ate)
    {
        $this->terca_manha_ate = $terca_manha_ate;
    }


}