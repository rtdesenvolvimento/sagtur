<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProductsTravel()
    {
        $this->db->where('viagem', 1);
        $this->db->where('unit', 'Confirmado');

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTours()
    {
        $this->db->where('unit in ("Confirmado", "ATIVO")');
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProducts($status=null)
    {
        if ($status) $this->db->where('unit', $status);

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsConfirmedLink()
    {
        $this->db->select('products.*, categories.name as category');
        $this->db->where('unit', 'Confirmado');
        $this->db->join('categories', 'categories.id=products.category_id', 'left');
        $this->db->order_by('products.data_saida');
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsAtivo()
    {
        $this->db->where('unit', 'ATIVO');
        $this->db->where('price > 0');
        $this->db->select('products.*, categories.name as category');
        $this->db->join('categories', 'categories.id=products.category_id', 'left');
        $this->db->order_by('products.data_saida');

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsConfirmed()
    {
        //$this->db->where('unit', 'Confirmado');
        $this->db->select('products.*, categories.name as category');
        $this->db->join('categories', 'categories.id=products.category_id', 'left');
        $this->db->order_by('products.data_saida');

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategoryProducts($subcategory_id)
    {
        $q = $this->db->get_where('products', array('subcategory_id' => $subcategory_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getProductsByUnit($unit)
    {
        $this->db->select('warehouses.id, warehouses.name');
        $this->db->join('warehouses', 'warehouses.code=products.code', 'left');

        if ($unit)  {
            $this->db->where('unit', $unit);
        }
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariants($pid)
    {
        $q = $this->db->get_where('product_variants', array('id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptions($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptions_Hotel($pid)
    {
        $this->db->where("product_id = ".$pid." 
    	    AND ( UPPER(name) like UPPER('%Hotel%')  OR 
    	          UPPER(name) like UPPER('%Hostel%') OR 
    	          UPPER(name) like UPPER('%Pousada%') OR 
    	          UPPER(name) like UPPER('%Camping%')   )");$q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function getProductOptions_Onibus($pid)
    {

    	$this->db->where("product_id = ".$pid." AND ( UPPER(name) like UPPER('%Onibus%') OR UPPER(name) like UPPER('%Transporte%') ) ");
    	$q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
        	foreach (($q->result()) as $row) {
        		$data[] = $row;
        	}
        	return $data;
        }
        return FALSE;
    }

    public function getProductVariantsById($id) {
        $this->db->limit(1);
        $this->db->where("id", $id);

        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getOptionOnibus($id) {

        $this->db->limit(1);
        $this->db->where("product_id = ".$id." AND ( UPPER(name) like UPPER('%Onibus%') OR UPPER(name) like UPPER('%Transporte%') )");
        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function buscaTodosLocaisEmbarqueAgrupadosById($id) {
        $this->db->group_by('products.origem');
        $this->db->limit(1);
        $q = $this->db->get_where('products', array('id' => $id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionsWithWH($pid)
    {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductOptionsWithWarehouseProducts($warehouses, $pid)
    {
    	$this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
    	->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
    	->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
    	->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
    	->order_by('product_variants.id');
    	$q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid,'warehouses_products_variants.warehouse_id' => $warehouses, 'warehouses_products_variants.quantity !=' => NULL));
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}

    		return $data;
    	}
    }

    public function getProdutosAdicionais($pid, $status = 'ATIVO')
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, 
        ' . $this->db->dbprefix('products') . '.code as code, 
        ' . $this->db->dbprefix('combo_items') . '.quantity as qty, 
        ' . $this->db->dbprefix('products') . '.product_details, 
        ' . $this->db->dbprefix('products') . '.details, 
        ' . $this->db->dbprefix('products') . '.image, 
        ' . $this->db->dbprefix('products') . '.name as name, combo_items.comissao, combo_items.fornecedorId,
         ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');

        if ($status) {
            $this->db->where('products.unit', $status);
        }

        $this->db->order_by("products.name");

        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $status = 'ATIVO')
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, 
        ' . $this->db->dbprefix('products') . '.code as code, 
        ' . $this->db->dbprefix('combo_items') . '.quantity as qty, 
        ' . $this->db->dbprefix('products') . '.product_details, 
        ' . $this->db->dbprefix('products') . '.image, 
        ' . $this->db->dbprefix('products') . '.name as name, combo_items.comissao, combo_items.fornecedorId,
         ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');


        if ($status) {
            $this->db->where('products.unit', $status);
        }

        $q = $this->db->get_where('combo_items', array('product_id' => $pid));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItemById($produtoId, $servicoId)
    {
        $q = $this->db->get_where('combo_items', array('product_id' => $produtoId, 'item_id' => $servicoId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWithCategory($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('categories') . '.name as category')
        ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function has_purchase($product_id, $warehouse_id = NULL)
    {
        if($warehouse_id) { $this->db->where('warehouse_id', $warehouse_id); }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductDetails($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductDetail($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('tax_rates') . '.code as tax_rate_code, ' . $this->db->dbprefix('categories') . '.code as category_code, ' . $this->db->dbprefix('subcategories') . '.code as subcategory_code')
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('subcategories', 'subcategories.id=products.subcategory_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {
        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }
        return FALSE;
    }

    public function getAllWarehousesWithPQ($product_id)
    {
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id)
            ->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehousesById($warehouse_id, $product_id)
    {
    	$this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
    	->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
    	->where('warehouses_products.warehouse_id', $warehouse_id)
    	->where('warehouses_products.product_id', $product_id)
    	->group_by('warehouses.id');
    	$q = $this->db->get('warehouses');
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    	return FALSE;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductPhotosLimit($id, $limite = 50)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id), $limite);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return false;
    }

    public function getProductPhotosSite($id)
    {
        $q = $this->db->get_where("product_photos_site", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProduct($data,
                               $items,
                               $warehouse_qty,
                               $transportes,
                               $product_attributes,
                               $locaisEmbarque,
                               $tiposCobranca,
                               $condicoesPagamento,
                               $faixaEtariaValor,
                               $faixaEtariaValoresServicoAdicional,
                               $photos,
                               $tiposHospedagem,
                               $tiposHospedagemFaixaValor,
                               $midia_data,
                               $seo,
                               $addresses_data,
                               $servicos_inclui)
    {

        if ($this->db->insert('products', $data)) {

            $product_id = $this->db->insert_id();

            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {

                foreach ($warehouse_qty as $wh_qty) {
                    //if (isset($wh_qty['quantity']) && ! empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack'], 'avg_cost' => $data['cost']));

                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'real_unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        //}
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {

                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);
                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    $warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);

                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                        //$this->add_despesas_viagem($pr_attr, $data['code'], $warehouse_id, $option_id);
                    }

                    //if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity'], 'avg_cost' => $data['cost']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                    //}

                    foreach ($warehouse_qty as $warehouse) {
                        if (!$this->getWarehouseProductVariant($warehouse['warehouse_id'], $product_id, $option_id)) {
                            $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse['warehouse_id'], 'quantity' => 0));
                        }
                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
                }
            }

            if ($transportes) {
                foreach ($transportes as $transporte) {
                    $transporte['produto'] = $product_id;
                    $this->db->insert('tipo_transporte_rodoviario_viagem', $transporte);
                }
            }

            if ($locaisEmbarque) {
                foreach ($locaisEmbarque as $localEmbarque) {
                    $localEmbarque['produto'] = $product_id;
                    $this->db->insert('local_embarque_viagem', $localEmbarque);
                }
            }

            if ($tiposCobranca) {
                foreach ($tiposCobranca as $tipoCobranca) {
                    $tipoCobranca['produtoId'] = $product_id;
                    $this->db->insert('tipo_cobranca_produto', $tipoCobranca);
                }
            }

            if ($condicoesPagamento) {
                foreach ($condicoesPagamento as $condicaoPagamento) {
                    $condicaoPagamento['produtoId'] = $product_id;
                    $this->db->insert('tipo_cobranca_condicao_produto', $condicaoPagamento);
                }
            }

            if ($faixaEtariaValor) {
                foreach ($faixaEtariaValor as $faixaValor) {
                    $faixaValor['produto'] = $product_id;
                    $this->db->insert('valor_faixa_etaria', $faixaValor);
                }
            }

            if ($faixaEtariaValoresServicoAdicional) {
                foreach ($faixaEtariaValoresServicoAdicional as $faixaValor) {
                    $faixaValor['produto'] = $product_id;
                    $this->db->insert('faixa_etaria_adicional', $faixaValor);
                }
            }

           if ($tiposHospedagemFaixaValor) {
               foreach ($tiposHospedagemFaixaValor as $tipoHospedagemFaixaValor) {
                   $tipoHospedagemFaixaValor['produto'] = $product_id;
                   $this->db->insert('faixa_etaria_tipo_hospedagem', $tipoHospedagemFaixaValor);
               }
           }

            if ($tiposHospedagem) {
                foreach ($tiposHospedagem as $tipoHospedagem) {
                    $tipoHospedagem['produto'] = $product_id;
                    $this->db->insert('tipo_hospedagem_viagem', $tipoHospedagem);
                }
            }

            if ($servicos_inclui) {
                foreach ($servicos_inclui as $servico_incluso) {
                    $servico_incluso['product_id'] = $product_id;
                    $this->db->insert('product_services_include', $servico_incluso);
                }
            }

            if ($midia_data) {
                $midia_data['product_id'] = $product_id;
                $this->db->insert('product_midia', $midia_data);
            }

            if ($seo) {
                $seo['product_id'] = $product_id;
                $this->db->insert('product_seo', $seo);
            }

            if ($addresses_data) {
                $addresses_data['product_id'] = $product_id;
                $this->db->insert('product_addresses', $addresses_data);
            }

            return $product_id;
        }
        return false;
    }

    public function update_despesas_viagem($pr_attr, $code, $warehouse_id, $option_id) {

    	$item_code 		= $code;
    	$nome_variacao 	= $pr_attr['name'];
    	$real_unit_cost = $pr_attr['price'];
    	$fornecedor 	= $pr_attr['fornecedor'];
    	$item_quantity 	= 1;

    	$variacao = $this->getVariantByName( $nome_variacao );
        $lancamento_type = $variacao->lancamento_type;

        if ($lancamento_type == 1) {

                $reference = $this->site->getReference('po');
                $date = date('Y-m-d H:i:s');
                $status = 'pending';

                if ($fornecedor) {
                    $supplier_id = $fornecedor;
                    $supplier_details = $this->site->getCompanyByID($supplier_id);
                    $supplier = trim($supplier_details->name . ' (' . $nome_variacao . ')');
                } else {
                    $supplier_id = 622;
                    $supplier = "Despesas de Viagem";
                }

                $item_option = null;
                $total = 0;

                $product_details = $this->getProductByCode($item_code);
                $unit_cost = $real_unit_cost;
                $unit_cost = $this->sma->formatDecimal($unit_cost);
                $item_net_cost = $unit_cost;
                $subtotal = ($item_net_cost * $item_quantity);

                $products[] = array(
                    'product_code' => $item_code,
                    'product_id' => $product_details->id,
                    'product_name' => $nome_variacao,
                    'net_unit_cost' => $item_net_cost,
                    'option_id' => $option_id,
                    'unit_cost' => $this->sma->formatDecimal($item_net_cost),
                    'quantity' => $item_quantity,
                    'quantity_balance' => $item_quantity,
                    'warehouse_id' => $warehouse_id,
                    'subtotal' => $this->sma->formatDecimal($subtotal),
                    'real_unit_cost' => $real_unit_cost,
                    'date' => date('Y-m-d', strtotime($date)),
                    'status' => $status
                );

                $total += $item_net_cost * $item_quantity;
                $grand_total = $this->sma->formatDecimal($total);

                $this->db->update('purchase_items', $products[0], array('option_id' => $option_id));

                $data[] = array(
                    'reference_no' => $reference,
                    'date' => $date,
                    'supplier_id' => $supplier_id,
                    'supplier' => $supplier,
                    'option_id' => $option_id,
                    'warehouse_id' => $warehouse_id,
                    'total' => $this->sma->formatDecimal($total),
                    'grand_total' => $grand_total,
                    'status' => $status,
                    'created_by' => $this->session->userdata('user_id')
                );

                //$this->sma->print_arrays($products,$data);
                $this->db->update('purchases', $data[0], array('option_id' => $option_id));
            }

    }

    public function add_despesas_viagem($pr_attr, $code, $warehouse_id, $option_id) {

    	$item_code 		= $code;
    	$nome_variacao 	= $pr_attr['name'];
		$real_unit_cost = $pr_attr['price'];
		$fornecedor 	= $pr_attr['fornecedor'];
 		$item_quantity 	= 1;

		$variacao = $this->getVariantByName ( $nome_variacao );
        $lancamento_type = $variacao->lancamento_type;

        if ($lancamento_type == 1) {

                $reference = $this->site->getReference('po');
                $date = date('Y-m-d H:i:s');
                $status = 'pending';

                if ($fornecedor) {
                    $supplier_id = $fornecedor;
                    $supplier_details = $this->site->getCompanyByID($supplier_id);
                    $supplier = trim($supplier_details->name . ' (' . $nome_variacao . ')');
                } else {
                    $supplier_id = 622;
                    $supplier = "Despesas de Viagem";
                }

                $item_option = null;
                $total = 0;

                $product_details = $this->getProductByCode($item_code);
                $unit_cost = $real_unit_cost;
                $unit_cost = $this->sma->formatDecimal($unit_cost);
                $item_net_cost = $unit_cost;
                $subtotal = ($item_net_cost * $item_quantity);

                $products [] = array(
                    'product_code' => $item_code,
                    'product_id' => $product_details->id,
                    'product_name' => $nome_variacao,
                    'net_unit_cost' => $item_net_cost,
                    'option_id' => $option_id,
                    'unit_cost' => $this->sma->formatDecimal($item_net_cost),
                    'quantity' => $item_quantity,
                    'quantity_balance' => $item_quantity,
                    'warehouse_id' => $warehouse_id,
                    'subtotal' => $this->sma->formatDecimal($subtotal),
                    'real_unit_cost' => $real_unit_cost,
                    'date' => date('Y-m-d', strtotime($date)),
                    'status' => $status
                );

                $total += $item_net_cost * $item_quantity;
                $grand_total = $this->sma->formatDecimal($total);
                krsort($products);

                $data = array(
                    'reference_no' => $reference,
                    'date' => $date,
                    'supplier_id' => $supplier_id,
                    'option_id' => $option_id,
                    'supplier' => $supplier,
                    'warehouse_id' => $warehouse_id,
                    'total' => $this->sma->formatDecimal($total),
                    'grand_total' => $grand_total,
                    'status' => $status,
                    'created_by' => $this->session->userdata('user_id')
                );

                $this->addPurchase($data, $products);
            }

    }

    public function getPrductVariantByPIDandName($product_id, $name)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addAjaxProduct($data)
    {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }
        return false;
    }

    public function add_products($products = array())
    {
        if (!empty($products)) {
            foreach ($products as $product) {
                $variants = explode('|', $product['variants']);
                unset($product['variants']);
                if ($this->db->insert('products', $product)) {
                    $product_id = $this->db->insert_id();
                    foreach ($variants as $variant) {
                        if ($variant && trim($variant) != '') {
                            $vat = array('product_id' => $product_id, 'name' => trim($variant));
                            $this->db->insert('product_variants', $vat);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            //->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
            ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductAdicionaisNames($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            //->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
            ->group_by('products.id')->limit($limit);

        $this->db->where('viagem', 0);

        $this->db->where('products.unit', 'ATIVO');


        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductsForPrinting($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price')
            ->where("(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updateProduct($id,
                                  $data,
                                  $items,
                                  $warehouse_qty,
                                  $product_attributes,
                                  $locaisEmbarque,
                                  $tiposCobranca,
                                  $condicoesPagamento,
                                  $faixaEtariaValor,
                                  $faixaEtariaValoresServicoAdicional,
                                  $photos,
                                  $update_variants,
                                  $tiposHospedagem,
                                  $tiposHospedagemFaixaValor,
                                  $transportes,
                                  $midia_data,
                                  $seo,
                                  $addresses_data,
                                  $servicos_inclui)
    {
        if ($this->db->update('products', $data, array('id' => $id))) {

            $this->db->update('warehouses', array('name' => strtoupper($data['name'])), array('code' => $data['code']));

            $this->db->delete('valor_faixa_etaria', array('produto' => $id));
            $this->db->delete('local_embarque_viagem', array('produto' => $id));
            $this->db->delete('tipo_transporte_rodoviario_viagem', array('produto' => $id));
            $this->db->delete('tipo_cobranca_produto', array('produtoId' => $id));
            $this->db->delete('tipo_cobranca_condicao_produto', array('produtoId' => $id));
            $this->db->delete('tipo_hospedagem_viagem', array('produto' => $id));
            $this->db->delete('faixa_etaria_tipo_hospedagem', array('produto' => $id));
            $this->db->delete('combo_items', array('product_id' => $id));
            $this->db->delete('faixa_etaria_adicional', array('produto' => $id));

            $this->db->delete('product_addresses', array('product_id' => $id));
            $this->db->delete('product_seo', array('product_id' => $id));
            $this->db->delete('product_midia', array('product_id' => $id));
            $this->db->delete('product_services_include', array('product_id' => $id));

            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    $this->db->update('warehouses_products', array('rack' => $wh_qty['rack']), array('product_id' => $id, 'warehouse_id' => $wh_qty['warehouse_id']));
                }
            }

            if ($update_variants) $this->db->update_batch('product_variants', $update_variants, 'id');

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
                }
            }

            if ($transportes) {
                foreach ($transportes as $transporte) {
                    $transporte['produto'] = $id;
                    $this->db->insert('tipo_transporte_rodoviario_viagem', $transporte);
                }
            }

            if ($locaisEmbarque) {
                foreach ($locaisEmbarque as $localEmbarque) {
                    $localEmbarque['produto'] = $id;
                    $this->db->insert('local_embarque_viagem', $localEmbarque);
                }
            }

            if ($tiposCobranca) {
                foreach ($tiposCobranca as $tipoCobranca) {
                    $tipoCobranca['produtoId'] = $id;
                    $this->db->insert('tipo_cobranca_produto', $tipoCobranca);
                }
            }

            if ($condicoesPagamento) {
                foreach ($condicoesPagamento as $condicaoPagamento) {
                    $condicaoPagamento['produtoId'] = $id;
                    $this->db->insert('tipo_cobranca_condicao_produto', $condicaoPagamento);
                }
            }

            if ($faixaEtariaValor) {
                $this->db->delete('valor_faixa_etaria', array('produto' => $id));
                foreach ($faixaEtariaValor as $faixaValor) {
                    $faixaValor['produto'] = $id;
                    $this->db->insert('valor_faixa_etaria', $faixaValor);
                }
            }

            if ($faixaEtariaValoresServicoAdicional) {
                foreach ($faixaEtariaValoresServicoAdicional as $faixaValor) {
                    $faixaValor['produto'] = $id;
                    $this->db->insert('faixa_etaria_adicional', $faixaValor);
                }
            }

            if ($tiposHospedagemFaixaValor) {
                foreach ($tiposHospedagemFaixaValor as $tipoHospedagemFaixaValor) {
                    $tipoHospedagemFaixaValor['produto'] = $id;
                    $this->db->insert('faixa_etaria_tipo_hospedagem', $tipoHospedagemFaixaValor);
                }
            }

            if ($tiposHospedagem) {
                foreach ($tiposHospedagem as $tipoHospedagem) {
                    $tipoHospedagem['produto'] = $id;
                    $this->db->insert('tipo_hospedagem_viagem', $tipoHospedagem);
                }
            }

            if ($servicos_inclui) {
                foreach ($servicos_inclui as $servico_incluso) {
                    $servico_incluso['product_id'] = $id;
                    $this->db->insert('product_services_include', $servico_incluso);
                }
            }

            if ($midia_data) {
                $midia_data['product_id'] = $id;
                $this->db->insert('product_midia', $midia_data);
            }

            if ($seo) {
                $seo['product_id'] = $id;
                $this->db->insert('product_seo', $seo);
            }

            if ($addresses_data) {
                $addresses_data['product_id'] = $id;
                $this->db->insert('product_addresses', $addresses_data);
            }

            if ($product_attributes) {
            	$viagem = null;
                foreach ($product_attributes as $pr_attr) {

                	$idProduct_variants = $pr_attr['id'];
                	$deletar = $pr_attr['deletar'];

                	if ($idProduct_variants == 0 && $deletar == 'false') {

	                    $pr_attr['product_id'] = $id;
	                    $variant_warehouse_id = $pr_attr['warehouse_id'];

	                    unset($pr_attr['warehouse_id']);
	                    unset($pr_attr['id']);
	                    unset($pr_attr['deletar']);

	                    $this->db->insert('product_variants', $pr_attr);
	                    $option_id = $this->db->insert_id();
	                    //$this->add_despesas_viagem($pr_attr, $data['code'], $variant_warehouse_id, $option_id);

	                    if ($pr_attr['quantity'] != 0) {
	                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

	                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
	                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
	                        $unit_cost = $data['cost'];
	                        if ($tax_rate) {
	                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
	                                if ($data['tax_method'] == '0') {
	                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
	                                    $net_item_cost = $data['cost'] - $pr_tax_val;
	                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
	                                } else {
	                                    $net_item_cost = $data['cost'];
	                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
	                                    $unit_cost = $data['cost'] + $pr_tax_val;
	                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
	                                }
	                            } else {
	                                $net_item_cost = $data['cost'];
	                                $item_tax = $tax_rate->rate;
	                            }
	                        } else {
	                            $net_item_cost = $data['cost'];
	                            $item_tax = 0;
	                        }

	                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
	                        $item = array(
	                            'product_id' => $id,
	                            'product_code' => $data['code'],
	                            'product_name' => $data['name'],
	                            'net_unit_cost' => $net_item_cost,
	                            'unit_cost' => $unit_cost,
	                            'quantity' => $pr_attr['quantity'],
	                            'option_id' => $option_id,
	                            'quantity_balance' => $pr_attr['quantity'],
	                            'item_tax' => $item_tax,
	                            'tax_rate_id' => $tax_rate_id,
	                            'tax' => $tax,
	                            'subtotal' => $subtotal,
	                            'warehouse_id' => $variant_warehouse_id,
	                            'date' => date('Y-m-d'),
	                            'status' => 'received',
	                        );
	                        $this->db->insert('purchase_items', $item);

	                    }
                	} else if ($idProduct_variants > 0) {

                		if ($deletar == 'true') {

                			$id_options = $pr_attr['id'];
                			$warehouse_id = $pr_attr['warehouse_id'];

                			$this->db->delete('warehouses_products_variants', array('option_id' => $id_options, 'product_id' => $id, 'warehouse_id' => $warehouse_id));
                			$this->db->delete('purchase_items', array('option_id' => $id_options, 'product_id' => $id, 'warehouse_id' => $warehouse_id));
                			$this->db->delete('product_variants', array('id' => $id_options));
                			$this->db->delete('purchase_items', array('option_id' => $id_options));
                			$this->db->delete('purchases', array('option_id' => $id_options));
                		} else {
	                    	$id_options = $pr_attr['id'];
	                    	$warehouse_id = $pr_attr['warehouse_id'];
	                    	$pr_attr['product_id'] = $id;

	                    	//atualizar hotel
                            $sales = $this->getSalesItemBy($pr_attr['fornecedor'], $warehouse_id);

                            foreach ($sales as $sale) {

                                if ($pr_attr['fornecedor'] > 0 ) {
                                    if (strpos($pr_attr['name'], 'Hotel') !== false) {

                                        if ($sale->hotel > 0) {
                                            if ($sale->hotel != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }


                                        if ($sale->hotel2 > 0) {
                                            if ($sale->hotel2 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel2' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }


                                        if ($sale->hotel3 > 0) {
                                            if ($sale->hotel3 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel3' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }


                                        if ($sale->hotel4 > 0) {
                                            if ($sale->hotel4 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel4' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }

                                        if ($sale->hotel5 > 0) {
                                            if ($sale->hotel5 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel5' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }

                                        if ($sale->hotel6 > 0) {
                                            if ($sale->hotel6 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel6' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }

                                        if ($sale->hotel7 > 0) {
                                            if ($sale->hotel7 != $pr_attr['fornecedor']) {
                                                $atualizar_sale = array(
                                                    'hotel7' => $pr_attr['fornecedor'],
                                                );
                                                $this->db->update('sales', $atualizar_sale, array('id' => $sale->id));
                                            }
                                        }
                                    }
                                }

                            }

	                     	unset($pr_attr['warehouse_id']);
	                    	unset($pr_attr['id']);
	                    	unset($pr_attr['deletar']);

	                    	$this->db->update('warehouses_products_variants', array('quantity' => $pr_attr['quantity']), array('option_id' => $id_options, 'product_id' => $id, 'warehouse_id' => $warehouse_id));
	                    	$this->db->update('purchase_items', array('quantity' => $pr_attr['quantity']), array('option_id' => $id_options, 'product_id' => $id, 'warehouse_id' => $warehouse_id));
                            $this->db->update('purchase_items', array('quantity_balance' => $pr_attr['quantity']), array('option_id' => $id_options, 'product_id' => $id, 'warehouse_id' => $warehouse_id));
                            $this->db->update('product_variants', $pr_attr, array('id' => $id_options));

                            $this->update_despesas_viagem($pr_attr, $data['code'], $warehouse_id, $id_options);
                		}
                    }
                }
            }
            //$this->site->syncQuantity(NULL, NULL, NULL, $id);
            return true;
        } else {
            return false;
        }
    }

    public function getSalesItemBy($hotel = NULL, $warehouse_id = NULL)
    {

        $this->db->where('sales.warehouse_id', $warehouse_id);
        $this->db->or_like('hotel', $hotel);
        $this->db->or_like('hotel2', $hotel);
        $this->db->or_like('hotel3', $hotel);
        $this->db->or_like('hotel4', $hotel);
        $this->db->or_like('hotel5', $hotel);
        $this->db->or_like('hotel6', $hotel);
        $this->db->or_like('hotel7', $hotel);

        $q = $this->db->get('sales');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getPurchasedItemDetails($product_id, $warehouse_id, $option_id = NULL)
    {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasedItemDetailsWithOption($product_id, $warehouse_id, $option_id)
    {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePrice($data = array())
    {

        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        } else {
            return false;
        }
    }

    public function arquivar($id) {
        if ($this->db->update('products', array('unit' => 'Arquivado'), array('id' => $id)) ) {
            return true;
        }
        return false;
    }

    public function desarquivar($id) {
        if ($this->db->update('products', array('unit' => 'Confirmado'), array('id' => $id)) ) {
            return true;
        }

        return false;
    }

    public function deleteProduct($id, $code = NULL)
    {
        if ($this->db->delete('products', array('id' => $id)) &&
            $this->db->delete('warehouses_products', array('product_id' => $id)) &&
            $this->db->delete('warehouses_products_variants', array('product_id' => $id))) {

            if ($code) {
                $this->db->delete('warehouses', array('code' => $code));
            }
            return true;
        }
        return FALSE;
    }


    public function totalCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));

        return $q->num_rows();
    }

    public function getSubcategoryByID($id)
    {
        $q = $this->db->get_where('subcategories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCategoryById($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubcategoryByCode($code)
    {

        $q = $this->db->get_where('subcategories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubCategories()
    {
        $this->db->select('id as id, name as text');
        $q = $this->db->get("subcategories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesForCategoryID($category_id)
    {
        $this->db->select('id as id, name as text');
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id)
    {
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getAdjustmentByID($id)
    {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncAdjustment($data = array())
    {
        if(! empty($data)) {

            if ($purchase_item = $this->getPurchasedItemDetails($data['product_id'], $data['warehouse_id'], $data['option_id'])) {
                $quantity_balance = $data['type'] == 'subtraction' ? $purchase_item->quantity_balance - $data['quantity'] : $purchase_item->quantity_balance + $data['quantity'];
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            } else {
                $pr = $this->site->getProductByID($data['product_id']);
                $item = array(
                    'product_id' => $data['product_id'],
                    'product_code' => $pr->code,
                    'product_name' => $pr->name,
                    'net_unit_cost' => 0,
                    'unit_cost' => 0,
                    'quantity' => 0,
                    'option_id' => $data['option_id'],
                    'quantity_balance' => $data['type'] == 'subtraction' ? (0 - $data['quantity']) : $data['quantity'],
                    'item_tax' => 0,
                    'tax_rate_id' => 0,
                    'tax' => '',
                    'subtotal' => 0,
                    'warehouse_id' => $data['warehouse_id'],
                    'date' => date('Y-m-d'),
                    'status' => 'received',
                );
                $this->db->insert('purchase_items', $item);
            }

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id)
    {
        if ($adjustment = $this->getAdjustmentByID($id)) {

            if ($purchase_item = $this->getPurchasedItemDetails($adjustment->product_id, $adjustment->warehouse_id, $adjustment->option_id)) {
                $quantity_balance = $adjustment->type == 'subtraction' ? $purchase_item->quantity_balance + $adjustment->quantity : $purchase_item->quantity_balance - $adjustment->quantity;
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            }

            $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
            if ($adjustment->option_id) {
                $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
            }
        }
    }

    public function addAdjustment($data)
    {
        if ($this->db->insert('adjustments', $data)) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }

    public function updateAdjustment($id, $data)
    {
        $this->reverseAdjustment($id);
        if ($this->db->update('adjustments', $data, array('id' => $id))) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id)
    {
        $this->reverseAdjustment($id);
        if ( $this->db->delete('adjustments', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $product = $this->site->getProductByID($product_id);
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack, 'avg_cost' => $product->cost))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {

        $this->db->limit($limit, $start);
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($option_id)
    {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductWarehouseOptions($option_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function setRack($data)
    {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack']), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getSoldQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSoldQtyAll()
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQtyAll()
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariacaoTransportes()
    {
        $this->db->order_by("name", "asc");
        $this->db->where("group_name", "TRANSPORTES RODOVIÁRIOS");
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariacaoHospedagens()
    {
        $this->db->order_by("name", "asc");
        $this->db->where("group_name", "HOTÉIS");
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllVariants()
    {
        $this->db->order_by("name", "asc");
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantByName($name)
    {
    	$q = $this->db->get_where('variants', array('name' => $name), 1);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return FALSE;
    }


    public function getWarehouseProductVariant($warehouse_id, $product_id, $option_id = NULL)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function getSaleByPoltrona($poltrona, $variacao)
    {
        $q = $this->db->get_where('sales', array('reference_no' => $poltrona , 'reference_no_variacao' => $variacao ), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSaleByPoltronaByItemVendaForMarcacao($tipoTransporteId, $programacaoId)
    {
        $this->db->select('sale_items.sale_id as id, sale_items.customerClientName as customer, sale_items.poltronaClient as poltrona');
        $this->db->join('sales', 'sales.id = sale_items.sale_id', 'left');
        $this->db->where('sale_items.tipoTransporte', $tipoTransporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleByPoltronaByItemVenda($poltrona, $tipoTransporteId, $programacaoId)
    {
        $this->db->select('sale_items.*,  sales.*, sales.customer responsavelPassagem, sale_items.customerClientName customer ');
        $this->db->join('sales', 'sales.id = sale_items.sale_id', 'left');
        $this->db->where('sale_items.poltronaClient', $poltrona);
        $this->db->where('sale_items.tipoTransporte', $tipoTransporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTransferItems($transfer_id)
    {
        $q = $this->db->get_where('purchase_items', array('transfer_id' => $transfer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function addPurchase($data, $items)
    {
        if ($this->db->insert('purchases', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('purchase_items', $item);
                 if($item['option_id']) {
                    $this->db->update('product_variants', array('cost' => $item['real_unit_cost']), array('id' => $item['option_id'], 'product_id' => $item['product_id']));
                }
                if ($data['status'] == 'received' || $data['status'] == 'returned') {
                    $this->site->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['real_unit_cost']));
                }
            }

            if ($data['status'] == 'returned') {
                $this->db->update('purchases', array('return_purchase_ref' => $data['return_purchase_ref'], 'surcharge' => $data['surcharge'],'return_purchase_total' => $data['grand_total'], 'return_id' => $purchase_id), array('id' => $data['purchase_id']));
            }

            if ($data['status'] == 'received' || $data['status'] == 'returned') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

	public function addWarehouse($data)
    {
        if ($this->db->insert('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function addDisposicaoAcentosAutomovel($data)
    {
        if ($this->db->insert('disposicao_acentos_automovel', $data)) {
            return true;
        }
        return false;
    }

    public function addPlanta($data)
    {
        if ($this->db->insert('visao_automovel', $data)) {
            return true;
        }
        return false;
    }

    public function editarPlanta($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('visao_automovel', $data);
        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function deletarPosicaoAutomovel($id) {
        $this->db->delete('disposicao_acentos_automovel', array('id' => $id));
    }

    public function deletarPlanta($id) {
        $this->db->delete('visao_automovel', array('id' => $id));
    }

	public function getWarehouseByCode($code)
    {
        $q = $this->db->get_where('warehouses', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseById($code)
    {
        $q = $this->db->get_where('warehouses', array('id' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCustomerCompanies()
    {
    	$q = $this->db->get_where('companies', array('group_name' => 'supplier'));
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    	return FALSE;
    }

    public function getConfiguracoesExtraAssento()
    {
        $q = $this->db->get('configuracao_cobranca_extra_assento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxasPagamentoConfiguracao($produtoID, $tipoCobrancaID, $condicaoPagamentoID)
    {
        $this->db->join('tipo_cobranca_produto', 'tipo_cobranca_produto.tipoCobrancaId=tipo_cobranca_condicao_produto.tipoCobrancaId and tipo_cobranca_produto.produtoId = tipo_cobranca_condicao_produto.produtoId');

        $q = $this->db->get_where("tipo_cobranca_condicao_produto", array('tipo_cobranca_condicao_produto.produtoId' => $produtoID, 'tipo_cobranca_condicao_produto.tipoCobrancaId' => $tipoCobrancaID, 'tipo_cobranca_condicao_produto.condicaoPagamentoId' => $condicaoPagamentoID), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}
