<?php defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/PageRepository_model', 'PageRepository_model');

        if ($this->Settings->web_page_caching) {
            $this->output->cache($this->Settings->cache_minutes);
        }
    }

    public function index() {
        $this->page('', $this->Settings->default_biller);
    }

    public function page($titulo, $vendedorId = null) {
        try {

            $titulo = $this->sma->slug($titulo);

            if ($titulo == '')  {
                $this->show_page_error('Página não encontrada', 'Página de conteúdo não encontrada');
            }

            if ($vendedorId == null)  $vendedorId = $this->Settings->default_biller;

            $vendedor   = $this->site->getCompanyByID($vendedorId);
            $user       = $this->site->getUserByBillerActive($vendedorId);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $this->data['vendedor'] = $vendedor;
            $this->data['menusPai'] = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']= $this->MenuRepository_model->isPhotosGallery();
            $this->data['page']     = $this->PageRepository_model->getPageBySlug($titulo);

            $this->load->view($this->theme . 'website/page', $this->data);

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }
}