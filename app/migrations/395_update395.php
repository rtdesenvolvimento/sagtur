<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update395 extends CI_Migration {

    public function up() {

        $this->correcao_mapa_automovel();

        $this->insert_table_automovel_semi_leito_ld_44_lugares();
        $this->insert_table_automovel_semi_leito_ld_46_lugares();

        $this->insert_table_automovel_semi_leito_dd_64_lugares_1andar();
        $this->insert_table_automovel_semi_leito_dd_64_lugares_2andar();

        $this->db->update('settings',  array('version' => '3.9.5'), array('setting_id' => 1));
    }

    public function correcao_mapa_automovel() {
        $this->db->query('delete from sma_mapa_automovel where automovel_id = 1');
        $this->db->query('delete from sma_mapa_automovel where automovel_id = 2;');
    }

    public function insert_table_automovel_semi_leito_ld_44_lugares() {

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 7, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 8, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 9, 'name' => '07', 'assento' => 7, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 10, 'name' => '08', 'assento' => 8, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 11, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 12, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 13, 'name' => '09', 'assento' => 9, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 14, 'name' => '10', 'assento' => 10, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 15, 'name' => '11', 'assento' => 11, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 16, 'name' => '12', 'assento' => 12, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 17, 'name' => '13', 'assento' => 13, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 18, 'name' => '14', 'assento' => 14, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 19, 'name' => '15', 'assento' => 15, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 20, 'name' => '16', 'assento' => 26, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 21, 'name' => '17', 'assento' => 17, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 22, 'name' => '18', 'assento' => 18, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 23, 'name' => '19', 'assento' => 19, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 24, 'name' => '20', 'assento' => 20, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 25, 'name' => '21', 'assento' => 21, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 26, 'name' => '22', 'assento' => 22, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 27, 'name' => '23', 'assento' => 23, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 28, 'name' => '24', 'assento' => 24, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 29, 'name' => '25', 'assento' => 25, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 30, 'name' => '26', 'assento' => 26, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 31, 'name' => '27', 'assento' => 27, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 32, 'name' => '28', 'assento' => 28, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 33, 'name' => '29', 'assento' => 29, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 34, 'name' => '30', 'assento' => 30, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 35, 'name' => '31', 'assento' => 31, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 36, 'name' => '32', 'assento' => 32, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 37, 'name' => '33', 'assento' => 33, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 38, 'name' => '34', 'assento' => 34, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 39, 'name' => '35', 'assento' => 35, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 40, 'name' => '36', 'assento' => 36, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 41, 'name' => '37', 'assento' => 37, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 42, 'name' => '38', 'assento' => 38, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 43, 'name' => '39', 'assento' => 39, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 44, 'name' => '40', 'assento' => 40, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 45, 'name' => '41', 'assento' => 41, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 46, 'name' => '42', 'assento' => 42, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 47, 'name' => '43', 'assento' => 43, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 48, 'name' => '44', 'assento' => 44, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 49, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 50, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 51, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 52, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 53, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 54, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 55, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 56, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));
    }

    public function insert_table_automovel_semi_leito_ld_46_lugares() {

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 7, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 8, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 9, 'name' => '07', 'assento' => 7, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 10, 'name' => '08', 'assento' => 8, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 11, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 12, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 13, 'name' => '09', 'assento' => 9, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 14, 'name' => '10', 'assento' => 10, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 15, 'name' => '11', 'assento' => 11, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 16, 'name' => '12', 'assento' => 12, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 17, 'name' => '13', 'assento' => 13, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 18, 'name' => '14', 'assento' => 14, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 19, 'name' => '15', 'assento' => 15, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 20, 'name' => '16', 'assento' => 26, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 21, 'name' => '17', 'assento' => 17, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 22, 'name' => '18', 'assento' => 18, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 23, 'name' => '19', 'assento' => 19, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 24, 'name' => '20', 'assento' => 20, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 25, 'name' => '21', 'assento' => 21, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 26, 'name' => '22', 'assento' => 22, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 27, 'name' => '23', 'assento' => 23, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 28, 'name' => '24', 'assento' => 24, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 29, 'name' => '25', 'assento' => 25, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 30, 'name' => '26', 'assento' => 26, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 31, 'name' => '27', 'assento' => 27, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 32, 'name' => '28', 'assento' => 28, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 33, 'name' => '29', 'assento' => 29, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 34, 'name' => '30', 'assento' => 30, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 35, 'name' => '31', 'assento' => 31, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 36, 'name' => '32', 'assento' => 32, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 37, 'name' => '33', 'assento' => 33, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 38, 'name' => '34', 'assento' => 34, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 39, 'name' => '35', 'assento' => 35, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 40, 'name' => '36', 'assento' => 36, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 41, 'name' => '37', 'assento' => 37, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 42, 'name' => '38', 'assento' => 38, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 43, 'name' => '39', 'assento' => 39, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 44, 'name' => '40', 'assento' => 40, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 45, 'name' => '41', 'assento' => 41, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 46, 'name' => '42', 'assento' => 42, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 47, 'name' => '43', 'assento' => 43, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 48, 'name' => '44', 'assento' => 44, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 49, 'name' => '45', 'assento' => 45, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 50, 'name' => '46', 'assento' => 45, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 51, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 52, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 53, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 54, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 55, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 56, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));
    }

    public function insert_table_automovel_semi_leito_dd_64_lugares_1andar() {

        $this->db->insert('automovel', array('name' => 'Leito DD 64 Lugares', 'andares' => 2, 'total_assentos' => 64));
        $this->db->insert('tipo_transporte_rodoviario', array('name' => 'Leito DD 64 Lugares', 'automovel_id' => 3, 'totalPoltronas' => 64, 'numeroAndares' => 2, 'active' => 0));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 7, 'name' => '07', 'assento' => 7, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 8, 'name' => '08', 'assento' => 8, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 9, 'name' => '09', 'assento' => 9, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 10, 'name' => '10', 'assento' => 10, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 11, 'name' => '11', 'assento' => 11, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 12, 'name' => '12', 'assento' => 12, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 13, 'name' => '13', 'assento' => 13, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 14, 'name' => '14', 'assento' => 14, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 15, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 16, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 17, 'name' => '15', 'assento' => 15, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 18, 'name' => '16', 'assento' => 16, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 19, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 20, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 21, 'name' => '17', 'assento' => 17, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 22, 'name' => '18', 'assento' => 18, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 23, 'name' => '19', 'assento' => 19, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 24, 'name' => '20', 'assento' => 20, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 25, 'name' => '21', 'assento' => 21, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 26, 'name' => '22', 'assento' => 22, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 27, 'name' => '23', 'assento' => 23, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 28, 'name' => '24', 'assento' => 24, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 29, 'name' => '25', 'assento' => 25, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 30, 'name' => '26', 'assento' => 26, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 31, 'name' => '27', 'assento' => 27, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 32, 'name' => '28', 'assento' => 28, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 33, 'name' => '29', 'assento' => 29, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 34, 'name' => '30', 'assento' => 30, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 35, 'name' => '31', 'assento' => 31, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 36, 'name' => '32', 'assento' => 32, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 37, 'name' => '33', 'assento' => 33, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 38, 'name' => '34', 'assento' => 34, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 39, 'name' => '35', 'assento' => 35, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 40, 'name' => '36', 'assento' => 36, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 41, 'name' => '37', 'assento' => 37, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 42, 'name' => '38', 'assento' => 38, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 43, 'name' => '39', 'assento' => 39, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 44, 'name' => '40', 'assento' => 40, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 45, 'name' => '41', 'assento' => 41, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 46, 'name' => '42', 'assento' => 42, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 47, 'name' => '43', 'assento' => 43, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 48, 'name' => '44', 'assento' => 44, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 49, 'name' => '45', 'assento' => 45, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 50, 'name' => '46', 'assento' => 46, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 51, 'name' => '47', 'assento' => 47, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 52, 'name' => '48', 'assento' => 48, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 53, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 54, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 55, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 56, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));

    }

    function insert_table_automovel_semi_leito_dd_64_lugares_2andar() {
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 1, 'name' => '', 'assento' => 0, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 2, 'name' => '', 'assento' => 0, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 3, 'name' => '', 'assento' => 0, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 4, 'name' => '', 'assento' => 0, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 5, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 6, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 7, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 8, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 9, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 10, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 11, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 12, 'name' => '', 'assento' => 0, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 13, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 14, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 15, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 16, 'name' => '', 'assento' => 0, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 17, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 18, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 19, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 20, 'name' => '', 'assento' => 0, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 21, 'name' => '', 'assento' => 0, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 22, 'name' => '', 'assento' => 0, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 23, 'name' => '', 'assento' => 0, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 24, 'name' => '', 'assento' => 0, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 25, 'name' => '', 'assento' => 0, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 26, 'name' => '', 'assento' => 0, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 27, 'name' => '', 'assento' => 0, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 28, 'name' => '', 'assento' => 0, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 29, 'name' => '', 'assento' => 0, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 30, 'name' => '', 'assento' => 0, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 31, 'name' => '', 'assento' => 0, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 32, 'name' => '', 'assento' => 0, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 33, 'name' => '', 'assento' => 0, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 34, 'name' => '', 'assento' => 0, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 35, 'name' => '', 'assento' => 0, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 36, 'name' => '', 'assento' => 0, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 37, 'name' => '', 'assento' => 0, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 38, 'name' => '', 'assento' => 0, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 39, 'name' => '', 'assento' => 0, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 40, 'name' => '', 'assento' => 0, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => false));


        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 41, 'name' => '49', 'assento' => 49, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 42, 'name' => '50', 'assento' => 50, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 43, 'name' => '51', 'assento' => 51, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 44, 'name' => '52', 'assento' => 52, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 45, 'name' => '53', 'assento' => 53, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 46, 'name' => '54', 'assento' => 54, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 47, 'name' => '55', 'assento' => 55, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 48, 'name' => '56', 'assento' => 56, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 49, 'name' => '57', 'assento' => 57, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 50, 'name' => '58', 'assento' => 58, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 51, 'name' => '59', 'assento' => 59, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 52, 'name' => '60', 'assento' => 60, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 53, 'name' => '61', 'assento' => 61, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 54, 'name' => '62', 'assento' => 62, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 55, 'name' => '63', 'assento' => 63, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 2, 'ordem' => 56, 'name' => '64', 'assento' => 64, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => true));
    }

    function all() {
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 7, 'name' => '07', 'assento' => 7, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 8, 'name' => '08', 'assento' => 8, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 9, 'name' => '09', 'assento' => 9, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 10, 'name' => '10', 'assento' => 10, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 11, 'name' => '11', 'assento' => 11, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 12, 'name' => '12', 'assento' => 12, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 13, 'name' => '13', 'assento' => 13, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 14, 'name' => '14', 'assento' => 14, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 15, 'name' => '15', 'assento' => 15, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 16, 'name' => '16', 'assento' => 16, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 17, 'name' => '17', 'assento' => 17, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 18, 'name' => '18', 'assento' => 18, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 19, 'name' => '19', 'assento' => 19, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 20, 'name' => '20', 'assento' => 20, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 21, 'name' => '21', 'assento' => 21, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 22, 'name' => '22', 'assento' => 22, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 23, 'name' => '23', 'assento' => 23, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 24, 'name' => '24', 'assento' => 24, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 25, 'name' => '25', 'assento' => 25, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 26, 'name' => '26', 'assento' => 26, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 27, 'name' => '27', 'assento' => 27, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 28, 'name' => '28', 'assento' => 28, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 29, 'name' => '29', 'assento' => 29, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 30, 'name' => '30', 'assento' => 30, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 31, 'name' => '31', 'assento' => 31, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 32, 'name' => '32', 'assento' => 32, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 33, 'name' => '33', 'assento' => 33, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 34, 'name' => '34', 'assento' => 34, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 35, 'name' => '35', 'assento' => 35, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 36, 'name' => '36', 'assento' => 36, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 37, 'name' => '37', 'assento' => 37, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 38, 'name' => '38', 'assento' => 38, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 39, 'name' => '39', 'assento' => 39, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 40, 'name' => '40', 'assento' => 40, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 41, 'name' => '41', 'assento' => 41, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 42, 'name' => '42', 'assento' => 42, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 43, 'name' => '43', 'assento' => 43, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 44, 'name' => '44', 'assento' => 44, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 45, 'name' => '45', 'assento' => 45, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 46, 'name' => '46', 'assento' => 46, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 47, 'name' => '47', 'assento' => 47, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 48, 'name' => '48', 'assento' => 48, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 49, 'name' => '49', 'assento' => 49, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 50, 'name' => '50', 'assento' => 50, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 51, 'name' => '51', 'assento' => 51, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 52, 'name' => '52', 'assento' => 52, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 53, 'name' => '53', 'assento' => 53, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 54, 'name' => '54', 'assento' => 54, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 55, 'name' => '55', 'assento' => 55, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 3, 'andar' => 1, 'ordem' => 56, 'name' => '56', 'assento' => 56, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => true));
    }
    public function down() {}
}
