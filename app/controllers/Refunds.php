<?php defined('BASEPATH') or exit('No direct script access allowed');

class Refunds extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('repository/RefundsRepository_model', 'RefundsRepository_model');

        $this->lang->load('refunds', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->model('Refunds_model');
    }

    public function index() {

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('refunds'), 'page' => lang('refunds')),
            array('link' => '#', 'page' => lang('refund_cards')));
        $meta = array('page_title' => lang('refund_cards'), 'bc' => $bc);
        $this->page_construct('refunds/index', $meta, $this->data);
    }

    /* ------------------------------------ Refund Cards ---------------------------------- */
    public function getRefundCards()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gift_cards') . ".id as id, card_no, value, balance, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name) as created_by, customer, expiry, status", false)
            ->join('users', 'users.id=gift_cards.created_by', 'left')
            ->from("gift_cards")
            ->order_by('expiry', 'asc');

        $this->datatables->where("type" ,'refund');
        $this->datatables->where("status in ('due','partial','paid') ");

        $edit_link          = '<a href="'.base_url().'refunds/edit_refund_card/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-edit"></i> ' . lang("edit_refund_card") . '</a>';
        $cancel_fin_link    = anchor('refunds/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancel_refunds_card'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link     = anchor('refunds/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('refunds/adicionarPagamento/$1', '<i class="fa fa-plus"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $impressao_reembolso = '<a href="'.base_url().'refunds/pdf/$1" target="_blank"><i class="fa fa-print"></i> ' . lang('impressao_reembolso') . '</a>';
        $email_link = anchor('refunds/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_refund'), 'data-toggle="modal" data-target="#myModal"');

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $impressao_reembolso . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $pagamento_link . '</li>
                    <li>' . $add_pagamento_link . '</li>
                    <li class="divider"></li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $cancel_fin_link . '</li>
                </ul>
            </div></div>';

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        if ($this->input->get('id')) $id = $this->input->get('id');

        $refund_card = $this->site->getRefundCardByID($id);

        $created_by = $this->site->getUser($refund_card->created_by);

        $this->data['refund']   = $refund_card;
        $this->data['biller']   = $this->site->getCompanyByID($created_by->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($refund_card->customer_id);
        $this->data['inv']      = $this->site->getInvoiceByID($refund_card->origin_sale_id);

        $name = 'Reembolso '. $refund_card->card_no.'.pdf';
        $html = $this->load->view($this->theme . 'refunds/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'refunds/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
            $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
        }
    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $refund_card = $this->site->getRefundCardByID($id);

        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {

            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($refund_card->created_by);
            }

            $to = $this->input->post('to');
            $subject = $this->input->post('subject');

            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }

            $customer = $this->site->getCompanyByID($refund_card->customer_id);
            $inv      = $this->site->getInvoiceByID($refund_card->origin_sale_id);

            $this->load->library('parser');
            $parse_data = array(
                'card_no'           => $refund_card->card_no,
                'nome_cliente'      => $customer->name,
                'cpf'               => $customer->vat_no,
                'reference_no'      => $inv->reference_no,
                'data_venda'        => $this->sma->hrld($inv->date),
                'data_cancelamento' => $this->sma->hrld($refund_card->date),
                'motivo_cancelamento' => $inv->motivo_cancelamento,
                'value'             => $this->sma->formatMoney($refund_card->value),
                'balance'           => $this->sma->formatMoney($refund_card->value-$refund_card->balance),
                'saldo'             => $this->sma->formatMoney($refund_card->balance),
                'expiry'            => $this->sma->hrsd($refund_card->expiry),
                'site_link'         => base_url(),
                'site_name'         => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
            );
            $msg        = $this->input->post('note');
            $message    = $this->parser->parse_string($msg, $parse_data);

            $message = '<!DOCTYPE html>
                        <html lang="pt-br">
                            <head>
                                <meta charset="UTF-8">
                            </head>
                            <body>'.
                                $message.'
                            </body>
                        </html>';
            $attachment = $this->pdf($id, null, 'S');
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc)) {
            delete_files($attachment);
            $this->session->set_flashdata('message', lang("email_sent"));
            redirect("refunds");
        } else {

            $gift_temp = file_get_contents('./themes/default/views/email_templates/refund.html');

            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('refund') . ' (' . $refund_card->card_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );

            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $gift_temp),
            );

            $this->data['id']       = $id;
            $this->data['customer'] = $this->site->getCompanyByID($refund_card->customer_id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'refunds/email', $this->data);
        }
    }

    public function pagamentos($refund_id)
    {
        $fatura = $this->RefundsRepository_model->getFaturaByRefund($refund_id);

        $this->data['payments'] = $this->site->getPagamentosByFatura($fatura->faturaId);

        $this->load->view($this->theme . 'financeiro/pagamentos', $this->data);
    }

    public function historico($refund_id)
    {
        $fatura = $this->RefundsRepository_model->getFaturaByRefund($refund_id);

        $this->data['warehouses']           = $this->site->getAllWarehouses();
        $this->data['modal_js']             = $this->site->modal_js();
        $this->data['receitas']             = $this->site->getAllReceitasSuperiores();
        $this->data['tiposCobranca']        = $this->site->getAllTiposCobranca();
        $this->data['fatura']               = $this->site->getFaturaById($fatura->faturaId);
        $this->data['parcelas']             = $this->site->buscarHistoricoParcelasByFatura($fatura->faturaId);
        $this->data['condicoesPagamento']   = $this->site->getAllCondicoesPagamento();
        $this->data['movimentadores']       = $this->site->getAllContas();

        $this->load->view($this->theme . 'financeiro/historicoParcelas', $this->data);
    }

    public function adicionarPagamento($refund_id = null)
    {
        $this->load->model('sales_model');

        $faturaRefund = $this->RefundsRepository_model->getFaturaByRefund($refund_id);

        $this->load->helper('security');

        $fatura = $this->site->getFaturaById($faturaRefund->faturaId);;

        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('formapagamento', lang("formapagamento"), 'required');
        $this->form_validation->set_rules('movimentador', lang("movimentador"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            //if ($this->input->post('paid_by') == 'deposit') {
            //  $sale = $this->site->getInvoiceByID($this->input->post('sale_id'));
            // $customer_id = $sale->customer_id;
            //  if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
            // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            // redirect($_SERVER["HTTP_REFERER"]);
            //  }
            // } else {
            $customer_id = null;
            // }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $formaPagamento = $this->site->getFormaPagamentoById($this->input->post('formapagamento'));
            $faturaParcela  = $this->site->getParcelaOneByFatura($fatura->id);
            $parcela        = $this->site->getParcelaById($faturaParcela->parcela);
            $contaReceber   = $this->site->getContaReceberById($parcela->contareceberId);

            $payment = array(
                'date'              => $date,
                'sale_id'           => $contaReceber->sale,
                'formapagamento'    => $this->input->post('formapagamento'),
                'movimentador'      => $this->input->post('movimentador'),
                'tipoCobranca'      => $this->input->post('tipoCobrancaPagamento'),
                'fatura'            => $fatura->id,
                'pessoa'            => $fatura->pessoa,
                'amount'            => $this->input->post('amount-paid'),
                'paid_by'           => $formaPagamento->name,
                'reference_no'      => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'cheque_no'         => $this->input->post('cheque_no'),
                'cc_no'             => $this->input->post('gift_card_no'),
                'cc_holder'         => $this->input->post('pcc_holder'),
                'cc_month'          => $this->input->post('pcc_month'),
                'cc_year'           => $this->input->post('pcc_year'),
                'cc_type'           => $this->input->post('pcc_type'),
                'note'              => $this->input->post('note'),
                'created_by'        => $this->session->userdata('user_id'),
            );

            if ($fatura->tipooperacao == 'CREDITO') {
                $payment['tipo'] = 'Recebimento';
                $payment['type'] = 'received';
            } else {
                $payment['tipo'] = 'Pagamento';
                $payment['type'] = 'sent';
            }

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('adicionarPagamento')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addPayment($payment, $customer_id, $fatura->id)) {
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $tipoCobranca = $this->site->getTipoCobrancaById($fatura->tipoCobranca);

            $this->data['fatura']           = $fatura;
            $this->data['payment_ref']      = $this->site->getReference('pay');
            $this->data['tiposCobranca']    = $this->site->getAllTiposCobranca();
            $this->data['movimentadores']   = $this->settings_model->getAllContas();
            $this->data['formaspagamento']  = $this->settings_model->getAllFormasPagamento();
            $this->data['refund_card']      = $this->RefundsRepository_model->getRefundByFatura($fatura->id);
            $this->data['formaPagamentoId'] = $tipoCobranca->formapagamento;

            $this->data['modal_js']         = $this->site->modal_js();

            $this->load->view($this->theme . 'faturas/adicionarPagamento', $this->data);
        }
    }

    public function motivoCancelamento($id = null)
    {
        $this->data['id'] = $id;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'refunds/motivoCancelamento', $this->data);
    }

    public function cancelar($refund_id)
    {
        try {
            $this->Refunds_model->cancel($refund_id, $this->input->post('note', true));
            $this->session->set_flashdata('message', lang("refund_cancelado_com_sucesso"));
        } catch (Exception $erro) {
            $this->session->set_flashdata('error', $erro->getMessage());
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function add_refund_card()
    {
        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|is_unique[gift_cards.card_no]|required');
        $this->form_validation->set_rules('value', lang("value"), 'required');

        if ($this->form_validation->run() == true) {

            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;

            $data = array('card_no' => $this->input->post('card_no'),
                'value'         => $this->input->post('value'),
                'customer_id'   => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer'      => $customer,
                'balance'       => $this->input->post('value'),
                'note'          => $this->input->post('note', true),
                'expiry'        => $this->input->post('expiry'),
                'created_by'    => $this->session->userdata('user_id'),
                'type'          => 'refund',
            );
            $sa_data = array();
            $ca_data = array();
            if ($this->input->post('staff_points')) {
                $sa_points = $this->input->post('sa_points');
                $user = $this->site->getUser($this->input->post('user'));
                if ($user->award_points < $sa_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    redirect("refunds");
                }
                $sa_data = array('user' => $user->id, 'points' => ($user->award_points - $sa_points));
            } elseif ($customer_details && $this->input->post('use_points')) {
                $ca_points = $this->input->post('ca_points');
                if ($customer_details->award_points < $ca_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    redirect("refunds");
                }
                $ca_data = array('customer' => $this->input->post('customer'), 'points' => ($customer_details->award_points - $ca_points));
            }
            // $this->sma->print_arrays($data, $ca_data, $sa_data);
        } elseif ($this->input->post('add_refund_card')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("refunds");
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->Refunds_model->addRefundCard($data, $ca_data, $sa_data);
                $this->session->set_flashdata('message', lang("refund_card_added"));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect("refunds");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['users']        = $this->Refunds_model->getStaff();
            $this->data['page_title']   = lang("new_refund_card");
            $this->load->view($this->theme . 'refunds/add_refund_card', $this->data);
        }
    }

    public function edit_refund_card($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        $gc_details = $this->site->getRefundCardByID($id);

        if ($this->input->post('card_no') != $gc_details->card_no) {
            $this->form_validation->set_rules('card_no', lang("card_no"), 'is_unique[gift_cards.card_no]');
        }

        $this->form_validation->set_rules('value', lang("value"), 'required');

        if ($this->form_validation->run() == true) {

            $refund_card        = $this->site->getRefundCardByID($id);
            $customer_details   = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer           = $customer_details ? $customer_details->company : null;

            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => ($this->input->post('value') - $refund_card->value) + $refund_card->balance,
                'expiry' => $this->input->post('expiry'),
            );
        } elseif ($this->input->post('edit_refund_card')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("refunds");
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->Refunds_model->updateRefundCard($id, $data);
                $this->session->set_flashdata('message', lang("refund_card_updated"));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect("refunds");
        } else {
            $this->data['error']        = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['refund_card']  = $this->site->getRefundCardByID($id);
            $this->data['id']           = $id;
            $this->data['modal_js']     = $this->site->modal_js();
            $this->load->view($this->theme . 'refunds/edit_refund_card', $this->data);
        }
    }

    public function refund_card_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('refund_cards'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('card_no'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('value'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('expiry'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('note'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->site->getRefundCardByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->card_no);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->value);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->customer);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $this->sma->hrsd($sc->expiry));
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, lang($sc->status));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sc->note);

                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(50);

                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = lang('refund_cards_report').'_' . date('Y_m_d_H_i_s');

                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_refund_card_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}?>