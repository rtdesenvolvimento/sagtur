<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update411 extends CI_Migration {

    public function up() {

        $this->create_index();
        $this->alter_table_companies();
        $this->create_table_cor();
        $this->alter_table_configuracao_cobranca_extra_assento();

        $this->db->update('settings',  array('version' => '4.1.1'), array('setting_id' => 1));
    }

    public function create_index() {
        //TODO NAO ESQUECER PARTIU TRIP;

        $this->db->query('CREATE INDEX indxs_tipo_cobranca_produto ON sma_tipo_cobranca_produto (produtoId, tipoCobrancaId)');
        $this->db->query('CREATE INDEX indxs_tipo_cobranca_condicao_produto ON sma_tipo_cobranca_condicao_produto (produtoId, tipoCobrancaId,condicaoPagamentoId)');
        $this->db->query('CREATE INDEX indxs_cupom_desconto_item_cupom_programacao_id ON sma_cupom_desconto_item (cupom, programacao_id)');
        $this->db->query('CREATE INDEX indxs_sale_items_cupom_programacao ON sma_sale_items (cupom_id, programacaoId)');
    }

    public function alter_table_companies() {
        $fields = array(
            'ultimo_valor_extra_assento' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
        );

        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_configuracao_cobranca_extra_assento() {
        $fields = array(
            'cor_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );

        $this->dbforge->add_column('configuracao_cobranca_extra_assento', $fields);
    }

    public function create_table_cor() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cor', TRUE, $attributes);

        $this->db->insert('cor', array('name' => 'Purple', 'cor' => '#A020F0'));
        $this->db->insert('cor', array('name' => 'Red', 'cor' => '#FF0000	'));
        $this->db->insert('cor', array('name' => 'Orange	', 'cor' => '#FFA500'));
        $this->db->insert('cor', array('name' => 'PowderBlue', 'cor' => '#B0E0E6'));
        $this->db->insert('cor', array('name' => 'SeaGreen', 'cor' => '#2E8B57'));
        $this->db->insert('cor', array('name' => 'Blue', 'cor' => '#0000FF'));
        $this->db->insert('cor', array('name' => 'Black', 'cor' => '#000000'));
    }

    public function down() {}
}
