<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update482 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.8.2'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $this->db->update('shop_settings',  array('web_page_caching' => 0), array('id' => 1));
        $this->db->update('shop_settings',  array('cache_minutes' => 60), array('id' => 1));//1 hora
    }

    public function down() {}
}
