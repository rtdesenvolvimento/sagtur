<?php
function row_status($x)
{
    if ($x == null) {
        return '';
    } elseif ($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">' . lang($x) . '</span></div>';
    } elseif ($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">' . lang($x) . '</span></div>';
    } elseif ($x == 'partial' || $x == 'transferring') {
        return '<div class="text-center"><span class="label label-info">' . lang($x) . '</span></div>';
    } elseif ($x == 'due') {
        return '<div class="text-center"><span class="label label-danger">' . lang($x) . '</span></div>';
    } else {
        return '<div class="text-center"><span class="label label-default">' . lang($x) . '</span></div>';
    }
}
?>

<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#details" class="tab-grey"><?= lang('product_details') ?></a></li>

    <?php if ($Owner || $Admin) {?>
        <li class="" style="display: none;"><a href="#bus" class="tab-grey"><?= lang('onibus') ?></a></li>
        <li class=""><a href="#chart" class="tab-grey"><?= lang('chart') ?></a></li>
        <li class="" style="display: none;"><a href="#fluxoCaixa" class="tab-grey"><?= lang('fluxo_caixa') ?></a></li>
        <li class=""><a href="#sales" class="tab-grey"><?= lang('sales') ?></a></li>
        <li class=""  style="display: none;"><a href="#purchases" class="tab-grey"><?= lang('purchases') ?></a></li>
        <!--<li class=""><a href="#outras_despesas" class="tab-grey"><?= lang('Outras Despesas') ?></a></li>
        <li class=""><a href="#returns" class="tab-grey"><?= lang('returns') ?></a></li>!-->
    <?php } ?>
</ul>

<div class="tab-content">

    <div id="bus" class="tab-pane fade in">
        <iframe src="<?= site_url('products/bus/' . $product->id) ?>" width="100%" scrolling="auto" height="1200px"></iframe>
    </div>

    <script>
        $(document).ready(function () {
            function attachment(x) {
                if (x != null) {
                    return '<a href="' + site.base_url + 'assets/uploads/' + x + '" target="_blank"><i class="fa fa-chain"></i></a>';
                }
                return x;
            }

            var oTable = $('#EXPData').dataTable({
                "aaSorting": [[0, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= site_url('reports/getExpensesReportProducts/' . $product->id); ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[7];
                    nRow.className = "expense_link2";
                    return nRow;
                },
                "aoColumns": [{"mRender": fld}, null, null, {"mRender": currencyFormat}, null, null, {
                    "bSortable": false,
                    "mRender": attachment
                }],
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        total += parseFloat(aaData[aiDisplay[i]][3]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[3].innerHTML = currencyFormat(total);
                }
            }).fnSetFilteringDelay().dtFilter([
                {
                    column_number: 0,
                    filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                    filter_type: "text",
                    data: []
                },
                {column_number: 1, filter_default_label: "[<?=lang('reference');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('category');?>]", filter_type: "text", data: []},
                {column_number: 4, filter_default_label: "[<?=lang('note');?>]", filter_type: "text", data: []},
                {column_number: 5, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
            ], "footer");

        });

    </script>
    <div id="outras_despesas" class="tab-pane fade in">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('Outras Despesas'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="EXPData" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr class="active">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference"); ?></th>
                                    <th class="col-xs-2"><?= lang("category"); ?></th>
                                    <th class="col-xs-1"><?= lang("amount"); ?></th>
                                    <th class="col-xs-3"><?= lang("note"); ?></th>
                                    <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                    <th style="min-width:30px; width: 30px; text-align: center;"><i
                                                class="fa fa-chain"></i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>

                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="min-width:30px; width: 30px; text-align: center;">
                                        <i class="fa fa-chain"></i>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="details" class="tab-pane fade in">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-file-text-o nb"></i><?= $product->name; ?></h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="icon fa fa-tasks tip" data-placement="left"
                                   title="<?= lang("actions") ?>"></i>
                            </a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu"
                                aria-labelledby="dLabel">
                                <li>
                                    <a href="<?= site_url('products/edit/' . $product->id) ?>">
                                        <i class="fa fa-edit"></i> <?= lang('edit') ?>
                                    </a>
                                </li>
                                <!--<li><a href="<?= site_url('products/excel/' . $product->id) ?>"><i class="fa fa-download"></i> <?= lang('excel') ?></a></li>-->
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('product_details'); ?></p>

                        <div class="row">
                            <div class="col-sm-5">
                                <img src="<?= base_url() ?>assets/uploads/<?= $product->image ?>"
                                     alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>

                                <div id="multiimages" class="padding10">
                                    <?php if (!empty($images)) {
                                        echo '<a class="img-thumbnail" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                        foreach ($images as $ph) {
                                            echo '<div class="gallery-image"><a class="img-thumbnail" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                            if ($Owner || $Admin || $GP['products-edit']) {
                                                echo '<a href="#" class="delimg" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                                            }
                                            echo '</div>';
                                        }
                                    }
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="table-responsive">
                                    <table class="table table-borderless table-striped dfTable table-right-left">
                                        <tbody>
                                        <tr>
                                            <td colspan="2" style="background-color:#FFF;"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%;"><?= lang("barcode_qrcode"); ?></td>
                                            <td style="width:70%;"><?php echo $barcode ?>
                                                <?php $this->sma->qrcode('link', urlencode(site_url('products/view/' . $product->id)), 1); ?>
                                                <img
                                                        src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                                        alt="<?= $product->name ?>" class="pull-right"/></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("product_type"); ?></td>
                                            <td><?php echo lang($product->type); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("product_name"); ?></td>
                                            <td><?php echo $product->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("product_code"); ?></td>
                                            <td><?php echo $product->code; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("category"); ?></td>
                                            <td><?php echo $category->name; ?></td>
                                        </tr>
                                        <?php if ($product->subcategory_id) { ?>
                                            <tr>
                                                <td><?= lang("subcategory"); ?></td>
                                                <td><?php echo $subcategory->name; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><?= lang("product_unit"); ?></td>
                                            <td><?php echo $product->unit; ?></td>
                                        </tr>

                                        <?php if ($product->viagem == 1) { ?>
                                            <tr>
                                                <td><?= lang("origem"); ?></td>
                                                <td><?php echo $product->origem; ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= lang("destino"); ?></td>
                                                <td><?php echo $product->destino; ?></td>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12" style="display: none;">
                                        <?php if ($product->cf1 || $product->cf2 || $product->cf3 || $product->cf4 || $product->cf5 || $product->cf6) { ?>
                                            <h3 class="bold"><?= lang('custom_fields') ?></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-condensed dfTable two-columns">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang('custom_field') ?></th>
                                                        <th><?= lang('value') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    if ($product->cf1) {
                                                        echo '<tr><td>' . lang("pcf1") . '</td><td>' . $product->cf1 . '</td></tr>';
                                                    }
                                                    if ($product->cf2) {
                                                        echo '<tr><td>' . lang("pcf2") . '</td><td>' . $product->cf2 . '</td></tr>';
                                                    }
                                                    if ($product->cf3) {
                                                        echo '<tr><td>' . lang("pcf3") . '</td><td>' . $product->cf3 . '</td></tr>';
                                                    }
                                                    if ($product->cf4) {
                                                        echo '<tr><td>' . lang("pcf4") . '</td><td>' . $product->cf4 . '</td></tr>';
                                                    }
                                                    if ($product->cf5) {
                                                        echo '<tr><td>' . lang("pcf5") . '</td><td>' . $product->cf5 . '</td></tr>';
                                                    }
                                                    if ($product->cf6) {
                                                        echo '<tr><td>' . lang("pcf6") . '</td><td>' . $product->cf6 . '</td></tr>';
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php } ?>

                                        <?php if ((!$Supplier || !$Customer) && !empty($warehouses) && $product->type == 'standard') { ?>
                                            <h3 class="bold"><?= lang('warehouse_quantity') ?></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-condensed dfTable two-columns">
                                                    <thead>
                                                    <tr>
                                                        <th align="center"><?= lang('warehouse_name') ?></th>
                                                        <th align="center"><?= lang('quantity_disponivel') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td align="center"><?php echo $product->name; ?></td>
                                                        <td align="center">
                                                            <?php $vendas = $this->site->getTotalVendasRealizadasParaAhViagem($product->id);
                                                            $totalVendas = 0;
                                                            foreach ($vendas as $venda) $totalVendas = $venda->quantity;

                                                            if ($totalVendas == '') $totalVendas = 0;
                                                            ?>
                                                            <a href="<?php echo base_url();?>/reports/relFinanceiroGeralNivel2/<?php echo $product->code; ?>" target="_blank"> <strong><?php echo (int) $product->quantidadePessoasViagem  .' estoque | '.(int) $totalVendas.' vendas  | '. (int) ( $product->quantidadePessoasViagem - $totalVendas).' disponível';?></strong> </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <?php if ($Owner || $Admin) {?>
                                    <div class="col-sm-12" style="display: none;">

                                        <?php if (!empty($options)) { ?>
                                            <h3 class="bold"><?= lang('product_variants_quantity'); ?></h3>
                                            <div class="table-responsive">
                                                <table
                                                        class="table table-bordered table-striped table-condensed dfTable">
                                                    <thead>
                                                    <tr class="active">
                                                        <th><?= lang('name') ?></th>
                                                        <th><?= lang('fornecedor') ?></th>
                                                        <th><?= lang('totalPoltrona') ?></th>
                                                        <th><?= lang('totalPessoasConsiderar') ?></th>
                                                        <th><?= lang('localizacao') ?></th>
                                                        <th><?= lang('margem') ?></th>
                                                        <th><?= lang('price') ?></th>
                                                        <th><?= lang('total') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $total = 0;
                                                    $custo = 0;

                                                    foreach ($options as $option) {
                                                        if ($option->name != 'Passagem') {
                                                            $total = $total + $option->price;
                                                            $custo = $custo + $option->totalPreco;
                                                            $supplier_details_name = '-';
                                                            $totalPoltrona = $option->totalPoltrona;
                                                            $totalPessoasConsiderar = $option->totalPessoasConsiderar;

                                                            if ($totalPoltrona <= 0) {
                                                                $totalPoltrona = '-';
                                                            }

                                                            if ($totalPessoasConsiderar <= 0) {
                                                                $totalPessoasConsiderar = '-';
                                                            }

                                                            if ($option->fornecedor > 0) {
                                                                $supplier_details = $this->site->getCompanyByID($option->fornecedor);
                                                                $supplier_details_name = $supplier_details->company.' ('.$supplier_details->name.')';
                                                            }

                                                            echo '
															<tr>
																<td class="text-center">' . $option->name . '</td>
																<td class="text-center">' . $supplier_details_name . '</td>
																<td class="text-center">' . $totalPoltrona . '</td>
																<td class="text-center">' . $totalPessoasConsiderar . '</td>
																<td class="text-center">' . $option->localizacao . '</td>
																<td class="text-center">' . $this->sma->formatQuantity($option->cost) . '</td>
                                                                <td class="text-right">' . $this->sma->formatMoney($option->price) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($option->totalPreco) . '</td>
															</tr>';
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr class="active">
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th class="text-right"><?= $this->sma->formatMoney($custo) ?></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                                <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                                <?= $product->itinerario ? '<div class="panel panel-info"><div class="panel-heading">' . lang('Roteiro') . '</div><div class="panel-body">' . $product->itinerario . '</div></div>' : ''; ?>
                                <?= $product->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading">' . lang('O que inclui') . '</div><div class="panel-body">' . $product->oqueInclui . '</div></div>' : ''; ?>
                                <?= $product->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading">' . lang('Valores e condições') . '</div><div class="panel-body">' . $product->valores_condicoes . '</div></div>' : ''; ?>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="btn-group btn-group-justified">

                                <div class="btn-group">
                                    <a href="<?= site_url('products/pdf/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('pdf') ?>">
                                        <i class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?= site_url('products/edit/' . $product->id) ?>"
                                       class="tip btn btn-warning tip" title="<?= lang('edit_product') ?>">
                                        <i class="fa fa-edit"></i> <span
                                                class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <?php if ($Owner || $Admin) {?>
                        <div class="buttons">
                            <div class="btn-group btn-group-justified">
                                <?php if ($product->viagem == 1) {
                                    $contador = 0; ?>
                                    <?php foreach ($hoteis as $hotel) {

                                        $fornecedor         = $hotel->fornecedor;
                                        $supplier_details 	= $this->site->getCompanyByID($fornecedor);
                                        $supplier 			= $supplier_details->company;
                                        ?>
                                        <div class="btn-group">
                                            <a href="<?= site_url('sales/montarHotel/' . $product->id.'/'.$hotel->id.'/'.$contador) ?>"
                                               class="tip btn btn-warning" title="<?= $supplier ?>">
                                                <i class="fa fa-bed"> <?= 'Configurar Rooming List Hotel '.$supplier  ?></i>
                                                <span class="hidden-sm hidden-xs"> </span>
                                            </a>
                                        </div>
                                        <?php $contador = $contador + 1;} ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="btn-group btn-group-justified">
                                <?php if ($product->viagem == 1) { ?>
                                    <?php foreach ($hoteis as $hotel) {

                                        $fornecedor         = $hotel->fornecedor;
                                        $supplier_details 	= $this->site->getCompanyByID($fornecedor);
                                        $supplier 			= $supplier_details->company;
                                        ?>
                                        <div class="btn-group">
                                            <a href="<?= site_url('sales/pdf_hotel/' . $product->id.'/'.$hotel->id) ?>"
                                               class="tip btn btn-primary" title="<?= $supplier ?>">
                                                <i class="fa fa-bed"> <?= 'Ver Room Lista do Hotel '.$supplier  ?></i>
                                                <span class="hidden-sm hidden-xs"></span>
                                            </a>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.tip').tooltip();
            });
        </script>
        <?php } ?>


    </div>
    <div id="chart" class="tab-pane fade">
        <script src="<?= $assets; ?>js/hc/highcharts.js"></script>
        <script type="text/javascript">
            $(function () {
                Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
                    return {
                        radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                        stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
                    };
                });
                <?php if($sold) { ?>
                var sold_chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'soldchart',
                        type: 'line',
                        width: <?= $purchased ? "($('#details').width()-160)/2" : "$('#details').width()-100"; ?>
                    },
                    credits: {enabled: false},
                    title: {text: ''},
                    xAxis: {
                        categories: [<?php
                            foreach ($sold as $r) {
                                $month = explode('-', $r->month);
                                echo "'" . lang('cal_' . strtolower($month[1])) . " " . $month[0] . "', ";
                            }
                            ?>]
                    },
                    yAxis: {min: 0, title: ""},
                    legend: {enabled: false},
                    tooltip: {
                        shared: true,
                        followPointer: true,
                        formatter: function () {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;min-width:150px;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                if (this.series.name == '<?= lang("amount"); ?>') {
                                    s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                        currencyFormat(this.y) + '</b></td></tr>';
                                } else {
                                    s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                        formatQuantity(this.y) + '</b></td></tr>';
                                }
                            });
                            s += '</table></div>';
                            return s;
                        },
                        useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    series: [{
                        type: 'spline',
                        name: '<?= lang("sold"); ?>',
                        data: [<?php
                            foreach ($sold as $r) {
                                echo "['" . lang('cal_' . strtolower($r->month)) . "', " . $r->sold . "],";
                            }
                            ?>]
                    }, {
                        type: 'spline',
                        name: '<?= lang("amount"); ?>',
                        data: [<?php
                            foreach ($sold as $r) {
                                echo "['" . lang('cal_' . strtolower($r->month)) . "', " . $r->amount . "],";
                            }
                            ?>]
                    }]
                });
                $(window).resize(function () {
                    sold_chart.setSize($('#soldchart').width(), 450);
                });
                <?php } if($purchased) { ?>
                var purchased_chart = new Highcharts.Chart({
                    chart: {renderTo: 'purchasedchart', type: 'line', width: ($('#details').width() - 160) / 2},
                    credits: {enabled: false},
                    title: {text: ''},
                    xAxis: {
                        categories: [<?php
                            foreach ($purchased as $r) {
                                $month = explode('-', $r->month);
                                echo "'" . lang('cal_' . strtolower($month[1])) . " " . $month[0] . "', ";
                            }
                            ?>]
                    },
                    yAxis: {min: 0, title: ""},
                    legend: {enabled: false},
                    tooltip: {
                        shared: true,
                        followPointer: true,
                        formatter: function () {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;min-width:150px;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                if (this.series.name == '<?= lang("amount"); ?>') {
                                    s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                        currencyFormat(this.y) + '</b></td></tr>';
                                } else {
                                    s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                        formatQuantity(this.y) + '</b></td></tr>';
                                }
                            });
                            s += '</table></div>';
                            return s;
                        },
                        useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                        style: {fontSize: '14px', padding: '0', color: '#000000'}
                    },
                    series: [{
                        type: 'spline',
                        name: '<?= lang("purchased"); ?>',
                        data: [<?php
                            foreach ($purchased as $r) {
                                echo "['" . lang('cal_' . strtolower($r->month)) . "', " . $r->purchased . "],";
                            }
                            ?>]
                    }, {
                        type: 'spline',
                        name: '<?= lang("amount"); ?>',
                        data: [<?php
                            foreach ($purchased as $r) {
                                echo "['" . lang('cal_' . strtolower($r->month)) . "', " . $r->amount . "],";
                            }
                            ?>]
                    }]
                });
                $(window).resize(function () {
                    purchased_chart.setSize($('#purchasedchart').width(), 450);
                });
                <?php } ?>

            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('chart'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-sm-12">
                                <div class="box" style="border-top: 1px solid #dbdee0;">
                                    <div class="box-header">
                                        <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('sold'); ?>
                                        </h2>
                                    </div>
                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="soldchart" style="width:100%; height:450px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($purchased) { ?>
                                <div class="col-sm-6" style="display: none;">
                                    <div class="box" style="border-top: 1px solid #dbdee0;">
                                        <div class="box-header">
                                            <h2 class="blue"><i
                                                        class="fa-fw fa fa-bar-chart-o"></i><?= lang('purchased'); ?>
                                            </h2>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="purchasedchart" style="width:100%; height:450px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="fluxoCaixa" class="tab-pane fade">

        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('fluxo_de_caixa'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">


                            <div class="box-header">
                                <h2 class="blue"><i
                                            class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas_adicionais_sinal'); ?>
                                </h2>
                            </div>

                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr class="active">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference"); ?></th>
                                    <th class="col-xs-2"><?= lang("category"); ?></th>
                                    <th class="col-xs-1"><?= lang("amount"); ?></th>
                                    <th class="col-xs-3"><?= lang("note"); ?></th>
                                    <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($outrasDespesasSinal)) {
                                    foreach ($outrasDespesasSinal as $order) {
                                        echo '
                                            <tr>
                                                <td>' . $order->date . '</td>
                                                <td>' . $order->reference . '</td>
                                                <td>' . $order->category . '</td>
                                                <td class="text-right">' . $this->sma->formatMoney($order->amount) . '</td>
                                                <td>' . $order->note . '</td>
                                                <td>' . $order->user . '</td>
                                            </tr>';
                                    }
                                } ?>
                                </tbody>

                            </table>
                            <?= lang("despesas_com_sinal_alert"); ?>
                            <br/><br/>
                            <div class="box-header">
                                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas'); ?></h2>
                            </div>
                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr>
                                    <th><?= lang("id"); ?></th>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("fornecedor_cliente"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("saldo"); ?></th>
                                    <th><?= lang("payment_status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_purchases = 0;
                                $total_pago_purchases = 0;
                                $total_saldo_purchases = 0;

                                if (!empty($purchases)) {
                                    $r = 1;
                                    foreach ($purchases as $purchase) {
                                        $payment_status = '';
                                        $total_purchases = $total_purchases + $purchase->grand_total;
                                        $total_pago_purchases = $total_pago_purchases + $purchase->paid;
                                        $total_saldo_purchases = $total_saldo_purchases + $purchase->balance;

                                        if ($purchase->paid >= $purchase->grand_total) {
                                            $payment_status = 'paid';
                                        } else {
                                            if ($purchase->paid > 0) {
                                                $payment_status = 'partial';
                                            } else {
                                                $payment_status = 'due';
                                            }
                                        }

                                        echo '
															<tr id="' . $purchase->id . '" class="purchase_link"><td>' . $r . '</td>
			                                                    <td>' . $this->sma->hrld($purchase->date) . '</td>
			                                                    <td>' . $purchase->reference_no . '</td>
			                                                    <td>' . $purchase->supplier . '</td>
			                                                    <td class="text-right">' . $this->sma->formatMoney($purchase->grand_total) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($purchase->paid) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($purchase->balance) . '</td>
																<td>' . row_status($payment_status) . '</td>
			                                                </tr>';
                                        $r++;
                                    }
                                } else { ?>
                                    <tr>
                                        <td colspan="6"
                                            class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_purchases) ?></th>
                                    <th><?= $this->sma->formatMoney($total_pago_purchases) ?></th>
                                    <th><?= $this->sma->formatMoney($total_saldo_purchases) ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>

                            <br/>

                            <div class="box-header">
                                <h2 class="blue"><i
                                            class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas_adicionais'); ?>
                                </h2>
                            </div>

                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr class="active">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference"); ?></th>
                                    <th class="col-xs-2"><?= lang("category"); ?></th>
                                    <th class="col-xs-1"><?= lang("amount"); ?></th>
                                    <th class="col-xs-3"><?= lang("note"); ?></th>
                                    <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_outras_despesas = 0;
                                if (!empty($outrasDespesas)) {
                                    foreach ($outrasDespesas as $order) {
                                        $total_outras_despesas = $total_outras_despesas + $order->amount;
                                        echo '
																<tr>
																	<td>' . $order->date . '</td>
		                                                            <td>' . $order->reference . '</td>
		                                                            <td>' . $order->category . '</td>
																	<td class="text-right">' . $this->sma->formatMoney($order->amount) . '</td>
		                                                            <td>' . $order->note . '</td>
  		                                                            <td>' . $order->user . '</td>
		                                                        </tr>';
                                    }
                                } ?>
                                </tbody>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_outras_despesas) ?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th colspan="6" class="text-right">Total das Depesas + Outras
                                        Despesas <?= $this->sma->formatMoney($total_outras_despesas + $total_purchases) ?></th>
                                </tr>
                                </thead>
                            </table>

                            <br/>
                            <div class="box-header">
                                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('receitas'); ?></h2>
                            </div>
                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr>
                                    <th><?= lang("id"); ?></th>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("fornecedor_cliente"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("saldo"); ?></th>
                                    <th><?= lang("payment_status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_sales = 0;
                                $total_pago_sales = 0;
                                $total_saldo_sales = 0;
                                if (!empty($sales)) {
                                    $r = 1;
                                    foreach ($sales as $order) {
                                        $total_sales = $total_sales + $order->grand_total;
                                        $total_pago_sales = $total_pago_sales + $order->paid;
                                        $total_saldo_sales = $total_saldo_sales + $order->balance;
                                        echo '
															<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
	                                                            <td>' . $this->sma->hrld($order->date) . '</td>
	                                                            <td>' . $order->reference_no . '</td>
	                                                            <td>' . $order->customer . '</td>
	                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($order->paid) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($order->balance) . '</td>
	                                                            <td>' . row_status($order->payment_status) . '</td>
	                                                             
	                                                        </tr>';
                                        $r++;
                                    }
                                } ?>
                                </tbody>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_sales) ?></th>
                                    <th><?= $this->sma->formatMoney($total_pago_sales) ?></th>
                                    <th><?= $this->sma->formatMoney($total_saldo_sales) ?></th>
                                    <th></th>
                                </tr>
                                </thead>

                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <?php if (($total_purchases + $total_outras_despesas) - $total_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-success">
					                                   					<?= $this->sma->formatMoney((($total_purchases + $total_outras_despesas) - $total_sales) * -1) ?>
					                                   				</span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-danger">
					                                   					<?= $this->sma->formatMoney(($total_purchases + $total_outras_despesas) - $total_sales) ?>
					                                   				</span>
                                            </div>
                                        </th>
                                    <?php } ?>

                                    <?php if (($total_pago_purchases + $total_outras_despesas) - $total_pago_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-success">
					                                    				<?= $this->sma->formatMoney((($total_pago_purchases + $total_outras_despesas) - $total_pago_sales) * -1) ?>
					                                    				</span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-danger">
					                                    				<?= $this->sma->formatMoney(($total_pago_purchases + $total_outras_despesas) - $total_pago_sales) ?>
					                                    				</span>
                                            </div>
                                        </th>
                                    <?php } ?>

                                    <?php if (($total_saldo_purchases + $total_outras_despesas) - $total_saldo_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-success">
					                                   					<?= $this->sma->formatMoney((($total_saldo_purchases + $total_outras_despesas) - $total_saldo_sales) * -1) ?>
					                                   				</span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
					                                   				<span class="label label-danger">
					                                   					<?= $this->sma->formatMoney(($total_saldo_purchases) - $total_saldo_sales) ?>
					                                   				</span>
                                            </div>
                                        </th>
                                    <?php } ?>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                            <br/>
                            <center>
                                <div class="btn-group">
                                    <a href="<?= site_url('reports/profit_loss') ?>" class="tip btn btn-primary"
                                       title="<?= lang('profit_loss') ?>">
                                        <i class="fa fa-money"></i> <span
                                                class="hidden-sm hidden-xs"><?= lang('profit_loss') ?></span>
                                    </a>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="sales" class="tab-pane fade">
        <?php $warehouse_id = NULL; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                var oTable = $('#SlRData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getSalesReport/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        var oSettings = oTable.fnSettings();
                        nRow.id = aData[9];
                        nRow.className = "invoice_link";
                        return nRow;
                    },
                    "aoColumns": [{"mRender": fld}, null, null, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                    }, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": row_status}],
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var gtotal = 0, paid = 0, balance = 0;
                        for (var i = 0; i < aaData.length; i++) {
                            gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                            paid += parseFloat(aaData[aiDisplay[i]][6]);
                            balance += parseFloat(aaData[aiDisplay[i]][7]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                        nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                        nCells[7].innerHTML = currencyFormat(parseFloat(balance));
                    }
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('reference_no');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                    {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                    {
                        column_number: 8,
                        filter_default_label: "[<?=lang('payment_status');?>]",
                        filter_type: "text",
                        data: []
                    },
                ], "footer");
            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-heart nb"></i><?= $product->name . ' ' . lang('sales'); ?></h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="SlRData" class="table table-bordered table-hover table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("biller"); ?></th>
                                    <th><?= lang("customer"); ?></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("balance"); ?></th>
                                    <th><?= lang("payment_status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="9"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("balance"); ?></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="quotes" class="tab-pane fade">
        <script type="text/javascript">
            $(document).ready(function () {
                var oTable = $('#QuRData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getQuotesReport/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    "aoColumns": [{"mRender": fld}, null, null, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                    }, {"mRender": currencyFormat}, {"mRender": row_status}],
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('reference_no');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                    {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                    {
                        column_number: 5,
                        filter_default_label: "[<?=lang('grand_total');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                ], "footer");
            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-heart-o nb"></i><?= $product->name . ' ' . lang('quotes'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf1" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image1" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="QuRData" class="table table-bordered table-hover table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("biller"); ?></th>
                                    <th><?= lang("customer"); ?></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="7"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="purchases" class="tab-pane fade">
        <script type="text/javascript">
            $(document).ready(function () {
                var oTable = $('#PoRData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getPurchasesReport/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        var oSettings = oTable.fnSettings();
                        nRow.id = aData[9];
                        nRow.className = "purchase_link";
                        return nRow;
                    },
                    "aoColumns": [{"mRender": fld}, null, null, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                    }, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": row_status}],
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var gtotal = 0, paid = 0, balance = 0;
                        for (var i = 0; i < aaData.length; i++) {
                            gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                            paid += parseFloat(aaData[aiDisplay[i]][6]);
                            balance += parseFloat(aaData[aiDisplay[i]][7]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                        nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                        nCells[7].innerHTML = currencyFormat(parseFloat(balance));
                    }
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('reference_no');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 2,
                        filter_default_label: "[<?=lang('warehouse');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
                    {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                ], "footer");
            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-star nb"></i><?= $product->name . ' ' . lang('purchases'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf2" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls2" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image2" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="PoRData" class="table table-bordered table-hover table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("warehouse"); ?></th>
                                    <th><?= lang("supplier"); ?></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("balance"); ?></th>
                                    <th><?= lang("status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="9"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("balance"); ?></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="transfers" class="tab-pane fade">
        <script type="text/javascript">
            $(document).ready(function () {
                var oTable = $('#TrRData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getTransfersReport/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    "aoColumns": [{"mRender": fld}, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                    }, null, null, {"mRender": currencyFormat}, {"mRender": row_status}],
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('reference_no');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 3,
                        filter_default_label: "[<?=lang("warehouse") . ' (' . lang('from') . ')';?>]",
                        filter_type: "text", data: []
                    },
                    {
                        column_number: 4,
                        filter_default_label: "[<?=lang("warehouse") . ' (' . lang('to') . ')';?>]",
                        filter_type: "text", data: []
                    },
                    {
                        column_number: 5,
                        filter_default_label: "[<?=lang('grand_total');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                ], "footer");
            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-star-o nb"></i><?= $product->name . ' ' . lang('transfers'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown"><a href="#" id="pdf3" class="tip" title="<?= lang('download_pdf') ?>"><i
                                        class="icon fa fa-file-pdf-o"></i></a></li>
                        <li class="dropdown"><a href="#" id="xls3" class="tip" title="<?= lang('download_xls') ?>"><i
                                        class="icon fa fa-file-excel-o"></i></a></li>
                        <li class="dropdown"><a href="#" id="image3" class="tip image"
                                                title="<?= lang('save_image') ?>"><i
                                        class="icon fa fa-file-picture-o"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="TrRData" class="table table-bordered table-hover table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th><?= lang("warehouse") . ' (' . lang('from') . ')'; ?></th>
                                    <th><?= lang("warehouse") . ' (' . lang('to') . ')'; ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="7"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("product_qty"); ?></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="damages" class="tab-pane fade">
        <script>
            $(document).ready(function () {
                $('#dmpData').dataTable({
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaSorting": [[0, "desc"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'sAjaxSource': '<?= site_url('products/getadjustments/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?php echo $this->security->get_csrf_token_name(); ?>",
                            "value": "<?php echo $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    "aoColumns": [
                        {"mRender": fld}, null, null, null, {"mRender": formatQuantity}, null, {"bSortable": false}
                    ]
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('product_code');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 2,
                        filter_default_label: "[<?=lang('product_name');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 3,
                        filter_default_label: "[<?=lang('product_variant');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 4, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
                    {
                        column_number: 5,
                        filter_default_label: "[<?=lang('warehouse');?>]",
                        filter_type: "text",
                        data: []
                    },
                ], "footer");
            });
        </script>
        <style type="text/css">#dmpData th:last-childe {
                width: 100px !important;
            }</style>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i
                            class="fa-fw fa fa-filter nb"></i><?= $product->name . ' ' . lang('quantity_adjustments'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf4" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls4" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image4" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="dmpData" class="table table-bordered table-condensed table-hover table-striped">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("product_code"); ?></th>
                                    <th><?= lang("product_name"); ?></th>
                                    <th><?= lang("product_variant"); ?></th>
                                    <th><?= lang("quantity"); ?></th>
                                    <th><?= lang("warehouse"); ?></th>
                                    <th style="text-align:center;"><?= lang("actions"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="7"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align:center;"><?= lang("actions"); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="returns" class="tab-pane fade">
        <script>
            $(document).ready(function () {
                var oTable = $('#PRESLData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getReturnsReport/?v=1&product=' . $product->id) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        var oSettings = oTable.fnSettings();
                        nRow.id = aData[7];
                        nRow.className = "return_link";
                        return nRow;
                    },
                    "aoColumns": [{"mRender": fld}, null, null, null, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                    }, {"mRender": currencyFormat}, {"mRender": currencyFormat}],
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var sc = 0, gtotal = 0;
                        for (var i = 0; i < aaData.length; i++) {
                            sc += parseFloat(aaData[aiDisplay[i]][6]);
                            gtotal += parseFloat(aaData[aiDisplay[i]][7]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[6].innerHTML = currencyFormat(parseFloat(sc));
                        nCells[7].innerHTML = currencyFormat(parseFloat(gtotal));
                    }
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('reference_no');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 2,
                        filter_default_label: "[<?=lang('sale_reference');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 3, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                    {column_number: 4, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                ], "footer");
            });
        </script>
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-random nb"></i><?= $product->name . ' ' . lang('returns'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf5" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls5" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image5" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div class="table-responsive">
                            <table id="PRESLData" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("sale_reference"); ?></th>
                                    <th><?= lang("biller"); ?></th>
                                    <th><?= lang("customer"); ?></th>
                                    <th class="col-sm-2"><?= lang("product_qty"); ?></th>
                                    <th class="col-sm-1"><?= lang("surcharges"); ?></th>
                                    <th class="col-sm-1"><?= lang("grand_total"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="9" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="col-sm-2"><?= lang("product_qty"); ?></th>
                                    <th class="col-sm-1"><?= lang("surcharges"); ?></th>
                                    <th class="col-sm-1"><?= lang("grand_total"); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="PI" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-file-text-o nb"></i><?= $product->name . ' ' . lang('pi'); ?>
                </h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#pdf1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getQuotesReport/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getQuotesReport/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#pdf2').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPurchasesReport/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls2').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPurchasesReport/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#pdf3').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getTransfersReport/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls3').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getTransfersReport/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#pdf4').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('products/getadjustments/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls4').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('products/getadjustments/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#pdf5').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getReturnsReport/pdf/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('#xls5').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getReturnsReport/0/xls/?v=1&product=' . $product->id)?>";
            return false;
        });
        $('.image').click(function (event) {
            var box = $(this).closest('.box');
            event.preventDefault();
            html2canvas(box, {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
    });
</script>
 
