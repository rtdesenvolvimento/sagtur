<script>
    $(document).ready(function () {
        $('#asaas_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('valepay_settings'); ?></h2>
        <div class="box-icon">
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('role' => 'form', 'id="valepay_form"');
                echo form_open("system_settings/valepay", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('1' => lang("sim"), '0' => lang("nao"));
                            echo form_dropdown('active', $yn, $valepay->active, 'class="form-control tip" required="required" id="active"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("public_key", "public_key"); ?>
                            <?php echo form_input('public_key', $valepay->public_key, 'class="form-control tip" id="public_key"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("private_key", "private_key"); ?>
                            <?php echo form_input('private_key', $valepay->private_key, 'class="form-control tip" id="private_key"'); ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label>Como pegar os dados para integração com ValePay</label>
                        <iframe width="383" height="384" src="https://www.youtube.com/embed/zC-f8FivJl4" title="Como pegar o Token de Integração da ValePay" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>