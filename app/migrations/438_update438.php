<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update438 extends CI_Migration {

    public function up() {

        $this->alter_table_sale_items();
        $this->alter_table_local_embarque();
        $this->alter_table_itinerario_column();
        $this->alter_table_settings();
        $this->alter_table_order_ref();

        $this->alter_table_itinerario();

        $this->db->update('settings',  array('version' => '4.3.8'), array('setting_id' => 1));
    }

    public function alter_table_itinerario_column()
    {
        $this->db->query('ALTER TABLE `sma_itinerario` CHANGE `dataEmbarque` `dataEmbarque` DATE NOT NULL;');
        $this->db->query('ALTER TABLE `sma_itinerario` CHANGE `horaEmbarque` `horaEmbarque` TIME NOT NULL;');
        $this->db->query('ALTER TABLE `sma_itinerario` CHANGE `motorista` `motorista` INT(11) NULL;');
        $this->db->query('ALTER TABLE `sma_itinerario` CHANGE `guia` `guia` INT(11) NULL;');
        $this->db->query('ALTER TABLE `sma_itinerario` CHANGE `veiculo` `veiculo` INT(11) NULL;');
        $this->db->query("ALTER TABLE `sma_itinerario` DROP `dataRetorno`;");
        $this->db->query("ALTER TABLE `sma_itinerario` DROP `horaRetorno`;");

    }

    public function alter_table_sale_items()
    {
        $fields = array(
            'hora_embarque' => array('type' => 'TIME', 'null' => TRUE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'referencia' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),

            'local_desembarque' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'data_desembarque' => array('type' => 'DATE', 'null' => TRUE ),
            'hora_desembarque' => array('type' => 'TIME', 'null' => TRUE ),
        );
        $this->dbforge->add_column('sale_items', $fields);
    }

    public function alter_table_local_embarque()
    {
        $fields = array(
            'country' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'bairro' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
        );
        $this->dbforge->add_column('local_embarque', $fields);
    }

    public function alter_table_itinerario()
    {
        $fields = array(
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'tipo_transporte_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
            'tipo_trajeto_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
            'product_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
            'retornoPrevisto' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('itinerario', $fields);
    }

    public function alter_table_settings() {
        $fields = array(
            'itinerary_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'OS'),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_order_ref() {
        $fields = array(
            'os' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('order_ref', $fields);
    }

    public function down() {}
}
