<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update460 extends CI_Migration {

    public function up() {

        $this->alter_table_update_categories();

        $this->update_sma_images_icon();

        $this->db->update('settings',  array('version' => '4.6.0'), array('setting_id' => 1));
    }

    public function update_sma_images_icon()
    {
        $this->db->query("UPDATE sma_images_icon
                            SET icon = CONCAT(
                                SUBSTRING_INDEX(icon, 'height=\"', 1), -- Parte antes do 'height=\"'
                                'height=\"18\"',                           -- O novo valor de height
                                SUBSTRING(
                                    icon, 
                                    LOCATE('\"', icon, LOCATE('height=\"', icon) + LENGTH('height=\"')) + 1
                                ) -- Parte depois do valor antigo de height
                            )
                            WHERE icon LIKE '%height=\"%';");

        $this->db->query("UPDATE sma_images_icon
                            SET icon = CONCAT(
                                SUBSTRING_INDEX(icon, 'width=\"', 1), -- Parte antes do 'width=\"'
                                'width=\"18\"',                       -- O novo valor de width
                                SUBSTRING(
                                    icon, 
                                    LOCATE('\"', icon, LOCATE('width=\"', icon) + LENGTH('width=\"')) + 1
                                ) -- Parte depois do valor antigo de width
                            )
                            WHERE icon LIKE '%width=\"%';");
    }

    public function alter_table_update_categories()
    {
        $this->db->update('categories', array('image_icon_id' => $this->insert_icon('<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="64" height="64" viewBox="0 0 64 64">
<path d="M 32 6 C 24.705 6 18.271125 6.7695937 14.078125 7.4335938 C 11.155125 7.8965938 9 10.404281 9 13.363281 L 9 47 L 9 52 L 9 54 C 9 55.105 9.895 56 11 56 L 15.306641 56 C 16.283641 56 17.118297 55.292125 17.279297 54.328125 L 17.667969 52 L 32 52 L 46.332031 52 L 46.720703 54.328125 C 46.881703 55.292125 47.716359 56 48.693359 56 L 53 56 C 54.105 56 55 55.105 55 54 L 55 47 L 55 13.363281 C 55 10.404281 52.844875 7.8965938 49.921875 7.4335938 C 45.728875 6.7695937 39.295 6 32 6 z M 18 10 L 46 10 L 46 11 C 46 12.105 45.105 13 44 13 L 20 13 C 18.895 13 18 12.105 18 11 L 18 10 z M 3 17 L 3 23 C 3 25.209 4.791 27 7 27 L 6 17 L 3 17 z M 15 17 L 49 17 C 50.105 17 51 17.895 51 19 L 51 34.333984 C 51 35.304984 50.305609 36.136734 49.349609 36.302734 C 46.141609 36.862734 38.787 38 32 38 C 25.213 38 17.858391 36.862734 14.650391 36.302734 C 13.694391 36.136734 13 35.304984 13 34.333984 L 13 19 C 13 17.895 13.895 17 15 17 z M 58 17 L 57 27 C 59.209 27 61 25.209 61 23 L 61 17 L 58 17 z M 13 42 L 22 43 L 21 47 L 18.5 47 L 14.5 47 C 13.672 47 13 46.328 13 45.5 L 13 42 z M 51 42 L 51 45.5 C 51 46.328 50.328 47 49.5 47 L 43 47 L 42 43 L 51 42 z"></path>
</svg>')), array('name' => 'Pacote Turísticos'));

        $this->db->update('categories', array('image_icon_id' => $this->insert_icon('<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="64" height="64" viewBox="0 0 64 64">
<path d="M 32 6 C 24.705 6 18.271125 6.7695937 14.078125 7.4335938 C 11.155125 7.8965938 9 10.404281 9 13.363281 L 9 47 L 9 52 L 9 54 C 9 55.105 9.895 56 11 56 L 15.306641 56 C 16.283641 56 17.118297 55.292125 17.279297 54.328125 L 17.667969 52 L 32 52 L 46.332031 52 L 46.720703 54.328125 C 46.881703 55.292125 47.716359 56 48.693359 56 L 53 56 C 54.105 56 55 55.105 55 54 L 55 47 L 55 13.363281 C 55 10.404281 52.844875 7.8965938 49.921875 7.4335938 C 45.728875 6.7695937 39.295 6 32 6 z M 18 10 L 46 10 L 46 11 C 46 12.105 45.105 13 44 13 L 20 13 C 18.895 13 18 12.105 18 11 L 18 10 z M 3 17 L 3 23 C 3 25.209 4.791 27 7 27 L 6 17 L 3 17 z M 15 17 L 49 17 C 50.105 17 51 17.895 51 19 L 51 34.333984 C 51 35.304984 50.305609 36.136734 49.349609 36.302734 C 46.141609 36.862734 38.787 38 32 38 C 25.213 38 17.858391 36.862734 14.650391 36.302734 C 13.694391 36.136734 13 35.304984 13 34.333984 L 13 19 C 13 17.895 13.895 17 15 17 z M 58 17 L 57 27 C 59.209 27 61 25.209 61 23 L 61 17 L 58 17 z M 13 42 L 22 43 L 21 47 L 18.5 47 L 14.5 47 C 13.672 47 13 46.328 13 45.5 L 13 42 z M 51 42 L 51 45.5 C 51 46.328 50.328 47 49.5 47 L 43 47 L 42 43 L 51 42 z"></path>
</svg>')), array('name' => 'Pacote turístico'));

    }
    public function insert_icon($icon) {
        $this->db->insert('images_icon', array('name' => '', 'type' => 'svg_embedded' , 'icon' => $icon));

        return $this->db->insert_id();
    }

    public function down() {}
}
