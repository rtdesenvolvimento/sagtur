<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update342 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.22.08.30', 'default_email' => 'voucher_no-replay@sagtur.com.br'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'habilitar_cupom_desconto' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'envio_email_venda_manual' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'mostrar_detalhes_link_pacote' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
