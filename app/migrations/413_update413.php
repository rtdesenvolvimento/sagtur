<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update413 extends CI_Migration {

    public function up() {

        $this->update_tipo_trajeto_veiculo();
        $this->insert_cor_agenda();
        $this->alter_agenda_viagem();
        $this->create_table_tipo_parentesco();
        $this->create_table_client_relationships();
        $this->create_table_sale_item_dependents();

        $this->db->update('settings',  array('version' => '4.1.3'), array('setting_id' => 1));
    }

    public function alter_agenda_viagem() {

        $this->db->query('ALTER TABLE sma_agenda_programacao DROP agenda_viagem_id;');

        $fields = array(
            'agenda_programacao_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('agenda_viagem', $fields);
    }

    public function update_tipo_trajeto_veiculo() {
        $this->db->update('tipo_trajeto_veiculo',  array('name' => 'Regular'), array('id' => 3));
    }

    public function insert_cor_agenda() {
        $this->db->insert('cor_agenda', array('name' => 'Roxo', 'cor' => '#A020F0'));
        $this->db->insert('cor_agenda', array('name' => 'Vermelho', 'cor' => '#FF0000	'));
        $this->db->insert('cor_agenda', array('name' => 'Lanranja	', 'cor' => '#FFA500'));
        $this->db->insert('cor_agenda', array('name' => 'Azul Forte', 'cor' => '#B0E0E6'));
        $this->db->insert('cor_agenda', array('name' => 'Azul', 'cor' => '#0000FF'));
        $this->db->insert('cor_agenda', array('name' => 'Verde', 'cor' => '#2E8B57'));
        $this->db->insert('cor_agenda', array('name' => 'Preto', 'cor' => '#000000'));
    }

    public function create_table_tipo_parentesco() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_parentesco', TRUE, $attributes);

        $this->db->insert('tipo_parentesco', array('name' => 'Esposo(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Mãe'));
        $this->db->insert('tipo_parentesco', array('name' => 'Pai'));
        $this->db->insert('tipo_parentesco', array('name' => 'Irmão(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Filho(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Amigo(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Tio(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Neto(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Bisneto(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Sobrinho(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Avô(ó)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Primo(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Afilhado(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Sogro(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Cunhado(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Enteado(a)'));
        $this->db->insert('tipo_parentesco', array('name' => 'Outros'));
    }

    public function create_table_client_relationships() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'customer_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),//eu
            'relative_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),//meu parente
            'tipo_parentesco_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('client_relationships', TRUE, $attributes);
    }


    public function create_table_sale_item_dependents() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'item_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'customer_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'faixa_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'faixa_name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'tipo_parentesco_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_item_dependents', TRUE, $attributes);
    }

    public function down() {}
}
