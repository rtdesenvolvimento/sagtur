<?php defined('BASEPATH') or exit('No direct script access allowed');


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use League\OAuth2\Client\Provider\Google;
use Mpdf\Mpdf;
use Google\Client;
use Google\Service\PeopleService;

//Load Composer's autoloader
require 'vendor/autoload.php';


class Teste extends MY_Controller
{

    const INFINITEPAY_TAX = [
        1,
        3.82,
        1.5041,
        1.5992,
        1.6630,
        1.7057,
        2.3454,
        2.3053,
        2.2755,
        2.2490,
        2.2306,
        2.2111,
        2.3333,
    ];


    public function __construct()
    {
        parent::__construct();
    }

    public function cache1() {
        $this->load->driver('cache');
        $produtos = $this->cache->file->get('produtos');

        // Se os produtos não estiverem no cache, obtê-los da fonte
        if (!$produtos) {
            $produtos = [
                [
                    'id' => 1,
                    'nome' => 'Caneta',
                    'preco' => 10,
                ],
                [
                    'id' => 2,
                    'nome' => 'Lápis',
                    'preco' => 5,
                ],
                [
                    'id' => 3,
                    'nome' => 'Borracha',
                    'preco' => 2,
                ],
            ];

            foreach ($produtos as $produto) {
                echo $produto['nome'] . ': R$' . $produto['preco'] . '<br>';
            }

            // Salvar a lista de produtos no cache
            $this->cache->file->save('produtos', $produtos, 30);
        }
    }

    public function cache_list() {
        $this->load->driver('cache');


        $produtos = $this->cache->file->get('produtos');

        // Exibir a lista de produtos
        foreach ($produtos as $produto) {
            echo $produto['nome'] . ': R$' . $produto['preco'] . '<br>';
        }
    }

    public function consulta_email() {

        $x_auth_token = '4172169648847151e6793fe79398f0f7';
       // $location = 'https://api.smtplw.com.br/v1/messages?status=all&start_date=2023-07-21&end_date=2023-07-21&page=39'; # get this from response header Location
        $location = 'https://api.smtplw.com.br/v1/messages/3'; # get this from response header Location

        $headers = array(
            "x-auth-token: $x_auth_token"
        );

        $ch = curl_init($location);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $curlInfo = curl_getinfo($ch);
        curl_close($ch);


        switch($curlInfo['http_code']) {
            case '200':
                $message = json_encode($response, true);
                echo "OK\n";
                break;
            default:
                echo "Error: $curlInfo[http_code]\n";
                break;
        }

        echo $message;
    }

    public function send_mail_api(){

        // requires module php5-curl
        $x_auth_token = '4172169648847151e6793fe79398f0f7';
        $api_url = 'https://api.smtplw.com.br/v1';

        $from = "voucher@envios.suareservaonline.com.br";
        $to = array("resultatec@gmail.com");
        $subject = "teste api html";
        $body = <<<EOT
                    <html>
                        <head>
                            <title>Teste</title>
                        </head>
                        <body>
                        <p>Teste</p>
                        </body>
                    </html>
                EOT;

        $headers = array(
            "x-auth-token: $x_auth_token",
            "Content-type: application/json"
        );

        $data_string = array(
            'from'    => $from,
            'to'      => $to,
            'subject' => $subject,
            'body'    => $body,
            'headers' => array('Content-type' => 'text/html')
        );

        $ch = curl_init("$api_url/messages");

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_string));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $curlInfo = curl_getinfo($ch);
        curl_close($ch);

        switch($curlInfo['http_code']) {
            case '201':
                $status = 'OK';
                if (preg_match('@^Location: (.*)$@m', $response, $matches)) {
                    $location = trim($matches[1]);
                }
                // Add other actions here, if necessary.
                break;
            default:
                $status = "Error: $curlInfo[http_code]";
                break;
        }

        echo "\nStatus: {$status}\n";

        if ($location) {
            echo "\nLocation: {$location}\n\n";
        }

    }
    public function email() {
        $this->send_mail();
        $this->send_mail();
        $this->send_mail();
    }

    public function send_mail() {
        $mail = new PHPMailer;
        $mail->setLanguage('br');                             // Habilita as saídas de erro em Português
        $mail->CharSet='UTF-8';                               // Habilita o envio do email como 'UTF-8'

        //$mail->SMTPDebug = 3;                               // Habilita a saída do tipo "verbose"

        $mail->isSMTP();                                      // Configura o disparo como SMTP
        $mail->Host = 'smtplw.com.br';                        // Especifica o enderço do servidor SMTP da Locaweb
        $mail->SMTPAuth = true;                               // Habilita a autenticação SMTP
        $mail->Username = 'sagtur';                        // Usuário do SMTP
        $mail->Password = 'QfepnoVG8761';                          // Senha do SMTP
        $mail->SMTPSecure = 'tls';                            // Habilita criptografia TLS | 'ssl' também é possível
        $mail->Port = 587;                                    // Porta TCP para a conexão

        $mail->From = 'voucher_no-replay@sagtur.com.br';                          // Endereço previamente verificado no painel do SMTP
        $mail->FromName = 'SMTP Locaweb SAGTur';                     // Nome no remetente
        $mail->addAddress('resultatec@gmail.com', 'Teste Api SMTP');// Acrescente um destinatário
        $mail->addReplyTo('resultatec@gmail.com', 'Informação');

        $mail->isHTML(true);                                  // Configura o formato do email como HTML

        $mail->Subject = 'Aqui o assunto da mensagem';
        $mail->Body    = 'Esse é o body de uma mensagem HTML <strong>em negrito!</strong>';
        $mail->AltBody = 'Esse é o corpo da mensagem em formato "plain text" para clientes de email não-HTML';

        if(!$mail->send()) {
            echo 'A mensagem não pode ser enviada';
            echo 'Mensagem de erro: ' . $mail->ErrorInfo;
        } else {
            echo 'Mensagem enviada com sucesso';
        }
    }

    public function authenticate() {
        $infinitePay = $this->getInfinitePay();

        $infinitePay->authenticate();
    }

    public function getInfinitePay() {
        $infinitePay = new Infinitepay();
        $infinitePay->sandbox = true;
        $infinitePay->client_id  = '031b9702a6ab780095ccfd08a6e505cc23f6792f7f1b37d56c770b56eba4e159';
        $infinitePay->client_secret = '54f33e0fed0c9f8a4e1ae7262517e9bc99bcf925f34e3efaf7fcce16d0692f1f';

        return $infinitePay;
    }


    public function calculate_installments()
    {
        $amount = 892.5;

        echo '<pre>original '.$amount.'</pre>';

        $installments_value = [];

        for (
            $i = 1;
            $i <= 12;
            $i++
        ) {
            $tax      = !((int) 1 >= $i) && $i > 1;
            $interest = 1;
            if ($tax) {
                $interest = Teste::INFINITEPAY_TAX[$i - 1] / 100;
            }
            $value = !$tax ? $amount / $i : $amount * ($interest / (1 - pow(1 + $interest, -$i)));

            $installments_value[] = array(
                'value'    => $value,
                'interest' => $tax,
            );
            echo '<pre>' .$i. 'x ' . $value * $i . '</pre>';
        }
    }

    public function whasapp()
    {

        $client = new Client();
        $client->setApplicationName('ERP to Google Contacts');
        $client->setScopes([Google_Service_PeopleService::CONTACTS]);
        $client->setAuthConfig('C:\\Users\\andre\\Downloads\\client_secret_1014563025282-bma5d3b3dlupv1lf58sr53qvo2a8irqh.apps.googleusercontent.com.json');
        $client->setAccessType('offline');

        // Configure o mesmo Redirect URI que você adicionou no console
        $client->setRedirectUri('http://localhost/callback');

        if (isset($_GET['code'])) {
            $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
            $client->setAccessToken($token);
        } elseif ($client->isAccessTokenExpired()) {
            // Gere um link para o usuário autenticar
            $authUrl = $client->createAuthUrl();
            echo "Autentique-se: <a href='$authUrl'>Clique aqui</a>";
            exit;
        }

        $peopleService = new PeopleService($client);
        $contact = new PeopleService\Person([
            'names' => [['givenName' => 'Nome', 'familyName' => 'Sobrenome']],
            'emailAddresses' => [['value' => 'email@example.com']],
            'phoneNumbers' => [['value' => '+5511999999999']],
        ]);

        $peopleService->people->createContact($contact);
        echo "Contato criado com sucesso!";

    }

}