<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update436 extends CI_Migration {

    public function up() {

        $this->add_table_checkin_location();
        $this->add_table_checkin();

        $this->db->update('settings',  array('version' => '4.3.6'), array('setting_id' => 1));
    }

    public function add_table_checkin_location()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'dtprevista' => array('type' => 'DATE', 'null' => TRUE ),
            'hrprevista' => array('type' => 'TIME'),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'type' => array('type' => 'VARCHAR', 'constraint' => '50' , 'default' => 'in'),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('checkin_location', TRUE, $attributes);
    }

    public function add_table_checkin()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'checkin_location_id' => array('type' => 'INT', 'constraint' => 1, 'null' => TRUE ),
            'customer_id' => array('type' => 'INT', 'constraint' => 1 ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 1 ),
            'sale_id' => array('type' => 'INT', 'constraint' => 1),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'type' => array('type' => 'VARCHAR', 'constraint' => '50' , 'default' => 'in'),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('checkin', TRUE, $attributes);
    }
    public function down() {}
}
