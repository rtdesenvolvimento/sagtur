<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MercadoPagoController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
    }

    function index()
    {
        show_404();
    }

    public function notification()
    {
        $paymentToken = (isset ($_GET['data_id']) && trim($_GET['data_id']) !== "" ? trim($_GET['data_id']) : null);

        if ($paymentToken != null) {
            $this->FinanceiroService_model->baixarPagamentoIntegracao('mercadopago', $paymentToken);
        }

        $this->LogMercadoPago_model->info(print_r($_GET, true));
        $this->LogMercadoPago_model->info(print_r($_POST, true));
        $this->LogMercadoPago_model->info('mercado pago realizaou uma consulta para o id= '.$paymentToken.' na empressa= '.  $_GET['empresa'].' type'.$_GET['type']);

        return TRUE;
    }

}