<script>
    $(document).ready(function () {
        $('#juno_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
        $('#active').change(function () {
            var v = $(this).val();
            if (v == 1) {
                $('#account_email').attr('required', 'required');
                $('#juno_form').bootstrapValidator('addField', 'account_email');
            } else {
                $('#account_email').removeAttr('required');
                $('#juno_form').bootstrapValidator('removeField', 'account_email');
            }
        });
        var v = <?=$juno->active;?>;
        if (v == 1) {
            $('#account_email').attr('required', 'required');
            $('#juno_form').bootstrapValidator('addField', 'account_email');
        } else {
            $('#account_email').removeAttr('required');
            $('#juno_form').bootstrapValidator('removeField', 'account_email');
        }
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('juno_settings'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="<?= site_url('system_settings/pagseguro') ?>" class="toggle_down"><i
                                class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_pagseguro'); ?></span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('role' => 'form', 'id="juno_form"');
                echo form_open("system_settings/juno", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12 alert alert-info">
                        <i class="fa fa-info-circle"></i>
                        <?= lang('info_cadastre_se_com_nossa_conta_juno'); ?><br/>
                        <a href="https://app.juno.com.br/#/onboarding/717350:031c08">Link de Indicação SAGTur Sistema</a>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('1' => lang("sim"), '0' => lang("nao"));
                            echo form_dropdown('active', $yn, $juno->active, 'class="form-control tip" required="required" id="active"');
                            ?>
                        </div>
                        <div class="form-group">
                            <?= lang("juno_account_email", "account_email"); ?>
                            <?php echo form_input('account_email', $juno->account_email, 'class="form-control tip" id="account_email"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("juno_account_token", "account_token"); ?>
                            <?php echo form_input('account_token', $juno->account_token, 'class="form-control tip" id="account_token"'); ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("sandbox", "sandbox"); ?>
                            <?php
                            $yn = array('0' => lang("nao"), '1' => lang("sim"));
                            echo form_dropdown('sandbox', $yn, $juno->sandbox, 'class="form-control tip" required="required" id="sandbox"');
                            ?>
                        </div>
                        <div class="form-group" style="display: none;">
                            <?= lang("juno_account_token_sandbox", "account_token_sandbox"); ?>
                            <?php echo form_input('account_token_sandbox', $juno->account_token_sandbox, 'class="form-control tip" id="account_token_sandbox"'); ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label>Como pegar o TOKEN PRIVADO da JUNO para integração</label>
                        <iframe width="383" height="384" src="https://www.youtube.com/embed/NY0ykQFgEPU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>