<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CupmDescontoViewDTO_model extends CI_Model
{

    public $status;
    public $valor;
    public $cupom;
    public $tipo_desconto;

    public function __construct()
    {
        parent::__construct();
    }

}