<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update373 extends CI_Migration {

    public function up() {

        $this->alter_table_tipo_hospedagem();
        $this->alter_table_tipo_hospedagem_viagem();
        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '3.7.3'), array('setting_id' => 1));
    }

    public function alter_table_tipo_hospedagem() {
        $fields = array(
            'ocupacao'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 1),
        );
        $this->dbforge->add_column('tipo_hospedagem', $fields);
    }

    public function alter_table_tipo_hospedagem_viagem() {
        $fields = array(
            'estoque'       => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
        );
        $this->dbforge->add_column('tipo_hospedagem_viagem', $fields);
    }

    public function alter_table_products() {
        $fields = array(
            'controle_estoque_hospedagem' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
