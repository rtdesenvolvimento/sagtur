<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update390 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.9.0'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'show_payment_report_shipment' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
            'dependent_shipping_report' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
