<?php defined('BASEPATH') or exit('No direct script access allowed');

class Analytical extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('service/AnalyticalService_model', 'AnalyticalService_model');
        $this->load->model('dto/AnalyticalDTO_model', 'AnalyticalDTO_model');

    }

}