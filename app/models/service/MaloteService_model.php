<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MaloteService_model extends CI_Model
{
    const STATUS_CANCELADA = 'cancel';

    public function __construct()
    {
        parent::__construct();
    }

    public function save($malote){
        try {
            $this->__iniciaTransacao();

            $malote_id = $this->__save('malotes', $malote->__toArray());

            foreach ($malote->getFaturas() as $fatura) {
                $fatura->malote_id = $malote_id;
                $this->__save('malote_faturas', $fatura->__toArray());
            }

            $this->__confirmaTransacao();

            return $malote_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function edit($malote, $malote_id){
        try {
            $this->__iniciaTransacao();

            $this->__edit('malotes',$malote->__toArray() , 'id', $malote_id);

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

}