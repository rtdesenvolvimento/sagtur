<!DOCTYPE html>
<!--
    Copyright (c) 2012-2016 Adobe Systems Incorporated. All rights reserved.

    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
-->
<html>

<head>
    <meta charset="utf-8"/>
    <title>Área do Cliente | <?php echo $this->Settings->site_name;?></title>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width"/>
    <!-- This is a wide open CSP declaration. To lock this down for production, see below. -->

    <!--
    <meta http-equiv="Content-Security-Policy" content="default-src * 'unsafe-inline' gap:; style-src 'self' 'unsafe-inline'; media-src *" />
    !-->

    <!-- Good default declaration:
    * gap: is required only on iOS (when using UIWebView) and is needed for JS->native communication
    * https://ssl.gstatic.com is required only on Android and is needed for TalkBack to function properly
    * Disables use of eval() and inline scripts in order to mitigate risk of XSS vulnerabilities. To change this:
        * Enable inline JS: add 'unsafe-inline' to default-src
        * Enable eval(): add 'unsafe-eval' to default-src
    * Create your own at http://cspisawesome.com
    -->
    <!-- <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: 'unsafe-inline' https://ssl.gstatic.com; style-src 'self' 'unsafe-inline'; media-src *" /> -->

    <!-- css do sistema !-->
    <link rel="stylesheet" type="text/css" href="<?php echo $assets ?>myapp/css/menu.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $assets ?>myapp/css/index.css"/>

    <link rel="stylesheet" href="<?php echo $assets ?>myapp/css/themes/sagtur.min.css" />
    <link rel="stylesheet" href="<?php echo $assets ?>myapp/css/themes/jquery.mobile.icons.min.css" />

    <link rel="stylesheet" href="<?php echo $assets ?>myapp/css/jquery-confirm.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $assets ?>myapp/css/jquery.mobile.structure-1.4.5.min.css" />

    <link href="<?php echo $assets ?>myapp/img/favicon.ico" rel="shortcut icon" type="img/vnd.microsoft.icon">

    <!-- CONFIRM JQUERY -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/jquery-confirm.min.css" rel="stylesheet">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <style>

        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif !important;
            text-shadow: none; !important;
        }

        .img-fluid {
            width: 332px!important;
            height: 250px!important;
        }

        .ui-btn-icon-left:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after, .ui-btn-icon-bottom:after, .ui-btn-icon-notext:after {
            background-color: <?=$this->Settings->theme_color;?> /*{global-icon-color}*/;
            background-position: center center;
            background-repeat: no-repeat;
            -webkit-border-radius: 1em;
            border-radius: 1em;
        }

        .ui-listview .ui-li-aside {
            position: absolute;
            top: 3px;
            right: 0.9em;
            margin: 0;
            text-align: right;
            color: <?=$this->Settings->theme_color;?> ;
        }

        .ui_pagar {
            background: #d9534f !important;
            color: #fff !important;
            text-shadow: none !important;
            border-radius: 10px !important;
        }

        .ui_parcial {
            background: #5bc0de !important;
            color: #fff !important;
            text-shadow: none !important;
            border-radius: 10px !important;
        }

        .ui_pago {
            background: #5cb85c !important;
            color: #fff !important;
            text-shadow: none !important;
            border-radius: 10px !important;
        }

        .page_login {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            background-size: cover !important;
            background-position: center !important;
            background-image: url("<?= $assets ?>login/images/<?php echo $num = rand(1, 33);?>.jpg") !important;
        }

        .painel_login {
            margin: 16px;
            border-radius: 6px;
            padding: 32px 16px;
            background: rgba(0, 0, 0, 0.1);
            border: 1px solid rgba(0, 0, 0, 0.2);
            color: #ffffff;
        }

        .page_login_data {
            max-width: 360px;
            margin: 0 auto;
            background: white;
            color: #333;
        }

        .buttom_login {
            background: <?=$this->Settings->theme_color;?> !important;
            opacity: inherit !important;
            color: #ffffff !important;
            text-indent: inherit !important;
            font-size: 1.2rem !important;
        }

        .page_main {
            max-width: 750px;
            margin: 0 auto;
            background: white;
            color: #333;
        }

        .cc_fieldset {
            font-size: 1.1em;
            font-weight: 500;
            text-align: left;
            width: auto;
            color: #428BCA;
            border: 1px solid #DBDEE0;
            margin: 0px;
            cursor: pointer;
            background: #FFFFFF;
            text-transform: uppercase;
        }

        .cc_assento_selecionado {
            border-left: 4px solid rgb(27, 0, 136);
            box-shadow: <?=$this->Settings->theme_color;?> 0px -5px 20px 0px;
        }

        .assento_selecionado {
            width: 45px;
            height: 40px;
            background: rgb(27, 0, 136);
            color: white;
            display: flex;
            align-self: flex-start;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            font-weight: 600;
            white-space: nowrap;
            flex: 0 0 40px;
        }

        .assento_selecionado_name {
            display: flex;
            -webkit-box-flex: 1;
            flex-grow: 1;
            flex-flow: column wrap;
            justify-content: space-around;
        }

        .assento_selecionado_name_inner {
            color: rgb(16, 0, 79)  !important;
            margin-left: 1rem  !important;
            margin-top: 0.5rem  !important;
            font-size: 0.9rem  !important;
            white-space: nowrap  !important;
            overflow: hidden  !important;
            text-overflow: ellipsis  !important;
            width: 100%  !important;
            font-weight: bold  !important;
        }

        .combo_assento {
            background: #1b0088  !important;
            color: #ffffff  !important;
            border: 0px  !important;
            outline: none  !important;
        }

        .botoes_avanca_confirma {
            display: block;
            position: fixed;

            width: 100%;
            bottom: 0vh;
            left: 0;
            background: white;
            box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #e8114b;
        }

        .col-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .botoes_assento {
            background: none rgb(232, 17, 75);
            border-radius: 0.5rem;
            font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
            font-weight: 600;
            line-height: 1;
            text-transform: none;
            transition: none 0s ease 0s;
            border: 0.0625rem solid rgb(232, 17, 75);
            color: rgb(255, 255, 255);
            font-size: 1.125rem;
            height: 3rem;
            padding: 0.75rem 2rem;
            cursor: pointer;
        }

        .close {
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .5;
            cursor: pointer;
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
        }

        .div_botao {
            position: relative;
            width: 100%;
        }

        .colulna_totalizador_left {
            color: rgb(16, 0, 79);
            font-size: 1.3rem;
        }

        .colulna_totalizador_right {
            color: rgb(16, 0, 79);
            font-weight: bold;
            font-size: 1.2rem;
        }

        .cart_destaque {
            border: 1px solid #dcdcdc;
            padding: 15px 15px 15px 15px;
            margin-bottom: 0;
            background: #ffff;
        }
    </style>
</head>
<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<!-- ########################### !-->
<!-- ############ LOGIN ######## !-->
<!-- ########################### !-->
<div data-role="page" data-theme="a" data-url="login" class="page_login" id="login">
    <div role="main" class="ui-content">
        <div style="padding: 25px;" class="painel_login page_login_data">
            <div style="text-align: center;margin-bottom: 25px;color: #ffffff;text-shadow: none;">
                <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" style="width: 160px;">
                <h3 style="color: #0b0b0b;">Área do Cliente</h3>
            </div>

            <div style="margin-bottom: 20px;">
                <label style="font-weight: 500;text-transform: none;">CPF*</label>
                <input name="imput_login_cpf" id="imput_login_cpf" placeholder="xxx.xxx.xxx-xx" value="" type="tel" required="required">
            </div>

            <?php if ($this->Settings->usar_dtnascimento_area_cliente) {?>
                <div style="margin-bottom: 20px;">
                    <label style="font-weight: 500;text-transform: none;">Data de Nascimento*</label>
                    <input name="imput_login_data_nascimento" id="imput_login_data_nascimento" value="" placeholder="dd/mm/yyyy" type="tel" required="required">
                </div>
            <?php } ?>

            <?php if ($this->Settings->usar_email_area_cliente) {?>
                <div style="margin-bottom: 20px;">
                    <label style="font-weight: 500;text-transform: none;">E-mail*</label>
                    <input name="imput_login_email" id="imput_login_email" placeholder="Seu e-mail de cadastro" value="" type="email" required="required">
                </div>
            <?php } ?>
            <input name="logar" type="submit" class="buttom_login" value="Entrar" id="logar">
        </div>
    </div>
</div>
<div data-role="page" data-theme="a" data-url="home" id="home">
    <div data-role="header" data-position="fixed" style="text-align: center;">
        <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" style="width: 160px;">
    </div>
    <div role="main" class="ui-content page_main">
        <button class="show-page-loading-msg" id="show-page-loading-msg" style="display: none;" data-textonly="false" data-textvisible="true" data-msgtext="" data-inline="true">Icon + text</button>
        <button class="hide-page-loading-msg" id="hide-page-loading-msg" style="display: none;" data-inline="true" data-icon="delete">Hide</button>
        <div style="margin-left: 2%;text-align: center;">
            <h2><span id="nome_cliente"></span></h2>
            <div data-role="navbar">
                <ul  class="ui-nodisc-icon my-ul" id="div-marcar-assento" style="display: none;">
                    <li class="pg-inicial">
                        <a  style="border-radius: 10px;background: #64b4ef;color: #000000;text-shadow: none;"
                           data-ajax="true"
                           id="href_marcacao_assento"
                           data-transition="turn">
                            <div style="float: left;color: #000000;font-size: 16px;"><h4>MARCAR ASSENTO<br/><small style="font-size: 12px;">Marcar assentos de embarque</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/marcacao.png"></div>
                        </a>
                    </li>
                </ul>
                <ul  class="ui-nodisc-icon my-ul">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/proximas_viagens'?>"
                           style="border-radius: 10px; background: #00a85a;color: #ffff;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           id="href_proximas_viagens"
                           data-transition="flip">
                            <div style="float: left;color: #ffffff;font-size: 16px;"><h4>PRÓXIMAS VIAGENS <br/><small style="font-size: 12px;">Baixar Voucher</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/viajar.png"></div>
                        </a>
                    </li>
                </ul>
                <ul  class="ui-nodisc-icon my-ul">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/viagens_passadas'?>"
                           style="border-radius: 10px;background: #ffcc2a;color: #000000;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           id="href_viagens_passadas"
                           data-transition="flip">
                            <div style="float: left;color: #000000;font-size: 16px;"><h4>VIAGENS ANTERIORES<br/><small style="font-size: 12px;">Histórico de Viagens</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/viagem_passada.png"></div>
                        </a>
                    </li>
                </ul>
                <ul  class="ui-nodisc-icon my-ul">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/minhas_faturas'?>"
                           style="border-radius: 10px;background: #1a3493;color: #ffff;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           id="href_meus_pagamentos"
                           data-transition="flip"
                           data-transition="none">
                            <div style="float: left;color: #ffffff;font-size: 16px;"><h4>MINHAS FATURAS<br/><small style="font-size: 12px;">Minhas cobranças</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/faturas2.png"></div>
                        </a>
                    </li>
                </ul>
                <ul  class="ui-nodisc-icon my-ul">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/meus_dados'?>"
                           style="border-radius: 10px;background: #ff5722;color: #ffff;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           id="href_meus_dados"
                           data-transition="flip">
                            <div style="float: left;color: #ffffff;font-size: 16px;"><h4>MEUS DADOS<br/><small style="font-size: 12px;">Informações pessoais</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/user2.png"></div>
                        </a>
                    </li>
                </ul>
                <ul class="ui-nodisc-icon my-ul" style="display: none;">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/marcacao_assento'?>"
                           style="border-radius: 10px;background: #64b4ef;color: #000000;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           data-transition="none">
                            <div style="float: left;color: #000000;font-size: 16px;"><h4>Fazer Check-in<br/><small style="font-size: 12px;">Marcar assentos de embarque</small></h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/checkin.png"></div>
                        </a>
                    </li>
                </ul>
                <ul class="ui-nodisc-icon ul" style="display: none;">
                    <li class="pg-inicial">
                        <a href="<?=base_url().'myapp/material_apoio'?>"
                           style="border-radius: 10px;background: <?=$this->Settings->theme_color;?>;color: #ffff;text-shadow: none;"
                           data-ajax="true"
                           data-transition="none">
                            <div style="float: left;color: #ffffff;font-size: 16px;"><h4>Material de Apoio</h4></div>
                            <div style="float: right;"><img style="width: 55px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/briefcase.png"></div>
                        </a>
                    </li>
                </ul>
                <ul class="ui-nodisc-icon my-ul">
                    <li class="pg-inicial">
                        <a style="border-radius: 10px;background: #9797a5;color: #ffff;text-shadow: none;padding: 30px;"
                           data-ajax="true"
                           id="sair-app"
                           data-transition="none">
                            <div style="float: left;color: #ffffff;font-size: 16px;"><h4>SAIR DO APP</h4></div>
                            <div style="float: right;"><img style="width: 35px;filter: invert(100%);" src="<?php echo $assets ?>myapp/img/exit2.png"></div>
                        </a>
                    </li>
                </ul>
            </div><!-- /navbar -->
        </div>
    </div>
    <?php
    $phone = $vendedor->phone;
    $phone = str_replace ( '(', '', str_replace ( ')', '', $phone));
    $phone = str_replace ( '-', '',  $phone);
    $phone = str_replace ( ' ', '',  $phone);
    ?>
    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="<?=$this->Settings->url_site_domain;?>" target="_blank"><button data-icon="home">Site</button></a></li>
                <li><a href="https://api.whatsapp.com/send?phone=55<?php echo trim($phone)?>" target="_blank"><button data-icon="phone">Telefone</button></a></li>
            </ul>
        </div>
    </div>
</div>

<!--script jQuery!-->
<script type="text/javascript" src="<?php echo $assets ?>myapp/js/util/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $assets ?>myapp/js/util/jquery.mobile-1.4.5.js"></script>
<script type="text/javascript" src="<?php echo $assets ?>myapp/js/util/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo $assets ?>myapp/js/util/jquery-confirm.min.js"></script>

<script type="text/javascript" src="<?php echo $assets ?>myapp/js/index.js"></script>
<script type="text/javascript" src="<?php echo $assets ?>myapp/js/controllerNotification.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/appcompra/js/jquery-confirm.min.js"></script>

<script type="text/javascript">

    var audio_error = new Audio('<?=$assets?>sounds/sound3.mp3');

    var controllerNotificacao = new ControllerNotificacao();

    $(document).on("pageinit", function(event){
       $('ul.my-ul').listview().listview('refresh');

        $('.pg-inicial').css('padding', '10px');
        $('.pg-inicial').css('margin-left', '10px');
    });

    $(document).ready(function () {

        $( document ).on( "click", ".show-page-loading-msg", function() {
            var $this = $( this ),
                theme = $this.jqmData( "theme" ) || $.mobile.loader.prototype.options.theme,
                msgText = $this.jqmData( "msgtext" ) || $.mobile.loader.prototype.options.text,
                textVisible = $this.jqmData( "textvisible" ) || $.mobile.loader.prototype.options.textVisible,
                textonly = !!$this.jqmData( "textonly" );
            html = $this.jqmData( "html" ) || "";

            $.mobile.loading( "show", {
                text: msgText,
                textVisible: textVisible,
                theme: theme,
                textonly: textonly,
                html: html
            });

        }).on( "click", ".hide-page-loading-msg", function() {$.mobile.loading( "hide" );});

        $('#logar').click(function (event) {

            var cpf = getString('imput_login_cpf');
            var dataNascimento = getString('imput_login_data_nascimento');
            var email = getString('imput_login_email');

            if (cpf === '') {controllerNotificacao.showAlert('CPF obrigatório', function () {}); return;};

            <?php if ($this->Settings->usar_dtnascimento_area_cliente) {?>
                if (dataNascimento === '') {controllerNotificacao.showAlert('Data de Nascimento obrigatório', function () {}); return;};
            <?php } ?>

            <?php if ($this->Settings->usar_email_area_cliente) {?>
                if (email === '') {controllerNotificacao.showAlert('E-mail obrigatório', function () {}); return;};
            <?php } ?>

            $.ajax({
                type: "get",
                url: '<?=base_url().'myapp/logar'?>',
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                    cpf : cpf,
                    data_nascimento: dataNascimento,
                    email: email
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (resultado) {

                    if (resultado.retorno) {

                        $('#href_marcacao_assento').attr('href', '<?=base_url().'myapp/viagens_checkin/'?>' + resultado.user.id);
                        $('#href_proximas_viagens').attr('href', '<?=base_url().'myapp/proximas_viagens/'?>' + resultado.user.id);
                        $('#href_viagens_passadas').attr('href', '<?=base_url().'myapp/viagens_passadas/'?>' + resultado.user.id);
                        $('#href_meus_pagamentos').attr('href', '<?=base_url().'myapp/minhas_faturas/'?>' + resultado.user.id);
                        $('#href_meus_dados').attr('href', '<?=base_url().'myapp/meus_dados/'?>' + resultado.user.id);

                        $('#nome_cliente').html('<small style="text-transform: none;font-size: 14px;">Bem Vindo(a)</small> ' + resultado.user.nome);

                        $('#imput_login_cpf').val('');
                        $('#imput_login_data_nascimento').val('');
                        $('#imput_login_email').val('');

                        if (resultado.habilitar_marcacao) {
                            $('#div-marcar-assento').show();
                        }

                        $.mobile.changePage("#home");
                    } else {
                        controllerNotificacao.showAlert('NÃO ENCONTRAMOS SEU CADASTRO CONFORME AS INFORMAÇÕES', function () {});
                    }
                }
            });

        });

        $('#imput_login_cpf').keyup(function (event){
            mascaraCpf( this, formataCPF );
        });

        $("#imput_login_data_nascimento").on("input", function() {
            const inputValue = $(this).val();
            const formattedValue = formatInputDate(inputValue);
            $(this).val(formattedValue);
        });

        $('#sair-app').click(function (event){
            location.reload();
        });

        $.mobile.changePage("#login");
        $('#div-marcar-assento').hide();
    });

    $( document ).on( "pageinit", "#minhas-viagens", function( event ) {

        let product_id          = $('#product_id').val();
        let programacao_id      = $('#programacao_id').val();
        let tipoTransporte_id   = $('#tipo_transporte_id').val();

        $('#iframe_onibus').attr('src', '<?=base_url()?>' + "bus/marcacao/" + product_id + '/' + programacao_id + '/' + tipoTransporte_id);

        j_passageiros_selecionados_assento  = [];
        cliente_selecionado_assento         = null;
        assento_selecionado_tag             = null;

        onlick_events_selecionar_cliente_assento();
        desbloquear_todos_assentos_session_card();
    });

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCPF(cpf){
        //retira os caracteres indesejados...
        cpf = cpf.replace(/[^\d]/g, "");

        //realizar a formatação...
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function formatInputDate(inputValue) {
        const cleanedValue = inputValue.replace(/[^\d]/g, "");
        const day = cleanedValue.slice(0, 2);
        const month = cleanedValue.slice(2, 4);
        const year = cleanedValue.slice(4, 8);

        return `${day}/${month}/${year}`;
    }

    var cliente_selecionado_assento = null;
    var assento_selecionado_tag = null;
    var j_passageiros_selecionados_assento = [];

    function onlick_events_selecionar_cliente_assento() {

        $('.cc_assentos').click(function(event){

            let id = $(this).attr('sel').replace("dependente", "");

            if ($('#div_assentos_marcacao_confirma').is(':visible')) {
                if (cliente_selecionado_assento !== id) {
                    return;
                }
            }

            $('.cc_assentos').addClass('cc_assentos_opaco');
            $('.cc_assentos').removeClass('cc_assento_selecionado')

            $(this).removeClass('cc_assentos_opaco');
            $(this).addClass('cc_assento_selecionado');

            cliente_selecionado_assento = id;

            let clienteName = $('#assento_dependente' + id).html();

            var novo_passageiro_selecionado = {
                id: id,
                nome_cliente: clienteName,
                assento: "",
                tag: null,
            };

            var clienteEncontrado = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.id === cliente_selecionado_assento;
            });

            if (!clienteEncontrado) {
                j_passageiros_selecionados_assento.push(novo_passageiro_selecionado);
                assento_selecionado_tag = null;
            } else {
                assento_selecionado_tag = clienteEncontrado.tag;
                if (clienteEncontrado.assento !== '') {
                    $('#botao_confirma').hide();
                    $('#div_assentos_marcacao_desmarca').show(300);
                    $('#pop_down_seleciona_assento_poltrona').html('Assento '+clienteEncontrado.assento);
                }
            }

            let nomePartes = clienteName.trim().split(' ');

            if (nomePartes.length >= 2) {
                let primeiroNome = nomePartes[0];
                let ultimoNome = nomePartes[nomePartes.length - 1];

                $('#pop_up_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
                $('#pop_down_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
            }
        });

        if ($('.cc_assentos')[0] !== undefined) {
            $('.cc_assentos')[0].click();//TODO CLICA NO PRIMEIRO PASSAGEIRO PARA INICIAR A SELECAO DE POLTRONAS
        }
    }

    function selecionar_assento(tag) {

        $('#div_assentos_marcacao_desmarca').hide();
        $('#botao_confirma').hide();

        let assento = tag.getAttribute('data-position-order_name');

        if (cliente_selecionado_assento == null) {
            alert("Nenhum passageiro selecionado para atribuir um assento.");

            tag.classList.remove('seat-reservado');
            tag.classList.add('seat-enabled');
            $('#botao_confirma').show();

            return;
        }

        if ($('#div_assentos_marcacao_confirma').is(':visible')) {

            let assento_selecionado = assento_selecionado_tag.getAttribute('data-position-order_name');

            if (assento !== assento_selecionado) {
                tag.classList.remove('seat-reservado');
                tag.classList.add('seat-enabled');
            }

            return;
        }

        if (assento_selecionado_tag !== null) {

            let assento_selecionado = assento_selecionado_tag.getAttribute('data-position-order_name');

            if (assento !== assento_selecionado) {

                var assentoJaSelecionado = j_passageiros_selecionados_assento.find(function(cliente) {
                    return cliente.assento === assento && cliente.id !== cliente_selecionado_assento;
                });

                if (assentoJaSelecionado) {//verificar se o assento ja foi selecionado para outro passageiro da venda
                    audio_error.play();
                    $('#botao_confirma').show();

                    return;
                } else {
                    assento_selecionado_tag.classList.remove('seat-reservado');
                    assento_selecionado_tag.classList.add('seat-enabled');
                    $('#assento' + cliente_selecionado_assento).val('');
                }
            }
        } else {

            var assentoJaSelecionado = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.assento === assento && cliente.id !== cliente_selecionado_assento;
            });

            if (assentoJaSelecionado) {//verificar se o assento ja foi selecionado para outro passageiro da venda
                audio_error.play();
                $('#botao_confirma').show();
                return;
            }
        }

        let clienteName = $('#assento_dependente' + cliente_selecionado_assento).html();
        let nomePartes = clienteName.trim().split(' ');

        if (nomePartes.length >= 2) {
            let primeiroNome = nomePartes[0];
            let ultimoNome = nomePartes[nomePartes.length - 1];

            $('#pop_up_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
            $('#pop_down_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
        }

        $('#pop_up_seleciona_assento_poltrona').html('Assento ' + assento);
        $('#pop_down_seleciona_assento_poltrona').html('Assento ' + assento);
        $('#div_assentos_marcacao_confirma').show(300);

        assento_selecionado_tag = tag;//todo selecionar tag do assento para o cliente selecionado
    }

    function confirmarAssentoMarcado() {

        $('#div_assentos_marcacao_confirma').hide(100, function() {

            $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();
            $('#botao_confirma').show();

            if (assento_selecionado_tag == null) {
                return;
            }

            let assento = assento_selecionado_tag.getAttribute('data-position-order_name');

            $('#assento' + cliente_selecionado_assento).val(assento);

            // Adiciona a nova opção ao select
            $("#select_assento_selecionadodependente" + cliente_selecionado_assento).append($("<option></option>").val(assento).text(assento));

            var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.id === cliente_selecionado_assento;
            });

            if (clienteExist) {
                clienteExist.assento = assento;
                clienteExist.tag = assento_selecionado_tag;

                $('.cc_assento_selecionado').next().click();//TODO PROXIMO ASSENTO
            }
        });
    }

    function desmarcarAssento() {

        $('#div_assentos_marcacao_desmarca').hide(100);

        $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();

        assento_selecionado_tag.classList.remove('seat-reservado');
        assento_selecionado_tag.classList.add('seat-enabled');
        assento_selecionado_tag = null;

        $('#assento' + cliente_selecionado_assento).val('');

        var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
            return cliente.id === cliente_selecionado_assento;
        });

        if (clienteExist) {
            clienteExist.assento = '';
            clienteExist.tag = null;
        }

        $('#botao_confirma').show();
    }

    function close_marcacao() {

        $('#div_assentos_marcacao_confirma').hide(100);
        $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();

        assento_selecionado_tag.classList.remove('seat-reservado');
        assento_selecionado_tag.classList.add('seat-enabled');
        assento_selecionado_tag = null;

        $('#assento' + cliente_selecionado_assento).val('');

        var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
            return cliente.id === cliente_selecionado_assento;
        });

        if (clienteExist) {
            clienteExist.assento = '';
            clienteExist.tag = null;
        }

        $('#botao_confirma').show();
    }

    function close_desmarcar() {
        $('#div_assentos_marcacao_desmarca').hide(100);
        $('#botao_confirma').show();
    }

    function confirmar_bloqueio_assento() {

        var assentoPreenchido = j_passageiros_selecionados_assento.find(function(cliente) {
            return cliente.assento !== '';
        });

        if (assentoPreenchido) {

            let product_id = $('#product_id').val();
            let programacao_id = $('#programacao_id').val();
            let tipoTransporte_id = $('#tipo_transporte_id').val();

            var jsonData = JSON.stringify({
                product_id: product_id,
                programacao_id: programacao_id,
                tipo_transporte_id: tipoTransporte_id,
                assentos: j_passageiros_selecionados_assento,
            });

            $.ajax({
                type: "get",
                data: {
                    dados: jsonData,
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                url: "<?=base_url()?>bus/marcacao_assento_area_cliente",
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.ocupado) {
                        $.confirm({
                            title: 'Atençao!',
                            closeIcon: true,
                            content: retorno.error,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                ok: function () {
                                    console.log('ok');
                                },
                            }
                        });
                    } else {
                        $.mobile.changePage("#home");

                        $.confirm({
                            title: 'Sucessso!',
                            type: 'green',
                            content: 'Check-in realizado com sucesso!',
                            autoClose: 'ok|5000',
                            buttons: {
                                ok: function () {}
                            }
                        });
                    }
                }
            });
        } else {
            $.mobile.changePage("#home");
        }
    }

    function bloqueio_assento_site() {
        $.confirm({
            title: 'Atençao!',
            closeIcon: true,
            content: 'Deseja realmente confirmar o Check-in?',
            type: 'blue',
            typeAnimated: true,
            buttons: {
                confirmar: function () {
                    confirmar_bloqueio_assento();
                },
                cancelar: function () {},
            }
        });
    }

    function desbloquear_todos_assentos_session_card() {
        $.ajax({
            type: "get",
            url: "<?=base_url()?>bus/desbloquear_todos_assentos_session_card",
            data: {
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
            },
            contentType: 'application/json',
            dataType: 'json',
            success: function (retorno) {}
        });
    }

    window.addEventListener('beforeunload', desbloquear_todos_assentos_session_card);

</script>
</body>

</html>
