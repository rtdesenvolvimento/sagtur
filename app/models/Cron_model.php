<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use GuzzleHttp\Promise\Utils;

class Cron_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //repository
        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/RatingRepository_model', 'RatingRepository_model');

        //service
        $this->load->model('service/RatingsService_model', 'RatingsService_model');
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/GoogleContactsService_model', 'GoogleContactsService_model');

        //model
        $this->load->model('model/MotivoCancelamentoVenda_model', 'MotivoCancelamentoVenda_model');

        $this->load->model('financeiro_model');
        $this->load->model('sales_model');
        $this->load->model('site');
    }

    /**
     * Run Cron Job - Nao tem funcao especifica, apenas para testes
     */
    public function run_cron()
    {
        return 'Cron job test';
    }

    /**
     * Run Diary - Cancelamento das vendas automaticamente e envio de e-mails das avaliacoes
     */
    public function run_diary()
    {
        //TODO ESSA FUNCAO E CHAMADA TODO DIA DEMANHA AS 6H
        $configuralGeral = $this->getSettingsDB();
        $log = 'cliente '.$configuralGeral->site_name;

        $log .= $this->cancelamento_automatico_faturas($configuralGeral, $log);

        //TODO CRIAR AVALIACAO DE VIAGEM QUE PASSARAM
        if ($configuralGeral->avaliar) {
            $this->enviar_avaliacao($configuralGeral);
        }

        return $log;
    }

    /**
     * Run Periodical - Envio de e-mails periodicos
     */
    public function run_email_queue()
    {
        define("DEMO", 0);

        $m = '';
        $database_padrao = 'demonstracaosagtur';
        $otherdb = $this->load->database($database_padrao, TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();
        $m .= $this->sent_email_queue($otherdb);

        foreach ($dbs as $db_name)
        {
            if ($this->isTableIgnore($db_name)) {
                $otherdb = $this->load->database($db_name, TRUE);
                $m .= $this->sent_email_queue($otherdb);
            }
        }

        return $m;
    }

    private function sent_email_queue($dbutil)
    {
        $this->sma->Settings = $this->getSettingsByConnection($dbutil);

        if ($this->sma->Settings->status == 1) {

            $emails = $this->getEmailsQueue($dbutil);

            if (empty($emails)) {
                return 'No emails to send db '.$this->sma->Settings->site_name;
            }

            foreach ($emails as $email) {
                try {

                    $this->sma->send_email($email->to, $email->subject, $email->body, $email->from, $email->from_name, $email->attachment, $email->cc, $email->bcc);
                    $dbutil->update('email_queue', array('status' => 'sent', 'sent_at' => date('Y-m-d H:i:s')), array('id' => $email->id));

                    if ($email->email_origin == 'sales' && $email->object_id) {
                        $this->log_send_mail($dbutil, $email->object_id);
                    }

                    if ($email->attachment) {
                        unlink($email->attachment);
                    }

                } catch (Exception $e) {
                    $attempts = $email->attempts + 1;
                    if ($attempts >= 3) {
                        $dbutil->update('email_queue', array('status' => 'failed', 'error' => $e->getMessage(), 'attempts' => $attempts), array('id' => $email->id));
                    } else {
                        $dbutil->update('email_queue', array('status' => 'pending', 'error' => $e->getMessage(), 'attempts' => $attempts), array('id' => $email->id));
                    }
                }
            }
            return count($emails) . ' emails sent db '.$this->sma->Settings->site_name;
        }

        return '';
    }

    public  function log_send_mail($dbutil, $sale_id) {

        $data = array(
            'date' => date('Y-m-d H:i:s'),
            'user_id' => 0,
            'user' => 'Sistema',
            'status' => 'E-MAIL ENVIADO',
            'event' =>  'E-mail do voucher foi enviado para o passageiro responsável pela compra.',
            'log' => print_r($_POST, true),
            'sale_id' => $sale_id,
        );

        return $dbutil->insert('sale_events', $data);
    }

    /**
     * Run Desbloqueio de Assentos - Desbloqueia os assentos que estao travados no link de reservas
     */
    public function run_desbloqueio_assento()
    {
        $m = '';
        $database_padrao = 'demonstracaosagtur';
        $otherdb = $this->load->database($database_padrao, TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();
        $m .= $this->atualizar_assentos($database_padrao);

        foreach ($dbs as $db_name)
        {
            if ($this->isTableIgnore($db_name)) {
                $m .= $this->atualizar_assentos($db_name);
            }
        }

        return $m;
    }


    /**
     * Run Create Google Contacts - Cria os contatos no Google Contacts
     */
    public function run_create_google_contacts()
    {
        $configuralGeral = $this->getSettingsDB();
        $log = 'cliente '.$configuralGeral->site_name.'<br/>';

        if ($this->getPlugSignActive()){
            $log .= ' - Sending persons to Google Contacts<br/>';
            $log .= $this->export_peoples_to_google_contact();
            $log .= ' - Sent persons to  Google Contacts<br/>';
        }

        return $log;
    }

    /**
     * Run Export Docs to CloudFlare R2  - Envia para o CloudFlare os documentos de comprovantes de pagamento para o CloudFlare R2
     */
    public function run_exportar_docs_to_cloudflarer2(): string
    {

        $payments = $this->getPayments();
        $m = '';

        foreach ($payments as $payment) {

            if ($payment->attachment) {

                $filePath = 'assets/uploads/' . $payment->attachment;

                if (file_exists($filePath)) {

                    $fileMimeType = mime_content_type($filePath);
                    $fileCreationTime = date('c', filemtime($filePath)); // ISO 8601 format

                    $m .= 'empresa '.$this->session->userdata('cnpjempresa'). 'uploading file '.$payment->attachment.' ID'. $payment->id .'<br/>';

                    $client = new S3Client($this->getConfig());
                    $folder_company = $this->session->userdata('cnpjempresa');

                    $config_bucket= [
                        'Bucket'     => 'sagtur',
                        'Key'        => $folder_company.'/docs/'.$payment->attachment,// Caminho no R2
                        'SourceFile' => $filePath,
                        'ContentType'=> $fileMimeType, // Define o tipo do documento
                        'ACL'        => 'public-read',
                        'Metadata'   => [
                            'original-creation-time' => $fileCreationTime, // Guarda a data original como metadado
                        ],
                    ];

                    $client->putObject($config_bucket);

                    $this->db->update('payments', array('attachment' => NULL, 'attachment_storage' => $folder_company.'/docs/'.$payment->attachment), array('id' => $payment->id));

                    unlink($filePath);//DELETE O ARQUIVO LOCAL
                }
            }
        }

        return $m;
    }

    public function run_exportar_img_banners_to_cloudflarer2(): string
    {
        $folder_company = $this->session->userdata('cnpjempresa');

        $m = '<pre/>scanner to '.$folder_company.'<pre/>';

        $configuracao = $this->getSettings();

        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->logo_shop, 'shop/banners', 'logo_shop');
        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->slider1, 'shop/banners', 'slider1');
        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->slider2, 'shop/banners', 'slider2');
        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->slider3, 'shop/banners', 'slider3');
        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->about_photo, 'shop/about_photo', 'about_photo');
        $m .= $this->exportar_img_banners_to_cloudflarer2($configuracao->assinatura, 'images/signature', 'assinatura');

        return $m;
    }

    public function exportar_img_banners_to_cloudflarer2($logo_name, $storage_path, $db_column): string
    {

        if ($logo_name == '') {
            return '';
        }

        $m = '';
        $folder_company = $this->session->userdata('cnpjempresa');

        $filePath = 'assets/uploads/logos/shop/' . $logo_name; // Caminho local do arquivo

        if (file_exists($filePath)) {

            $m .= 'Empresa ' . $folder_company . ' enviando arquivo ' . $filePath . '<br/>';

            $client = new S3Client($this->getConfig());
            $fileMimeType = mime_content_type($filePath);
            $fileCreationTime = date('c', filemtime($filePath)); // ISO 8601 format

            $config_bucket = [
                'Bucket'     => 'sagtur',
                'Key'        => $folder_company . '/' . $storage_path . '/' . $logo_name, // Caminho dinâmico no R2
                'SourceFile' => $filePath,
                'ContentType'=> $fileMimeType,
                'ACL'        => 'public-read',
                'Metadata'   => [
                    'original-creation-time' => $fileCreationTime,
                ],
            ];

            $client->putObject($config_bucket);

            // Atualiza a coluna correta no banco
            $this->db->update('settings', [
                $db_column => NULL,
                $db_column . '_storage' => $folder_company . '/' . $storage_path . '/' . $logo_name
            ], ['setting_id' => 1]);


            if ($filePath == 'assets/uploads/logos/shop/a99d8b08781acac4c4750a45da6ae1f5.jpg' ||
                $filePath == 'assets/uploads/logos/shop/0bc3cd37b73ae2470e49541630e1ef28.jpg'||
                $filePath == 'assets/uploads/logos/shop/about.jpg') {
                //nao deleta
            } else {
                unlink($filePath); // Remove arquivo local após upload
            }
        }

        return $m;
    }

    public function run_exportar_img_gallery_cloudflarer2(): string
    {
        $m = '';

        $galeries = $this->getGalleryItens();
        $folder_company = $this->session->userdata('cnpjempresa');
        $client = new S3Client($this->getConfig());

        foreach ($galeries as $galleray_item) {
            if ($galleray_item->photo) {

                $file_name = $galleray_item->photo;
                $filePath = 'assets/uploads/' . $file_name; // Caminho local do arquivo
                $fileThumbsPath = 'assets/uploads/thumbs/' . $file_name; // Caminho local do arquivo

                if (file_exists($filePath)) {

                    $m .= 'Empresa ' . $folder_company . ' enviando arquivo ' . $filePath . '<br/>';

                    $fileMimeType = mime_content_type($filePath);
                    $fileCreationTime = date('c', filemtime($filePath)); // ISO 8601 format

                    // Upload para o Cloudflare R2
                    $config_bucket = [
                        'Bucket'     => 'sagtur',
                        'Key'        => "$folder_company/shop/gallery/$file_name",
                        'SourceFile' => $filePath,
                        'ContentType'=> $fileMimeType,
                        'ACL'        => 'public-read',
                        'Metadata'   => [
                            'original-creation-time' => $fileCreationTime,
                        ],
                    ];
                    $client->putObject($config_bucket);

                    unlink($filePath); // Remove arquivo local após upload

                    $this->db->update('gallery_item', array('photo' => '', 'photo_storage' => $folder_company.'/shop/gallery/'.$file_name), array('id' => $galleray_item->id));
                }

                if (file_exists($fileThumbsPath)) {
                    // Upload para o Cloudflare R2 da thumbnail
                    $config_bucket = [
                        'Bucket'     => 'sagtur',
                        'Key'        => "$folder_company/shop/gallery/thumbs/$file_name",
                        'SourceFile' => $fileThumbsPath,
                        'ContentType'=> $fileMimeType,
                        'ACL'        => 'public-read',
                        'Metadata'   => [
                            'original-creation-time' => $fileCreationTime,
                        ],
                    ];

                    $client->putObject($config_bucket);

                    unlink($fileThumbsPath); // Remove arquivo local após upload
                }
            }
        }

        return $m;
    }

    public function run_exportar_docs_customer_cloudflarer2(): string
    {
        $m = '';

        $customers = $this->getAllCompaniesIsIMG();
        $client = new S3Client($this->getConfig());
        $folder_company = $this->session->userdata('cnpjempresa');

        foreach ($customers as $customer) {

            if ($customer->image) {

                $filePath = 'assets/uploads/' . $customer->image; // Caminho local do arquivo
                if (file_exists($filePath) && ($customer->image != 'no_image.png' && $customer->image != 'no_image.jpg')) {

                    $m .= 'Empresa ' . $folder_company . ' cliente ' . $customer->name . ' arquivo ' . $filePath .'<br/>';
                    $fileMimeType = mime_content_type($filePath);
                    $fileCreationTime = date('c', filemtime($filePath)); // ISO 8601 format

                    $config_bucket = [
                        'Bucket'     => 'sagtur',
                        'Key'        => $folder_company . '/documents/customers/images/' . $customer->image, // Caminho dinâmico no R2
                        'SourceFile' => $filePath,
                        'ContentType'=> $fileMimeType,
                        'ACL'        => 'public-read',
                        'Metadata'   => [
                            'original-creation-time' => $fileCreationTime,
                        ],
                    ];

                    $client->putObject($config_bucket);

                    $this->db->update('companies', array('image' => NULL, 'image_storage' => $folder_company . '/documents/customers/images/' . $customer->image), array('id' => $customer->id));

                    unlink($filePath); // Remove arquivo local após upload
                }
            }
        }

        $m.= $this->run_exportar_docs_customer_cloudflarer2_docs($folder_company, $customer->name, $client);

        return $m;
    }

    public function run_exportar_docs_customer_cloudflarer2_docs($folder_company, $name, $client): string
    {
        $photos = $this->getCustomerPhotos();

        $m = '';

        foreach ($photos as $photo) {
            if ($photo->photo) {
                $filePath = 'assets/uploads/' . $photo->photo; // Caminho local do arquivo
                if (file_exists($filePath)) {

                    $m .= 'Empresa ' . $folder_company . ' cliente ' . $name. ' arquivo ' . $filePath .'<br/>';
                    $fileMimeType = mime_content_type($filePath);
                    $fileCreationTime = date('c', filemtime($filePath)); // ISO 8601 format

                    $config_bucket = [
                        'Bucket'     => 'sagtur',
                        'Key'        => $folder_company . '/documents/customers/docs/' . $photo->photo, // Caminho dinâmico no R2
                        'SourceFile' => $filePath,
                        'ContentType'=> $fileMimeType,
                        'ACL'        => 'public-read',
                        'Metadata'   => [
                            'original-creation-time' => $fileCreationTime,
                        ],
                    ];

                    $client->putObject($config_bucket);

                    $this->db->update('customer_photos', array('photo' => NULL, 'photo_storage' => $folder_company . '/documents/customers/docs/' . $photo->photo), array('id' => $photo->id));

                    unlink($filePath); // Remove arquivo local após upload

                }
            }
        }

        return $m;
    }

    public function getCustomerPhotos()
    {
        $q = $this->db->get("customer_photos");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    private function getGalleryItens()
    {
        $q = $this->db->get_where("gallery_item");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCompaniesIsIMG() {

        $this->db->where('image !=', '');
        $q = $this->db->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function getConfig(): array
    {
        $config =
            [
                'endpoint' => 'https://11652bc094dc1f514aaafb293ce84bd9.r2.cloudflarestorage.com',
                'version' => 'latest',
                'region' => 'auto',
                'credentials' =>
                    [
                        'key' => 'a2ffaffdd37af96137755f22eab7d61e',
                        'secret' => 'f86afbe7c1287f3b55d4685a6b485089290d279f9026958774b06ba45c2d2cfc',
                    ],
            ];

        return $config;
    }

    private function getEmailsQueue($db) {

        $db->where('status', 'pending');

        $q = $db->get("email_queue");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    private function cancelamento_automatico_faturas($configuralGeral, $m = '')
    {
        if ($faturas = $this->getFaturasSemPagamentoVencida()) {
            $parc = 0;
            foreach($faturas as $fatura) {
                if ($fatura->sale_id) {

                    $sale = $this->site->getSaleByID($fatura->sale_id);

                    if ($sale->payment_status == 'due') {//TODO APENAS VENDAS ABERTAS PODEM SER CANCELADAS

                        $note = 'Venda cancelada automaticamente pelo sistema na data '.date('d/m/Y H:i:s');

                        $motivoCancelamento             = new MotivoCancelamentoVenda_model();
                        $motivoCancelamento->sale_id    = $fatura->sale_id;
                        $motivoCancelamento->note       = $note;
                        $motivoCancelamento->motivo_cancelamento_id = $configuralGeral->motivo_cancelamento_padrao_id;

                        $this->sales_model->cancelar($motivoCancelamento);

                        $this->send_email($fatura->sale_id);
                    }
                } else {
                    $note = 'Fatura cancelada automaticamente pelo sistema na data '.date('d-m-Y H:i:s');
                    $this->FinanceiroService_model->cancelar($fatura->id, $note);
                }
                $parc++;
            }
            $m .= '<p>'.$parc.' faturas examinadas para o cancelamento automatico.</p>';
        }

        return $m;
    }

    private function enviar_avaliacao($configuralGeral)
    {
        try {

            $ratingsArray = $this->RatingsService_model->saveAll();

            foreach ($ratingsArray as $rating) {
                $this->send_email_avaliacao($rating, $configuralGeral);
            }

        } catch (Exception $exception) {}
    }

    private function export_peoples_to_google_contact()
    {
        try {
            return $this->GoogleContactsService_model->export_peoples();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function send_email_avaliacao($rating_id, $configuralGeral) {

        $url_site_domain = $this->getUrlSiteDomain($configuralGeral);
        $rating          = $this->site->getRatingID($rating_id);
        $customer        = $this->site->getCompanyByID($rating->customer_id);
        $product         = $this->site->getProductByID($rating->product_id);

        $this->load->library('parser');

        $parse_data = array(
            'url'               => $url_site_domain.'/rating/'.$rating->uuid,
            'product_name'      => $product->name,
            'contact_person'    => $customer->name,
            'company'           => $customer->company,
            'site_link'         => base_url(),
            'site_name'         => $configuralGeral->site_name,
            'logo'              => '<img src="' . base_url() . 'assets/uploads/logos/' . $configuralGeral->logo2 . '" alt="' . $configuralGeral->site_name . '"/>',
        );

        $rating_temp = file_get_contents('./themes/default/views/email_templates/ratings.html');

        $subject = 'Obrigado pela sua presença - por favor, deixe uma avaliação 🌟 ' . $configuralGeral->site_name;

        if ($customer->email) {
            $to = $customer->email;

            $message = $this->parser->parse_string($rating_temp, $parse_data);

            $body = <<<EOT
                $message
            EOT;

            $this->sma->send_email_api($to, $subject, $body);
        }
    }

    public function send_email($id = null) {

        $configuralGeral = $this->getSettingsDB();

        $inv        = $this->sales_model->getInvoiceByID($id);
        $customer   = $this->site->getCompanyByID($inv->customer_id);
        $biller     = $this->site->getCompanyByID($inv->biller_id);

        $this->load->library('parser');
        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company,
            'site_link' => base_url(),
            'site_name' => $configuralGeral->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $configuralGeral->logo2 . '" alt="' . $configuralGeral->site_name . '"/>',
        );

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            if ($inv->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/orcamento.html');
            } else if ($inv->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/lista_espera.html');
            } else if ($inv->sale_status == 'cancel') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale_cancel.html');
            } else {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            }
        } else {
            if ($inv->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/orcamento.html');
            } else if ($inv->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/lista_espera.html');
            } else if ($inv->sale_status == 'cancel')  {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale_cancel.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
            }
        }

        if ($inv->sale_status == 'orcamento') {
            $subject = 'Orçamento (' . $inv->reference_no . ') DE ' . $configuralGeral->site_name;
        } else if ($inv->sale_status == 'lista_espera') {
            $subject =  'Lista de Espera (' . $inv->reference_no . ') DE ' . $configuralGeral->site_name;
        } else if ($inv->sale_status == 'cancel') {
            $subject =  'Venda Cancelada (' . $inv->reference_no . ') DE ' . $configuralGeral->site_name;
        } else {
            $subject =  'Venda (' . $inv->reference_no . ') DE ' . $configuralGeral->site_name;
        }

        if ($customer->email) {
            $to = $customer->email;
            $cc = $biller->email;
        } else {
            $to = $biller->email;
        }

        $message = $this->parser->parse_string($sale_temp, $parse_data);

        $body = <<<EOT
                $message
            EOT;

        $this->sma->send_email_api($to, $subject, $body, $cc);
    }

    public function atualizar_assentos($db)
    {
        $m = '';

        try {

            $otherdb = $this->load->database($db, TRUE);
            $configuracaoGeral = $this->getSettingsByConnection($otherdb);

            if ($configuracaoGeral->status == 1) {

                $m = 'Atualizando Assento(s) '.$db.'<br/>';

                $tempo_expirar_limite_minutos = 10;

                $sql = 'UPDATE sma_assento_bloqueio
                        SET 
                            active = 0, 
                            updated_at = NOW()
                        WHERE active = 1 AND cart_lock = 1 AND cart_time_blocked <= NOW() - INTERVAL '. $tempo_expirar_limite_minutos .' MINUTE';

                $otherdb->query($sql);
            }

        } catch (Exception $exception) {
            $m .= $exception->getMessage();
        }

        return $m;
    }

    public function run_cron_bkp()
    {
        $m = '';
        if ($this->resetOrderRef()) {
            $m .= '<p>Reference number has been reset.</p>';
        }
        if ($pendingInvoices = $this->getAllPendingInvoices()) {
            $p = 0;
            foreach ($pendingInvoices as $invoice) {
                $this->updateInvoiceStatus($invoice->id);
                $p++;
            }
            $m .= '<p>' . $p . ' pending invoices status has been changed to due.</p>';
        }
        if ($partialInvoices = $this->getAllPPInvoices()) {
            $pp = 0;
            foreach ($partialInvoices as $invoice) {
                $this->updateInvoiceStatus($invoice->id);
                $pp++;
            }
            $m .= '<p>' . $pp . ' partially paid invoices status has been changed to due.</p>';
        }
        if ($unpaidpurchases = $this->getUnpaidPuchases()) {
            $up = 0;
            foreach ($unpaidpurchases as $purchase) {
                $this->db->update('purchases', array('payment_status' => 'due'), array('id' => $purchase->id));
                $up++;
            }
            $m .= '<p>' . $up . ' pending/partially paid purchases has been changed to due.</p>';
        }
        if ($pis = $this->get_expired_products()) {
            $e = 0; $ep = 0;
            foreach($pis as $pi) {
                $this->db->update('purchase_items', array('quantity_balance' => 0), array('id' => $pi->id));
                $e++;
                $ep += $pi->quantity_balance;
            }
            $this->site->syncQuantity(NULL, NULL, $pis);
            $m .= '<p>'.$e.' products with total quantity of '.$ep.' are expired.</p>';
        }
        if ($promos = $this->getPromoProducts()) {
            $pro = 0;
            foreach($promos as $pr) {
                $this->db->update('products', array('promotion' => 0), array('id' => $pr->id));
                $pro++;
            }
            $m .= '<p>'.$pro.' product promotions are expired.</p>';
        }

        if ($faturas = $this->getAllFaturasPendentesDePagamento()) {
            $parc = 0;
            foreach($faturas as $fatura) {
                $this->calcularJurosMultaParcelasVencida($fatura);
                $parc++;
            }
            $m .= '<p>'.$parc.' faturas examinadas para calculo de juros e multa por atraso no pagamento.</p>';
        }

        $date = date('Y-m-d H:i:s', strtotime('-1 month'));
        if ($this->deleteUserLgoins($date)) {
            $m .= '<p>User login records previous to ' . $date . ' had been deleted.</p>';
        }
        if ($this->db_backup()) {
            $m .= '<p>Database backup successful and backups older then 30 days are deleted.</p>';
        }
        if ($this->checkUpdate()) {
            $m .= '<p>New update(s) available, please visit the updates menu under settings.</p>';
        }
        $r = $m != '' ? $m : false;
        return $r;
    }

    public function calcularJurosMultaParcelasVencida($fatura) {

        $parcelasFatura = $this->financeiro_model->getParcelasByFatura($fatura->id);
        $totalAcrescimoNovoCalculado = 0;

        foreach ($parcelasFatura as $parcelaFatura) {

            $parcela = $this->financeiro_model->getParcelaById($parcelaFatura->parcela);

            if ($parcela->dtultimopagamento != null ) $dataVencimento = $parcela->dtultimopagamento;
            else $dataVencimento = $parcela->dtvencimento;

            $percentualMulta = $this->getPercentualMulta();
            $percentualJurosMora = $this->getPercentualJuros();

            $diasAtraso = $this->sma->diasDeAtrasoConta($dataVencimento);

            $multa = $this->getValorMulta($parcela->valorvencimento, $dataVencimento);
            $mora = $this->getJurosMora($parcela->valorvencimento, $dataVencimento);

            if ($diasAtraso > 0) $status = Financeiro_model::STATUS_VENCIDA;
            else if ($fatura->valorpago > 0) $status = Financeiro_model::STATUS_PARCIAL;
            else $status = Financeiro_model::STATUS_ABERTA;

            $totalAcrescimoNovoCalculado = $multa + $mora;
            $valorPagarParcela = $parcela->valorvencimento;

            $atualizarDados = array(

                'valorpagar' => (float) number_format($valorPagarParcela + $totalAcrescimoNovoCalculado, 2),
                'totalAcrescimo' => (float) number_format( $totalAcrescimoNovoCalculado, 2),

                'multa' => (float) $multa,
                'mora' => (float) $mora,

                'percentualmulta' =>(float) $percentualMulta,
                'percentualmora' => (float) $percentualJurosMora,

                'acrescimo' => (float) number_format($totalAcrescimoNovoCalculado, 2),
                'diasatraso' => $diasAtraso,

                'status' => $status
            );

            $this->financeiro_model->edit('parcela', $atualizarDados, 'id', $parcela->id);
         }

        $this->calcularJurosMultaFaturaVencida($fatura);
    }

    private function calcularJurosMultaFaturaVencida($fatura) {

        if ($fatura->dtultimopagamento != null ) $dataVencimento = $fatura->dtultimopagamento;
        else $dataVencimento = $fatura->dtvencimento;

        $percentualMulta = $this->getPercentualMulta();
        $percentualJurosMora = $this->getPercentualJuros();

        $diasAtraso = $this->sma->diasDeAtrasoConta($dataVencimento);

        $multa = $this->getValorMulta($fatura->valorfatura, $dataVencimento);
        $mora = $this->getJurosMora($fatura->valorfatura, $dataVencimento);

        $totalAcrescimoNovoCalculado = $multa + $mora;
        $valorPagarParcela = $fatura->valorfatura;

        if ($diasAtraso > 0) $status = Financeiro_model::STATUS_VENCIDA;
        else if ($fatura->valorpago > 0) $status = Financeiro_model::STATUS_PARCIAL;
        else $status = Financeiro_model::STATUS_ABERTA;

        echo 'dias='.$diasAtraso.'<br/>';
        echo 'multa='.$multa.'<br/>';
        echo 'mora='.$mora.'<br/>';
        echo 'fatura='.$fatura->id.'<br/>';


        $atualizarDados = array(
            'valorpagar' => (float)  number_format( $valorPagarParcela + $totalAcrescimoNovoCalculado, 2),
            'totalAcrescimo' => (float) number_format(  $totalAcrescimoNovoCalculado, 2),

            'multa' => $multa,
            'mora' => $mora,

            'percentualmulta' => (float) $percentualMulta,
            'percentualmora' => (float) $percentualJurosMora,

            'acrescimo' => (float) number_format($totalAcrescimoNovoCalculado, 2),
            'diasatraso' => $diasAtraso,

            'status' => $status
        );

        $this->financeiro_model->edit('fatura', $atualizarDados, 'id', $fatura->id);
        $this->financeiro_model->edit('parcela_fatura',  array('status' => Financeiro_model::STATUS_VENCIDA),'fatura', $fatura->id);
    }

    public function getValorMulta($valorPagar, $dataVencimento) {

        $percentualMulta = $this->getPercentualMulta();

        if( $this->isContaVencida($dataVencimento) && $percentualMulta > 0)  {
            return $valorPagar * $percentualMulta/100;
        }

        return 0;
    }

    public function getJurosMora($valorPagar, $dataVencimento) {

        $moraDia = $this->getPercentualJuros();

        if($this->isContaVencida($dataVencimento) && $moraDia > 0) {
            $dias = $this->sma->diasDeAtrasoConta($dataVencimento);
            return  ($moraDia * $dias);
        }

        return 0;
    }

    private function isContaVencida($dataVencimento)
    {
        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));
        return strtotime($dataVencimento) < strtotime($dataHoje);
    }

    public function getPercentualMulta() {
        $settings = $this->getSettings();
        return $settings->percentualMulta;
    }

    public function getPercentualJuros() {
        $settings = $this->getSettings();
        return $settings->percentualJurosMoraDia;
    }

    public function getAllFaturasPendentesDePagamento() {

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');
        $q = $this->db->get("fatura");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPendingInvoices()
    {
        $today = date('Y-m-d');
        $paid = $this->lang->line('paid');
        $canceled = $this->lang->line('cancelled');
        $q = $this->db->get_where('sales', array('due_date <=' => $today, 'due_date !=' => '1970-01-01', 'due_date !=' => NULL, 'payment_status' => 'pending'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPPInvoices()
    {
        $today = date('Y-m-d');
        $paid = $this->lang->line('paid');
        $canceled = $this->lang->line('cancelled');
        $q = $this->db->get_where('sales', array('due_date <=' => $today, 'due_date !=' => '1970-01-01', 'due_date !=' => NULL, 'payment_status' => 'partial'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateInvoiceStatus($id)
    {
        if ($this->db->update('sales', array('payment_status' => 'due'), array('id' => $id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function resetOrderRef()
    {
        $settings = $this->getSettings();
        if ($settings->reference_format == 1 || $settings->reference_format == 2) {
            $month = date('Y-m') . '-01';
            $year = date('Y') . '-01-01';
            if ($ref = $this->getOrderRef()) {
                $reset_ref = array('date' => $month, 'so' => 1, 'qu' => 1, 'po' => 1, 'to' => 1, 'pos' => 1, 'do' => 1, 'pay' => 1, 're' => 1, 'rep' => 1, 'ex' => 1);
                if ($settings->reference_format == 1) {
                    if (strtotime($ref->date) < strtotime($year)) {
                        $this->db->update('order_ref', $reset_ref, array('ref_id' => 1));
                        return TRUE;
                    }
                } elseif ($settings->reference_format == 2) {
                    if (strtotime($ref->date) < strtotime($month)) {
                        $this->db->update('order_ref', $reset_ref, array('ref_id' => 1));
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }

    public function getOrderRef()
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSettings()
    {
        $q = $this->db->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSettingsByConnection($db)
    {
        $q = $db->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteUserLgoins($date)
    {
        $this->db->where('time <', $date);
        if ($this->db->delete('user_logins')) {
            return true;
        }
        return FALSE;
    }

    public function checkUpdate()
    {
        $settings = $this->getSettings();
        $fields = array('version' => $settings->version, 'code' => $settings->purchase_code, 'username' => $settings->envato_username, 'site' => base_url());
        $this->load->helper('update');
        $protocol = is_https() ? 'https://' : 'http://';
        $updates = get_remote_contents($protocol.'tecdiary.com/api/v1/update/', $fields);
        $response = json_decode($updates);
        if (!empty($response->data->updates)) {
            $this->db->update('settings', array('update' => 1), array('setting_id' => 1));
            return TRUE;
        }
        return FALSE;
    }

    public function get_expired_products() {
        $settings = $this->getSettings();
        if ($settings->remove_expired) {
            $date = date('Y-m-d');
            $this->db->where('expiry <=', $date)->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')->where('quantity_balance >', 0);
            $q = $this->db->get('purchase_items');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return FALSE;
    }

    public function getUnpaidPuchases()
    {
        $today = date('Y-m-d');
        $q = $this->db->get_where('purchases', array('payment_status !=' => 'paid', 'payment_status !=' => 'due', 'payment_term >' => 0, 'due_date !=' => NULL, 'due_date <=' => $today));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPromoProducts()
    {
        $today = date('Y-m-d');
        $q = $this->db->get_where('products', array('promotion' => 1, 'end_date !=' => NULL, 'end_date <=' => $today));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function db_backup() {
        $this->load->dbutil();
        $prefs = array(
            'format' => 'txt',
            'filename' => 'sma_db_backup.sql'
        );
        $back = $this->dbutil->backup($prefs);
        $backup =& $back;
        $db_name = 'db-backup-on-' . date("Y-m-d-H-i-s") . '.txt';
        $save = './files/backups/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);

        $files = glob('./files/backups/*.txt', GLOB_BRACE);
        $now   = time();
        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24 * 30) {
                    unlink($file);
                }
            }
        }

        return TRUE;
    }

    public function getFaturasSemPagamentoVencida() {

        $this->db->select('fatura.id, fatura.sale_id');

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id=fatura.tipoCobranca');

        $this->db->where('fatura.status', 'ABERTA');//TODO APENAS FATURAS ABERTAS OU SEJA, SEM PAGAMENTOS
        $this->db->where('automatic_cancellation_sale', 1);//TODO CANCELAMENTO AUTOMATICO ATIVO NO TIPO DE COBRANCA
        $this->db->where('fatura.dtvencimento<', date('Y-m-d')); // A data de vencimento é anterior à data atual ou seja ja esta atrasada

        // Calcula a diferença em dias entre a data de vencimento e a data atual
        $this->db->where('DATEDIFF(NOW(), sma_fatura.dtvencimento) >= numero_dias_cancelamento', NULL, FALSE);

        $this->db->group_by('fatura.sale_id');

        $q = $this->db->get("fatura");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSettingsDB()
    {
        $q = $this->db->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPayments()
    {
        $this->db->where('attachment !=', NULL);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPlugSignActive()
    {

        try {
            $plugsignSetting = $this->getPlugsignSettings();

            if ($plugsignSetting->active && $plugsignSetting->token) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return false;
        }
    }

    public function getPlugsignSettings()
    {
        $q = $this->db->get('plugsign');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function isTableIgnore($db)
    {
        return  ($db != 'information_schema' && $db != 'mysql' && $db != 'performance_schema' && $db != 'phpmyadmin' && $db != 'sys' && $db != '_session_database' && $db != 'sagtur_administrativo');
    }

    private function getUrlSiteDomain($configuralGeral) {

        $url_site_domain = MY_Controller::URL_SITE_SUA_RESERVA_ONLINE.'/'.$this->session->userdata('cnpjempresa');

        if ($configuralGeral->own_domain && $configuralGeral->url_site) {
            $url_site_domain = $configuralGeral->url_site;//TODO SITE PROPRIO
        } elseif ($configuralGeral->own_domain) {
            $url_site_domain = 'https://'. $this->session->userdata('cnpjempresa') .'.suareservaonline.com.br';//TODO SUBDOMINIO
        } else if ($configuralGeral->url_site == MY_Controller::URL_SITE_KATATUR) {
            $url_site_domain = MY_Controller::URL_SISTEMA_KATATUR;
        }

        return $url_site_domain;
    }

}
