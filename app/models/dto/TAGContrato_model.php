<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TAGContrato_model extends CI_Model
{

    /**
     * Desconsiderar USO INTERNO
     */
    const PAGADOR = '_pagador';

    /**
     * DIA DO MÊS
     */
    const DIA = 'dia';

    /**
     * MÊS POR EXTENSO
     */
    const MES = 'mes';

    /**
     * MÊS NUMERAL
     */
    const MES_NUMERAL = 'mes_numero';

    /**
     * ANO
     */
    const ANO = 'ano';

    /**
     * DATA POR EXTENSO
     */
    const DATA_HOJE_EXTENSO = 'data_hoje_extenso';

    /**
     * SEXO DO PASSAGEIRO
     */
    const SEXO_PASSAGEIRO = 'sexo';

    /**
     * CUMPRIMENTO DO PASSAGEIRO (SENHOR/SENHORA)
     */
    const CUMPRIMENTO = 'cumprimento';

    /**
     * NOME DO PASSAGEIRO
     */
    const NOME_PASSAGEIRO = 'nome_passageiro';

    /**
     * NOME DO PASSAGEIRO
     */
    const NOME_RESPONSAVEL = 'nome_responsavel';

    /**
     * CPF DO PASSAGEIRO
     */
    const CPF_PASSAGEIRO = 'cpf_passageiro';

    /**
     * RG DO PASSAGEIRO
     */
    const RG_PASSAGEIRO = 'rg_passageiro';

    /**
     * ORGÃO EMISSOR DO RG DO PASSAGEIRO
     */
    const ORGAO_EMISSOR_RG = 'orgao_emissor_rg';

    /**
     * TIPO DE DOCUMENTO DO PASSAGEIRO
     */
    const TIPO_DOCUMENTO = 'tipo_documento_passageiro';

    /**
     * ENDEREÇO DO PASSAGEIRO
     */
    const ENDERECO_PASSAGEIRO = 'endereco_passageiro';

    /**
     * BAIRRO DO PASSAGEIRO
     */
    const BAIRRO_PASSAGEIRO = 'bairro_passageiro';

    /**
     * COMPLEMENTO DO ENDEREÇO DO PASSAGEIRO
     */
    const COMPLEMENTO_PASSAGEIRO = 'complemento_passageiro';

    /**
     * CIDADE DO PASSAGEIRO
     */
    const CIDADE_PASSAGEIRO = 'cidade_passageiro';

    /**
     * NÚMERO DO ENDEREÇO DO PASSAGEIRO
     */
    const NUMERO_PASSAGEIRO = 'numero_endereco_passageiro';

    /**
     * CEP DO PASSAGEIRO
     */
    const CEP_PASSAGEIRO = 'cep_passageiro';

    /**
     * ESTADO DO PASSAGEIRO
     */
    const ESTADO_PASSAGEIRO = 'estado_passageiro';

    /**
     * PAÍS DO PASSAGEIRO
     */
    const PAIS = 'pais';

    /**
     * E-MAIL DO PASSAGEIRO
     */
    const EMAIL_PASSAGEIRO = 'email_passageiro';

    /**
     * DATA DE NASCIMENTO DO PASSAGEIRO
     */
    const DATA_NASCIMENTO = 'data_nascimento';

    /**
     * TELEFONES DO PASSAGEIRO (FIXO E CELULAR)
     */
    const TELEFONES_PASSAGEIRO = 'telefone_passageiro';

    /**
     * TELEFONE DE EMERGÊNCIA
     */
    const TELEFONE_EMERGENCIAL = 'telefone_emergencia';

    /**
     * APENAS O TELEFONE
     */
    const APENAS_TELEFONE = 'telefone';

    /**
     * APENAS O CELULAR
     */
    const APENAS_CELULAR = 'celular';

    /**
     * NACIONALIDADE DO PASSAGEIRO
     */
    const NATURALIDADE_PASSAGEIRO = 'naturalidade_passageiro';

    /**
     * TIPO DE FAIXA ETÁRIA DO PASSAGEIRO
     */
    const TIPO_FAIXA = "tipo_faixa";

    /**
     * FAIXA ETÁRIA DO PASSAGEIRO
     */
    const PLANO_SAUDE = "plano_saude";

    /**
     * CÓDIGO DA VENDA
     */
    const CODIGO_CONTRATO = 'reference_no';

    /**
     * TOTAL DE PASSAGEIROS
     */
    const TOTAL_PASSAGEIROS = 'total_passageiros';

    /**
     * TOTAL DE PASSAGEIROS POR EXTENSO
     */
    const TOTAL_PASSAGEIROS_EXTENSO = 'total_passageiros_extenso';

    /**
     * VALOR TOTAL DA VENDA
     */
    const SUB_TOTAL_VENDA = 'sub_total_venda';

    /**
     * VALOR TOTAL DA VENDA POR EXTENSO
     */
    const SUB_TOTAL_VENDA_EXTENSO = 'sub_total_venda_extenso';

    /**
     * VALOR POR PASSAGEIRO
     */
    const PRECO_POR_ITEM = 'preco';

    /**
     * CONDIAÇÃO DE PAGAMENTO DA VENDA
     */
    const CONDICAO_PAGAMENTO = 'condicao_pagamento';

    /**
     * TIPO DE COBRANÇA DA VENDA
     */
    const TIPO_COBRANCA = 'tipo_cobranca';

    /**
     * OBSERVAÇÃO DA VENDA
     */
    const OBSERVACAO_VENDA = 'observacao_venda';

    /**
     * DATA DO PRIMEIRO VENCIMENTO
     */
    const DATA_PRIMEIRO_VENCIMENTO = 'data_primeiro_vencimento';

    /**
     * TOTAL DE PAGAMENTO DA VENDA
     */
    const TOTAL_PAGAMENTO = 'total_pagamento';


    /**
     * NOME DA EMPRESA DO VENDEDOR
     */
    const NOME_EMPRESA_VENDEDOR = 'nome_empresa_vendedor';

    /**
     * NOME DO VENDEDOR
     */
    const NOME_VENDEDOR = 'nome_vendedor';

    /**
     * CPF OU CNPJ DO VENDEDOR
     */
    const CPF_CNPJ_VENDEDOR = 'cpf_cnpj_vendedor';

    /**
     * LOCA DE EMBARQUE DO PASSAGEIRO
     */
    const LOCAL_EMBARQUE = 'local_embarque';

    /**
     * HORA DE EMBARQUE DO PASSAGEIRO
     */
    const HORA_EMBARQUE = 'hora_embarque';

    /**
     * DATA DE EMBARQUE DO PASSAGEIRO
     */
    const DATA_EMBARQUE = 'data_embarque';

    /**
     * TIPO DE HOSPEDAGEM
     */
    const TIPO_HOSPEDAGEM = 'tipo_hospedagem';

    /**
     * NOME DO PACOTE
     */
    const NOME_VIAGEM = 'viagem';

    /**
     * DATA DE SAÍDA DA VIAGEM
     */
    const DATA_SAIDA_VIAGEM = 'data_saida';

    /**
     * DATA DE SAÍDA DA VIAGEM POR EXTENSO
     */
    const DATA_SAIDA_VIAGEM_EXTENSO = 'data_saida_extenso';

    /**
     * POLTRONA DO PASSAGEIRO
     */
    const POLTRONA_PASSAGEIRO = 'poltrona_passageiro';

    /**
     * NOME DA CATEGORIA DO PACOTE
     */
    const TIPO_PACOTE = 'tipo_pacote';

    /**
     * NOME DO TRANSPORTE
     */
    const TIPO_TRANSPORTE = "tipo_transporte";

    /**
     * DATA DA VENDA
     */
    const DATA_VENDA = 'data_venda';

    /**
     * DATA DO RETORNO DA VIAGEM
     */
    const DATA_RETORNO = 'data_retorno';

    /**
     * HORA DE SAIDA DA VIAGEM
     */
    const HORA_SAIDA = 'hora_saida';

    /**
     * HORA DE RETORNO DA VIAGEM
     */
    const HORA_RETORNO = 'hora_retorno';

    /**
     * DADOS DIGITADOS EM:: SOBRE A VIAGEM
     */
    const SOBRE_A_VIAGEM = 'sobre_viagem';

    /**
     * DADOS DIGITADOS EM:: O QUE INCLUI
     */
    const O_QUE_INCLUI = 'oque_inclui';

    /**
     * DADOS DIGITADOS EM:: ROTEIRO
     */
    const ROTEIRO = 'roteiro';

    /**
     * DADOS DIGITADOS EM:: VALORES E CONDIÇÕES
     */
    const VALORES_E_CONDICOES = 'valores_condicoes';

    /**
     * DADOS DIGITADOS EM:: DETALHES GERAIS DA VIAGEM
     */
    const DETALHES_GERAIS_DA_VIAGEM = 'detalhes_gerais_viagem';

    /**
     * COM HOSPEDAGEM: SIM/NÃO
     */
    const IS_HOSPEDAGEM = 'com_hospedagem';

    /**
     * SEM HOSPEDAGEM: SIM/NÃO
     */
    const IS_SEM_HOSPEDAGEM = 'sem_hospedagem';

    /**
     * TABELA COMPLETA COM OS DADOS DOS DEPENDENTES
     */
    const  TB_DEPENDENTES = 'tabela_dependentes';

    public function __construct() {
        parent::__construct();
    }

}
