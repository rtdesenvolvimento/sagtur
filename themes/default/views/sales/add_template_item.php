<ul id="myTabMeusPacotes" class="nav nav-tabs">
    <li class=""><a href="#passageiro1" class="tab-grey" style="text-align: center;"><i class="fa fa-user" style="font-size: 20px;"></i><br/><span id="meu-pacote-new-contato"> <?= lang("label.novo.passageiro")?></span></a></li>
</ul>
<div class="tab-content">
    <div id="passageiro1" style="padding: 0px;" class="tab-pane fade in">
        <div class="box-header">
            <h2 class="blue"><?= lang("header.label.adicionar.passageiro")?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 15px;">
                    <div class="row" style="margin-top: -15px;">
                        <div class="col-md-3" style="display: none;">
                            <div class="form-group">
                                <?= lang("tipoContato", "tipoContato"); ?>
                                <select id="tipoContato" name="tipoContato" class="form-control">
                                    <option value="buscar">Contato existente</option>
                                    <option value="criar">Criar Novo Contato</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12" id="view-cliente-ja-cadastrado">
                            <div class="form-group">
                                <?= lang("cliente", "search-customer"); ?>
                                <?php echo form_input('search-customer', (isset($_POST['search-customer']) ? $_POST['search-customer'] : ""), 'id="search-customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div id="view-novo-contato" style="display: none;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group company">
                                    <?= lang("sexo", "sexo"); ?>
                                    <select id="sexo" name="sexo" class="form-control" required="required">
                                        <option value="FEMININO">FEMININO</option>
                                        <option value="MASCULINO">MASCULINO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group person">
                                    <?= lang("name", "slName"); ?>
                                    <?php echo form_input('name', '', 'class="form-control tip" id="slName" autocomplete="off" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("vat_no", "vat_no"); ?>
                                    <?php echo form_input('vat_no', '', 'class="form-control cpf" id="vat_no"'); ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <?= lang("email_address", "email_address"); ?>
                                    <input type="email" name="email"  id="email" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= lang('data_aniversario', 'data_aniversario'); ?>
                                    <input type="date" name="data_aniversario" value="" class="form-control tip" id="data_aniversario">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div><b><?php echo lang("tipo_documento", "tipo_documento");?></b></div>
                                    <select id="tipo_documento" name="tipo_documento" class="form-control" required="required">
                                        <option value="rg">RG</option>
                                        <option value="passaporte">Passaporte</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rg"><div id="div_rgPassaporte"><b>R.G</b></div></label>
                                    <?php echo form_input('cf1', '', 'class="form-control rg" id="cf1"'); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="emissaorg"><div id="div_vlPassaporteRG"><b>Emissão RG</b></div></label>
                                    <input type="date" name="validade_rg_passaporte" value="" class="form-control tip" id="validade_rg_passaporte">
                                </div>
                            </div>
                            <div class="col-md-2" id="div_orgaoemissor">
                                <div class="form-group">
                                    <?= lang("ccf3", "cf3"); ?>
                                    <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("naturalidade", "naturalidade"); ?>
                                    <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("phone", "phone"); ?>
                                    <input type="tel" name="phone" class="form-control" id="phone"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("ccf5", "cf5"); ?>
                                    <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("telefone_emergencia", "telefone_emergencia"); ?>
                                    <?php echo form_input('telefone_emergencia', '', 'class="form-control" id="telefone_emergencia"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("nQuarto", "nQuarto"); ?>
                                    <?php echo form_input('nQuarto', '', 'class="form-control input-tip" id="nQuarto"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend style="cursor: pointer;" id="field-view-endereco-new-contact">Clique aqui para ver endereço</legend>
                                    <div id="div_endereco" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("postal_code", "postal_code"); ?>
                                                    <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <?= lang("address", "address"); ?>
                                                    <?php echo form_input('address', '', 'class="form-control" id="address" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("country", "country"); ?>
                                                    <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("city", "city"); ?>
                                                    <?php echo form_input('city', '', 'class="form-control" id="city"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("state", "state"); ?>
                                                    <?php echo form_input('state', '', 'class="form-control" id="state"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <ul id="myTabMeusPacotesParametros" class="nav nav-tabs">
                    <li class=""><a href="#receita-passageiro" class="tab-grey" style="text-align: center;"><i class="fa fa-money" style="font-size: 20px;"></i><br/>Taxas, Tarifas e Valores Recebidos do Passageiro</a></li>
                    <li class=""><a href="#taxas-fornecedor" class="tab-grey" style="text-align: center;"><i class="fa fa-usd" style="font-size: 20px;"></i><br/>Taxas, Tarifas e Valores Pago ao Fornecedor</a></li>
                </ul>
                <div  class="tab-content">
                    <div id="receita-passageiro" style="padding: 0px;" class="tab-pane fade in">
                        <div class="sales-table">
                            <style>
                                .totalizador {
                                    background-color: #428bca;
                                    color: #ffff;
                                    border-radius: 10px;
                                    width: 57%;
                                    font-size: 20px;
                                    text-align: center;
                                    margin-top: 15px;
                                }

                                hr {
                                    margin-top: 5px;
                                    margin-bottom: 20px;
                                    border: 0;
                                    border-top: 2px solid #ccc;
                                }

                            </style>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row" style="display: none;">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("currencieCustomer", "slCurrencieCustomer"); ?>
                                                    <?php
                                                    foreach ($currencies as $currencie) {
                                                        $cbMoeda[$currencie->id] = $currencie->name;
                                                    }
                                                    echo form_dropdown('currencieCustomer', $cbMoeda, (isset($_POST['currencieCustomer']) ? $_POST['currencieCustomer'] :  ''), 'id="slCurrencieCustomer" data-placeholder="' . lang("select") . ' ' . lang("currencie") . '" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("tarifaCurrencieCustomer", "slTarifaCurrencieCustomer"); ?>
                                                    <?php echo form_input('tarifaCurrencieCustomer', '0.00', 'class="form-control input-tip mask_money" id="slTarifaCurrencieCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("cambioCustomer", "slCambioCustomer"); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            1 BRL<i class="fa fa-long-arrow-right" style="font-size: 1.2em;"></i>
                                                        </div>
                                                        <?php echo form_input('cambioCustomer', '1.0000', 'class="form-control input-tip" readonly id="slCambioCustomer"'); ?>
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">BRL</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("tarifaRealCustomer", "slTarifaRealCustomer"); ?>
                                                    <?php echo form_input('tarifaRealCustomer', '0.00', 'class="form-control input-tip " readonly id="slTarifaRealCustomer"'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("taxaFornecedorCustomer", "slTaxaFornecedorCustomer"); ?>
                                                    <?php echo form_input('taxaFornecedorCustomer', '0.00', 'class="form-control input-tip mask_money" id="slTaxaFornecedorCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador" style="width: 100%">
                                                    Tarifa + Taxa (BRL)<br/>
                                                    <span id="tarifaTaxaCustomer">R$0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDadosAereo" style="display: none;">
                                            <h2 class="title"><?= lang('header.label.taxas.bagagem.reserva.assento')?></h2>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <?= lang("taxaBagagemCustomer", "slTaxaBagagemCustomer"); ?>
                                                        <?php echo form_input('taxaBagagemCustomer', '0.00', 'class="form-control input-tip mask_money" id="slTaxaBagagemCustomer"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?= lang("reservaAssentoCustomer", "slReservaAssentoCustomer"); ?>
                                                        <?php echo form_input('reservaAssentoCustomer', '0.00', 'class="form-control input-tip mask_money" id="slReservaAssentoCustomer"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="title" style="display: none;"><?= lang('header.label.informacoes.bilhete')?></h2>
                                        <div class="row" style="display: none;" >
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("ravCustomer", "slRavCustomer"); ?>
                                                    <?php echo form_input('ravCustomer', '0.00', 'class="form-control input-tip mask_money" id="slRavCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <br/><br/>
                                                    <input type="checkbox" id="slRemarcacaoCustomer" name="remarcacaoCustomer" />
                                                    <?= lang("remarcacaoCustomer", "slRemarcacaoCustomer"); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("taxasAdicionaisDescontoCustomer", "slTaxasAdicionaisDescontoCustomer"); ?>
                                                    <?php echo form_input('taxasAdicionaisDescontoCustomer', '0.00', 'class="form-control input-tip mask_money" id="slTaxasAdicionaisDescontoCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("taxasAdicionaisCartaoCustomer", "slTaxasAdicionaisCartaoCustomer"); ?>
                                                    <?php echo form_input('taxasAdicionaisCartaoCustomer', '0.00', 'class="form-control input-tip mask_money" id="slTaxasAdicionaisCartaoCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Total Bruto<br/>
                                                    <span id="vlTotalBruto">R$0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="title"><?= lang('header.label.pagamento.cliente.direto.fornecedor')?></h2>
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <?= lang("vlPagamentoFornecedorCustomer", "slVlPagamentoFornecedorCustomer"); ?>
                                                    <?php echo form_input('vlPagamentoFornecedorCustomer', '0.00', 'class="form-control input-tip mask_money" id="slVlPagamentoFornecedorCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;">
                                                <div class="form-group">
                                                    <?= lang("cambioFornecedorCustomer", "slCambioFornecedorCustomer"); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            1 BRL<i class="fa fa-long-arrow-right" style="font-size: 1.2em;"></i>
                                                        </div>
                                                        <?php echo form_input('cambioFornecedorCustomer', '1.0000', 'class="form-control input-tip" readonly id="slCambioFornecedorCustomer"'); ?>
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">BRL</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("vlPagamentoFornecedorRealCustomer", "slVlPagamentoFornecedorRealCustomer"); ?>
                                                    <?php echo form_input('vlPagamentoFornecedorRealCustomer', '0.00', 'class="form-control input-tip" readonly id="slVlPagamentoFornecedorRealCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Pagar na Agência<br/>
                                                    <span id="vlLiquidoCustomer">0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang('condicaoPagamentoFornecedorCustomer', 'slFormaPagamentoFornecedorCustomer'); ?>
                                                    <?php
                                                    $cbFormasPagamento = array(
                                                        '' => lang('select'),
                                                        'dinheiro' => lang('dinheiro'),
                                                        'cartao_credito' => lang('cartao_credito') ,
                                                        'cartao_debito' => lang('cartao_debito'),
                                                        'transferencia' => lang('transferencia'),
                                                        'debito_em_conta' => lang('debito_em_conta'),
                                                        'cheque' => lang('cheque'),
                                                        'boleto' => lang('boleto'),
                                                        'credito_loja' => lang('credito_loja'),
                                                    );
                                                    foreach ($condicoesPagamento as $condicaoPagamento) {
                                                        $cbCondicoesPagamento[$condicaoPagamento->id] = $condicaoPagamento->name;
                                                    } ?>
                                                    <?= form_dropdown('slFormaPagamentoFornecedorCustomer', $cbFormasPagamento, '', 'class="form-control tip" id="slFormaPagamentoFornecedorCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang('bandeiraCartaoFornecedorCustomer', 'slBandeiraCartaoFornecedorCustomer'); ?>
                                                    <?php
                                                    $cbBanceiraCartao = array(
                                                        '' => lang('select'),
                                                        'visa' => lang('visa'),
                                                        'mastercard' => lang('mastercard') ,
                                                        'hibercard' => lang('hibercard'),
                                                        'elo' => lang('elo'),
                                                        'diners' => lang('free'),
                                                        'amex' => lang('amex'),
                                                    );
                                                    ?>
                                                    <?= form_dropdown('bandeiraCartaoFornecedorCustomer', $cbBanceiraCartao,'', 'class="form-control tip" id="slBandeiraCartaoFornecedorCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("nParcelasCustomer", "slNParcelasCustomer"); ?>
                                                    <?php echo form_input('nParcelasCustomer', '0', 'class="form-control input-tip mask_integer" id="slNParcelasCustomer"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="title" style="display: none;"><?= lang('header.label.reembolso')?></h2>
                                        <div class="row" style="display: none;">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("reembolsoTarifaCustomer", "slReembolsoTarifaCustomer"); ?>
                                                    <?php echo form_input('reembolsoTarifaCustomer', '0.00', 'class="form-control input-tip" readonly id="slReembolsoTarifaCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("reembolsoMultaCustomer", "slReembolsoMultaCustomer"); ?>
                                                    <?php echo form_input('reembolsoMultaCustomer', '0.00', 'class="form-control input-tip" readonly id="slReembolsoMultaCustomer"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("reembolsoTaxaReembolsoCustomer", "slReembolsoTaxaReembolsoCustomer"); ?>
                                                    <?php echo form_input('reembolsoTaxaReembolsoCustomer', '0.00', 'class="form-control input-tip" readonly id="slReembolsoTaxaReembolsoCustomer"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="taxas-fornecedor" style="padding: 0px;" class="tab-pane">
                        <div class="box sales-table">
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("tarifaCurrencieSupplier", "slTarifaCurrencieSupplier"); ?>
                                                    <?php echo form_input('tarifaCurrencieSupplier', '0.00', 'class="form-control input-tip mask_money" readonly id="slTarifaCurrencieSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("currencieSupplier", "slCurrencieSupplier"); ?>
                                                    <?php
                                                    foreach ($currencies as $currencie) {
                                                        $cbMoeda[$currencie->id] = $currencie->name;
                                                    }
                                                    echo form_dropdown('currencieSupplier', $cbMoeda, (isset($_POST['currencieSupplier']) ? $_POST['currencieSupplier'] :  ''), 'id="slCurrencieSupplier" data-placeholder="' . lang("select") . ' ' . lang("currencie") . '" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("cambioSupplier", "slCambioSupplier"); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            1 BRL<i class="fa fa-long-arrow-right" style="font-size: 1.2em;"></i>
                                                        </div>
                                                        <?php echo form_input('cambioSupplier', '1.0000', 'class="form-control input-tip" readonly id="slCambioSupplier"'); ?>
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">BRL</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display: none;">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("tarifaRealSupplier", "slTarifaRealSupplier"); ?>
                                                    <?php echo form_input('tarifaRealSupplier', '0.00', 'class="form-control input-tip mask_money" readonly id="slTarifaRealSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("taxaSupplier", "slTaxaSupplier"); ?>
                                                    <?php echo form_input('taxaSupplier', '0.00', 'class="form-control input-tip mask_money" readonly id="slTaxaSupplier"'); ?>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-3" style="display: none">
                                                <div class="form-group">
                                                    <?= lang("descontoPercentualSupplier", "slDescontoPercentualSupplier"); ?>
                                                    <?php echo form_input('descontoPercentualSupplier', '0.00', 'class="form-control input-tip mask_money" id="slDescontoPercentualSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <?= lang("descontoAbsolutoSupplier", "slDescontoAbsolutoSupplier"); ?>
                                                    <?php echo form_input('descontoAbsolutoSupplier', '0.00', 'class="form-control input-tip mask_money" id="slDescontoAbsolutoSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Tarifa + Taxa<br/>
                                                    <span id="vlTarifaTaxaSupplier">R$0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2" style="display:none;">
                                                <div class="form-group">
                                                    <?= lang("comissaoPercentualSupplier", "slComissaoPercentualSupplier"); ?>
                                                    <?php echo form_input('comissaoPercentualSupplier', '0.00', 'class="form-control input-tip mask_money" id="slComissaoPercentualSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("comissaoAbsolutaSupplier", "slComissaoAbsolutoSupplier"); ?>
                                                    <?php echo form_input('comissaoAbsolutaSupplier', '0.00', 'class="form-control input-tip mask_money" id="slComissaoAbsolutoSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="display:none;">
                                                <div class="form-group">
                                                    <?= lang("incentivoPercentualSupplier", "slIncentivoPercentualSupplier"); ?>
                                                    <?php echo form_input('incentivoPercentualSupplier', '0.00', 'class="form-control input-tip mask_money" id="slIncentivoPercentualSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("incentivoAbsolutoSupplier", "slIncentivoAbsolutoSupplier"); ?>
                                                    <?php echo form_input('incentivoAbsolutoSupplier', '0.00', 'class="form-control input-tip mask_money" id="slIncentivoAbsolutoSupplier"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("outrasTaxasSupplier", "slOutrasTaxasSupplier"); ?>
                                                    <?php echo form_input('outrasTaxasSupplier', '0.00', 'class="form-control input-tip mask_money" id="slOutrasTaxasSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("taxasCartaoSupplier", "slTaxasCartaoSupplier"); ?>
                                                    <?php echo form_input('taxasCartaoSupplier', '0.00', 'class="form-control input-tip mask_money" id="slTaxasCartaoSupplier"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="title"><?= lang('header.label.pagamento.cliente.direto.fornecedor')?></h2>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("pagamentoFornecedorSupplier", "slPagamentoFornecedorSupplier"); ?>
                                                    <?php echo form_input('pagamentoFornecedorSupplier', '0.00', 'class="form-control input-tip mask_money" readonly id="slPagamentoFornecedorSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("tipo_cobranca", "tipoCobrancaFornecedorId"); ?>
                                                    <?php
                                                    $cbTipoCobranca[""] = "";
                                                    foreach ($tiposCobranca as $tc) {
                                                        $cbTipoCobranca[$tc->id] = $tc->name;
                                                    }
                                                    echo form_dropdown('tipoCobrancaFornecedorId', $cbTipoCobranca, (isset($_POST['tipoCobrancaFornecedorId']) ? $_POST['tipoCobrancaFornecedorId'] : $Settings->default_biller), 'id="tipoCobrancaFornecedorId" name="tipoCobrancaFornecedorId" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("Condições", "condicaoPagamentoFornecedorId"); ?>
                                                    <?php
                                                    $cbCondicaoPagamento[""] = "";
                                                    foreach ($condicoesPagamento as $cp) {
                                                        $cbCondicaoPagamento[$cp->id] = $cp->name;
                                                    }
                                                    echo form_dropdown('condicaoPagamentoFornecedorId', $cbCondicaoPagamento, (isset($_POST['condicaoPagamentoFornecedorId']) ? $_POST['condicaoPagamentoFornecedorId'] : $Settings->default_biller), 'id="condicaoPagamentoFornecedorId" name="condicaoPagamentoFornecedorId" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="hidden" value="" id="vlTotalPagamentoFornecedorInput"/>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Pagar p/ Fornecedor<br/>
                                                    <span id="vlTotalPagamentoFornecedor">R$0,00</span>
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="title" style="display: none;"><?= lang('header.label.reembolso')?></h2>
                                        <div class="row" style="display: none;">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("reembolsoTarifaEmbarqueSupplier", "slReembolsoTarifaEmbarqueSupplier"); ?>
                                                    <?php echo form_input('reembolsoTarifaEmbarqueSupplier', '0.00', 'class="form-control input-tip mask_money" id="slReembolsoTarifaEmbarqueSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <?= lang("reembolsoMultaSupplier", "slReembolsoMultaSupplier"); ?>
                                                    <?php echo form_input('reembolsoMultaSupplier', '0.00', 'class="form-control input-tip mask_money" id="slReembolsoMultaSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("reembolsoIncentivoComissaoSupplier", "slReembolsoIncentivoComissaoSupplier"); ?>
                                                    <?php echo form_input('reembolsoIncentivoComissaoSupplier', '0.00', 'class="form-control input-tip mask_money" id="slReembolsoIncentivoComissaoSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("reembolsoTaxasAdicionaisSupplier", "slReembolsoTaxasAdicionaisSupplier"); ?>
                                                    <?php echo form_input('reembolsoTaxasAdicionaisSupplier', '0.00', 'class="form-control input-tip mask_money" id="slReembolsoTaxasAdicionaisSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Pagamento p/ Fornecedor<br/>
                                                   <span>R$0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="title"><?= lang('header.label.comissionamento.consultor')?></h2>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("comissaoConsultorPercentualSupplier", "slComissaoConsultorPercentualSupplier"); ?>
                                                    <?php echo form_input('comissaoConsultorPercentualSupplier', '0.00', 'class="form-control input-tip mask_money" id="slComissaoConsultorPercentualSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("comissaoConsultorAbsolutoSupplier", "slComissaoConsultorAbsolutoSupplier"); ?>
                                                    <?php echo form_input('comissaoConsultorAbsolutoSupplier', '0.00', 'class="form-control input-tip mask_money" id="slComissaoConsultorAbsolutoSupplier"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="text-align: -webkit-right;">
                                                <div class="form-group totalizador">
                                                    Receita da Agência<br/>
                                                    <input type="hidden" id="receitaAgencia"/>
                                                    <span id="vlTotalCustomer">R$0,00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("sale_descricao_servico", "sldescricaoservico"); ?>
                            <?php echo form_textarea('noteitem', (isset($_POST['noteitem']) ? $_POST['noteitem'] : ""), 'class="form-control" id="sldescricaoservico" style="margin-top: 10px; height: 100px;"'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("sale_obsrevacao_item", "slobservacaoitem"); ?>
                            <?php echo form_textarea('observacaoitem', (isset($_POST['observacaoitem']) ? $_POST['observacaoitem'] : ""), 'class="form-control" id="slobservacaoitem" style="margin-top: 10px; height: 100px;"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>