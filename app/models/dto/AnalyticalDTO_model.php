<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AnalyticalDTO_model extends CI_Model
{
    const PAGE_VIEW = 'PAGE_VIEW';
    const PAGE_VIEW_PRODUCT = 'PAGE_VIEW_PRODUCT';

    const PRODUCT_ADDED_CART = 'PRODUCT_ADDED_CART';

    public $product_id;
    public $programacao_id;
    public $type;

    public function __construct() {
        parent::__construct();
    }

}