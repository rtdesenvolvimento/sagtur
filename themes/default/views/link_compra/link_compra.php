<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $Settings->site_name;?>|| RESERVA</title>

    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta name="description" content="<?php echo $Settings->site_name;?> || RESERVA" >
    <meta name="keywords" content="">
    <meta name="application-name" content="Resultatec Sistamas Digitais">
    <meta name="title" content="<?php echo $Settings->site_name;?> || RESERVA">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta property="og:image" content="<?php echo base_url('assets/uploads/logos/logo.png');?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $Settings->site_name;?> || RESERVA" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta name="author" content="Resultec Sistemas Digitais"/>

    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->

    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/themes/theme-default.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/estilo.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/datepicker/css/datepicker.css">

    <link rel="icon" href="<?php echo base_url() ?>/assets/images/favicon.ico"/>

    <style>

        <?php $cor = '#007df9'; //'#c84f50';//'#1395a2;';//'#ff9000'; ?>

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-family:  "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;!important;
        }

        body{
            font-family:  "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;!important;
            margin: 0;
            color: #0b0b0b;!important;
        }

        h2, .h2 {
            font-size: 32px;
            border-radius: 10px;
        }

        .fundo
        {
            background: #d1e9fd;
            background-size: 100% 100%;
            bottom: 0;
            left: 0;
            overflow: auto;
            padding: 3em;
            position: absolute;
            right: 0;
            top: 0;
            font-size: 14px;
        }

        .panel-info>.panel-heading {
            color: #ffffff;
            background-color: <?php echo $cor;?>;
            border-color: #ffffff;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: right;
        }

        .btn-blue {
            background-color: <?php echo $cor;?>;
            border-color: <?php echo $cor;?>;
            color: white;
        }

        .swMain > ul li > a.selected .stepNumber {
            border-color: <?php echo $cor;?>;
        }

        .panel-green, .partition-green {
            background-color: <?php echo $cor;?>;!important;
            background-image: <?php echo $cor;?>
            position: relative;
            background-image: -webkit-gradient(linear, left top, left bottom, from(<?php echo $cor;?> 0px), to(<?php echo $cor;?> 100%));!important;
            background-image: -webkit-linear-gradient(top, <?php echo $cor;?> 0px, <?php echo $cor;?> 100%);!important;
            background-image: -moz-linear-gradient(top, <?php echo $cor;?> 0px, <?php echo $cor;?> 100%);!important;
            background-image: -ms-linear-gradient(top, <?php echo $cor;?> 0px, <?php echo $cor;?> 100%);!important;
            background-image: -o-linear-gradient(top, <?php echo $cor;?> 0px,<?php echo $cor;?> 100%);!important;
            background-image: linear-gradient(top, <?php echo $cor;?> 0px, <?php echo $cor;?> 100%);!important;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#23d1b9 0px', endColorStr='#1fbba6 100%');!important;
            color: white;!important;
        }

        .help-block {
            display: block;
            margin-top: 5px;
            margin-bottom: 10px;
            color: #ffffff;
        }

        .cabecalho {
            color: #010101;
            font-family: 'Patrick Hand', fantasy;
            background-repeat: no-repeat;
        }

        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo $cor;?>;
            background-color: <?php echo $cor;?>;
            color: #fff;
            text-indent: -9999px;
        }

        .swMain > ul li > a.selected:before, .swMain li > a.done:before {
            border-color: <?php echo $cor;?>;
        }

        .panel {
            background-color: #FFFFFF;
            -webkit-box-shadow: 0 1px 2px #c3c3c3;
            -moz-box-shadow: 0 1px 2px #c3c3c3;
            box-shadow: 0 1px 2px #c3c3c3;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            color: #010101;
            border: none;
            position: relative;
        }

        label {
            font-weight: normal;
            color: #010101;
        }

        @media screen and (min-width: 700px) {.painel_principal {}}
        @media screen and (max-width: 500px) {.painel_principal {margin-left: -25%}}
        @media screen and (min-width: 700px) {.logotipo {padding-left: 0%;}}
        @media screen and (max-width: 500px) {.logotipo {width: 50%;margin-left: 20%;margin-top: -20%}}
    </style>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<!-- end: SLIDING BAR -->
<div class="container">
    <div class="fundo">
        <!-- start: FORM WIZARD PANEL -->
        <div class="panel-body col-md-7 col-md-offset-2">
            <!-- Menu -->
            <div class="container-fluid">
                <div class="row cabecalho col-md-12" style="text-align: center;">
                    <a href="#">
                        <img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="Logotipo da empresa">
                    </a>
                </div>
                <div class="row cabecalho col-md-12">
                    <div class="col-md-12" style="text-align: center;">
                        <H2>FORMULÁRIO DE RESERVA<br/><?php echo strtoupper($Settings->site_name);?></H2>
                    </div>
                </div>
            </div>
            <form action="<?php echo base_url().'sales/criarUmaNovaVendaViaLinkDePagamento';?>" role="form" class="smart-wizard form-horizontal" method="get" id="form">
                <div id="wizard" class="swMain">
                    <ul>
                        <li>
                            <a href="#step-1">
                                <div class="stepNumber">1</div>
                                <span class="stepDesc">1&ordm; Etapa</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <div class="stepNumber">2</div>
                                <span class="stepDesc">2&ordm; Etapa</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">
                                <div class="stepNumber">3</div>
                                <span class="stepDesc">3&ordm; Etapa</span>
                            </a>
                        </li>
                    </ul>
                    <div class="progress progress-xs transparent-black no-radius active">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                            <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                    </div>
                    <div id="step-1">

                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"> <h4><div class="fa fa-map-marker"></div> Qual seu destino?</h4>
                                <span class="help-block">
                                    <i class="fa fa-info-circle"></i> verificar os embarques disponíveis do seu roteiro <br/>
                                    <i class="fa fa-info-circle"></i> Atenção 10 mim tolerância nos embarques e nos passeios
                                </span>
                            </div>
                            <div class="panel-body">
                                <?php
                                $contadorViagem =0;
                                $encontrouPacotes = false;
                                $locaisEmbarque = null;

                                if (count($products) > 1 && $produtoId == '') {
                                    foreach ($products as $product) {

                                        $vendas = $this->sales_model->getTotalVendasRealizadasParaAhViagem($product->id);
                                        $totalVendas = 0;
                                        foreach ($vendas as $venda) $totalVendas = $venda->quantity;
                                        if ($totalVendas == '') $totalVendas = 0;

                                        $disponivel = ( $product->quantidadePessoasViagem - $totalVendas);

                                        if ($disponivel <= 0) continue;

                                        $encontrouPacotes = true;

                                        //$locaisEmbarque .= $product->origem;
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7">
                                                <input type="radio" disponivel="<?php echo $disponivel;?>" style="margin-left: 4px;" value="<?php echo $product->id;?>" name="viagem" class="grey radio-callback-local-embarque">
                                                <?php echo $product->name?> <a onclick="verDadosDaViagem(<?php echo $contadorViagem;?>)" style="cursor: pointer;"><i class="fa fa-plus"></i> Detalhes</a>

                                                <div class="panel panel-info divLocalEmbarque" style="margin-top: 20px;display: none;" id="localEmbarque<?php echo $product->id;?>">
                                                    <div class="panel-heading"><div class="fa fa-car"></div> Selecione o seu local de embarque</div>
                                                    <div class="panel-body" id="div_local_embarque">
                                                        <?php

                                                        $locais = $product->origem;
                                                        $locais = strip_tags($locais);
                                                        $locais = explode(';',$locais);

                                                        foreach ($locais as $local){
                                                            if ($local != ''){?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label"></label>
                                                                    <div class="col-sm-7" style="text-align: left;">
                                                                        <input type="radio" style="margin-left: 4px;" value="<?php echo $local;?>" name="local_embarque"  class="grey checkLocalEmbarque">
                                                                        <?php echo $local?>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" id="verDetalhesDoProduto<?php echo $contadorViagem;?>" style="display: none;">
                                                    <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>', strip_tags( str_replace('</p>','@quebra-linha@',$product->details ))) . '</div></div>' : ''; ?>
                                                    <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>',strip_tags(  str_replace('</p>','@quebra-linha@',$product->product_details))) . '</div></div>' : ''; ?>
                                                    <?= $product->itinerario ? '<div class="panel panel-info"><div class="panel-heading">' . lang('Roteiro') . '</div><div class="panel-body">' .str_replace('@quebra-linha@', '<br/><br/>', strip_tags(  str_replace('</p>','@quebra-linha@',$product->itinerario))) . '</div></div>' : ''; ?>
                                                    <?= $product->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading">' . lang('O que inclui') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>',strip_tags( str_replace('</p>','@quebra-linha@',$product->oqueInclui))) . '</div></div>' : ''; ?>
                                                    <?= $product->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading">' . lang('Valores e condições') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>',strip_tags(  str_replace('</p>','@quebra-linha@',$product->valores_condicoes))) . '</div></div>' : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $contadorViagem = $contadorViagem + 1; } ?>
                                <?php } else {

                                    foreach ($products as $product) {

                                        if ($produtoId != '' && $product->id != $produtoId) continue;

                                        $vendas = $this->sales_model->getTotalVendasRealizadasParaAhViagem($product->id);
                                        $totalVendas = 0;
                                        foreach ($vendas as $venda) $totalVendas = $venda->quantity;
                                        if ($totalVendas == '') $totalVendas = 0;

                                        $disponivel = ( $product->quantidadePessoasViagem - $totalVendas);

                                        if ($disponivel <= 0) continue;

                                        $encontrouPacotes = true;

                                        $locaisEmbarque .= $product->origem;

                                        ?>
                                        <h3 style="text-align: center;font-weight: 700;"><?php echo $product->name?></h3>
                                        <input type="radio" checked="checked" disponivel="<?php echo $disponivel;?>" style="display: none;" value="<?php echo $product->id;?>" name="viagem">

                                        <div class="col-sm-12" id="verDetalhesDoProduto<?php echo $contadorViagem;?>" >
                                            <?= $product->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$product->image.'" alt="'.$product->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                                            <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  str_replace('@quebra-linha@', '<br/><br/>', strip_tags( str_replace('</p>','@quebra-linha@',$product->details ))) . '</div></div>' : ''; ?>
                                            <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>', strip_tags( str_replace('</p>','@quebra-linha@',$product->product_details ))). '</div></div>' : ''; ?>
                                            <?= $product->itinerario ? '<div class="panel panel-info"><div class="panel-heading">' . lang('Roteiro') . '</div><div class="panel-body">' .str_replace('@quebra-linha@', '<br/><br/>',strip_tags( str_replace('</p>','@quebra-linha@',$product->itinerario ))). '</div></div>' : ''; ?>
                                            <?= $product->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading">' . lang('O que inclui') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>',strip_tags( str_replace('</p>','@quebra-linha@',$product->oqueInclui ))). '</div></div>' : ''; ?>
                                            <?= $product->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading">' . lang('Valores e condições') . '</div><div class="panel-body">' . str_replace('@quebra-linha@', '<br/><br/>',strip_tags( str_replace('</p>','@quebra-linha@',$product->valores_condicoes ))). '</div></div>' : ''; ?>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="panel panel-info" style="margin-top: 20px;">
                                                <div class="panel-heading"> <div class="fa fa-car"></div>  Selecione o seu local de embarque</div>
                                                <div class="panel-body" id="div_local_embarque">
                                                    <?php
                                                    $locais = $locaisEmbarque;
                                                    $locais = strip_tags($locais);
                                                    $locais = explode(';',$locais);

                                                    foreach ($locais as $local){
                                                        if ($local != ''){?>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label"></label>
                                                                <div class="col-sm-7" style="text-align: left;">
                                                                    <input type="radio" style="margin-left: 4px;" value="<?php echo $local;?>" name="local_embarque" class="grey">
                                                                    <?php echo $local?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                        </div>

                                    <?php } ?>

                                <?php } ?>

                                <?php if (!$encontrouPacotes) {?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7">
                                            <?php
                                            $arquivo = fopen('files/dados_empresa.txt', 'r');
                                            $dadosEmpresa = '';
                                            while (!feof($arquivo)) :
                                                $linha          = fgets($arquivo, 1024);
                                                $dadosEmpresa    .= $linha;
                                            endwhile;
                                            fclose($arquivo);
                                            ?>
                                            Nenhuma viagem disponível. Entre em contato:<br/>
                                            <?php echo $dadosEmpresa;?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Vendedor</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Vendedor <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <span class="input-group-addon"><div class="fa fa-user"></div></span>
                                            <select class="form-control" id="vendedor" name="vendedor">
                                                <?php foreach ($vendedores as $vendedor) {
                                                    if ($vendedor->id == $vendedorId) { ?>
                                                        <option value="<?php echo $vendedor->id;?>"><?php echo $vendedor->name;?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Informe seus dados</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        CPF <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                                            <span id="spanCpf"></span>
                                            <i class="fa fa-hand-o-right"></i>
                                         </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome completo (idêntico ao RG e CPF, sem abreviações)<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Sexo <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <i class="fa fa-hand-o-right"></i>
                                            <select class="form-control" id="sexo" style="padding: 0 20px;" name="sexo">
                                                <option value="MASCULINO">Masculino</option>
                                                <option value="FEMININO">Feminino</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        RG <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="rg" name="rg" placeholder="RG">
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone celular com DDD <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" id="celular" name="celular" placeholder="Telefone celular">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone residencial com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="Telefone residencial">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome e Telefone de contato com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="telefone_emergencia" name="telefone_emergencia" placeholder="">
                                            <i class="fa fa-comment"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Alguma alergia ou doença grave
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="alergia_medicamento" name="alergia_medicamento" placeholder="Alergia ou doença grave">
                                            <i class="fa fa-ambulance"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group" id="alertaCadastroEncontado" style="display: none;font-size: 12px;">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <div class="alert alert-success">
                                            <button data-dismiss="alert" class="close">&times;</button>
                                            <strong>Já encontramos seu cadastro!</strong></br>Verifique e atualize seu telefone e e-mail de contato.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        E-mail <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                                            <i class="fa fa-send"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Data de nascimento<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" id="dataNascimento" readonly name="dataNascimento"
                                                   data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $arquivo = fopen('files/hospedagem.txt', 'r');
                        $hospedagens = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $hospedagens    .= $linha;
                        endwhile;
                        fclose($arquivo);

                        $hospedagens = explode(';',$hospedagens);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"> <h4><div class="fa fa-bed"></div> Hospedagem /  Day Use</h4></div>
                            <div class="panel-body">
                                <?php foreach ($hospedagens as $hospeagem){
                                    if ($hospeagem != ''){?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7" style="text-align: left;">
                                                <input type="radio" style="margin-left: 4px;" value="<?php echo $hospeagem;?>" name="tipos_hospedagem" class="grey">
                                                <?php echo $hospeagem?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;display: none;">
                            <div class="panel-heading">Assento do ônibus (no caso de ônibus lotado, faremos a marcação do assento, favor escolha uma das opções que faremos o possível para deixá-lo onde prefere e sempre com as pessoas que vão junto com você. O número do seu assento irá no voucher, que será enviado até 2 dias antes da viagem) *</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7" style="text-align: left;">
                                        <input type="radio" style="margin-left: 4px;" checked="checked" value="Do meio para frente" name="posicao_assento" class="grey">Do meio para frente
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7" style="text-align: left;">
                                        <input type="radio" style="margin-left: 4px;" value="Do meio para o fundo" name="posicao_assento" class="grey">Do meio para o fundo
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($encontrouPacotes) { ?>
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <button class="btn btn-blue next-step btn-block" style="text-align: center;">
                                        Próximo passo <i class="fa fa-arrow-circle-right"></i><br/>
                                    </button>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div id="step-2">
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"><h3>Mais alguém vai viajar com voce? Clique em SIM e adicione.</h3></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <input type="radio" checked="checked" style="margin-left: 4px;" value="" name="maispassageiros" class="grey radio-callback-ifUnchecked">NÃO
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <input type="radio" style="margin-left: 4px;" value="" name="maispassageiros" class="grey radio-callback-ifChecked">SIM
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;display: none;" id="divAdicionarMaisPassageiros">
                            <div class="panel-heading">Passageiro adicional</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome completo (idêntico ao RG e CPF, sem abreviações)
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" required="required"  class="form-control" id="nomeDependente" name="nomeDependente[]" placeholder="Nome">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Sexo
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <select class="form-control" name="sexoDependente[]" style="padding: 0 20px;">
                                                <option value="MASCULINO">Masculino</option>
                                                <option value="FEMININO">Feminino</option>
                                            </select>
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Criança de colo <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <select class="form-control" id="crianca_colo" name="crianca_colo[]" style="padding: 0 20px;">
                                                <option value="NAO">NÃO</option>
                                                <option value="SIM">SIM</option>
                                            </select>
                                            <small style="color: red;">Crianças até cinco anos (5 anos) podem viajar no colo</small>
                                           <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CPF</label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control cpfDependente" contador="1" name="cpfDependente[]" placeholder="CPF">
                                            <span id="spanCpf"></span>
                                            <small style="color: red;">Se criança e não possuir CPF, deixe em branco.</small>
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">RG</label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control"  name="rgDependente[]" placeholder="RG">
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Data de nascimento</label>
                                    <div class="col-sm-7">
                                         <span class="input-icon">
                                            <input type="text" required="required" readonly data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker" name="dataNascimentoDependente[]" placeholder="Data de nascimento">
                                             <i class="fa fa-calendar"></i>
                                         </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone celular com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" id="celularDependente" name="celularDependente" placeholder="Telefone celular">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome de contato e telefone de emergência com DDD
                                    </label>
                                    <div class="col-sm-7">
                                       <span class="input-icon">
                                           <input type="text" class="form-control" id="telefoneEmergenciaDependente" name="telefoneEmergenciaDependente[]" placeholder="Telefone de Emergência">
                                            <i class="fa fa-phone"></i>
                                       </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Alguma alergia ou doença grave
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="alergiaMedicamentoDependente" name="alergiaMedicamentoDependente[]" placeholder="Alergia ou doença grave">
                                            <i class="fa fa-ambulance"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inserirDependente"></div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <button class="btn btn-azure btn-block adicionar-dependente" type="button" style="display: none;">
                                    <i class="fa fa-plus"></i> ADICIONE MAIS UM PASSAGEIRO
                                </button>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-blue next-step btn-block" style="text-align: center;">
                                    Próximo passo <i class="fa fa-arrow-circle-right"></i><br/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="step-3">

                        <?php
                        $arquivo = fopen('files/formas_pagamento.txt', 'r');
                        $formas_pagamento = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $formas_pagamento    .= $linha;
                        endwhile;
                        fclose($arquivo);

                        $pagamentos = explode('@label@',$formas_pagamento);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">
                                FORMA DE PAGAMENTO, DEPOSITO OU CARTÃO DE CREDITO PARA RESERVAS
                                SINAL DE 10%, RESTANTE PODE SER PARCELADODIRETO COM AGENCIA OU
                                CARTÃO DE CREDITO PELO PAGSEGURO.*com acréscimo de juros 2,99% a.m
                            </div>
                            <div class="panel-body">
                                <?php
                                $contador = 0;
                                foreach ($pagamentos as $pagamento){
                                    if ($pagamento != ''){
                                        $pag = explode('@quebra@',$pagamento); ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7" style="text-align: left;">
                                                <input type="radio" style="margin-left: 4px;" value="<?php echo $pag[0];?>" name="formas_pagamento" class="grey">
                                                <?php echo $pag[0];?>
                                                <a onclick="abrirInformacoesPagamento(<?php echo $contador;?>);" style="cursor: pointer;"><i class="fa fa-eye"></i></a>
                                                <div class="form-group" id="verDadosFormaPagamento<?php echo $contador;?>" style="font-size: 10px;margin-top: 10px;display: none;">
                                                    <div class="col-sm-12">
                                                        <div class="alert alert-success">
                                                            <?php echo $pag[1];?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php $contador = $contador +1;} ?>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Informações adicionais</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Se você estiver viajando com outra pessoa que preencheu contrato separado informe aqui o nome completo dela para que possamos deixá-los no mesmo ônibus, caso alguém que vai com você ainda não preencheu a ficha, diga para ela escrever o seu nome aqui.
                                    </label>
                                    <div class="col-sm-7">
                                        <textarea id="observacoes" class="form-control"  rows="10" cols="10" placeholder="Informações adicionais."  name="observacoes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $arquivo = fopen('files/aceite_contrato.txt', 'r');
                        $aceistes_contrato = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $aceistes_contrato    .= $linha;
                        endwhile;
                        fclose($arquivo);
                        $aceistes_contrato_label = explode('@label@',$aceistes_contrato); ?>

                        <?php
                        $contadorAceite = 0;
                        foreach ($aceistes_contrato_label as $labelAceite){
                            if ($labelAceite != '') {
                                $labelAceite = explode('@quebra@', $labelAceite); ?>
                                <div class="panel panel-info" style="margin-top: 20px;">
                                    <div class="panel-heading"><?php echo $labelAceite[0];?></div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12" style="text-align: left;">
                                                <?php echo $labelAceite[1];?>
                                                <br/><br/>
                                                <input type="checkbox" class="grey" value="" required="required" name="temos_de_aceite<?php echo $contadorAceite;?>[]" id="">ACEITO / CONCORDO
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $contadorAceite = $contadorAceite + 1;} ?>
                        <?php
                        $arquivo = fopen('files/dados_empresa.txt', 'r');
                        $dadosEmpresa = '';
                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $dadosEmpresa    .= $linha;
                        endwhile;
                        fclose($arquivo);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Dados da Empresa</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12" style="text-align: left;">
                                        <?php echo $dadosEmpresa?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-6">
                                <button type="submit" class="btn btn-blue next-step btn-block finish-step" style="text-align: left;">
                                    Aceitar e concluir minha reserva
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- end: FORM WIZARD PANEL -->
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/rapido/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url() ?>/assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?= $assets ?>js/valida_cpf_cnpj.js"></script>

<script src="<?php echo base_url() ?>assets/rapido/js/subview.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/subview-examples.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/form-wizard-cadastro-pessoa.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url() ?>assets/rapido/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->
<script>

    var CPF_PASSAGEIRO_PRINCIPAL = '';
    var listCPFS = [];

    jQuery(document).ready(function() {

        Main.init();
        FormWizard.init();

        $('.date-picker').datepicker({
            autoclose: true,
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
        });

        //$("#cpf").mask("999.999.999-99");
        $(".cpfDependente").mask("999.999.999-99");
        //$(".date-picker").mask("99/99/9999");

        $(".adicionar-dependente").unbind("click").click(function (e) {
            e.preventDefault();

            ondAdicionarDependente();
        });

        $('#cep').blur(function (event) {
            prreencherEndereco($(this).val());
        });

        $('.cpfDependente').blur(function (event){
            validarCPFDependente($(this));
        });

        $('#cpf').blur(function (event) {

            if ($(this).val() === '') return false;

            $('#nome').attr('readonly', false);

            if (!valida_cpf($(this).val())) {
                alert('CPF inválido!');
                CPF_PASSAGEIRO_PRINCIPAL = '';

                $(this).val('');
                $(this).focus();
                return false;
            }

            $(this).val(formataCPF($(this).val()));
            CPF_PASSAGEIRO_PRINCIPAL = $(this).val();

            verificaCadastroPassageiroPrincipal($(this).val());
        });

        $('input.radio-callback-ifChecked').on('ifChecked', function(event) {

            $('#divAdicionarMaisPassageiros').show();
            $('.adicionar-dependente').show();
            $('#inserirDependente').show();
            $('#inserirDependente').html('');

            $($("input[name='viagem']")).each(function( index , tag) {
                 if (tag.checked) {

                     let disponivel = tag.getAttribute('disponivel');

                    if ( (disponivel - 1) <= 0) {
                        $('input.radio-callback-ifChecked').iCheck('uncheck');
                        $('input.radio-callback-ifUnchecked').iCheck('check');

                        alert('Já atingimos o número de vagas disponível.' +
                            ' Se o dependente for criança de colo informe o nome da criança nas observações da 3º Etapa e informe' +
                            ' que é criança de colo com nome e idade da criança. Para maiores informações entre em contato com o vendedor para ver a possíbilidade de vagas adicionais.');
                    }

                }
            });

        });

        $('input.radio-callback-ifUnchecked').on('ifChecked', function(event) {
            $('#divAdicionarMaisPassageiros').hide();
            $('.adicionar-dependente').hide();
            $('#inserirDependente').hide();
        });

        $('input.radio-callback-local-embarque').on('ifChecked', function(event) {
            var produto = event.target.value;

            $(".checkLocalEmbarque").iCheck('uncheck');
            $(".divLocalEmbarque").hide();
            $('#localEmbarque'+produto).show();
            $('#inserirDependente').html('');
            $('input.radio-callback-ifUnchecked').iCheck('check');
            $('input[name="nomeDependente[]"]').val('');

            //buscarLocalDeEmbarqueAoSelecionarViagem(produto);
        });
    });

    function formataCPF(cpf){
        //retira os caracteres indesejados...
        cpf = cpf.replace(/[^\d]/g, "");

        //realizar a formatação...
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    var CONTADOR = 2;

    function ondAdicionarDependente() {

        let totalDeVendas = 0;
        let isDisponibilidade = true;

        $("input[name='nomeDependente[]']").each(function(){
            totalDeVendas = totalDeVendas + 1;
        });

        $($("input[name='viagem']")).each(function( index , tag) {
            if (tag.checked) {
                let disponivel = parseInt(tag.getAttribute('disponivel')) - totalDeVendas;
                if ( (disponivel - 1) <= 0) isDisponibilidade = false;
            }
        });

        if (!isDisponibilidade) {
            alert('Já atingimos o número de vagas disponível.' +
                ' Se o dependente for criança de colo informe o nome da criança nas observações da 3º Etapa e informe' +
                ' que é criança de colo com nome e idade da criança. Para maiores informações entre em contato com o vendedor para ver a possíbilidade de vagas adicionais.');
            return false;
        }

        var html = '<div class="panel panel-info" style="margin-top: 20px;" id="divAdicionarMaisPassageiros">\n' +
            '                            <div class="panel-heading">Passageiro adicional</div>\n' +
            '                            <div class="panel-body">\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Nome completo (idêntico ao RG e CPF, sem abreviações)\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <input type="text" required="required"  class="form-control" id="nomeDependente" name="nomeDependente[]" placeholder="Nome">\n' +
            '                                            <i class="fa fa-user"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Sexo\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <select class="form-control" name="sexoDependente[]" style="padding: 0 20px;">\n' +
            '                                                <option value="MASCULINO">Masculino</option>\n' +
            '                                                <option value="FEMININO">Feminino</option>\n' +
            '                                            </select>\n' +
            '                                            <i class="fa fa-hand-o-right"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Criança de colo <span class="symbol required"></span>\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <select class="form-control" id="crianca_colo" name="crianca_colo[]" style="padding: 0 20px;">\n' +
            '                                                <option value="NAO">NÃO</option>\n' +
            '                                                <option value="SIM">SIM</option>\n' +
            '                                            </select>\n' +
            '                                            <small style="color: red;">Crianças até cinco anos (5 anos) podem viajar no colo</small>\n' +
            '                                           <i class="fa fa-hand-o-right"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">CPF</label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <input type="text" class="form-control cpfDependente" contador="'+CONTADOR+'" name="cpfDependente[]" placeholder="CPF">\n' +
            '                                            <span id="spanCpf"></span>\n' +
            '                                            <small style="color: red;">Se criança e não possuir CPF, deixe em branco.</small>\n' +
            '                                            <i class="fa fa-hand-o-right"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">RG</label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <input type="text" class="form-control"  name="rgDependente[]" placeholder="RG">\n' +
            '                                            <i class="fa fa-hand-o-right"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">Data de nascimento</label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                         <span class="input-icon">\n' +
            '                                            <input type="text" required="required" readonly data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker" name="dataNascimentoDependente[]" placeholder="Data de nascimento">\n' +
            '                                             <i class="fa fa-calendar"></i>\n' +
            '                                         </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Telefone celular com DDD\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <input type="tel" class="form-control" id="celularDependente" name="celularDependente" placeholder="Telefone celular">\n' +
            '                                            <i class="fa fa-phone"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Nome de contato e telefone de emergência com DDD\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                       <span class="input-icon">\n' +
            '                                           <input type="text" class="form-control" id="telefoneEmergenciaDependente" name="telefoneEmergenciaDependente[]" placeholder="Telefone de Emergência">\n' +
            '                                            <i class="fa fa-phone"></i>\n' +
            '                                       </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="col-sm-3 control-label">\n' +
            '                                        Alguma alergia ou doença grave\n' +
            '                                    </label>\n' +
            '                                    <div class="col-sm-7">\n' +
            '                                        <span class="input-icon">\n' +
            '                                            <input type="text" class="form-control" id="alergiaMedicamentoDependente" name="alergiaMedicamentoDependente[]" placeholder="Alergia ou doença grave">\n' +
            '                                            <i class="fa fa-ambulance"></i>\n' +
            '                                        </span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>';


        $('#inserirDependente').append(html);
        $(".cpfDependente").mask("999.999.999-99");
        //$(".date-picker").mask("99/99/9999");

        $('.cpfDependente').blur(function (event){
            validarCPFDependente($(this));
        });

        $('.date-picker').datepicker({
            autoclose: true,
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
        });

        CONTADOR++;
    }

    function prreencherEndereco(cep) {
        if (cep === '') return;

        cep = cep.replace('-', '');
        cep = cep.replace('.', '');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url,
            function (data) {
                if(data !== null){
                    $("#endereco").val(data.logradouro);
                    $("#bairro").val( data.bairro);
                    $("#cidade").val( data.localidade);
                    $("#estado").val( data.uf);
                    $('#numero').focus();
                }
            });
    }

    function buscarLocalDeEmbarqueAoSelecionarViagem(id) {

        $.ajax({
            type: "GET",
            url: '<?php echo base_url();?>/linkcompra/buscarLocalEmbarque/'+id,
            data: {
                cpf: $('#cpf').val()
            },
            dataType: 'json',
            success: function (embarque) {

                var embarques = embarque.split(";");
                var html = '';

                for(var i=0;embarques.length>i;i++) {
                    var origem = embarques[i];

                    if (origem !== '') {
                        html += '<div class="form-group">\n' +
                            '           <label class="col-sm-3 control-label"></label>\n' +
                            '           <div class="col-sm-7" style="text-align: left;">\n' +
                            '               <input type="radio" style="margin-left: 4px;" value="' + origem + '" name="local_embarque" class="grey">\n' +
                            '               ' + origem +
                            '           </div>\n' +
                            '      </div>';
                    }
                }
                $('#div_local_embarque').html(html);
                Main.init();
                FormWizard.init();

                $('input.radio-callback-local-embarque').on('ifChecked', function(event) {
                    var produto = event.target.value;
                    //buscarLocalDeEmbarqueAoSelecionarViagem(produto);
                });
            }
        });
    }

    function verificaCadastroPassageiroPrincipal(cpf) {
        if (cpf  !== '') {
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>/linkcompra/getVerificaCustomeByCPF/'+cpf,
                data: {
                    cpf: $('#cpf').val()
                },
                dataType: 'json',
                success: function (pessoa) {
                    if (pessoa != null) {
                        $('#nome').val(pessoa.name);
                        $('#sexo').val(pessoa.sexo);
                        $('#cpf').val(pessoa.vat_no);
                        $('#rg').val(pessoa.cf1);

                        $('#telefone').val(pessoa.phone);
                        $('#celular').val(pessoa.cf5);
                        $('#telefone_emergencia').val(pessoa.telefone_emergencia);
                        $('#alergia_medicamento').val(pessoa.alergia_medicamento);

                        $('#email').val(pessoa.email);
                        if (pessoa.data_aniversario !== '') $('#dataNascimento').val(pessoa.data_aniversario.split('-').reverse().join('/'));
                        $('#nome').attr('readonly', true);


                        $('#celular').focus();
                    } else {

                        $('#alertaCadastroEncontado').hide();
                        $('#nome').attr('readonly', false);

                        $('#nome').focus();
                    }
                }
            });
        }
    }

    function abrirInformacoesPagamento(contador) {
        var isVisible = $( "#verDadosFormaPagamento"+contador ).is( ":visible" );
        if (isVisible) $('#verDadosFormaPagamento'+contador).hide();
        else $('#verDadosFormaPagamento'+contador).show();
    }

    function  verDadosDaViagem(contador) {
        var isVisible = $( "#verDetalhesDoProduto"+contador ).is( ":visible" );
        if (isVisible) $('#verDetalhesDoProduto'+contador).hide();
        else $('#verDetalhesDoProduto'+contador).show();
    }

    function validarCPFDependente(tag) {

        let contador = tag.attr('contador');
        let cpfDependente = tag.val()+'_'+contador;

        listCPFS.shift(cpfDependente);

        let isCPFJAUtilizado = false;

        $(listCPFS).each(function(index, cpf) {
            if (cpfDependente === cpf) isCPFJAUtilizado = true;
        });

        if (tag.val() === CPF_PASSAGEIRO_PRINCIPAL) isCPFJAUtilizado = true;

        if (isCPFJAUtilizado) {
            alert('Você já utilizou este CPF para outra pessoa.');

            tag.val('');
            tag.focus();
            return false;
        }

        if (tag.val() !== '' && !valida_cpf(tag.val())) {
            alert('CPF inválido!');

            tag.val('');
            tag.focus();
            return false;
        }

        if (cpfDependente !== '')  listCPFS.push(cpfDependente);
    }

    function fechar() {}
</script>
</body>
<!-- end: BODY -->
</html>