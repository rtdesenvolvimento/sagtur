<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BusRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllAssentosBloqueadosCard() {

        $this->db->where('active', 1);
        $this->db->where('cart_lock', 1);

        $q = $this->db->get('assento_bloqueio');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }
        return FALSE;
    }

    public function getMapaByAutomovel($automovel_id, $andar = 1) {

        $this->db->where('automovel_id', $automovel_id);
        $this->db->where('andar', $andar);

        $q = $this->db->get('mapa_automovel');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }
        return FALSE;
    }

    public function getAssentosBloqueados($productID, $programacaoID, $tipoTransporteID) {

        $this->db->where('product_id', $productID);
        $this->db->where('programacao_id', $programacaoID);
        $this->db->where('tipo_transporte_id', $tipoTransporteID);
        $this->db->where('active', 1);

        $q = $this->db->get('assento_bloqueio');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }
        return FALSE;
    }

    public function isAssentoBloqueado($productID, $programacaoID, $tipoTransporteID, $poltrona) {

        if (!$poltrona) return false;

        $this->db->where('product_id', $productID);
        $this->db->where('programacao_id', $programacaoID);
        $this->db->where('tipo_transporte_id', $tipoTransporteID);
        $this->db->where('str_assento', $poltrona);
        $this->db->where('active', 1);

        $q = $this->db->get('assento_bloqueio');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function isAssentoMarcado($productID, $programacaoID, $tipoTransporteID, $poltrona, $itemID = null) {

        if (!$poltrona) return false;

        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where('sale_items.product_id', $productID);
        $this->db->where('sale_items.programacaoId', $programacaoID);
        $this->db->where('sale_items.tipoTransporte', $tipoTransporteID);
        $this->db->where('sale_items.poltronaClient', $poltrona);
        $this->db->where("sales.sale_status in ('faturada', 'orcamento') ");

        if ($itemID && $itemID != 'undefined') {
            $this->db->where('sale_items.id !=' . $itemID);
        }

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function isAssentoBloqueadoOuMarcado($productID, $programacaoID, $tipoTransporteID, $poltrona, $itemID = null) {

        $bloqueadao = $this->isAssentoBloqueado($productID, $programacaoID, $tipoTransporteID, $poltrona);
        $marcado    = $this->isAssentoMarcado($productID, $programacaoID, $tipoTransporteID, $poltrona, $itemID);

        return ($marcado || $bloqueadao);
    }

    public function desbloquear_todos_assentos_session_card($sessionsID) {
        $data = array(
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('user_id'),
            'active' => 0,
        );
        $this->db->update('assento_bloqueio', $data, array('sessions_id' => $sessionsID));

    }

    public function getAllCobrancaExtraAssentoByID($ID) {

        $this->db->where('sma_configuracao_cobranca_extra_assento.id', $ID);

        $this->db->join('sma_configuracao_cobranca_extra_assento', 'sma_configuracao_cobranca_extra_assento.id=sma_configuracao_cobranca_extra_assento_marcacao.configuracao_extra_id', 'left');
        $this->db->join('cor', 'cor.id=configuracao_cobranca_extra_assento_marcacao.cor_id', 'left');

        $q = $this->db->get('sma_configuracao_cobranca_extra_assento_marcacao');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }

        return false;
    }

    public function getlRotuloCobrancaExtra($configuracao_id) {

        $this->db->select("valor, cor_id, cor.cor, cor.name as name_cor");
        $this->db->join('cor', 'cor.id=configuracao_cobranca_extra_assento_marcacao.cor_id', 'left');

        $this->db->where('configuracao_extra_id', $configuracao_id);
        $this->db->where('valor>0');

        $this->db->group_by('cor_id, valor');
        $this->db->order_by('valor');

        $q = $this->db->get('sma_configuracao_cobranca_extra_assento_marcacao');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }

        return false;
    }
}