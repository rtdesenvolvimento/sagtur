<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_itinerary'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'form');
        echo form_open("itinerary/edit/".$itinerary->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("date_itinerary", "dataEmbarque"); ?>
                        <?php echo form_input('dataEmbarque', $itinerary->dataEmbarque, 'class="form-control tip" id="dataEmbarque" required="required" ', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("hora_itinerary", "horaEmbarque"); ?>
                        <?php echo form_input('horaEmbarque', $itinerary->horaEmbarque, 'class="form-control tip" id="horaEmbarque" required="required" ', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("provider_service", "fornecedor"); ?>
                        <?php echo form_input('fornecedor', $itinerary->fornecedor, 'id="fornecedor" data-placeholder="' . lang("select") . ' ' . lang("fornecedor") . '" class="form-control input-tip" required="required"  style="width:100%;"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("motorista", "motorista"); ?>
                        <?php echo form_input('motorista', $itinerary->motorista, 'id="motorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("guia", "guia"); ?>
                        <?php echo form_input('guia',  $itinerary->guia, 'id="guia" data-placeholder="' . lang("select") . ' ' . lang("guia") . '"  class="form-control input-tip" style="width:100%;"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("tipo_transporte", "tipo_transporte"); ?>
                        <?php echo form_input('tipo_transporte_id',  $itinerary->tipo_transporte_id, 'id="tipo_transporte_id" data-placeholder="' . lang("select") . ' ' . lang("tipo_transporte") . '" class="form-control input-tip" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?= lang("tipo_trajeto", "tipo_trajeto"); ?>
                        <?php
                        $cbTipoTrajeto[""] = lang("select") . ' ' . lang("tipo_trajeto") ;
                        foreach ($tiposTrajeto as $tipoTrajeto) {
                            $cbTipoTrajeto[$tipoTrajeto->id] = $tipoTrajeto->name;
                        }
                        echo form_dropdown('tipo_trajeto_id', $cbTipoTrajeto, $itinerary->tipo_trajeto_id, 'id="tipo_trajeto_id" data-placeholder="' . lang("select") . ' ' . lang("tipo_trajeto") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("retorno_previsto", "retornoPrevisto"); ?>
                        <?php echo form_input('retornoPrevisto', $itinerary->retornoPrevisto, 'class="form-control tip" id="retornoPrevisto"', 'datetime-local'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <div class="form-group">
                        <?= lang("product", "product") ?>
                        <?php
                        $cbProducts[''] =  lang("select") . " " . lang("product") ;
                        foreach ($products as $produts) {
                            $cbProducts[$produts->id] = $produts->name;
                        }
                        echo form_dropdown('product_id', $cbProducts, $itinerary->product_id, 'class="form-control select" id="product_id" placeholder="' . lang("select") . " " . lang("product") . '"style="width:100%"')
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <?= lang("executivo", "executivo"); ?>
                        <?php
                        $opts = array(1 => lang('yes'), 0 => lang('no'));
                        echo form_dropdown('executivo', $opts, $itinerary->executivo, 'id="executivo" class="form-control select" style="width:100%;"');
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group all">
                        <?= lang("note", "note") ?>
                        <?= form_textarea('note', $itinerary->note, 'class="form-control" id="note" '); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_itinerary', lang('edit_itinerary'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('#motorista').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#guia').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#fornecedor').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#tipo_transporte_id').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "veiculo/getTipoTransporte/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "veiculo/suggestions_tipo_tranporte",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

    });
</script>