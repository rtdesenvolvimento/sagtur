<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update362 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.6.2'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'is_biller' => array('type' => 'INT', 'constraint' => 11 , 'default' => 1),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    public function down() {}
}
