<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileSitemapsController extends CI_Controller {

    public function index($client_name) {

        $roots_to_file = 'assets/uploads';

        $file_path = $roots_to_file."/sitemap_$client_name.xml";

        if (file_exists($file_path)) {
            $xml = file_get_contents($file_path);
            header('Content-Type: application/xml');
            echo $xml;
        } else {
            echo 'Sitemap file not found';
        }
    }
}