<?php defined('BASEPATH') OR exit('No direct script access allowed');

class GooglePersonRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($person): bool
    {
        if ($this->db->insert("google_persons", $person)) {
            return true;
        }
        return false;
    }

    public function getByID($id) {
        $q = $this->db->get_where('google_persons', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAll($imported = 0) {

        $q = $this->db->get_where('google_persons', array('imported' => $imported));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPersonsNotImported()
    {
        return $this->getAll(0);
    }

    function existPerson($phone)
    {
        if ($phone == null || $phone == '') {
            return false;
        }

        $q = $this->db->get_where('google_persons', array('phoneNumbers' => $phone), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function personImported($phone)
    {
        if ($phone == null || $phone == '') {
            return false;
        }

        $q = $this->db->get_where('google_persons', array('phoneNumbers' => $phone, 'imported' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}