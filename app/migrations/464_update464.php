<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update464 extends CI_Migration {

    public function up() {

        $this->drop_tables();

        $this->create_table_folders_group();
        $this->create_table_folders();

        $this->create_table_contracts();
        $this->create_table_contract_settings();
        $this->create_table_documents();
        $this->create_table_document_historys();
        $this->create_table_document_archives();
        $this->create_table_document_signatures();
        $this->create_table_document_customers();
        $this->create_table_plugsign();

        $this->insert_folder();
        $this->insert_folder_group();
        $this->insert_table_contract_settings();
        $this->insert_contracts();

        $this->alter_table_documents();
        $this->alter_table_settings();
        $this->alter_table_products();

        $this->alter_table_order_ref();
        $this->alter_table_settings_to_doc();

        $this->alter_table_warehouses();
        $this->alter_table_sales();

        $this->db->update('settings',  array('version' => '4.6.4'), array('setting_id' => 1));
    }

    public function drop_tables()
    {
        $this->db->query('DROP TABLE sma_folder_group');
        $this->db->query('DROP TABLE sma_folder');
        $this->db->query('DROP TABLE sma_document');
        $this->db->query('DROP TABLE sma_signatures');
        $this->db->query('DROP TABLE sma_document_history');
        $this->db->query('DROP TABLE sma_archive');

    }

    function create_table_document_customers() {//clientes

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100'),
            'last_name' => array('type' => 'VARCHAR', 'constraint' => '100'),
            'email' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'cpf' => array('type' => 'VARCHAR', 'constraint' => '50' , 'null' => TRUE ),
            'address' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'recive_whatsapp' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'recive_email' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'password' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'can_login' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'cert_file' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'cert_password' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),//data de criacao
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_customers', TRUE, $attributes);
    }

    public function alter_table_warehouses() {
        $fields = array(
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'first_name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'last_name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'cnpj' => array('type' => 'VARCHAR', 'constraint' => '50' , 'null' => TRUE ),
            'cpf' => array('type' => 'VARCHAR', 'constraint' => '50' , 'null' => TRUE ),
            'password' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'recive_whatsapp' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'recive_email' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'can_login' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('warehouses', $fields);
    }

    public function alter_table_order_ref() {
        $fields = array(
            'doc' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('order_ref', $fields);
    }

    public function alter_table_settings_to_doc() {
        $fields = array(
            'doc_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'DOC'),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_settings() {
        $fields = array(
            'contract_settings_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('contract_settings_id' => 1), array('setting_id' => 1));
    }

    public function alter_table_documents()
    {
        $fields = array(
            'monitorar_validade' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'validade_meses'   => array('type' => 'INT', 'constraint' => 11, 'default' => 60),

            'auto_destruir_solicitacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'validade_auto_destruir_dias'   => array('type' => 'INT', 'constraint' => 11, 'default' => 60),//2 meses

            'solicitar_selfie' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'solicitar_documento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'signature_type' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => 'text'),//all, text, draw, upload

            'is_observadores' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'observadores' => array('type' => 'VARCHAR', 'constraint' => '999' ),

            'data_vencimento_contrato' => array('type' => 'DATE', 'null' => TRUE ),

            'signatories_note' => array('type' => 'LONGTEXT', 'default' => ''),

            'enviar_sequencial' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),//1 - sequencial, 0 - paralelo
        );

        $this->dbforge->add_column('documents', $fields);
    }

    function create_table_contract_settings() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'contract_name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'monitorar_validade' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'validade_meses'   => array('type' => 'INT', 'constraint' => 11, 'default' => 12),
            'auto_destruir_solicitacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'validade_auto_destruir_dias'   => array('type' => 'INT', 'constraint' => 11, 'default' => 30),
            'solicitar_selfie' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'solicitar_documento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'signature_type' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => 'text'),//all, text, draw, upload
            'is_observadores' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'observadores' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'enviar_sequencial' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),//1 - sequencial, 0 - paralelo
            'enviar_contrato_signatarios_venda' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'gerar_contrato_faturamento_venda' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'contract_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'auto_assinar_biller_contrato' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'auto_assinar_customer_contrato' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'default_biller_contract' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),//responsavel padrao do contrato para assinatura
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('contract_settings', TRUE, $attributes);



    }

    public function getSettings() {
        $q = $this->db->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function create_table_folders_group() {//grupo de pasta pasta


        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),

            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),

        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('folders_group', TRUE, $attributes);
    }

    function create_table_folders() {//pasta

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'public_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),

            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'archived' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),

            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),

        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('folders', TRUE, $attributes);
    }

    function create_table_documents() {//documento

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),

            'document_status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE, 'default' => 'draft' ),

            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'folder_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),

            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),

            'sale_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),

            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'file_name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'url_link' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'url_validate' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'document_key' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE, 'default' => ''  ),
            'extension' => array('type' => 'VARCHAR', 'constraint' => '30' , 'null' => TRUE , 'default' => '' ),
            'size' => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'archived' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),

        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('documents', TRUE, $attributes);
    }

    function create_table_document_signatures() {//assinatura

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE, 'default' => 'unsigned' ),

            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),

            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'customer' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),

            'is_biller' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),

            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'action' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'url_link' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => TRUE ),

            'viewd' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_viewd' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data visto o link

            'signed' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_signature' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data da assinatura

            'rejected' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_rejected' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data da assinatura

            'canceled' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_canceled' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data do cancelamento

            'removed' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_removed' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data em que o signatorio foi removido

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data de criacao
            'created_by' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_signatures', TRUE, $attributes);
    }

    function create_table_document_historys() {//histico de documento

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_historys', TRUE, $attributes);
    }

    function create_table_document_archives() {//arquivos de documento

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 ),
            'folder_id' => array('type' => 'INT', 'constraint' => 11 ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'url_link' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_archives', TRUE, $attributes);
    }

    public function create_table_plugsign() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'token' => array('type' => 'VARCHAR', 'constraint' => '500' , 'null' => TRUE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('plugsign', TRUE, $attributes);

        $data_plugsign = array(
            'token' => '',
            'active' => 0,
            'note' => 'Integração com assinatura digital - plugsign',
        );

        $this->db->insert('plugsign', $data_plugsign);
    }

    public function insert_folder() {
        $data_folder = array(
            'folder_group_id' => 1,
            'hidden' => 0,
            'name' => 'RAIZ',
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
            'note' => 'Criado pelo sistema'
        );

        $this->db->insert('folders', $data_folder);
    }

    public function insert_folder_group() {

        $data_folder_group = array(
            'hidden' => 1,
            'name' => 'RAIZ',
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
            'note' => 'Criado pelo sistema'
        );

        $this->db->insert('folders_group', $data_folder_group);
    }

    public function insert_table_contract_settings() {

        $tipo_faixa_padrao_bebe = array(
            'contract_name' => 'CONFIGURAÇÕES DE CONTRATOS',
            'contract_id' => 1,
            'note' => 'Olá, você recebeu uma solicitação de assinatura de contrato.',
        );
        $this->db->insert('contract_settings', $tipo_faixa_padrao_bebe);

        $setgs = $this->getSettings();
        $this->db->update('contract_settings',  array('default_biller_contract' => $setgs->default_biller), array('id' => 1));
    }

    public function alter_table_sales() {
        $fields = array(
            'contract_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
            'file_name_contract' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_products() {
        $fields = array(
            'contract_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
        );
        $this->dbforge->add_column('products', $fields);
    }

    function create_table_contracts() {//contratos

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'type' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE, 'default' => 'text' ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),
            'contract' => array('type' => 'LONGTEXT', 'default' => ''),
            'file_name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('contracts', TRUE, $attributes);
    }

    public function insert_contracts() {
        $data_folder = array(
            'name' => 'CONTRATO PADRAO',
            'contract' => 'CONTRATO PADRAO',
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
            'created_by' => 1,
            'update_by' => 1,
        );

        $this->db->insert('contracts', $data_folder);
    }

    public function down() {}
}
