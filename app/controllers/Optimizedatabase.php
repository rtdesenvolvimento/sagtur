<?php defined('BASEPATH') or exit('No direct script access allowed');

class Optimizedatabase extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function index() {
        show_404();
    }

    public function optimizar() {

        $otherdb = $this->load->database('sagtur_administrativo', TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();

        foreach ($dbs as $db_name)
        {
            if ($this->isOptimize($db_name)) {

                echo $db_name . '<br/><br/><br/><br/>';

                $otherdbInner       = $this->load->database($db_name, TRUE);
                $dbutildb           = $this->load->dbutil($otherdbInner, true);
                $configuracaoGeral  = $this->getSettingsDB($otherdbInner);

                if ($configuracaoGeral->status != null) {
                    if ($configuracaoGeral->status == 1) {
                        $result             = $dbutildb->optimize_database();
                        if ($result !== FALSE) {
                            print_r($result);
                        }
                    }
                }
            }
        }
    }

    private function isOptimize($db) {
        return  ($db != 'information_schema' && $db != 'mysql' && $db != 'performance_schema' && $db != 'phpmyadmin' && $db != 'sys');
    }

    public function getSettingsDB($otherdbInner)
    {
        $q = $otherdbInner->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}?>