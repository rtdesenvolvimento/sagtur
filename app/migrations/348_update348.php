<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update348 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '3.30.09.22.1'), array('setting_id' => 1));
    }

    public function create_index() {
        try {

            //sma_agenda_viagem
            $this->db->query('CREATE INDEX indxs_agv_produto_dataSaida	ON sma_agenda_viagem (produto, dataSaida)');
            $this->db->query('CREATE INDEX indxs_agv_produto	ON sma_agenda_viagem (produto)');

            //sma_products
            $this->db->query('CREATE INDEX indxs_products_name	 ON sma_products (name)');
            $this->db->query('CREATE INDEX indxs_products_unit	 ON sma_products (unit)');
            $this->db->query('CREATE INDEX indxs_products_active ON sma_products (active)');


            //sma_combo_items
            $this->db->query('CREATE INDEX indxs_cbi_product_id	ON sma_combo_items (product_id)');
            $this->db->query('CREATE INDEX indxs_cbi_product_id_item_id	ON sma_combo_items (product_id,item_id)');


        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
