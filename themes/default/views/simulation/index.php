<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Resultatec">
    <meta name="robots" content="noindex">

    <title><?php echo $this->Settings->site_name;?> || SIMULAÇÃO VALEPAY</title>
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">


    <style>
        table {
            width: 80%;
            margin: 0 auto;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
            font-weight: bold;
            text-align: center;
        }
        td {
            text-align: center;
        }

        .buttom_class {
            margin-top: 15px;
            padding: 10px 10px 10px 10px;
            width: 100%;
            background: #e58801;
            font-size: 15px;
            border: 1px solid #ccc;
            cursor: pointer;
            color: #ffff;
        }

        .header {
            background: #e58801;
            padding: 15px;
            width: 100%;
            text-align: center;
            color: #ffffff;
            font-size: 20px;
        }
    </style>
</head>

<body>

<main id="general_page">
    <div class="header">
        SIMULAÇÃO
    </div>
    <div class="row" style="margin: 0 auto;margin-top: 20px;width: 80%;">
        <div class="form-group col-sm-12" style="padding: 0;">
            <label for="cardholderName">Valor</label>
            <input id="valor" value="0.00" placeholder="R$ 0.00" type="number" required="required" class="form-control mask_money">
            <button class="buttom_class" onclick="popular_table_parcelamento_valepay()">SIMULAR</button>
        </div>
    </div>
    <table id="tb-parcelamento-valepay">
        <caption>
            <div class="row">
                <div class="form-group col-sm-12">
                    <span style="font-size: 11px;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                         <span style="font-size: 11px;color: #28a745;font-weight: 500;">Você está fazendo uma simulação com <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" /></span>
                    </span>
                </div>
            </div>
        </caption>
        <thead>
        <tr>
            <th colspan="3">Tabela onde o cliente assume o juros do parcelamento</th>
        </tr>
        </thead>
        <thead>
        <tr>
            <th>Parcela</th>
            <th>Valor</th>
            <th>Taxa do Cliente</th>
        </tr>
        </thead>
         <tbody></tbody>
    </table>



    <table id="tb-parcelamento-agencia-valepay">
        <caption>
            <div class="row">
                <div class="form-group col-sm-12">
                    <span style="font-size: 11px;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                         <span style="font-size: 11px;color: #28a745;font-weight: 500;">Você está fazendo uma simulação com <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" /></span>
                    </span>
                </div>
            </div>
        </caption>
        <thead>
        <tr>
            <th colspan="3">Tabela onde agência assume o juros de parcelamento</th>
        </tr>
        </thead>
        <thead>
        <tr>
            <th>Parcela</th>
            <th>Repasse</th>
            <th>Taxa da Agência</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</main>

<!-- /cd-overlay-content -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>

<script>
    var site = <?=json_encode(array('base_url' => base_url(), 'settings' => $this->Settings, 'dateFormats' => $dateFormats))?>;

    var mask = {
        money: function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"0.0$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"0.$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'.$1');
                }
                return v;
            };

            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(document).ready(function() {
        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });
    });

    (function (p, z) {
        function q(a) {
            return !!("" === a || a && a.charCodeAt && a.substr)
        }

        function m(a) {
            return u ? u(a) : "[object Array]" === v.call(a)
        }

        function r(a) {
            return "[object Object]" === v.call(a)
        }

        function s(a, b) {
            var d, a = a || {}, b = b || {};
            for (d in b)b.hasOwnProperty(d) && null == a[d] && (a[d] = b[d]);
            return a
        }

        function j(a, b, d) {
            var c = [], e, h;
            if (!a)return c;
            if (w && a.map === w)return a.map(b, d);
            for (e = 0, h = a.length; e < h; e++)c[e] = b.call(d, a[e], e, a);
            return c
        }

        function n(a, b) {
            a = Math.round(Math.abs(a));
            return isNaN(a) ? b : a
        }

        function x(a) {
            var b = c.settings.currency.format;
            "function" === typeof a && (a = a());
            return q(a) && a.match("%v") ? {
                pos: a,
                neg: a.replace("-", "").replace("%v", "-%v"),
                zero: a
            } : !a || !a.pos || !a.pos.match("%v") ? !q(b) ? b : c.settings.currency.format = {
                pos: b,
                neg: b.replace("%v", "-%v"),
                zero: b
            } : a
        }

        var c = {
            version: "0.4.1",
            settings: {
                currency: {symbol: "$", format: "%s%v", decimal: ".", thousand: ",", precision: 2, grouping: 3},
                number: {precision: 0, grouping: 3, thousand: ",", decimal: "."}
            }
        }, w = Array.prototype.map, u = Array.isArray, v = Object.prototype.toString, o = c.unformat = c.parse = function (a, b) {
            if (m(a))return j(a, function (a) {
                return o(a, b)
            });
            a = a || 0;
            if ("number" === typeof a)return a;
            var b = b || ".", c = RegExp("[^0-9-" + b + "]", ["g"]), c = parseFloat(("" + a).replace(/\((.*)\)/, "-$1").replace(c, "").replace(b, "."));
            return !isNaN(c) ? c : 0
        }, y = c.toFixed = function (a, b) {
            var b = n(b, c.settings.number.precision), d = Math.pow(10, b);
            return (Math.round(c.unformat(a) * d) / d).toFixed(b)
        }, t = c.formatQuantity = c.format = function (a, b, d, i) {
            if (m(a))return j(a, function (a) {
                return t(a, b, d, i)
            });
            var a = o(a), e = s(r(b) ? b : {
                precision: b,
                thousand: d,
                decimal: i
            }, c.settings.number), h = n(e.precision), f = 0 > a ? "-" : "", g = parseInt(y(Math.abs(a || 0), h), 10) + "", l = 3 < g.length ? g.length % 3 : 0;
            return f + (l ? g.substr(0, l) + e.thousand : "") + g.substr(l).replace(/(\d{3})(?=\d)/g, "$1" + e.thousand) + (h ? e.decimal + y(Math.abs(a), h).split(".")[1] : "")
        }, A = c.formatMoney = function (a, b, d, i, e, h) {
            if (m(a))return j(a, function (a) {
                return A(a, b, d, i, e, h)
            });
            var a = o(a), f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format);
            return (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal))
        };
        c.formatColumn = function (a, b, d, i, e, h) {
            if (!a)return [];
            var f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format), l = g.pos.indexOf("%s") < g.pos.indexOf("%v") ? !0 : !1, k = 0, a = j(a, function (a) {
                if (m(a))return c.formatColumn(a, f);
                a = o(a);
                a = (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal));
                if (a.length > k)k = a.length;
                return a
            });
            return j(a, function (a) {
                return q(a) && a.length < k ? l ? a.replace(f.symbol, f.symbol + Array(k - a.length + 1).join(" ")) : Array(k - a.length + 1).join(" ") + a : a
            })
        };
        if ("undefined" !== typeof exports) {
            if ("undefined" !== typeof module && module.exports)exports = module.exports = c;
            exports.accounting = c
        } else"function" === typeof define && define.amd ? define([], function () {
            return c
        }) : (c.noConflict = function (a) {
            return function () {
                p.accounting = a;
                c.noConflict = z;
                return c
            }
        }(p.accounting), p.accounting = c)
    })(this);

    function formatMoney(x, symbol) {
        if(!symbol) { symbol = ""; }
        if(site.settings.sac == 1) {
            return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
                ''+formatSA(parseFloat(x).toFixed(site.settings.decimals)) +
                (site.settings.display_symbol == 2 ? site.settings.symbol : '');
        }
        var fmoney = accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
        return fmoney;
    }

    function popular_table_parcelamento_valepay() {

        popular_table_parcelamento_valepay_agencia_assume_juros();

        $.blockUI(
            {
                message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                css: {width: '70%', left: '15%'}
            }
        );

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>simulationcreditcard/simulation_installments",
            data: {
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                valor_total_taxas: $('#valor').val(),
            },
            dataType: 'json',
            success: function (parcelas) {

                let table = $('#tb-parcelamento-valepay tbody');
                let dataRow;

                table.empty();

                if (parcelas.status === 'success') {

                    for (let i = 0; i < parcelas.data.length; i++) {

                        const valorOriginal = parseFloat($('#valor').val());

                        dataRow = $('<tr>', {style: 'border-bottom: 1px solid #ccc;padding: 15px;font-size: 15px;font-weight: 400;'});

                        let numero_parcela  = parcelas.data[i].parcela
                        let valor_parcela   = parcelas.data[i].valor_parcela;
                        let totalPagar      = parseFloat(parcelas.data[i].total);

                        let dataCellParcela = $('<td>').text(numero_parcela + 'X');
                        let dataCellValorParcela = $('<td>').text('R$'+formatMoney(valor_parcela) + ' (' + 'R$'+formatMoney(totalPagar) + ')');

                        let indiceConversao = (totalPagar/valorOriginal)
                        let taxa = 1 - (indiceConversao - 1);

                        let tax = ((totalPagar/valorOriginal) - 1) * 100;

                        let dataCellTax = $('<td>').text(formatMoney(tax)+'%');

                        dataRow.append(dataCellParcela);
                        dataRow.append(dataCellValorParcela);
                        dataRow.append(dataCellTax);

                        table.append(dataRow);
                    }

                    $.unblockUI();
                }

            }
        });
    }

    function popular_table_parcelamento_valepay_agencia_assume_juros() {

        $.blockUI(
            {
                message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                css: {width: '70%', left: '15%'}
            }
        );

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>simulationcreditcard/simulation_agencia_installments",
            data: {
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                valor_total_taxas: $('#valor').val(),
            },
            dataType: 'json',
            success: function (parcelas) {

                let table = $('#tb-parcelamento-agencia-valepay tbody');
                let dataRow;

                table.empty();

                if (parcelas.status === 'success') {

                    for (let i = 0; i < parcelas.data.length; i++) {

                        dataRow = $('<tr>', {style: 'border-bottom: 1px solid #ccc;padding: 15px;font-size: 15px;font-weight: 400;'});

                        let numero_parcela  = parcelas.data[i].parcela
                        let repasse      = parseFloat(parcelas.data[i].repasse);
                        let total      = parseFloat(parcelas.data[i].total);

                        let dataCellParcela = $('<td>').text(numero_parcela + 'X');
                        let dataCellValorParcela = $('<td>').text('R$'+formatMoney(repasse));
                        let tax = (((repasse/total) - 1) * 100)*-1;

                        let dataCellTax = $('<td>').text(formatMoney(tax)+'%');

                        dataRow.append(dataCellParcela);
                        dataRow.append(dataCellValorParcela);
                        dataRow.append(dataCellTax);

                        table.append(dataRow);
                    }

                    $.unblockUI();
                }

            }
        });
    }

</script>
</body>
</html>