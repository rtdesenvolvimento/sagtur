<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update374 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.7.4'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'send_email_canceling_sale' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }


    public function down() {}
}
