<?php defined('BASEPATH') or exit('No direct script access allowed');

class Location extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        //services
        $this->load->model('service/LocationService_model', 'LocationService_model');

        //consutlas
        $this->load->model('repository/AvailabilityRepository_model', 'AvailabilityRepository_model');
        $this->load->model('repository/RegraCobrancaLocacaoRepository_model', 'RegraCobrancaLocacaoRepository_model');
        $this->load->model('repository/TipoVeiculoRepository_model', 'TipoVeiculoRepository_model');
        $this->load->model('repository/StatusDisponibilidadeRepository_model', 'StatusDisponibilidadeRepository_model');
        $this->load->model('repository/TanqueVeiculoRepository_model', 'TanqueVeiculoRepository_model');
        $this->load->model('repository/CategoriaVeiculoRepository_model', 'CategoriaVeiculoRepository_model');
        $this->load->model('repository/VehicleRepository_model', 'VehicleRepository_model');

        //model
        $this->load->model('Tipocontrato_model', 'Tipocontrato_model');
        $this->load->model('Financeiro_model', 'Financeiro_model');

        $this->lang->load('location', $this->Settings->user_language);

        $this->load->library('form_validation');
    }
    public function index() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('locations')));
        $meta = array('page_title' => lang('locations'), 'bc' => $bc);

        $this->data['tiposContrato'] = $this->Tipocontrato_model->getAll();
        $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
        $this->data['statusDisponibilidade'] = $this->StatusDisponibilidadeRepository_model->getAll();

        $this->page_construct('location/index', $meta, $this->data);
    }

    public function getLocations() {

        $edit_link = anchor('location/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_location'), 'class="sledit"');
        $pdf_link = anchor('location/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher_location'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> ' . lang('historico_parcela') . '</a>';

        $delete_link = "<a href='#' class='po' title='<b>" . lang("cancel_location") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('location/cancel/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('cancel_location') . "</a>";

        $add_pickup = anchor('location/add_pickup_vehicle/$1', '<i class="fa fa-check"></i> ' . lang('add_pickup_vehicle'), 'data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false"');
        $add_return = anchor('location/add_vehicle_return/$1', '<i class="fa fa-check-circle"></i> ' . lang('add_return_vehicle'), 'data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"');

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $add_pickup . '</li>
                    <li>' . $add_return . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->load->library('datatables');

        $this->datatables
            ->select("locacao_veiculo.id as id, 
                    products.image as image,
                    locacao_veiculo.reference_no, 
                    tipo_veiculo.name as tipo_veiculo, 
                    concat(sma_veiculo.name,'<br/><small>(', sma_veiculo.placa,')</small>') as veiculo, 
                    companies.name as locatario,
                    concat(sma_status_disponibilidade.name, '@', sma_status_disponibilidade.cor) as status,
                    tipo_contrato.sigla as tipo_contrato,
                    concat(sma_locacao_veiculo.dataRetiradaPrevista, ' ', sma_locacao_veiculo.horaRetiradaPrevista) as retidada,
                    concat(sma_locacao_veiculo.dataDevolucaoPrevista, ' ', sma_locacao_veiculo.horaDevolucaoPrevista) as devolucao,
                    locacao_veiculo.locacao_status as locacao_status,
                    locacao_veiculo.grand_total as grand_total,
                    locacao_veiculo.paid as paid,
                    (sma_locacao_veiculo.grand_total-sma_locacao_veiculo.paid) as balance,
                    locacao_veiculo.payment_status as payment_status
                    ")
            ->from('locacao_veiculo')
            ->join('companies', 'companies.id = locacao_veiculo.customer_id', 'left')
            ->join('status_disponibilidade', 'status_disponibilidade.id = locacao_veiculo.status_disponibilidade_id', 'left')
            ->join('tipo_contrato', 'tipo_contrato.id = locacao_veiculo.tipo_contrato')
            ->join('products', 'products.id = locacao_veiculo.product_id')
            ->join('veiculo', 'veiculo.id = products.veiculo')
            ->join('tipo_veiculo', 'tipo_veiculo.id = veiculo.tipo_veiculo');

        if ($this->input->post('fl_data_retirada')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.dataRetiradaPrevista >= '{$this->input->post('fl_data_retirada')}' ");
        if ($this->input->post('fl_data_retirada_ate')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.dataRetiradaPrevista <= '{$this->input->post('fl_data_retirada_ate')}' ");
        if ($this->input->post('fl_hora_retirada_de')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.horaRetiradaPrevista >= '{$this->input->post('fl_hora_retirada_de')}' ");
        if ($this->input->post('fl_hora_retirada_ate')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.horaRetiradaPrevista <= '{$this->input->post('fl_hora_retirada_ate')}' ");

        if ($this->input->post('fl_data_devolucao_de')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.dataDevolucaoPrevista >= '{$this->input->post('fl_data_devolucao_de')}' ");
        if ($this->input->post('fl_data_devolucao_ate')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.dataDevolucaoPrevista <= '{$this->input->post('fl_data_devolucao_ate')}' ");
        if ($this->input->post('fl_hora_devolucao_de')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.horaDevolucaoPrevista >= '{$this->input->post('fl_hora_devolucao_de')}' ");
        if ($this->input->post('fl_hora_devolucao_ate')) $this->datatables->where("{$this->db->dbprefix('locacao_veiculo')}.horaDevolucaoPrevista <= '{$this->input->post('fl_hora_devolucao_ate')}' ");

        if ($this->input->post('fl_tipo_contrato')) $this->datatables->where("{$this->db->dbprefix('tipo_contrato')}.id", $this->input->post('fl_tipo_contrato'));
        if ($this->input->post('fl_tipo_veiculo')) $this->datatables->where("{$this->db->dbprefix('tipo_veiculo')}.id", $this->input->post('fl_tipo_veiculo'));
        if ($this->input->post('fl_veiculo')) $this->datatables->where("{$this->db->dbprefix('veiculo')}.id", $this->input->post('fl_veiculo'));
        if ($this->input->post('fl_status_disponibilidade')) $this->datatables->where("{$this->db->dbprefix('status_disponibilidade')}.id", $this->input->post('fl_status_disponibilidade'));

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function map() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('location')));
        $meta = array('page_title' => lang('locations'), 'bc' => $bc);
        $this->page_construct('location/map', $meta, $this->data);
    }

    public function add() {

        $this->form_validation->set_rules('customer', lang("customer"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('lo');

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else{
                $date = date('Y-m-d H:i:s');
            }

            $dataLocation = array(
                'date'  => $date,
                'reference_no' => $reference,

                'status_disponibilidade_id' =>  $this->Settings->status_disponibilidade_reservado_id,

                'locacao_status' => 'billed',
                'payment_status' => 'due',

                //'biller' => $this->input->post('biller'),//TODO NOME DO VENDEDOR
                'biller_id'     => $this->input->post('biller'),

                //'customer' => $this->input->post('biller'),//TODO NOME DO CLIENT
                'customer_id'   => $this->input->post('customer'),

                'veiculo_id'   => $this->input->post('veiculo'),
                'product_id' => '117',//TODO VIR DA BUSCA

                'tipo_contrato'   => $this->input->post('tipo_contrato'),
                //'regra_cobranca_id'   => $this->input->post('tipo_contrato'),//TODO REGRA DE COBRANCA

                'valor_franquia' => $this->input->post('valor_franquia'),
                'valor_excedente' => $this->input->post('valor_excedente'),
                'acrescimo' => $this->input->post('acrescimo'),
                'desconto' => $this->input->post('desconto'),
                'valor_aluguel' => $this->input->post('valor_aluguel'),

                'qtd_dias_locacao' => $this->input->post('qtdDias'),
                'qtd_horas_locacao' => $this->input->post('qtdHoras'),

                'dataRetiradaPrevista'   => $this->input->post('dataRetiradaPrevista'),
                'horaRetiradaPrevista'   => $this->input->post('horaRetiradaPrevista'),
                'dataDevolucaoPrevista'   => $this->input->post('dataDevolucaoPrevista'),
                'horaDevolucaoPrevista'   => $this->input->post('horaDevolucaoPrevista'),

                'local_retirada_id'    => $this->input->post('local_retirada_id'),
                'endereco_retirada'      => $this->input->post('endereco_retirada'),
                'numero_retirada'      => $this->input->post('numero_retirada'),
                'bairro_retirada'      => $this->input->post('bairro_retirada'),
                'complemento_retirada'      => $this->input->post('complemento_retirada'),
                'cidade_retirada'      => $this->input->post('cidade_retirada'),
                'estado_retirada'      => $this->input->post('estado_retirada'),
                'country_retirada'      => $this->input->post('country_retirada'),

                'local_devolucao_id'    => $this->input->post('local_devolucao_id'),
                'cep_devolucao'    => $this->input->post('cep_devolucao'),
                'endereco_devolucao'      => $this->input->post('endereco_devolucao'),
                'bairro_devolucao'      => $this->input->post('bairro_devolucao'),
                'numero_devolucao'      => $this->input->post('numero_devolucao'),
                'complemento_devolucao'      => $this->input->post('complemento_devolucao'),
                'cidade_devolucao'      => $this->input->post('cidade_devolucao'),
                'estado_devolucao'      => $this->input->post('estado_devolucao'),
                'country_devolucao'      => $this->input->post('country_devolucao'),

                'note'  => $this->sma->clear_tags($this->input->post('note')),

                'created_by' => $this->session->userdata('user_id'),
            );

        } elseif ($this->input->post('add_location')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->LocationService_model->addLocationVehicle($dataLocation)) {
            $this->session->set_flashdata('message', lang("location_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['billers']  = $this->site->getAllCompanies('biller');
            $this->data['tiposContrato'] = $this->Tipocontrato_model->getAll();
            $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();

            $this->load->view($this->theme . 'location/add', $this->data);
        }
    }

    public function edit($id=null) {

    }

    public function add_pickup_vehicle($id = null) {

        $location = $this->LocationService_model->getById($id);

        $this->form_validation->set_rules('locacao_veiculo_id', lang("locacao_veiculo_id"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            $dataPickupVehicle = array(
                'date'  => date('Y-m-d H:i:s'),
                'reference_no' => $reference,
                'locacao_veiculo_id'    => $this->input->post('locacao_veiculo_id'),
                'dataRetirada'    => $this->input->post('dataRetirada'),
                'horaRetirada'    => $this->input->post('horaRetirada'),
                'local_retirada_id'    => $this->input->post('local_retirada_id'),
                'veiculo_id'    => $this->input->post('veiculo_id'),
                'product_id'    => $this->input->post('product_id'),
                'tanque_veiculo_id'    => $this->input->post('tanque_veiculo_id'),
                'km_retirada_veiculo'    => $this->input->post('km_retirada_veiculo'),
                'note'    => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

        } elseif ($this->input->post('add_pickup_vehicle')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->LocationService_model->addPickupVehicle($dataPickupVehicle)) {
            $this->session->set_flashdata('message', lang("pickup_vehicle_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['location']         = $location;
            $this->data['cumbustiveis']     = $this->TanqueVeiculoRepository_model->getAllByTipoVeiculo($location->tipo_combustivel);
            $this->data['locais_embarque']  = $this->site->getLocaisEmbarque();

            $this->load->view($this->theme . 'location/add_pickup_vehicle', $this->data);
        }
    }

    public function add_vehicle_return($id=null) {
        $location = $this->LocationService_model->getById($id);

        $this->form_validation->set_rules('locacao_veiculo_id', lang("locacao_veiculo_id"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            $dataVehicleReturn = array(
                'date'  => date('Y-m-d H:i:s'),
                'reference_no' => $reference,
                'locacao_veiculo_id'    => $this->input->post('locacao_veiculo_id'),
                'dataDevolucao'    => $this->input->post('dataDevolucao'),
                'horaDevolucao'    => $this->input->post('horaDevolucao'),
                'local_devolucao_id'    => $this->input->post('local_devolucao_id'),
                'veiculo_id'    => $this->input->post('veiculo_id'),
                'product_id'    => $this->input->post('product_id'),
                'tanque_veiculo_id'    => $this->input->post('tanque_veiculo_id'),
                'km_devolucao_veiculo'    => $this->input->post('km_devolucao_veiculo'),
                'note'    => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

        } elseif ($this->input->post('add_vehicle_return')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->LocationService_model->addVehicleReturn($dataVehicleReturn)) {
            $this->session->set_flashdata('message', lang("vehicle_return_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['location']         = $location;
            $this->data['cumbustiveis']     = $this->TanqueVeiculoRepository_model->getAllByTipoVeiculo($location->tipo_combustivel);
            $this->data['locais_embarque']  = $this->site->getLocaisEmbarque();

            $this->load->view($this->theme . 'location/add_vehicle_return', $this->data);
        }
    }

    public function location_reminders() {

        $this->data['reminders'] = $this->AvailabilityRepository_model->getVehicle_availability();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('location_reminders')));
        $meta = array('page_title' => lang('location_reminders'), 'bc' => $bc);
        $this->page_construct('location/location_reminders', $meta, $this->data);
    }

    public function vehicle_fleet_panel() {
        $this->data['carros'] = $this->AvailabilityRepository_model->getVehicle_availability();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('vehicle_fleet_panel')));
        $meta = array('page_title' => lang('vehicle_fleet_panel'), 'bc' => $bc);
        $this->page_construct('location/list', $meta, $this->data);
    }

    public function availability_panel() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('availability_panel')));
        $meta = array('page_title' => lang('availability_panel'), 'bc' => $bc);

        $this->data['categoriasVeiculo'] = $this->CategoriaVeiculoRepository_model->getAll();
        $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
        $this->data['statusDisponibilidade'] = $this->StatusDisponibilidadeRepository_model->getAll();

        $this->page_construct('location/availability_panel', $meta, $this->data);
    }
    public function availability_searh($veiculo_id = null) {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('availability_searh')));
        $meta = array('page_title' => lang('availability_searh'), 'bc' => $bc);

        $programacao = $this->getProgramacao();

        $start_time = $programacao->dataSaida.' '.$programacao->horaSaida;
        $end_time   = $programacao->dataRetorno.' '.$programacao->horaRetorno;

        $datetimes = $this->generate_datetime($start_time, $end_time);

        $this->data['datetimes'] = $datetimes;

        $this->data['location'] = $this->LocationService_model->getById(1);

        $this->data['datas'] = $this->AvailabilityRepository_model->getVehicle_availability(true);
        $this->data['categoriasVeiculo'] = $this->CategoriaVeiculoRepository_model->getAll();
        $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
        $this->data['statusDisponibilidade'] = $this->StatusDisponibilidadeRepository_model->getAll();

        $this->data['vehicle'] = $this->VehicleRepository_model->getByID($veiculo_id);

        $this->page_construct('location/availability_searh', $meta, $this->data);
    }

    public function availability_searh_modal($veiculo_id = 3) {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $programacao = $this->getProgramacao();

        $start_time = $programacao->dataSaida.' '.$programacao->horaSaida;
        $end_time   = $programacao->dataRetorno.' '.$programacao->horaRetorno;

        $datetimes = $this->generate_datetime($start_time, $end_time);

        $this->data['datetimes'] = $datetimes;

        $this->data['location'] = $this->LocationService_model->getById(1);

        $this->data['datas'] = $this->AvailabilityRepository_model->getVehicle_availability(true);
        $this->data['categoriasVeiculo'] = $this->CategoriaVeiculoRepository_model->getAll();
        $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
        $this->data['statusDisponibilidade'] = $this->StatusDisponibilidadeRepository_model->getAll();

        $this->data['vehicle'] = $this->VehicleRepository_model->getByID($veiculo_id);

        $this->load->view($this->theme . 'location/availability_searh_modal', $this->data);

    }

    function get_events() {

        $events = $this->AvailabilityRepository_model->getVehicle_availability(true);
        $output_arrays = array();

        foreach ($events as $event) {
            $output_arrays[] = array(
                'id' => $event->id,
                'title' => $event->veiculo.' | '.$event->disponibilidade.' | '.$event->cliente,
                'start' => $event->data.'T'.$event->hora,
                'end' => $event->data.'T'.$event->hora,
                //'description' => $event->id,
                'color' => '#ff9f89',
                //'overlap' => true,
                //'display' => 'background',
            );
        }

        $this->sma->send_json($output_arrays);
    }

    function getAvailability()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("
                agenda_disponibilidade.id as id,
                products.image as image,
                veiculo.reference_no, 
                tipo_veiculo.name as tipo_veiculo, 
                categoria_veiculo.name as categoria_veiculo, 
                veiculo.name as veiculo, 
                veiculo.placa, 
                veiculo.ano, 
                veiculo.cor, 
                status_disponibilidade.name as disponibilidade,
                agenda_disponibilidade.data as data,
                agenda_disponibilidade.hora as hora,
                concat(sma_companies.name, '<br/>' , sma_companies.cf5) as cliente,
                concat(sma_locacao_veiculo.dataDevolucaoPrevista,' ', sma_locacao_veiculo.horaDevolucaoPrevista) as devolucao
                ")
            ->from("agenda_disponibilidade")
            ->join('locacao_veiculo', '
                (sma_locacao_veiculo.dataDevolucaoPrevista >= sma_agenda_disponibilidade.data AND (sma_locacao_veiculo.horaRetiradaPrevista < sma_agenda_disponibilidade.hora AND sma_locacao_veiculo.dataRetiradaPrevista = sma_agenda_disponibilidade.data AND sma_locacao_veiculo.dataRetiradaPrevista != sma_locacao_veiculo.dataDevolucaoPrevista)) 
                OR (sma_locacao_veiculo.dataDevolucaoPrevista > sma_locacao_veiculo.dataRetiradaPrevista AND sma_agenda_disponibilidade.data > sma_locacao_veiculo.dataRetiradaPrevista  AND sma_agenda_disponibilidade.data < sma_locacao_veiculo.dataDevolucaoPrevista)
                OR (sma_locacao_veiculo.dataDevolucaoPrevista >= sma_agenda_disponibilidade.data AND (sma_locacao_veiculo.horaDevolucaoPrevista > sma_agenda_disponibilidade.hora AND sma_locacao_veiculo.dataRetiradaPrevista != sma_agenda_disponibilidade.data AND sma_locacao_veiculo.dataDevolucaoPrevista = sma_agenda_disponibilidade.data))
                OR ((sma_locacao_veiculo.dataRetiradaPrevista = sma_agenda_disponibilidade.data  AND sma_locacao_veiculo.horaRetiradaPrevista <= sma_agenda_disponibilidade.hora  AND sma_locacao_veiculo.horaDevolucaoPrevista >= sma_agenda_disponibilidade.hora)
                OR (sma_locacao_veiculo.dataDevolucaoPrevista = sma_agenda_disponibilidade.data  AND sma_locacao_veiculo.horaRetiradaPrevista <= sma_agenda_disponibilidade.hora  AND sma_locacao_veiculo.horaDevolucaoPrevista >= sma_agenda_disponibilidade.hora))
                ', 'left')
            ->join('status_disponibilidade', 'status_disponibilidade.id = locacao_veiculo.status_disponibilidade_id', 'left')
            ->join('companies', 'companies.id = locacao_veiculo.customer_id', 'left')
            ->join('products', 'products.id = agenda_disponibilidade.produto')
            ->join('veiculo', 'veiculo.id = products.veiculo')
            ->join('tipo_veiculo', 'tipo_veiculo.id = veiculo.tipo_veiculo')
            ->join('categoria_veiculo', 'categoria_veiculo.id = veiculo.categoria')
            ->order_by('agenda_disponibilidade.data, agenda_disponibilidade.hora')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('financeiroutil/editarTipoCobranca/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("add_location") . "'><i class=\"fa fa-calendar\"></i></a> </div>", "id");

        if ($this->input->post('start_date'))   $this->datatables->where("{$this->db->dbprefix('agenda_disponibilidade')}.data >= '{$this->input->post('start_date')}'");
        if ($this->input->post('end_date'))     $this->datatables->where("{$this->db->dbprefix('agenda_disponibilidade')}.data <= '{$this->input->post('end_date') }'");

        if ($this->input->post('start_time'))   $this->datatables->where("{$this->db->dbprefix('agenda_disponibilidade')}.hora >= '{$this->input->post('start_time')}'");
        if ($this->input->post('end_time'))   $this->datatables->where("{$this->db->dbprefix('agenda_disponibilidade')}.hora <= '{$this->input->post('end_time') }'");

        if ($this->input->post('fl_veiculo')) $this->datatables->where("{$this->db->dbprefix('veiculo')}.id", $this->input->post('fl_veiculo'));
        if ($this->input->post('fl_categoria_veiculo')) $this->datatables->where("{$this->db->dbprefix('categoria_veiculo')}.id", $this->input->post('fl_categoria_veiculo'));
        if ($this->input->post('fl_tipo_veiculo')) $this->datatables->where("{$this->db->dbprefix('tipo_veiculo')}.id", $this->input->post('fl_tipo_veiculo'));
        if ($this->input->post('fl_status_disponibilidade')) $this->datatables->where("{$this->db->dbprefix('status_disponibilidade')}.id", $this->input->post('fl_status_disponibilidade'));

        echo $this->datatables->generate();
    }

    public function  disponibilidade() {

        $programacao = $this->getProgramacao();

        $start_time = $programacao->dataSaida.' '.$programacao->horaSaida;
        $end_time   = $programacao->dataRetorno.' '.$programacao->horaRetorno;

        $datetimes = $this->generate_datetime($start_time, $end_time);

       // $this->data['datetimes'] = $datetimes;


        $this->data['datas'] = $this->AvailabilityRepository_model->getVehicle_availability();

        $this->load->view($this->theme . 'location/disponibilidade', $this->data);
    }

    public function searh_vehicle_availability() {
        $tavailable_times = $this->AvailabilityRepository_model->getVehicle_availability();

        $this->sma->send_json($tavailable_times);
    }

    public function searh_vehicle_availability_hour() {
        $date = $this->input->get('date', false);

        $tavailable_times = $this->AvailabilityRepository_model->getVehicle_availability($date);

        $this->sma->send_json($tavailable_times);
    }

    public function cancel($id = null) {

        if ($this->input->get('id')) $id = $this->input->get('id');

        try {
            if ($this->LocationService_model->cancelLocation($id)) {

                if ($this->input->is_ajax_request()) {
                    echo lang("lease_canceled_successfully");
                    die();
                }

                redirect('location');
            }
        } catch (Exception $exception) {
            //$this->session->set_flashdata('error', lang($exception->getMessage()));
            if ($this->input->is_ajax_request()) {
                echo  lang($exception->getMessage());
                die();
            }
        }
    }
    public function search_value_vehicle_rental_rule() {
        $tipo_contrato = $this->input->get('tipo_contrato', false);
        $qtdDias = $this->input->get('qtd_dias', false);
        $qtHoras = $this->input->get('qtd_horas', false);

        $regra_cobranca = $this->RegraCobrancaLocacaoRepository_model->search_value_vehicle_rental_rule(1, $qtdDias);

        if ($regra_cobranca) {
            $regra_cobranca->valor_aluguel = $regra_cobranca->valor * $qtdDias;
        }

        $this->sma->send_json($regra_cobranca);
    }

    function generate_datetime($start_time, $end_time) {
        $datetimes = array();
        $current_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        while ($current_time <= $end_time) {
            $datetimes[] = date('Y-m-d H:i:s', $current_time);
            $current_time = strtotime('+1 hour', $current_time);
        }

        return $datetimes;
    }

    public function getProgramacao()
    {
        $this->db->limit(1);

        $q = $this->db->get('agenda_programacao');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}