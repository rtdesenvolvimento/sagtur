<?php defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class CI_Storage
{

    const BUCKET = 'sagtur';

    private $client;
    public $error_msg = array();

    public $file_size = NULL;

    public $file_temp = '';

    public $file_name = '';

    public $file_type = '';

    public $max_size = 0;

    protected $_mimes = array();

    protected $_CI;

    protected $_file_name_override = '';

    public $allowed_types = '';

    public $file_ext = '';

    public $mod_mime_fix = TRUE;

    public function __construct()
    {
        empty($config) OR $this->initialize($config, FALSE);

        $this->_mimes =& get_mimes();
        $this->_CI =& get_instance();

        //$this->client = new S3Client($this->getConfig());

        log_message('info', 'Storage Class Initialized');

    }

    public function initialize(array $config = array(), $reset = TRUE): CI_Storage
    {
        $reflection = new ReflectionClass($this);

        if ($reset === TRUE)
        {
            $defaults = $reflection->getDefaultProperties();
            foreach (array_keys($defaults) as $key)
            {
                if ($key[0] === '_')
                {
                    continue;
                }

                if (isset($config[$key]))
                {
                    if ($reflection->hasMethod('set_'.$key))
                    {
                        $this->{'set_'.$key}($config[$key]);
                    }
                    else
                    {
                        $this->$key = $config[$key];
                    }
                }
                else
                {
                    $this->$key = $defaults[$key];
                }
            }
        }
        else
        {
            foreach ($config as $key => &$value)
            {
                if ($key[0] !== '_' && $reflection->hasProperty($key))
                {
                    if ($reflection->hasMethod('set_'.$key))
                    {
                        $this->{'set_'.$key}($value);
                    }
                    else
                    {
                        $this->$key = $value;
                    }
                }
            }
        }

        // if a file_name was provided in the config, use it instead of the user input
        // supplied file name for all uploads until initialized again
        $this->_file_name_override = $this->file_name;
        return $this;
    }


    private function getConfig(): array
    {
        $config =
            [
                'endpoint' => 'https://11652bc094dc1f514aaafb293ce84bd9.r2.cloudflarestorage.com',
                'version' => 'latest',
                'region' => 'auto',
                'credentials' =>
                    [
                        'key' => 'a2ffaffdd37af96137755f22eab7d61e',
                        'secret' => 'f86afbe7c1287f3b55d4685a6b485089290d279f9026958774b06ba45c2d2cfc',
                    ],
            ];

        return $config;
    }

    public function listBuckets() {

        $buckets = $this->client->listBuckets();

        print_r($buckets);
    }

    public function createBucket($bucket) {
        $this->client->createBucket(['Bucket' => $bucket]);
    }

    private function bucketExists($bucket): bool
    {
        try {
            $this->client->headBucket(['Bucket' => $bucket]);
            return true;
        } catch (AwsException $e) {
            return false;
        }
    }

    public function deleteBucket($bucket) {
        $this->client->deleteBucket([
            'Bucket' => $bucket
        ]);
    }

    public function listObjects($bucket) {

        $result =  $this->client->listObjects(['Bucket' => $bucket]);

        foreach ($result['Contents'] as $object) {
            echo $object['Key'] . "<br/>";
        }

    }

    public function do_upload($fileInputName = 'userfile', $subfolder = 'docs'): string
    {

        try {

            $this->client = new S3Client($this->getConfig());

            // Is $_FILES[$field] set? If not, no reason to continue.
            if (isset($_FILES[$fileInputName]))
            {
                $_file = $_FILES[$fileInputName];
            }
            // Does the field name contain array notation?
            elseif (($c = preg_match_all('/(?:^[^\[]+)|\[[^]]*\]/', $fileInputName, $matches)) > 1)
            {
                $_file = $_FILES;
                for ($i = 0; $i < $c; $i++)
                {
                    // We can't track numeric iterations, only full field names are accepted
                    if (($field = trim($matches[0][$i], '[]')) === '' OR ! isset($_file[$field]))
                    {
                        $_file = NULL;
                        break;
                    }

                    $_file = $_file[$field];
                }
            }

            if ( ! isset($_file))
            {
                $this->set_error('upload_no_file_selected', 'debug');
                return FALSE;
            }

            $folder_company = $this->_CI->session->userdata('cnpjempresa');


            // Skip MIME type detection?
            if ($this->detect_mime !== FALSE)
            {
                $this->_file_mime_type($_file);
            }

            $fileTmpPath = $_FILES[$fileInputName]['tmp_name'];
            $fileOriginalName = $_FILES[$fileInputName]['name'];
            $extension = $this->get_extension($fileOriginalName);
            $fileMimeType = mime_content_type($fileTmpPath);

            $this->file_type = preg_replace('/^(.+?);.*$/', '\\1', $this->file_type);
            $this->file_type = strtolower(trim(stripslashes($this->file_type), '"'));
            $this->file_name = $this->_prep_filename($_file['name']);
            $this->file_ext	 = $this->get_extension($this->file_name);
            
            // Set the uploaded data as class variables
            $this->file_temp = $_file['tmp_name'];
            $this->file_size = $_file['size'];

            // Is the file type allowed to be uploaded?
            if ( ! $this->is_allowed_filetype())
            {
                $this->set_error('upload_invalid_filetype', 'debug');
                return FALSE;
            }

            // Convert the file size to kilobytes
            if ($this->file_size > 0)
            {
                $this->file_size = round($this->file_size/1024, 2);
            }

            if (!$this->is_allowed_filesize()) {
                $this->set_error('upload_invalid_filesize', 'info');
                return false;
            }

            $fileName = $folder_company.'/'.$subfolder.'/' . md5(uniqid(mt_rand())) . $extension;

            $retorno = $this->client->putObject([
                'Bucket'     => CI_Storage::BUCKET,
                'Key'        => $fileName, // Caminho no R2
                'SourceFile' => $fileTmpPath,
                'ContentType' => $fileMimeType, // Define o tipo do documento
                'ACL'        => 'public-read',
            ]);

            $this->file_name = $fileName;

            return TRUE;
        } catch (AwsException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function is_allowed_filesize(): bool
    {
        return ($this->max_size === 0 OR $this->max_size > $this->file_size);
    }

    public function get_extension($filename): string
    {
        $x = explode('.', $filename);

        if (count($x) === 1)
        {
            return '';
        }

        $ext = ($this->file_ext_tolower) ? strtolower(end($x)) : end($x);
        return '.'.$ext;
    }

    public function getObjectDonwload($bucket) {

        $this->client->getObject([
            'Bucket' => $bucket,
            'Key'    => '92610a520b418cdac43d58a0dfa343672e3.png',
            'SaveAs' => 'C:\Users\andre\Downloads\2.png'
        ]);
    }

    public function getObjectURLContentType($bucket) {

        $retorno =  $this->client->getObject([
            'Bucket' => $bucket,
            'Key'    => '92610a520b418cdac43d58a0dfa343672e3.png',
        ]);

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $tipoArquivo = finfo_buffer($finfo, $retorno['Body']);
        finfo_close($finfo);
        header('Content-Type: ' . $tipoArquivo);

        echo $retorno['Body'];
    }

    public function getObjectURLContentURL($bucket) {

        $retorno = $this->client->getObjectUrl($bucket,'92610a520b418cdac43d58a0dfa343672e3.png');

        echo $retorno;
    }

    public function getObjectURL($bucket) {

        $key = '92610a520b418cdac43d58a0dfa343672e3.png';

        $cmd = $this->client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key'    => $key,
        ]);

        $request = $this->client->createPresignedRequest($cmd, '+1 minutes');

        // Obtém a URL assinada
        $url = (string) $request->getUri();

        echo $url;
    }

    public function deleteObject($bucket) {

        $this->client->deleteObject([
            'Bucket' => $bucket,
            'Key' => 'object-name'
        ]);
    }

    public function display_errors($open = '<p>', $close = '</p>'): string
    {
        return (count($this->error_msg) > 0) ? $open.implode($close.$open, $this->error_msg).$close : '';
    }

    public function set_error($msg, $log_level = 'error'): CI_Storage
    {
        $this->_CI->lang->load('upload');

        is_array($msg) OR $msg = array($msg);
        foreach ($msg as $val)
        {
            $msg = ($this->_CI->lang->line($val) === FALSE) ? $val : $this->_CI->lang->line($val);
            $this->error_msg[] = $msg;
            log_message($log_level, $msg);
        }

        return $this;
    }

    public function set_max_filesize($n): CI_Storage
    {
        $this->max_size = ($n < 0) ? 0 : (int) $n;
        return $this;
    }

    protected function set_max_size($n): CI_Storage
    {
        return $this->set_max_filesize($n);
    }

    public function set_allowed_types($types): CI_Storage
    {
        $this->allowed_types = (is_array($types) OR $types === '*')
            ? $types
            : explode('|', $types);
        return $this;
    }

    public function is_allowed_filetype($ignore_mime = FALSE): bool
    {
        if ($this->allowed_types === '*')
        {
            return TRUE;
        }

        if (empty($this->allowed_types) OR ! is_array($this->allowed_types))
        {
            $this->set_error('upload_no_file_types', 'debug');
            return FALSE;
        }

        $ext = strtolower(ltrim($this->file_ext, '.'));

        if ( ! in_array($ext, $this->allowed_types, TRUE))
        {
            return FALSE;
        }

        // Images get some additional checks
        if (in_array($ext, array('gif', 'jpg', 'jpeg', 'jpe', 'png'), TRUE) && @getimagesize($this->file_temp) === FALSE)
        {
            return FALSE;
        }

        if ($ignore_mime === TRUE)
        {
            return TRUE;
        }

        if (isset($this->_mimes[$ext]))
        {
            return is_array($this->_mimes[$ext])
                ? in_array($this->file_type, $this->_mimes[$ext], TRUE)
                : ($this->_mimes[$ext] === $this->file_type);
        }

        return FALSE;
    }

    protected function _prep_filename($filename)
    {
        if ($this->mod_mime_fix === FALSE OR $this->allowed_types === '*' OR ($ext_pos = strrpos($filename, '.')) === FALSE)
        {
            return $filename;
        }

        $ext = substr($filename, $ext_pos);
        $filename = substr($filename, 0, $ext_pos);
        return str_replace('.', '_', $filename).$ext;
    }

    protected function _file_mime_type($file)
    {
        // We'll need this to validate the MIME info string (e.g. text/plain; charset=us-ascii)
        $regexp = '/^([a-z\-]+\/[a-z0-9\-\.\+]+)(;\s.+)?$/';

        /**
         * Fileinfo extension - most reliable method
         *
         * Apparently XAMPP, CentOS, cPanel and who knows what
         * other PHP distribution channels EXPLICITLY DISABLE
         * ext/fileinfo, which is otherwise enabled by default
         * since PHP 5.3 ...
         */
        if (function_exists('finfo_file'))
        {
            $finfo = @finfo_open(FILEINFO_MIME);
            if (is_resource($finfo)) // It is possible that a FALSE value is returned, if there is no magic MIME database file found on the system
            {
                $mime = @finfo_file($finfo, $file['tmp_name']);
                finfo_close($finfo);

                /* According to the comments section of the PHP manual page,
                 * it is possible that this function returns an empty string
                 * for some files (e.g. if they don't exist in the magic MIME database)
                 */
                if (is_string($mime) && preg_match($regexp, $mime, $matches))
                {
                    $this->file_type = $matches[1];
                    return;
                }
            }
        }

        /* This is an ugly hack, but UNIX-type systems provide a "native" way to detect the file type,
         * which is still more secure than depending on the value of $_FILES[$field]['type'], and as it
         * was reported in issue #750 (https://github.com/EllisLab/CodeIgniter/issues/750) - it's better
         * than mime_content_type() as well, hence the attempts to try calling the command line with
         * three different functions.
         *
         * Notes:
         *	- the DIRECTORY_SEPARATOR comparison ensures that we're not on a Windows system
         *	- many system admins would disable the exec(), shell_exec(), popen() and similar functions
         *	  due to security concerns, hence the function_usable() checks
         */
        if (DIRECTORY_SEPARATOR !== '\\')
        {
            $cmd = function_exists('escapeshellarg')
                ? 'file --brief --mime '.escapeshellarg($file['tmp_name']).' 2>&1'
                : 'file --brief --mime '.$file['tmp_name'].' 2>&1';

            if (function_usable('exec'))
            {
                /* This might look confusing, as $mime is being populated with all of the output when set in the second parameter.
                 * However, we only need the last line, which is the actual return value of exec(), and as such - it overwrites
                 * anything that could already be set for $mime previously. This effectively makes the second parameter a dummy
                 * value, which is only put to allow us to get the return status code.
                 */
                $mime = @exec($cmd, $mime, $return_status);
                if ($return_status === 0 && is_string($mime) && preg_match($regexp, $mime, $matches))
                {
                    $this->file_type = $matches[1];
                    return;
                }
            }

            if ( ! ini_get('safe_mode') && function_usable('shell_exec'))
            {
                $mime = @shell_exec($cmd);
                if (strlen($mime) > 0)
                {
                    $mime = explode("\n", trim($mime));
                    if (preg_match($regexp, $mime[(count($mime) - 1)], $matches))
                    {
                        $this->file_type = $matches[1];
                        return;
                    }
                }
            }

            if (function_usable('popen'))
            {
                $proc = @popen($cmd, 'r');
                if (is_resource($proc))
                {
                    $mime = @fread($proc, 512);
                    @pclose($proc);
                    if ($mime !== FALSE)
                    {
                        $mime = explode("\n", trim($mime));
                        if (preg_match($regexp, $mime[(count($mime) - 1)], $matches))
                        {
                            $this->file_type = $matches[1];
                            return;
                        }
                    }
                }
            }
        }

        // Fall back to mime_content_type(), if available (still better than $_FILES[$field]['type'])
        if (function_exists('mime_content_type'))
        {
            $this->file_type = @mime_content_type($file['tmp_name']);
            if (strlen($this->file_type) > 0) // It's possible that mime_content_type() returns FALSE or an empty string
            {
                return;
            }
        }

        $this->file_type = $file['type'];
    }

}