<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AvailabilityService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function generate_availability() {

        $start_time = '2023-05-01'.' '.'00:00';
        $end_time   = '2023-05-31'.' '.'23:00';

        $programacao = $this->getProgramacao(117);

        $this->deleteAll($programacao->id, $programacao->produto);

        $datetimes = $this->generate_datetime($start_time, $end_time);

        foreach ($datetimes as $datetime) {
            $date = explode(' ', $datetime)[0];
            $time = explode(' ', $datetime)[1];

            $item = array(
                'agenda_viagem_id'  => $programacao->id,
                'produto'           => $programacao->produto,
                'data'              => $date,
                'hora'              => $time,
            );
            $this->db->insert('agenda_disponibilidade', $item);
        }
    }

    private function generate_datetime($start_time, $end_time) {
        $datetimes = array();
        $current_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        while ($current_time <= $end_time) {
            $datetimes[] = date('Y-m-d H:i:s', $current_time);
            $current_time = strtotime('+1 hour', $current_time);//todo aqui
        }

        return $datetimes;
    }

    public function getProgramacao($produto_id)
    {
        $this->db->limit(1);

        $q = $this->db->get_where('agenda_programacao', array('produto' => $produto_id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteAll($programacao_id, $produto_id) {
        $this->db->delete('agenda_disponibilidade', array('agenda_viagem_id' => $programacao_id, 'produto' => $produto_id));
    }

}