<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RoomListService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('Roomlist_model');
        $this->load->model('Roomlisttipohospedagem_model');
        $this->load->model('Roomlisthospede_model');
        $this->load->model('sales_model');
    }

    public function salvarTipoHospedagem($roomLisTipoHospedage) {
        $tipo_hospedagem_id = $this->Roomlisttipohospedagem_model->add($roomLisTipoHospedage);

        return $this->Roomlisttipohospedagem_model->getById($tipo_hospedagem_id);
    }

    public function salvarHospede($roomLisHospede) {

        $roomListaTipoHospedagem = $this->Roomlisttipohospedagem_model->getById($roomLisHospede['room_list_hospedagem_id']);
        $roomLisHospede['tipo_hospedagem_id'] = $roomListaTipoHospedagem->tipo_hospedagem_id;
        $hospede_id = $this->Roomlisthospede_model->add($roomLisHospede);

        return $this->Roomlisthospede_model->getById($hospede_id);
    }

    public function excluirHospede($id) {
        return $this->Roomlisthospede_model->delete($id);
    }

    public function adicionarRoomList($data)
    {
        if ($this->db->insert("room_list", $data)) {
            return $this->db->insert_id();
        }

        return false;
    }

    public function editarRoomList($id, $data = array())
    {
        if ($this->db->update("room_list", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function excluir($id) {

        $hospedes = $this->Roomlisthospede_model->getAllHospedes($id);

        foreach ($hospedes as $hospede) {
            $this->Roomlisthospede_model->delete($hospede->id);
        }

        $this->Roomlisttipohospedagem_model->deleteByRoomList($id);
        $this->Roomlist_model->delete($id);

        return true;
    }

    public  function excluirTipoHospedagem($tipoHospedagemRoomListId) {

        $hospedes = $this->Roomlisthospede_model->getAllHospedesByTipoHospedagemRoomList($tipoHospedagemRoomListId);

        foreach ($hospedes as $hospede) {
            $this->Roomlisthospede_model->delete($hospede->id);
        }

        $this->Roomlisttipohospedagem_model->delete($tipoHospedagemRoomListId);

        return true;
    }

    public function excluirHospedeByVenda($vendaId) {
        $inv_items = $this->sales_model->getAllInvoiceItems($vendaId);

        foreach ($inv_items as $item) {
            $this->Roomlisthospede_model->deleteByItem($item->id);
        }

    }

}