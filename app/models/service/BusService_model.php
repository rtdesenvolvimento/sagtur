<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BusService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/AutomovelRepository_model', 'AutomovelRepository_model');
    }

    public function addBus($automovel) {

        //$automovel = new Automovel_model();

        $automovel->total_assentos = $this->totalAssentosMapa($automovel);
        $automovelID = $this->__save('automovel', $automovel->__toArray());

        foreach ($automovel->getAssentos() as $assento_automovel) {
            $assento_automovel->automovel_id = $automovelID;
            $this->__save('mapa_automovel', $assento_automovel->__toArray());
        }

        return $automovelID;
    }

    public function editBus($automovel, $automovelID) {
        //$automovel = new Automovel_model();

        $automovel->total_assentos = $this->totalAssentosMapa($automovel);
        $this->__edit('automovel', $automovel->__toArray(),'id', $automovelID);

        $this->db->delete('mapa_automovel', array('automovel_id' => $automovelID));

        foreach ($automovel->getAssentos() as $assento_automovel) {
            $assento_automovel->automovel_id = $automovelID;
            $this->__save('mapa_automovel', $assento_automovel->__toArray());
        }

        return $automovelID;
    }
    public function deletar_bus($id) {
        $this->db->delete('mapa_automovel', array('automovel_id' => $id));
        $this->db->delete('automovel', array('id' => $id));

        return true;
    }

    private function totalAssentosMapa($automovel) {
        $totalAssentos = 0;

        foreach ($automovel->getAssentos() as $assento_automovel) {
            if ($assento_automovel->habilitado) $totalAssentos++;
        }
        return $totalAssentos;
    }

    public function getPlantaDisponivelMarcacao($productID, $programacaoID) {
        $transportes = $this->ProdutoRepository_model->getTransportes($productID);
        $transporteDisponivel       = null;
        $ultimoTransporteDisponivel  = null;

        if (is_array($transportes)) {
            foreach ($transportes as $transporte) {
                if ($transporte->habilitar_selecao_link && $transporte->automovel_id) {

                    $isAssentoLivre = $this->verifica_lotacao_planta($productID, $programacaoID, $transporte->id, $transporte->automovel_id);

                    if ($isAssentoLivre) {
                        $transporteDisponivel = $transporte;
                        break;
                    }

                    $ultimoTransporteDisponivel = $transporte;
                }
            }
        }

        if ($transporteDisponivel == null) {
            $transporteDisponivel = $ultimoTransporteDisponivel;
        }

        return $transporteDisponivel;
    }

    private function verifica_lotacao_planta($productID, $programacaoID, $tipoTransporteID, $automovelID) {

        $automovel = $this->AutomovelRepository_model->getById($automovelID);
        $isAssentoLivre = false;

        for ($i=1; $i<=$automovel->total_assentos;$i++) {
            $assentoFormat = sprintf('%02d', $i);
            $isBloqueadoOuMarcado = $this->BusRepository_model->isAssentoBloqueadoOuMarcado($productID, $programacaoID, $tipoTransporteID, $assentoFormat);

            if (!$isBloqueadoOuMarcado) {
                $isAssentoLivre = true;
            }
        }

        return $isAssentoLivre;
    }

    public function bloqueio_assento($bloqueio_bus) {

        //$bloqueio_bus = new BusDTO_model();

        $data = array(
            'product_id' => $bloqueio_bus->product_id,
            'programacao_id' => $bloqueio_bus->programacao_id,
            'tipo_transporte_id' => $bloqueio_bus->tipo_transporte_id,
            'assento' => $bloqueio_bus->assento,
            'str_assento' => $bloqueio_bus->assentoStr,
            'note' => $bloqueio_bus->note,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id'),
            'active' => 1,
        );
        $this->db->insert('assento_bloqueio', $data);
    }

    public function desbloquear_todos_assentos_session_card() {
        $this->BusRepository_model->desbloquear_todos_assentos_session_card(session_id());
    }

    public function marcacao_assento_area_cliente($bloqueios_bus = array()) {
        try {
            $this->__iniciaTransacao();

            if (!empty($bloqueios_bus)) {

                foreach ($bloqueios_bus as $bloqueio_bus) {
                    if ($bloqueio_bus->assentoStr != '') {
                        $this->marcar_assento_area_cliente($bloqueio_bus);
                    }
                }
            }

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function bloqueio_assentos_site($bloqueios_bus = array()) {
        try {
            $this->__iniciaTransacao();

            if (!empty($bloqueios_bus)) {

                $this->desbloquear_todos_assentos_session_card();

                foreach ($bloqueios_bus as $bloqueio_bus) {
                    if ($bloqueio_bus->assentoStr != '') {
                        $this->bloqueio_assento_site($bloqueio_bus);
                    }
                }
            }

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function marcar_assento_area_cliente($bloqueio_bus) {

        //$bloqueio_bus = new BusDTO_model();
        $bloqueado = $this->BusRepository_model->isAssentoBloqueado($bloqueio_bus->product_id, $bloqueio_bus->programacao_id, $bloqueio_bus->tipo_transporte_id, $bloqueio_bus->assentoStr);

        if ($bloqueado) {
            throw new Exception(lang('Não foi possível confirmar sua reserva. O assento selecionado \"'. $bloqueado->str_assento .'\" não está mais disponível. Por favor selecione outro assento.'));
        }

        $marcado    = $this->BusRepository_model->isAssentoMarcado($bloqueio_bus->product_id, $bloqueio_bus->programacao_id, $bloqueio_bus->tipo_transporte_id, $bloqueio_bus->assentoStr);

        if ($marcado) {
            throw new Exception(lang('Não foi possível confirmar sua reserva. O assento selecionado \"'. $marcado->poltronaClient .'\" não está mais disponível. Por favor selecione outro assento.'));
        }

        // Verificar se o servidor fornece o cabeçalho 'HTTP_X_FORWARDED_FOR'
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        // Verificar se o servidor fornece o cabeçalho 'HTTP_CLIENT_IP'
        elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        // Caso contrário, obter o endereço IP padrão do servidor
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $data = array(
            'product_id' => $bloqueio_bus->product_id,
            'programacao_id' => $bloqueio_bus->programacao_id,
            'tipo_transporte_id' => $bloqueio_bus->tipo_transporte_id,
            'assento' => $bloqueio_bus->assento,
            'str_assento' => $bloqueio_bus->assentoStr,
            'note' => $bloqueio_bus->note,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id'),
            'active' => 0,//deixar desativado so para registro da manipulacao
            'cart_lock' => 1,
            'cart_time_blocked' => date('Y-m-d H:i:s'),

            'sessions_id' => session_id(),
            'ip_address' => $ip,
        );
        $this->db->insert('assento_bloqueio', $data);

        $this->atribuir_assento($bloqueio_bus->item_id, $bloqueio_bus->assentoStr, $bloqueio_bus->tipo_transporte_id);

    }
    private function bloqueio_assento_site($bloqueio_bus) {

        //$bloqueio_bus = new BusDTO_model();
        $bloqueado = $this->BusRepository_model->isAssentoBloqueado($bloqueio_bus->product_id, $bloqueio_bus->programacao_id, $bloqueio_bus->tipo_transporte_id, $bloqueio_bus->assentoStr);

        if ($bloqueado) {
            throw new Exception(lang('Não foi possível confirmar sua reserva. O assento selecionado \"'. $bloqueado->str_assento .'\" não está mais disponível. Por favor selecione outro assento.'));
        }

        $marcado    = $this->BusRepository_model->isAssentoMarcado($bloqueio_bus->product_id, $bloqueio_bus->programacao_id, $bloqueio_bus->tipo_transporte_id, $bloqueio_bus->assentoStr);

        if ($marcado) {
            throw new Exception(lang('Não foi possível confirmar sua reserva. O assento selecionado \"'. $marcado->poltronaClient .'\" não está mais disponível. Por favor selecione outro assento.'));
        }

        // Verificar se o servidor fornece o cabeçalho 'HTTP_X_FORWARDED_FOR'
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        // Verificar se o servidor fornece o cabeçalho 'HTTP_CLIENT_IP'
        elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        // Caso contrário, obter o endereço IP padrão do servidor
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $data = array(
            'product_id' => $bloqueio_bus->product_id,
            'programacao_id' => $bloqueio_bus->programacao_id,
            'tipo_transporte_id' => $bloqueio_bus->tipo_transporte_id,
            'assento' => $bloqueio_bus->assento,
            'str_assento' => $bloqueio_bus->assentoStr,
            'note' => $bloqueio_bus->note,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id'),
            'active' => 1,
            'cart_lock' => 1,
            'cart_time_blocked' => date('Y-m-d H:i:s'),

            'sessions_id' => session_id(),
            'ip_address' => $ip,
        );
        $this->db->insert('assento_bloqueio', $data);
    }

    public function desbloqueio_assento($id) {
        $data = array(
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('user_id'),
            'active' => 0,
        );
        $this->db->update('assento_bloqueio', $data, array('id' => $id));
    }

    public function atribuir_todos_ao_automovel($productID, $programacaoID, $tipoTransporteID, $embarqueID = null) {

        if ($embarqueID) {
            $this->db->update('sale_items', array('tipoTransporte'=> $tipoTransporteID), array('product_id' => $productID, 'programacaoId' => $programacaoID, 'localEmbarque' => $embarqueID));
        } else {
            $this->db->update('sale_items', array('tipoTransporte'=> $tipoTransporteID), array('product_id' => $productID, 'programacaoId' => $programacaoID));
        }

        $this->db->update('assento_bloqueio', array('tipo_transporte_id'=> $tipoTransporteID), array('product_id' => $productID, 'programacao_id' => $programacaoID));
    }

    public  function atribuir_assento($itemId, $poltrona, $tipoTransporte) {

        if ($poltrona == 'SN') $poltrona = '';

        $this->db->update('sale_items', array('poltronaClient' => $poltrona, 'tipoTransporte'=> $tipoTransporte), array('id' => $itemId));
    }

    public function isAssentoBloqueadoOuMarcado($productID, $programacaoID, $tipoTransporteID, $poltrona, $itemID = null) {
        return $this->BusRepository_model->isAssentoBloqueadoOuMarcado($productID, $programacaoID, $tipoTransporteID, $poltrona, $itemID);
    }
}