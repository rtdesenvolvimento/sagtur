<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update389 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.8.9'), array('setting_id' => 1));
    }

    public function alter_table_settings() {

        $this->db->insert('meio_divulgacao', array('name' => 'Venda Manual',));

        $meio_divulgacao_id = $this->db->insert_id();

        $this->db->update('settings',  array('meio_divulgacao_default' => $meio_divulgacao_id), array('setting_id' => 1));
    }

    public function down() {}
}
