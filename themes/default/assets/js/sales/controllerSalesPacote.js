var ControllerSalesPacote = function () {

    this.onLoad = onLoad;
    this.adicionarNovoItem = adicionarNovoItem;
    this.onLoadCarregarConfiguracaoItem = onLoadCarregarConfiguracaoItem;
    this.preencherTaxasCliente = preencherTaxasCliente;
    this.preencherDadosCliente = preencherDadosCliente;
    this.limparDadosFormularioPacotes = limparDadosFormularioPacotes;
    this.dataAtualFormatada = dataAtualFormatada;

    function onLoad() {
        onClick();
        onChange();
    }

    function onChange() {
        $('#pacoteId').change(function (event) {
            $('#load-new-meu-pacote').load(site.base_url+"sales/adicionarMeuPacote", function() {
                onLoadCarregarConfiguracaoItem();
            });
        });
    }

    function onClick() {

        $('#adicionar-formulario-pacote-operadora').click(function (event) {

            if (!controllerSales.isPreencheuFilial() ) return;

            exibirFormularioPacotesOperadora();
        });

        $('#adicionar-pacote-operadora').click(function (event) {
            salvarPacoteOperadora();
        });

        $('#adicionar-meu-pacote').click(function (event) {

            let pacote = $('#pacoteId').select2('data').text;

            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                $('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }

            $('#add_item').val(pacote);
            $('#add_item').autocomplete("search");
            return false;
        });

        $('#remover-pacote').click(function (event) {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result)  limparDadosFormularioPacotes();
            });
        });
    }

    function exibirFormularioPacotesOperadora() {

        limparDadosFormularioPacotes();

        if (isExibirFormularioOperadora()) buscarFormularioPacotesOperadora();

    }

    function isExibirFormularioOperadora() {
        return !$("#form-pacotes-operadoras" ).is( ":visible" );
    }

    function isExibirFormularioProprio() {
        return !$( "#form-meu-pacote" ).is( ":visible" )
    }

    function buscarFormularioPacotesOperadora() {

        $('#load-new-pacote-operadora').load(site.base_url+"sales/adicionarPasseio", function() {

            $('#descricaoCadastroManual').html('Adicionar Serviço Turístico');

            $('#iconCadastroManual').removeClass('fa-plus');
            $('#iconCadastroManual').removeClass('fa-plane');
            $('#iconCadastroManual').addClass('fa-briefcase');

            setCampoObrigatorio('lsNomePacote');
            setCampoObrigatorio('slEmissaoPacote');
            setCampoObrigatorio('slDateCheckinPacote');
            setCampoObrigatorio('slDateCheckoutPacote');
            setCampoObrigatorio('slSupplierPacote');
            setCampoObrigatorio('search-customer');

            setCampoObrigatorio('origemPacote');
            setCampoObrigatorio('destinoPacote');

            setCampoObrigatorio('fornecedorReceptivo');
            setCampoObrigatorio('telefoneReceptivo');
            setCampoObrigatorio('enderecoReceptivo');

            onLoadCarregarConfiguracaoItem();

            $('#slEmissaoPacote').val(dataAtualFormatada());

            $('#form-pacotes-operadoras').show(600);
            $('#lsNomePacote').focus();
            $('#sticker').hide();

            $("#origemPacote").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'get',
                        url: site.base_url+"sales/getLocais",
                        dataType: "json",
                        data: {
                            term : $("#origemPacote").val(),
                        },
                        success: function (data) {

                            if (data === '') return false;

                            data = JSON.parse(data);
                            response(data);
                        }
                    });
                },
                minLength: 3,
                autoFocus: false,
                delay: 200,
                placeholder: 'Digite uma origem/aeroporto',
                response: function (event, ui) {},
                select: function (event, ui) {}
            });

            $("#destinoPacote").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'get',
                        url: site.base_url+"sales/getLocais",
                        dataType: "json",
                        data: {
                            term : $("#destinoPacote").val(),
                        },
                        success: function (data) {

                            if (data === '') return false;

                            data = JSON.parse(data);
                            response(data);
                        }
                    });
                },
                minLength: 3,
                autoFocus: false,
                delay: 200,
                placeholder: 'Digite uma origem/aeroporto',
                response: function (event, ui) {},
                select: function (event, ui) {}
            });
        });
    }

    function onLoadCarregarConfiguracaoItem() {

        let customer = $('#slcustomer').val();

        window.location.hash ? $('#myTab a[href="' + window.location.hash + '"]').tab('show') : $("#myTabMeusPacotesParametros a:first, #myTabMeusPacotes a").tab("show");

        $('select').select2({minimumResultsForSearch: 7});

        $("#myTabMeusPacotes a, #myTabMeusPacotesParametros a").click(function(t) {
            t.preventDefault();
            $(this).tab("show");
        });

        $('#search-customer').val(customer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        $('#meu-pacote-new-contato').html(data[0].text);
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#meu-pacote-new-contato').html(e.added.text);
        });

        $('#receptivo').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url+"suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            let endereco = e.added.address+' '+e.added.city+'/'+e.added.state+' CEP:'+e.added.postal_code+' - '+e.added.country;

            $('#fornecedorReceptivo').val(e.added.text);
            $('#telefoneReceptivo').val(e.added.phone);
            $('#enderecoReceptivo').val(endereco);
        });

        $('textarea').not('.skip').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function(e) {
                var editor = this.$editor.next('textarea');
                if($(editor).attr('required')){
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
                }
            }
        });

        $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        controllerSales.eventosCadastrarNovoCliente();
        controllerSalesCalculoTarifas.onLoad();

        $('#tipoContato').change(function (event) {

            let nomeDigitado = $('#slName').val() !== '' ?  $('#slName').val() : 'Novo Cliente';
            let nomePassageiro = $('#search-customer').select2('data').text;

            if ($(this).val() === 'criar') {
                $('#meu-pacote-new-contato').html(nomeDigitado);
                $('#view-novo-contato').show();
                $('#view-cliente-ja-cadastrado').hide();
            } else {
                $('#meu-pacote-new-contato').html(nomePassageiro);
                $('#view-novo-contato').hide();
                $('#view-cliente-ja-cadastrado').show();
            }
        });

        $('#slName').keyup(function(event){
            replaceLineStringNewContato($(this));
        });

        $('#slTable').hide();
        $('#adicionar-formulario-pacote-operadora').hide();
        $('#barra-remover').show();

        controllerSales.adicionarCamposObrigatorioAoFormulario();
    }

    function setCampoObrigatorio(atributo) {
        $('#'+atributo).attr("required", true);
    }

    function isCamposObrigatoriosPreenchidosAntesSalvarPacoteOperadora() {
        var customer =  $('#search-customer').val();
        var category =  $('#category').val();
        var supplierPacote =  $('#slSupplierPacote').val();

        return $('form[data-toggle="validator"]').data('bootstrapValidator').validate().isValid() &&
            customer !== '' &&
            category !== '' &&
            supplierPacote !== '';
    }

    function adicionarNovoItem(item) {

        if (!controllerSales.isPreencheuFilial() ) {
            return null;
        }

        if (item == null) return null;

        var item_id = site.settings.item_addition === 1 ? item.item_id : item.id;

        if (slitems[item_id]) {
            slitems[item_id].row.qty = parseFloat(slitems[item_id].row.qty) + 1;
        } else {
            slitems[item_id] = item;
        }

        slitems[item_id].order = new Date().getTime();

        slitems[item_id].supdate = 'I';

        localStorage.setItem('slitems', JSON.stringify(slitems));

        loadItems();

        limparDadosFormularioPacotes();

        //audio_success.play();

        return slitems[item_id];
    }

    function salvarPacoteOperadora() {

        if (!controllerSales.isPreencheuFilial() ) return;

        if (!isCamposObrigatoriosPreenchidosAntesSalvarPacoteOperadora()){
            bootbox.alert(lang.select_above);
            return false;
        }

        let mid = (new Date).getTime();

        let mcode = mid;
        let mqty = 1;
        let mtax = 0;

        let nomePassageiro = $('#search-customer').select2('data').text;
        let clienteId = $('#search-customer').val();

        let receptivo =  $('#receptivo').val();
        let nmReceptivo = '';

        if (receptivo !== '') {
            receptivo =  $('#receptivo').val();
            nmReceptivo =  $('#receptivo').select2('data').text;
        }

        let categoria = $('#category').select2('data').text;
        let categoriaId = $('#category').val();
        let origemPacote = $('#origemPacote').val();
        let destinoPacote = $('#destinoPacote').val();

        let nomePacote = $('#lsNomePacote').val();
        let tipoDestino = $('#slTipoDestino').val();
        let reservaPacote = $('#slReservaPacote').val();

        let emissaoPacote = $('#slEmissaoPacote').val();
        let dateCheckinPacote = $('#slDateCheckinPacote').val();
        let dateCheckOutPacote = $('#slDateCheckoutPacote').val();

        let fornecedor = $('#slSupplierPacote').val();
        let nmFornecedor = $('#slSupplierPacote').select2('data').text;

        let unit_price = getNumber($('#vlLiquidoCustomer').find('div').html());
        let mdiscount = getNumber($('#slTaxasAdicionaisDescontoCustomer').val());
        let comissao = getNumber($('#slComissaoConsultorAbsolutoSupplier').val());

        let tipoCobrancaFornecedorId = $('#tipoCobrancaFornecedorId').val();
        let condicaoPagamentoFornecedorId = $('#condicaoPagamentoFornecedorId').val();
        let vlTotalPagamentoFornecedorInput = $('#vlTotalPagamentoFornecedorInput').val();

        let nmEmpresaManual = $('#fornecedorReceptivo').val();
        let telefoneRecpetivo = $('#telefoneReceptivo').val();
        let enderecoReceptivo = $('#enderecoReceptivo').val();

        let mtax_rate = {};

        $.each(tax_rates, function () {
            if (this.id === mtax) {
                mtax_rate = this;
            }
        });

        slitems[mid] = {
            "id": mid,
            "item_id": mid,
            "label": nomePacote,
            "row": {
                "id": mid,
                "code": mcode,
                "name": nomePacote,
                "quantity": mqty,
                "price": unit_price,
                "unit_price": unit_price,
                "real_unit_price": unit_price,
                "tax_rate": mtax,
                "tax_method": 0,
                "qty": mqty,
                "type": "manual",
                "discount": String(mdiscount),
                "serial": "",
                "option": "",

                "customerClient" :clienteId,
                "customerClientName" : nomePassageiro,

                "tipoDestino" : tipoDestino,
                "reserva" : reservaPacote,

                "dataEmissao" : emissaoPacote,
                "dateCheckin" : dateCheckinPacote,
                "dateCheckOut" : dateCheckOutPacote,

                "receptivo" : receptivo,
                'nmReceptivo' : nmReceptivo,

                "nmEmpresaManual" : nmEmpresaManual,
                "telefoneEmpresa" : telefoneRecpetivo,
                "enderecoEmpresa" : enderecoReceptivo,

                "fornecedor" : fornecedor,
                'nmFornecedor' : nmFornecedor,

                "tipoCobrancaFornecedorId": tipoCobrancaFornecedorId,
                "condicaoPagamentoFornecedorId": condicaoPagamentoFornecedorId,
                "vlTotalPagamentoFornecedorInput": vlTotalPagamentoFornecedorInput,

                "comissao" : comissao,

                "category_name" : categoria,
                "categoria_id": categoriaId,

                "origem" : origemPacote,
                "destino" : destinoPacote,

                "digitado": '1',
            },
            "tax_rate": mtax_rate,
            "options": false
        };

        preencherTaxasCliente(slitems[mid]);
        preencherDadosCliente(slitems[mid]);

        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();

        exibirFormularioPacotesOperadora();
    }

    function preencherTaxasCliente(slitems) {

        //dados de clientes
        let currencieCustomer = $('#slCurrencieCustomer').val();
        let cambioCustomer = $('#slCambioCustomer').val();
        let tarifaCurrencieCustomer = $('#slTarifaCurrencieCustomer').val();
        let tarifaRealCustomer = $('#slTarifaRealCustomer').val();
        let taxaFornecedorCustomer = $('#slTaxaFornecedorCustomer').val();
        let taxasAdicionaisDescontoCustomer = $('#slTaxasAdicionaisDescontoCustomer').val();
        let taxasAdicionaisCartaoCustomer = $('#slTaxasAdicionaisCartaoCustomer').val();
        let vlPagamentoFornecedorCustomer = $('#slVlPagamentoFornecedorCustomer').val();
        let vlPagamentoFornecedorRealCustomer = $('#slVlPagamentoFornecedorRealCustomer').val();
        let formaPagamentoFornecedorCustomer = $('#slFormaPagamentoFornecedorCustomer').val();
        let bandeiraCartaoFornecedorCustomer = $('#slBandeiraCartaoFornecedorCustomer').val();
        let cambioFornecedorCustomer = $('#slCambioFornecedorCustomer').val();
        let nParcelasCustomer = $('#slNParcelasCustomer').val();
        let descricaoServico = $('#sldescricaoservico').val();
        let observacaoServico= $('#slobservacaoitem').val();

        let adicionalBagagem = $('#slTaxaBagagemCustomer').val();
        let adicionalReservaAssento = $('#slReservaAssentoCustomer').val();


        //dado da taxa de fornecedor
        let tarifaCurrencieSupplier = $('#slTarifaCurrencieSupplier').val();
        let currencieSupplier = $('#slCurrencieSupplier').val();
        let cambioSupplier = $('#slCambioSupplier').val();
        let tarifaRealSupplier = $('#slTarifaRealSupplier').val();
        let taxaSupplier = $('#slTaxaSupplier').val();
        let descontoAbsolutoSupplier = $('#slDescontoAbsolutoSupplier').val();
        let comissaoPercentualSupplier = $('#slComissaoPercentualSupplier').val();
        let comissaoAbsolutoSupplier = $('#slComissaoAbsolutoSupplier').val();
        let incentivoPercentualSupplier = $('#slIncentivoPercentualSupplier').val();
        let incentivoAbsolutoSupplier = $('#slIncentivoAbsolutoSupplier').val();
        let outrasTaxasSupplier = $('#slOutrasTaxasSupplier').val();
        let taxasCartaoSupplier = $('#slTaxasCartaoSupplier').val();
        let pagamentoFornecedorSupplier = $('#slPagamentoFornecedorSupplier').val();
        let comissaoConsultorPercentualSupplier = $('#slComissaoConsultorPercentualSupplier').val();
        let comissaoConsultorAbsolutoSupplier = $('#slComissaoConsultorAbsolutoSupplier').val();

        slitems.taxas = {
            "taxas_cliente": {

                "tarifa": tarifaCurrencieCustomer,
                "taxaAdicionalFornecedor" : taxaFornecedorCustomer,
                "adicionalBagagem" : adicionalBagagem,
                "adicionalReservaAssento" : adicionalReservaAssento,
                "descontoTarifa" : taxasAdicionaisDescontoCustomer,
                "acrescimoTarifa" : taxasAdicionaisCartaoCustomer,
                "valorPagoFornecedor" : vlPagamentoFornecedorCustomer,
                "formaPagamentoFornecedorCustomer" : formaPagamentoFornecedorCustomer,
                "bandeiraPagoFornecedor" : bandeiraCartaoFornecedorCustomer,
                "numeroParcelasPagoFornecedor" : nParcelasCustomer,
                "descricaoServicos" : descricaoServico,
                "observacaoServico" : observacaoServico,

                "currencieCustomer" : currencieCustomer,
                "cambioCustomer" : cambioCustomer,
                "tarifaRealCustomer" : tarifaRealCustomer,
                "vlPagamentoFornecedorRealCustomer" : vlPagamentoFornecedorRealCustomer,
                "cambioFornecedorCustomer" : cambioFornecedorCustomer,

            },
            "taxas_fornecedor": {
                "tarifaCurrencieSupplier" : tarifaCurrencieSupplier,
                "currencieSupplier" : currencieSupplier,
                "cambioSupplier" : cambioSupplier,
                "tarifaRealSupplier" : tarifaRealSupplier,
                "taxaSupplier" : taxaSupplier,
                "descontoAbsolutoSupplier" : descontoAbsolutoSupplier,
                "comissaoPercentualSupplier" : comissaoPercentualSupplier,
                "comissaoAbsolutoSupplier" : comissaoAbsolutoSupplier,
                "incentivoPercentualSupplier" : incentivoPercentualSupplier,
                "incentivoAbsolutoSupplier" : incentivoAbsolutoSupplier,
                "outrasTaxasSupplier" : outrasTaxasSupplier,
                "taxasCartaoSupplier" : taxasCartaoSupplier,
                "pagamentoFornecedorSupplier" : pagamentoFornecedorSupplier,
                "comissaoConsultorPercentualSupplier" : comissaoConsultorPercentualSupplier,
                "comissaoConsultorAbsolutoSupplier" : comissaoConsultorAbsolutoSupplier
            }
        }
    }

    function preencherDadosCliente(slitems) {

        let tipoContato = $('#tipoContato').val();
        let sexo = $('#sexo').val();
        let slName = $('#slName').val();
        let vat_no = $('#vat_no').val();
        let email = $('#email').val();
        let data_aniversario = $('#data_aniversario').val();
        let tipo_documento = $('#tipo_documento').val();
        let cf1 = $('#cf1').val();
        let cf3 = $('#cf3').val();
        let cf4 = $('#cf4').val();
        let cf5 = $('#cf5').val();
        let phone = $('#phone').val();
        let validade_rg_passaporte = $('#validade_rg_passaporte').val();
        let telefone_emergencia = $('#telefone_emergencia').val();
        let nQuarto = $('#nQuarto').val();
        let postal_code = $('#postal_code').val();
        let address = $('#address').val();
        let country = $('#country').val();
        let city = $('#city').val();
        let state = $('#state').val();

        slitems.dados_cliente = {
            "tipoContato" : tipoContato,
            "sexo" : sexo,
            "slName" : slName,
            "vat_no" : vat_no,
            "email" : email,
            "data_aniversario" : data_aniversario,
            "tipo_documento" : tipo_documento,
            "cf1" : cf1,
            "validade_rg_passaporte" :  validade_rg_passaporte,
            "cf3" : cf3,
            "cf4" : cf4,
            "phone" : phone,
            "cf5" : cf5,
            "telefone_emergencia" : telefone_emergencia,
            "nQuarto" : nQuarto,
            "postal_code" : postal_code,
            "address" : address,
            "country" : country,
            "city" : city,
            "state" : state
        }
    }

    function replaceLineStringNewContato(tag) {
        $('#meu-pacote-new-contato').html(tag.val());
    }

    function dataAtualFormatada(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();

        return  ano+'-'+mes+'-'+dia;
    }

    function limparDadosFormularioPacotes() {

        $('#form-meu-pacote').hide(600);
        $('#form-pacotes-operadoras').hide(600);
        $('#barra-remover').hide();
        $('#divDadosAereo').hide();
        $('#sticker').show();

        $('#pacotes-com-aereo').html('');
        $('#pacotes-com-hospedagem').html('');
        $('#pacotes-com-transfer').html('');

        $('#adicionar-formulario-pacote-operadora').show();
        $('#slTable').show();

        $('#slpacote-proprio').val('').select2();
        $('#pacoteId').val('').select2();
        $('#nVagas').val(0);
        $('#nReservas').val(0);

        $('#lsNomePacote').val('');
        $('#slReservaPacote').val('');
        $('#slEmissaoPacote').val('');
        $('#slDateCheckinPacote').val('');
        $('#slDateCheckoutPacote').val('');

        $('#load-new-pacote-operadora').html('');
        $('#load-new-meu-pacote').html('');

        controllerSales.adicionarCamposObrigatorioAoFormulario();

        /*
        $('#slSupplierPacote').val(OPERADORA).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url+"suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

         */
    }
}