<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;

class RatingsService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('model/Rating_model', 'Rating_model');
    }

    public function saveAll($avaliar = TRUE, $avaliar_dias = TRUE, $sale_id = NULL): array
    {
        try {

            $this->__iniciaTransacao();

            $ratingsArray   = [];

            if ($avaliar) {

                $itens  = $this->RatingRepository_model->getAllItensAvaliacao($avaliar_dias, $sale_id);

                foreach ($itens as $item) {

                    $verifica = $this->RatingRepository_model->naoEncontrouAvaliacao($item->sale_id, $item->customerClient);

                    if ($verifica) {
                        $rating = new Rating_model();
                        $uuid   = $this->generateUUID();

                        $rating->uuid           = $uuid;
                        $rating->sale_id        = $item->sale_id;
                        $rating->item_id        = $item->id;
                        $rating->customer_id    = $item->customerClient;
                        $rating->customer       = $item->customerClientName;
                        $rating->programacao_id = $item->programacaoId;
                        $rating->product_id     = $item->product_id;
                        $rating->answered       = false;//respondido
                        $rating->rating_private = false;//privado
                        $rating->publish        = false;//publicado
                        $rating->average        = 0;//media das notas
                        $rating->created_at     = date('Y-m-d H:i:s');

                        $rating_id = $this->save($rating);

                        $ratingsArray[] = $rating_id;
                    }
                }

            }

            $this->__confirmaTransacao();

            return $ratingsArray;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function save_rate($customer = 'Avaliação Anônima', $productID = NULL, $programacaoID = NULL )
    {
        try {
            $this->__iniciaTransacao();

            $rating = new Rating_model();
            $uuid   = $this->generateUUID();

            $rating->uuid           = $uuid;
            $rating->sale_id        = NULL;
            $rating->item_id        = NULL;
            $rating->customer_id    = 0;
            $rating->customer       = $customer;
            $rating->programacao_id = $programacaoID;
            $rating->product_id     = $productID;
            $rating->answered       = false;//respondido
            $rating->rating_private = false;//privado
            $rating->publish        = false;//publicado
            $rating->average        = 0;//media das notas
            $rating->created_at     = date('Y-m-d H:i:s');

            $rating_id = $this->save($rating);

            $this->__confirmaTransacao();

            return $rating_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function save($rating)
    {
        return $this->__save('ratings', $rating->__toArray());
    }

    public function save_responses($responses, $rating_private = false, $rating_id = NULL): bool
    {
        try {

            $this->__iniciaTransacao();
            $notas          = 0;
            $total_estrelas = 0;
            $average        = 0;

            foreach ($responses as $response) {

                $this->__save('rating_responses', $response->__toArray());

                if ($response->type_question == 'rating' || $response->type_question == 'rating_note') {//apenas computa quando for estrelas
                    $notas = $notas + $response->rating;
                    $total_estrelas++;
                }
            }

            if ($total_estrelas > 0) {
                $average = round($notas/$total_estrelas);
            }

            $data_rating = array(
                'answered'          => true,//respondido
                'average'           => $average,//media de estrelas
                'rating_private'    => $rating_private
            );

            if ($rating_private) {
                $data_rating['customer']    = 'Avaliação Anônima';
            }

            $this->db->update('ratings', $data_rating, array('id' => $rating_id));

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function generateUUID(): string
    {
        return Uuid::uuid4()->toString();
    }

}