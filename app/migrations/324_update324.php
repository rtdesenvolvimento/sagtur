<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update324 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '2021.3.24', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));
    }

    function alter_table_settings() {
        $fields = array(
            'exibirCancelamentosListaVenda' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE, 'default' => '0'),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
