<?php defined('BASEPATH') OR exit('No direct script access allowed');

class IndicatorsRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ARR_Total($ano)
    {
        $start_date = date($ano.'-01-01');
        $end_date = date($ano.'-12-31');

        $this->db->select("SUM(valorfatura) as totalRecebimentoAno");
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row()->totalRecebimentoAno;
        }

        return 0;
    }

    public function ARR_AgrupadoPorGrupo($ano): array
    {
        $start_date = date($ano.'-01-01');
        $end_date = date($ano.'-12-31');

        // Seleciona os campos desejados (Soma das faturas por customer_group)
        $this->db->select("c.customer_group_name, SUM(f.valorfatura) as totalRecebimentoGrupo, COUNT(DISTINCT c.id) as totalClientes");
        $this->db->from('sma_fatura f');
        $this->db->join('sma_companies c', 'f.pessoa = c.id and c.active = 1');
        $this->db->where("f.status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("f.tipooperacao", 'CREDITO');
        $this->db->where("f.dtVencimento >=", $start_date);
        $this->db->where("f.dtVencimento <=", $end_date);
        $this->db->group_by('c.customer_group_id');  // Agrupa por customer_group

        $q = $this->db->get();

        $labels = [];
        $dados = [];
        $total_clientes = 0;

        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $labels[] = $row->customer_group_name;
                $dados[] = (float) $row->totalRecebimentoGrupo;
                $total_clientes += $row->totalClientes;
            }
        }

        return [
            'labels' => $labels,
            'dados' => $dados,
            'total_clientes' => $total_clientes
        ];
    }

    public function ARR_Grafico($ano): array
    {
        $firstDay = "{$ano}-01-01";
        $lastDay = "{$ano}-12-31";

        $dataPrevisto = array();

        $previsto = $this->db->query("
            SELECT DATE_FORMAT(t.data, '%Y-%m') AS mes, 
            sum(f.valorfatura) AS totalFaturas, 
            SUM(CASE 
                    WHEN c.data_aniversario BETWEEN '{$firstDay}' AND '{$lastDay}' 
                         AND MONTH(c.data_aniversario) = MONTH(f.dtvencimento) 
                         AND YEAR(c.data_aniversario) = YEAR(f.dtvencimento) 
                    THEN f.valorfatura 
                    ELSE 0 
                END) AS novosContratos 
            From sma_tempo t 
                LEFT OUTER JOIN sma_fatura f ON f.dtvencimento = t.data AND f.status IN ('ABERTA', 'PARCIAL', 'QUITADA') AND f.tipooperacao = 'CREDITO' 
                LEFT OUTER JOIN sma_companies c ON c.id = f.pessoa
            WHERE t.data >= '" . $firstDay. "' AND t.data <= '" . $lastDay . "'  
            GROUP by DATE_FORMAT(t.data, '%Y-%m')
            ORDER by t.data");

        if ($previsto->num_rows() > 0) {
            foreach (($previsto->result()) as $row) {
                $mes = $row->mes;

                $dataPrevisto[$mes]['arr'] = (double) $row->totalFaturas;
                $dataPrevisto[$mes]['novosContratos'] = (double) $row->novosContratos;

            }
        }

        return $dataPrevisto;
    }

    public function activeCustomers()
    {
        $this->db->select('count(*) as total');
        $this->db->from('sma_companies');
        $this->db->where('active', 1);

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->row()->total - 1;
        }

        return 0;
    }

    public function inactiveCustomers()
    {
        $this->db->select('count(*) as total');
        $this->db->from('sma_companies');
        $this->db->where('active', 0);

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->row()->total - 1;
        }

        return 0;
    }

    public function customersLastYear($ano)
    {
        $firstDay = "{$ano}-01-01";
        $lastDay = "{$ano}-12-31";

        $lastYearDate = date('Y-m-d', strtotime('-1 year', strtotime($firstDay)));
        $currentDate = date('Y-m-d', strtotime('-1 year', strtotime($lastDay)));

        $this->db->select('COUNT(*) as total');
        $this->db->from('companies');
        $this->db->where('data_aniversario >=', $lastYearDate);
        $this->db->where('data_aniversario <=', $currentDate);

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->row()->total;
        }
        return 0;
    }
}