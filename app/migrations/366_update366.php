<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update366 extends CI_Migration {

    public function up() {

        $this->create_table_autentique();
        $this->create_table_folder_group();
        $this->create_table_folder();
        $this->create_table_document();
        $this->create_table_document_history();
        $this->create_table_archive();
        $this->create_table_signatures();
        $this->insert_folder();
        $this->insert_folder_group();

        $this->db->update('settings',  array('version' => '3.6.6'), array('setting_id' => 1));
    }

    function create_table_folder_group() {//grupo de pasta pasta
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('folder_group', TRUE, $attributes);
    }

    function create_table_folder() {//pasta
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11 ),
            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'public_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('folder', TRUE, $attributes);
    }

    function create_table_document() {//documento
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'document_status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 ),
            'folder_id' => array('type' => 'INT', 'constraint' => 11 ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'hidden' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'file_name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'url_link' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document', TRUE, $attributes);
    }

    function create_table_document_history() {//pasta
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),//data de criacao
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('document_history', TRUE, $attributes);
    }

    function create_table_signatures() {//assinatura
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),

            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'action' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),//data de criacao
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'url_link' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'viewd' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_viewd' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data visto o link

            'signed' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_signature' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data da assinatura

            'rejected' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date_rejected' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//data da assinatura

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'public_id' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('signatures', TRUE, $attributes);
    }

    function create_table_archive() {//arquivos
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'folder_group_id' => array('type' => 'INT', 'constraint' => 11 ),
            'folder_id' => array('type' => 'INT', 'constraint' => 11 ),
            'document_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'url_link' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('archive', TRUE, $attributes);
    }

    public function create_table_autentique() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'token' => array('type' => 'INT', 'constraint' => 11 ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('autentique', TRUE, $attributes);

        $data_autentique = array(
            'token' => '#',
            'active' => 0,
            'note' => 'Integração com assinatura digital',
        );

        $this->db->insert('autentique', $data_autentique);
    }

    public function insert_folder() {
        $data_folder = array(
            'folder_group_id' => 1,
            'hidden' => 0,
            'name' => 'Meus Documentos',
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
            'note' => 'Criado pelo sistema'
        );

        $this->db->insert('folder', $data_folder);
    }

    public function insert_folder_group() {

        $data_folder_group = array(
            'hidden' => 1,
            'name' => 'ROOT',
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
            'note' => 'Criado pelo sistema'
        );

        $this->db->insert('folder_group', $data_folder_group);
    }

    public function down() {}
}
