<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaViagem_model extends CI_Model
{
    public $produto;
    public $vagas;
    public $preco;
    public $active = true;

    public $percentualMaximoDesconto;
    public $sinalMinimoPagamento;
    public $dataSaida;
    public $dataRetorno;
    public $horaSaida;
    public $horaRetorno;
    public $produtoModel;

    public $habilitar_marcacao_area_cliente;
    public $permitir_marcacao_dependente;
    public $data_inicio_marcacao;
    public $data_final_marcacao;

    public $agenda_programacao_id;

    //somente leitura
    private $totalVendasFaturas;
    private $totalVendasPendentes;
    private $totalListaEspera;
    private $totalOrcamento;
    private $totalCancelados;

    private $totalDisponvel;

    private $controleEstoqueHospedagem;

    private $quartos = [];

    public function __construct() {
        parent::__construct();
    }

    public function adicionarQuarto($quarto, $tipo_hospedagem) {
        $this->quartos[(int) $tipo_hospedagem] = $quarto;
    }

    /**
     * @return array
     */
    public function getQuartos(): array
    {
        return $this->quartos;
    }

    /**
     * @param array $quartos
     */
    public function setQuartos(array $quartos): void
    {
        $this->quartos = $quartos;
    }



    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getVagas()
    {
        return $this->vagas;
    }

    /**
     * @param mixed $vagas
     */
    public function setVagas($vagas)
    {
        $this->vagas = $vagas;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    /**
     * @return mixed
     */
    public function getPercentualMaximoDesconto()
    {
        return $this->percentualMaximoDesconto;
    }

    /**
     * @param mixed $percentualMaximoDesconto
     */
    public function setPercentualMaximoDesconto($percentualMaximoDesconto)
    {
        $this->percentualMaximoDesconto = $percentualMaximoDesconto;
    }

    /**
     * @return mixed
     */
    public function getSinalMinimoPagamento()
    {
        return $this->sinalMinimoPagamento;
    }

    /**
     * @param mixed $sinalMinimoPagamento
     */
    public function setSinalMinimoPagamento($sinalMinimoPagamento)
    {
        $this->sinalMinimoPagamento = $sinalMinimoPagamento;
    }

    /**
     * @return mixed
     */
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    /**
     * @param mixed $dataSaida
     */
    public function setDataSaida($dataSaida)
    {
        $this->dataSaida = $dataSaida;
    }

    /**
     * @return mixed
     */
    public function getDataRetorno()
    {
        return $this->dataRetorno;
    }

    /**
     * @param mixed $dataRetorno
     */
    public function setDataRetorno($dataRetorno)
    {
        $this->dataRetorno = $dataRetorno;
    }

    /**
     * @return mixed
     */
    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    /**
     * @param mixed $horaSaida
     */
    public function setHoraSaida($horaSaida)
    {
        $this->horaSaida = $horaSaida;
    }

    /**
     * @return mixed
     */
    public function getHoraRetorno()
    {
        return $this->horaRetorno;
    }

    /**
     * @param mixed $horaRetorno
     */
    public function setHoraRetorno($horaRetorno)
    {
        $this->horaRetorno = $horaRetorno;
    }

    /**
     * @return mixed
     */
    public function getTotalVendasFaturas()
    {
        return $this->totalVendasFaturas;
    }

    /**
     * @param mixed $totalVendasFaturas
     */
    public function setTotalVendasFaturas($totalVendasFaturas)
    {
        $this->totalVendasFaturas = $totalVendasFaturas;
    }

    /**
     * @return mixed
     */
    public function getTotalVendasPendentes()
    {
        return $this->totalVendasPendentes;
    }

    /**
     * @param mixed $totalVendasPendentes
     */
    public function setTotalVendasPendentes($totalVendasPendentes)
    {
        $this->totalVendasPendentes = $totalVendasPendentes;
    }

    /**
     * @return mixed
     */
    public function getTotalDisponvel()
    {
        return $this->totalDisponvel;
    }

    /**
     * @param mixed $totalDisponvel
     */
    public function setTotalDisponvel($totalDisponvel)
    {
        $this->totalDisponvel = $totalDisponvel;
    }

    /**
     * @return mixed
     */
    public function getTotalListaEspera()
    {
        return $this->totalListaEspera;
    }

    /**
     * @param mixed $totalListaEspera
     */
    public function setTotalListaEspera($totalListaEspera)
    {
        $this->totalListaEspera = $totalListaEspera;
    }

    /**
     * @return mixed
     */
    public function getTotalOrcamento()
    {
        return $this->totalOrcamento;
    }

    /**
     * @param mixed $totalOrcamento
     */
    public function setTotalOrcamento($totalOrcamento)
    {
        $this->totalOrcamento = $totalOrcamento;
    }

    /**
     * @return mixed
     */
    public function getTotalCancelados()
    {
        return $this->totalCancelados;
    }

    /**
     * @param mixed $totalCancelados
     */
    public function setTotalCancelados($totalCancelados): void
    {
        $this->totalCancelados = $totalCancelados;
    }

    /**
     * @return mixed
     */
    public function getControleEstoqueHospedagem()
    {
        return $this->controleEstoqueHospedagem;
    }

    /**
     * @param mixed $controle_estoque_hospedagem
     */
    public function setControleEstoqueHospedagem($controle_estoque_hospedagem): void
    {
        $this->controleEstoqueHospedagem = $controle_estoque_hospedagem;
    }

    /**
     * @return mixed
     */
    public function getHabilitarMarcacaoAreaCliente()
    {
        return $this->habilitar_marcacao_area_cliente;
    }

    /**
     * @param mixed $habilitar_marcacao_area_cliente
     */
    public function setHabilitarMarcacaoAreaCliente($habilitar_marcacao_area_cliente): void
    {
        $this->habilitar_marcacao_area_cliente = $habilitar_marcacao_area_cliente;
    }

    /**
     * @return mixed
     */
    public function getPermitirMarcacaoDependente()
    {
        return $this->permitir_marcacao_dependente;
    }

    /**
     * @param mixed $permitir_marcacao_dependente
     */
    public function setPermitirMarcacaoDependente($permitir_marcacao_dependente): void
    {
        $this->permitir_marcacao_dependente = $permitir_marcacao_dependente;
    }

    /**
     * @return mixed
     */
    public function getDataInicioMarcacao()
    {
        return $this->data_inicio_marcacao;
    }

    /**
     * @param mixed $data_inicio_marcacao
     */
    public function setDataInicioMarcacao($data_inicio_marcacao): void
    {
        $this->data_inicio_marcacao = $data_inicio_marcacao;
    }

    /**
     * @return mixed
     */
    public function getDataFinalMarcacao()
    {
        return $this->data_final_marcacao;
    }

    /**
     * @param mixed $data_final_marcacao
     */
    public function setDataFinalMarcacao($data_final_marcacao): void
    {
        $this->data_final_marcacao = $data_final_marcacao;
    }

    /**
     * @return mixed
     */
    public function getAgendaProgramacaoId()
    {
        return $this->agenda_programacao_id;
    }

    /**
     * @param mixed $agenda_programacao_id
     */
    public function setAgendaProgramacaoId($agenda_programacao_id): void
    {
        $this->agenda_programacao_id = $agenda_programacao_id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }


}