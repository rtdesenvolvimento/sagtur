<div class="clearfix"></div>
<?= '</div></div></div></td></tr></table></div></div>'; ?>
<div class="clearfix"></div>
<footer>
    <!--
    <div class="fab">
        <button class="main">
        </button>
        <ul>
            <li>
                <label for="opcao1">AJUDA DO SISTEMA</label>
                <button id="opcao1">
                    <i class="fa fa-question"></i>
                </button>
            </li>
        </ul>
    </div>

    <script>
        function toggleFAB(fab){
            if(document.querySelector(fab).classList.contains('show')){
                document.querySelector(fab).classList.remove('show');
            }else{
                document.querySelector(fab).classList.add('show');
            }
        }

        document.querySelector('.fab .main').addEventListener('click', function(){
            toggleFAB('.fab');
        });

        document.querySelectorAll('.fab ul li button').forEach((item)=>{
            item.addEventListener('click', function(){
                window.location = '<?= site_url('help'); ?>';
            });
        });
    </script>
    !-->

<a href="#" id="toTop" class="blue" style="position: fixed; bottom: 30px; right: 30px; font-size: 30px; display: none;">
    <i class="fa fa-chevron-circle-up"></i>
</a>
    <p style="text-align:right;">&copy; <?= date('Y') . " SAGTur | Licenciado para " . $Settings->site_name; ?> (v<?= $Settings->version; ?>) <?php if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
            echo ' - Page rendered in <strong>{elapsed_time}</strong> seconds';
        } ?> | <img src="<?php echo base_url();?>/assets/images/sag-footer.png" width="7%">
    </p>
</footer>
<?= '</div>'; ?>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>
<div class="modal fade in" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>
<div class="modal fade in" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>
<div id="modal-loading" style="display: none;">
    <div class="blackbg"></div>
    <div class="loader"></div>
</div>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
<script type="text/javascript">
var dt_lang = <?=$dt_lang?>, dp_lang = <?=$dp_lang?>, site = <?=json_encode(array('base_url' => base_url(), 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
var lang = {
        BLOQUEADO: '<?=lang('BLOQUEADO');?>',
        CANCELADA: '<?=lang('CANCELADA');?>'  ,
        ABERTA: '<?=lang('ABERTA');?>'  ,
        AGUARDANDO_PAGAMENTO : '<?=lang('AGUARDANDO_PAGAMENTO');?>',
        PAGO: '<?=lang('PAGO');?>',
        PARCIAL: '<?=lang('PARCIAL');?>',
        VENCIDA: '<?=lang('VENCIDA');?>',
        QUITADA: '<?=lang('paid');?>',

        paid: '<?=lang('paid');?>',
        orcamento: '<?=lang('orcamento');?>' ,
        faturada: '<?=lang('faturada');?>' ,
        billed: '<?=lang('billed');?>' ,
        lista_espera: '<?=lang('lista_espera');?>' ,

        reembolso: '<?=lang('reembolso');?>' ,
        cancel: '<?=lang('cancel');?>',pending: '<?=lang('pending');?>',
        completed: '<?=lang('completed');?>',
        ordered: '<?=lang('ordered');?>',
        received: '<?=lang('received');?>',
        partial: '<?=lang('partial');?>',
        sent: '<?=lang('sent');?>',
        r_u_sure: '<?=lang('r_u_sure');?>',
        due: '<?=lang('due');?>',
        returned: '<?=lang('returned');?>',
        transferring: '<?=lang('transferring');?>',
        active: '<?=lang('active');?>',
        inactive: '<?=lang('inactive');?>',
        unexpected_value: '<?=lang('unexpected_value');?>',
        select_above: '<?=lang('select_above');?>'
    };
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.dtFilter.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script>
<!--<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>!-->
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>

<!--Core JS !-->
<script type="text/javascript" src="<?= $assets ?>js/core6.js"></script>

<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/maskphone.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/masked.js"></script>

<?= ($m == 'roomlist' && ($v == 'montar' || $v == 'montar_old')) ||
    ($m == 'system_settings' && ($v == 'index')) ||
    ($m == 'sales' && ($v == 'add' || $v == 'edit')) ||
    ($m == 'ServicoAdicional' && ($v == 'add' || $v == 'edit')) ||
    ($m == 'products' && ($v == 'add' || $v == 'edit' || $v == 'duplicar')) ||
    ($m == 'tours' && ($v == 'add' || $v == 'edit' || $v == 'duplicar')) ? '<script type="text/javascript" src="' . $assets . 'js/jquery-ui2.min.js"></script>' : ''; ?>

<script type="text/javascript" src="<?= $assets ?>toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/alerts.js"></script>

<?= ($m == 'purchases' && ($v == 'add' || $v == 'edit' || $v == 'purchase_by_csv')) ? '<script type="text/javascript" src="' . $assets . 'js/purchases.js"></script>' : ''; ?>
<?= ($m == 'transfers' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/transfers.js"></script>' : ''; ?>

<!-- Sales JS !-->
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/sales.9.min.js"></script>' : ''; ?>

<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSales.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSalesPacote.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSaleAereo.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSaleHospedagem.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSaleTransfer.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales/controllerSalesCalculoTarifas.js"></script>' : ''; ?>
<?= ($m == 'quotes' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes.js"></script>' : ''; ?>
<?= ($m == 'products' && ($v == 'add' || $v == 'edit' || $v == 'duplicar')) ? '<script type="text/javascript" src="' . $assets . 'js/products.js"></script>' : ''; ?>
<?= ($m == 'tours' && ($v == 'add' || $v == 'edit' || $v == 'duplicar')) ? '<script type="text/javascript" src="' . $assets . 'js/products.js"></script>' : ''; ?>

<script type="text/javascript" charset="UTF-8">var r_u_sure = "<?=lang('r_u_sure')?>";
    <?=$s2_file_date?>

    $.extend(true, $.fn.dataTable.defaults, {"oLanguage":<?=$dt_lang?>});
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;

    $(window).load(function () {

        $('.mm_<?=$m?>').addClass('active');
        $('.mm_<?=$m?>').find("ul").first().slideToggle();
        $('#<?=$m?>_<?=$v?>').addClass('active');
        $('.mm_<?=$m?> a .chevron').removeClass("closed").addClass("opened");

        if ($.cookie('sma_style')  === undefined) {
            $.cookie('sma_style', 'light', { path: '/' });
            cssStyle();
        }

        $('#myModal3').on('hidden.bs.modal', function () {
            $(this).find('.modal-dialog').empty();
            $(this).removeData('bs.modal');
            $('#myModal').css('zIndex', '1050');
            $('#myModal').css('overflow-y', 'scroll');
        });

        $('#myModal4').on('hidden.bs.modal', function () {
            $(this).find('.modal-dialog').empty();
            $(this).removeData('bs.modal');
            $('#myModal').css('zIndex', '1050');
            $('#myModal').css('overflow-y', 'scroll');
        });
    });

    function copyTextToClipboard(text, clipper_pacote) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }

        document.body.removeChild(textArea);
        $('.'+clipper_pacote).show();
    }
</script>
</body>
</html>
