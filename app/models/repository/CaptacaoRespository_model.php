<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CaptacaoRespository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($id) {
        $q = $this->db->get_where('captacao', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function read($id)
    {
        return $this->db->update('captacao', array('read' => 1), array('id' => $id));
    }
}