<style>
    .panel-info {
        transition: box-shadow 0.3s ease-in-out;
    }

    .panel-info:hover {
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
    }

    .folder {
        padding: 35px;
        border: 1px solid #ddd;
        box-shadow: none;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .status_documento {
        font-size: 1.0rem;
    }
    .sigantarios {
        color: #585f73;
        padding: 2px;
        font-size: 1.0rem;
        font-weight: 400;
    }

</style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-files-o"></i><?= lang('my_documents'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('contracts/addDocument'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-plus"></i> <?= lang('add_document') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown" style="display: none;">
                    <a href="<?= site_url('contracts') ?>" class="toggle_up" title="<?= lang("view_documents_list") ?>">
                        <i class="icon fa fa-reorder"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">

        <div class="row">
            <div class="col-md-12" style="margin-bottom: 25px;">
                <div id="form">
                    <div  class="col-sm-3">
                        <?= lang("status_document", "flStatusDocument") ?>
                        <?php
                        $cbStatus = array(
                            '' => lang('select'),
                            'draft' => lang('draft'),
                            'unsigned' => lang('unsigned'),
                            'pending' => lang('pending'),
                            'completed' => lang('completed'),
                            'canceled' => lang('canceled'),
                        );
                        echo form_dropdown('flStatusDocument', $cbStatus,  '', 'class="form-control" id="flStatusDocument"'); ?>
                    </div>
                    <div class="col-sm-7">
                        <?= lang("product", "filter_programacao_id"); ?>
                        <?php
                        $pgs[""] = lang('select').' '.lang('product');
                        echo form_dropdown('filter_programacao_id', $pgs,  '', 'class="form-control" id="filter_programacao_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" style="width: 100%;margin-top: 25px;" data-toggle="modal" data-target="#filterModal"><i class="fa fa-search"></i> <?=$this->lang->line("filter_product");?></button>
                    </div>

                    <div class="col-sm-3">
                        <?= lang("data_venda_de", "data_venda_de"); ?>
                        <?php echo form_input('flDataVendaDe', '', 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                    </div>
                    <div class="col-sm-3">
                        <?= lang("data_venda_ate", "data_venda_ate"); ?>
                        <?php echo form_input('flDataVendaAte', '', 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                    </div>
                    <div class="col-sm-3">
                        <?= lang("biller", "billerFilter"); ?>
                        <?php
                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->name;
                        }
                        echo form_dropdown('flBiller', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>

                    <div class="col-sm-3">
                        <?= lang("signatory", "flSignatory"); ?>
                        <?php echo form_input('flSignatory', '', 'class="form-control" id="flSignatory" placeholder="' . lang("search_signatory") . '" '); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="grid_search"></div>
            </div>

            <div class="col-md-12" style="text-align: center;margin-top: 25px;">
                <div class="form-group">
                    <?php echo form_button('load_more',  $this->lang->line("load_more_documents"), 'id="load_more" class="btn btn-primary"'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Filter Pacote-->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filtros de Pacote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('filter_status_pacote', $opts,  'Confirmado', 'class="form-control" id="filter_status_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('filter_ano_pacote', $opts,  date('Y'), 'class="form-control" id="filter_ano_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('filter_mes_pacote', $opts,  date('m'), 'class="form-control" id="filter_mes_pacote"'); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button"  id="applyFilters" class="btn btn-primary">Aplicar Filtros</button>
            </div>
        </div>
    </div>
</div>

<script language="javascript">

    var limit = 0;

    $(document).ready(function () {

        load_grid_search();
        buscarProdutos();

        $('#load_more').click(function () {

            limit += 48;

            load_grid_search();
        });

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });

        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $('#applyFilters').click(function() {
            buscarProdutos();
        });

        $('#flStatusDocument').change(function() {
            load_grid_search();
        });

        $('#filter_programacao_id').change(function() {
            load_grid_search();
        });

        $('#billerFilter').change(function() {
            load_grid_search();
        });

        $('#flDataVendaDe').change(function() {
            load_grid_search();
        });

        $('#flDataVendaAte').change(function() {
            load_grid_search();
        });

        $('#flSignatory').on('input', debounce(load_grid_search, 300)); // 300ms de atraso
    });

    function load_grid_search() {

        var searchTerm = $('#search_term').val();
        var statusDocument = $('#flStatusDocument').val();
        var programacaoID = $('#filter_programacao_id').val();
        var billerFilter = $('#billerFilter').val();
        var flDataVendaDe = $('#flDataVendaDe').val();
        var flDataVendaAte = $('#flDataVendaAte').val();
        var flSignatory = $('#flSignatory').val();

        var url = '<?= base_url('contracts/grid_search') ?>?search_term=' + encodeURIComponent(searchTerm) +
            '&statusDocument=' + encodeURIComponent(statusDocument) +
            '&programacaoID=' + encodeURIComponent(programacaoID) +
            '&billerID=' + encodeURIComponent(billerFilter) +
            '&dataVendaDe=' + encodeURIComponent(flDataVendaDe) +
            '&dataVendaAte=' + encodeURIComponent(flDataVendaAte) +
            '&strSignatory=' + encodeURIComponent(flSignatory) +
            '&limit=' + limit;

        $('#grid_search').load(url, function () {

            $('.documents').click(function (event) {

                if ($(event.target).closest('.box-icon').length > 0) {
                    if ($(event.target).is('a')) {
                        return true;
                    } else {
                        return;
                    }
                }

                var document_id = $(this).attr('document_id');
                var status = $(this).attr('status');

                if (status === 'draft') {
                   window.location.href = '<?=base_url('contracts/addSignatories/')?>' + document_id;
                } else {
                   window.location.href = '<?=base_url('contracts/monitorContractSignature/')?>' + document_id;
                }
            });

            $('.dropdown').on('show.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
            });
            $('.dropdown').on('hide.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
            });

            $('.tip').tooltip();
        });
    }

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#filter_status_pacote').val(),
                ano: $('#filter_ano_pacote').val(),
                mes: $('#filter_mes_pacote').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {

                $('#filter_programacao_id').empty();
                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#filter_programacao_id').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#filter_programacao_id').append(option);
                });

                $('#filter_programacao_id').select2({minimumResultsForSearch: 7});
                $('#filterModal').modal('hide');
            }
        });
    }

    function debounce(func, wait) {
        let timeout;
        return function(...args) {
            const later = () => {
                clearTimeout(timeout);
                func.apply(this, args);
            };
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    }

</script>

