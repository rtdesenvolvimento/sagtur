<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('account', $this->Settings->user_language);
    }

    public function index() {

        $cnpj_origem = $this->getCnpj_origem();
        $db = $this->load->database('sagtur_administrativo', TRUE);
        $customer = $this->getCompanyByID($db, $cnpj_origem);

        $this->data['customer'] = $customer;
        $this->data['group'] = $this->getCustomerGroupByID($db, $customer->customer_group_id);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('accounts')));
        $meta = array('page_title' => lang('accounts'), 'bc' => $bc);
        $this->page_construct('accounts/index', $meta, $this->data);
    }

    public function receipt_pdf($id = null, $view = null)
    {
        $db = $this->load->database('sagtur_administrativo', TRUE);

        $this->data['fatura'] = $this->getFaturaById($db, $id);

        if ($view) {
            $this->load->view($this->theme . 'accounts/receipt_pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'accounts/receipt_pdf', $this->data, true);
            $name = lang("payment") . "_" . str_replace('/', '_', $id) . ".pdf";
            $this->sma->generate_pdf($html, $name, false);
        }
    }

    function getFaturaById($db, $faturaId){
        $db->select('fatura.*, tipo_cobranca.name as tipo_cobranca');
        $db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');

        $db->where('fatura.id',$faturaId);
        $db->limit(1);
        return $db->get('fatura')->row();
    }

    public function getCompanyByID($db, $cnpj_origem) {
        $q = $db->get_where('companies', array('vat_no' => $this->formatar_cpf_cnpj($cnpj_origem)), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getCustomerGroupByID($db, $id) {
        $q = $db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getNextCharges() {

        $cnpj_origem = $this->getCnpj_origem();
        $this->load->library('datatables');

        $this->datatables->set_database('sagtur_administrativo');

        $action = "<div class=\"text-center\"> 
                    <a class=\"tip\" title='" . lang("pagar_agora") . "' href='" . ('$1') . "' target='_blank'>
                        <button type='button' class='btn btn-primary'><i class=\"fa fa-usd\"></i> " . lang("pagar_agora") . "</button>
                    </a> 
                   </div>";

        $this->datatables
            ->select("{$this->db->dbprefix('fatura_cobranca')}.link as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('fatura')}.valorpagar,
                {$this->db->dbprefix('companies')}.customer_group_name, 
                {$this->db->dbprefix('fatura')}.status,
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'CREDITO');

        $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA')");
        $this->datatables->where("{$this->db->dbprefix('companies')}.vat_no", $this->formatar_cpf_cnpj($cnpj_origem));

        $this->datatables->order_by('dtVencimento');

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getBillingHistory() {

        $cnpj_origem = $this->getCnpj_origem();
        $this->load->library('datatables');
        $this->datatables->set_database('sagtur_administrativo');

        $action = "<div class=\"text-center\"> 
                    <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('account/receipt_pdf/$1') . "' target='_blank'>
                        <button type='button' class='btn btn-primary'><i class=\"fa fa-usd\"></i> ". lang("recibo") ."</button>
                    </a> 
                   </div>";

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtultimopagamento,
                {$this->db->dbprefix('fatura')}.valorfatura,
                {$this->db->dbprefix('companies')}.customer_group_name, 
                {$this->db->dbprefix('fatura')}.status")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'CREDITO');

        $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('QUITADA')");
        $this->datatables->where("{$this->db->dbprefix('companies')}.vat_no", $this->formatar_cpf_cnpj($cnpj_origem));

        $this->datatables->order_by('dtultimopagamento desc');

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    private function getCnpj_origem() {
        $cnpj_origem = $this->session->userdata('cnpj_origem');
        return $cnpj_origem;
    }

    private function formatar_cpf_cnpj($doc) {

        $doc = preg_replace("/[^0-9]/", "", $doc);
        $doc = str_replace("-", "", $doc);
        $doc = str_replace(".", "", $doc);
        $doc = trim($doc);

        $qtd = strlen($doc);

        if($qtd >= 11) {

            if($qtd === 11 ) {

                $docFormatado = substr($doc, 0, 3) . '.' .
                    substr($doc, 3, 3) . '.' .
                    substr($doc, 6, 3) . '-' .
                    substr($doc, 9, 2);
            } else {
                $docFormatado = substr($doc, 0, 2) . '.' .
                    substr($doc, 2, 3) . '.' .
                    substr($doc, 5, 3) . '/' .
                    substr($doc, 8, 4) . '-' .
                    substr($doc, -2);
            }

            return $docFormatado;

        } else {
            return $doc;
        }
    }

}