<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Integracaositetrendytravel extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');
        $this->load->model('integracaositetrendytravel_model');
        $this->load->model('sales_model');
        $this->load->model('products_model');
    }

    function importPedidosSite() {

        $pedidos = $this->integracaositetrendytravel_model->getAllPedidos();
        foreach ($pedidos as $pedido) {

            $post_id            = $pedido->ID;
            $post_status        = $pedido->post_status;
            $sale               = $this->site->isVendaImportada($post_id);

            if (!$sale) {

                //customer
                $customer_id        = $this->integracaositetrendytravel_model->getPassageiro($post_id);
                $customer_details   = $this->site->getCompanyByID($customer_id);
                $customer           = $customer_details->name.'(compra site)';
                $plano_saude        = $customer_details->plano_saude;
                $alergia_medicamento= $customer_details->alergia_medicamento;
                $doenca_informar    = $customer_details->doenca_informar;
                $telefone_emergencia= $customer_details->telefone_emergencia;


                //biller
                $biller_id      = 3;
                $biller_details = $this->site->getCompanyByID($biller_id);
                $biller         = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;

                $date           = $pedido->post_date;
                $reference      = $this->site->getReference('so');
                $note           = $pedido->post_excerpt;

                if($plano_saude != '') {
                    $note .= '<br/>Tem plano de saúde? Qual plano?<br/>'.$plano_saude;
                }

                if($alergia_medicamento != '') {
                    $note .= '<br/>Alergia a algum Medicamento?<br/>'.$alergia_medicamento;
                }

                if($doenca_informar != '') {
                    $note .= '<br/>Alguma Doença pré-existente a informar?<br/>'.$doenca_informar;
                }

                if($telefone_emergencia != '') {
                    $note .= '<br/>Telefone para emergência<br/>'.$telefone_emergencia;
                }

                $order_item_id  = $this->integracaositetrendytravel_model->getWoocommerceOrderItems($post_id);
                $_line_subtotal = $this->integracaositetrendytravel_model->getWoocommerceOrderItemmetaByType($order_item_id, '_line_subtotal');
                $_line_total    = $this->integracaositetrendytravel_model->getWoocommerceOrderItemmetaByType($order_item_id, '_line_total');
                $_qty           = $this->integracaositetrendytravel_model->getWoocommerceOrderItemmetaByType($order_item_id, '_qty');
                $_product_id    = $this->integracaositetrendytravel_model->getWoocommerceOrderItemmetaByType($order_item_id, '_product_id');
                $local_embarque = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_billing_local_embarque');

                //$_line_total = 0;

                if (!$_line_subtotal) {
                    $_line_subtotal = 0;
                }

                $produtcs       = $this->site->getProductByProductSite($_product_id);
                $warehouse      = $this->site->getWarehouseByCode($produtcs->code);
                $percentual_desconto_a_vista = $produtcs->percentual_desconto_a_vista;

                $origem = $produtcs->origem;
                $origem = $origem.'*DEMAIS EMBARQUES (Informe o local de embarque nas observações do pedido (requer confirmação))';
                $origem = trim($origem);
                $origem = str_replace('</p>','', $origem);
                $origem = str_replace('<p>','', $origem);
                $origem = explode(';',$origem);

                $warehouse_id   = $warehouse->id;

                $passagem_option = $this->site->getProductVariantsPassagem($produtcs->id);

                unset($products);

                $_payment_method_title      = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_payment_method_title');
                $_payment_method            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_payment_method');
                $url_pagamento              = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'URL de pagamento.');
                $parcelas                   = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Parcelas');
                $metodo_pagamento           = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Método de pagamento');
                $tipo_pagamento             = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Tipo de pagamento');
                $nome_comprador             = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Nome do comprador');
                $email_comprador            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'E-mail do comprador');
                $_transaction_id            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_transaction_id');
                $_paid_date                 = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_paid_date');
                $_customer_ip_address       = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_customer_ip_address');

                $sale_status    = '';
                $payment_status = '';
                $payment        = array();

                if ($_payment_method == 'pagseguro'
                    && $tipo_pagamento != '' &&
                    ($post_status == 'wc-processing' || $post_status == 'wc-completed' )) {

                    $payment_status = 'paid';
                    $paid_by        = '';

                    if ($tipo_pagamento == 'Cartão de Crédito') {
                        $paid_by = 'CC';
                    }

                    if ($tipo_pagamento == 'Cartão de Débito') {
                        $paid_by = 'other';
                    }

                    if ($tipo_pagamento == 'Boleto') {
                        $paid_by = 'cash';
                    }

                    $notePagamento  = '<b>Tipo de pagamento:</b>'.$tipo_pagamento.'</br>';
                    $notePagamento .= '<b>Método de pagamento:</b>'.$metodo_pagamento.'</br>';
                    $notePagamento .= '<b>Número Parcelas:</b>'.$parcelas.'</br>';
                    $notePagamento .= '<b>URL Pagamento:</b>'.$url_pagamento.'</br>';
                    $notePagamento .= '<b>Nome do comprador:</b>'.$nome_comprador.'</br>';
                    $notePagamento .= '<b>Email Comprador:</b>'.$email_comprador.'</br>';
                    $notePagamento .= '<b>Identificador da Transação:</b>'.$_transaction_id.'</br>';
                    $notePagamento .= '<b>IP do Comprador:</b>'.$_customer_ip_address.'</br>';

                    $payment = array(
                        'date'          => $_paid_date,
                        'reference_no'  => $this->site->getReference('pay'),
                        'amount'        => $this->sma->formatDecimal($_line_subtotal),
                        'paid_by'       => $paid_by,
                        'created_by'    => $this->session->userdata('user_id'),
                        'note'          => $notePagamento,
                        'type'          => 'received',
                    );


                    $this->integracaositetrendytravel_model->atualizarStatusPedidoCompleto($post_id);
                }

                //aguardando pagamento
                //pagamento em transferencia aguardando.
                //reembolso
                if ($post_status == 'wc-pending' ||
                    $post_status == 'wc-on-hold'  ||
                    $post_status == 'wc-refunded') {

                    $sale_status    = 'pending';
                    $payment_status = 'pending';
                }

                if ($post_status == 'wc-completed'
                    || $post_status == 'wc-processing') {//pagamento  completo
                    $sale_status    = 'completed';
                    $payment_status = 'paid';
                }

                if ("Depósito ou Transferência bancária/DOC" == $_payment_method_title) {
                    if ($percentual_desconto_a_vista > 0) {
                        $perc = ($percentual_desconto_a_vista/100);
                        $totalDescontoAVista = ($_line_total*$perc);
                        $_line_total = $_line_total - $totalDescontoAVista;
                    }
                }

                $desconto = ($_line_subtotal - $_line_total);

                if ($desconto < 0) {
                    $desconto = 0;
                }

                $sale = array(
                    'date' => $date,
                    'reference_no' => $reference,
                    'customer_id' => $customer_id,
                    'customer' => $customer,
                    'biller_id' => $biller_id,
                    'biller' => $biller,
                    'vencimento'=> date('Y-m-d'),
                    'local_saida' => $origem[$local_embarque],
                    'condicao_pagamento' => $_payment_method_title,
                    'note' => 'METODO DE PAGAMENTO::'.$_payment_method_title.'<br/>'.$note,
                    'total' => $this->sma->formatDecimal($_line_total),
                    'product_discount'=>$desconto,
                    'total_discount'=>$desconto,
                    'shipping' => 0,
                    'grand_total' => $this->sma->formatDecimal($_line_total),
                    'total_items' => $_qty,
                    'sale_status' => $sale_status,
                    'payment_status' => $payment_status,
                    'warehouse_id' => $warehouse_id,
                    'paid' => 0,
                    'created_by' => $this->session->userdata('user_id'),
                    'order_site_id' => $post_id
                );

                $total =  $this->sma->formatDecimal($_line_total/$_qty);

                $products[] = array(
                    'product_id' => $produtcs->id,
                    'option_id' => $passagem_option->id,
                    'product_code' => $produtcs->code,
                    'product_name' => $produtcs->name,
                    'product_type' => $produtcs->type,
                    'customerClient' => $customer_id,
                    'customerClientName' => $customer,
                    'net_unit_price' => $total,
                    'unit_price' => $total,
                    'quantity' => 1,
                    'warehouse_id' => $warehouse_id,
                    'subtotal' => $total,
                    'real_unit_price' => $this->sma->formatDecimal($_line_subtotal/$_qty),
                    'discount'=>$desconto,
                    'item_discount'=>$desconto
                );

                $this->add_despesas_viagem($produtcs->code,
                    $warehouse_id,
                    $customer,
                    1,
                    '',
                    $customer_id,
                    $customer);


                for ($r = 2; $r <= $_qty; $r++) {

                    //customer
                    $passageiroAdicional_id         = $this->integracaositetrendytravel_model->getPassageiro($post_id, $r);
                    $passageiroAdicional_details    = $this->site->getCompanyByID($passageiroAdicional_id);
                    $nomePassageiroAdicional        = $passageiroAdicional_details->name;

                    $products[] = array(
                        'product_id' => $produtcs->id,
                        'option_id' => $passagem_option->id,
                        'product_code' => $produtcs->code,
                        'product_name' => $produtcs->name,
                        'product_type' => $produtcs->type,
                        'customerClient' => $passageiroAdicional_id,
                        'customerClientName' => $nomePassageiroAdicional,
                        'net_unit_price' => $total,
                        'unit_price' => $total,
                        'quantity' => 1,
                        'warehouse_id' => $warehouse_id,
                        'subtotal' => $total,
                        'real_unit_price' => $total,
                    );
                }

                print_r($products);

                $sale_id = $this->sales_model->addSaleReturnId($sale, $products, $payment);

                if (count($payment) > 0) {
                    $this->enviarEmailConfirmacaoPedido($sale_id);
                }

                $this->add_despesas_viagem($produtcs->code,
                    $warehouse_id,
                    $customer,
                    1,
                    '',
                    $customer_id,
                    $nomePassageiroAdicional);

            } else if($post_status == 'wc-cancelled') {
                $this->site->atualizarSaleStatusCancel($sale->id);
            } else if ($post_status == 'wc-refunded') {
                $this->site->atualizarSaleStatusReembolso($sale->id);
            } else if ($post_status == 'wc-on-hold' && $sale->payment_status == 'paid') {
                $this->integracaositetrendytravel_model->atualizarStatusPedidoCompleto($post_id);
                $this->site->atualizarSaleStatusCompleted($sale->id);
                $this->enviarEmailConfirmacaoPedido($sale->id);
            } else if ($post_status == 'wc-completed' &&
                ($sale->payment_status == 'due' || $sale->payment_status == 'pending') ){
                $this->integracaositetrendytravel_model->atualizarStatusPedidoAguardando($post_id);
            }
        }

        $this->atualizarStatusPagamento();
        $this->atualizarEstoqueSite();
    }

    function atualizarStatusPagamento() {

        $sales =  $this->site->getAllSalesPaymentPending();

        foreach ($sales as $sale) {

            $post_id = $sale->order_site_id;

            $pedido         =  $this->integracaositetrendytravel_model->getPedidoById($post_id);
            $statusPedido   = $pedido->post_status;

            $_payment_method            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_payment_method');
            $url_pagamento              = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'URL de pagamento.');
            $parcelas                   = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Parcelas');
            $metodo_pagamento           = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Método de pagamento');
            $tipo_pagamento             = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Tipo de pagamento');
            $nome_comprador             = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'Nome do comprador');
            $email_comprador            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, 'E-mail do comprador');
            $_transaction_id            = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_transaction_id');
            $_paid_date                 = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_paid_date');
            $_customer_ip_address       = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_customer_ip_address');
            $_order_total               = $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_order_total');

            if ($tipo_pagamento == '')  $tipo_pagamento =  $this->integracaositetrendytravel_model->getInfoPedidoByType($post_id, '_payment_method_title');

            if ($tipo_pagamento != ''
                && $_paid_date != '' &&
                ($statusPedido == 'wc-processing' || $statusPedido == 'wc-completed' ) ){

                $paid_by        = '';

                if ($tipo_pagamento == 'Cartão de Crédito') {
                    $paid_by = 'CC';
                }

                if ($tipo_pagamento == 'Cartão de Débito') {
                    $paid_by = 'other';
                }

                if ($tipo_pagamento == 'Boleto') {
                    $paid_by = 'cash';
                }

                $notePagamento = '<b>Tipo de pagamento:</b>'.$tipo_pagamento.'</br>';
                $notePagamento .= '<b>Método de pagamento:</b>'.$metodo_pagamento.'</br>';
                $notePagamento .= '<b>Número Parcelas:</b>'.$parcelas.'</br>';
                $notePagamento .= '<b>URL Pagamento:</b>'.$url_pagamento.'</br>';
                $notePagamento .= '<b>Nome do comprador:</b>'.$nome_comprador.'</br>';
                $notePagamento .= '<b>Email Comprador:</b>'.$email_comprador.'</br>';
                $notePagamento .= '<b>Identificador da Transação:</b>'.$_transaction_id.'</br>';
                $notePagamento .= '<b>IP do Comprador:</b>'.$_customer_ip_address.'</br>';

                $payment = array(
                    'date'          => $_paid_date,
                    'sale_id'       => $sale->id,
                    'reference_no'  => $this->site->getReference('pay'),
                    'amount'        => $this->sma->formatDecimal($_order_total),
                    'paid_by'       => $paid_by,
                    'created_by'    => $this->session->userdata('user_id'),
                    'note'          => $notePagamento,
                    'type'          => 'received',
                );

                print_r($payment);

                $this->sales_model->addPayment($payment);
                $this->site->atualizarSaleStatusCompleted($sale->id);
                $this->integracaositetrendytravel_model->atualizarStatusPedidoCompleto($post_id);

                $this->enviarEmailConfirmacaoPedido($sale->id);

            } else if($statusPedido == 'wc-cancelled') {
                $this->site->atualizarSaleStatusCancel($sale->id);
            } else if ($statusPedido == 'wc-refunded') {
                $this->site->atualizarSaleStatusReembolso($sale->id);
            }
        }
    }

    function atualizarEstoqueSite() {
        $products = $this->site->getAllProductsDisponivelSite();
        foreach ($products as $product) {
            $sale               = $this->site->getCountSalesByProducts($product->id);
            $name               = $product->name;
            $warehouses         = $this->site->getWarehouseByCode($product->code);
            $product_options    = $this->products_model->getProductOptionsWithWarehouseProducts($warehouses->id, $product->id);

            $numeroVendas   = 0;
            if ($sale->stock) {
                $numeroVendas = $sale->stock;
            }

            $estoque = 0;
            foreach ($product_options as $option) {
                if ($option->name == 'Passagem') {
                    $estoque = (int) $option->wh_qty;
                }
            }
            echo $name.' - '.$numeroVendas.' estoque original = '.$estoque.' estoque atual = '.($estoque-$numeroVendas).'<br/>';
            $this->integracaositetrendytravel_model->atualizarEstoque($product->id,($estoque-$numeroVendas) );
        }
    }

     public function add_despesas_viagem($item_code, $warehouse_id,  $customer=null, $item_quantity=1, $poltronaClient=null,$customerClient=null, $customerClientName=null) {

        $product_details 	= $this->sales_model->getProductByCode($item_code);
        $products_variacoes = $this->sales_model->getProductOptions($product_details->id,  $warehouse_id , true);

        foreach ($products_variacoes as $products_variacao) {

            unset($products);
            unset($data);

            $nome_variacao 		= $products_variacao->name;
            $fornecedor 		= $products_variacao->fornecedor;
            $variacao 			= $this->sales_model->getVariantByName($nome_variacao);

            if(is_array($variacao)) {

                $lancamento_type = $variacao[0]->lancamento_type;

                if ($lancamento_type == 2) {

                    if ($products_variacao->price) {
                        $totalPreco = $products_variacao->price;
                    } else {
                        $totalPreco = 0.00;
                    }

                    $reference = $this->site->getReference('po');
                    $date = date('Y-m-d H:i:s');

                    if ($fornecedor > 0) {
                        $supplier_id = $fornecedor;
                        $supplier_details = $this->site->getCompanyByID($fornecedor);
                        $supplier = $supplier_details->name . ' (' . $nome_variacao . ')';
                    } else {
                        $supplier_id = 622;//fixo para um fornecedor configurado como despesas de viagem
                        $supplier = "Despesas de Viagem";
                    }

                    $status = 'pending';
                    $total = 0;

                    $unit_cost = $totalPreco;
                    $unit_cost = $this->sma->formatDecimal($unit_cost);
                    $item_net_cost = $unit_cost;
                    $subtotal = ($item_net_cost * $item_quantity);

                    if ($customerClientName != '' &&
                        $customerClientName != 'undefined' &&
                        $customerClientName != '0') {
                        $customer = $customerClientName;
                    }

                    $products[] = array(
                        'product_code'      => $item_code,
                        'poltronaClient'    => $poltronaClient,
                        'customerClient'    => $customer,
                        'customerClientName'=> $customer,
                        'product_id'        => $product_details->id,
                        'product_name'      => $customer,//nome do cliente como produto
                        'net_unit_cost'     => $item_net_cost,
                        'option_id'         => $products_variacao->id,
                        'unit_cost'         => $this->sma->formatDecimal($item_net_cost),
                        'quantity'          => $item_quantity,
                        'quantity_balance'  => $item_quantity,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'real_unit_cost'    => $totalPreco,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                    );

                    $total += ($item_net_cost * $item_quantity);

                    if (empty($products)) {
                        $this->form_validation->set_rules('product', lang("order_items"), 'required');
                    } else {
                        krsort($products);
                    }

                    $grand_total = $this->sma->formatDecimal($total);
                    $data = array(
                        'reference_no'  => $reference,
                        'date'          => $date,
                        'supplier_id'   => $supplier_id,
                        'supplier'      => $supplier,
                        'warehouse_id'  => $warehouse_id,
                        'total'         => $this->sma->formatDecimal($total),
                        'grand_total'   => $grand_total,
                        'status'        => $status,
                        'created_by'    => $this->session->userdata('user_id'),
                    );

                    $q = $this->db->get_where('purchases',
                        array(
                            'supplier_id'   => $supplier_id,
                            'warehouse_id'  => $warehouse_id),
                        1);

                    if ($q->num_rows() > 0) {
                        $purchases = $q->row();
                        $this->sales_model->updatePurchase($purchases->id, $data, $products);
                    } else {
                        $this->sales_model->addPurchase($data, $products);
                    }
                }
            }
        }
    }

    function enviarEmailConfirmacaoPedido($id=null) {

        $inv = $this->sales_model->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }

        $customer = $this->site->getCompanyByID($inv->customer_id);
        $this->load->library('parser');
        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
        );

        $to         = $customer->email;
        $subject    = 'Confirmação de passagem '.$this->Settings->site_name;
        $cc         = null;
        $bcc        = null;

        $msg        = '<h3>{logo}</h3>
                        <h4>Detalhes da Passagem.</h4>
                        <p>
                            Olá {contact_person} agradecemos a sua compra. Este é o e-mail de confirmação.
                        </p>
                        <p>
                            Em anexo os dados da sua passagem ({reference_number}).
                        </p>
                        <p>
                            <a href="{client_link}"></a>
                        </p>
                        <p>
                            Atenciosamente,
                            <br>
                            {site_name}
                        </p>';
        $message    = $this->parser->parse_string($msg, $parse_data);

        echo $message;

        $attachment = $this->pdf($id, null, 'S');

        $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $return = $this->sales_model->getReturnBySID($id);
        $this->data['return_sale'] = $return;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;
        //$this->data['paypal'] = $this->sales_model->getPaypalSettings();
        //$this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

}
