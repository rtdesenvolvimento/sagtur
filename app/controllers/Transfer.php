<?php defined('BASEPATH') or exit('No direct script access allowed');

class Transfer extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');

        $this->lang->load('transfer', $this->Settings->user_language);
    }

    public function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('transfer')));
        $meta = array('page_title' => lang('transfer'), 'bc' => $bc);
        $this->page_construct('transfer/index', $meta, $this->data);
    }

    function getTransfers()
    {

        $detail_link = anchor('transfer/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $detail_link . '</li>
			<li><a href="' . site_url('transfer/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>';

        $action .= '<li><a href="' . site_url() . 'assets/uploads/$2" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i> '
            . lang('view_image') . '</a></li>
			</ul>
		</div></div>';
        $this->load->library('datatables');

        $this->datatables
            ->select(
                  $this->db->dbprefix('products') . ".id as productid, "
                . $this->db->dbprefix('products') . ".image as image, "
                . $this->db->dbprefix('products') . ".code as code, "
                . $this->db->dbprefix('products') . ".name as name", FALSE)
            ->from('products')
            ->group_by("products.id");

        $this->db->where('viagem', 0);

        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    function add()
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run() == true) {

            $data = array(
                'code' => rand(1000,100000),
                'barcode_symbology' => 'ean13',
                'viagem' => 0,
                'type' => 'standard',
                'unit' => 'ATIVO',
                'name' => $this->input->post('name'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'margem' => 0,
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->sma->fsd($this->input->post('start_date')),
                'end_date' => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
            );
        }

        $transportes = [];
        $locaisEmbarque = [];
        $tiposCobranca = [];
        $faixaEtariaValor = [];
        $faixaEtariaValoresServicoAdicional = [];

        if ($this->form_validation->run() == true && $this->products_model->addProduct(
                $data,
                $items,
                $warehouse_qty,
                $transportes,
                $product_attributes ,
                $locaisEmbarque,
                $tiposCobranca,
                $faixaEtariaValor,
                $faixaEtariaValoresServicoAdicional,
                $photos ,
                [],
                [],
                [])) {
            $this->session->set_flashdata('message', lang("product_added"));
            redirect('ServicoAdicional');
        } else {

            $this->data['error']        = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('transfer'), 'page' => lang('transfer')),
                array('link' => '#', 'page' => lang('add_transfer')));

            $meta = array('page_title' => lang('add_transfer'), 'bc' => $bc);
            $this->page_construct('transfer/add', $meta, $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->load->helper('security');

        if ($this->input->post('id')) $id = $this->input->post('id');

        $product = $this->site->getProductByID($id);

        if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run('products/add') == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'margem' => 0,
                'viagem' => 0,
                'unit' => $this->input->post('unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->sma->fsd($this->input->post('start_date')),
                'end_date' => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
            );

        }

        $locaisEmbarque = [];
        $tiposCobranca = [];
        $faixaEtariaValor = [];
        $faixaEtariaValoresServicoAdicional = [];

        if ($this->form_validation->run() == true &&
            $this->products_model->updateProduct(
                $id,
                $data,
                $items,
                $warehouse_qty,
                $product_attributes,
                $locaisEmbarque,
                $tiposCobranca,
                $faixaEtariaValor,
                $faixaEtariaValoresServicoAdicional,
                $photos,
                $update_variants,
                [],
                [],
                []
            )
        ) {
            $this->session->set_flashdata('message', lang("product_updated"));
            redirect('transfer');
        } else {

            $this->data['error']    = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['product']  = $product;

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('ServicoAdicional'), 'page' => lang('ServicoAdicional')), array('link' => '#', 'page' => lang('edit_product')));
            $meta = array('page_title' => lang('edit_product'), 'bc' => $bc);
            $this->page_construct('transfer/edit', $meta, $this->data);
        }
    }

}