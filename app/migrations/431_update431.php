<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update431 extends CI_Migration {

    public function up() {

        $this->create_table_motivo_cancelamento();

        $this->alter_table_sales();
        $this->alter_table_tipo_cobranca();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.3.1'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'motivo_cancelamento_padrao_id' => array('type' => 'INT', 'constraint' => 1, 'default' => null),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->insert('motivo_cancelamento', array('name' => 'Cancelado pelo Sistema. Não realizou o pagamento da primeira parcela/sinal dentro do prazo.', 'internal' => 1));
        $motivo_id = $this->db->insert_id();

        $this->db->update('settings',  array('motivo_cancelamento_padrao_id' => $motivo_id), array('setting_id' => 1));
    }

    function create_table_motivo_cancelamento() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'obriga_observacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'internal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'gerar_credito' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('motivo_cancelamento', TRUE, $attributes);

        $this->db->insert('motivo_cancelamento', array('name' => 'Não realizou o pagamento da reserva dentro do prazo do vencimento/sinal.'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Desistiu da compra dentro do prazo de cancelamento'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Mudanças de planos pessoais ou familiares'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Emergências médicas'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Condições meteorológicas extremas que afetam a segurança da viagem'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Cancelamento de voos ou outras formas de transporte'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Restrições de viagem devido a eventos políticos ou de segurança'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Problemas financeiros inesperados'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Mudanças nas políticas de visto ou imigração'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Problemas no destino, como instabilidade política ou desastres naturais'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Conflitos de agenda inesperados'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Falta de participantes em uma viagem em grupo'));
        $this->db->insert('motivo_cancelamento', array('name' => 'Outros Motivos', 'obriga_observacao' => 1));
    }

    public function alter_table_sales() {
        $fields = array(
            'motivo_cancelamento_id' => array('type' => 'INT', 'constraint' => 1, 'default' => null),
            'motivo_cancelamento' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_tipo_cobranca() {
        $fields = array(
            'numero_dias_cancelamento' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
            'automatic_cancellation_sale' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('tipo_cobranca', $fields);
    }

    public function down() {}
}
