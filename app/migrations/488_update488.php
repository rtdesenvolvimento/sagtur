<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update488 extends CI_Migration {

    public function up() {

        $this->update_shop_settings();

        $this->db->update('settings',  array('version' => '4.8.8'), array('setting_id' => 1));
    }

    public function update_shop_settings()
    {
        $this->db->update('shop_settings',  array('web_page_caching' => 1), array('id' => 1));
        $this->db->update('shop_settings',  array('cache_minutes' => 300), array('id' => 1));//5 hora
        $this->db->update('shop_settings',  array('max_packages_line' => 50), array('id' => 1));
    }

    public function down() {}
}
