<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update403 extends CI_Migration {

    public function up() {

        $this->db->update('settings',  array('version' => '4.0.3', 'site_width' => 332), array('setting_id' => 1));
    }

    public function down() {}
}
