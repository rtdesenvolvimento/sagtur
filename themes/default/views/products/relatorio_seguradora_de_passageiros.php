<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>

<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
            <?php
            $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
            $dataRetorno = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataRetorno);

            $nomeViagem = strtoupper($product->name).' <b>Saída:</b> '.$data.' <b>Retorno:</b> '.$dataRetorno;
            ?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
            <h4>RELATÓRIO DE PASSAGEIROS PARA SEGURADORA</h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
            <h5>Destino: <?=$nomeViagem?></h5>
        </td>
    </tr>
    </thead>
</table>
<table border="0" style="width: 100%;">
    <thead>
     <tr>
        <th align="left" width="35%">Passageiro</th>
        <th align="center" width="15%">CPF</th>
        <th align="center" width="15%">R.G</th>
         <th align="center" width="10%">&nbsp;Tel.&nbsp;</th>
         <th align="center" width="15%">&nbsp;E-mail&nbsp;</th>
         <th align="center" width="10%">&nbsp;Nascimento&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $contador = 1;
    $isColorBackground = false;

    foreach ($itens as $row) {

            $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);

            $customer                = $objCustomer->name;
            $cpf                     = $objCustomer->vat_no;
            $rg                      = $objCustomer->cf1;
            $orgaoEmissor           = $objCustomer->cf3;
            $data_aniversario        = $objCustomer->data_aniversario;

            $email                  = $objCustomer->email;
            $whatsApp               = $objCustomer->cf5;
            $phone                  = $objCustomer->phone;
            $telefone_emergencia    = $objCustomer->telefone_emergencia;
            $contato = '';

            if ($whatsApp) {
                $contato = $whatsApp;
            }

            if ($phone) {
                $contato = $contato.' '.$phone;
            }

            if ($data_aniversario) {
                $data_aniversario = $this->sma->hrsd($data_aniversario);
            }

            $background = "#eee";

            if ($isColorBackground) {
                $background = "#eee";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }

            ?>
            <tr style="background: <?=$background;?>">
                <td align="left">&nbsp;<?php echo $customer;?></td>
                <td align="center"><?php echo $cpf;?></td>
                <td align="center"><?php echo $rg.'/'.$orgaoEmissor;?></td>
                <td align="center"><?php echo $contato;?></td>
                <td align="center"><?php echo $email;?></td>
                <td align="center"><?php echo $data_aniversario;?></td>
            </tr>

    <?php }?>
    </tbody>
</table>

</body>
</html>
