<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParcelaService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('model/Parcela_model', 'Parcela_model');
        $this->load->model('model/ContaReceber_model', 'ContaReceber_model');

        $this->load->model('Gifts_model');
    }

    public function reembolso($parcelaId) {
        $parcela = $this->__getById('parcela', $parcelaId);
        $this->__update('parcela', array('valorpagar' => 0, 'status' => Parcela_model::STATUS_REEMBOLSO), 'id', $parcelaId);

        if ($parcela->contareceberId != null)  $this->__update('conta_receber', array('status' => Parcela_model::STATUS_REEMBOLSO), 'id', $parcela->contareceberId);
    }

    public function cancelar($parcelaId, $cancelamentoViaVenda = NULL) {

        $parcela = $this->__getById('parcela', $parcelaId);

        $this->__update('parcela', array('status' => Parcela_model::STATUS_CANCELADA), 'id', $parcelaId);

        if ($parcela->contareceberId != null) {
            $this->cancelarConta('conta_receber', $parcela->contareceberId, $cancelamentoViaVenda);
        }

        if ($parcela->contapagarId != null) {
            $this->cancelarConta('conta_pagar', $parcela->contapagarId, $cancelamentoViaVenda);

            $conta_pagar = $this->__getById('conta_pagar', $parcela->contapagarId);

            if ($conta_pagar->gift_cards_id) {
                $this->Gifts_model->cancelGiftCard($conta_pagar->gift_cards_id);
            }

        }
    }

    public function cancelar_com_lancamento_credito($parcelaId) {

        $parcela = $this->__getById('parcela', $parcelaId);

        $this->__update('parcela', array('valor' => $parcela->valorpago, 'valorpagar' => 0, 'valorpago'=> $parcela->valorpago, 'status' => Parcela_model::STATUS_QUITADA), 'id', $parcelaId);

        if ($parcela->contareceberId != null) {
            $this->__update('conta_receber', array('status' => ContaReceber_model::STATUS_QUITADA), 'id', $parcela->contareceberId);
        }
    }

    public function cancelarConta($tipo, $id, $cancelamentoViaVenda = NULL) {

        if ($tipo == 'conta_receber' && $cancelamentoViaVenda != null && $this->isContaVinculadaAhUmaVenda($tipo, $id)) {
            throw new Exception('Não é possível cancelar uma conta a receber vinculada a uma venda, cancele a venda e todo financeiro será cancelado junto automaticamente.');
        }

        $this->__update($tipo, array('status' => Parcela_model::STATUS_CANCELADA), 'id', $id);
    }

    public function isContaVinculadaAhUmaVenda($tipo, $id) {
       return $this->__getById($tipo, $id)->sale > 0;
    }
}
