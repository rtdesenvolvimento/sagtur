<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Malote_model extends CI_Model
{

    public $date;
    public $status = 'Aberto';

    public $dt_fechamento;
    public $tipo_cobranca_id;
    public $forma_pagamento_id;

    public $movimentador_id;

    public $total;
    public $total_pago;

    public $note = '';
    public $created_by;
    public $created_at;

    public $updated_by;
    public $updated_at;

    private $faturas;

    public function __construct() {
        parent::__construct();
    }

    public function addFatura($fatura) {
        $this->faturas[count($this->faturas) + 1] = $fatura;
    }

    /**
     * @return mixed
     */
    public function getFaturas()
    {
        return $this->faturas;
    }


}
