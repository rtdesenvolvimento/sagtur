<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_vehicle'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("vehicle/add", $attrib)
                ?>
                <div class="col-md-6">
                    <?= lang("tipo_veiculo", "tipo_veiculo") ?>
                    <?php
                    $catTiposVeiculo[''] = lang("select") . " " . lang("tipo_veiculo") ;
                    foreach ($tiposVeiculo as $tipoVeiculo) {
                        $catTiposVeiculo[$tipoVeiculo->id] = $tipoVeiculo->name;
                    }
                    echo form_dropdown('tipo_veiculo', $catTiposVeiculo, (isset($_POST['tipo_veiculo']) ? $_POST['tipo_veiculo'] : ($vehicle ? $vehicle->tipo_veiculo : '')), 'class="form-control select" id="tipo_veiculo" placeholder="' . lang("select") . " " . lang("tipo_veiculo") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <div class="col-md-6">
                    <?= lang("categoria", "categoria") ?>
                    <?php
                    $catCategorias[''] = lang("select") . " " . lang("categoria") ;
                    foreach ($categoriasVeiculo as $categoria) {
                        $catCategorias[$categoria->id] = $categoria->name;
                    }
                    echo form_dropdown('categoria', $catCategorias, (isset($_POST['categoria']) ? $_POST['categoria'] : ($vehicle ? $vehicle->categoria : '')), 'class="form-control select" id="categoria" placeholder="' . lang("select") . " " . lang("categoria") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <div class="col-md-10">
                    <div class="form-group all">
                        <?= lang("name", "name") ?>
                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($vehicle ? $vehicle->name : '')), 'class="form-control" id="name" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("placa", "placa") ?>
                        <?= form_input('placa', (isset($_POST['placa']) ? $_POST['placa'] : ($vehicle ? $vehicle->placa : '')), 'class="form-control" id="placa" required="required"'); ?>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("marca", "marca") ?>
                        <?= form_input('marca', (isset($_POST['marca']) ? $_POST['marca'] : ($vehicle ? $vehicle->marca : '')), 'class="form-control" id="marca" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("modelo", "modelo") ?>
                        <?= form_input('modelo', (isset($_POST['modelo']) ? $_POST['modelo'] : ($vehicle ? $vehicle->modelo : '')), 'class="form-control" id="modelo" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("cor", "cor") ?>
                        <?= form_input('cor', (isset($_POST['cor']) ? $_POST['cor'] : ($vehicle ? $vehicle->cor : '')), 'class="form-control" id="cor" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("ano", "ano") ?>
                        <?= form_input('ano', (isset($_POST['ano']) ? $_POST['ano'] : ($vehicle ? $vehicle->ano : '')), 'class="form-control" id="ano"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("municipio", "municipio") ?>
                        <?= form_input('municipio', (isset($_POST['municipio']) ? $_POST['municipio'] : ($vehicle ? $vehicle->municipio : '')), 'class="form-control" id="municipio"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group all">
                        <?= lang("uf", "uf") ?>
                        <?= form_input('uf', (isset($_POST['uf']) ? $_POST['uf'] : ($vehicle ? $vehicle->uf : '')), 'class="form-control" id="uf"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group all">
                        <?= lang("chassi", "chassi") ?>
                        <?= form_input('chassi', (isset($_POST['chassi']) ? $_POST['chassi'] : ($vehicle ? $vehicle->chassi : '')), 'class="form-control" id="chassi"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <?= lang("tipo_combustivel", "tipo_combustivel") ?>
                    <?php
                    $catTiposCombustivel[''] = lang("select") . " " . lang("tipo_combustivel") ;
                    foreach ($tiposCombustivel as $tipoCombustivel) {
                        $catTiposCombustivel[$tipoCombustivel->id] = $tipoCombustivel->name;
                    }
                    echo form_dropdown('tipo_combustivel', $catTiposCombustivel, (isset($_POST['tipo_combustivel']) ? $_POST['tipo_combustivel'] : ($vehicle ? $vehicle->tipo_combustivel : '')), 'class="form-control select" id="tipo_combustivel" placeholder="' . lang("select") . " " . lang("tipo_combustivel") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("capacidade_min", "capacidade_min"); ?>
                        <?php echo form_input('capacidade_min', '0', 'class="form-control input-tip mask_integer" id="capacidade_min"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("capacidade_max", "capacidade_max"); ?>
                        <?php echo form_input('capacidade_max', '0', 'class="form-control input-tip mask_integer" id="capacidade_max"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("km_atual", "km_atual"); ?>
                        <?php echo form_input('km_atual', '0', 'class="form-control input-tip mask_integer" id="km_atual"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group all">
                        <?php echo form_checkbox('cambio_automatico', '1', FALSE, 'id="cambio_automatico"'); ?>
                        <label for="attributes" class="padding05"><?= lang('cambio_automatico'); ?></label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><i class="fa-fw fa fa-photo"></i> <?= lang("vehicle_gallery", "vehicle_gallery") ?></div>
                        <div class="panel-body">
                            <div class="col-md-12 alert-info">
                                <h3><i class="fa fa-info-circle"></i> Atencão</h3>
                                <ul>
                                    <li>Adicione imagens nos formatos (jpg, jpeg ou png) com no máximo 1MB de Tamanho.</li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group all" id="div_product_details">
                                    <div class="form-group all">
                                        <?= lang("photo", "photo") ?>
                                        <input id="photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="photo" data-show-upload="false"
                                               data-show-preview="false" accept="image/*" class="form-control file">
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("vehicle_gallery_images", "images") ?>
                                        <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                                               data-show-preview="false" class="form-control file" accept="image/*">
                                    </div>
                                    <div id="img-details" ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("note", "note"); ?>
                        <?php echo form_textarea('note', '', 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_submit('add_vehicle', $this->lang->line("add_vehicle"), 'id="add_vehicle" class="btn btn-primary"'); ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
</script>