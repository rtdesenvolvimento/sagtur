<?php defined('BASEPATH') or exit('No direct script access allowed');

class Saleitem extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //repository
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');

    }

    function getValoresPorFaixaEtaria() {

        $productId      = $this->input->get('productId');
        $programacaoId  = $this->input->get('programacaoId');
        $retorno        = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($productId);

        if ($retorno == null) {
            $this->sma->send_json([]);
        } else  {
            if ($this->Settings->receptive) {
                $product = $this->site->getProductByID($productId);
                if ($product->cat_precificacao == 'preco_por_data') {
                    $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
                    foreach ($retorno as $data) {
                        $data->text = $data->name.' '.$this->sma->formatMoney($programacao->preco);
                        $data->valor = $programacao->preco;
                    }
                }
            }
            $this->sma->send_json($retorno);
        }
    }

    function getFaixasTipoHospedagemProduto() {
        $productId = $this->input->get('productId');

        $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getFaixasTipoHospedagemProduto($productId);

        $this->sma->send_json($valoresPorTipoHospedagem);
    }

    function getLocaisEmbarque() {

        $productId  = $this->input->get('productId');
        $product    = $this->site->getProductByID($productId);
        $isPasseio  = $product->passeio;//TODO EXIBE TODOS OS EMBARQUE PARA PACOTES DO RECEPTIVO

        if ($isPasseio) {
            $this->sma->send_json($this->site->getLocaisEmbarque());
        } else {
            $this->sma->send_json($this->ProdutoRepository_model->getLocaisEmbarque($productId));
        }
    }

    function getTransportes() {
        $productId = $this->input->get('productId');
        $this->sma->send_json($this->ProdutoRepository_model->getTransportes($productId));
    }

    function getServicosAdicionais() {

        $productId  = $this->input->get('productId');
        $faixaId    = $this->input->get('faixaId');
        $servicos   = $this->ProdutoRepository_model->getServicosAdicionais($productId);

        foreach ($servicos as $servico) {

            $adicionais = $this->ProdutoRepository_model->getServicosAdicionaisPorFaixaEtaria($productId, $servico->id, $faixaId);

            foreach ($adicionais as $adicional) {
                $pr[] = array(
                    'id' => $adicional->id,
                    'text' => $adicional->text,
                    'code' => $adicional->code,
                    'price' => $adicional->price,
                    'category_id' => $adicional->category_id,
                    'category' => $adicional->category,
                );
            }

        }
        $this->sma->send_json($pr);
    }

    function getCustomer($id = NULL)
    {
        $row = $this->site->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->name)));
    }

    function suggestions($term = NULL, $limit = NULL)
    {

        $this->load->model('companies_model');

        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getCustomerSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    function getTipoHospedagem() {

        $productId      = $this->input->get('productId');
        $faixaId        = $this->input->get('faixaId');
        $programacaoId  = $this->input->get('programacao_id');

        $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagem($productId, $faixaId);

        if (!$valoresPorTipoHospedagem) {
            $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getTipoHospedagem($productId);
            $this->sma->send_json($this->getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId));
        } else {
            $this->sma->send_json($this->getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId));
        }
    }

    private function getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId) {
        $data = [];

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        foreach ($valoresPorTipoHospedagem as $tipo_hospedagem) {

            $agenda = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
            $vagas  = $this->AgendaViagemService_model->vagas($programacaoId, $agenda->getControleEstoqueHospedagem(), $tipo_hospedagem->id);
            $label  = '';

            $totalQuartosDisponivel = 0;

            foreach ($agenda->getQuartos() as $quarto) {
                if ($quarto->tipo_hospedagem == $tipo_hospedagem->id) {
                    $totalQuartosDisponivel = $quarto->qtd_quartos_disponivel;
                }
            }

            if ($agenda->getControleEstoqueHospedagem()) {
                $label .= ' ('.$vagas.' Vaga(s) disponíveis | Quarto(s) Disponíveis: '.$totalQuartosDisponivel.')';
            } else {
                if ($agenda->getTotalDisponvel() > 1) {
                    $label .=  '  ('.$agenda->getTotalDisponvel() .' Vagas disponível)';
                } else {
                    $label .= ' (' . $agenda->getTotalDisponvel() . ' Vaga disponível)';
                }
            }

            $tipo_hospedagem->text = $tipo_hospedagem->text.$label;

            $data[] = $tipo_hospedagem;
        }

        return $data;
    }

    public function getParcelas()
    {
        $condicaoPagamentoId = $this->input->get('condicaoPagamentoId');
        $dtvencimento = $this->input->get('dtvencimento');
        $valorVencimento = $this->input->get('valorVencimento');
        $tipoCobrancaId = $this->input->get('tipoCobrancaId');
        $desconto = $this->input->get('desconto');
        $blockUltimaParcela = $this->input->get('blockUltimaParcela');

        $tiposCobranca      = $this->site->getAllTiposCobranca();
        $condicaoPagamento  = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
        $tipoCobranca       = $this->site->getTipoCobrancaById($tipoCobrancaId);
        $movimentadores     = $this->site->getAllContas();

        $totalParcelas      = $condicaoPagamento->parcelas;

        if ($desconto != '' && $desconto < $valorVencimento) {
            $valorVencimento = $valorVencimento - $desconto;
        } else {
            $desconto = 0;
        }

        $valorVencimento = number_format($valorVencimento / $totalParcelas, 2, '.', '');

        $html = '';

        if ($dtvencimento == null) {
            $dtvencimento = date('Y-m-d');
        } else {
            $dtvencimento = date('Y-m-d', strtotime($dtvencimento));
        }

        $preservaDia = date('d', strtotime($dtvencimento));

        $tipo       = $tipoCobranca->tipo;
        $integracoa = $tipoCobranca->integracao;
        $conta      = $tipoCobranca->conta;
        $readOnly   = '';

        if ($tipo == 'carne' ||
            $tipo == 'carne_boleto' ||
            $tipo == 'carne_cartao' ||
            $tipo == 'carne_boleto_cartao' &&
            $integracoa != '') {
            $readOnly = 'readonly';
        }

        $readOnlyVencimento = '';

        for ($i = 0; $i < $totalParcelas; $i++) {

            if ($blockUltimaParcela == 'true' && ($i == $totalParcelas - 1) || $totalParcelas == 1) $readOnlyVencimento = 'readonly';

            $html .= '<tr>
                    <td>' . ($i + 1) . '/' . $totalParcelas . '</td>
                    <td><input type="text" ' . $readOnly . ' ' . $readOnlyVencimento . ' name="valorVencimentoParcela[]" onkeyup="atualizarValorVencimentoAoEditarParcela(this, ' . $i . ');" required="required" class="form-control mask_money" vencimento="' . $valorVencimento . '" value="' . $valorVencimento . '"/></td>
                    <td><input type="text" ' . $readOnly . ' name="descontoParcela[]" class="form-control" readonly value="' . $desconto / $totalParcelas . '" /></td>
                    <td><input type="date" ' . $readOnly . ' name="dtVencimentoParcela[]" required="required" class="form-control" value="' . $dtvencimento . '" /></td>
                    <td>
                        <select class="form-control" ' . $readOnly . ' name="tipocobrancaParcela[]" required="required">';

            foreach ($tiposCobranca as $tipoCobranca) {
                if ($tipoCobrancaId == $tipoCobranca->id) {
                    $html .= '      <option selected="selected" value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                }
            }

            $html .= ' </select> 
                      </td>';

            $html .= '<td>
                        <select class="form-control" ' . $readOnly . ' name="movimentadorParcela[]" required="required">';

            foreach ($movimentadores as $movimentador) {
                if ($movimentador->id == $conta) {
                    $html .= '      <option selected="selected" value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                }
            }

            $html .= '    </select>
                    </td>
                </tr>';

            $anoAtual = date('Y', strtotime($dtvencimento));
            $mesAtual = date('m', strtotime($dtvencimento));
            $diaAtual = date('d', strtotime($dtvencimento));

            if ($mesAtual == 1 && $diaAtual > 28) {
                if (date('L', strtotime($anoAtual . '-01-01'))) {
                    $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-02-29'));
                } else {
                    $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-02-28'));
                }
            } else if ($mesAtual == 2) {//todo gera marco
                $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-03-' . $preservaDia));
            } else {
                $dtvencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtvencimento)));
            }
        }
        echo $html;
    }

    public function consultar_servicos()
    {
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $term           = $this->input->get('term', true);
        $warehouse_id   = $this->input->get('warehouse_id', true);
        $customer_id    = $this->input->get('customer_id', true);
        $ano            = $this->input->get('ano', true);
        $mes            = $this->input->get('mes', true);
        $data_passeio   = $this->input->get('data_passeio', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed   = $this->sma->analyze_term($term);
        $sr         = $analyzed['term'];
        $option_id  = $analyzed['option_id'];
        $limit      = 200;
        $productId  = null;
        $group_by   = NULL;

        $customer       = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows           = $this->getProgramacoesVenda($sr, $warehouse_id, $limit, $productId, $group_by, $ano, $mes, $data_passeio);

        if ($rows) {
            foreach ($rows as $row) {

                $label =  $row->name.' - '.$this->sma->hrsd($row->dataSaida). ' '.$this->sma->hf($row->horaSaida);

                if ($row->programacaoId != null) {
                    $agenda = $this->AgendaViagemService_model->getProgramacaoById($row->programacaoId);

                    $totalQuartosDisponivel = 0;

                    foreach ($agenda->getQuartos() as $quarto) {
                        $totalQuartosDisponivel += $quarto->qtd_quartos_disponivel;
                    }

                    if ($agenda->getControleEstoqueHospedagem()) {
                        $label .= ' ('.$agenda->getTotalDisponvel().' Vaga(s) disponíveis | Quarto(s) Disponíveis: '.$totalQuartosDisponivel.')';
                    } else {
                        if ($agenda->getTotalDisponvel() > 1) {
                            $label .=  '  ('.$agenda->getTotalDisponvel() .' Vagas disponível)';
                        } else {
                            $label .= ' (' . $agenda->getTotalDisponvel() . ' Vaga disponível)';
                        }
                    }

                    $row->quantity = $agenda->getTotalDisponvel();
                }

                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';

                $row->destino = '';
                $row->origem = '';
                $row->customerClient = $customer->id;
                $row->customerClientName = $customer->name;

                $row->faixaEtaria = null;
                $row->nmFaixaEtaria = '';

                $row->dateCheckin = $row->dataSaida;
                $row->dateCheckOut = $row->dataRetorno;

                $row->horaCheckin = $row->horaSaida;
                $row->horaCheckOut = $row->horaRetorno;

                $row->dataEmissao = date('Y-m-d');
                $row->digitado = '0';
                $row->comissao = 0;

                $opt = json_decode('{}');
                $opt->price = 0;
                $row->option = $option_id;

                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }

                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $row->real_unit_price = $row->price;
                $combo_items = false;

                $pr[] = array(
                    'id' => str_replace(".", "", microtime(true)),
                    'item_id' => $row->id,
                    'label' => $label,
                    'category' => $row->category_id,
                    'row' => $row,
                    'combo_items' => $combo_items,
                    'tax_rate' => false,
                    'options' => [],
                    'dataSaida' => $this->sma->hrsd($row->dataSaida)
                );
            }

            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    private function getProgramacoesVenda($term, $warehouse_id, $limit = 200, $productId = null, $group_by = NULL, $ano = NULL, $mes = NULL, $data_passeio = NULL) {
        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }

        if ($productId != null && $productId != 'all') {
            $this->db->where("products.id", $productId);
        }

        if ($group_by != null) {
            $this->db->group_by($group_by);
        }

        if ($ano) {
            $this->db->where("YEAR(sma_agenda_viagem.dataSaida)", $ano);
        }

        if ($mes) {
            $this->db->where("MONTH(sma_agenda_viagem.dataSaida)", $mes);
        }

        if ($data_passeio) {
            $this->db->where("sma_agenda_viagem.dataSaida", $data_passeio);
        }

        $this->db->limit($limit);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

}