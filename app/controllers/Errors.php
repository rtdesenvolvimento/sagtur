<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends MY_Controller
{

    public function error_404()
    {
        $data['title'] = lang('error_404');
        $data['site_url'] = base_url();
        $data['site_name'] = $this->Settings->site_name;
        $data['assets'] = $this->data['assets'];
        //$this->load->view('default/views/errors/error_404', $data); // For errors style
        $this->load->view('default/views/errors/circle', $data); // For circle 404 page
        //$this->load->view('default/views/errors/rectangle', $data); // For rectangle 404 page
    }

    public function error_new_page_cart_1()
    {

        $empresa =  $this->router->uri->segments[1];

        if ($empresa=='vfbturismo') {
            header('Location: https://www.suareservaonline.com.br/vfbturismo/44017');
        } else {
            $data['title'] = 'Nova Página';
            $data['site_url'] = base_url();
            $data['empresa'] = $empresa;
            $data['site_name'] = $this->Settings->site_name;
            $data['assets'] = $this->data['assets'];
            $this->load->view('default/views/errors/error_new_page_cart', $data); // For circle 404 page
        }
    }

    public function error_new_page_cart()
    {

        $empresa =  $this->router->uri->segments[1];

        if ($empresa == 'appcompra') $empresa =  $this->router->uri->segments[2];
        if ($empresa == 'loja')  $empresa =  $this->router->uri->segments[2];

        $uri     = str_replace($empresa.'/', '', $_SERVER['REQUEST_URI']);
        $uri     = str_replace('appcompra', 'carrinho', $uri);
        $uri     = str_replace('loja', '', $uri);

        if ($empresa=='vfbturismo') {
            header('Location: https://www.suareservaonline.com.br/vfbturismo/44017');
        } else {

            if ($Settings->own_domain){
                header('Location: '.$Settings->own_domain.'/'.$empresa.$uri);
            } else {
                header('Location: https://www.suareservaonline.com.br/'.$empresa.$uri);
            }
        }
    }

    public function show_error()
    {
        show_error('This is general error message.');
    }

}
