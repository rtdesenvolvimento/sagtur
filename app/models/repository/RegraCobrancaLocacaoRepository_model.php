<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RegraCobrancaLocacaoRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function search_value_vehicle_rental_rule($regra_id, $qtd) {

        $this->db->where('regra_cobranca_locacao_proporcao.inicio>='.$qtd.' AND regra_cobranca_locacao_proporcao.inicio<='.$qtd);

        $q = $this->db->get_where('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => $regra_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}