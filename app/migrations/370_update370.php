<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update370 extends CI_Migration {

    public function up() {

        $this->insert_tempo();

        $this->db->update('settings',  array('version' => '3.7.0'), array('setting_id' => 1));
    }

    public function insert_tempo() {
        try {

            $this->db->query('delete from sma_tempo');

            for ($ano=2020;$ano<=2030;$ano++) {
                $this->db->query("INSERT INTO sma_tempo (data)
                                    SELECT ADDDATE('".$ano."-01-01', INTERVAL (num - 1) DAY) as date
                                    FROM (
                                        SELECT @num := @num + 1 as num
                                        FROM (
                                            SELECT 0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9
                                        ) n1, (
                                            SELECT 0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9
                                        ) n2, (
                                            SELECT @num := 0
                                        ) num_init
                                    ) num_gen
                                    WHERE ADDDATE('".$ano."-01-01', INTERVAL (num - 1) DAY) <= '".$ano."-12-31'");
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
