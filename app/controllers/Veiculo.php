<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculo extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->model('veiculo_model');

    }

    function suggestions($term = NULL, $limit = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->veiculo_model->getSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }


    function suggestions_tipo_tranporte($term = NULL, $limit = NULL)
    {
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);

        $rows['results'] = $this->veiculo_model->getSuggestionsTipoTransporte($term, $limit);

        $this->sma->send_json($rows);
    }

    function getTipoTransporte($id = NULL)
    {
        $row = $this->veiculo_model->getTipoTransporte($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->company.' ('.$row->name.')')));
    }

}