<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update456 extends CI_Migration {

    public function up() {

        $this->create_table_commission_settings();
        $this->create_table_comissoes();
        $this->create_table_commissions_items();
        $this->create_table_close_commissions();
        $this->create_table_close_commission_items();
        $this->create_table_commission_billers();
        $this->alter_table_companies();
        $this->alter_table_categories();
        $this->alter_table_conta_pagar();
        $this->alter_table_parcela();
        $this->alter_table_fatura();
        $this->alter_table_payments();
        $this->alter_table_settings();
        $this->create_table_malotes();
        $this->create_table_malote_faturas();

        $this->insert_despesa();
        $this->insert_dados_para_controle_fechamento_comissao();

        $this->db->update('settings',  array('version' => '4.5.6'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'commission_settings_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('commission_settings_id' => 1), array('setting_id' => 1));
    }

    function create_table_malotes()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'status'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Aberto'),
            'date' => array('type' => 'DATE', 'null' => FALSE ),
            'dt_fechamento' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'tipo_cobranca_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'movimentador_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),//conta
            'forma_pagamento_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'total'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'total_pago'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE  ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('malotes', TRUE, $attributes);
    }

    function create_table_malote_faturas()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'fatura_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'malote_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'valor_fatura'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'valor_pagar'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('malote_faturas', TRUE, $attributes);
    }

    function create_table_close_commissions() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'status'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Pendente'),
            'dt_competencia' => array('type' => 'DATE', 'null' => FALSE ),
            'dt_fechamento' => array('type' => 'DATE', 'null' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),
            'title'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => ''),
            'total_commission'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'paid'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('close_commissions', TRUE, $attributes);
    }

    function create_table_close_commission_items() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'status'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Pendente'),
            'close_commission_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do fechamento
            'commission_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do fechamento
            'product_name' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do cliente
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),//produto
            'programacao_id' => array('type' => 'INT', 'constraint' => 11 ),//programacao agendamento
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),//venda
            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do vendedor
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do vendedor
            'customer' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do cliente
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do cliente
            'commission_percentage'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'commission'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'base_value'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'paid'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('close_commission_items', TRUE, $attributes);
    }

    function create_table_commission_settings() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'commission_name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'dia_fechamento_comissao' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('commission_settings', TRUE, $attributes);

        $tipo_faixa_padrao_bebe = array(
            'commission_name' => 'CONFIGURAÇÕES PADRÃO DE COMISSÃO',
        );

        $this->db->insert('commission_settings', $tipo_faixa_padrao_bebe);
    }

    public function insert_despesa() {

        $data_despesa = array(
            'name' => 'COMISSÃO DE VENDEDOR',
            'code' => 'COMISSÃO DE VENDEDOR',
            'tipo' => 'subgrupo',
            'despesasuperior' => 18,
        );

        $this->db->insert('despesa', $data_despesa);
        $despesa_id = $this->db->insert_id();

        $fields = array(
            'despesa_fechamento_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('commission_settings', $fields);

        $this->db->update('commission_settings',  array('despesa_fechamento_id' => $despesa_id), array('id' => 1));
    }

    public function insert_dados_para_controle_fechamento_comissao()
    {
        $conta_comissao = array(
            'user_id'       => 1,
            'name'          => 'CAIXA - COMISSÃO DE VENDEORES',
            'status'        => 'open',
            'date'          => date('Y-m-d H:i:s'),
            'cash_in_hand'  => 0,
            'caixa'         => 0,
        );
        $this->db->insert('pos_register', $conta_comissao);
        $conta_caixa_comissao_id = $this->db->insert_id();

        $tipo_cobranca_pgm = array(
            'name' => 'Pagamento em mãos (Dinheiro)',
            'status' => 'Ativo',
            'note' => '',
            'tipo' => 'nenhuma',
            'tipoExibir' => 'despesa',
            'integracao' => 'nenhuma',
            'faturar_automatico' => 'sim',
            'formapagamento' => 2,//DINHEIRO
            'conta' => $conta_caixa_comissao_id,
            'faturarVenda' => 1,
            'automatic_cancellation_sale' => 0,
            'numero_dias_cancelamento' => 0,
            'exibirLinkCompras' => TRUE,
            'credito_cliente' => 0,
        );
        $this->db->insert('tipo_cobranca', $tipo_cobranca_pgm);

        $tipo_cobranca_transferencia = array(
            'name' => 'Transferência/Depósito (Pagamento Vendedor)',
            'status' => 'Ativo',
            'note' => '',
            'tipo' => 'nenhuma',
            'tipoExibir' => 'despesa',
            'integracao' => 'nenhuma',
            'faturar_automatico' => 'sim',
            'formapagamento' => 2,//DINHEIRO
            'conta' => $conta_caixa_comissao_id,
            'faturarVenda' => 1,
            'automatic_cancellation_sale' => 0,
            'numero_dias_cancelamento' => 0,
            'exibirLinkCompras' => TRUE,
            'credito_cliente' => 0,
        );
        $this->db->insert('tipo_cobranca', $tipo_cobranca_transferencia);

        $tipo_cobranca_pix = array(
            'name' => 'PIX (Pagamento Vendedor)',
            'status' => 'Ativo',
            'note' => '',
            'tipo' => 'nenhuma',
            'tipoExibir' => 'despesa',
            'integracao' => 'nenhuma',
            'faturar_automatico' => 'sim',
            'formapagamento' => 2,//DINHEIRO
            'conta' => $conta_caixa_comissao_id,
            'faturarVenda' => 1,
            'automatic_cancellation_sale' => 0,
            'numero_dias_cancelamento' => 0,
            'exibirLinkCompras' => TRUE,
            'credito_cliente' => 0,
        );
        $this->db->insert('tipo_cobranca', $tipo_cobranca_pix);

        $tipo_cobranca_pix_id = $this->db->insert_id();

        $fields = array(
            'tipo_cobranca_fechamento_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('commission_settings', $fields);

        $this->db->update('commission_settings',  array('tipo_cobranca_fechamento_id' => $tipo_cobranca_pix_id), array('id' => 1));
    }

    public function alter_table_companies()
    {
        $fields = array(
            'tipo_comissao' => array('type' => 'VARCHAR', 'constraint' => '50' , 'default' => 'percentual'),
            'dia_fechamento_comissao' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE, 'default' => 0),
            'limite_desconto'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ,  'default' => 0.00),
            'billing_data' => array('type' => 'VARCHAR', 'constraint' => '600'  ),//dados do pagamento para o vendedor
        );

        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_categories()
    {
        $fields = array(
            'tipo_comissao' => array('type' => 'VARCHAR', 'constraint' => '50' , 'default' => 'percentual'),
        );

        $this->dbforge->add_column('categories', $fields);
    }

    function create_table_comissoes() {//commission type product
        $fields = array(
            'id'        => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),
            'status'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Pendente'),
            'subtotal'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//venda
            'total_commission'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('commissions', TRUE, $attributes);
    }

    function create_table_commissions_items() {
        $fields = array(
            'id'        => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'status'    => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Pendente'),

            'commission_percentage'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'commission'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),
            'base_value'  => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0.00),

            'commission_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),

            'commission_type_product' => array('type' => 'VARCHAR', 'constraint' => '50' , 'null' => FALSE),

            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do vendedor
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do vendedor

            'customer' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do vendedor
            'customer_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do cliente

            'product_name' => array('type' => 'VARCHAR', 'constraint' => '300' ),//nome do vendedor
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),//produto

            'programacao_id' => array('type' => 'INT', 'constraint' => 11 ),//programacao agendamento
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),//venda

            'date_payment' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//pagamento
            'date_approval' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//aprovacao
            'date_closure' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//fechamento da comissao

            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('commission_items', TRUE, $attributes);
    }

    function create_table_commission_billers() {
        $fields = array(
            'id'        => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'commission_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do vendedor
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('commission_billers', TRUE, $attributes);
    }

    public function alter_table_conta_pagar()
    {
        $fields = array(
            'fechamento_comissao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
            'fechamento_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
        );

        $this->dbforge->add_column('conta_pagar', $fields);
    }

    public function alter_table_parcela()
    {
        $fields = array(
            'fechamento_comissao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
            'fechamento_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
        );

        $this->dbforge->add_column('parcela', $fields);
    }

    public function alter_table_fatura()
    {
        $fields = array(
            'fechamento_comissao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
            'fechamento_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
        );

        $this->dbforge->add_column('fatura', $fields);
    }

    public function alter_table_payments()
    {
        $fields = array(
            'fechamento_comissao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
            'fechamento_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true),
        );

        $this->dbforge->add_column('payments', $fields);
    }

    public function down() {}
}
