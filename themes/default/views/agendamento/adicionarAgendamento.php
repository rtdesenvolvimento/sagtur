<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_agendamento_header'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agendamento/adicionarAgendamento/", $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("status", "status") ?>
                        <?php
                        $opts = array(
                            'AGUARDANDO_PAGAMENTO' => lang('AGUARDANDO_PAGAMENTO'),
                            'PAGO' => lang('PAGO'),
                            'CANCELADA' => lang('CANCELADA'),
                        );
                        echo form_dropdown('status', $opts,  '' , 'class="form-control" id="status" readonly required="required"');
                        ?>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <?= lang('servico', 'servico'); ?>
                        <div class="input-group">
                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                            </div>
                            <?php
                            $cbServicos[''] = lang('select').' '.lang('servico');
                            foreach ($servicos as $servico) {
                                $cbServicos[$servico->id] = $servico->name;
                            } ?>
                            <?= form_dropdown('produto', $cbServicos,  '', 'class="form-control" required="required" id="produto"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <?= lang('tipo_cobranca_financeiro', 'tipo_cobranca'); ?>
                        <div class="input-group">
                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                <i class="fa fa-barcode"  style="font-size: 1.2em;"></i>
                            </div>
                            <?php
                            $cbTipoDeCobranca[''] = lang('select').' '.lang('tipo_cobranca');
                            foreach ($tiposCobranca as $tipoCobranca) {
                                $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                            }
                            ?>
                            <?= form_dropdown('tipoCobranca', $cbTipoDeCobranca,  '', 'class="form-control" required="required" id="tipoCobranca"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('date', 'dataDe'); ?>
                        <?= form_input('data', date('Y-m-d'), 'class="form-control" id="data" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('horaInicio', 'horaInicio'); ?>
                        <?= form_input('horaInicio', '', 'class="form-control" id="horaInicio" required="required"', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('horaTermino', 'horaTermino'); ?>
                        <?= form_input('horaTermino', '', 'class="form-control" id="horaTermino" required="required"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('nome_completo', 'nomeCompleto'); ?>
                        <?= form_input('nomeCompleto','', 'class="form-control" id="nomeCompleto" required="required"'); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('celular', 'celular'); ?>
                        <?= form_input('celular', '', 'class="form-control" id="celular" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('cpf', 'cpf'); ?>
                        <?= form_input('cpf', '', 'class="form-control" id="cpf"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('email', '', 'class="form-control" id="email"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_hidden('isBloqueado', FALSE); ?>
        <div class="modal-footer">
            <?php echo form_submit('adicionarAgendamento', lang('add'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $('#status').attr('readonly', true);
        $("#cpf").mask("999.999.999-99");
    });
</script>
