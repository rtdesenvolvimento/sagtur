<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Agendamento_model extends CI_Model
{
    const STATUS_PAGO = 'PAGO';
    const STATUS_AGUARDANDO_PAGAMENTO = 'AGUARDANDO_PAGAMENTO';
    const STATUS_CANCELADA = 'CANCELADA';
    const STATUS_REALIZADO = 'REALIZADO';
    const STATUS_ORCAMENTO = 'ORCAMENTO';
    const STATUS_BLOQUEADO = 'BLOQUEADO';

    public $status;
    public $produto;
    public $cliente;
    public $horaInicio;
    public $horaTermino;
    public $data;
    public $dataAte;
    public $tipoCobranca;
    public $contaReceber;
    public $valor;
    public $nomeProduto;
    public $nomeCategoria;
    public $nomeTipoCobranca;
    public $nomeCompleto;
    public $cpf;
    public $email;
    public $celular;
    public $compromisso;
    public $dataReserva;
    public $isBloqueado;
    public $dataNascimento;

    public $cardName;
    public $senderHash;
    public $creditCardToken;
    public $parcelas;

    public function __construct() {

        parent::__construct();

        $this->setDataReserva(date('Y-m-d'));
        $this->setStatus(Agendamento_model::STATUS_BLOQUEADO);
        $this->setNomeProduto('Horário Bloqueado');
        $this->setNomeCategoria('Bloqueado');
        $this->setNomeTipoCobranca('Bloqueado');
        $this->setValor(0);
        $this->setCliente(0);
        $this->setContaReceber(0);
        $this->setProduto(0);
        $this->setParcelas(0);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getHoraInicio()
    {
        return $this->horaInicio;
    }

    /**
     * @param mixed $horaInicio
     */
    public function setHoraInicio($horaInicio)
    {
        $this->horaInicio = $horaInicio;
    }

    /**
     * @return mixed
     */
    public function getHoraTermino()
    {
        return $this->horaTermino;
    }

    /**
     * @param mixed $horaTermino
     */
    public function setHoraTermino($horaTermino)
    {
        $this->horaTermino = $horaTermino;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getTipoCobranca()
    {
        return $this->tipoCobranca;
    }

    /**
     * @param mixed $tipoCobranca
     */
    public function setTipoCobranca($tipoCobranca)
    {
        $this->tipoCobranca = $tipoCobranca;
    }

    /**
     * @return mixed
     */
    public function getContaReceber()
    {
        return $this->contaReceber;
    }

    /**
     * @param mixed $contaReceber
     */
    public function setContaReceber($contaReceber)
    {
        $this->contaReceber = $contaReceber;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        if ($this->valor == null) $this->valor = 0;

        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    /**
     * @param mixed $nomeCompleto
     */
    public function setNomeCompleto($nomeCompleto)
    {
        $this->nomeCompleto = $nomeCompleto;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getNomeProduto()
    {
        return $this->nomeProduto;
    }

    /**
     * @param mixed $nomeProduto
     */
    public function setNomeProduto($nomeProduto)
    {
        $this->nomeProduto = $nomeProduto;
    }

    /**
     * @return mixed
     */
    public function getCompromisso()
    {
        return $this->compromisso;
    }

    /**
     * @param mixed $compromisso
     */
    public function setCompromisso($compromisso)
    {
        $this->compromisso = $compromisso;
    }

    /**
     * @return mixed
     */
    public function getCardName()
    {
        return $this->cardName;
    }

    /**
     * @param mixed $cardName
     */
    public function setCardName($cardName)
    {
        $this->cardName = $cardName;
    }

    /**
     * @return mixed
     */
    public function getSenderHash()
    {
        return $this->senderHash;
    }

    /**
     * @param mixed $senderHash
     */
    public function setSenderHash($senderHash)
    {
        $this->senderHash = $senderHash;
    }

    /**
     * @return mixed
     */
    public function getCreditCardToken()
    {
        return $this->creditCardToken;
    }

    /**
     * @param mixed $creditCardToken
     */
    public function setCreditCardToken($creditCardToken)
    {
        $this->creditCardToken = $creditCardToken;
    }

    /**
     * @return mixed
     */
    public function getDataReserva()
    {
        return $this->dataReserva;
    }

    /**
     * @param mixed $dataReserva
     */
    public function setDataReserva($dataReserva)
    {
        $this->dataReserva = $dataReserva;
    }

    /**
     * @return mixed
     */
    public function getDataAte()
    {
        return $this->dataAte;
    }

    /**
     * @param mixed $dataAte
     */
    public function setDataAte($dataAte)
    {
        $this->dataAte = $dataAte;
    }

    /**
     * @return mixed
     */
    public function getIsBloqueado()
    {
        return $this->isBloqueado;
    }

    /**
     * @param mixed $isBloqueado
     */
    public function setIsBloqueado($isBloqueado)
    {
        $this->isBloqueado = $isBloqueado;
    }

    /**
     * @return mixed
     */
    public function getNomeCategoria()
    {
        return $this->nomeCategoria;
    }

    /**
     * @param mixed $nomeCategoria
     */
    public function setNomeCategoria($nomeCategoria)
    {
        $this->nomeCategoria = $nomeCategoria;
    }

    /**
     * @return mixed
     */
    public function getNomeTipoCobranca()
    {
        return $this->nomeTipoCobranca;
    }

    /**
     * @param mixed $nomeTipoCobranca
     */
    public function setNomeTipoCobranca($nomeTipoCobranca)
    {
        $this->nomeTipoCobranca = $nomeTipoCobranca;
    }

    /**
     * @return mixed
     */
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * @param mixed $dataNascimento
     */
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = $dataNascimento;
    }

    /**
     * @return mixed
     */
    public function getParcelas()
    {
        return $this->parcelas;
    }

    /**
     * @param mixed $parcelas
     */
    public function setParcelas($parcelas)
    {
        $this->parcelas = $parcelas;
    }

}
