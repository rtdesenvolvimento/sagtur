<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FolderRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id)
    {
        return parent::__getById('folders', $id);
    }

    public function getAllFolders() {

        $this->db->select("folders.id as id");
        $q = $this->db->get('folders');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = new Folder_model($row->id);
            }
            return $data;
        }
        return FALSE;
    }

    public function getAll($filter)
    {

        //$filter = new FolderFilterDTO_model();

        $this->db->select("folders.id as id");

        $this->db->join('documents', 'documents.folder_id=folders.id', 'left');

        if ($filter->statusDocument) {
            $this->db->where('document_status', $filter->statusDocument);
        }

        if ($filter->biller_id) {
            $this->db->where('documents.biller_id', $filter->biller_id);
        }

        if ($filter->strFolder) {

            $term = $this->db->escape_like_str($filter->strFolder);

            $sql = "EXISTS (
                                SELECT 1 
                                FROM sma_document_signatures
                                WHERE sma_document_signatures.document_id = sma_documents.id AND ";

            $sql .= "(  sma_document_signatures.name LIKE '%" . $term . "%' OR 
                        sma_document_signatures.email LIKE '%" . $term . "%' OR 
                        sma_document_signatures.phone LIKE '%" . $term . "%' OR
                        REPLACE(REPLACE(REPLACE(sma_document_signatures.phone, '-', ''), '(', ''), ')', '') LIKE '%" . $term . "%') OR 
                        folders.name LIKE '%" . $term . "%'";
            $sql .= ')';

            $this->db->where($sql);

        }

        $this->db->group_by('folders.id');

        $q = $this->db->get('folders');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = new Folder_model($row->id);
            }
            return $data;
        }
        return FALSE;
    }

    public function getFoldersByProgramacao($programacaoID)
    {
        $q = $this->db->get_where('folders', array('programacao_id' => $programacaoID), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}