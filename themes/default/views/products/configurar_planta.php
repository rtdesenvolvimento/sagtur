<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_figura_planta').' '.$visao->name; ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("products/addPosicaoPlanta", $attrib)
                ?>

                <div class="col-md-4">
                    <div class="form-group all">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <input type="hidden" id="visal_automovel_id" value="<?php echo $visal_automovel_id;?>" name="visal_automovel_id"/>
                                        <?= lang('nome_utensilho', 'nome_utensilho') ?>
                                        <?= form_input('name', '', 'class="form-control tip" required="required" id="name"') ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("tipo", "tipo") ?>
                                        <?php
                                        $opts = array(
                                            'acento' => lang('acento'),
                                            'outros' => lang('outros_utencilhos')
                                        );
                                        echo form_dropdown('tipo', $opts, '', 'class="form-control" id="tipo"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= lang('client_x', 'client_x') ?>
                                        <?= form_input('client_x', '', 'class="form-control tip" readonly id="client_x"') ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= lang('client_y', 'client_y') ?>
                                        <?= form_input('client_y', '', 'class="form-control tip" readonly id="client_y"') ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo form_submit('add_figura', $this->lang->line("add_figura"), 'class="btn btn-primary"'); ?>
                                        <input type="button" value="Limpar" id="limpar" class="btn btn-primary">
                                        <input type="button" value="Voltar" id="voltar" class="btn btn-primary">
                                    </div>
                                </div>

                            </div>
                        </div>

                     </div>
                </div>

                <div class="col-md-8">
                    <div class="form-group all">
                        <iframe height="1200" style="margin: 0;" id="iframeimagem" frameborder="0" scrolling="yes" src="<?php echo base_url(); ?>products/iframePontosPlantaVeiculo/<?php echo $visal_automovel_id;?>" width="100%"></iframe>
                    </div>
                </div>

                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#limpar').click(function (e) {
            var visal_automovel_id = $('#visal_automovel_id').val();
            var url = '<?php echo base_url(); ?>products/iframePontosPlantaVeiculo/'+visal_automovel_id;
            $('#iframeimagem').attr('src',url);

            $('#client_x').val('');
            $('#client_y').val('');
        });

        $('#voltar').click(function (e) {
            window.location = '<?php echo base_url();?>/products/planta';
        });
    });

    function atribuirXY(x,y) {
        $('#client_x').val(x);
        $('#client_y').val(y);
    }

    function recarregar() {
        var visal_automovel_id = $('#visal_automovel_id').val();
        var url = '<?php echo base_url(); ?>products/iframePontosPlantaVeiculo/'+visal_automovel_id;
        $('#iframeimagem').attr('src',url);
    }
</script>
