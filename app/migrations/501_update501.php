<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update501 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '5.0.1'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'about_photo_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
            'assinatura_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
