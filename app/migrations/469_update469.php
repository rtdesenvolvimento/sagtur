<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update469 extends CI_Migration {

    public function up() {

        $this->create_table_google_persons();

        $this->db->update('settings',  array('version' => '4.6.9'), array('setting_id' => 1));
    }

    function create_table_google_persons() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'imported' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'givenName' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'familyName' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'phoneNumbers' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'emailAddresses' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'google_person_id' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'error' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('google_persons', TRUE, $attributes);
    }
    public function down() {}
}
