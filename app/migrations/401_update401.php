<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update401 extends CI_Migration {

    public function up() {

        $this->create_table_sale_comments();
        $this->create_table_sale_notes();
        $this->create_table_sale_documents();
        $this->create_table_sale_item_documents();
        $this->create_table_sale_reminder();

        $this->create_table_menu_site();
        $this->create_table_page();
        $this->create_table_assento_cobranca_extra();
        $this->create_table_assento_cobranca_extra_base();

        $this->db->update('settings',  array('version' => '4.0.1'), array('setting_id' => 1));
    }

    public function create_table_sale_notes() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'display_voucher' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_notes', TRUE, $attributes);
    }

    public function create_table_sale_comments() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'comment' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'display_voucher' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_comments', TRUE, $attributes);
    }

    public function create_table_sale_reminder() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'comment' => array('type' => 'LONGTEXT', 'default' => ''),
            'date_start' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'date_end' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'display_voucher' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_reminder', TRUE, $attributes);
    }

    public function create_table_sale_documents() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'document' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'display_voucher' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_documents', TRUE, $attributes);
    }

    public function create_table_sale_item_documents() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11 ),
            'item_id' => array('type' => 'INT', 'constraint' => 11 ),
            'document' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'display_voucher' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('sale_item_documents', TRUE, $attributes);
    }

    public function create_table_assento_cobranca_extra_base() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'tipo_transporte_id' => array('type' => 'INT', 'constraint' => 11 ),
            'assento' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('assento_cobranca_extra_base', TRUE, $attributes);
    }

    public function create_table_assento_cobranca_extra() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),
            'tipo_transporte_id' => array('type' => 'INT', 'constraint' => 11 ),
            'assento' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('assento_cobranca_extra', TRUE, $attributes);
    }

    public function create_table_page() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'titulo' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'type' => array('type' => 'VARCHAR', 'constraint' => '30' , 'default' => 'conteudo' ),
            'url_page' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => true ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'menu_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true ),
            'data_publicacao' => array('type' => 'DATE', 'null' => TRUE ),
            'data_despublicacao' => array('type' => 'DATE', 'null' => TRUE ),
            'conteudo' => array('type' => 'LONGTEXT', 'default' => ''),
            'tag_title' => array('type' => 'VARCHAR', 'constraint' => '70', 'null' => FALSE  ),
            'meta_tag_description' => array('type' => 'VARCHAR', 'constraint' => '250', 'null' => FALSE  ),
            'header_conteudo' => array('type' => 'LONGTEXT', 'default' => ''),
            'pre_conteudo' => array('type' => 'LONGTEXT', 'default' => ''),
            'pos_conteudo' => array('type' => 'LONGTEXT', 'default' => ''),
            'footer_conteudo' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('page', TRUE, $attributes);
    }

    public function create_table_menu_site() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'type' => array('type' => 'VARCHAR', 'constraint' => '30' , 'default' => 'url' ),
            'new_page' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'url_page' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => true ),
            'page_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => true ),
            'menu_pai' => array('type' => 'INT', 'constraint' => 11 , 'null' => true ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
         );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('menu', TRUE, $attributes);
    }

    public function down() {

    }
}
