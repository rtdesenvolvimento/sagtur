<?php

$inv_items = $this->sales_model->getAllInvoiceItems($fatura->sale_id, true);
$ProdutoID = null;
$istaxasComissaoAtivo = FALSE;

foreach ($inv_items as $item) {
    $product = $this->site->getProductByID($item->product_id);

    $ProdutoID = $product->id;
    $istaxasComissaoAtivo = $product->isTaxasComissao;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $Settings->site_name;?>">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>
    <title><?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $Settings->site_name;?> || PAGUE COM CARTÃO DE CRÉDITO" >
    <meta name="keywords" content="<?php echo $Settings->site_name;?>">
    <meta name="application-name" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_br" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $Settings->site_name;?>"  />
    <meta property="og:description" content="CARTÃO DE CRÉDITO || <?php echo $fatura->product_name;?>" />
    <meta property="og:site_name" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO" />
    <meta property="og:image:alt" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $assets ?>styles/mercadopago/index.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

    <script src="https://sdk.mercadopago.com/js/v2"></script>

    <style>
        body {
            background: #ff4957;
        }
        .error {
            border: 1px solid red;
        }

        label.error {
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            font-size: 12px;
            position: absolute;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            top: -5px;
            right: -5px;
            z-index: 2;
            height: 25px;
            line-height: 1;
            background-color: #e34f4f;
            color: #fff !important;;
            font-weight: normal;
            display: inline-block;
            padding: 6px 8px;
        }

        label.error:after {
            content: '';
            position: absolute;
            border-style: solid;
            border-width: 0 6px 6px 0;
            border-color: transparent #e34f4f;
            display: block;
            width: 0;
            z-index: 1;
            bottom: -6px;
            left: 20%;
        }

       .card_destaque {
            border: 1px solid #dcdcdc;
            padding: 15px 15px 15px 15px;
            margin-bottom: 0;
            box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            background: #ffff;
            border-radius: 5px;
        }

    </style>

    <?php if ($this->Settings->pixelFacebook) {?>
        <!-- Meta Pixel Code | Purchase -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'Purchase', {
                content_name: '<?=str_replace("'", "", $fatura->product_name);?>',
                content_type: 'product',
                value: <?=$fatura->valorpagar;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=Purchase&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>
<body>
<main>
    <!-- Payment -->
    <section class="payment-form dark">
        <div class="container__payment">
            <div class="block-heading">
                <h2>Cartão de Crédito</h2>
            </div>
            <div class="form-payment">
                <span style="font-size: 11px;color: #28a745;font-weight: 500;padding: 5px;">Você está pagamento com a segurança da <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" /></span>
                <div class="products">
                    <div class="item">
                        <span class="price" id="summary-price"></span>
                        <p class="item-name"> <?php echo $fatura->product_name;?> <span id="summary-quantity"></span></p>
                    </div>
                    <div class="total">Valor Pagar<span class="price" id="summary-total"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></span></div>
                </div>
                <div class="payment-details">
                    <?php $attrib = array('id' => 'form-checkout');echo form_open_multipart("cartaocredito/processar_pagamento/".$cobrancaFatura->code, $attrib);  ?>
                        <h3 class="title">Preencha os dados do seu cartão</h3>
                        <div class="row card_destaque">
                            <div class="form-group col-sm-8">
                                <label for="cardholderName">Nome do titular</label>
                                <input id="form-checkout__cardholderName" name="cardholderName" placeholder="Nome do Titular (como gravado no cartão)" type="text" required="required" class="form-control"/>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="cardExpirationMonth">Vencimento</label>
                                <div class="input-group expiration-date">
                                    <input id="form-checkout__cardExpirationMonth" name="cardExpirationMonth" maxlength="2" placeholder="MM" required="required" type="tel" class="form-control"/>
                                    <span class="date-separator">/</span>
                                    <input id="form-checkout__cardExpirationYear" name="cardExpirationYear" placeholder="YY" maxlength="2" required="required" type="tel" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="cardNumber">Número do cartão</label>
                                <input id="form-checkout__cardNumber" name="cardNumber" maxlength="19"
                                       placeholder="•••• •••• •••• ••••" required="required" type="tel" class="form-control"/>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="cardholderName">Cód.Segurnaça</label>
                                <input id="form-checkout__securityCode" name="securityCode"required="required" placeholder="CVV"  type="tel" class="form-control"/>
                            </div>
                        </div>
                        <br>
                        <h3 class="title">Selecione o parcelamento</h3>
                        <div class="row card_destaque">
                            <div class="form-group col-sm-12">
                                <table style="width: 100%;" id="tb-parcelamento-valepay">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Parcela</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot><tr><td colspan="3"></td></tr></tfoot>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="ck-titular-cartao-valepay">ATENÇÃO: CASO NÃO SEJA O TÍTULAR DO CARTÃO ALTERE AS INFORMAÇÕES ABAIXO.</label>
                            </div>
                        </div>
                        <h3 class="title">Informe os dados do titular</h3>
                        <div class="row card_destaque">
                            <div class="form-group col-sm-12">
                                <label for="cardholderEmail">E-mail</label>
                                <input id="form-checkout__cardholderEmail" name="cardholderEmail" type="email" required="required"
                                       value="<?php echo $cliente->email;?>" class="form-control"/>
                                <span style="font-size: 11px;">Enviaremos os detalhes do pagamento para você assim que terminar</span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="docNumber">CPF doTitular do Cartão</label>
                                <input id="form-checkout__identificationNumber" name="docNumber" type="tel"
                                       required="required"
                                       value="<?php echo $cliente->vat_no;?>" class="form-control"/>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="docNumber">Telefone do Titular do Cartão</label>
                                <input id="form-checkout__telNumber"
                                       name="telNumber" type="tel"
                                       required="required"
                                       value="<?php echo $cliente->cf5;?>" class="form-control"/>
                            </div>
                        </div>
                        <br/>
                        <h3 class="title">Endereço da fatura do cartão</h3>
                        <div class="row card_destaque">
                            <div class="form-group col-sm-4">
                                <label for="cep_titular_valepay">CEP</label>
                                <input type="tel"
                                       class="form-control"
                                       name="cep_titular_valepay"
                                       id="cep_titular_valepay"
                                       autocomplete="off"
                                       maxlength="9"
                                       required="required"
                                       placeholder="xxxxx-xxx">
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="endereco_titular_valepay">Endereço</label>
                                <input type="text"
                                       class="form-control"
                                       name="endereco_titular_valepay"
                                       id="endereco_titular_valepay"
                                       autocomplete="off"
                                       required="required"
                                       placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="numero_endereco_titular_valepay">Número</label>
                                <input type="text"
                                       class="form-control"
                                       id="numero_endereco_titular_valepay"
                                       name="numero_endereco_titular_valepay"
                                       autocomplete="off"
                                       placeholder="">
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="complemento_endereco_titular">Complemento</label>
                                <input type="text"
                                       class="form-control"
                                       name="complemento_endereco_titular"
                                       autocomplete="off"
                                       placeholder="">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="bairro_titular_valepay">Bairro</label>
                                <input type="text"
                                       class="form-control"
                                       name="bairro_titular_valepay"
                                       id="bairro_titular_valepay"
                                       autocomplete="off"
                                       required="required"
                                       placeholder="">
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="cidade_titular_valepay">Cidade</label>
                                <input type="text"
                                       class="form-control"
                                       name="cidade_titular_valepay"
                                       id="cidade_titular_valepay"
                                       autocomplete="off"
                                       required="required"
                                       placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="estado_titular_valepay">UF</label>
                                <input type="text"
                                       class="form-control"
                                       name="estado_titular_valepay"
                                       id="estado_titular_valepay"
                                       autocomplete="off"
                                       required=""
                                       placeholder="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <button id="form-checkout__submit" type="button" class="btn btn-primary btn-block"><i class="fa fa-credit-card"></i>  PAGAR</button>
                                <span style="font-size: 11px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                                     <span style="font-size: 11px;color: #28a745;font-weight: 500;">Você está pagamento com a segurança da <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" /></span>
                                </span>
                                <br/>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Result -->
    <section class="shopping-cart dark" style="background: #ff4957;">
        <div class="container container__result">
            <div class="content">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="items product info product-details" style="background: #ff4957;">
                            <div class="row justify-content-md-center">
                                <div class="col-md-4 product-detail">
                                    <div class="product-info">
                                        <div id="fail-response" style="background: #ff4957">
                                            <br/>
                                            <img src="<?= $assets ?>images/cancelamento.gif"  style="width: 100%;" />
                                            <p class="text-center" style="color: #FFFFFF;">Ocorreu um erro...</p>
                                            <p id="error-message" class="text-center" style="color: #FFFFFF;font-size: 20px;font-weight: bold;padding: 15px;"></p>
                                            <br/>
                                            <button id="nova-tentativa" type="button" class="btn btn-primary btn-block" style="padding: 30px;background: #495057;border-color: #495057;">FAZER UMA NOVA TENTATIVA</button>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="<?= $assets ?>js/valida_cpf_cnpj.js" type="text/javascript"></script>

<script type="text/javascript">
    var site = <?=json_encode(array('base_url' => base_url(), 'settings' => $this->Settings, 'dateFormats' => $dateFormats))?>;

    (function (p, z) {
        function q(a) {
            return !!("" === a || a && a.charCodeAt && a.substr)
        }

        function m(a) {
            return u ? u(a) : "[object Array]" === v.call(a)
        }

        function r(a) {
            return "[object Object]" === v.call(a)
        }

        function s(a, b) {
            var d, a = a || {}, b = b || {};
            for (d in b)b.hasOwnProperty(d) && null == a[d] && (a[d] = b[d]);
            return a
        }

        function j(a, b, d) {
            var c = [], e, h;
            if (!a)return c;
            if (w && a.map === w)return a.map(b, d);
            for (e = 0, h = a.length; e < h; e++)c[e] = b.call(d, a[e], e, a);
            return c
        }

        function n(a, b) {
            a = Math.round(Math.abs(a));
            return isNaN(a) ? b : a
        }

        function x(a) {
            var b = c.settings.currency.format;
            "function" === typeof a && (a = a());
            return q(a) && a.match("%v") ? {
                pos: a,
                neg: a.replace("-", "").replace("%v", "-%v"),
                zero: a
            } : !a || !a.pos || !a.pos.match("%v") ? !q(b) ? b : c.settings.currency.format = {
                pos: b,
                neg: b.replace("%v", "-%v"),
                zero: b
            } : a
        }

        var c = {
            version: "0.4.1",
            settings: {
                currency: {symbol: "$", format: "%s%v", decimal: ".", thousand: ",", precision: 2, grouping: 3},
                number: {precision: 0, grouping: 3, thousand: ",", decimal: "."}
            }
        }, w = Array.prototype.map, u = Array.isArray, v = Object.prototype.toString, o = c.unformat = c.parse = function (a, b) {
            if (m(a))return j(a, function (a) {
                return o(a, b)
            });
            a = a || 0;
            if ("number" === typeof a)return a;
            var b = b || ".", c = RegExp("[^0-9-" + b + "]", ["g"]), c = parseFloat(("" + a).replace(/\((.*)\)/, "-$1").replace(c, "").replace(b, "."));
            return !isNaN(c) ? c : 0
        }, y = c.toFixed = function (a, b) {
            var b = n(b, c.settings.number.precision), d = Math.pow(10, b);
            return (Math.round(c.unformat(a) * d) / d).toFixed(b)
        }, t = c.formatQuantity = c.format = function (a, b, d, i) {
            if (m(a))return j(a, function (a) {
                return t(a, b, d, i)
            });
            var a = o(a), e = s(r(b) ? b : {
                precision: b,
                thousand: d,
                decimal: i
            }, c.settings.number), h = n(e.precision), f = 0 > a ? "-" : "", g = parseInt(y(Math.abs(a || 0), h), 10) + "", l = 3 < g.length ? g.length % 3 : 0;
            return f + (l ? g.substr(0, l) + e.thousand : "") + g.substr(l).replace(/(\d{3})(?=\d)/g, "$1" + e.thousand) + (h ? e.decimal + y(Math.abs(a), h).split(".")[1] : "")
        }, A = c.formatMoney = function (a, b, d, i, e, h) {
            if (m(a))return j(a, function (a) {
                return A(a, b, d, i, e, h)
            });
            var a = o(a), f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format);
            return (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal))
        };
        c.formatColumn = function (a, b, d, i, e, h) {
            if (!a)return [];
            var f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format), l = g.pos.indexOf("%s") < g.pos.indexOf("%v") ? !0 : !1, k = 0, a = j(a, function (a) {
                if (m(a))return c.formatColumn(a, f);
                a = o(a);
                a = (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal));
                if (a.length > k)k = a.length;
                return a
            });
            return j(a, function (a) {
                return q(a) && a.length < k ? l ? a.replace(f.symbol, f.symbol + Array(k - a.length + 1).join(" ")) : Array(k - a.length + 1).join(" ") + a : a
            })
        };
        if ("undefined" !== typeof exports) {
            if ("undefined" !== typeof module && module.exports)exports = module.exports = c;
            exports.accounting = c
        } else"function" === typeof define && define.amd ? define([], function () {
            return c
        }) : (c.noConflict = function (a) {
            return function () {
                p.accounting = a;
                c.noConflict = z;
                return c
            }
        }(p.accounting), p.accounting = c)
    })(this);

    jQuery(document).ready(function() {

        jQuery.extend(jQuery.validator.messages, {
            required: "Campo é obrigatório.",
            email: "Por favor insira um endereço de e-mail válido.",
            date: "Please enter a valid date.",
            number: "Por favor insira um número válido.",
            digits: "Por favor insira um número válido.",
            maxlength: jQuery.validator.format("Não insira mais de {0} caracteres."),
            minlength: jQuery.validator.format("Insira pelo menos {0} caracteres."),
        });

        $('#form-checkout__submit').click(function (){
            processar_pagamento();
        });

        <?php if ($cobrancaFatura->errorProcessamento){?>
            $('.container__payment').fadeOut(0);
            setTimeout(() => { $('.container__result').show(0).fadeIn(); }, 100);

            document.getElementById("error-message").innerText = '<?php echo str_replace("'", "", $cobrancaFatura->errorProcessamento);?>'
            document.getElementById("fail-response").style.display = "block";
        <?php } ?>

        $('#nova-tentativa').click(function(){
            setTimeout(() => {
                $('.container__result').fadeOut(0);
                $('.container__payment').show(0).fadeIn();
            }, 100);
        });

        $('#form-checkout__cardNumber').keyup(function (event) {
            mascaraCartaoCredito( this, mcc );
        });

        $('#form-checkout__cardholderName').on('keyup', (ev) => {
            $('#form-checkout__cardholderName').val($('#form-checkout__cardholderName').val().toUpperCase());
        });

        $('#form-checkout__identificationNumber').keyup(function (event){
            mascaraCpf( this, formataCPF );
        });

        $('input[name="docNumber"]').keyup(function (event) {
            mascaraCpf( this, formataCPF );
            if (this.value.length === 11) {
                if (!valida_cpf(this.value)) {
                    alert('CPF inválido!');
                    this.value = '';
                    this.focus();
                    return false;
                }
            }
        });

        $('#form-checkout__telNumber').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $( "#cep_titular_valepay" ).blur(function() {
            mascara(this, mcep );
            getConsultaCEPTitularValePay();
        });

        $( "#cep_titular_valepay" ).keyup(function() {
            mascara(this, mcep );
            getConsultaCEPTitularValePay();
        });

        popular_table_parcelamento_valepay();
    });

    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    function mcep(v){
        v=v.replace(/\D/g,"")                    //Remove tudo o que n?o ? d?gito
        v=v.replace(/^(\d{5})(\d)/,"$1-$2") 	//Esse ? t?o f?cil que n?o merece explica??es
        return v
    }

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCPF(cpf){
        cpf = cpf.replace(/[^\d]/g, "");
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function popular_table_parcelamento_valepay() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>appcompra/simulation_installments_valepay",
            data: {
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                valor_total_taxas: <?php echo $fatura->valorpagar;?>,
                tipo_cobranca: <?php echo $fatura->tipoCobranca;?>,
                produtoID : <?=$ProdutoID;?>,
                istaxasComissaoAtivo : <?=$istaxasComissaoAtivo;?>,

            },
            dataType: 'json',
            success: function (parcelas) {

                let table = $('#tb-parcelamento-valepay tbody');
                let dataRow;

                table.empty();

                if (parcelas.status === 'success') {

                    for (let i = 0; i < parcelas.data.length; i++) {

                        dataRow = $('<tr>', {style: 'border-bottom: 1px solid #ccc;padding: 15px;font-size: 20px;font-weight: 400;'});

                        let numero_parcela = parcelas.data[i].parcela
                        let valor_parcela = parcelas.data[i].valor_parcela;
                        let totalPagar = parcelas.data[i].total;
                        let sem_juros = parcelas.data[i].sem_juros;

                        var radioButton = $("<input>", {type: "radio", required: 'required', name: "parcelamento_valepay", style: 'width: 25px;height: 30px;border: 1px solid #ccc;cursor: pointer;', value: numero_parcela});
                        let dataCellRadioSelecao = $('<td>').append(radioButton);
                        let dataCellParcela = $('<td>').text(numero_parcela + 'X');
                        let dataCellValorParcela

                        if (sem_juros) {
                            dataCellValorParcela = $('<td>').text('R$'+formatMoney(valor_parcela) + ' (Sem Juros)');
                        } else {
                            dataCellValorParcela = $('<td>').text('R$'+formatMoney(valor_parcela) + ' (' + 'R$'+formatMoney(totalPagar) + ')');
                        }

                        dataRow.append(dataCellRadioSelecao);
                        dataRow.append(dataCellParcela);
                        dataRow.append(dataCellValorParcela);

                        table.append(dataRow);
                    }

                    $('#form-checkout__cardholderName').focus();
                }

            }
        });
    }

    function processar_pagamento() {

        if ($('#form-checkout').valid()) {

            $.blockUI({
                overlayCSS: { backgroundColor: '#007bff' } ,
                message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                css: {width: '70%', left: '15%'}
            });

            $('#form-checkout__submit').prop('disabled', true);
            $('#form-checkout').submit();

        }
    }

    function mascaraCartaoCredito(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracartaocredito()",1)
    }

    function execmascaracartaocredito(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mcc(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{4})(\d)/g,"$1 $2");
        v=v.replace(/^(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3");
        v=v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3 $4");
        return v;
    }

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formatMoney(x, symbol) {
        if(!symbol) { symbol = ""; }
        if(site.settings.sac == 1) {
            return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
                ''+formatSA(parseFloat(x).toFixed(site.settings.decimals)) +
                (site.settings.display_symbol == 2 ? site.settings.symbol : '');
        }
        var fmoney = accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
        return fmoney;
    }

    function getConsultaCEPTitularValePay() {

        if($.trim($("#cep_titular_valepay").val()) === "") {
            return false;
        }

        if ($('#cep_titular_valepay').val().length === 9) {

            var cep = $.trim($("#cep_titular_valepay").val());
            cep = cep.replace('-', '');
            cep = cep.replace('.', '');
            cep = cep.replace(' ','');

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url,
                function (data) {
                    if (data !== null) {
                        $("#endereco_titular_valepay").val(data.logradouro);
                        $("#bairro_titular_valepay").val(data.bairro);
                        $("#cidade_titular_valepay").val(data.localidade);
                        $("#estado_titular_valepay").val(data.uf);

                        $('#numero_endereco_titular_valepay').focus();
                    }
                });
        }
    }

</script>
</body>
</html>