<!DOCTYPE html>
<html>
	<title>Relat&oacute;rio de acessos da viagem <?php echo $product->name; ?></title>
 	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
   	<link href="<?= $assets ?>poltronas_arquivos/layout-pt.css" rel="stylesheet" type="text/css" media="all">
 	<link href="<?= $assets ?>poltronas_arquivos/bus.css" rel="stylesheet"  type="text/css" media="screen">
    <body>
        <?php include 'bus_template_1_andar.php';?>
      
 	  <script>
			function mostra_venda(tab) {
				var venda = tab.getAttribute("venda");
				if (venda != '') {
					var customer = tab.getAttribute("customer");
					if (confirm('Passageiro '+customer+' \nDeseja abrir os dados da poltrona?')) {
						var url = '<?php echo base_url() ?>sales/edit/' + venda;
						window.open(url,'_blank');
					}
				} else {
					alert('Assento nao atribuido a nenhum passageiro');
				}
			}
	  </script>
   </body>
</html>