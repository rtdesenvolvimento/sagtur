<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CommissionsService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        //model
        $this->load->model('model/Commission_model', 'Commission_model');
        $this->load->model('model/CommissionItem_model', 'CommissionItem_model');

        //service
        $this->load->model('service/CommissionEventService_model', 'CommissionEventService_model');
        $this->load->model('service/FechamentoComissaoService_model', 'FechamentoComissaoService_model');

        //repository
        $this->load->model('repository/CommissionsRepository_model', 'CommissionsRepository_model');
        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');

    }

    public function cancelarComissao($saleId, $note = 'Comissão cancelada por alteração na venda')
    {
        try {

            $this->__iniciaTransacao();

            $encontrouComissao = $this->CommissionsRepository_model->getComissionBySaleID($saleId);

            //buscar todos os itens de comissao
            if ($encontrouComissao) {

                $comissoesItens = $this->CommissionsRepository_model->getAllCommissionItensByComissionID($encontrouComissao->id);

                foreach ($comissoesItens as $comissoesItem) {

                    $itemFechamento = $this->FechamentoComissaoRepository_model->getItemFechamentoByComissaoItemID($comissoesItem->id);

                    if ($itemFechamento) {
                        $this->FechamentoComissaoService_model->removeItemFechamento($itemFechamento->id, $itemFechamento->commission_item_id, $itemFechamento->close_commission_id);
                    }

                    $itemFechamentoComissao = $this->CommissionsRepository_model->getComissionsItemByID($comissoesItem->id);

                    if ($itemFechamentoComissao->status !== 'Cancelada') {
                        $this->cancelCommissionItem($note, $comissoesItem->id);
                    }

                }
                $this->__update('commissions', array('status' => Commission_model::COMISSION_CANCELADA), 'id', $encontrouComissao->id);
            }

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function save($saleId): int
    {
        try {

            $this->__iniciaTransacao();

            $sale           = $this->CommissionsRepository_model->getInvoiceByID($saleId);
            $commission_id  = false;

            $this->cancelarComissao($saleId);

            if ($sale->grand_total > 0) {

                if ($this->session->userdata('user_id')) {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user = $this->site->getUserByBiller($sale->biller_id);
                    $user_id = $user->id;
                }

                $commission = new Commission_model();

                $reference = $this->site->getReference('comm');

                $commission->reference_no      = $reference;
                $commission->status            = Commission_model::COMISSION_PENDENTE;
                $commission->subtotal          = $sale->grand_total;
                $commission->sale_id           = $sale->id;

                //log
                $commission->date              = date('Y-m-d H:i:s');
                $commission->created_by        = $user_id;
                $commission->updated_by        = $user_id;

                $commission->created_at        = date('Y-m-d H:i:s');
                $commission->updated_at        = date('Y-m-d H:i:s');

                $commission_id  = $this->__save('commissions', $commission->__toArray());

                $total_comissao = $this->saveAllItens($commission_id, $sale);

                $this->site->updateReference('comm');

                $this->__update('commissions', array('total_commission' => $total_comissao), 'id', $commission_id);
            }

            $this->__confirmaTransacao();

            return $commission_id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function editCommissionItem($data, $note, $itemID)
    {
        try {

            $this->__iniciaTransacao();

            $item = $this->CommissionsRepository_model->getComissionsItemByID($itemID);

            if ($data['commission'] <= 0) {
                throw new Exception('O valor da comissão deve ser maior que zero');
            }

            if ($item->status != 'Pendente') {
                throw new Exception('Não é possível alterar uma comissão que não está pendente');
            }

            $note = 'Comissão do item alterada de ' . $this->sma->formatMoney($item->commission) .
                ' para ' . $this->sma->formatMoney($data['commission']) . ' e o ' .
                ' percentual de ' . $this->sma->formatNumber($item->commission_percentage) . '% para ' . $this->sma->formatNumber($data['commission_percentage']) .'%'.
                ' - ' . $note;

            $this->__update('commission_items', $data, 'id', $itemID);

            $total_comissao = $this->updateTotalComission($item->commission_id);

            $this->CommissionEventService_model->addEvent($item->commission_id, $itemID, $note, CommissionEventService_model::VALOR_COMISSAO_ALTERADO, $this->session->userdata('user_id'));

            $this->__confirmaTransacao();

            $update = $this->__update('commissions', array('total_commission' => $total_comissao, 'updated_by' => $this->session->userdata('user_id') , 'updated_at' => date('Y-m-d H:i:s') ), 'id', $item->commission_id);

            return $update;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function cancelCommissionItem($note, $itemID): bool
    {
        try {

            $this->__iniciaTransacao();

            $item = $this->CommissionsRepository_model->getComissionsItemByID($itemID);


            if ($this->session->userdata('user_id')) {
                $user_id = $this->session->userdata('user_id');
            } else {
                $user = $this->site->getUserByBiller($item->biller_id);
                $user_id = $user->id;
            }


            if ($item->status != 'Pendente') {
                throw new Exception('Não é possível cancelar uma comissão que não está pendente. Verifiquei se um dos passageiros tem comissão já paga. Estorne a comissão para realizar a edição.');
            }

            $note = 'Comissão cancelada - ' . $note;

            $this->__update('commission_items', array('status'=> 'Cancelada', 'updated_by' => $this->session->userdata('user_id') , 'updated_at' => date('Y-m-d H:i:s')), 'id', $itemID);

            $this->CommissionEventService_model->addEvent($item->commission_id, $itemID, $note, CommissionEventService_model::COMISSAO_CANCELADA, $user_id);

            $total_comissao = $this->updateTotalComission($item->commission_id);

            $atualizado = $this->__update('commissions', array('total_commission' => $total_comissao, 'updated_by' => $this->session->userdata('user_id') , 'updated_at' => date('Y-m-d H:i:s') ), 'id', $item->commission_id);

            $this->__confirmaTransacao();

            return $atualizado;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function updateTotalComission($commission_id) {

        $itens = $this->CommissionsRepository_model->getAllCommissionItensByComissionID($commission_id);

        $total_comissao = 0;
        foreach ($itens as $item) {
            if ($item->commission > 0) {
                $total_comissao += $item->commission;
            }
        }

        return number_format($total_comissao, 2, '.', '');
    }

    private function saveAllItens($commission_id, $sale)
    {
        $itens = $this->CommissionsRepository_model->getAllInvoiceItems($sale->id);

        $total_comissao = 0;

        foreach ($itens as $item) {
            if ($item->subtotal > 0) {
                $total_comissao += $this->save_commission_item($commission_id, $sale, $item);
            }
        }

        return number_format($total_comissao, 2, '.', '');
    }

    private function save_commission_item($commission_id, $sale, $item): float
    {

        $product = $this->site->getProductByID($item->product_id);

        $commissionValue = 0;

        if ($product->isComissao) {

            $commisionItem  = new CommissionItem_model();

            if ($this->session->userdata('user_id')) {
                $user_id = $this->session->userdata('user_id');
            } else {
                $user = $this->site->getUserByBiller($sale->biller_id);
                $user_id = $user->id;
            }

            $biller         = $this->site->getCompanyByID($sale->biller_id);
            $customer       = $this->site->getCompanyByID($item->customerClient);

            $commisionItem->status                  = Commission_model::COMISSION_PENDENTE;
            $commisionItem->commission_id           = $commission_id;

            $commisionItem->sale_id                 = $sale->id;

            $commisionItem->biller                  = $biller->name;
            $commisionItem->biller_id               = $biller->id;

            $commisionItem->customer                = $customer->name;
            $commisionItem->customer_id             = $customer->id;

            $commisionItem->commission_type_product = $product->tipoComissao;
            $commisionItem->product_name            = $item->product_name;
            $commisionItem->product_id              = $item->product_id;
            $commisionItem->programacao_id          = $item->programacaoId;

            $totalVenda = $sale->total;
            $descontoVenda = $sale->order_discount;
            $acrescimoVenda = $sale->shipping;
            $totalItem = $item->subtotal;

            // Calcular a proporção do desconto para cada item
            $proporcaoDesconto = $descontoVenda / $totalVenda;

            // Calcular a proporção do acréscimo para cada item
            $proporcaoAcrescimo = $acrescimoVenda / $totalVenda;

            // Aplicar a proporção do desconto e do acréscimo ao subtotal do item
            $descontoItem = $totalItem * $proporcaoDesconto;
            $acrescimoItem = $totalItem * $proporcaoAcrescimo;

            // Calcular o subtotal do item após desconto e acréscimo
            $totalItemAposDescontoEAcrescimo = $totalItem - $descontoItem + $acrescimoItem;

            //calcular comissao item
            $comissionResult =  $this->calcular_comissao($product, $sale->biller_id, $totalItemAposDescontoEAcrescimo);

            $commissionValue = $comissionResult['comissao'];

            if ($commissionValue > 0) {

                $totalItemAposDescontoEAcrescimo = number_format($totalItemAposDescontoEAcrescimo, 2, '.', '');

                $commisionItem->commission              = $commissionValue;
                $commisionItem->commission_percentage   = $comissionResult['percentualComissao'];
                $commisionItem->base_value              = $totalItemAposDescontoEAcrescimo;

                //log manutencao
                $commisionItem->created_by        = $user_id;
                $commisionItem->created_at        = date('Y-m-d H:i:s');

                $commisionItem->updated_by        = $user_id;
                $commisionItem->updated_at        = date('Y-m-d H:i:s');

                $commission_item_id = $this->__save('commission_items', $commisionItem->__toArray());

                $note = 'Comissão gerada no valor de ' . $this->sma->formatMoney($commissionValue) . ' e ' .
                    ' percentual de ' . $this->sma->formatNumber($commisionItem->commission_percentage) .'%';

                $this->CommissionEventService_model->addEvent($commission_id, $commission_item_id, $note, CommissionEventService_model::COMISSAO_GERADA, $user_id);

            }
        }

        return number_format($commissionValue, 2, '.', '');
    }

    private function calcular_comissao($product, $biller_id, $subTotal)
    {
        $tipoComissaoProduto    = $product->tipoComissao;
        $comissao               = 0;

        if ($tipoComissaoProduto == 'comissao_produto') {
            $comissao = $this->comissao_produto($product, $subTotal);
        } else if ($tipoComissaoProduto === 'comissao_vendedor' ) {
            $comissao = $this->comissao_vendedor($biller_id, $subTotal);
        } else if ($tipoComissaoProduto == 'comissao_categoria') {
            $comissao = $this->comissao_categoria($product, $subTotal);
        }

        return $comissao;
    }

    private function comissao_produto($product, $subTotalVenda = 0.00 ): array
    {
        return $this->getComissao($product->tipoCalculoComissao, $product->comissao, $subTotalVenda);
    }

    private function comissao_vendedor($biller_id, $subTotalVenda = 0.00): array
    {
        $vendedor = $this->site->getCompanyByID($biller_id);
        return $this->getComissao($vendedor->tipo_comissao, $vendedor->comissao, $subTotalVenda);
    }

    private function comissao_categoria($product, $subTotalVenda = 0.00): array
    {
        $cagegoria = $this->site->getCategoryByID($product->category_id);
        return $this->getComissao($cagegoria->tipo_comissao, $cagegoria->comissao, $subTotalVenda);
    }

    private function getComissao($tipoCalculoComissao, $percentualComissao, $subTotalVenda): array
    {
        $percentualComissao = floatval($percentualComissao);
        $subTotalVenda      = floatval($subTotalVenda);

        if ($tipoCalculoComissao == '1') {
            $comissao = $subTotalVenda * ($percentualComissao/100);
        } else {
            $comissao = $percentualComissao;

            // Calculando o percentual com base no subTotalVenda
            $percentualComissao = ($comissao / $subTotalVenda) * 100;
        }

        $comissaoFormatado = number_format($comissao, 2, '.', '');
        $percentualComissaoFormatado = number_format($percentualComissao, 2, '.', '');

        return ['comissao' => $comissaoFormatado, 'percentualComissao' => $percentualComissaoFormatado];
    }

}