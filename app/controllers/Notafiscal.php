<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notafiscal extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('service/NotafiscalService_model', 'NotafiscalService_model');

        $this->lang->load('nf', $this->Settings->user_language);
    }

    function index() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('help')));
        $meta = array('page_title' => lang('help'), 'bc' => $bc);
        $this->page_construct('nf/index', $meta, $this->data);
    }

    function agendamento() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('nf_agendamento')));
        $meta = array('page_title' => lang('nf_agendamento'), 'bc' => $bc);
        $this->page_construct('nf/nf_agendamento', $meta, $this->data);
    }

    function getNFAGendamentos()
    {
        $this->load->library('datatables');

        $emitir_agendamento = "<a href='#' class='po' title='" . lang("emitir_agendamento") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger cp-read' href='" . site_url('notafiscal/emitir_agendamento/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-refresh'></i> "
            . lang('emitir_agendamento') . "</a>";

        $cancelar_notafiscal = "<a href='#' class='po' title='" . lang("cancelar_notafiscal") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger cp-read' href='" . site_url('notafiscal/cancelar_notafiscal/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash'></i> "
            . lang('cancelar_notafiscal') . "</a>";

        $excluir_emitir_agendamento = "<a href='#' class='po' title='" . lang("excluir_agendamento") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger cp-read' href='" . site_url('notafiscal/excluir_agendamento/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-times'></i> "
            . lang('excluir_agendamento') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $emitir_agendamento . '</li>
                    <li>' . $cancelar_notafiscal . '</li>
                    <li class="divider"></li>
                    <li>' . $excluir_emitir_agendamento . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("nf_agendamentos.id as id,
                companies.name as customer,
                valor_nf,
                observations,
                effectiveDate,
                status, 
                 ")
            ->from("nf_agendamentos")
            ->join('companies', 'companies.id=nf_agendamentos.customer_id', 'left');

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function emitir_agendamento($nfa_id)
    {
        try {

            $this->NotafiscalService_model->emitir_agendamento($nfa_id);

            sleep(1);

            if ($this->input->is_ajax_request()) {
                die();
            }

            $this->session->set_flashdata('message', lang('nf_emitida'));
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }

        if ($this->input->is_ajax_request()) {
            die();
        }
        redirect('notafiscal/agendamento');
    }

    public function cancelar_notafiscal($nfa_id)
    {
        try {
            $this->NotafiscalService_model->cancelar_notafiscal($nfa_id);
            $this->session->set_flashdata('message', lang('nf_cancelada'));
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }
        redirect('notafiscal/agendamento');
    }

    public function excluir_agendamento($nfa_id)
    {
        try {
            $this->NotafiscalService_model->excluir_agendamento($nfa_id);
            $this->session->set_flashdata('message', lang('nf_agendamento_excluida'));
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }
        redirect('notafiscal/agendamento');
    }

    public function expense_nfa_actions()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {

                if ($this->input->post('form_action') == 'emitir_notasfiscal') {

                    $nfa = $this->NotafiscalService_model->getAgendamentoByID($_POST['val']);

                    if ($nfa->status == NFAgendamento::GERADA || $nfa->status == NFAgendamento::AGENDADA) {
                        //$this->NotafiscalService_model->emitir_agendamento($_POST['val']);
                        sleep(1);
                    }
                }

            } else {
                $this->session->set_flashdata('error', lang("no_nf_selecao_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', lang("no_nf_selecao_selected"));
            redirect($_SERVER["HTTP_REFERER"]);
        }


    }

}?>