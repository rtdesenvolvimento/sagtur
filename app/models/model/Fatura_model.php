<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Fatura_model extends CI_Model
{
    const STATUS_ABERTA = 'ABERTA';

    const STATUS_CANCELADA = 'CANCELADA';
    const STATUS_REPARCELADA = 'REPARCELADA';
    const STATUS_REEMBOLSO = 'REEMBOLSO';
    const STATUS_PARCIAL = 'PARCIAL';

    const STATUS_QUITADA = 'QUITADA';


    public $id;
    public $dtvencimento;
    public $warehouse;
    public $tipocobranca;
    public $valorfatura;
    public $pessoa;
    public $receita;
    public $totalAcrescimo;
    public $totalDesconto;
    public $valorpagar;
    public $nota;
    public $movimentador;

    public function __construct() {
        parent::__construct();
    }

}
