<style>
    .div_sr {
        width: 100%;
        height: 300px;
        overflow: auto;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_fechamento_comissao'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'form');
        echo form_open("commissions/edit_fechamento/".$fechamento->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("dt_competencia", "dt_competencia"); ?>
                        <?php echo form_input('dt_competencia', $fechamento->dt_competencia, 'class="form-control input-tip" id="dt_competencia" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <?= lang('title', 'title'); ?>
                        <?= form_input('title', $fechamento->title, 'class="form-control" id="title" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><i class="fa fa-user"></i> <?= lang('info_header_billers'); ?></div>
                        <div class="panel-body div_sr">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="attrTableVendedores" class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;">
                                            <thead>
                                            <tr class="active">
                                                <th  style="text-align: right;width: 2%;"><input class="checkbox checkbillers" type="checkbox" name="check"/></th>
                                                <th class="col-md-10" style="text-align: left;"><?= lang('name') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($billers as $biller) {
                                                $isBillerActive = $this->FechamentoComissaoRepository_model->isBillerActive($biller->id, $fechamento->id); ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo form_checkbox('ativarBiller[]', $biller->id, $isBillerActive,'class="checkbillersfc"');?></td>
                                                    <td>
                                                        <?= form_input('biller_id[]',   $biller->id, '', 'hidden') ?>
                                                        <span><?php echo $biller->name;?></span>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12"">
                    <div class="form-group">
                        <?= lang("note", "note"); ?>
                        <?php echo form_textarea('note', $fechamento->note, 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_fechamento_comissao', lang('edit_fechamento_comissao'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('ifChecked', '.checkbillers', function(event) {
            $('.checkbillersfc').iCheck('check');
        });
        $(document).on('ifUnchecked', '.checkbillers', function(event) {
            $('.checkbillersfc').iCheck('uncheck');
        });
    });
</script>