<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update369 extends CI_Migration {

    public function up() {

        $this->create_table_vale_pay();
        $this->create_table_tempo();
        $this->insert_tempo();
        $this->create_index();

        $this->create_table_asaas();

        $this->db->update('settings',  array('version' => '3.6.9'), array('setting_id' => 1));
    }

    public function create_table_tempo() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'mes' => array('type' => 'INT', 'constraint' => 11 ),
            'trimestre' => array('type' => 'INT', 'constraint' => 11 ),
            'semestre' => array('type' => 'INT', 'constraint' => 11 ),
            'mes_extenso' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'data' => array('type' => 'DATE', 'null' => TRUE ),
            'bimestre' => array('type' => 'INT', 'constraint' => 11 ),
            'ano' => array('type' => 'INT', 'constraint' => 11 ),
            'dia_semana_extenso' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'dia' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tempo', TRUE, $attributes);
    }

    public function create_table_asaas() {

        $this->db->query('DROP TABLE sma_assas');

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'token' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('asaas', TRUE, $attributes);

        $data_assas = array(
            'token' => '#',
            'note' => 'Integração SAGTur com Asaas - Boleto, Carnê (Parcelamento) e Pix',
        );
        $this->db->insert('asaas', $data_assas);
    }

    public function insert_tempo() {
        try {

            for ($ano=2020;$ano<=2030;$ano++) {
                $this->db->query("INSERT INTO sma_tempo (data, mes, trimestre, semestre, mes_extenso, bimestre, ano, dia_semana_extenso, dia)
                                SELECT DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY) as data, MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) as mes, 
                                    QUARTER(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) as trimestre, 
                                CASE WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 1 AND 6 THEN 1 ELSE 2 END as semestre, 
                                DATE_FORMAT(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY), '%M') as mes_extenso,
                                CASE WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 1 AND 2 THEN 1 
                                WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 3 AND 4 THEN 2 
                                WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 5 AND 6 THEN 3 
                                WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 7 AND 8 THEN 4 
                                WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 9 AND 10 THEN 5 
                                WHEN MONTH(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) BETWEEN 11 AND 12 THEN 6 END as bimestre,
                                YEAR(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) as ano,
                                DATE_FORMAT(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY), '%W') as dia_semana_extenso,
                                DAY(DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY)) as dia
                                FROM (
                                    SELECT a.N + b.N * 10 + c.N * 100 + 1 n
                                    FROM 
                                    (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
                                    ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
                                    ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) c
                                ) d
                                WHERE DATE_ADD('" .$ano. "-01-01', INTERVAL n DAY) <= '" .$ano. "-12-31'");
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }
    public function create_index() {
        try {
            $this->db->query('CREATE INDEX indxs_sma_tempo_data	ON sma_tempo (data)');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function create_table_vale_pay() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'app_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'public_key' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'private_key' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('vale_pay', TRUE, $attributes);

        $data_vale_pay = array(
            'note' => 'Integração SAGTur com Asaas - Boleto, Carnê (Parcelamento) e Pix',
        );
        $this->db->insert('vale_pay', $data_vale_pay);
    }

    public function down() {}
}
