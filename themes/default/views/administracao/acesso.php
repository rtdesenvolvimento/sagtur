<?php

function getStatus($status, $dias) {
    if ($status == 'ABERTA') {
        if ($dias < 0)  return '<div class="text-center"><span class="label label-danger">Vencida</span></div>';
        else return '<div class="text-center"><Vencerspan class="label label-warning">À Vencer</Vencerspan></div>';
    } else if ($status == 'NAO ENCONTRADO') {
        return  '<div class="text-center"> - </div>';
    }

    return  '<div class="text-center"><span class="label label-success">Pago</span></div>';
}
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('Empresas'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="CusData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="text-align: left;width: 90%"><?= lang("company"); ?></th>
                            <th class="center" style="width: 10%;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($customers as $customer) {
                            $cnpj = $customer['cnpj'];
                            ?>
                            <tr>
                                <td style="text-align: left;color: #428bca;" class="bold"><?php echo $customer['name'];?></td>
                                <td style="text-align: center;">
                                    <form action="https://sistema.sagtur.com.br/auth/login" method="post" accept-charset="utf-8" target="_blank">
                                        <input type="hidden" name="token" value="d774f9a4314a1220962db98e187aa972" />
                                        <input type="hidden" name="cnpjempresa" value="<?php echo $cnpj;?>" />
                                        <input type="hidden" name="identity" value="andre@resultatec.com.br" />
                                        <input type="hidden" name="password" value="2078404abC@!" />
                                        <button type="submit"><i class="fa fa-external-link"></i> </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr class="primary">
                            <th style="text-align: left;width: 90%"><?= lang("company"); ?></th>
                            <th class="center" style="width: 10%;"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
      $('#CusData').DataTable({
          paging: true,       // Ativar paginação
          searching: true,    // Ativar busca
          ordering: true,     // Ativar ordenação
          info: true,         // Exibir informações
          pageLength: 500,    // Exibir 500 registros por página
          language: {
              url: "https://cdn.datatables.net/plug-ins/1.13.6/i18n/Portuguese-Brasil.json"
          }
      });
      
        $('.empresa_ck').on('ifChecked', function (e) {
            console.log('checked');

            if (confirm('Deseja Realmente ATIVAR essa empresa?')) {
                $.ajax({
                    type: "get",
                    url: site.base_url + "administracao/ativar",
                    data: {
                        cnpj: $(this).attr('cnpj'),
                    },
                    dataType: "json",
                    success: function (data) {
                    }
                });
            } else {
                location.reload();
            }
        });

        $('.empresa_ck').on('ifUnchecked', function (e) {
            if (confirm('Deseja Realmente INATIVAR essa empresa?')) {
                $.ajax({
                    type: "get",
                    url: site.base_url + "administracao/inativar",
                    data: {
                        cnpj: $(this).attr('cnpj'),
                    },
                    dataType: "json",
                    success: function (data) {
                    }
                });
            } else {
                location.reload();
            }
        });
    });
</script>
