<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" style="margin-bottom:0;">
                    <tbody>
                     <tr>
                        <td colspan="2"> 
                        	<div style="max-width:200px; margin: 0 auto;">
                                <?php if (strpos($customer->image, '.pdf') !== false ||
                                            strpos($customer->image, '.doc') !== false ||
                                            strpos($customer->image, '.docx') !== false||
                                            strpos($customer->image_storage, '.pdf') !== false||
                                            strpos($customer->image_storage, '.doc') !== false||
                                            strpos($customer->image_storage, '.docx') !== false){ ?>
                                    <?php if ($customer->image){?>
                                        <?=  '<a alt="" href="' . base_url() . 'assets/uploads/' . $customer->image.'" target="_blank" class="avatar">Ver o arquivo</a>';  ?>
                                    <?php } else if ($customer->image_storage) {?>
                                        <?=  '<a alt="" href="' . storage_img_url($customer->image_storage) .'" target="_blank" class="avatar">Ver o arquivo</a>';  ?>
                                    <?php } ?>
                                <?php } else {?>
                                    <?php if ($customer->image){?>
                                        <a href="<?= base_url() . 'assets/uploads/' . $customer->image; ?>" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages">
                                            <?=  '<img alt="" src="' . base_url() . 'assets/uploads/' . $customer->image.'" class="avatar">';  ?>
                                        </a>
                                    <?php } else if ($customer->image_storage) {?>
                                        <a href="<?= storage_img_url($customer->image_storage); ?>" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages">
                                            <?=  '<img alt="" src="' . storage_img_url($customer->image_storage) .'" class="avatar">';  ?>
                                        </a>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </td>
                     </tr>

                     <tr>
                         <td colspan="2">
                             <div id="multiimages" class="padding10">
                                 <?php if (!empty($images)) {
                                       if (strpos($customer->image, '.pdf') !== false ||
                                           strpos($customer->image, '.doc') !== false ||
                                           strpos($customer->image, '.docx') !== false||
                                           strpos($customer->image_storage, '.pdf') !== false||
                                           strpos($customer->image_storage, '.doc') !== false||
                                           strpos($customer->image_storage, '.docx') !== false) {
                                           //nao precisa
                                       } else {
                                           if ($customer->image) {
                                               echo '<a class="img-thumbnail" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages" href="' . base_url() . 'assets/uploads/' . $customer->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $customer->image . '" alt="' . $customer->image . '" style="width:' . $this->Settings->twidth . 'px; height:' . $this->Settings->theight . 'px;" /></a>';
                                           } elseif ($customer->image_storage) {
                                               echo '<a class="img-thumbnail" data-toggle="lightbox" data-gallery="multiimages" data-parent="#multiimages" href="' . storage_img_url($customer->image_storage)  . '" style="margin-right:5px;"><img class="img-responsive" src="' . storage_img_url($customer->image_storage)  . '" alt="' . $customer->image_storage . '" style="width:' . $this->Settings->twidth . 'px; height:' . $this->Settings->theight . 'px;" /></a>';
                                           }
                                       }
                                     foreach ($images as $ph) {
                                         if (strpos($ph->photo, '.pdf') !== false ||
                                             strpos($ph->photo, '.doc') !== false ||
                                             strpos($ph->photo, '.docx') !== false ||
                                             strpos($ph->photo_storage, '.pdf') !== false||
                                             strpos($ph->photo_storage, '.doc') !== false||
                                             strpos($ph->photo_storage, '.docx') !== false) {?>
                                             <?php if ($ph->photo){?>
                                                 <?=  '<a alt="" href="' . base_url() . 'assets/uploads/' . $ph->photo.'" target="_blank" class="avatar">Ver o arquivo</a>';  ?>
                                             <?php } else if ($ph->photo_storage) {?>
                                                 <?=  '<a alt="" href="' . storage_img_url($ph->photo_storage) .'" target="_blank" class="avatar">Ver o arquivo</a>';  ?>
                                             <?php } ?>
                                         <?php } else {
                                             if ($ph->photo) {
                                                 echo '<div class="gallery-image">
                                                        <a class="img-thumbnail" 
                                                        data-toggle="lightbox" 
                                                        data-gallery="multiimages" 
                                                        data-parent="#multiimages" 
                                                        href="' . base_url() . 'assets/uploads/' . $ph->photo . '" 
                                                        style="margin-right:5px;">
                                                            <img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" 
                                                            style="width:' . $this->Settings->twidth . 'px; 
                                                            height:' . $this->Settings->theight . 'px;" />
                                                        </a>';
                                                 echo '</div>';
                                             } else if($ph->photo_storage) {
                                                 echo '<div class="gallery-image">
                                                        <a class="img-thumbnail" 
                                                        data-toggle="lightbox" 
                                                        data-gallery="multiimages" 
                                                        data-parent="#multiimages" 
                                                        href="' . storage_img_url($ph->photo_storage)  . '" 
                                                        style="margin-right:5px;">
                                                            <img class="img-responsive" src="' . storage_img_url($ph->photo_storage) . '" alt="' . $ph->photo . '" 
                                                            style="width:' . $this->Settings->twidth . 'px; 
                                                            height:' . $this->Settings->theight . 'px;" />
                                                        </a>';
                                                 echo '</div>';
                                             }
                                         }
                                     }
                                 }
                                 ?>
                                 <div class="clearfix"></div>
                             </div>
                         </td>
                     </tr>

                     <?php if ( $customer->data_aniversario){ ?>
                        <tr>
                            <td><strong><?= lang("data_aniversario"); ?></strong></td>
                            <td><?= $this->sma->hrsd($customer->data_aniversario); ?></strong></td>
                         </tr>
                     <?php }?>
                    <tr>
                        <td><strong><?= lang("name"); ?></strong></td>
                        <td><?= $customer->name; ?></strong></td>
                    </tr>
                     <?php if ( $customer->social_name){ ?>
                         <tr>
                             <td><strong><?= lang("social_name"); ?></strong></td>
                             <td><?= $customer->social_name; ?></strong></td>
                         </tr>
                     <?php }?>
                     <?php if ( $customer->company){ ?>
                         <tr>
                             <td><strong><?= lang("company"); ?></strong></td>
                             <td><?= $customer->company; ?></strong></td>
                         </tr>
                     <?php }?>
                     <?php if ( $customer->nome_responsavel){ ?>
                         <tr>
                             <td><strong><?= lang("nome_responsavel"); ?></strong></td>
                             <td><?= $customer->nome_responsavel; ?></strong></td>
                         </tr>
                     <?php }?>
                    <tr>
                        <td><strong><?= lang("vat_no"); ?></strong></td>
                        <td><?= $customer->vat_no; ?></strong></td>
                    </tr>
                    <tr style="display: none;">
                        <td><strong><?= lang("award_points"); ?></strong></td>
                        <td><?= $customer->award_points; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("email"); ?></strong></td>
                        <td><?= $customer->email; ?></strong></td>
                    </tr>
                     <tr>
                         <td><strong><?= lang("ccf5"); ?></strong></td>
                         <td>
                             <?php
                             $whatsapp = $customer->cf5;

                             $whatsapp = str_replace('(', '', str_replace(')', '', $whatsapp));
                             $whatsapp = str_replace('-', '', $whatsapp);
                             $whatsapp = str_replace(' ', '', $whatsapp);
                             ?>
                             <a href="https://api.whatsapp.com/send?phone=55<?=trim($whatsapp);?>" target="_blank">
                                 <?= $customer->cf5; ?>
                             </a>
                             </strong></td>
                     </tr>
                     <tr>
                         <td><strong><?= lang("telefone_emergencia"); ?></strong></td>
                         <td><?= $customer->telefone_emergencia; ?></strong></td>
                     </tr>
                    <tr>
                        <td><strong><?= lang("phone"); ?></strong></td>
                        <td><?= $customer->phone; ?></strong></td>
                    </tr>
                     <tr>
                         <td><strong><?= strtoupper($customer->tipo_documento); ?></strong></td>
                         <td><?= $customer->cf1; ?></strong></td>
                     </tr>
                     <tr>
                         <td><strong><?= lang("ccf3"); ?></strong></td>
                         <td><?= $customer->cf3; ?></strong></td>
                     </tr>
                     <?php if ( $customer->validade_rg_passaporte){ ?>
                         <tr>
                             <td><strong>Emissão / Validade Documento</strong></td>
                             <td><?= $this->sma->hrsd($customer->validade_rg_passaporte); ?></strong></td>
                         </tr>
                     <?php }?>
                    <tr>
                        <td><strong><?= lang("address"); ?></strong></td>
                        <td><?= $customer->address.' '.$customer->numero; ?></strong></td>
                    </tr>
                     <tr>
                         <td><strong><?= lang("complemento"); ?></strong></td>
                         <td><?= $customer->complemento; ?></strong></td>
                     </tr>
                     <tr>
                         <td><strong><?= lang("bairro"); ?></strong></td>
                         <td><?= $customer->bairro; ?></strong></td>
                     </tr>
                    <tr>
                        <td><strong><?= lang("city"); ?></strong></td>
                        <td><?= $customer->city; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("state"); ?></strong></td>
                        <td><?= $customer->state; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("postal_code"); ?></strong></td>
                        <td><?= $customer->postal_code; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("country"); ?></strong></td>
                        <td><?= $customer->country; ?></strong></td>
                    </tr>

                    <tr>
                        <td><strong><?= lang("ccf4"); ?></strong></td>
                        <td><?= $customer->cf4; ?></strong></td>
                    </tr>
                     <?php if ( $customer->profession){ ?>
                         <tr>
                             <td><strong><?= lang("profession"); ?></strong></td>
                             <td><?= $customer->profession; ?></strong></td>
                         </tr>
                     <?php }?>

                     <?php if ( $customer->plano_saude){ ?>
                         <tr>
                             <td><strong><?= lang("plano_saude"); ?></strong></td>
                             <td><?= $customer->plano_saude; ?></strong></td>
                         </tr>
                     <?php }?>
                     <?php if ( $customer->alergia_medicamento){ ?>
                         <tr>
                             <td><strong><?= lang("alergia_medicamento"); ?></strong></td>
                             <td><?= $customer->alergia_medicamento; ?></strong></td>
                         </tr>
                     <?php }?>
                     <?php if ( $customer->doenca_informar){ ?>
                         <tr>
                             <td><strong><?= lang("doenca_informar"); ?></strong></td>
                             <td><?= $customer->doenca_informar; ?></strong></td>
                         </tr>
                     <?php }?>
                     <tr>
                         <td><strong><?= lang("observacao"); ?></strong></td>
                         <td><?= $customer->observacao; ?></strong></td>
                     </tr>
                    <tr>
                        <?php
                        $biller = $this->site->getCompanyByID($this->Settings->default_biller);
                        ?>
                        <td colspan="2"><?=$biller->invoice_footer;?></td>
                     </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['reports-customers']) { ?>
                    <a href="<?=site_url('reports/customer_report/'.$customer->id);?>" target="_blank" class="btn btn-primary"><?= lang('customers_report'); ?></a>
                <?php } ?>
                <?php if ($Owner || $Admin || $GP['customers-edit']) { ?>
                    <a href="<?=site_url('customers/edit/'.$customer->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><?= lang('edit_customer'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>