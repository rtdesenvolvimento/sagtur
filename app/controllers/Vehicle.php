<?php defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        //services
        $this->load->model('service/VehicleService_model', 'VehicleService_model');

        //repositorys
        $this->load->model('repository/TipoVeiculoRepository_model', 'TipoVeiculoRepository_model');
        $this->load->model('repository/VehicleRepository_model', 'VehicleRepository_model');
        $this->load->model('repository/CategoriaVeiculoRepository_model', 'CategoriaVeiculoRepository_model');
        $this->load->model('repository/TipoCombustivelRepository_model', 'TipoCombustivelRepository_model');
        $this->load->model('repository/StatusVeiculoRepository_model', 'StatusVeiculoRepository_model');

        //model
        $this->load->model('Tipocontrato_model', 'Tipocontrato_model');

        $this->lang->load('location', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '2048';//2MB
    }

    public function index() {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('vehicles')));
        $meta = array('page_title' => lang('vehicles'), 'bc' => $bc);

        $this->page_construct('vehicle/index', $meta, $this->data);
    }

    public function getVehicles() {
        $edit_link = anchor('vehicle/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_vehicle'), 'class="sledit"');

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $edit_link . '</li>
                </ul>
            </div></div>';

        $this->load->library('datatables');

        $this->datatables
            ->select("veiculo.id as id,
                    veiculo.photo as image,
                    veiculo.reference_no,
                    tipo_veiculo.name as tipo_veiculo,
                    categoria_veiculo.name as categoria,
                    veiculo.name as veiculo,
                    veiculo.placa,
                    veiculo.marca,
                    veiculo.modelo,
                    veiculo.cor,
                    concat(sma_status_veiculo.name, '@', sma_status_veiculo.cor) as status")
            ->from('veiculo')
            ->join('tipo_veiculo', 'tipo_veiculo.id = veiculo.tipo_veiculo', 'left')
            ->join('categoria_veiculo', 'categoria_veiculo.id = veiculo.categoria', 'left')
            ->join('status_veiculo', 'status_veiculo.id = veiculo.status_veiculo', 'left');

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function add() {

        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('placa', lang("placa"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('ve');

            $data_vehicle = array(
                'active' => 1,
                'status_veiculo' => 1,//TODO AQUI O STATUS CONFORME CONFIGURACAO
                'reference_no' => $reference,
                'name'      => $this->input->post('name'),
                'placa'      => $this->input->post('placa'),
                'marca'      => $this->input->post('marca'),
                'modelo'      => $this->input->post('modelo'),
                'cor'      => $this->input->post('cor'),
                'uf'      => $this->input->post('uf'),
                'ano'      => $this->input->post('ano'),
                'chassi'      => $this->input->post('chassi'),
                'tipo_combustivel'      => $this->input->post('tipo_combustivel'),
                'capacidade_min'      => $this->input->post('capacidade_min'),
                'capacidade_max'      => $this->input->post('capacidade_max'),
                'km_atual'      => $this->input->post('km_atual'),
                'cambio_automatico'      => $this->input->post('cambio_automatico'),
                'categoria'      => $this->input->post('categoria'),
                'tipo_veiculo'      => $this->input->post('tipo_veiculo'),
                'note'      => $this->input->post('note'),
            );

            $image = $this->adicionarFotoPrincipal(false);

            if ($image) $data_vehicle['photo'] = $image;

            $photos =  $this->adicionarFotosAdicionais();

        } elseif ($this->input->post('add_vehicle')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && ($id = $this->VehicleService_model->add($data_vehicle, $photos)) ) {
            $this->session->set_flashdata('message', lang("vehicle_added"));
            redirect('vehicle/edit/'.$id);

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('vechicles'), 'page' => lang('vechicles')),
                array('link' => '#', 'page' => lang('add_vehicle')));
            $meta = array('page_title' => lang('add_vehicle'), 'bc' => $bc);

            $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
            $this->data['categoriasVeiculo'] = $this->CategoriaVeiculoRepository_model->getAll();
            $this->data['tiposCombustivel'] = $this->TipoCombustivelRepository_model->getAll();

            $this->page_construct('vehicle/add', $meta, $this->data);
        }
    }

    public function edit($id = null) {

        if ($this->input->post('id')) $id = $this->input->post('id');

        $vehicle = $this->VehicleRepository_model->getByID($id);

        if (!$id || !$vehicle) {
            $this->session->set_flashdata('error', lang('vehicle_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('placa', lang("placa"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            $data_vehicle = array(
                'active' => $this->input->post('active'),
                'reference_no' => $reference,
                'name'      => $this->input->post('name'),
                'placa'      => $this->input->post('placa'),
                'marca'      => $this->input->post('marca'),
                'modelo'      => $this->input->post('modelo'),
                'cor'      => $this->input->post('cor'),
                'uf'      => $this->input->post('uf'),
                'ano'      => $this->input->post('ano'),
                'chassi'      => $this->input->post('chassi'),
                'tipo_combustivel'      => $this->input->post('tipo_combustivel'),
                'capacidade_min'      => $this->input->post('capacidade_min'),
                'capacidade_max'      => $this->input->post('capacidade_max'),
                'km_atual'      => $this->input->post('km_atual'),
                'cambio_automatico'      => $this->input->post('cambio_automatico'),
                'categoria'      => $this->input->post('categoria'),
                'tipo_veiculo'      => $this->input->post('tipo_veiculo'),
                'note'      => $this->input->post('note'),
            );

            $image = $this->adicionarFotoPrincipal(false);

            if ($image) $data_vehicle['photo'] = $image;

            $photos =  $this->adicionarFotosAdicionais();

        } elseif ($this->input->post('edit_vehicle')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->VehicleService_model->edit($data_vehicle, $photos, $id)) {
            $this->session->set_flashdata('message', lang("vehicle_updated"));
            redirect('vehicle/edit/'.$id);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('vechicles'), 'page' => lang('vechicles')),
                array('link' => '#', 'page' => lang('edit_vehicle')));
            $meta = array('page_title' => lang('edit_vehicle'), 'bc' => $bc);

            $this->data['vehicle'] = $vehicle;
            $this->data['images'] = $this->VehicleRepository_model->getPhotos($id);

            $this->data['tiposVeiculo'] = $this->TipoVeiculoRepository_model->getAll();
            $this->data['categoriasVeiculo'] = $this->CategoriaVeiculoRepository_model->getAll();
            $this->data['tiposCombustivel'] = $this->TipoCombustivelRepository_model->getAll();
            $this->data['statusVeiculo'] = $this->StatusVeiculoRepository_model->getAll();
            $this->data['tiposContrato'] = $this->Tipocontrato_model->getAll();

            $this->page_construct('vehicle/edit', $meta, $this->data);
        }
    }

    private function adicionarFotoPrincipal($add = TRUE) {

        $this->load->library('upload');

        if ($add) $photo = 'no_image.png';
        else $photo = false;

        if ($_FILES['photo']['size'] > 0) {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('photo')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $photo = $this->upload->file_name;

            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';

                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }

        return $photo;
    }

    private function adicionarFotosAdicionais() {

        $photos = null;
        $this->load->library('upload');

        if ($_FILES['userfile']['name'][0] == "") return NULL;

        $config['upload_path'] = $this->upload_path;
        $config['allowed_types'] = $this->image_types;
        $config['max_size'] = $this->allowed_file_size;
        $config['max_width'] = $this->Settings->iwidth;
        $config['max_height'] = $this->Settings->iheight;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $config['max_filename'] = 25;

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/add");
            } else {

                $pho = $this->upload->file_name;
                $photos[] = $pho;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $pho;
                $config['new_image'] = $this->thumbs_path . $pho;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;

                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $pho;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
            }
        }

        $config = NULL;
        $_FILES = $files;

        return $photos;
    }

    public function add_shedule() {
        $dia_semana = $this->input->get('dia_semana');

        $this->data['dia_semana'] = $dia_semana;

        $this->load->view($this->theme . 'vehicle/add_schedule', $this->data);
    }

    public function punctual_dates() {
        $this->load->view($this->theme . 'vehicle/punctual_dates', $this->data);
    }

    public function period_dates() {
        $this->load->view($this->theme . 'vehicle/period_dates', $this->data);
    }
    public function getVehicle($id = null) {
        $row = $this->VehicleRepository_model->getByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->name.' | '.$row->placa.' | '.$row->ano.' | '.$row->cor)));
    }

    function suggestions($term = NULL, $limit = NULL)
    {
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }

        if (strlen($term) < 1) {
            return FALSE;
        }

        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->VehicleRepository_model->suggestions($term, $limit);
        $this->sma->send_json($rows);
    }

}