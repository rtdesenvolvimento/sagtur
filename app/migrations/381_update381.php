<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update381 extends CI_Migration {

    public function up() {

        $this->create_table_regra_cobranca_locacao();
        $this->create_table_regra_cobranca_locacao_proporcao();
        $this->create_table_location_setting();

        $this->alter_table_settings();
        $this->alter_table_products();

        $this->create_table_tanque_veiculo();
        $this->create_table_retirada_veiculo();

        $this->create_table_devolucao_veiculo();

        $this->db->update('settings',  array('version' => '3.8.1'), array('setting_id' => 1));
    }

    public function create_table_retirada_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),

            'locacao_veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),

            'dataRetirada' => array('type' => 'DATE', 'null' => TRUE ),
            'horaRetirada' => array('type' => 'TIME', 'null' => TRUE ),
            'local_retirada_id' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),
            'tanque_veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
            'km_retirada_veiculo'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('retirada_veiculo', TRUE, $attributes);
    }

    public function create_table_devolucao_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),

            'locacao_veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),

            'dataDevolucao' => array('type' => 'DATE', 'null' => TRUE ),
            'horaDevolucao' => array('type' => 'TIME', 'null' => TRUE ),
            'local_devolucao_id' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),
            'tanque_veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
            'km_devolucao_veiculo'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('devolucao_veiculo', TRUE, $attributes);
    }


    public function create_table_location_setting() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'status_disponibilidade_disponivel_id' => array('type' => 'INT', 'constraint' => 11 ),
            'status_disponibilidade_reservado_id' => array('type' => 'INT', 'constraint' => 11 ),
            'status_disponibilidade_locado_id' => array('type' => 'INT', 'constraint' => 11 ),
            'status_disponibilidade_devolvido_id' => array('type' => 'INT', 'constraint' => 11 ),
            'status_disponibilidade_cancelado_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('location_setting', TRUE, $attributes);

        $this->db->insert('location_setting', array(
                'name' => 'Configuração de Locaçao Geral' ,
                'status_disponibilidade_disponivel_id' => 1,
                'status_disponibilidade_reservado_id' => 2,
                'status_disponibilidade_locado_id' => 3,
                'status_disponibilidade_cancelado_id' => 4,
                'status_disponibilidade_devolvido_id' => 5
            )
        );
    }

    public function alter_table_settings() {
        $fields = array(
            'location_setting_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('location_setting_id' => 1), array('setting_id' => 1));
    }

    public function create_table_regra_cobranca_locacao() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'tipo_franquia' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => 'POR_ITEM'  ),
            'periodo_cobranca' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'DIA'  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('regra_cobranca_locacao', TRUE, $attributes);

        $this->db->insert('regra_cobranca_locacao', array('name' => 'Contrato p/Hora', 'periodo_cobranca' => 'HORA'));
        $this->db->insert('regra_cobranca_locacao', array('name' => 'Contrato Diário', 'periodo_cobranca' => 'DIA'));
    }

    public function create_table_tanque_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'tipo_combustivel_id' => array('type' => 'INT', 'constraint' => 11 ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tanque_veiculo', TRUE, $attributes);

        //gasolina
        $this->db->insert('tanque_veiculo', array('name' => 'R', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '1/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '2/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '3/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '4/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '5/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '6/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => '7/8', 'tipo_combustivel_id' => 1));
        $this->db->insert('tanque_veiculo', array('name' => 'F', 'tipo_combustivel_id' => 1));

        //disel
        $this->db->insert('tanque_veiculo', array('name' => 'R', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '1/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '2/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '3/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '4/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '5/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '6/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => '7/8', 'tipo_combustivel_id' => 2));
        $this->db->insert('tanque_veiculo', array('name' => 'F', 'tipo_combustivel_id' => 2));

        //Etanol
        $this->db->insert('tanque_veiculo', array('name' => 'R', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '1/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '2/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '3/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '4/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '5/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '6/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => '7/8', 'tipo_combustivel_id' => 3));
        $this->db->insert('tanque_veiculo', array('name' => 'F', 'tipo_combustivel_id' => 3));

        //Flex (Gasolina + Etanol)
        $this->db->insert('tanque_veiculo', array('name' => 'R', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '1/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '2/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '3/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '4/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '5/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '6/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => '7/8', 'tipo_combustivel_id' => 4));
        $this->db->insert('tanque_veiculo', array('name' => 'F', 'tipo_combustivel_id' => 4));

        //GNV
        $this->db->insert('tanque_veiculo', array('name' => 'R', 'tipo_combustivel_id' => 5));
        $this->db->insert('tanque_veiculo', array('name' => '|', 'tipo_combustivel_id' => 5));
        $this->db->insert('tanque_veiculo', array('name' => '||', 'tipo_combustivel_id' => 5));
        $this->db->insert('tanque_veiculo', array('name' => '|||', 'tipo_combustivel_id' => 5));
        $this->db->insert('tanque_veiculo', array('name' => '||||', 'tipo_combustivel_id' => 5));
    }

    public function create_table_regra_cobranca_locacao_proporcao() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'regra_cobranca_locacao_id' => array('type' => 'INT', 'constraint' => 11 ),
            'inicio'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'fim'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'tolerancia'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'tipo_cobranca' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => 'ABSOLUTO'  ),
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'excedente' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('regra_cobranca_locacao_proporcao', TRUE, $attributes);

        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 1 , 'fim' => 1, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 2 , 'fim' => 2, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 3 , 'fim' => 3, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 4 , 'fim' => 4, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 5 , 'fim' => 5, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 6 , 'fim' => 6, 'valor' => 50.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 7 , 'fim' => 7, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 8 , 'fim' => 8, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 9 , 'fim' => 9, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 10 , 'fim' => 10, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 11 , 'fim' => 11, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 12 , 'fim' => 12, 'valor' => 100.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 13 , 'fim' => 13, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 14 , 'fim' => 14, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 15 , 'fim' => 15, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 16 , 'fim' => 16, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 17 , 'fim' => 17, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 18 , 'fim' => 18, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 19 , 'fim' => 19, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 20 , 'fim' => 20, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 21 , 'fim' => 21, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 22 , 'fim' => 22, 'valor' => 200.00 ,  'excedente' => 100.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 1, 'inicio' => 23 , 'fim' => 23, 'valor' => 200.00 ,  'excedente' => 100.00 ));


        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 1 , 'fim' => 1, 'valor' => 100.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 2 , 'fim' => 2, 'valor' => 200.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 3 , 'fim' => 3, 'valor' => 300.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 4 , 'fim' => 4, 'valor' => 400.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 5 , 'fim' => 5, 'valor' => 500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 6 , 'fim' => 6, 'valor' => 600.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 7 , 'fim' => 7, 'valor' => 700.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 8 , 'fim' => 8, 'valor' => 800.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 9 , 'fim' => 9, 'valor' => 900.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 10 , 'fim' => 10, 'valor' => 1000.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 11 , 'fim' => 11, 'valor' => 1100.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 12 , 'fim' => 12, 'valor' => 1200.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 13 , 'fim' => 13, 'valor' => 1300.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 14 , 'fim' => 14, 'valor' => 1400.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 15 , 'fim' => 15, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 16 , 'fim' => 16, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 17 , 'fim' => 17, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 18 , 'fim' => 18, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 19 , 'fim' => 19, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 20 , 'fim' => 20, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 21 , 'fim' => 21, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 22 , 'fim' => 22, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 23 , 'fim' => 23, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 24 , 'fim' => 24, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 25 , 'fim' => 25, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 26 , 'fim' => 26, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 27 , 'fim' => 27, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 28 , 'fim' => 28, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 29 , 'fim' => 29, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 30 , 'fim' => 30, 'valor' => 1500.00 ,  'excedente' => 200.00 ));
        $this->db->insert('regra_cobranca_locacao_proporcao', array('regra_cobranca_locacao_id' => 2, 'inicio' => 31 , 'fim' => 31, 'valor' => 1500.00 ,  'excedente' => 200.00 ));

    }

    public function alter_table_products() {
        $fields = array(
            'tipo_trajeto_id' => array('type' => 'INT', 'constraint' => 11 ),
            'color_agenda_id' => array('type' => 'INT', 'constraint' => 11 ),
            'executivo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'tipo_transfer' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//{1 - Somente ida, 2 - somente volta, 3 ida e volta}

            'tipo_duracao_atividade' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => 'HORAS' ),
            'duracao_atividade'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 1),

            'dias_vendas_antes_viagem'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'veiculo' => array('type' => 'INT', 'constraint' => 11 ),
            'fornecedor' => array('type' => 'INT', 'constraint' => 11 ),
            'fornecedor_terceirizado' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'transfer' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'locacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'passeio' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'ingresso' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'grupo_exclusivo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'senha_grupo' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),

            'destaque' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'incluir_home_site' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'incluir_texto_a_partir_de' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'unidade' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => 'por pessoa' ),

            //'usar_localizacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            //'cep' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            //'numero' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            //'ponto_encontro' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            //'complemento' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            //'cidade' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            //'estado' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
        );

        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
