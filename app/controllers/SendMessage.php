<?php defined('BASEPATH') or exit('No direct script access allowed');

class SendMessage extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //service
        $this->load->model('service/SendMessageService_model', 'SendMessageService_model');

    }

    public function index()
    {
    }

    public function sendText()
    {
        try {

            $message = 'como posso fazer para rezalizar uma consulta no interior do estado de são paulo?';
            $this->SendMessageService_model->send_text(urldecode($message), '18002428478');

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sendDocumentPDF()
    {
        try {
            $this->SendMessageService_model->send_document_pdf('https://sistema.sagtur.com.br/salesutil/pdf/11089?token=maistravelviagens', 'Meu arquivo de teste', '5548998235746');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function creatGroup()
    {
        try {
            $this->SendMessageService_model->create_group('Grupo de Teste', 'André Velho', ['5548998235746']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function removeParticipantGroup($groupID)
    {
        try {
            $this->SendMessageService_model->remove_participant_group($groupID, ['5548998235746']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function addParticipantGroup($groupID)
    {
        try {
            $this->SendMessageService_model->add_participant_group($groupID, ['5548998235746']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function addAdminGroup($groupID)
    {
        try {
            $this->SendMessageService_model->add_admin_group($groupID, ['5548998235746']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}