<?php

class PagSeguroController extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->config('pagseguro');
        $this->load->library('pagsegurolibrary');
    }

    function index()
    {
        show_404();
    }

    public function buscarDadosTransacaoByReference($code){

        $this->data['modal_js'] = $this->site->modal_js();

        try {

            $this->FinanceiroService_model->baixarPagamentoIntegracaoByReference('pagseguro', $code);

            $transaction = $this->FinanceiroService_model->buscarDadosTransacaoByReference($code);

            if ($transaction != null) {
                $this->data['transaction'] = $transaction;
                $this->load->view($this->theme . 'financeiro/info_fatura_pagseguro', $this->data);
            }

        } catch (Exception $exception) {
            $this->load->view($this->theme . 'financeiro/info_fatura_pagseguro_nao_passou_cartao', $this->data);
        }
    }

    public function notification()
    {
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        header("access-control-allow-origin: https://pagseguro.uol.com.br");

        $type = (isset ($_POST['notificationType']) && trim($_POST['notificationType']) !== "" ? trim($_POST['notificationType']) : null);
        $code = (isset ($_POST['notificationCode']) && trim($_POST['notificationCode']) !== "" ? trim($_POST['notificationCode']) : null);

        if ($code && $type) {
            $notificationType = new PagSeguroNotificationType($type);
            $strType = $notificationType->getTypeFromValue();

            switch ($strType) {
                case 'TRANSACTION':
                    $this->FinanceiroService_model->baixarPagamentoIntegracao('pagseguro', $code);
                    break;
                default : LogPagSeguro::error("Unknown notification type [" . $notificationType->getValue() . "]");
            }
        } else {
            LogPagSeguro::error("Invalid notification parameters.");
            self::printLog();
        }
        return TRUE;
    }

    private static function printLog($strType = null)
    {
        $count = 30;
        echo "<h2>Receive notifications</h2>";
        if ($strType) {
            echo "<h4>notifcationType: $strType</h4>";
        }
        echo "<p>Last <strong>$count</strong> items in <strong>log file:</strong></p><hr>";
        echo LogPagSeguro::getHtml($count);
    }
}