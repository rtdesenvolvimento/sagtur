<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RefundsRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFaturaByRefund($refund_id)
    {
        $this->db->select("fatura.id as faturaId, fatura.pessoa as customer_id, parcela.id parcelaId, conta_pagar.id as contaPagarId");
        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');
        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId');

        $this->db->where('conta_pagar.gift_cards_id', $refund_id);

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRefundByFatura($fatura_id)
    {
        $this->db->select("gift_cards.id as giftId, gift_cards.card_no, gift_cards.value, gift_cards.balance, gift_cards.expiry, gift_cards.note as note_gift");
        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');
        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId');
        $this->db->join('gift_cards', 'gift_cards.id = conta_pagar.gift_cards_id');

        $this->db->where('parcela_fatura.fatura', $fatura_id);

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
}
