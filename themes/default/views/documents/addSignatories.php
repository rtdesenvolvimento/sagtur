<style media="screen">


    .signatory {
        padding: 5px;
        border: 1px solid #ddd;
        background-color: #f6f6f6;
        box-shadow: none;
        margin-bottom: 10px;
    }

    .me_signatory {
        padding: 5px;
        box-shadow: none;
        margin-bottom: 10px;
        text-align: center;
    }

    .me_buttom {
        border-radius: 6px;
        color: #0461ff;
        outline: none;
        background-color: transparent;
        font-size: 1.2rem;
        font-weight: 400;
        width: 100%;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0;
        transition: all .1s ease;
        line-height: 1em;
        border: 1px solid #ddd;
    }

    .user-select-image {
        width: 40px;
        float: left;
    }

    .user-select-name {
        width: 100%;
        padding-left: 50px;
    }

</style>
<?php echo form_open("contracts/create_signatories/".$document->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-files-o"></i><?= lang('signatories_details'); ?></h2>
    </div>
    <div class="box-content">
        <p class="introtext"><?= lang('contracts_info'); ?></p>
        <div class="row">
            <div class="col-md-4">
                <div class="row">

                    <div class="col-md-12">
                        <ul id="myTab" class="nav nav-tabs" style="text-align: center">
                            <li><a href="#signatorys" class="tab-grey"><i class="fa fa-edit"></i> <?= lang('signatorys') ?></a></li>
                            <li><a href="#search_customers" class="tab-grey"><i class="fa fa-search-plus"></i> <?= lang('search_customers') ?></a></li>
                        </ul>
                        <div class="tab-content">

                            <!-- TAB Signatorys -->
                            <div id="signatorys" class="tab-pane fade in">
                                <div class="col-md-12 me_signatory">
                                    <button class="me_buttom" type="button">
                                        + Me adicionar à lista
                                    </button>
                                </div>

                                <div class="col-md-12 div_me_signatory"></div>

                                <?php
                                if (!empty($document->getSignatories())) {
                                    $contador_signatory = 2;?>
                                    <?php foreach ($document->getSignatories() as $signatory) {

                                        if ($signatory->biller_id) {
                                            continue;
                                        }
                                        ?>
                                        <div class="col-md-12 signatory">
                                            <input type="hidden" name="customer_id[]" value="<?=$signatory->customer_id;?>">
                                            <input type="hidden" name="biller_id[]" value="<?=$signatory->biller_id;?>">
                                            <div class="col-md-12" style="text-align: right; cursor: pointer;">
                                                <i class="fa fa-times remove-signatory<?=$contador_signatory;?>"></i>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <?php if ($signatory->customer_id){ ?>
                                                        <label for="signatory">Cliente Código <?=$signatory->customer_id;?></label>
                                                    <?php } else { ?>
                                                        <?= lang("signatory", "signatory"); ?>
                                                    <?php } ?>

                                                    <?php if ($signatory->customer_id) {?>
                                                        <?php echo form_input('signatory[]', $signatory->name, 'class="form-control tip signatory_name'. $contador_signatory .'" placeholder="' . lang("signatory_name") . '" readonly required="required"'); ?>
                                                    <?php } else { ?>
                                                        <?php echo form_input('signatory[]', $signatory->name, 'class="form-control tip signatory_name'. $contador_signatory .'" placeholder="' . lang("signatory_name") . '" required="required"'); ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("shipping_type", "shipping_type") ?>
                                                    <?php

                                                    if ($signatory->customer_id) {
                                                        if ($signatory->phone) {
                                                            $opts = array(
                                                                'telefone' => lang('signer_telefone'),
                                                            );
                                                        } else if($signatory->email) {
                                                            $opts = array(
                                                                'email' => lang('signer_email'),
                                                            );
                                                        } else {
                                                            $opts = array(
                                                                'telefone' => lang('signer_telefone'),
                                                                'email' => lang('signer_email'),
                                                            );
                                                        }
                                                    } else {
                                                        $opts = array(
                                                            'telefone' => lang('signer_telefone'),
                                                            'email' => lang('signer_email'),
                                                        );
                                                    }

                                                    if ($signatory->phone) {
                                                        $signature_type = 'telefone';
                                                    } else {
                                                        $signature_type = 'email';
                                                    }
                                                    echo form_dropdown('shipping_type[]', $opts, $signature_type, 'class="form-control shipping_type' . $contador_signatory . '" required="required"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php if ($signatory->phone) {?>
                                                        <?php if ($signatory->customer_id) {?>
                                                            <?php echo form_input('signer_field[]', $signatory->phone, 'placeholder="' . lang("telefone_to_signatario") . '" class="form-control tip signer_field' . $contador_signatory . '" readonly required="required"'); ?>
                                                        <?php } else { ?>
                                                            <?php echo form_input('signer_field[]', $signatory->phone, 'placeholder="' . lang("telefone_to_signatario") . '" class="form-control tip signer_field' . $contador_signatory . '" required="required"'); ?>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php if ($signatory->customer_id) {?>
                                                            <?php echo form_input('signer_field[]', $signatory->email, 'placeholder="' . lang("telefone_to_signatario") . '" class="form-control tip signer_field' . $contador_signatory . '" readonly required="required"'); ?>
                                                        <?php } else { ?>
                                                            <?php echo form_input('signer_field[]', $signatory->email, 'placeholder="' . lang("telefone_to_signatario") . '" class="form-control tip signer_field' . $contador_signatory . '" required="required"'); ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php $contador_signatory++;} ?>
                                <?php } ?>

                                <!-- Signatário Padrão -->
                                <div class="col-md-12 signatory" id="div_signatory_default" <?= !empty($document->getSignatories())  ? 'style="display:none;"' : '';?> >
                                    <input type="hidden" name="customer_id[]" value="">
                                    <input type="hidden" name="biller_id[]" value="">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <?= lang("signatory", "signatory"); ?>
                                            <?php echo form_input('signatory[]', '', 'class="form-control tip signatory_name1" placeholder="' . lang("signatory_name") . '" required="required"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("shipping_type", "shipping_type") ?>
                                            <?php
                                            $opts = array(
                                                'telefone' => lang('signer_telefone'),
                                                'email' => lang('signer_email'),
                                            );
                                            echo form_dropdown('shipping_type[]', $opts, 1 , 'class="form-control shipping_type1" required="required"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php echo form_input('signer_field[]', '', 'placeholder="' . lang("telefone_to_signatario") . '" class="form-control tip signer_field1" required="required"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM Signatário Padrão -->

                                <!-- add signatory -->
                                <div class="div_signatory"></div>
                                <!-- FIM add signatory -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php echo form_button('add_signatory', '<i class="fa fa-plus"></i> '.$this->lang->line("add_signatory"), 'id="add_signatory" style="width:100%;" class="btn btn-primary"'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- FIM TAB Signatorys -->

                            <!--TAB Consulta Cliente-->
                            <div id="search_customers" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("customer", "customer") ?>
                                            <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php echo form_button('add_customer', '<i class="fa fa-plus"></i> '.$this->lang->line("add_customer"), 'id="add_customer" class="btn btn-primary" style="width:100%;"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--FIM TAB Consulta Cliente-->


                        </div>
                    </div>
                </div>

                <!-- Inserir Equipe -->
                <div class="row" style="margin-top: 15px; display: none;">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-users"> </i> <?= lang('inserir_equipe'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div style="float: left;width: 40px;">
                                                <img src="https://aplicacao.sagtur.com.br/assets/uploads/avatars/thumbs/6a3cd62a7e71f089af03bd053699afa2.png"
                                                     alt="" style="width:40px; height:40px;border-radius: 40px;">
                                            </div>
                                            <div class="user-select-name">
                                                <h5>André Velho</h5>
                                                <p>sistema.sagtur@gmail.com</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="text-align: center;">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="attributes[]" checked value="1"> <?= lang('no'); ?>
                                                </label>
                                                <label>
                                                    <input type="radio" name="attributes[]" value="0"> <?= lang('yes'); ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim Inserir Equipe -->

                <!-- Configuracoes -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("signature_type", "signature_type") ?>
                            <?php
                            $opts = array(
                                'all' => lang('all_signature_type'),
                                'text' => lang('text_signature_type'),
                                'draw' => lang('draw_signature_type'),
                                'upload' => lang('upload_signature_type'),
                            );
                            echo form_dropdown('signature_type', $opts, $this->Settings->signature_type , 'class="form-control" required="required"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('solicitar_selfie', 1, $this->Settings->solicitar_selfie, 'id="solicitar_selfie"'); ?>
                            <label for="attributes" class="padding05"><?= lang('solicitar_selfie'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('solicitar_documento', 1, $this->Settings->solicitar_documento, 'id="solicitar_documento"'); ?>
                            <label for="attributes" class="padding05"><?= lang('solicitar_documento'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('auto_destruir_solicitacao', 1,  $this->Settings->auto_destruir_solicitacao, 'id="auto_destruir_solicitacao"'); ?>
                            <label for="attributes" class="padding05"><?= lang('auto_destruir_solicitacao'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12" style="<?php if(!$this->Settings->auto_destruir_solicitacao) echo 'display: none;';?>" id="div_data_vencimento_contrato">
                        <div class="form-group all">
                            <?= lang("data_vencimento_contrato", "data_vencimento_contrato") ?>

                            <?php if ($this->Settings->auto_destruir_solicitacao) {?>
                                <?= form_input('data_vencimento_contrato',   date('Y-m-d', strtotime("+" . $this->Settings->validade_auto_destruir_dias . " day", strtotime(date('y-m-d')))), 'class="form-control tip"', 'date') ?>
                            <?php } else { ?>
                                <?= form_input('data_vencimento_contrato',  '', 'class="form-control tip"', 'date') ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("signatories_note", "signatories_note"); ?>
                            <?php echo form_textarea('signatories_note', ($document->signatories_note != '' ? $document->signatories_note : $this->Settings->note_contract), 'class="form-control skip" placeholder="' . lang("signatory_note_placeholder") . '" id="signatories_note" style="margin-top: 10px; height: 200px;"'); ?>
                        </div>
                    </div>
                </div>
                <!-- Fim Configuracoes -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('send_subscription_request',  $this->lang->line("send_subscription_request"), 'id="send_subscription_request" class="btn btn-success"'); ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <iframe src="<?=site_url('contracts/downloadOriginalDocument/'.$document->id)?>" width="100%" height="900px"></iframe>
            </div>
        </div>
    </div>
</div>
<?=form_close()?>

<script type="text/javascript">

    var contador_signatory = <?= !empty($document->getSignatories()) ? count($document->getSignatories()) + 1 : 2; ?>;

    $(document).ready(function () {

        add_chagen_shipping_type(1);

        nsCustomer();

        <?php foreach ($document->getSignatories() as $signatory) {?>
            <?php if ($signatory->biller_id) {?>
                add_me();
            <?php break; } ?>
        <?php } ?>

        $('.signer_field1').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('.signatory_name1').focus();

        $('#auto_destruir_solicitacao').on('ifChecked', function (e) {$('#div_data_vencimento_contrato').show(300);});
        $('#auto_destruir_solicitacao').on('ifUnchecked', function (e) {$('#div_data_vencimento_contrato').hide(300);});

        $('.me_buttom').click(function () {
            add_me();
        });

        $('#add_customer').click(function (event) {
            event.preventDefault();
            var customer = $('#slcustomer').select2('data');

            if (customer) {

                if ($('.signatory_name1').val() === '') {
                    $('#div_signatory_default').hide();
                }

                var signatorysTab = $('a[href="#signatorys"]');
                signatorysTab.tab('show');
                signatorysTab.trigger('click');

                $('#slcustomer').select2("val", '');

            } else  {
                bootbox.alert(lang.select_above);
            }
        })

        $('#add_signatory').click(function () {
            add_signatory(null);
        });


        <?php if (!empty($document->getSignatories())) {
            $contador_signatory = 2;?>
            <?php foreach ($document->getSignatories() as $signatory) {

                if ($signatory->biller_id) {
                    continue;
                }

                ?>
                add_chagen_shipping_type(<?=$contador_signatory;?>);
                $('.signer_field<?=$contador_signatory;?>').keyup(function (event) {
                    mascaraTelefone( this, mtel );
                });
                $('.remove-signatory<?=$contador_signatory;?>').click(function() {
                    $(this).closest('.signatory').remove();
                    if ($('.signatory').length === 1) {
                        $('#div_signatory_default').show();
                    }
                });
            <?php $contador_signatory++;} ?>
        <?php } ?>

    });

    function add_signatory(customer) {

        contador_signatory++;
        var html = '';

        html =  '<div class="col-md-12 signatory">';

        html += '<input type="hidden" name="biller_id[]" value="">';

        if (customer!== null && customer.customer) {
            html += '   <input type="hidden" name="customer_id[]" value="' + customer.id + '">';
        } else {
            html += '   <input type="hidden" name="customer_id[]" value="">';
        }

        if (customer!== null && customer.customer) {
            html += '   <div class="col-md-12" style="text-align: right; cursor: pointer;"> <i class="fa fa-times remove-signatory' + contador_signatory + '"></i> </div>'
            html += '       <div class="col-md-8">';
            html += '           <div class="form-group">';
            html += '           <label for="signatory">Cliente Código ' + customer.id + '</label>'
            html += '           <input type="text" name="signatory[]" class="form-control tip" value="' + customer.customer + '" readonly placeholder="Nome do Signatário" required="required">';
            html += '       </div>';
            html += '   </div>';
        } else {
            html += '   <div class="col-md-12" style="text-align: right; cursor: pointer;"> <i class="fa fa-times remove-signatory' + contador_signatory + '"></i> </div>'
            html += '       <div class="col-md-8" style="margin-top: 10px;">';
            html += '           <div class="form-group">';
            html += '           <input type="text" name="signatory[]" class="form-control tip" placeholder="Nome do Signatário" required="required">';
            html += '       </div>';
            html += '   </div>';
        }


        if (customer!== null && customer.phone) {//com telefone

            html += '   <div class="col-md-4">';
            html += '       <div class="form-group">';
            html += '           <label for="signatory">Envio Por</label>'
            html += '           <select name="shipping_type[]" class="form-control shipping_type' + contador_signatory + '" readonly required="required">';
            html += '               <option value="telefone" selected>WhatsApp</option>';
            html += '           </select>';
            html += '       </div>';
            html += '   </div>';

        } else if (customer!== null && customer.email) {//com email

            html += '   <div class="col-md-4">';
            html += '       <div class="form-group">';
            html += '           <label for="signatory">Envio Por</label>'
            html += '           <select name="shipping_type[]" class="form-control shipping_type' + contador_signatory + '" readonly required="required">';
            html += '               <option value="email" selected>E-mail</option>';
            html += '           </select>';
            html += '       </div>';
            html += '   </div>';

        } else if (customer !== null && customer.customer) {//apenas o nome

            html += '   <div class="col-md-4">';
            html += '       <div class="form-group">';
            html += '           <label for="signatory">Envio Por</label>'
            html += '           <select name="shipping_type[]" class="form-control shipping_type' + contador_signatory + '" required="required">';
            html += '               <option value="telefone" selected>WhatsApp</option>';
            html += '               <option value="email">E-mail</option>';
            html += '           </select>';
            html += '       </div>';
            html += '   </div>';

        } else {//novo signatário

            html += '   <div class="col-md-4" style="margin-top: 10px;">';
            html += '       <div class="form-group">';
            html += '           <select name="shipping_type[]" class="form-control shipping_type' + contador_signatory + '" required="required">';
            html += '               <option value="telefone" selected>WhatsApp</option>';
            html += '               <option value="email">E-mail</option>';
            html += '           </select>';
            html += '       </div>';
            html += '   </div>';
        }

        html += '   <div class="col-md-12">';
        html += '       <div class="form-group">';

        if (customer!== null && customer.phone) {
            html += '           <input type="tel" name="signer_field[]" class="form-control tip signer_field' + contador_signatory + '" value="' + customer.phone + '" readonly placeholder="Telefone do Signatário"' + 'required="required">';
        } else if (customer!== null && customer.email) {
            html += '           <input type="email" name="signer_field[]" class="form-control tip signer_field' + contador_signatory + '" value="' + customer.email + '" readonly placeholder="Telefone do Signatário"' + 'required="required">';
        } else {
            html += '           <input type="text" name="signer_field[]" class="form-control tip signer_field' + contador_signatory + '" placeholder="Telefone do Signatário"' + 'required="required">';
        }

        html += '       </div>';
        html += '   </div>';
        html += '</div>';

        $(html).insertAfter('.div_signatory');

        add_chagen_shipping_type(contador_signatory);

        $('.signer_field' + contador_signatory).keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('.remove-signatory' + contador_signatory).click(function() {
            $(this).closest('.signatory').remove();

            if ($('.signatory').length === 1) {
                $('#div_signatory_default').show();
            }
        });

        adicionarCamposObrigatorioAoFormulario();
    }

    function add_chagen_shipping_type(contador) {

        $('.shipping_type' + contador).change(function () {

            $('.signer_field' + contador).val('');

            var value = $(this).val();
            if (value == 'telefone') {
                $('.signer_field' + contador).attr('placeholder', 'Telefone do Signatário');
                $('.signer_field' + contador).attr('type', 'tel');

                $('.signer_field' + contador).keyup(function (event) {
                    mascaraTelefone( this, mtel );
                });

            } else {
                $('.signer_field' + contador).attr('placeholder', 'E-mail do Signatário');
                $('.signer_field' + contador).attr('type', 'email');
                $('.signer_field' + contador).off('keyup');
            }
        });
    }

    function add_me() {

        var html = '<div class="col-md-12 signatory">';

        html += '<input type="hidden" name="customer_id[]" value="">';
        html += '<input type="hidden" name="biller_id[]" value="<?= $biller->id;?>">';

        html += '<div class="col-md-12" style="text-align: right; cursor: pointer;"> <i class="fa fa-times remove-signatory-me"></i> </div>'
        html += '<div class="col-md-8">';
        html += '<div class="form-group">';
        html += '<label for="signatory">Responsável</label>';
        html += '<input type="text" name="signatory[]" value="<?=$biller->name;?>" readonly class="form-control tip" placeholder="Nome do Signatário" required="required">';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<div class="form-group">';
        html += '<label for="shipping_type">Enviar Por</label>';
        html += '<select name="shipping_type[]" class="form-control shipping_type" readonly required="required">';
        html += '<option value="telefone" selected>WhatsApp</option>';
        html += '</select>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-md-12">';
        html += '<div class="form-group">';
        html += '<input type="text" name="signer_field[]" value="<?=$biller->phone;?>" readonly class="form-control tip signer_field" placeholder="Telefone do Signatário" required="required">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $(html).insertAfter('.div_me_signatory');

        $('.remove-signatory-me').click(function() {
            $(this).closest('.signatory').remove();
            $('.me_signatory').show();

            if ($('.signatory').length === 1) {
                $('#div_signatory_default').show();
            }
        });

        $('.signatory_name1').focus();
        $('.me_signatory').hide();
    }

    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    function adicionarCamposObrigatorioAoFormulario() {

        $('form[data-toggle="validator"]').data('bootstrapValidator', null);
        $('form[data-toggle="validator"]').bootstrapValidator();

        $('form[data-toggle="validator"]').data('bootstrapValidator').destroy();

        $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

        var fields = $('.form-control');
        $.each(fields, function() {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#'+id;

            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                if ($("label[for='" + id + "']").html() !== undefined) {
                    var label =  $("label[for='" + id + "']").html().replace('*', '');
                    $("label[for='" + id + "']").html(label + ' *');
                    $(document).on('change', iid, function () {
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                    });
                }
            }
        });
    }

    function nsCustomer() {

        $('#slcustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {

            if (e.added === undefined) return false;

            add_signatory(e.added);

        });
    }

</script>
