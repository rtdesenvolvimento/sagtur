<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NFAgendamento extends CI_Model
{

    const GERADA = 'generated';
    const AGENDADA = 'scheduled';

    const EMITIDA = 'issued';

    const CANCELADA = 'canceled';


    const NOTE = 'LEMBRANDO QUE SE SUA MENSALIDADE ESTIVER EM ABERTO POR 7 DIAS SEU SISTEMA SERA BLOQUEADO E 10 DIAS OCORRERA O CANCELAMENTO DO SISTEMA AUTOMATICAMENTE.';

    public $id;
    public $status;
    public $code;
    public $nf_code = '';
    public $customer_id;
    public $fatura_id;
    public $serviceDescription = '';
    public $observations;
    public $externalReference;
    public $valor_nf;
    public $deductions = 0;
    public $effectiveDate;
    public $municipalServiceId = '';
    public $municipalServiceCode;
    public $municipalServiceName;
    public $updatePayment = 0;
    public $type_nf = 'nf';


    public function __construct()
    {
        parent::__construct();
    }

}
