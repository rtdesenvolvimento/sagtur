<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update405 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.0.5'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'instrucoes_planta_onibus' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
