<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update493 extends CI_Migration {

    public function up() {
        $this->db->update('shop_settings',  array('atividades_recomendadas' => 1), array('id' => 1));
        $this->db->update('settings',  array('version' => '4.9.3'), array('setting_id' => 1));
    }

    public function down() {}
}
