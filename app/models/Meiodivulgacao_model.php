<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Meiodivulgacao_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getAll()
    {
        $q = $this->db->get('meio_divulgacao');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllActive() {

        $this->db->where('meio_divulgacao.active', 1);

        $q = $this->db->get('meio_divulgacao');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function add($data = array()) {
        if ($this->db->insert('meio_divulgacao', $data)) {
            return true;
        }
        return false;
    }

    public function update($id, $data = array()) {
        if ($this->db->update('meio_divulgacao', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('meio_divulgacao', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getById($id)
    {
        $q = $this->db->get_where('meio_divulgacao', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
