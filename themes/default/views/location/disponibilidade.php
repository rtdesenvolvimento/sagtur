<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_orange.css">

</head>
<body>


<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>
<script type="text/javascript" src="http://andrevelho/sagtur/themes/default/assets/js/jquery-2.0.3.min.js"></script>
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>

<input class="flatpickr" type="text" style="display: none;">

<div id='calendar'></div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'timeGridDay',
            businessHours: true,
            locale: 'pt-br',
            display: 'inverse-background',
            headerToolbar: {
                left: 'prev, next, today',
                center: 'title',
                right: 'timeGridDay,listDay,listWeek,listMonth,timeGridWeek,timeGridDay,listMonth'
            },
            events: [
                <?php foreach ($datas as $data) {
                    if ($data->disponibilidade == 'Disponível'){
                        $eventColor = '#8fdf82';
                    } else {
                        $eventColor = '#ff0000';
                    }
                    ?>
                {
                    title  : '<?=$data->veiculo.' | '.$data->disponibilidade.' | '.$data->cliente;?>',
                    start  : '<?=$data->data.'T'.$data->hora;?>',
                    color   : '<?=$eventColor;?>',
                },
                <?php } ?>

            ]
        });
        calendar.render();
    });

    var optional_config = {
        enableTime: false,
        dateFormat: "Y-m-d",
        locale : "pt",
        theme: 'material_orange',
        inline: true,

        enable: [
            <?php foreach ($datetimes as $datetime) {
                $date = explode(' ', $datetime)[0];
                $time = explode(' ', $datetime)[1];
                ?>
                {
                    from: "<?php echo $date;?>",
                    to: "<?php echo $date;?>"
                },
            <?php } ?>

            <?php foreach ($datas as $data) {?>
            {
                from: "<?php echo $data->data;?>",
                to: "<?php echo $data->data;?>"
            },
            <?php } ?>
        ],
        onChange: function(selectedDates, dateStr, instance) {
           // alert('aqui');
           console.log(selectedDates);
           console.log(dateStr);
           console.log(instance);
        },
    };
    var fp = flatpickr(".flatpickr",  optional_config);
</script>
</body>
</html>