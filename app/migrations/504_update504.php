<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update504 extends CI_Migration {

    public function up() {

        $this->alter_table_companies();
        $this->alter_table_customer_photos();

        $this->db->update('settings',  array('version' => '5.0.4'), array('setting_id' => 1));
    }

    public function alter_table_companies() {
        $fields = array(
            'image_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_customer_photos() {
        $fields = array(
            'photo_storage' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('customer_photos', $fields);
    }

    public function down() {}
}
