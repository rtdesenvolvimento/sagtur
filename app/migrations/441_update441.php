<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update441 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.4.1'), array('setting_id' => 1));
    }

    public function alter_table_products()
    {
        $fields = array(
            'obriga_sinal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
