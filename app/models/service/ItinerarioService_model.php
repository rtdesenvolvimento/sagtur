<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ItinerarioService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function gerar($itinerario) {

        //$itinerario = new Itinerario_model();
        $dados =  array('veiculo' => $itinerario->veiculo, 'motorista' => $itinerario->motorista, 'guia' => $itinerario->guia);

        foreach ($itinerario->itens as $item) {
            $this->db->update('sale_items', $dados , array('id' => $item->item));
        }
    }
}