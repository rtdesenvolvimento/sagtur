<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MenuRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getById($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('menu');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllActives() {
        $this->db->order_by('name');
        $this->db->where("active", 1);
        $q = $this->db->get('menu');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllMenusPai() {
        $this->db->order_by('name');
        $this->db->where("active", 1);
        $this->db->where("type", 'menu_superior');
        $q = $this->db->get('menu');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllMenusSuperiorEhURL() {
        $this->db->order_by('name');
        $this->db->where("active", 1);
        $this->db->where("type = 'menu_superior' or type = 'menu_superior_url'");
        $q = $this->db->get('menu');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllMenuItens($menuPaiID) {
        $this->db->where("menu_pai", $menuPaiID);
        $this->db->where("active", 1);

        $this->db->order_by('name');
        $q = $this->db->get('menu');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPagesByMenu($id) {
        $currentDate = date('Y-m-d');

        $this->db->where("active", 1);
        $this->db->where("menu_id", $id);
        $this->db->where("data_publicacao<=", $currentDate);

        $q = $this->db->get('page');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $despublicacaoTimestamp = strtotime($row->data_despublicacao);

                if ($despublicacaoTimestamp !== false) {
                    if ($despublicacaoTimestamp >= strtotime($currentDate)) {
                        $data[] = $row;
                    }
                } else {
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function isPhotosGallery()
    {
        return $this->db->count_all_results('gallery');
    }

}
