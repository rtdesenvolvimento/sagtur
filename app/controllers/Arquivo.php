<?php defined('BASEPATH') or exit('No direct script access allowed');

class Arquivo extends MY_Controller
{

    public $path = "assets/uploads/";
    public $path_qrcode = "assets/uploads/qrcode/";

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {}

    public function deletar() {

        $this->deletar_qrcodes();

        $diretorio = dir($this->path);
        echo "Lista de Arquivos do diretório '<strong>".$this->path."</strong>':<br />";

        while($arquivo = $diretorio -> read()){
            $this->deletar_barcode($arquivo);
            $this->deletar_qrcode($arquivo);
            $this->deletar_pdf_passagem($arquivo);
            $this->deletar_pdf_cotacao($arquivo);
        }
        $diretorio->close();
    }

    public function deletar_qrcodes() {
        $diretorio = dir($this->path_qrcode);
        echo "Lista de Arquivos do diretório '<strong>".$this->path_qrcode."</strong>':<br />";

        while($arquivo = $diretorio -> read()){

            if (!unlink($this->path_qrcode.$arquivo)) {
                echo ($this->path_qrcode.$arquivo." Mão pode ser excluído devido a um erro<br />");
            }
            else {
                echo ($this->path_qrcode.$arquivo." foi deletado<br />");
            }
        }
        $diretorio->close();
    }

    private function deletar_pdf_cotacao($arquivo) {
        $this->deletar_arquivo($arquivo, 'Cotação_');
    }

    private function deletar_pdf_passagem($arquivo) {
        $this->deletar_arquivo($arquivo, 'Passagem_');
    }

    private function deletar_qrcode($arquivo) {
        $this->deletar_arquivo($arquivo, 'qrcode');
    }

    private function deletar_barcode($arquivo) {
        $this->deletar_arquivo($arquivo, 'barcode');
    }

    private function deletar_arquivo($arquivo, $find) {
        if (stristr($arquivo, $find)) {
            if (!unlink($this->path.$arquivo)) {
                echo ($this->path.$arquivo.". Não pode ser excluído devido a um erro<br />");
            }
            else {
                echo ($this->path.$arquivo." foi deletado<br />");
            }
        }
    }
}