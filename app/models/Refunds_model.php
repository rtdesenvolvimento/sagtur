<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Refunds_model extends CI_Model
{
    const TABELA = 'gift_cards';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');

        $this->load->model('repository/RefundsRepository_model', 'RefundsRepository_model');

    }

    /* ----------------- Refund Cards --------------------- */

    public function addRefundCard($data = array(), $ca_data = array(), $sa_data = array())
    {
        try {

            $value = $data['value'];

            if ($value <= 0) {
                throw new Exception('Valor do reembolso deve ser maior que zero');
            }

            if ($this->db->insert(Refunds_model::TABELA, $data)) {
                if (!empty($ca_data)) {
                    $this->db->update('companies', array('award_points' => $ca_data['points']), array('id' => $ca_data['customer']));
                } elseif (!empty($sa_data)) {
                    $this->db->update('users', array('award_points' => $sa_data['points']), array('id' => $sa_data['user']));
                }

                $refund_card_id = $this->db->insert_id();

                $this->adicionar_conta_pagar($data, $refund_card_id);

                if ($data['origin_sale_id']) {
                    $note = $data['note'].'<br/> Valor do Reembolso '.$this->sma->formatMoney($value).' Expira em '.$this->sma->hrsd($data['expiry']);
                    $this->SaleEventService_model->gerar_reembolso($data['origin_sale_id'], $note);
                }

            }
            return true;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    private function adicionar_conta_pagar($refud, $refund_card_id)
    {

        $this->load->model('financeiro_model');
        $this->load->model('model/ContaPagar_model', 'ContaPagar_model');

        $tbCobranca      = $this->financeiro_model->getTipoCobrancaById($this->Settings->tipo_cobranca_reembolso_cliente_id);

        $sale_id            = $refud['origin_sale_id'];
        $vencimentos        = [$refud['expiry']];
        $valorVencimento    = [$refud['balance']];
        $tiposCobranca      = [$tbCobranca->id];
        $movimentadores     = [$tbCobranca->conta];
        $descontoParcela    = [];

        $data = array(
            'status'        => ContaReceber_model::STATUS_ABARTA,
            'reference'     => $this->site->getReference('ex'),

            'created_by'    => $this->session->userdata('user_id'),
            'pessoa'        => $refud['customer_id'],
            'valor'         => $refud['balance'],
            'dtvencimento'  => $refud['expiry'],
            'dtcompetencia' => $refud['expiry'],

            'warehouse'         => $this->Settings->default_warehouse,
            'despesa'           => $this->Settings->despesa_padrao_reembolso_id,
            'condicaopagamento' => $this->Settings->condicaoPagamentoAVista,
            'tipocobranca'      => $tbCobranca->id,

            'programacaoId' => null,
            'note'          => $refud['note'],
            'palavra_chave' => '',
            'numero_documento' => '',
            'gift_cards_id' => $refund_card_id,

            'sale'          => $sale_id,
        );

        $contaPagar = new ContaPagar_model();
        $contaPagar->data           = $data;
        $contaPagar->valores        = $valorVencimento;
        $contaPagar->vencimentos    = $vencimentos;
        $contaPagar->cobrancas      = $tiposCobranca;
        $contaPagar->movimentadores = $movimentadores;
        $contaPagar->descontos      = $descontoParcela;
        $contaPagar->senderHash     = '';

        return $this->financeiro_model->adicionarDespesa($contaPagar);
    }

    public function updateRefundCard($id, $data = array())
    {
        try {
            $refund = $this->site->getRefundCardByID($id);

            if ($refund->status != 'due') {
                throw new Exception('Não é possível editar reembolso de cliente com Pagamentos, estorne os valores para alterar o reembolso');
            }

            if ($refund->origin_sale_id) {
                throw new Exception('Não é permitido realizar alteração no reembolso de cliente lançado através de uma Venda!');
            }

            $this->db->where('id', $id);

            if ($this->db->update(Refunds_model::TABELA, $data)) {

                $fatura = $this->RefundsRepository_model->getFaturaByRefund($id);

                $this->financeiro_model->edit('fatura', array('totaldebito' => $data['value'], 'valorfatura' => $data['value'], 'valorpagar' => $data['value'], 'dtvencimento' => $data['expiry'], 'dtfaturamento' => $data['expiry']), 'id', $fatura->faturaId);
                $this->financeiro_model->edit('parcela', array('valor' => $data['value'], 'valorpagar' => $data['value'], 'dtvencimento' => $data['expiry'], 'dtcompetencia' => $data['expiry']), 'id', $fatura->parcelaId);
                $this->financeiro_model->edit('conta_pagar', array('valor' => $data['value'], 'dtvencimento' => $data['expiry'], 'dtcompetencia' => $data['expiry']), 'id', $fatura->contaPagarId);

                return true;
            }

            return false;

        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    public function deleteRefundCard($id)
    {
        if ($this->db->delete(Refunds_model::TABELA, array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function cancel($refund_id, $note = null)
    {
        $fatura = $this->RefundsRepository_model->getFaturaByRefund($refund_id);

        $this->FinanceiroService_model->cancelar($fatura->faturaId, $note);

    }

    public function cancelRefundCard($id)
    {
        $this->db->where('id', $id);

        $data = array('status' => 'cancel');

        if ($this->db->update(Refunds_model::TABELA, $data)) {
            return true;
        }
        return false;
    }

    public function getStaff()
    {
        if (!$this->Owner) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}