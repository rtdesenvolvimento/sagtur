<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('location_reminders'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div id='calendar'></div>
            </div>

        </div>
    </div>
</div>

<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>

<script type="text/javascript">

    var calendarEl = document.getElementById('calendar');

    $(document).ready(function () {
        calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'listDay',
            //editable: true,
            //selectable: true,
            //businessHours: true,
            locale: 'pt-br',
            views: {
                listDay: { buttonText: '<?=lang('list_by_day');?>' },
                listWeek: { buttonText: 'Lista da Semana' },
                listMonth: { buttonText: 'Lista do Mês' },
                listYear: { buttonText: 'list year' }
            },
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                //right: 'dayGridMonth,timeGridWeek,timeGridDay,listDay,listWeek,listMonth,listYear'
                right: 'listDay,listWeek,listMonth,timeGridWeek,timeGridDay,dayGridMonth'

            },
            events: site.base_url+'location/get_events',
        });
        calendar.render();
    });
</script>