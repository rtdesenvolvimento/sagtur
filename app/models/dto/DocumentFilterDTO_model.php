<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentFilterDTO_model extends CI_Model
{

    public $statusDocument;
    public $folder_id;
    public $programacao_id;
    public $biller_id;
    public $strSignatory;

    public $dataVendaDe;
    public $dataVendaAte;

    public $limit = 24;

    public function __construct()
    {
        parent::__construct();
    }


}