<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id)
    {
        return parent::__getById('document', $id);
    }

    public function getAll($filter)
    {
        //$filter = new DocumentFilterDTO_model();

        $this->db->select("documents.id as id");

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        if ($filter->folder_id) {
            $this->db->where('folder_id', $filter->folder_id);
        }

        if ($filter->statusDocument) {
            $this->db->where('document_status', $filter->statusDocument);
        } else {
            $this->db->where('document_status in ("draft","unsigned","pending","signed", "completed")');
        }

        if ($filter->biller_id) {
            $this->db->where('biller_id', $filter->biller_id);
        }

        if ($filter->dataVendaDe || $filter->dataVendaAte) {

            $this->db->join('sales', 'sales.id=documents.sale_id');

            if ($filter->dataVendaDe) {
                $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $filter->dataVendaDe);
            }

            if ($filter->dataVendaAte) {
                $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $filter->dataVendaAte);
            }

        }

        if ($filter->programacao_id) {
            $this->db->where("EXISTS (
                                SELECT 1 
                                FROM sma_sale_items 
                                WHERE sma_sale_items.sale_id = sma_documents.sale_id 
                                AND sma_sale_items.programacaoId = " . $this->db->escape($filter->programacao_id) . "
                            )");
        }

        if ($filter->strSignatory) {

            $term = $this->db->escape_like_str($filter->strSignatory);

            $sql = "EXISTS (
                                SELECT 1 
                                FROM sma_document_signatures
                                WHERE sma_document_signatures.document_id = sma_documents.id AND ";

            $sql .= "(  sma_document_signatures.name LIKE '%" . $term . "%' OR 
                        sma_document_signatures.email LIKE '%" . $term . "%' OR 
                        sma_document_signatures.phone LIKE '%" . $term . "%' OR
                        REPLACE(REPLACE(REPLACE(sma_document_signatures.phone, '-', ''), '(', ''), ')', '') LIKE '%" . $term . "%')";
            $sql .= ')';

            $this->db->where($sql);
        }

        $this->db->order_by('documents.id', 'desc');

        $this->db->limit($filter->limit);

        $q = $this->db->get('documents');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = new Document_model($row->id);
            }
            return $data;
        }
        return FALSE;
    }

    public function deleteSignaturesAll($documentoID)
    {
        $this->db->delete('document_signatures', array('document_id' => $documentoID));
    }

    public function getDocumentsBySaleID($saleID)
    {
        $this->db->select("documents.id as id");

        $this->db->where('sale_id', $saleID);

        $q = $this->db->get('documents');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = new Document_model($row->id);
            }
            return $data;
        }
        return FALSE;
    }

    public function getDocumentByDocumentKey($document_key)
    {
        $q = $this->db->get_where('documents', array('document_key' => $document_key), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSignatoryBySigningKey($signing_key)
    {
        $q = $this->db->get_where('document_signatures', array('public_id' => $signing_key), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getEvents($document_id)
    {
        $this->db->order_by('id');
        $q = $this->db->get_where('document_events', array('document_id' => $document_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}