<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->lang->load('auth', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->model('auth_model');
        $this->load->model('companies_model');
        $this->load->library('ion_auth');
    }

    function index()
    {

        if (!$this->loggedIn) {
            redirect('login');
        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function users()
    {
        if ( ! $this->loggedIn) {
            redirect('login');
        }
        if ( ! $this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'welcome');
        }

        $totalUsers = $this->getTotalUsers();
        $totalUsersUsed = $this->getTotalUsersUsed();
        $totalUsersAvailable = $totalUsers - $totalUsersUsed;

        $this->data['total_users'] = $this->getTotalUsers();
        $this->data['total_users_used'] = $totalUsersUsed;
        $this->data['total_users_available'] = $totalUsersAvailable;
        $this->data['plan_name'] = $this->getCustomerPlanName();
        $this->data['plan_price'] = $this->getCustomerPlanPrice();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('users')));
        $meta = array('page_title' => lang('users'), 'bc' => $bc);
        $this->page_construct('auth/index', $meta, $this->data);
    }

    function getCustomerPlanPrice() {

        $cnpj_origem = $this->formatar_cpf_cnpj($this->session->userdata('cnpj_origem'));
        $otherdb = $this->load->database('sagtur_administrativo', TRUE);

        $otherdb->select('customer_groups.price as price');
        $otherdb->join('customer_groups', 'customer_groups.id=companies.customer_group_id', 'left');
        $q = $otherdb->get_where('companies', array('vat_no' => $cnpj_origem), 1);

        if ($q->num_rows() > 0) {
            return $q->row()->price;
        }

        return 0;
    }
    function getCustomerPlanName() {

        $cnpj_origem = $this->formatar_cpf_cnpj($this->session->userdata('cnpj_origem'));
        $otherdb = $this->load->database('sagtur_administrativo', TRUE);

        $otherdb->select('customer_groups.name as plan');
        $otherdb->join('customer_groups', 'customer_groups.id=companies.customer_group_id', 'left');
        $q = $otherdb->get_where('companies', array('vat_no' => $cnpj_origem), 1);

        if ($q->num_rows() > 0) {
            return $q->row()->plan;
        }

        return 'SEM';
    }

    function getTotalUsers() {

        $cnpj_origem = $this->formatar_cpf_cnpj($this->session->userdata('cnpj_origem'));
        $otherdb = $this->load->database('sagtur_administrativo', TRUE);

        $otherdb->select('customer_groups.users as total_users');
        $otherdb->join('customer_groups', 'customer_groups.id=companies.customer_group_id', 'left');
        $q = $otherdb->get_where('companies', array('vat_no' => $cnpj_origem), 1);

        if ($q->num_rows() > 0) {
            return (int) $q->row()->total_users;
        }

        return 0;
    }

    private function formatar_cpf_cnpj($doc) {

        $doc = preg_replace("/[^0-9]/", "", $doc);
        $doc = str_replace("-", "", $doc);
        $doc = str_replace(".", "", $doc);
        $doc = trim($doc);

        $qtd = strlen($doc);

        if($qtd >= 11) {

            if($qtd === 11 ) {

                $docFormatado = substr($doc, 0, 3) . '.' .
                    substr($doc, 3, 3) . '.' .
                    substr($doc, 6, 3) . '-' .
                    substr($doc, 9, 2);
            } else {
                $docFormatado = substr($doc, 0, 2) . '.' .
                    substr($doc, 2, 3) . '.' .
                    substr($doc, 5, 3) . '/' .
                    substr($doc, 8, 4) . '-' .
                    substr($doc, -2);
            }

            return $docFormatado;

        } else {
            return $doc;
        }
    }

    public function getTotalUsersUsed()
    {
        $this->db->select('count(sma_users.id) qtd', false);
        $this->db->where('active', 1);
        $this->db->where('email != "andre@resultatec.com.br"');

        $q = $this->db->get_where('users', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row()->qtd;
        }

        return 0;
    }


    function getUsers()
    {
        if ( ! $this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            $this->sma->md();
        }

        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('users').".id as id, first_name, last_name, email, company, award_points, " . $this->db->dbprefix('groups') . ".name, active, biller_id")
            ->from("users")
            ->join('groups', 'users.group_id=groups.id', 'left')
            ->where('email != "andre@resultatec.com.br"')
            ->group_by('users.id')
            ->where('company_id', NULL)
            ->edit_column('active', '$1__$2', 'active, id, biller_id')
            ->add_column("Actions", "<div class=\"text-center\"><a  href='" . $this->Settings->url_site_domain.'/$2' . "' target='_blank'><i class=\"fa fa-cart-plus\"></i></a> </div> <div class=\"text-center\"><a href='" . site_url('auth/profile/$1') . "' class='tip' title='" . lang("edit_user") . "'><i class=\"fa fa-edit\"></i></a></div>", "id, biller_id");

        if (!$this->Owner) {
            $this->datatables->unset_column('id');
        }
        echo $this->datatables->generate();
    }

    function getUserLogins($id = NULL)
    {
        if (!$this->ion_auth->in_group(array('super-admin', 'admin'))) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect('welcome');
        }
        $this->load->library('datatables');
        $this->datatables
            ->select("login, ip_address, time")
            ->from("user_logins")
            ->where('user_id', $id);

        echo $this->datatables->generate();
    }

    function delete_avatar($id = NULL, $avatar = NULL)
    {

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('owner') && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . $_SERVER["HTTP_REFERER"] . "'; }, 0);</script>");
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            unlink('assets/uploads/avatars/' . $avatar);
            unlink('assets/uploads/avatars/thumbs/' . $avatar);
            if ($id == $this->session->userdata('user_id')) {
                $this->session->unset_userdata('avatar');
            }
            $this->db->update('users', array('avatar' => NULL), array('id' => $id));
            $this->session->set_flashdata('message', lang("avatar_deleted"));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . $_SERVER["HTTP_REFERER"] . "'; }, 0);</script>");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function profile($id = NULL)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('owner') && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$id || empty($id)) {
            redirect('auth');
        }

        $this->data['title'] = lang('profile');

        $user = $this->ion_auth->user($id)->row();
        $groups = $this->ion_auth->groups()->result_array();
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['biller'] = $this->companies_model->getCompanyByID($user->biller_id);
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
        );
        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        $this->data['old_password'] = array(
            'name' => 'old',
            'id' => 'old',
            'class' => 'form-control',
            'type' => 'password',
        );
        $this->data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
        );
        $this->data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
        );
        $this->data['user_id'] = array(
            'name' => 'user_id',
            'id' => 'user_id',
            'type' => 'hidden',
            'value' => $user->id,
        );

        $this->data['id'] = $id;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('auth/users'), 'page' => lang('users')), array('link' => '#', 'page' => lang('profile')));
        $meta = array('page_title' => lang('profile'), 'bc' => $bc);
        $this->page_construct('auth/profile', $meta, $this->data);
    }

    public function captcha_check($cap)
    {
        $expiration = time() - 300; // 5 minutes limit
        $this->db->delete('captcha', array('captcha_time <' => $expiration));

        $this->db->select('COUNT(*) AS count')
            ->where('word', $cap)
            ->where('ip_address', $this->input->ip_address())
            ->where('captcha_time >', $expiration);

        if ($this->db->count_all_results('captcha')) {
            return true;
        } else {
            $this->form_validation->set_message('captcha_check', lang('captcha_wrong'));
            return FALSE;
        }
    }

    private function get_company($cnpjempresa) {

        if ($cnpjempresa == '06624498943')    $cnpjempresa = 'andrevelho';//TODO TEMPORARIO
        if ($cnpjempresa == '38055980000100') $cnpjempresa = 'demonstracaosagtur';//TODO TEMPORARIO

        if ($cnpjempresa == '34400561000108') $cnpjempresa = 'yellowfun';//TODO TEMPORARIO
        if ($cnpjempresa == '26774129000140') $cnpjempresa = 'italiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24500089000113') $cnpjempresa = 'marcosexcursoeseeventos';//TODO TEMPORARIO
        if ($cnpjempresa == '34722503000192') $cnpjempresa = 'tripdajana';//TODO TEMPORARIO
        if ($cnpjempresa == '33619978000195') $cnpjempresa = 'realitytur';//TODO TEMPORARIO
        if ($cnpjempresa == '33166902000151') $cnpjempresa = 'crisandtriptours';//TODO TEMPORARIO
        if ($cnpjempresa == '28203338000160') $cnpjempresa = 'muralhatour';//TODO TEMPORARIO
        if ($cnpjempresa == '30331234000182') $cnpjempresa = 'sicatur';//TODO TEMPORARIO
        if ($cnpjempresa == '17833342000195') $cnpjempresa = 'csviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24005646000120') $cnpjempresa = 'alcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13338144000102') $cnpjempresa = 'leehtourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '34615832000134') $cnpjempresa = 'augustusturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40129230000170') $cnpjempresa = 'goldtourtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '28076047000158') $cnpjempresa = 'viajandobrasilafora';//TODO TEMPORARIO
        if ($cnpjempresa == '13241569000190') $cnpjempresa = 'riosexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '20928726000104') $cnpjempresa = 'lptur';//TODO TEMPORARIO
        if ($cnpjempresa == '29245296000192') $cnpjempresa = 'semprevix';//TODO TEMPORARIO
        if ($cnpjempresa == '27890670000187') $cnpjempresa = 'mochilaselfie';//TODO TEMPORARIO
        if ($cnpjempresa == '30891666000148') $cnpjempresa = 'pipaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14941063000157') $cnpjempresa = 'amigostur';//TODO TEMPORARIO
        if ($cnpjempresa == '38402047000154') $cnpjempresa = 'vivitour';//TODO TEMPORARIO
        if ($cnpjempresa == '19288850000165') $cnpjempresa = 'tatatur';//TODO TEMPORARIO
        if ($cnpjempresa == '30439133000120') $cnpjempresa = 'glatur';//TODO TEMPORARIO
        if ($cnpjempresa == '37182808000147') $cnpjempresa = 'stylosturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '29097930000197') $cnpjempresa = 'bmsadventure';//TODO TEMPORARIO
        if ($cnpjempresa == '29793176000120') $cnpjempresa = 'oliveirastrips';//TODO TEMPORARIO
        if ($cnpjempresa == '27004104000120') $cnpjempresa = 'viajecomigomm';//TODO TEMPORARIO
        if ($cnpjempresa == '33078629000103') $cnpjempresa = 'diversaotour';//TODO TEMPORARIO
        if ($cnpjempresa == '26976469000154') $cnpjempresa = 'excursoesbrunamoraes';//TODO TEMPORARIO
        if ($cnpjempresa == '35424883000141') $cnpjempresa = 'mendesviagenstur';//TODO TEMPORARIO
        if ($cnpjempresa == '26708588000126') $cnpjempresa = 'thaistourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37432064000171') $cnpjempresa = 'magictour';//TODO TEMPORARIO
        if ($cnpjempresa == '35681423000107') $cnpjempresa = 'itaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43759073000174') $cnpjempresa = 'viagensturismosp';//TODO TEMPORARIO
        if ($cnpjempresa == '31014032000170') $cnpjempresa = 'mieventoseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43348901000181') $cnpjempresa = 'brasiltripstour';//TODO TEMPORARIO
        if ($cnpjempresa == '32305744000100') $cnpjempresa = 'eufuiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '37738156000184') $cnpjempresa = 'abrangeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26954715000177') $cnpjempresa = 'felipeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '24729013000164') $cnpjempresa = 'sanmartinviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27288477000170') $cnpjempresa = 'carolviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28935650000148') $cnpjempresa = 'pajeturexcursoesmaraba';//TODO TEMPORARIO
        if ($cnpjempresa == '35023482000180') $cnpjempresa = 'turismojs';//TODO TEMPORARIO
        if ($cnpjempresa == '30491098000198') $cnpjempresa = 'turdans';//TODO TEMPORARIO
        if ($cnpjempresa == '31012340000166') $cnpjempresa = 'grazziviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26705523000127') $cnpjempresa = 'sergiotur';//TODO TEMPORARIO
        if ($cnpjempresa == '30244458000157') $cnpjempresa = 'luturismoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '29362577000125') $cnpjempresa = 'vemcomagenteviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27547096000169') $cnpjempresa = 'barrilviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32172434000165') $cnpjempresa = 'riscandoomapa';//TODO TEMPORARIO
        if ($cnpjempresa == '33917431000176') $cnpjempresa = 'angelacardosoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28452160000190') $cnpjempresa = 'dannytur';//TODO TEMPORARIO
        if ($cnpjempresa == '40986189000158') $cnpjempresa = 'trip4fun';//TODO TEMPORARIO
        if ($cnpjempresa == '28526240000143') $cnpjempresa = 'partiutrips';//TODO TEMPORARIO
        if ($cnpjempresa == '33812938000165') $cnpjempresa = 'jalapaoeletrizanteturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41856069000107') $cnpjempresa = 'daviturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26538969000104') $cnpjempresa = 'depalmasturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '14073554000123') $cnpjempresa = 'classeviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39679371000187') $cnpjempresa = 'tfturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28592620000186') $cnpjempresa = 'newdreamstur';//TODO TEMPORARIO
        if ($cnpjempresa == '36056364000130') $cnpjempresa = 'excomturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33384229000126') $cnpjempresa = 'dnexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '31447413000142') $cnpjempresa = 'trictour';//TODO TEMPORARIO
        if ($cnpjempresa == '25045997000127') $cnpjempresa = 'avesvoandoalto';//TODO TEMPORARIO
        if ($cnpjempresa == '30949801000169') $cnpjempresa = 'saochicoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34718011000123') $cnpjempresa = 'brotherstrips';//TODO TEMPORARIO
        if ($cnpjempresa == '23923768000132') $cnpjempresa = 'gbtour';//TODO TEMPORARIO
        if ($cnpjempresa == '42980073000137') $cnpjempresa = 'kltourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '20131237000119') $cnpjempresa = 'daniviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24020259000162') $cnpjempresa = 'natsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45656099000112') $cnpjempresa = 'mtkturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '16862375000109') $cnpjempresa = 'mroyalviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '28521221000124') $cnpjempresa = 'paraviverturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29676117000171') $cnpjempresa = 'excursoestiacleusa';//TODO TEMPORARIO
        if ($cnpjempresa == '41483075000158') $cnpjempresa = 'terradosolecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47998851000139') $cnpjempresa = 'nenaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32515539000170') $cnpjempresa = 'maeefilhosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32076814000104') $cnpjempresa = 'azevedoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37476039000190') $cnpjempresa = 'viagensorion';//TODO TEMPORARIO
        if ($cnpjempresa == '31659433000187') $cnpjempresa = 'mpwturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '18138398000192') $cnpjempresa = 'originalestradeiros';//TODO TEMPORARIO
        if ($cnpjempresa == '32250898000142') $cnpjempresa = 'eleveviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30320509000182') $cnpjempresa = 'curtaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '36293940000163') $cnpjempresa = 'agenciamonteiro';//TODO TEMPORARIO
        if ($cnpjempresa == '42015101000186') $cnpjempresa = 'sophiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '73379372000144') $cnpjempresa = 'aramastur';//TODO TEMPORARIO
        if ($cnpjempresa == '40229228000172') $cnpjempresa = 'leotavaresturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '11075549000161') $cnpjempresa = 'nanaturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '12760402000173') $cnpjempresa = 'romeroexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '31063525000108') $cnpjempresa = 'acosttaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23429156000198') $cnpjempresa = 'gusmaoviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45674415000189') $cnpjempresa = 'familialourencoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33967010000150') $cnpjempresa = 'vfbturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42196707000165') $cnpjempresa = 'perollaricoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27897111000107') $cnpjempresa = 'rumocertoturismoperuibe';//TODO TEMPORARIO
        if ($cnpjempresa == '33452360000183') $cnpjempresa = 'roselyviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31493840000167') $cnpjempresa = 'avohaiecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '20435412000161') $cnpjempresa = 'inesbitencourtturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28940710000110') $cnpjempresa = 'tripforever';//TODO TEMPORARIO
        if ($cnpjempresa == '15382408000142') $cnpjempresa = 'dclasseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '11880764000135') $cnpjempresa = 'sabrinaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '20317795000173') $cnpjempresa = 'marciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '05830342000152') $cnpjempresa = 'transcapturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38285051000180') $cnpjempresa = 'viajareprecisope';//TODO TEMPORARIO
        if ($cnpjempresa == '26109186000105') $cnpjempresa = 'locomotivanomade';//TODO TEMPORARIO
        if ($cnpjempresa == '15544990000104') $cnpjempresa = 'sntturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33520971000111') $cnpjempresa = 'plenasviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43627182000138') $cnpjempresa = 'dottaaventura';//TODO TEMPORARIO
        if ($cnpjempresa == '47648941000108') $cnpjempresa = 'entreamigosexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '28122222000104') $cnpjempresa = 'ctljexecutiva';//TODO TEMPORARIO
        if ($cnpjempresa == '32177583000117') $cnpjempresa = 'livreacessoturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '46936366000178') $cnpjempresa = 'mgbusviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '74775701000139') $cnpjempresa = 'bmgturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45519169000190') $cnpjempresa = 'juhtur';//TODO TEMPORARIO
        if ($cnpjempresa == '32384712000148') $cnpjempresa = 'todeboaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33022309000131') $cnpjempresa = 'spvturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31834090000140') $cnpjempresa = 'giexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '09084776000101') $cnpjempresa = 'andreytur';//TODO TEMPORARIO
        if ($cnpjempresa == '44673343000192') $cnpjempresa = 'giselleviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31111469000121') $cnpjempresa = 'doneganatour';//TODO TEMPORARIO
        if ($cnpjempresa == '00840324000138') $cnpjempresa = 'navaltur';//TODO TEMPORARIO
        if ($cnpjempresa == '24072440000112') $cnpjempresa = '2forasteiros';//TODO TEMPORARIO
        if ($cnpjempresa == '32261084000103') $cnpjempresa = 'familyturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40678527000194') $cnpjempresa = 'essenciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34181741000138') $cnpjempresa = 'plenissimostrips';//TODO TEMPORARIO
        if ($cnpjempresa == '34774052000137') $cnpjempresa = 'dgaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23167053000105') $cnpjempresa = 'dhagesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28646407000100') $cnpjempresa = 'emersonfelipeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '14554319000173') $cnpjempresa = 'descubrafloripa';//TODO TEMPORARIO
        if ($cnpjempresa == '42461649000150') $cnpjempresa = 'ldviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23335365000172') $cnpjempresa = 'tripsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40070888000153') $cnpjempresa = 'msvviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '40070888000153') $cnpjempresa = 'msaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '45848368000142') $cnpjempresa = 'agenciaplanettrip';//TODO TEMPORARIO
        if ($cnpjempresa == '45848368000142') $cnpjempresa = 'planettrip';//TODO TEMPORARIO
        if ($cnpjempresa == '35443481000194') $cnpjempresa = 'mimatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '13864528000150') $cnpjempresa = 'maxxitours';//TODO TEMPORARIO
        if ($cnpjempresa == '34773592000104') $cnpjempresa = 'cincoamigostour';//TODO TEMPORARIO
        if ($cnpjempresa == '38385149000109') $cnpjempresa = 'diegotourjalapao';//TODO TEMPORARIO
        if ($cnpjempresa == '36247836000132') $cnpjempresa = 'girltrips';//TODO TEMPORARIO
        if ($cnpjempresa == '21646605000124') $cnpjempresa = 'evoluirviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30197759000177') $cnpjempresa = 'jkturismobc';//TODO TEMPORARIO
        if ($cnpjempresa == '38296660000134') $cnpjempresa = 'agencianovosdestinos';//TODO TEMPORARIO
        if ($cnpjempresa == '28559768000119') $cnpjempresa = 'mariliaviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39753194000131') $cnpjempresa = 'stilostur';//TODO TEMPORARIO
        if ($cnpjempresa == '31748310000112') $cnpjempresa = 'raphatrip';//TODO TEMPORARIO
        if ($cnpjempresa == '49059176000108') $cnpjempresa = 'tursolidario';//TODO TEMPORARIO
        if ($cnpjempresa == '33557911000173') $cnpjempresa = 'msbusviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40714437000101') $cnpjempresa = 'turmadovandre';//TODO TEMPORARIO
        if ($cnpjempresa == '49185081000130') $cnpjempresa = 'tremdaalegriaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '48145185000159') $cnpjempresa = 'f5tour';//TODO TEMPORARIO
        if ($cnpjempresa == '34416896000106') $cnpjempresa = 'mmturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '52596973000139') $cnpjempresa = 'reservasangulotravel';//TODO TEMPORARIO
        if ($cnpjempresa == '35286918000123') $cnpjempresa = 'lovefortrips';//TODO TEMPORARIO
        if ($cnpjempresa == '28524458000169') $cnpjempresa = 'jalapoeirosecotour';//TODO TEMPORARIO
        if ($cnpjempresa == '40932815000123') $cnpjempresa = 'turismoguarulhos';//TODO TEMPORARIO
        if ($cnpjempresa == '47958925000103') $cnpjempresa = 'mulheres100fronteiras';//TODO TEMPORARIO
        if ($cnpjempresa == '35125923000154') $cnpjempresa = 'katatur';//TODO TEMPORARIO
        if ($cnpjempresa == '35597067000130') $cnpjempresa = 'excursoespontonell';//TODO TEMPORARIO
        if ($cnpjempresa == '23998127000147') $cnpjempresa = 'andersonexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '24971929000126') $cnpjempresa = 'azenhaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19720969000165') $cnpjempresa = 'konexaodosambaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33575305000180') $cnpjempresa = 'petrusviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '23916973000170') $cnpjempresa = 'teudestino';//TODO TEMPORARIO
        if ($cnpjempresa == '49420809000161') $cnpjempresa = 'peacetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '33878354000192') $cnpjempresa = 'excursoesdabecky';//TODO TEMPORARIO
        if ($cnpjempresa == '48969391000183') $cnpjempresa = 'fdtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '44676714000190') $cnpjempresa = 'alvarengaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '18376031000107') $cnpjempresa = 'novotempoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28719264000119') $cnpjempresa = 'gmtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '28155974000163') $cnpjempresa = 'vounaviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '26188041000147') $cnpjempresa = 'viajolandiatour';//TODO TEMPORARIO
        if ($cnpjempresa == '52678636000190') $cnpjempresa = 'girandoomundotour';//TODO TEMPORARIO
        if ($cnpjempresa == '49310740000113') $cnpjempresa = 'alvanevasconcelosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26868644000190') $cnpjempresa = 'princesaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30827727000108') $cnpjempresa = 'tioniaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43258712000118') $cnpjempresa = 'consutoriaeassessoriaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '12241910000145') $cnpjempresa = 'smcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '42048755000106') $cnpjempresa = 'donatoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '16588392000191') $cnpjempresa = 'carolviagenseconcursos';//TODO TEMPORARIO
        if ($cnpjempresa == '33358225000173') $cnpjempresa = 'soniaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47027449000107') $cnpjempresa = 'tripemgalera';//TODO TEMPORARIO
        if ($cnpjempresa == '26015341000124') $cnpjempresa = 'lilaotrip';//TODO TEMPORARIO
        if ($cnpjempresa == '25174268000170') $cnpjempresa = 'bragaturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '15188548000184') $cnpjempresa = 'estanciatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33824874000112') $cnpjempresa = 'lamarcaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32094553000147') $cnpjempresa = 'easytransfer';//TODO TEMPORARIO
        if ($cnpjempresa == '46837285000110') $cnpjempresa = 'leoturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32368439000168') $cnpjempresa = 'jrtour';//TODO TEMPORARIO
        if ($cnpjempresa == '22307450000164') $cnpjempresa = 'mfviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42300807000190') $cnpjempresa = 'refexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '17353036000151') $cnpjempresa = 'patotrips';//TODO TEMPORARIO
        if ($cnpjempresa == '03354507000131') $cnpjempresa = 'cpcturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33924480000136') $cnpjempresa = 'tripturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '50911487000132') $cnpjempresa = 'flaviustur';//TODO TEMPORARIO
        if ($cnpjempresa == '29713734000108') $cnpjempresa = 'marcinhotur';//TODO TEMPORARIO
        if ($cnpjempresa == '41537685000197') $cnpjempresa = 'pdviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24657373000106') $cnpjempresa = 'boralaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '51003730000187') $cnpjempresa = 'pedrocordeiroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28581000000141') $cnpjempresa = 'gustavotour';//TODO TEMPORARIO
        if ($cnpjempresa == '40964847000100') $cnpjempresa = 'javoltour';//TODO TEMPORARIO
        if ($cnpjempresa == '31483792000126') $cnpjempresa = 'professorperegrino';//TODO TEMPORARIO
        if ($cnpjempresa == '28264253000191') $cnpjempresa = 'expressorodriguez';//TODO TEMPORARIO
        if ($cnpjempresa == '02333534000165') $cnpjempresa = 'ituturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27544470000172') $cnpjempresa = 'connectviagem';//TODO TEMPORARIO
        if ($cnpjempresa == '42572981000191') $cnpjempresa = 'kingtripsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '40710284000124') $cnpjempresa = 'upofiicial';//TODO TEMPORARIO
        if ($cnpjempresa == '29610454000166') $cnpjempresa = 'parceirustur';//TODO TEMPORARIO
        if ($cnpjempresa == '41251615000178') $cnpjempresa = 'amazonnegociostur';//TODO TEMPORARIO
        if ($cnpjempresa == '12362419000172') $cnpjempresa = 'afaiaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '47650962000150') $cnpjempresa = 'dunamisexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '51833792000116') $cnpjempresa = 'rpassaingressos';//TODO TEMPORARIO
        if ($cnpjempresa == '02109322000107') $cnpjempresa = 'voyageturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '12144461000117') $cnpjempresa = 'agtturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27797821000157') $cnpjempresa = 'jalapaobrasilto';//TODO TEMPORARIO
        if ($cnpjempresa == '39811525000142') $cnpjempresa = 'vinitur';//TODO TEMPORARIO
        if ($cnpjempresa == '41446324000135') $cnpjempresa = 'alchaarparticipacoes';//TODO TEMPORARIO
        if ($cnpjempresa == '31044704000190') $cnpjempresa = 'charmeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46614496000194') $cnpjempresa = 'goncalvesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32258248000143') $cnpjempresa = 'encantoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '31962554000101') $cnpjempresa = 'gersonaguiarturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19225075000107') $cnpjempresa = 'premiumturismmo';//TODO TEMPORARIO
        if ($cnpjempresa == '41810081000172') $cnpjempresa = 'partiuexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '38068658000108') $cnpjempresa = 'angelicaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '38060954000162') $cnpjempresa = 'liberttravel';//TODO TEMPORARIO
        if ($cnpjempresa == '30600166000100') $cnpjempresa = 'boraviajeiros';//TODO TEMPORARIO
        if ($cnpjempresa == '38377678000160') $cnpjempresa = 'memexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '45802502000174') $cnpjempresa = 'ganeshaadventureviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32868033000144') $cnpjempresa = 'edivanturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '15599774000158') $cnpjempresa = 'safaritur';//TODO TEMPORARIO
        if ($cnpjempresa == '36941996000187') $cnpjempresa = 'poraidemochila';//TODO TEMPORARIO
        if ($cnpjempresa == '38152634000132') $cnpjempresa = 'escarpaspasseiosnauticos';//TODO TEMPORARIO
        if ($cnpjempresa == '35757018000117') $cnpjempresa = 'jbmexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '14699949000136') $cnpjempresa = 'mrfernandes';//TODO TEMPORARIO
        if ($cnpjempresa == '46561439000194') $cnpjempresa = 'levetravel';//TODO TEMPORARIO
        if ($cnpjempresa == '10339582000199') $cnpjempresa = 'caminhosdaterraturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39706089000141') $cnpjempresa = 'agenciadeviagensaguiar';//TODO TEMPORARIO
        if ($cnpjempresa == '26869304000183') $cnpjempresa = 'justgotrips';//TODO TEMPORARIO
        if ($cnpjempresa == '32413081000148') $cnpjempresa = 'estrelastour';//TODO TEMPORARIO
        if ($cnpjempresa == '52103581000190') $cnpjempresa = 'viixeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '21223683000116') $cnpjempresa = 'deboraexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '51488766000106') $cnpjempresa = 'aventuresejalapao';//TODO TEMPORARIO
        if ($cnpjempresa == '53315041000133') $cnpjempresa = 'desrotinetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '43382120000103') $cnpjempresa = 'keviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '29722726000110') $cnpjempresa = 'excursoesroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '43755659000160') $cnpjempresa = 'glautour';//TODO TEMPORARIO
        if ($cnpjempresa == '50582521000172') $cnpjempresa = 'tripdavania';//TODO TEMPORARIO
        if ($cnpjempresa == '53541141000188') $cnpjempresa = 'tripswei';//TODO TEMPORARIO
        if ($cnpjempresa == '02088829000113') $cnpjempresa = 'pigozzoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19834210000103') $cnpjempresa = 'boraboraviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49108028000136') $cnpjempresa = 'tawtour';//TODO TEMPORARIO
        if ($cnpjempresa == '37561054000136') $cnpjempresa = 'diadetur';//TODO TEMPORARIO
        if ($cnpjempresa == '36661564000112') $cnpjempresa = 'giratour';//TODO TEMPORARIO
        if ($cnpjempresa == '36985188000111') $cnpjempresa = 'franturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '50616315000136') $cnpjempresa = 'betripsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '44754101000123') $cnpjempresa = 'guiviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22800325000191') $cnpjempresa = 'materdeiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '36512111000124') $cnpjempresa = 'riccioviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '14220847000196') $cnpjempresa = 'bellitur';//TODO TEMPORARIO
        if ($cnpjempresa == '44995985000108') $cnpjempresa = 'seashoreturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '21294464000128') $cnpjempresa = 'federaltur';//TODO TEMPORARIO
        if ($cnpjempresa == '51377758000184') $cnpjempresa = 'deiaexcursoeseviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '43469031000107') $cnpjempresa = 'stonetour';//TODO TEMPORARIO
        if ($cnpjempresa == '16667689000142') $cnpjempresa = 'raphaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '54181952000188') $cnpjempresa = 'luhexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '12944002000118') $cnpjempresa = 'kilazer';//TODO TEMPORARIO
        if ($cnpjempresa == '30043362000120') $cnpjempresa = 'gtviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '50361687000169') $cnpjempresa = 'vemprocorre';//TODO TEMPORARIO
        if ($cnpjempresa == '42722575000168') $cnpjempresa = 'batsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24122089000127') $cnpjempresa = 'playup';//TODO TEMPORARIO
        if ($cnpjempresa == '11546358000130') $cnpjempresa = 'mtviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '24480213000126') $cnpjempresa = 'agenciavicktrips';//TODO TEMPORARIO
        if ($cnpjempresa == '35433301000193') $cnpjempresa = 'invistatour';//TODO TEMPORARIO
        if ($cnpjempresa == '46172157000103') $cnpjempresa = 'dinahturviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '14798331000123') $cnpjempresa = 'douratour';//TODO TEMPORARIO
        if ($cnpjempresa == '46977470000100') $cnpjempresa = 'alternativeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32401582000104') $cnpjempresa = 'brtravel';//TODO TEMPORARIO
        if ($cnpjempresa == '38342425000151') $cnpjempresa = 'allinturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46118182000109') $cnpjempresa = 'girlstrips';//TODO TEMPORARIO
        if ($cnpjempresa == '27147915000180') $cnpjempresa = 'gecioneturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48027215000122') $cnpjempresa = 'danturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49538346000137') $cnpjempresa = 'megacorporativa';//TODO TEMPORARIO
        if ($cnpjempresa == '54932495000116') $cnpjempresa = 'fenixturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07173874000190') $cnpjempresa = 'oliviatour';//TODO TEMPORARIO
        if ($cnpjempresa == '43546540000188') $cnpjempresa = 'brexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '54443081000123') $cnpjempresa = 'gouveiatourgo';//TODO TEMPORARIO
        if ($cnpjempresa == '55008647000151') $cnpjempresa = 'viajandocommaria';//TODO TEMPORARIO
        if ($cnpjempresa == '36147547000161') $cnpjempresa = 'camaleaoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '10876079000172') $cnpjempresa = 'caicaraexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '22521858000134') $cnpjempresa = 'registurismo';//TODO TEMPORARIO
        if ($cnpjempresa == '02878805000168') $cnpjempresa = 'viagensdetrem';//TODO TEMPORARIO
        if ($cnpjempresa == '35998495000174') $cnpjempresa = 'sonicviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30708489000111') $cnpjempresa = 'minasecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32781439000195') $cnpjempresa = 'rssviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '55190707000108') $cnpjempresa = 'traveltur';//TODO TEMPORARIO
        if ($cnpjempresa == '55351004000106') $cnpjempresa = 'edianetour';//TODO TEMPORARIO
        if ($cnpjempresa == '45247738000196') $cnpjempresa = 'aglaiaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '55437946000101') $cnpjempresa = 'alineviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '55378356000155') $cnpjempresa = 'styllusturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '01679099000162') $cnpjempresa = 'tourmix';//TODO TEMPORARIO
        if ($cnpjempresa == '45711558000113') $cnpjempresa = 'ederturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38237902000119') $cnpjempresa = 'paraisoviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '24641996000182') $cnpjempresa = 'noninoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33019182000100') $cnpjempresa = 'pacotesdetrip';//TODO TEMPORARIO
        if ($cnpjempresa == '54797333000112') $cnpjempresa = 'oasistur';//TODO TEMPORARIO
        if ($cnpjempresa == '49538985000100') $cnpjempresa = 'panatourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '31881940000160') $cnpjempresa = 'cristiannetur';//TODO TEMPORARIO
        if ($cnpjempresa == '30954702000175') $cnpjempresa = 'dhiturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '07710851000177') $cnpjempresa = 'cativatur';//TODO TEMPORARIO
        if ($cnpjempresa == '30742042000169') $cnpjempresa = 'goodtrip';//TODO TEMPORARIO
        if ($cnpjempresa == '55516027000123') $cnpjempresa = 'theworldturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35046011000197') $cnpjempresa = 'enoturbrasilia';//TODO TEMPORARIO
        if ($cnpjempresa == '55778376000113') $cnpjempresa = 'domturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32805877000146') $cnpjempresa = 'viagensnacional';//TODO TEMPORARIO
        if ($cnpjempresa == '48823486000194') $cnpjempresa = 'bernardinitrips';//TODO TEMPORARIO
        if ($cnpjempresa == '46022793000140') $cnpjempresa = 'fariaesoaresturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '46084930000171') $cnpjempresa = 'morangostour';//TODO TEMPORARIO
        if ($cnpjempresa == '53258852000140') $cnpjempresa = 'renatoturismoadventure';//TODO TEMPORARIO
        if ($cnpjempresa == '35530958000179') $cnpjempresa = 'vivitourltda';//TODO TEMPORARIO
        if ($cnpjempresa == '32496114000160') $cnpjempresa = 'televo';//TODO TEMPORARIO
        if ($cnpjempresa == '23977495000109') $cnpjempresa = 'vibless';//TODO TEMPORARIO
        if ($cnpjempresa == '21286270000180') $cnpjempresa = 'ceumarturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '56052805000133') $cnpjempresa = 'aslexpresso';//TODO TEMPORARIO
        if ($cnpjempresa == '51239568000109') $cnpjempresa = 'biaviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44377353000180') $cnpjempresa = 'lamsturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27223121000159') $cnpjempresa = 'patyupexcursao';//TODO TEMPORARIO
        if ($cnpjempresa == '55880437000159') $cnpjempresa = 'abepetur';//TODO TEMPORARIO
        if ($cnpjempresa == '40530741000107') $cnpjempresa = 'camargosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33164372000102') $cnpjempresa = 'karinaviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38260023000108') $cnpjempresa = 'conexaobrasilperu';//TODO TEMPORARIO
        if ($cnpjempresa == '25342789000190') $cnpjempresa = 'busmar';//TODO TEMPORARIO
        if ($cnpjempresa == '50387887000190') $cnpjempresa = 'stellatours';//TODO TEMPORARIO
        if ($cnpjempresa == '43483748000103') $cnpjempresa = 'celurturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28456609000199') $cnpjempresa = 'salvadorviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '51929835000161') $cnpjempresa = 'atualviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32875646000109') $cnpjempresa = 'lionstrance';//TODO TEMPORARIO
        if ($cnpjempresa == '51453754000138') $cnpjempresa = 'solariumtur';//TODO TEMPORARIO
        if ($cnpjempresa == '37680710000110') $cnpjempresa = 'ueltrekking';//TODO TEMPORARIO
        if ($cnpjempresa == '32059493000121') $cnpjempresa = 'scturismoeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '32938039000140') $cnpjempresa = 'busaodocabral';//TODO TEMPORARIO
        if ($cnpjempresa == '30751863000161') $cnpjempresa = 'dugeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '57045804000124') $cnpjempresa = 'plenitudeexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '56178704000103') $cnpjempresa = 'vamoscomigoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52668950000192') $cnpjempresa = 'travellertur';//TODO TEMPORARIO
        if ($cnpjempresa == '57369672000196') $cnpjempresa = 'viajemaisporai';//TODO TEMPORARIO
        if ($cnpjempresa == '27589649000146') $cnpjempresa = 'gabrielviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '33701240000172') $cnpjempresa = 'cenarionativoexpedicoes';//TODO TEMPORARIO
        if ($cnpjempresa == '43351110000100') $cnpjempresa = 'dedessatour';//TODO TEMPORARIO
        if ($cnpjempresa == '27565228000185') $cnpjempresa = 'moserturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37617942000123') $cnpjempresa = 'terraceuviagemeturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13659716000147') $cnpjempresa = 'marialuciaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32676271000158') $cnpjempresa = 'rodzexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '48661022000129') $cnpjempresa = 'ladtur';//TODO TEMPORARIO
        if ($cnpjempresa == '23097837000104') $cnpjempresa = 'qualosegredoaventuras';//TODO TEMPORARIO
        if ($cnpjempresa == '20004996000110') $cnpjempresa = 'hollidayviagensetur';//TODO TEMPORARIO
        if ($cnpjempresa == '43002118000161') $cnpjempresa = 'verdeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '42443680000168') $cnpjempresa = 'tatytourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '53718437000121') $cnpjempresa = 'jaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44926127000101') $cnpjempresa = 'turismopedregulho';//TODO TEMPORARIO
        if ($cnpjempresa == '57215144000182') $cnpjempresa = 'colinaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '22176138000189') $cnpjempresa = 'caetanotur';//TODO TEMPORARIO
        if ($cnpjempresa == '56050478000180') $cnpjempresa = 'mianovichiexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '54220201000123') $cnpjempresa = 'bggturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47633156000173') $cnpjempresa = 'primeturexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '27225931000144') $cnpjempresa = 'bigbearexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '43310128000164') $cnpjempresa = 'rodribus';//TODO TEMPORARIO
        if ($cnpjempresa == '31364720000160') $cnpjempresa = 'uaitour';//TODO TEMPORARIO
        if ($cnpjempresa == '15163665000193') $cnpjempresa = 'wgexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '53890814000105') $cnpjempresa = 'rpthurturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '36344509000107') $cnpjempresa = 'edinhotur';//TODO TEMPORARIO
        if ($cnpjempresa == '57575788000181') $cnpjempresa = 'conectatour';//TODO TEMPORARIO
        if ($cnpjempresa == '47282508000193') $cnpjempresa = 'maisroteirosviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '27755665000161') $cnpjempresa = 'sunset';//TODO TEMPORARIO
        if ($cnpjempresa == '03780520000152') $cnpjempresa = 'samistur';//TODO TEMPORARIO
        if ($cnpjempresa == '31943070000107') $cnpjempresa = 'dsbtur';//TODO TEMPORARIO
        if ($cnpjempresa == '40184621000197') $cnpjempresa = 'constelacaotour';//TODO TEMPORARIO
        if ($cnpjempresa == '42404066000197') $cnpjempresa = 'solartrips';//TODO TEMPORARIO
        if ($cnpjempresa == '45499429000103') $cnpjempresa = 'lumasterturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41865555000183') $cnpjempresa = 'vavdavareturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '26026641000109') $cnpjempresa = 'centauroturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '25935290000196') $cnpjempresa = 'mdv';//TODO TEMPORARIO
        if ($cnpjempresa == '55161122000151') $cnpjempresa = 'libertyturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29194452000133') $cnpjempresa = 'penochaoviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '35671132000120') $cnpjempresa = 'divinopolisecoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '05760627000164') $cnpjempresa = 'estrelaguiaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34612158000134') $cnpjempresa = 'luviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48385995000182') $cnpjempresa = 'dielyelorenaviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '39448228000184') $cnpjempresa = 'mahila';//TODO TEMPORARIO
        if ($cnpjempresa == '37762944000106') $cnpjempresa = 'flyturturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '53361106000187') $cnpjempresa = 'stevenlindley';//TODO TEMPORARIO
        if ($cnpjempresa == '57543888000126') $cnpjempresa = 'nanytrips';//TODO TEMPORARIO
        if ($cnpjempresa == '43731417000137') $cnpjempresa = 'sabinoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '13333777000110') $cnpjempresa = 'penaestradaoficial';//TODO TEMPORARIO
        if ($cnpjempresa == '41329070000175') $cnpjempresa = 'alexteleva';//TODO TEMPORARIO
        if ($cnpjempresa == '49427050000149') $cnpjempresa = 'minasgoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '58150252000187') $cnpjempresa = 'zeroonzetour';//TODO TEMPORARIO
        if ($cnpjempresa == '58298825000114') $cnpjempresa = 'bhorizonteviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '30875999000183') $cnpjempresa = 'larissaphasseios';//TODO TEMPORARIO
        if ($cnpjempresa == '57426387000160') $cnpjempresa = 'voandoaltoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '33831909000140') $cnpjempresa = 'primeturismobaixada';//TODO TEMPORARIO
        if ($cnpjempresa == '29165462000140') $cnpjempresa = 'wtorresviagemfamilia';//TODO TEMPORARIO
        if ($cnpjempresa == '25104434000162') $cnpjempresa = 'suetrips';//TODO TEMPORARIO
        if ($cnpjempresa == '61957205000154') $cnpjempresa = 'transgerciturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '32581932000161') $cnpjempresa = 'maistravelviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '29751750000187') $cnpjempresa = 'rctt';//TODO TEMPORARIO
        if ($cnpjempresa == '30500686000140') $cnpjempresa = 'deiaexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '30025683000100') $cnpjempresa = 'gomestur';//TODO TEMPORARIO
        if ($cnpjempresa == '37168505000170') $cnpjempresa = 'andreiaviagenseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34899233000190') $cnpjempresa = 'wineviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '35259511000107') $cnpjempresa = 'anjosturismorp';//TODO TEMPORARIO
        if ($cnpjempresa == '55447895000107') $cnpjempresa = 'kadoshadonaiviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '48360841000136') $cnpjempresa = 'agenciaalohaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '27443866000123') $cnpjempresa = 'gratitudeviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '50664906000189') $cnpjempresa = 'tripmaia';//TODO TEMPORARIO
        if ($cnpjempresa == '38113127000190') $cnpjempresa = 'rcviagemturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '49438637000153') $cnpjempresa = 'bellalunatour';//TODO TEMPORARIO
        if ($cnpjempresa == '28104179000146') $cnpjempresa = 'penaestradapasseios';//TODO TEMPORARIO
        if ($cnpjempresa == '31121776000193') $cnpjempresa = 'richardturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '56322119000135') $cnpjempresa = 'alveselimatur';//TODO TEMPORARIO
        if ($cnpjempresa == '58746568000136') $cnpjempresa = 'jldestinosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '41836139000157') $cnpjempresa = 'patitour';//TODO TEMPORARIO
        if ($cnpjempresa == '46719723000146') $cnpjempresa = 'magiatrip';//TODO TEMPORARIO
        if ($cnpjempresa == '51088958000117') $cnpjempresa = 'helloutrip';//TODO TEMPORARIO
        if ($cnpjempresa == '31183947000109') $cnpjempresa = 'ahmturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '52170551000105') $cnpjempresa = 'luluturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '19425377000110') $cnpjempresa = 'naestradabauru';//TODO TEMPORARIO
        if ($cnpjempresa == '42589349000150') $cnpjempresa = 'diogotour';//TODO TEMPORARIO
        if ($cnpjempresa == '54611366000126') $cnpjempresa = 'receptivoconecttur';//TODO TEMPORARIO
        if ($cnpjempresa == '50450639000147') $cnpjempresa = 'planejaviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37711612000100') $cnpjempresa = 'lifestour';//TODO TEMPORARIO
        if ($cnpjempresa == '54626188000107') $cnpjempresa = 'paratytemporadaoficial';//TODO TEMPORARIO
        if ($cnpjempresa == '33239637000194') $cnpjempresa = 'gbturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '44485775000170') $cnpjempresa = 'phoenixturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '59082341000104') $cnpjempresa = 'fofaoexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '53303440000184') $cnpjempresa = 'starstrip';//TODO TEMPORARIO
        if ($cnpjempresa == '49169620000148') $cnpjempresa = 'doisirmaosturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '28981135000102') $cnpjempresa = 'girlsgoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '30043721000140') $cnpjempresa = 'kleexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '59087176000175') $cnpjempresa = 'mceturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '39604835000196') $cnpjempresa = 'agendaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '47667976000186') $cnpjempresa = 'pensetripsviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '59128566000146') $cnpjempresa = 'boraviagensturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '08395983000114') $cnpjempresa = 'agvtur';//TODO TEMPORARIO
        if ($cnpjempresa == '44920659000131') $cnpjempresa = 'trembaoturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '37035711000101') $cnpjempresa = 'letsboratur';//TODO TEMPORARIO
        if ($cnpjempresa == '55698714000107') $cnpjempresa = 'familiarexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '11748846000120') $cnpjempresa = 'penseturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '34111451000118') $cnpjempresa = 'mundreamviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '58930168000186') $cnpjempresa = 'ericlesturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '58957953000122') $cnpjempresa = 'maxtourviagenseturimo';//TODO TEMPORARIO
        if ($cnpjempresa == '50981053000109') $cnpjempresa = 'mbviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '49484323000197') $cnpjempresa = 'turismogess';//TODO TEMPORARIO
        if ($cnpjempresa == '43718396000110') $cnpjempresa = 'kakaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '38903517000163') $cnpjempresa = 'vmturismoviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '09590283000143') $cnpjempresa = 'viajarecorrer';//TODO TEMPORARIO
        if ($cnpjempresa == '58581339000109') $cnpjempresa = 'lucianotourviagens';//TODO TEMPORARIO
        if ($cnpjempresa == '36152220000188') $cnpjempresa = 'gmturismorealizasonhos';//TODO TEMPORARIO
        if ($cnpjempresa == '42673262000167') $cnpjempresa = 'tripsdareh';//TODO TEMPORARIO
        if ($cnpjempresa == '29256998000171') $cnpjempresa = 'crturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '48838019000138') $cnpjempresa = 'mulheresviajantesrj';//TODO TEMPORARIO
        if ($cnpjempresa == '31305652000168') $cnpjempresa = 'ketitaturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '29798224000172') $cnpjempresa = 'seguetur';//TODO TEMPORARIO
        if ($cnpjempresa == '33467712000174') $cnpjempresa = 'realizeviagens-es';//TODO TEMPORARIO
        if ($cnpjempresa == '42808736000130') $cnpjempresa = 'idealoperadora';//TODO TEMPORARIO
        if ($cnpjempresa == '30395675000148') $cnpjempresa = 'nevesturexcursoes';//TODO TEMPORARIO
        if ($cnpjempresa == '59538290000174') $cnpjempresa = 'rbrturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '20959666000189') $cnpjempresa = 'leoturismobh';//TODO TEMPORARIO
        if ($cnpjempresa == '59785911000114') $cnpjempresa = 'crissturismo';//TODO TEMPORARIO
        if ($cnpjempresa == '54394144000107') $cnpjempresa = 'osfarristastur';//TODO TEMPORARIO

        return $cnpjempresa;
    }

    function login($m = NULL)
    {
        if ($this->loggedIn) {
            $this->session->set_flashdata('error', $this->session->flashdata('error'));
            redirect('welcome');
        }
        $this->data['title'] = lang('login');

        if ($this->Settings->captcha) {
            $this->form_validation->set_rules('captcha', lang('captcha'), 'required|callback_captcha_check');
        }

        if ($this->form_validation->run() == true) {

            $remember       = (bool) $this->input->post('remember');
            $cnpjempresa    = $this->get_company($this->input->post('cnpjempresa'));

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $cnpjempresa, $this->input->post('cnpjempresa'), $remember)) {

                if ($this->Settings->mmode) {
                    if (!$this->ion_auth->in_group('owner')) {
                        $this->session->set_flashdata('error', lang('site_is_offline_plz_try_later'));
                        redirect('auth/logout');
                    }
                }

                /*
                if ($this->ion_auth->in_group('customer') || $this->ion_auth->in_group('supplier')) {
                    redirect('auth/logout/1');
                }
                */ //TODO ESSE FONTE IMPEDE QUE UM CLIENTE/FORNECEDOR POSSA ACESSAR O SISTEMA COM SEU EMAIL

                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $referrer = $this->session->userdata('requested_page') ? $this->session->userdata('requested_page') : 'welcome';
                redirect($referrer);
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                redirect('login');
            }
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['message'] = $this->session->flashdata('message');

            if ($this->Settings->captcha) {
                $this->load->helper('captcha');
                $vals = array(
                    'img_path' => './assets/captcha/',
                    'img_url' => site_url() . 'assets/captcha/',
                    'img_width' => 150,
                    'img_height' => 34,
                    'word_length' => 5,
                    'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
                );
                $cap = create_captcha($vals);
                $capdata = array(
                    'captcha_time' => $cap['time'],
                    'ip_address' => $this->input->ip_address(),
                    'word' => $cap['word']
                );

                $query = $this->db->insert_string('captcha', $capdata);
                $this->db->query($query);
                $this->data['image'] = $cap['image'];
                $this->data['captcha'] = array('name' => 'captcha',
                    'id' => 'captcha',
                    'type' => 'text',
                    'class' => 'form-control',
                    'required' => 'required',
                    'placeholder' => lang('type_captcha')
                );
            }

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => lang('email'),
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => lang('password'),
            );
            $this->data['allow_reg'] = $this->Settings->allow_reg;
            if ($m == 'db') {
                $this->data['message'] = lang('db_restored'). ' error: '.validation_errors() ;
            } elseif ($m) {
                $this->data['error'] = lang('we_are_sorry_as_this_sction_is_still_under_development.'). ' error: '.validation_errors() ;
            }

            $this->load->view($this->theme . 'auth/login', $this->data);
        }
    }

    function reload_captcha()
    {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './assets/captcha/',
            'img_url' => site_url() . 'assets/captcha/',
            'img_width' => 150,
            'img_height' => 34,
            'word_length' => 5,
            'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
        );
        $cap = create_captcha($vals);
        $capdata = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $capdata);
        $this->db->query($query);
        //$this->data['image'] = $cap['image'];

        echo $cap['image'];
    }

    function logout($m = NULL)
    {
        $logout = $this->ion_auth->logout();
        $this->session->set_flashdata('message', $this->ion_auth->messages());

        redirect('login/' . $m);
    }

    function change_password()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('login');
        }
        $this->form_validation->set_rules('old_password', lang('old_password'), 'required');
        $this->form_validation->set_rules('new_password', lang('new_password'), 'required|min_length[8]|max_length[25]');
        $this->form_validation->set_rules('new_password_confirm', lang('confirm_password'), 'required|matches[new_password]');

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('auth/profile/' . $user->id . '/#cpassword');
        } else {
            if (DEMO) {
                $this->session->set_flashdata('warning', lang('disabled_in_demo'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

            $change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

            if ($change) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                redirect('auth/profile/' . $user->id . '/#cpassword');
            }
        }
    }

    function forgot_password()
    {
        $this->form_validation->set_rules('forgot_email', lang('email_address'), 'required|valid_email');
        $this->form_validation->set_rules('forgot_cnpjempresa', lang('forgot_cnpjempresa'), 'required');

        if ($this->form_validation->run() == false) {
            $error = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->session->set_flashdata('error', $error);
            redirect("login#forgot_password");
        } else {

            $cnpjemppresa   = $this->input->post('forgot_cnpjempresa');
            $empresa        = $this->get_company($cnpjemppresa);
            $otherdb        = $this->load->database($empresa, TRUE);

            $identity = $this->ion_auth->where('email', strtolower($this->input->post('forgot_email')))->users(null, $otherdb)->row();

            if (empty($identity)) {
                $this->ion_auth->set_message('forgot_password_email_not_found');
                $this->session->set_flashdata('error', $this->ion_auth->messages());
                redirect("login#forgot_password");
            }

            $forgotten = $this->ion_auth->forgotten_password($identity->email, $otherdb, $cnpjemppresa);

            if ($forgotten) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("login#forgot_password");
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                redirect("login#forgot_password");
            }
        }
    }

    public function reset_password($cnpj = NULL, $code = NULL)
    {
        if (!$code || !$cnpj) show_404();

        $empresa        = $this->get_company($cnpj);
        $otherdb        = $this->load->database($empresa, TRUE);
        $user           = $this->ion_auth->forgotten_password_check($code, $otherdb);

        if ($user) {

            $this->form_validation->set_rules('new', lang('password'), 'required|min_length[8]|max_length[25]|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', lang('confirm_password'), 'required');

            if ($this->form_validation->run() == false) {

                $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
                $this->data['message'] = $this->session->flashdata('message');
                $this->data['title'] = lang('reset_password');
                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
                    'data-bv-regexp-message' => lang('pasword_hint'),
                    'placeholder' => lang('new_password')
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'data-bv-identical' => 'true',
                    'data-bv-identical-field' => 'new',
                    'data-bv-identical-message' => lang('pw_not_same'),
                    'placeholder' => lang('confirm_password')
                );
                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;
                $this->data['cnpj'] = $cnpj;
                $this->data['identity_label'] = $user->email;
                //render
                $this->load->view($this->theme . 'auth/reset_password', $this->data);
            } else {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) {

                    //something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error(lang('error_csrf'));

                } else {
                    // finally change the password
                    $identity = $user->email;

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'), $empresa, $otherdb);

                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        //$this->logout();
                        redirect('login');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->errors());
                        redirect('auth/reset_password/' . $code);
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            redirect("login#forgot_password");
        }
    }

    function activate($id, $code = false)
    {

        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->Owner) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            if ($this->Owner) {
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                redirect("auth/login");
            }
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            redirect("forgot_password");
        }
    }

    function deactivate($id = NULL)
    {
        $this->sma->checkPermissions('users', TRUE);
        $id = $this->config->item('use_mongodb', 'ion_auth') ? (string)$id : (int)$id;
        $this->form_validation->set_rules('confirm', lang("confirm"), 'required');

        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('deactivate')) {
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['user'] = $this->ion_auth->user($id)->row();
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'auth/deactivate_user', $this->data);
            }
        } else {

            if ($this->input->post('confirm') == 'yes') {
                if ($id != $this->input->post('id')) {
                    show_error(lang('error_csrf'));
                }

                if ($this->ion_auth->logged_in() && $this->Owner) {
                    $this->ion_auth->deactivate($id);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                }
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function create_user()
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->data['title'] = "Create User";
        $this->form_validation->set_rules('email', lang("username_login"), 'trim|is_unique[users.email]');//TODO E-MAIL UNICO PARA LOGIN
        $this->form_validation->set_rules('status', lang("status"), 'trim|required');
        $this->form_validation->set_rules('group', lang("group"), 'trim|required');

        if ($this->form_validation->run() == true) {

            $username   = strtolower($this->input->post('username'));//TODO USADO PARA O E-MAIL DO VOUCHER
            $email      = strtolower($this->input->post('email'));//TODO USADO PARA LOGAR
            $password   = $this->input->post('password');
            $notify     = $this->input->post('notify');
            $active     = $this->input->post('status');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'group_id' => $this->input->post('group') ? $this->input->post('group') : '3',
                'biller_id' => null,//$this->input->post('biller'),
                'view_right' => $this->input->post('view_right'),

                'edit_right' => 1,//$this->input->post('edit_right'),
                'allow_discount' => 1,//$this->input->post('allow_discount'),
                'warehouse_id' => null, //$this->input->post('warehouse'),
            );

            $data_biller = array(
                'logo' => $this->Settings->logo2,//logo principal do sistema
                'company' => $this->input->post('company'),
                'name' =>  $this->input->post('first_name').' '.$this->input->post('last_name'),
                'email' => $this->input->post('username'),//TODO USADO PARA EMAIL DO VOUCHER
                'group_id' => NULL,
                'group_name' => 'biller',
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'invoice_footer' => $this->input->post('invoice_footer'),
                'is_biller' => $this->input->post('is_biller'),
            );
        }

        $totalUsers = $this->getTotalUsers();
        $totalUsersUsed = $this->getTotalUsersUsed();
        $totalUsersAvailable = $totalUsers - $totalUsersUsed;

        if ($totalUsersAvailable <= 0)  {
            $this->session->set_flashdata('error', lang("no_more_registration_available"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {

            $idb_uid = $this->companies_model->addCompany($data_biller);

            $additional_data['biller_id'] = $idb_uid;

            $this->ion_auth->register($email, $password, $email, $additional_data, $active, $notify);

            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth/users");

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['groups'] = $this->ion_auth->groups()->result_array();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => site_url('home'), 'page' => lang('home')), array('link' => site_url('auth/users'), 'page' => lang('users')), array('link' => '#', 'page' => lang('create_user')));
            $meta = array('page_title' => lang('users'), 'bc' => $bc);
            $this->page_construct('auth/create_user', $meta, $this->data);
        }
    }

    function edit_user($id = NULL)
    {

        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }

        $this->data['title'] = lang("edit_user");

        if (!$this->loggedIn || !$this->Owner && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $user = $this->ion_auth->user($id)->row();
        $biller = $this->companies_model->getCompanyByID($user->biller_id);

        if ($user->email != $this->input->post('email'))  $this->form_validation->set_rules('email', lang("username_login"), 'trim|is_unique[users.email]');//TODO EMAIL UNICO PARA LOGUIN

        if ($this->form_validation->run() === TRUE) {

            if ($this->Owner || $this->Admin) {
                /*
                if ($id == $this->session->userdata('user_id')) {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                    );
                }
                */

                if ($this->ion_auth->in_group('customer', $id) || $this->ion_auth->in_group('supplier', $id)) {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                    );
                } else {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'username' => $this->input->post('username'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                        'active' => $this->input->post('status'),
                        'group_id' => $this->input->post('group'),
                        'biller_id' => $this->input->post('biller') ? $this->input->post('biller') : NULL,
                        'warehouse_id' => $this->input->post('warehouse') ? $this->input->post('warehouse') : NULL,
                        'award_points' => $this->input->post('award_points'),
                        'view_right' => $this->input->post('view_right'),
                        'edit_right' => $this->input->post('edit_right'),
                        'allow_discount' => $this->input->post('allow_discount'),
                    );

                    $totalUsers = $this->getTotalUsers();
                    $totalUsersUsed = $this->getTotalUsersUsed();

                    if ($this->input->post('status') == 1) {//TODO ativou agora
                        if ($user->active  == 0) {//TODO ESTAVA DESATIVADO
                            $totalUsersAvailable = $totalUsers - ($totalUsersUsed + 1);

                            if ($totalUsersAvailable < 0)  {
                                $this->session->set_flashdata('error', lang("no_more_registration_available"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                    }

                    $data_biller = array(
                        'logo' => $this->Settings->logo2,//logo principal do sistema
                        'company' => $this->input->post('company'),
                        'name' =>  $this->input->post('first_name').' '.$this->input->post('last_name'),
                        'email' => $this->input->post('username'),//TODO USADO PARA EMAIL DO VOUCHER
                        'address' => $this->input->post('address'),
                        'vat_no' => $this->input->post('vat_no'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'postal_code' => $this->input->post('postal_code'),
                        'country' => $this->input->post('country'),
                        'phone' => $this->input->post('phone'),
                        'invoice_footer' => $this->input->post('invoice_footer'),
                        'is_biller' => $this->input->post('is_biller'),
                    );

                    $this->companies_model->updateCompany($biller->id, $data_biller);

                }

            } else {//outros perfis
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'gender' => $this->input->post('gender'),
                );
            }

            if ($this->Owner) {
                if ($this->input->post('password')) {
                    if (DEMO) {
                        $this->session->set_flashdata('warning', lang('disabled_in_demo'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $this->form_validation->set_rules('password', lang('edit_user_validation_password_label'), 'required|min_length[8]|max_length[25]|matches[password_confirm]');
                    $this->form_validation->set_rules('password_confirm', lang('edit_user_validation_password_confirm_label'), 'required');

                    $data['password'] = $this->input->post('password');
                }
            }
            //$this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() === TRUE && $this->ion_auth->update($user->id, $data)) {
            $this->session->set_flashdata('message', lang('user_updated'));
            redirect("auth/profile/" . $id);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }


    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function _render_page($view, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;
        $view_html = $this->load->view('header', $this->viewdata, $render);
        $view_html .= $this->load->view($view, $this->viewdata, $render);
        $view_html = $this->load->view('footer', $this->viewdata, $render);

        if (!$render)
            return $view_html;
    }

    /**
     * @param null $id
     */
    function update_avatar($id = NULL)
    {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }

        if (!$this->ion_auth->logged_in() || !$this->Owner && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //validate form input
        $this->form_validation->set_rules('avatar', lang("avatar"), 'trim');

        if ($this->form_validation->run() == true) {

            if ($_FILES['avatar']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png';
                //$config['max_size'] = '500';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('avatar')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->helper('file');
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/avatars/' . $photo;
                $config['new_image'] = 'assets/uploads/avatars/thumbs/' . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $user = $this->ion_auth->user($id)->row();
            } else {
                $this->form_validation->set_rules('avatar', lang("avatar"), 'required');
            }
        }

        if ($this->form_validation->run() == true && $this->auth_model->updateAvatar($id, $photo)) {
            unlink('assets/uploads/avatars/' . $user->avatar);
            unlink('assets/uploads/avatars/thumbs/' . $user->avatar);
            $this->session->set_userdata('avatar', $photo);
            $this->session->set_flashdata('message', lang("avatar_updated"));
            redirect("auth/profile/" . $id);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect("auth/profile/" . $id);
        }
    }

    function register()
    {
        $this->data['title'] = "Register";
        if (!$this->allow_reg) {
            $this->session->set_flashdata('error', lang('registration_is_disabled'));
            redirect("login");
        }
        //validate form input
        //$this->form_validation->set_message('is_unique', 'An account already registered with this email');
        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'required');
        $this->form_validation->set_rules('email', lang('email_address'), 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('phone', lang('phone'), 'required');
        $this->form_validation->set_rules('company', lang('company'), 'required');
        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[25]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', lang('confirm_password'), 'required');
        $this->form_validation->set_rules('captcha', 'captcha', 'required|callback_captcha_check');

        if ($this->form_validation->run() == true) {
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data)) {

            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("login");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['groups'] = $this->ion_auth->groups()->result_array();

            $this->load->helper('captcha');
            $vals = array(
                'img_path' => './assets/captcha/',
                'img_url' => site_url() . 'assets/captcha/',
                'img_width' => 150,
                'img_height' => 34,
            );
            $cap = create_captcha($vals);
            $capdata = array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );

            $query = $this->db->insert_string('captcha', $capdata);
            $this->db->query($query);
            $this->data['image'] = $cap['image'];
            $this->data['captcha'] = array('name' => 'captcha',
                'id' => 'captcha',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => lang('type_captcha')
            );

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name' => 'company',
                'id' => 'company',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'required' => 'required',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->load->view('auth/register', $this->data);
        }
    }

    function user_actions()
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->auth_model->delete_user($id);
                    }
                    $this->session->set_flashdata('message', lang("users_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('first_name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('last_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('group'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $user = $this->site->getUser($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $user->first_name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $user->last_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $user->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $user->company);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $user->group);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $user->status);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'users_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_user_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function delete($id = NULL)
    {
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ( ! $this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'welcome');
        }

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->auth_model->delete_user($id)) {
            //echo lang("user_deleted");
            $this->session->set_flashdata('message', 'user_deleted');
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function getAgencias() {
        $agencias = array(
            array('id'=> '1' , 'label' => 'Resultatec'),
            array('id'=> '2' , 'label' => 'Teste')
        );

        $allowed = ['Resultatec'];

        $filtered = array_filter($agencias,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );



        $this->sma->send_json($filtered);
    }

}
