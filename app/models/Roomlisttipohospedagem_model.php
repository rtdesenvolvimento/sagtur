<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roomlisttipohospedagem_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function add($data = array()) {

        $this->db->insert('room_list_hospedagem', $data);

        if ($this->db->affected_rows() == '1') {
            return $this->db->insert_id('room_list_hospedagem');
        }
        return FALSE;
    }

    public function getById($id)
    {
        $this->db->select('room_list_hospedagem.*, tipo_hospedagem.name as name');
        $this->db->join('tipo_hospedagem', 'room_list_hospedagem.tipo_hospedagem_id=tipo_hospedagem.id', 'left');

        $q = $this->db->get_where('room_list_hospedagem', array('room_list_hospedagem.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTiposHospedagemByRoomList($room_list_id, $tipo_hospedagem_id)
    {

        $this->db->where('room_list_id', $room_list_id);
        $this->db->where('tipo_hospedagem_id', $tipo_hospedagem_id);

        $q = $this->db->get('room_list_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function update($id, $data = array()) {
        if ($this->db->update('room_list_hospedagem', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('room_list_hospedagem', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteByRoomList($roomListId) {
        if ($this->db->delete('room_list_hospedagem', array('room_list_id' => $roomListId))) {
            return true;
        }
        return FALSE;
    }

}
