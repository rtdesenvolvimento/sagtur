<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update439 extends CI_Migration {

    public function up() {

        $this->alter_table_valor_faixa();
        $this->alter_table_companies();

        $this->alter_table_itinerario();
        $this->alter_table_sale_items();

        $this->db->update('settings',  array('version' => '4.3.9'), array('setting_id' => 1));
    }

    public function alter_table_sale_items()
    {
        $this->db->query("ALTER TABLE `sma_sale_items` DROP `note`;");

        $fields = array(
            'note_item' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('sale_items', $fields);
    }

    public function alter_table_itinerario()
    {
        $fields = array(
            'executivo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('itinerario', $fields);
    }

    public function alter_table_valor_faixa() {
        $fields = array(
            'captar_observacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0 ),
            'placeholder_observacao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'Informações importantes! Exemplo: Com Quem deseja compartilhar o assento ou Hospedagem.'),
        );
        $this->dbforge->add_column('valor_faixa', $fields);
    }

    public function alter_table_companies() {
        $fields = array(
            'ultima_nota' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => ''),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    public function down() {}
}
