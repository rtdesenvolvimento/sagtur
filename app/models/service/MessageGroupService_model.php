<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MessageGroupService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $this->db->insert('message_groups', $data);
        return $this->db->insert_id();
    }

    public function update_group($id, $data)
    {
        $this->db->update('message_groups', $data, array('id' => $id));
    }

    public function save_participant($data, $group_id)
    {
        $data['message_group_id'] = $group_id;
        $this->db->insert('message_group_participants', $data);
        return $this->db->insert_id();
    }

    public function save_participant_invitation($data, $group_id)
    {
        $data['message_group_id'] = $group_id;
        $data['invitation'] = 1;
        $this->db->insert('message_group_participants', $data);
        return $this->db->insert_id();
    }

    public function update_participant($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('message_group_participants', $data);
    }

}