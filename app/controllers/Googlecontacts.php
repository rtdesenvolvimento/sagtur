<?php defined('BASEPATH') or exit('No direct script access allowed');

use Google\Client;

require 'vendor/autoload.php';

class Googlecontacts extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('messages', $this->Settings->user_language);

        //service
        $this->load->model('service/GoogleContactsService_model', 'GoogleContactsService_model');

        //model
        $this->load->model('model/GooglePerson_model', 'GooglePerson_model');

        //repository
        $this->load->model('repository/GooglePersonRepository_model', 'GooglePersonRepository_model');
    }


    public function serviceGoogleCreateToken()
    {
        $client_secret = 'assets/uploads/client_secret.json';

        $client = new Client();
        $client->setApplicationName('Google Contacts Integration');
        $client->setAuthConfig($client_secret); // Substitua pelo caminho do seu arquivo de credenciais JSON
        $client->setScopes([Google_Service_PeopleService::CONTACTS]);
        $client->setRedirectUri('https://sistema.sagtur.com.br/googlecontacts/serviceGoogleCreateToken');
        $client->setAccessType('offline'); // Solicita o Refresh Token
        $client->setPrompt('consent');    // Garante que o usuário verá a solicitação para conceder o token

        $tokenPath = 'assets/uploads/token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);

            // Renovar o token se ele estiver expirado
            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $accessToken = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                    file_put_contents($tokenPath, json_encode($accessToken));
                } else {
                    if (!$client->getAccessToken() || $client->isAccessTokenExpired() && !$client->getRefreshToken()) {
                        // Redireciona para autenticação
                        header('Location: ' . $client->createAuthUrl());
                        exit;
                    }
                    exit;
                }
            }
        } else {
            // Se não há token salvo, iniciar o fluxo de autenticação
            if (!isset($_GET['code'])) {
                $authUrl = $client->createAuthUrl();
                echo "Autentique-se aqui: <a href='$authUrl'>$authUrl</a>";
                exit;
            } else {
                // Trocar o código de autorização por um token
                $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);
                if (isset($accessToken['error'])) {
                    echo "Erro ao obter o token: " . $accessToken['error_description'];
                    exit;
                }
                file_put_contents($tokenPath, json_encode($accessToken));
                $client->setAccessToken($accessToken);
            }
        }

        return $client;
    }

    public function getGoogleService()
    {
        $client_secret = 'assets/uploads/client_secret.json';

        $client = new Client();
        $client->setApplicationName('Google Contacts Integration');
        $client->setAuthConfig($client_secret); // Substitua pelo caminho do seu arquivo de credenciais JSON
        $client->setScopes([Google_Service_PeopleService::CONTACTS]);
        $client->setRedirectUri('https://sistema.sagtur.com.br/googlecontacts/serviceGoogleCreateToken');
        $client->setAccessType('offline'); // Solicita o Refresh Token
        $client->setPrompt('consent');    // Garante que o usuário verá a solicitação para conceder o token

        $tokenPath = 'assets/uploads/token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);

            // Renovar o token se ele estiver expirado
            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $accessToken = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                    file_put_contents($tokenPath, json_encode($accessToken));
                } else {
                    if (!$client->getAccessToken() || $client->isAccessTokenExpired() && !$client->getRefreshToken()) {
                        echo "NÃO ENCONTROU O TOKEN PARA CRIAÇÃO DOS CONTATOS - ENTRE EM CONTATO COM A SASGTUR";
                        exit;
                    }
                    exit;
                }
            }
        } else {
            // Se não há token salvo, iniciar o fluxo de autenticação
            if (!isset($_GET['code'])) {
                echo "NÃO ENCONTROU O TOKEN PARA CRIAÇÃO DOS CONTATOS - ENTRE EM CONTATO COM A SASGTUR";
                exit;
            } else {
                // Trocar o código de autorização por um token
                $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);
                if (isset($accessToken['error'])) {
                    echo "Erro ao obter o token: " . $accessToken['error_description'];
                    exit;
                }
                file_put_contents($tokenPath, json_encode($accessToken));
                $client->setAccessToken($accessToken);
            }
        }

        return $client;
    }

    public function export_peoples()
    {
        try {

            $googleService = $this->getGoogleService();

            $peoples = $this->GooglePersonRepository_model->getAll(false);

            foreach ($peoples as $person) {
                $this->GoogleContactsService_model->create_person($googleService, $person);
            }

            $this->session->set_flashdata('success', lang('peoples_exported'));

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function createTeste()
    {
        try {
            $googleService = $this->getGoogleService();

            $googlePerson = new GooglePerson_model();
            $googlePerson->givenName = 'João';
            $googlePerson->familyName = 'Velho';
            $googlePerson->phoneNumbers = '5548998235746';

            $this->GoogleContactsService_model->create_person($googleService, $googlePerson);

            $this->session->set_flashdata('success', lang('peoples_exported'));

            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}