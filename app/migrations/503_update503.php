<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update503 extends CI_Migration {

    public function up() {

        $this->alter_table_taxas_venda_configuracao();
        $this->alter_table_tipo_cobranca_produto();

        $this->db->update('settings',  array('version' => '5.0.3'), array('setting_id' => 1));
    }

    public function alter_table_taxas_venda_configuracao() {
        $fields = array(
            'diasAvancaPrimeiroVencimentoPosSinal' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'usarCondicaoPosSinal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('taxas_venda_configuracao', $fields);

        $this->db->query("UPDATE sma_taxas_venda_configuracao SET diasAvancaPrimeiroVencimentoPosSinal = diasAvancaPrimeiroVencimento;");
    }


    public function alter_table_tipo_cobranca_produto() {
        $fields = array(
            'diasAvancaPrimeiroVencimentoPosSinal' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'usarCondicaoPosSinal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('tipo_cobranca_produto', $fields);

        $this->db->query("UPDATE sma_tipo_cobranca_produto SET diasAvancaPrimeiroVencimentoPosSinal = diasAvancaPrimeiroVencimento;");
    }

    public function down() {}
}
