<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Class constructor
	 *
	 * @link	https://github.com/bcit-ci/CodeIgniter/issues/5332
	 * @return	void
	 */
	public function __construct() {}

	/**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key)
	{
		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}

    function __toArray() {
        return get_object_vars($this);
    }

    function __toModel($array, $object) {
        foreach ($array as $k=> $v) $object->{$k} = $v;

        return $object;
    }

    /*
    function __toModelAll($arrayItens, $object) {
	    foreach ($arrayItens as $item) {
            $this->__toModel($item, $object);
        }
    }
    */

    function __save($table, $data)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return $this->db->insert_id($table);
        }
        return FALSE;
    }

    function __update($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }


    function __delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function __getById($table, $id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get($table)->row();
    }

    function __openOrException($table, $id) {
        $obj = $this->__getById($table, $id);
        if ($obj->id != null) return $obj;
        else throw new Exception("Não foi possível encontrar [".$table."]");
    }


    function __edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function __exist($table, $id) {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result = $this->db->get($table)->row(); // Retorna um objeto ou null
        return $result !== null; // Verifica se o objeto não é null
    }

    function __existByField($table, $field, $id) {
        $this->db->where($field,$id);
        $this->db->limit(1);
        return count($this->db->get($table)->row()) > 0;
    }

    public function __getByIdToModel($table, $id, $object) {
        return $this->__toModel($this->__getById($table, $id), $object);
    }

    public function __getAll($table)
    {
        $this->db->from($table);
        return $this->db->get()->result();
    }

    public function __getAllItens($table, $fatherField, $fatherId) {
        $this->db->from($table);
        $this->db->where($fatherField, $fatherId);
        return $this->db->get()->result();
    }

    function __count($table)
    {
        return $this->db->count_all($table);
    }

    function __iniciaTransacao(){
        $this->db->_trans_begin();
    }

    function __confirmaTransacao() {
        $this->db->_trans_commit();
    }

    function __cancelaTransacao() {
        $this->db->_trans_rollback();
    }


}
