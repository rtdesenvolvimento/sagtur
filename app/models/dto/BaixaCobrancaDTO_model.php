<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaixaCobrancaDTO_model extends CI_Model
{

    public $faturas = [];
    public $sucesso = false;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarFatura($fatura) {
        $this->faturas[count($this->faturas)] = $fatura;
    }
}
