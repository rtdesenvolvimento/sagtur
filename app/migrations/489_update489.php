<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update489 extends CI_Migration {

    public function up() {

        $this->update_shop_settings();

        $this->db->update('settings',  array('version' => '4.8.9'), array('setting_id' => 1));
    }

    public function update_shop_settings()
    {
        $this->db->update('shop_settings',  array('cache_minutes' => 180), array('id' => 1));//3 hora
    }

    public function down() {}
}
