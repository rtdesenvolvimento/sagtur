<style media="screen">
    #FechamentoComissaoData td:nth-child(2) {text-align: center;}
    #FechamentoComissaoData td:nth-child(3) {text-align: center;}
    #FechamentoComissaoData td:nth-child(4) {width: 40%;}

    #FechamentoComissaoData td:nth-child(5) {text-align: right;}
    #FechamentoComissaoData td:nth-child(6) {text-align: right;}
    #FechamentoComissaoData td:nth-child(7) {text-align: right;}

    #FechamentoComissaoData td:nth-child(8) {text-align: center;}

    .badge {
        font-size: 11px;
        font-weight: 700;
        color: #404040;
        border: 1px solid #d9d9d9;
        margin: 0 2px;
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        line-height: 1;
        vertical-align: middle;
        white-space: nowrap;
        text-align: center;
        background-color: #fdf59a;
        border-radius: 3px;
    }

</style>

<script>

    $(document).ready(function () {

        function st_status(x) {
            if (x === 'Pendente') {
                return '<div class="text-center"><span class="label label-danger">Pendente</span></div>';
            } else if (x === 'Confirmada') {
                return '<div class="text-center"><span class="label label-success">Confirmada</span></div>';
            } else if (x === 'Cancelada') {
                return '<div class="text-center"><span class="label label-beige" style="background: #000000">Cancelada</span></div>';
            }

            return x;
        }

        var oTable = $('#FechamentoComissaoData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('commissions/getFechamentosComissao')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "fechamento_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filter_date_de", "value":  $('#filter_dateDe').val() });
                aoData.push({ "name": "filter_date_ate", "value":  $('#filter_dateAte').val() });
                aoData.push({ "name": "filter_biller", "value":  $('#filter_biller').val() });
                aoData.push({ "name": "filter_status", "value":  $('#filter_status').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                {"mRender": fsd},
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": st_status},
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][4]);
                    paid += parseFloat(aaData[aiDisplay[i]][5]);
                    balance += parseFloat(aaData[aiDisplay[i]][6]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[5].innerHTML = currencyFormat(parseFloat(paid));
                nCells[6].innerHTML = currencyFormat(parseFloat(balance));
            },
        });

    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('commissions/commissions_close_actions', 'id="action-form"');
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue">
                    <i class="fa-fw fa fa-money"></i><?=lang('fechamento_comissao')?>
                </h2>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a href="<?=site_url('commissions/add_fechamento')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-plus"></i> <?=lang('add_fechamento_comissao')?>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" id="excel" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> <?=lang('export_commissions_approved_to_excel')?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <?= lang("dt_competencia_de", "filter_dateDe") ?>
                            <?= form_input('filter_dateDe', (isset($_POST['filter_dateDe']) ? $_POST['filter_dateDe'] : ''), 'class="form-control" id="filter_dateDe"','date'); ?>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <?= lang("dt_competencia_ate", "filter_dateAte") ?>
                            <?= form_input('filter_dateAte', (isset($_POST['filter_dateAte']) ? $_POST['filter_dateAte'] : ''), 'class="form-control" id="filter_dateAte"','date'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?= lang("biller", "filter_tour") ?>
                            <?php
                            $pBillers[''] =  lang("select") . " " . lang("biller") ;
                            foreach ($billers as $biller) {
                                $pBillers[$biller->id] = $biller->name;
                            }
                            echo form_dropdown('filter_biller', $pBillers, (isset($_POST['filter_biller']) ? $_POST['filter_biller'] : ''), 'class="form-control select" id="filter_biller" placeholder="' . lang("select") . " " . lang("biller") . '" style="width:100%"')
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("status", "status") ?>
                        <?php
                        $opts = array(
                             '' => lang('select'),
                            'Pendente' => lang('pendente'),
                            'Confirmada' => lang('confirmada'),
                            'Cancelada' => lang('cancelada'),
                        );
                        echo form_dropdown('filter_status', $opts,  '', 'class="form-control" id="filter_status" style="width:100%"');
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="FechamentoComissaoData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                <thead>
                                <tr>
                                    <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("competencia"); ?></th>
                                    <th style="text-align: left;"><?php echo $this->lang->line("title"); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("total_commission"); ?></th>
                                    <th><?php echo $this->lang->line("paid"); ?></th>
                                    <th><?php echo $this->lang->line("balance"); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("status"); ?></th>
                                    <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr><td colspan="7" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("competencia"); ?></th>
                                    <th style="text-align: left;"><?php echo $this->lang->line("title"); ?></th>
                                    <th style="text-align: right;"><?php echo $this->lang->line("total_commission"); ?></th>
                                    <th><?php echo $this->lang->line("paid"); ?></th>
                                    <th><?php echo $this->lang->line("balance"); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line("status"); ?></th>
                                    <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#filter_dateDe").change(function (event){
            $('#FechamentoComissaoData').DataTable().fnClearTable();
        });

        $("#filter_dateAte").change(function (event){
            $('#FechamentoComissaoData').DataTable().fnClearTable();
        });

        $("#filter_biller").change(function (event){
            $('#FechamentoComissaoData').DataTable().fnClearTable();
        });

        $("#filter_status").change(function (event){
            $('#FechamentoComissaoData').DataTable().fnClearTable();
        });

        $('body').on('click', '.fechamento_link td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'commissions/modal_view/' + $(this).parent('.fechamento_link').attr('id')});
            $('#myModal').modal('show');
        });

    });
</script>
