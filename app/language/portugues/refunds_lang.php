<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['refunds']                = 'Reembolsos';
$lang['refund']                   = 'Reembolso';
$lang['refund_cards'] 			= "Reembolso de Clientes";
$lang['refund_cards_report']           = 'Relatório de Reembolsos';
$lang['add_refund_card'] 			= "Adicionar Reembolso";
$lang['edit_refund_card'] 		= "Editar Reembolso";
$lang['delete_refund_cards'] 		= "Excluir Reembolso";
$lang['refund_card_added'] 		= "Reembolso adicionado com sucesso";
$lang['refund_card_updated'] 		= "Reembolso atualizado com sucesso";
$lang['refund_card_deleted'] 		= "Reembolso excluído com sucesso";
$lang['refund_cards_deleted'] 	= "Reembolso excluído com sucesso";
$lang['card_no'] 				= "Nº do Reembolso";
$lang['balance'] 				= "Saldo";
$lang['value'] 					= "Pagar";
$lang['new_refund_card'] 			= "Adicionar Reembolso";
$lang['sell_refund_card'] 		= "Usar um Reembolso";
$lang['refund_card']              = 'Reembolso';
$lang['view_refund_card']         = "Reembolso Impressão";
$lang['view_refunds_card']         = "Reembolso Impressão";
$lang['card_expired']           = "Este Reembolso já está Vencido";
$lang['cancel_refunds_card']       = 'Cancelar Reembolso';
$lang['motivo_cancelamento']    = 'Motivo Cancelamento';
$lang['refund_cancelado_com_sucesso'] = 'Reembolso de Cliente Cancelado com Sucesso';
$lang['card_is_used']           = "Este Reembolso já foi usado";
$lang['card_cancel']            = 'Reembolso Cancelado';
$lang['add_payment'] 		    = "Adicionar Pagamento";
$lang['valor_vencimento']       = 'Valor Vencimento';
$lang['pago_financeiro']        = "Pago";
$lang['tipo_cobranca_financeiro'] = "Tipo de Cobrança";
$lang['valor_pagamento']        = "Valor do Pagamento";
$lang['forma_pagamento']        = "Forma de Pagamento";
$lang['payment_added'] 			= "Pagamento Adicionado com Sucesso";
$lang['refund_card_balance']      = 'Saldo do Reembolso';
$lang['refund_impressao'] = 'Imprimir Reembolso';
$lang['email_refund']             = 'Enviar E-mail do Reembolso';
$lang['impressao_reembolso']      = 'Imprimir Reembolso';
$lang['paid_by'] 				= "Pago em";
$lang['movimentador']           = "Conta";
$lang['view_payments']          = 'Ver Pagamentos';
$lang['no_refund_card_selected'] = 'Nenhum Reembolso Selecionado';