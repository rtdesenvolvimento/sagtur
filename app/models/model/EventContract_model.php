<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EventContract_model extends CI_Model
{

    public $date;
    public $document_id;
    public $user_id;
    public $user;
    public $event;
    public $status;
    public $log;
    public $link_contract;


    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDocumentId()
    {
        return $this->document_id;
    }

    /**
     * @param mixed $document_id
     */
    public function setDocumentId($document_id): void
    {
        $this->document_id = $document_id;
    }


    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event): void
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log): void
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getLinkContract()
    {
        return $this->link_contract;
    }

    /**
     * @param mixed $link_contract
     */
    public function setLinkContract($link_contract): void
    {
        $this->link_contract = $link_contract;
    }

}
