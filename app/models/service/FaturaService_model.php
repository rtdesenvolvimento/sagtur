<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FaturaService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository_model');
        $this->load->model('repository/CommissionsRepository_model', 'CommissionsRepository_model');
        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');
        $this->load->model('service/ParcelaService_model','ParcelaService_model');
        $this->load->model('service/MovimentadorSaldoPeriodo_model', 'MovimentadorSaldoPeriodo_model');
        $this->load->model('service/AgendamentoServico_model', 'AgendamentoServico_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');

        $this->load->model('model/Fatura_model', 'Fatura_model');
        $this->load->model('model/Pagamento_model', 'Pagamento_model');

        $this->load->model('financeiro_model');
    }

    public function editarFatura($faturaModel) {

        try {
            //$faturaModel = new Fatura_model();

            $this->__iniciaTransacao();

            //if ($this->sma->isContaVencida($faturaModel->dtvencimento)) throw new Exception('Não é permitido editar a fatura com data vencida!');

            $fatura = $this->__getById('fatura', $faturaModel->faturaId);
            $parcela = $this->FinanceiroRepository_model->getParcelaOneByFatura($fatura->id);//TODO AQUI SERA MUDADO COM O SISTEMA DE FATURAMENTO

            $dados_atualizados = array(
                'totalAcrescimo' => $faturaModel->totalAcrescimo,
                'totalDesconto' => $faturaModel->totalDesconto,
                'acrescimo' => $faturaModel->totalAcrescimo,
                'desconto' => $faturaModel->totalDesconto,
                'dtvencimento' => $faturaModel->dtvencimento,
                'valorpagar' => $faturaModel->valorpagar + $faturaModel->totalAcrescimo - $faturaModel->totalDesconto,
                'receita' => $faturaModel->receita,
                'tipocobranca' => $faturaModel->tipocobranca,
                'movimentador' => $faturaModel->movimentador,
                'note' => $faturaModel->nota
            );

            if ($this->isGeraSegundaViaCobrancaFatura($fatura, $faturaModel)) {

                $this->CobrancaService_model->cancelarCobranca($fatura->id, true);

                $this->__edit('parcela', $dados_atualizados, 'id', $parcela->parcela);
                $this->__edit('fatura', $dados_atualizados, 'id', $fatura->id);

                $this->geraCobrancasFatura($fatura->id);
            }

            $this->__confirmaTransacao();

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function isGeraSegundaViaCobrancaFatura($fatura, $faturaModel) {
        return ($fatura->valorpagar != $faturaModel->valorpagar || $fatura->tipocobranca != $faturaModel->tipocobranca);
    }

    private function geraCobrancasFatura($faturaId) {
        $cobrancasFatura = new CobrancaFaturaDTO_model();
        $cobrancasFatura->adicionarFatura($this->__getById('fatura', $faturaId));

        $this->CobrancaService_model->geraCobrancasFatura($cobrancasFatura);
    }

    public function consultaPagamentoByCode($code) {
        return $this->CobrancaService_model->consultaPagamentoByCode($code);
    }

    public function buscarDadosTransacaoByReference($code) {
        return $this->CobrancaService_model->buscarDadosTransacaoByReference($code);
    }

    private function permiteAtualizarFatura($fatura) {
        return $fatura->status == Financeiro_model::STATUS_ABERTA || $fatura->status == Financeiro_model::STATUS_PARCIAL;
    }

    private function pagamentoCancelado($pagamento) {
        return $pagamento->status == 'CANCELADA' ||
            $pagamento->status == 'cancelled' ||
            $pagamento->status == 'canceled';
    }

    private function pagamentoEmProcesso($pagamento) {
        return $pagamento->status == 'in_process' ||
            $pagamento->status == 'processing' ||
            $pagamento->status == 'pending'  ;
    }

    public function baixarPagamentoIntegracaoByReference($integracao, $reference) {
        try {
            $this->__iniciaTransacao();

            $cobrancas = $this->CobrancaService_model->baixarPagamentoIntegracaoReference($integracao, $reference);

            $this->baixar($cobrancas);

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function baixarPagamentoIntegracao($integracao, $code) {

        try {
            $this->__iniciaTransacao();

            $cobrancas = $this->CobrancaService_model->baixarPagamentoIntegracao($integracao, $code);

            $this->baixar($cobrancas);

            $this->__confirmaTransacao();
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function baixar($cobrancas) {

        foreach ($cobrancas->faturas as $cobranca) {

            $cobrancaSistema = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracaoAberta($cobranca->code);

            if (empty($cobrancaSistema)) {
                return;
            }

            $fatura = $this->__getById('fatura', $cobrancaSistema->fatura);

            if ($this->permiteAtualizarFatura($fatura)) {

                foreach ($cobranca->pagamentos as $pagamento) {

                    if ($this->statusAutorizado($pagamento)) {

                        if ($this->pagamentoCancelado($pagamento)) {
                            $this->cancelarAgendamento($fatura->id);
                            $this->cancelarFatura($fatura->id, $cobranca);
                            $this->cancelarFaturaCobranca($cobrancaSistema->id);
                        } else if ($this->pagamentoEmProcesso($pagamento)) {
                            $this->pendenciaFaturaCobranca($cobrancaSistema->id);
                        } else {
                            $payment = $this->preencherPagamento($pagamento, $fatura, $cobranca->code);
                            $this->sales_model->addPayment($payment, NULL, $fatura->id);
                        }
                    }
                }
            }
        }
    }

    private function statusAutorizado($pagamento) {
        return
            $pagamento->status == 'CONFIRMED' ||
            $pagamento->status == 'AUTHORIZED' ||
            $pagamento->status == 'approved' ||
            $pagamento->status == 'APPROVED' ||

            $pagamento->status == 'RECEIVED' ||
            $pagamento->status == 'CONFIRMED' ||
            $pagamento->status == 'RECEIVED_IN_CASH' ||

            $pagamento->status == 'paid' ||
            $pagamento->status == 'manual_payment' ||

            $pagamento->status == 'in_process' ||

            $pagamento->status == 'CANCELADA' ||
            $pagamento->status == 'canceled' ||
            $pagamento->status == 'cancelled';
    }

    private function cancelarFaturaCobranca($faturaCobrancaId) {
        $this->__update('fatura_cobranca', array('status'=> 'CANCELADA'), 'id', $faturaCobrancaId);
    }

    private function pendenciaFaturaCobranca($faturaCobrancaId) {
        $this->__update('fatura_cobranca', array('status'=> 'PENDENTE'), 'id', $faturaCobrancaId);
    }

    private function cancelarFatura($faturaId, $cobranca = null) {
        $parcelasFatura = $this->financeiro_model->getParcelasByFatura($faturaId);

        foreach ($parcelasFatura as $parcelaFatura) {
            $parcela = $this->financeiro_model->getParcelaById($parcelaFatura->parcela);

            if ($parcela->contareceberId > 0)  {
                $contaReceber = $this->financeiro_model->getContaReceberById($parcela->contareceberId);

                if ($contaReceber->sale > 0) {

                    $faturasContaReceber = $this->financeiro_model->getParcelasFaturaByContaReceber($contaReceber->sale );
                    $faturasContaPagar = $this->financeiro_model->getParcelasFaturaByContaPagar($contaReceber->sale );

                    foreach ($faturasContaPagar as $faturasDebito) {
                        $this->cancelar($faturasDebito->id, NULL, FALSE);
                    }

                    foreach ($faturasContaReceber as $faturasCredito) {
                        $this->cancelar($faturasCredito->id, NULL, FALSE);
                    }

                    $this->cancelarVenda($contaReceber->sale, $cobranca);
                } else {
                    $this->cancelar($faturaId, NULL, FALSE);
                }
            }
        }
    }

    private function cancelarVenda($saleId, $cobranca) {
        if ($saleId > 0) {
            $this->__update('sales', array('sale_status'=> 'cancel', 'payment_status' => 'cancel'), 'id', $saleId);

            $this->SaleEventService_model->cancel_integration($saleId, $cobranca->log);
        }
    }

    private function cancelarAgendamento($faturaId) {
        $parcelasFatura = $this->financeiro_model->getParcelasByFatura($faturaId);

        foreach ($parcelasFatura as $parcelaFatura) {
            $parcela = $this->financeiro_model->getParcelaById($parcelaFatura->parcela);

            if ($parcela->contareceberId > 0) $this->AgendamentoServico_model->cancelarByContaReceber($parcela->contareceberId );
        }
    }

    private function preencherPagamento($pagamento, $fatura, $approval_code) {

        $parcelaFatura = $this->FinanceiroRepository_model->getParcelaOneByFatura($fatura->id);
        $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
        $contaReceber = $this->__getById('conta_receber', $parcela->contareceberId);
        $tipoCobranca = $this->__getById('tipo_cobranca', $fatura->tipoCobranca);
        $formaPagamento = $this->__getById('forma_pagamento', $tipoCobranca->formapagamento);

        $dtPagamento = str_replace('/', '-', $pagamento->dataPagamento);
        $dtPagamento = date('Y-m-d H:i:s', strtotime($dtPagamento));

        if ($tipoCobranca->tipo == 'carne_cartao') {
            $valorPago = $pagamento->valorPago*$fatura->numeroParcelaCC;
            $acrescimo = $valorPago - $fatura->valorfatura;
            $taxas = $pagamento->taxa*$fatura->numeroParcelaCC;
        } else {
            $valorPago = $pagamento->valorPago;
            $acrescimo = $pagamento->valorPago - $fatura->valorfatura;
            $taxas = $pagamento->taxa;
        }

        $payment = array(
            'reference_no' => $this->site->getReference('pay'),
            'type' => 'received',
            'date' => $dtPagamento,
            'sale_id' => $contaReceber->sale,
            'fatura' => $fatura->id,
            'pessoa' => $fatura->pessoa,
            'amount' => $valorPago,
            'acrescimo' => $acrescimo,
            'taxa'  => $taxas,
            'movimentador' => $fatura->movimentador,
            'formapagamento' => $formaPagamento->id,
            'tipoCobranca' => $tipoCobranca->id,
            'paid_by' => $formaPagamento->name,
            'note' => $pagamento->observacao,
            'approval_code' => $approval_code,
            'created_by' => 4//TODO VIA CONFIGURACAO PORQUE O SISTEMA NAO SABE O USUARIO QUANDO NAO ESTA LOGADO
        );

        return $payment;
    }

    public function cancelar($faturaId, $motivoCancelamento, $cancelarCobranca = TRUE) {

        try {

            $this->db->_trans_begin();

            $fatura = $this->__getById('fatura', $faturaId);

            if ($this->isCancelada($fatura)) {
                throw new Exception("A Conta já encontra-se CANCELADA e não pode ser cancelada novamente.");
            }

            if ($this->isReparcelada($fatura)) {
                throw new Exception("A Conta encontra-se REPARCELADA e não pode ser cancelada.");
            }

            if ($this->comPagamentos($faturaId)) {
                throw new Exception("Não é possível cancelar uma conta com pagamentos já realizado, estorne os pagamentos e tente cancelar novamente a conta.");
            }

            $parcelasFatura = $this->FinanceiroRepository_model->getParcelasByFatura($faturaId);

            foreach ($parcelasFatura as $parcela) {
                $this->ParcelaService_model->cancelar($parcela->parcela, $motivoCancelamento);
            }

            if ($cancelarCobranca) {
                $this->CobrancaService_model->cancelarCobranca($faturaId);
            }

            $this->__update('fatura', array('status' => Fatura_model::STATUS_CANCELADA, 'motivo_cancelamento' => $motivoCancelamento), 'id', $faturaId);

            $this->db->_trans_commit();

        } catch(Exception $exception) {
            $this->db->_trans_rollback();
            throw new Exception($exception->getMessage());
        }
    }

    public function cancelar_com_multa($faturaId, $motivoCancelamento, $cancelarCobranca = TRUE)
    {
        try {

            $this->db->_trans_begin();

            $fatura = $this->__getById('fatura', $faturaId);

            if ($this->isCancelada($fatura)) {
                throw new Exception("A Conta já encontra-se CANCELADA e não pode ser cancelada novamente.");
            }

            if ($this->isReparcelada($fatura)) {
                throw new Exception("A Conta encontra-se REPARCELADA e não pode ser cancelada.");
            }

            $parcelasFatura = $this->FinanceiroRepository_model->getParcelasByFatura($faturaId);

            //se a parcela estiver aberta entao pode cancelar sem problemas pois nao havera mais caixa par aela
            if ($this->isAberta($fatura)) {
                foreach ($parcelasFatura as $parcela) {
                    $this->ParcelaService_model->cancelar($parcela->parcela, $motivoCancelamento);
                }
            } elseif ($this->isParcial($fatura)) {
                foreach ($parcelasFatura as $parcela) {
                    $this->ParcelaService_model->cancelar_com_lancamento_credito($parcela->parcela);
                }
            }

            //cancela a integracao apenas das parcelas abertas
            if ($cancelarCobranca && $this->isAberta($fatura)) {
                $this->CobrancaService_model->cancelarCobranca($faturaId);
            }

            if ($this->isAberta($fatura)) {
                $data_fatura = array(
                    'status' => Fatura_model::STATUS_CANCELADA,
                    'motivo_cancelamento' => $motivoCancelamento,
                );
                $this->__update('fatura', $data_fatura, 'id', $faturaId);
            } elseif ($this->isParcial($fatura)) {
                $data_fatura = array(
                    'status'        => Fatura_model::STATUS_QUITADA,
                    'totalcredito'  => $fatura->valorpago,
                    'valorfatura'   => $fatura->valorpago,
                    'valorpago'     => $fatura->valorpago,
                    'valorpagar'    => 0,
                );
                $this->__update('fatura', $data_fatura, 'id', $faturaId);
            }

            $this->db->_trans_commit();

        } catch(Exception $exception) {
            $this->db->_trans_rollback();
            throw new Exception($exception->getMessage());
        }
    }

    public function cancelar_com_lancamento_credito($faturaId, $motivoCancelamento, $cancelarCobranca = TRUE)
    {
        try {

            $this->db->_trans_begin();

            $fatura = $this->__getById('fatura', $faturaId);

            if ($this->isCancelada($fatura)) throw new Exception("A Conta já encontra-se CANCELADA e não pode ser cancelada novamente.");
            if ($this->isReparcelada($fatura)) throw new Exception("A Conta encontra-se REPARCELADA e não pode ser cancelada.");

            $parcelasFatura = $this->FinanceiroRepository_model->getParcelasByFatura($faturaId);

            //se a parcela estiver aberta entao pode cancelar sem problemas pois nao havera mais caixa par aela
            if ($this->isAberta($fatura)) {
                foreach ($parcelasFatura as $parcela) {
                    $this->ParcelaService_model->cancelar($parcela->parcela, $motivoCancelamento);
                }
            } elseif ($this->isParcial($fatura)) {
                foreach ($parcelasFatura as $parcela) {
                    $this->ParcelaService_model->cancelar_com_lancamento_credito($parcela->parcela);
                }
            }

            //cancela a integracao apenas das parcelas abertas
            if ($cancelarCobranca && $this->isAberta($fatura)) {
                $this->CobrancaService_model->cancelarCobranca($faturaId);
            }

            if ($this->isAberta($fatura)) {
                $data_fatura = array(
                    'status' => Fatura_model::STATUS_CANCELADA,
                    'motivo_cancelamento' => $motivoCancelamento,
                );
                $this->__update('fatura', $data_fatura, 'id', $faturaId);
            } elseif ($this->isParcial($fatura)) {
                $data_fatura = array(
                    'status'        => Fatura_model::STATUS_QUITADA,
                    'totalcredito'  => $fatura->valorpago,
                    'valorfatura'   => $fatura->valorpago,
                    'valorpago'     => $fatura->valorpago,
                    'valorpagar'    => 0,
                );
                $this->__update('fatura', $data_fatura, 'id', $faturaId);
            }

            $this->db->_trans_commit();

        } catch(Exception $exception) {
            $this->db->_trans_rollback();
            throw new Exception($exception->getMessage());
        }
    }

    public function estornar($pagamentoId, $motivoEstornoDePagamento, $cancelar_pagamento_credito = true) {
        try {

            $this->__iniciaTransacao();

            $pagamento      = $this->__getById('payments', $pagamentoId);
            $fatura         = $this->__getById('fatura', $pagamento->fatura);
            $parcelasFatura = $this->financeiro_model->getParcelasByFatura($fatura->id);

            foreach ($parcelasFatura as $parcelaFatura) {

                $parcela = $this->financeiro_model->getParcelaById($parcelaFatura->parcela);

                $valorPago          = $pagamento->amount;
                $taxa               = $pagamento->taxa;
                $acrescimo          = $pagamento->acrescimo;
                $valorVencimento    = $fatura->valorfatura;
                $valorPagar         = $fatura->valorpagar + $valorPago + $taxa - $acrescimo;
                $valorPago          = $fatura->valorpago - $valorPago;
                $saldo              = $valorVencimento - $valorPagar;

                if ($saldo > 0) {
                    $status = Financeiro_model::STATUS_PARCIAL;
                } else {
                    $status = Financeiro_model::STATUS_ABERTA;
                }

                $dataUltimoPagamento = $this->FinanceiroRepository_model->buscarUltimoPagamentoParaEstorno($pagamento->id);

                $atualizarFatura = array(
                    'dtultimopagamento' => $dataUltimoPagamento->date,
                    'valorpago'         => $valorPago,
                    'valorpagar'        => $valorPagar,
                    'status'            => $status,
                    'taxas'             => $fatura->taxas - $taxa,
                    'totalAcrescimo'    => $fatura->totalAcrescimo - $acrescimo,
                    'acrescimo'         => $fatura->acrescimo - $acrescimo
                );

                $atualizarParcela = array(
                    'dtultimopagamento' => $dataUltimoPagamento->date,
                    'valorpago'         => $valorPago,
                    'valorpagar'        => $valorPagar,
                    'status'            => $status,
                    'taxas'             => $parcela->taxas - $taxa,
                    'totalAcrescimo'    => $parcela->totalAcrescimo - $acrescimo,
                    'acrescimo'         => $parcela->acrescimo - $acrescimo
                );

                $this->financeiro_model->edit('fatura', $atualizarFatura, 'id', $fatura->id);
                $this->financeiro_model->edit('parcela_fatura', array('status' => $status), 'id', $parcelaFatura->id);
                $this->financeiro_model->edit('parcela', $atualizarParcela, 'id', $parcela->id);

                if ($parcela->contareceberId != null ) {

                    $this->financeiro_model->edit('conta_receber',  array('status' => $status), 'id', $parcela->contareceberId );
                    $this->reabrirPagamentoAgendamento($parcela->contareceberId);

                    $fatura_cobranca = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);

                    if ($fatura_cobranca) {
                        $this->financeiro_model->dezconfirmacao_pagamento($fatura_cobranca, $fatura_cobranca->code);
                    }

                }

                if ($parcela->contapagarId != null ) {

                    if ($parcela->fechamento_comissao_id) {
                        $this->atualizar_comissao_vendedor($parcela->fechamento_comissao_id, $parcela->fechamento_item_id, $pagamento->amount, $status);
                    }

                    $this->financeiro_model->edit('conta_pagar',  array('status' => $status), 'id', $parcela->contaPagarId );
                }
            }

            $this->CobrancaService_model->estornarCobranca($pagamentoId);

            $this->MovimentadorSaldoPeriodo_model->atualizarMovimentadorSaldoNoPeriodo($fatura, $pagamento);

            $this->__edit('payments', array('status'=> Pagamento_model::STATUS_ESTORNO, 'motivo_estorno' => $motivoEstornoDePagamento), 'id', $pagamentoId);

            $this->site->syncSalePayments($pagamento->sale_id);

            //atualiza a carta de credito
            if ($pagamento->cc_no) {

                $gc     = $this->site->getGiftCardByNO($pagamento->cc_no);

                $saldo  =  ($gc->balance + $pagamento->amount);
                $status = 'partial';

                if ($saldo == $gc->value) {
                    $status = 'due';
                }

                $this->db->update('gift_cards', array('balance' => $saldo, 'status' => $status), array('card_no' => $pagamento->cc_no));

                //estornar o pagamento do credito do  contas a pagar vinculado a essa conta
                $fatura_credito     = $this->GiftRepository_model->getFaturaByGift($gc->id);
                $payments_credito   = $this->financeiro_model->getPagamentosByFatura($fatura_credito->faturaId);

                foreach ($payments_credito as $payment_credito) {
                    if ($payment_credito->status == 'ESTORNO') {
                        continue;
                    }

                    $this->estornar($payment_credito->id, $motivoEstornoDePagamento, false);
                }
            }

            if ($pagamento->sale_id) {
                $user = $this->site->getUser( $this->session->userdata('user_id'));
                $event = "Pagamento Estornado de ". $this->sma->formatMoney($pagamento->amount)." em ".$pagamento->paid_by.". Obs: ".$motivoEstornoDePagamento;
                $this->SaleEventService_model->estorno($pagamento->sale_id, $event, $user);
            }

            //neste caso estorna tambem o pagamento de origem do credito
            if ($pagamento->origin_payment_id && $cancelar_pagamento_credito) {
                $this->estornar($pagamento->origin_payment_id, $motivoEstornoDePagamento);
            }

            $this->__confirmaTransacao();
        } catch(Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function atualizar_comissao_vendedor($fechamento_id, $fechamento_item_id, $amount, $status)
    {

        $this->db->set('paid', 'paid-'.$amount, FALSE);
        $this->db->where('id', $fechamento_id);
        $this->db->update('close_commissions');

        if ($status == Financeiro_model::STATUS_ABERTA) {
            $this->db->set('status', 'Aprovada');
        } else {
            $this->db->set('status', $status);
        }

        $this->db->set('paid', 'paid-'.$amount, FALSE);
        $this->db->where('id', $fechamento_item_id);
        $this->db->update('close_commission_items');

        //log da comissao paga
        $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamento_id);
        $fechamentoItem = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($fechamento_item_id);
        $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($fechamentoItem->commission_item_id);

        $note = 'Comissão com pagamento estorndo no fechamento ('.$fechamento->reference_no.') '.$fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);
        $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $itemComissao->id, $note, CommissionEventService_model::COMISSAO_PAGAMENTO_ESTORNADO, $this->session->userdata('user_id'));
    }

    private function reabrirPagamentoAgendamento($contareceberId) {
        $this->AgendamentoServico_model->reabrirPagamentoAgendamento($contareceberId);
    }

    public function reembolso($faturaId, $motivoReembolso) {

        try {
            $this->__iniciaTransacao();

            $fatura = $this->__getById('fatura', $faturaId);

            if ($this->isCancelada($fatura)) throw new Exception("A Conta encontra-se CANCELADA e não pode ser reembolsada.");
            if ($this->isReembolso($fatura)) throw new Exception("A Conta já tem um REEMBOLSO e não pode ser reembolsada novamente.");

            $parcelasFatura = $this->FinanceiroRepository_model->getParcelasByFatura($faturaId);

            foreach ($parcelasFatura as $parcela) {
                $this->ParcelaService_model->reembolso($parcela->parcela);
            }

            $this->__update('fatura', array('valorpagar' => 0, 'status' => Fatura_model::STATUS_REEMBOLSO, 'motivo_reembolso' => $motivoReembolso), 'id', $faturaId);

            $this->__confirmaTransacao();
        } catch(Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function consultaSaldo() {
        echo $this->CobrancaService_model->consultaSaldo();
    }

    private function comPagamentos($faturaId) {
        $pagamentos = $this->FinanceiroRepository_model->buscarPagamentoNaoEstornadosFatura($faturaId);
        return count($pagamentos) > 0;
    }

    private function isCancelada($fatura) {
        return $fatura->status === Fatura_model::STATUS_CANCELADA;
    }

    private function isReparcelada($fatura) {
        return $fatura->status === Fatura_model::STATUS_REPARCELADA;
    }

    private function isReembolso($fatura) {
        return $fatura->status === Fatura_model::STATUS_REEMBOLSO;
    }

    private function isAberta($fatura)
    {
        return $fatura->status === Fatura_model::STATUS_ABERTA;
    }

    private function isParcial($fatura)
    {
        return $fatura->status === Fatura_model::STATUS_PARCIAL;
    }
}
