<style>
    .panel-info {
        transition: box-shadow 0.3s ease-in-out;
    }

    .panel-info:hover {
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
    }

    .folder {
        padding: 35px;
        border: 1px solid #ddd;
        box-shadow: none;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .status_documento {
        font-size: 1.3rem;
    }
    .sigantarios {
        padding: 5px;
        font-size: 11px;
        font-weight: 500;
    }

    }

</style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder"></i><?= lang('my_folders'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('contracts/addFolder'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_folder') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-12">
                <div id="grid_search"></div>
            </div>
            <div class="col-md-12" style="text-align: center;margin-top: 25px;">
                <div class="form-group">
                    <?php echo form_button('load_more',  $this->lang->line("load_more_folders"), 'id="load_more" class="btn btn-primary"'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript">

    var limit = 0;

    $(document).ready(function () {

        load_folders_search();

        $('#load_more').click(function () {
            load_folders_search();
        });

    });

    function load_folders_search() {

        limit += 25;

        var searchTerm = $('#search_term').val();
        var statusFilter = $('#status_filter').val();

        var url = '<?= base_url('contracts/folders_search') ?>?search_term=' + encodeURIComponent(searchTerm) +
            '&status=' + encodeURIComponent(statusFilter) +
            '&limit=' + limit;

        $('#grid_search').load(url, function () {

            $('.folders').click(function (event) {

                if ($(event.target).closest('.box-icon').length > 0) {
                    if ($(event.target).is('a')) {
                        return true;
                    } else {
                        return;
                    }
                }

                var document_id = $(this).attr('document_id');
                var status = $(this).attr('status');

                if (status === 'draft') {
                   window.location.href = '<?=base_url('contracts/addSignatories/')?>' + document_id;
                } else {
                   window.location.href = '<?=base_url('contracts/monitorContractSignature/')?>' + document_id;
                }
            });

            $('.dropdown').on('show.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
            });
            $('.dropdown').on('hide.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
            });

            $('.tip').tooltip();

        });
    }

</script>

