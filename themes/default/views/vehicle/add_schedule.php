<div class="col-lg-12">
    <div class="col-md-5">
        <div class="form-group all">
            <?= lang("hora_saida", "hora_saida") ?>
            <?= form_input('hora_inicio_'. $dia_semana .'[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group all">
            <?= lang("hora_retorno", "hora_retorno") ?>
            <?= form_input('hora_final_'. $dia_semana .'[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-1" style="float: left;margin-top: 5px;text-align: right;">
        <i class="fa fa-trash fa-2x removeItemData_<?=$dia_semana;?>" style="cursor: pointer"></i>
    </div>
</div>