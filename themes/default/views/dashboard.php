<?php
function row_status($x)
{
    if($x == null) {
        return '';
    } else if($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    } else if($x == 'partial' || $x == 'transferring' || $x == 'ordered') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if($x == 'due' || $x == 'returned') {
        return '<div class="text-center"><span class="label label-danger">'.lang($x).'</span></div>';
    } else if ($x == 'cancel') {
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'reembolso'){
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'orcamento') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if ($x == 'lista_espera') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if ($x == 'faturada'){
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    }
}
?>

<?php
$user =  $this->db_model->getUserById($this->session->userdata('user_id'));
?>
<?php if ($Owner || $Admin) { ?>


    <style>
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 20px;
            padding-right: 15px;
            padding-left: 15px;
        }
    </style>
    <div class="row" style="margin-bottom: 15px;">

        <div class="col-sm-12">
            <div id="totalizadore-role" >
                <div class="row">
                    <?php
                    $col_sm_fin = 5;
                    if ($this->Settings->usar_captacao) {
                        $col_sm_fin = 3;?>
                    <div class="col-sm-2">
                        <a href="<?= site_url('captacao'); ?>">
                            <div class="small-box padding1010 bdarkGreen col-sm-12" style="color: #ffffff">
                                <h4 class="bold" style="font-size: 17px;"><?= lang('captacoes') ?></h4>
                                <i class="fa fa-whatsapp"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;font-size: 3rem;"> <?=$count_unread_captacoes?> </div>
                                        <div style="float: right;font-size: 8px;"><?= lang('count_unread_captacoes') ?></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"></div>
                                        <div style="float: right;"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                    <div class="col-sm-<?=$col_sm_fin;?>">
                        <div class="small-box padding1010 bblue col-sm-12" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('recebimentos_mensal') ?></h4>
                            <i class="fa fa-usd"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"> Total de Recebimentos <small>(R$)</small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total a Receber <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalReceberMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total  <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoMes+$totalReceberMes);?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="small-box padding1010 bblue col-sm-12" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('pagamentos_mensal') ?></h4>
                            <i class="fa fa-money"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"> Total de Pagamentos <small>(R$)</small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalPagamentosMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total a Pagar <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalPagarMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total  <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalPagamentosMes+$totalPagarMes);?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="small-box padding1010 bblue col-sm-12" id="divTotalizador" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('total_mensal') ?></h4>
                            <i class="fa fa-pie-chart"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"><small> = </small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoMes-$totalPagamentosMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"><small> = </small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalReceberMes-$totalPagarMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"><small> = </small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney(($totalRecebimentoMes+$totalReceberMes)-($totalPagamentosMes+$totalPagarMes));?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php } ?>

<?php if ($Owner || $Admin) { ?>
    <div class="row" style="display: none;">
        <div class="col-lg-12">
            <div class="alert alert-warning">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <a class="spwHtmlIcoAlerta">&nbsp;</a>
                <span class="spwTextoRef">Atualização do Sistema!</span>
                <p class="spwTextoRefMensagem">
                <p style="margin: 0px;margin-left: 35px;font-size: 14px;" id="novo-room-list">
                    Agora é possível incluir serviços adicionais (opcionais) ao cadastro de viagens.<br/>
                    <br/>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><i class="fa fa-th"></i><span class="break"></span><?= lang('quick_links') ?></h2>
                </div>
                <div class="box-content">

                    <?php if ($valepaySetting->active) {?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                <i class="fa fa-calculator"></i>
                                <p><?= lang('Simular ValePay') ?></p>
                            </a>
                        </div>
                    <?php } ?>

                    <?php if ($configMercadoPago->active) {?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation/mercadopago/?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                <i class="fa fa-calculator"></i>
                                <p><?= lang('Simular MercadoPago') ?></p>
                            </a>
                        </div>
                    <?php } ?>

                    <?php if ($configPagSeguro->active) {?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation/pagseguro/?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                <i class="fa fa-calculator"></i>
                                <p><?= lang('Simular PagSeguro') ?></p>
                            </a>
                        </div>
                    <?php } ?>

                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bred  white quick-button small" href="<?=$this->Settings->url_site_domain.'/estoque/'.$user->biller_id ?>" target="_blank">
                            <i class="fa fa-table"></i>
                            <p><?= lang('abrir_estoque') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="blightBlue white quick-button small" href="<?=$this->Settings->url_site_domain.'/'.$user->biller_id ?>" target="_blank">
                            <i class="fa fa-link"></i>
                            <p><?= lang('abrir_link_reserva') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bblue white quick-button small" href="<?= site_url('products') ?>">
                            <i class="fa fa-barcode"></i>
                            <p><?= lang('products') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bdarkGreen white quick-button small" href="<?= site_url('sales') ?>">
                            <i class="fa fa-heart"></i>
                            <p><?= lang('sales') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bred white quick-button small" href="<?= site_url('agenda/abrir') ?>">
                            <i class="fa fa-calendar"></i>
                            <p><?= lang('agenda') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bpink white quick-button small" href="<?= site_url('contareceber') ?>">
                            <i class="fa fa-money"></i>
                            <p><?= lang('receitas') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="blightOrange white quick-button small" href="<?= site_url('contapagar') ?>">
                            <i class="fa fa-usd"></i>
                            <p><?= lang('despesas') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="bgrey white quick-button small" href="<?= site_url('customers') ?>">
                            <i class="fa fa-users"></i>

                            <p><?= lang('customers') ?></p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-xs-6">
                        <a class="blightBlue white quick-button small" href="<?= site_url('notifications') ?>">
                            <i class="fa fa-comments"></i>

                            <p><?= lang('notifications') ?></p>
                            <!--<span class="notification green">4</span>-->
                        </a>
                    </div>
                    <?php if ($Owner) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6" style="display: none;">
                            <a class="bblue white quick-button small" href="<?= site_url('auth/users') ?>">
                                <i class="fa fa-group"></i>
                                <p><?= lang('users') ?></p>
                            </a>
                        </div>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bblue white quick-button small" href="<?= site_url('system_settings') ?>">
                                <i class="fa fa-cogs"></i>

                                <p><?= lang('settings') ?></p>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><i class="fa fa-th"></i><span class="break"></span><?= lang('quick_links') ?></h2>
                </div>
                <div class="box-content">

                    <?php if ($GP['sales-index']) { ?>

                        <?php if ($valepaySetting->active) {?>
                            <div class="col-lg-1 col-md-2 col-xs-6">
                                <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                    <i class="fa fa-calculator"></i>
                                    <p><?= lang('simular') ?></p>
                                </a>
                            </div>
                        <?php } ?>

                        <?php if ($configMercadoPago->active) {?>
                            <div class="col-lg-1 col-md-2 col-xs-6">
                                <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation/mercadopago/?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                    <i class="fa fa-calculator"></i>
                                    <p><?= lang('Simular MercadoPago') ?></p>
                                </a>
                            </div>
                        <?php } ?>

                        <?php if ($configPagSeguro->active) {?>
                            <div class="col-lg-1 col-md-2 col-xs-6">
                                <a class="bdarkGreen  white quick-button small" href="<?=$this->Settings->url_site_domain.'/simulation/pagseguro/?token='.$this->session->userdata('cnpjempresa') ?>" target="_blank">
                                    <i class="fa fa-calculator"></i>
                                    <p><?= lang('Simular PagSeguro') ?></p>
                                </a>
                            </div>
                        <?php } ?>

                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="blightBlue white quick-button small" href="<?=$this->Settings->url_site_domain.'/estoque/'.$user->biller_id ?>" target="_blank">
                                <i class="fa fa-table"></i>
                                <p><?= lang('abrir_estoque') ?></p>
                            </a>
                        </div>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bred  white quick-button small" href="<?=$this->Settings->url_site_domain.'/'.$user->biller_id ?>" target="_blank">
                                <i class="fa fa-link"></i>

                                <p><?= lang('abrir_link_reserva') ?></p>
                            </a>
                        </div>
                    <?php } ?>

                    <?php if ($GP['products-index']) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bblue white quick-button small" href="<?= site_url('products') ?>">
                                <i class="fa fa-barcode"></i>
                                <p><?= lang('products') ?></p>
                            </a>
                        </div>
                    <?php } if ($GP['sales-index']) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bdarkGreen white quick-button small" href="<?= site_url('sales') ?>">
                                <i class="fa fa-heart"></i>
                                <p><?= lang('sales') ?></p>
                            </a>
                        </div>
                    <?php } if ($GP['quotes-index']) { ?>

                    <?php } if ($GP['purchases-index']) { ?>

                    <?php } if ($GP['transfers-index']) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bpink white quick-button small" href="<?= site_url('contareceber') ?>">
                                <i class="fa fa-money"></i>
                                <p><?= lang('receitas') ?></p>
                            </a>
                        </div>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="blightOrange white quick-button small" href="<?= site_url('contapagar') ?>">
                                <i class="fa fa-usd"></i>
                                <p><?= lang('despesas') ?></p>
                            </a>
                        </div>
                    <?php } if ($GP['customers-index']) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bgrey white quick-button small" href="<?= site_url('customers') ?>">
                                <i class="fa fa-users"></i>
                                <p><?= lang('customers') ?></p>
                            </a>
                        </div>
                    <?php } if ($GP['suppliers-index']) { ?>
                        <div class="col-lg-1 col-md-2 col-xs-6">
                            <a class="bgrey white quick-button small" href="<?= site_url('suppliers') ?>">
                                <i class="fa fa-users"></i>

                                <p><?= lang('suppliers') ?></p>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($Owner || $Admin) { ?>
    <div class="row" style="margin-bottom: 15px;">
        <?php if ($junoDisponivel != null){?>
            <div class="col-lg-2">
                <div class="box">
                    <img style="width: 100%;" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        Disponível<br/><span style="color: #4AB858;font-weight: bold;"> <?php echo $this->sma->formatMoney($junoDisponivel);?></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="box">
                    <img style="width: 100%;" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        A Liberar<br/> <span style="color: #4AB858;font-weight: bold;"> <?php echo $this->sma->formatMoney($junoLiberar);?></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="box">
                    <img style="width: 100%;" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        Total<br/><span style="color: #4AB858;font-weight: bold;"><?php echo $this->sma->formatMoney($junoTotal);?></span>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php }?>

<?php if ($Owner || $Admin) { ?>
    <div class="row" style="margin-bottom: 15px;">
        <?php if ($asaasSetting->active){?>
            <div class="col-lg-2">
                <div class="box" style="text-align: center;">
                    <img width="150px" height="85px" height="85px" src="<?= $assets ?>images/asaas.svg">
                    <div class="box-content">
                        Saldo em conta<br/>
                        <span style="color: #4AB858;font-weight: bold;" id="saldo-asaas">
                            <a onclick="verSaldoAsaas();" style="cursor: pointer;">
                                <i class="fa fa-eye"></i> Clique aqui para ver saldo
                            </a>
                        </span>
                        <span id="webhook-asaas" style="display: none;"></span>
                        <span id="webhook-habilitar-asaas" style="display: none;">
                            <a onclick="habilitar_webhook_asaas();" style="cursor: pointer;">
                               <br/> <i class="fa fa-check-circle"></i> Clique aqui para Habilitar a Integração
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        <?php }?>
        <?php if ($plugsignSetting->active && $plugsignSetting->token){
            $totalDocsAssitura = $this->PlugsignService_model->getTotalDocs(1); ?>
            <div class="col-lg-2">
                <div class="box" style="text-align: center;">
                    <img src="<?= $assets ?>images/plugsign.png" style="width: 42%;">
                    <div class="box-content">
                        <i class="fa fa-check" style="color: green;"></i> Assinatura Digital Ativa <br/>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php }?>

<div class="row" style="margin-bottom: 15px;">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue">
                    <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('painel_vagas'); ?>
                </h2>
                 <div class="box-icon">
                     <select class="combo" id="anoPainelVagas" onchange="buscarPainelDeVagas();">
                         <option value="2020" <?php if($anoFilter == '2020') echo ' selected="selected"';?>>2020</option>
                         <option value="2021" <?php if($anoFilter == '2021') echo ' selected="selected"';?>>2021</option>
                         <option value="2022" <?php if($anoFilter == '2022') echo ' selected="selected"';?>>2022</option>
                         <option value="2023" <?php if($anoFilter == '2023') echo ' selected="selected"';?>>2023</option>
                         <option value="2024" <?php if($anoFilter == '2024') echo ' selected="selected"';?>>2024</option>
                         <option value="2025" <?php if($anoFilter == '2025') echo ' selected="selected"';?>>2025</option>
                         <option value="2026" <?php if($anoFilter == '2026') echo ' selected="selected"';?>>2026</option>
                         <option value="2027" <?php if($anoFilter == '2027') echo ' selected="selected"';?>>2027</option>
                         <option value="2028" <?php if($anoFilter == '2028') echo ' selected="selected"';?>>2028</option>
                         <option value="2029" <?php if($anoFilter == '2029') echo ' selected="selected"';?>>2029</option>
                         <option value="2030" <?php if($anoFilter == '2030') echo ' selected="selected"';?>>2030</option>
                     </select>
                     <select class="combo" id="mesPainelVagas" onchange="buscarPainelDeVagas();">
                         <option value="all" <?php if($mesFilter == 'all') echo ' selected="selected"';?>>Ano Todo</option>
                         <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                         <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                         <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                         <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                         <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                         <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                         <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                         <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                         <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                         <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                         <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                         <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                         <option value="PROXIMAS" <?php if($mesFilter == 'PROXIMAS') echo ' selected="selected"';?>>Próximas Viagens do Ano</option>
                     </select>
                     <select class="combo" id="diaPainelVagas" onchange="buscarPainelDeVagas();">
                         <option value="" <?php if($diaFilter == '') echo ' selected="selected"';?>>Todos</option>
                         <option value="1" <?php if($diaFilter == '1') echo ' selected="selected"';?>>01</option>
                         <option value="2" <?php if($diaFilter == '2') echo ' selected="selected"';?>>02</option>
                         <option value="3" <?php if($diaFilter == '3') echo ' selected="selected"';?>>03</option>
                         <option value="4" <?php if($diaFilter == '4') echo ' selected="selected"';?>>04</option>
                         <option value="5" <?php if($diaFilter == '5') echo ' selected="selected"';?>>05</option>
                         <option value="6" <?php if($diaFilter == '6') echo ' selected="selected"';?>>06</option>
                         <option value="7" <?php if($diaFilter == '7') echo ' selected="selected"';?>>07</option>
                         <option value="8" <?php if($diaFilter == '8') echo ' selected="selected"';?>>08</option>
                         <option value="9" <?php if($diaFilter == '9') echo ' selected="selected"';?>>09</option>
                         <option value="10" <?php if($diaFilter == '10') echo ' selected="selected"';?>>10</option>
                         <option value="11" <?php if($diaFilter == '11') echo ' selected="selected"';?>>11</option>
                         <option value="12" <?php if($diaFilter == '12') echo ' selected="selected"';?>>12</option>
                         <option value="13" <?php if($diaFilter == '13') echo ' selected="selected"';?>>13</option>
                         <option value="14" <?php if($diaFilter == '14') echo ' selected="selected"';?>>14</option>
                         <option value="15" <?php if($diaFilter == '15') echo ' selected="selected"';?>>15</option>
                         <option value="16" <?php if($diaFilter == '16') echo ' selected="selected"';?>>16</option>
                         <option value="17" <?php if($diaFilter == '17') echo ' selected="selected"';?>>17</option>
                         <option value="18" <?php if($diaFilter == '18') echo ' selected="selected"';?>>18</option>
                         <option value="19" <?php if($diaFilter == '19') echo ' selected="selected"';?>>19</option>
                         <option value="20" <?php if($diaFilter == '20') echo ' selected="selected"';?>>20</option>
                         <option value="21" <?php if($diaFilter == '21') echo ' selected="selected"';?>>21</option>
                         <option value="22" <?php if($diaFilter == '22') echo ' selected="selected"';?>>22</option>
                         <option value="23" <?php if($diaFilter == '23') echo ' selected="selected"';?>>23</option>
                         <option value="24" <?php if($diaFilter == '24') echo ' selected="selected"';?>>24</option>
                         <option value="25" <?php if($diaFilter == '25') echo ' selected="selected"';?>>25</option>
                         <option value="26" <?php if($diaFilter == '26') echo ' selected="selected"';?>>26</option>
                         <option value="27" <?php if($diaFilter == '27') echo ' selected="selected"';?>>27</option>
                         <option value="28" <?php if($diaFilter == '28') echo ' selected="selected"';?>>28</option>
                         <option value="29" <?php if($diaFilter == '29') echo ' selected="selected"';?>>29</option>
                         <option value="30" <?php if($diaFilter == '30') echo ' selected="selected"';?>>30</option>
                         <option value="31" <?php if($diaFilter == '31') echo ' selected="selected"';?>>31</option>
                     </select>
                 </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div id="bschart-vagas" style="width:100%; height:450px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#bschart-vagas').highcharts({
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                }
            },
            title: {
                text: 'Calor de Vagas das Viagens'
            },
            xAxis: {
                categories: [
                    <?php
                    foreach ($programacoes as $programacaoEstoque) {

                        $reservadas[] = $programacaoEstoque->getTotalVendasFaturas();
                        $disponivel[] = $programacaoEstoque->getTotalDisponvel();

                        $nomeProduto = str_replace("'", "", $programacaoEstoque->name);

                        echo "'".$nomeProduto.' <br/> '.$this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida())."',";
                    }?>
                ]
            },
            colors: ['#5cb85c', '#f43e61'],
            yAxis: {
                min: 0,
                title: {
                    text: 'Total de Vagas'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: 'gray',
                        fontSize: '12px'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [
                {
                    name: 'Vendido',
                    data: [<?php echo implode(', ', $reservadas);?>]
                },
                {
                    name: 'Disponível',
                    data: [<?php echo implode(', ', $disponivel);?>]
                }
            ]
        });
    });

    function buscarPainelDeVagas() {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>welcome/buscarPainelVagas",
            data: {
                mes: $('#mesPainelVagas').val(),
                ano: $('#anoPainelVagas').val(),
                dia: $('#diaPainelVagas').val(),
            },
            dataType: 'json',
            success: function (programacoes) {

                let produtos = [];
                let faturadas = [];
                let disponivel = [];

                if (programacoes != null) {
                    for (let i=0;i<programacoes.length;i++) {

                        faturadas.push(parseInt(programacoes[i].faturadas));
                        disponivel.push(parseInt(programacoes[i].disponivel));
                        produtos.push(programacoes[i].nome);
                    }
                }

                $('#bschart-vagas').highcharts({
                    chart: {
                        type: 'column',
                        options3d: {
                            enabled: true,
                            alpha: 15,
                            beta: 15,
                            viewDistance: 25,
                            depth: 40
                        }
                    },
                    title: {
                        text: 'Calor de Vagas das Viagens'
                    },
                    xAxis: {
                        categories: produtos
                    },
                    colors: ['#5cb85c', '#f43e61'],
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total de Vagas'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: 'gray',
                                fontSize: '12px'
                            }
                        }
                    },
                    legend: {
                        align: 'right',
                        x: -30,
                        verticalAlign: 'top',
                        y: 25,
                        floating: true,
                        backgroundColor: 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Vendido',
                            data: faturadas
                        },
                        {
                            name: 'Disponível',
                            data: disponivel
                        }
                    ]
                });
            }
        });
    }
</script>

<style type="text/css" media="screen">
    .tooltip-inner {
        max-width: 500px;
    }
</style>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>


<?php if (($Owner || $Admin) && ($relatorioVendasPorDespesas || $relatorioVendasPorHora)) {

    $hours = $relatorioVendasPorHora['hours'];
    $hourlySales = $relatorioVendasPorHora['hourlySales'];

    foreach ($relatorioVendasPorDespesas as $month_sale) {
        $months[]       = date('M-Y', strtotime($month_sale->month));
        $msales[]       = $month_sale->sales;
    }

    foreach ($hourlySales as $year => $months_h) {
        foreach ($months_h as $month => $days) {
            foreach ($days as $day => $hours) {
                foreach ($hours as $hour => $sales) {
                    // Criar a label como "YYYY-MM-DD HH:00"
                    $formattedLabels[] = $year . '/' . str_pad($month, 2, '0', STR_PAD_LEFT) . '/' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' ' . str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00';
                    $formattedSales[] = $sales;
                }
            }
        }
    }

    ?>
    <div class="box" style="margin-bottom: 15px;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('overview_chart'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <p class="introtext"><?php echo lang('overview_chart_heading'); ?></p>

                    <div id="ov-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box" style="margin-bottom: 15px;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('Gráfico de Vendas por Hora'); ?></h2>
            <div class="box-icon">
                <select class="combo" id="anoVendasPorHora" onchange="buscarVendasPorHora();">
                    <option value="2020" <?php if($anoFilter == '2020') echo ' selected="selected"';?>>2020</option>
                    <option value="2021" <?php if($anoFilter == '2021') echo ' selected="selected"';?>>2021</option>
                    <option value="2022" <?php if($anoFilter == '2022') echo ' selected="selected"';?>>2022</option>
                    <option value="2023" <?php if($anoFilter == '2023') echo ' selected="selected"';?>>2023</option>
                    <option value="2024" <?php if($anoFilter == '2024') echo ' selected="selected"';?>>2024</option>
                    <option value="2025" <?php if($anoFilter == '2025') echo ' selected="selected"';?>>2025</option>
                    <option value="2026" <?php if($anoFilter == '2026') echo ' selected="selected"';?>>2026</option>
                    <option value="2027" <?php if($anoFilter == '2027') echo ' selected="selected"';?>>2027</option>
                    <option value="2028" <?php if($anoFilter == '2028') echo ' selected="selected"';?>>2028</option>
                    <option value="2029" <?php if($anoFilter == '2029') echo ' selected="selected"';?>>2029</option>
                    <option value="2030" <?php if($anoFilter == '2030') echo ' selected="selected"';?>>2030</option>
                </select>
                <select class="combo" id="mesVendasPorHora" onchange="buscarVendasPorHora();">
                    <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                    <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                    <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                    <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                    <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                    <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                    <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                    <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                    <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                    <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                    <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                    <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                </select>
                <select class="combo" id="diaVendasPorHora" onchange="buscarVendasPorHora();">
                    <option value="" <?php if($diaFilter == '') echo ' selected="selected"';?>>Todos</option>
                    <option value="1" <?php if($diaFilterRelatorioVendaHora == '1') echo ' selected="selected"';?>>01</option>
                    <option value="2" <?php if($diaFilterRelatorioVendaHora == '2') echo ' selected="selected"';?>>02</option>
                    <option value="3" <?php if($diaFilterRelatorioVendaHora == '3') echo ' selected="selected"';?>>03</option>
                    <option value="4" <?php if($diaFilterRelatorioVendaHora == '4') echo ' selected="selected"';?>>04</option>
                    <option value="5" <?php if($diaFilterRelatorioVendaHora == '5') echo ' selected="selected"';?>>05</option>
                    <option value="6" <?php if($diaFilterRelatorioVendaHora == '6') echo ' selected="selected"';?>>06</option>
                    <option value="7" <?php if($diaFilterRelatorioVendaHora == '7') echo ' selected="selected"';?>>07</option>
                    <option value="8" <?php if($diaFilterRelatorioVendaHora == '8') echo ' selected="selected"';?>>08</option>
                    <option value="9" <?php if($diaFilterRelatorioVendaHora == '9') echo ' selected="selected"';?>>09</option>
                    <option value="10" <?php if($diaFilterRelatorioVendaHora == '10') echo ' selected="selected"';?>>10</option>
                    <option value="11" <?php if($diaFilterRelatorioVendaHora == '11') echo ' selected="selected"';?>>11</option>
                    <option value="12" <?php if($diaFilterRelatorioVendaHora == '12') echo ' selected="selected"';?>>12</option>
                    <option value="13" <?php if($diaFilterRelatorioVendaHora == '13') echo ' selected="selected"';?>>13</option>
                    <option value="14" <?php if($diaFilterRelatorioVendaHora == '14') echo ' selected="selected"';?>>14</option>
                    <option value="15" <?php if($diaFilterRelatorioVendaHora == '15') echo ' selected="selected"';?>>15</option>
                    <option value="16" <?php if($diaFilterRelatorioVendaHora == '16') echo ' selected="selected"';?>>16</option>
                    <option value="17" <?php if($diaFilterRelatorioVendaHora == '17') echo ' selected="selected"';?>>17</option>
                    <option value="18" <?php if($diaFilterRelatorioVendaHora == '18') echo ' selected="selected"';?>>18</option>
                    <option value="19" <?php if($diaFilterRelatorioVendaHora == '19') echo ' selected="selected"';?>>19</option>
                    <option value="20" <?php if($diaFilterRelatorioVendaHora == '20') echo ' selected="selected"';?>>20</option>
                    <option value="21" <?php if($diaFilterRelatorioVendaHora == '21') echo ' selected="selected"';?>>21</option>
                    <option value="22" <?php if($diaFilterRelatorioVendaHora == '22') echo ' selected="selected"';?>>22</option>
                    <option value="23" <?php if($diaFilterRelatorioVendaHora == '23') echo ' selected="selected"';?>>23</option>
                    <option value="24" <?php if($diaFilterRelatorioVendaHora == '24') echo ' selected="selected"';?>>24</option>
                    <option value="25" <?php if($diaFilterRelatorioVendaHora == '25') echo ' selected="selected"';?>>25</option>
                    <option value="26" <?php if($diaFilterRelatorioVendaHora == '26') echo ' selected="selected"';?>>26</option>
                    <option value="27" <?php if($diaFilterRelatorioVendaHora == '27') echo ' selected="selected"';?>>27</option>
                    <option value="28" <?php if($diaFilterRelatorioVendaHora == '28') echo ' selected="selected"';?>>28</option>
                    <option value="29" <?php if($diaFilterRelatorioVendaHora == '29') echo ' selected="selected"';?>>29</option>
                    <option value="30" <?php if($diaFilterRelatorioVendaHora == '30') echo ' selected="selected"';?>>30</option>
                    <option value="31" <?php if($diaFilterRelatorioVendaHora == '31') echo ' selected="selected"';?>>31</option>
                </select>
            </div>
        </div>

        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="vh-chart" style="width:100%; height:450px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="box" style="margin-bottom: 15px;">

        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('Gráfico de Vendas por Semana'); ?></h2>
            <div class="box-icon">
                <select class="combo" id="anoVendasPorSemana" onchange="buscarVendasPorSemana();">
                    <option value="2020" <?php if($anoFilter == '2020') echo ' selected="selected"';?>>2020</option>
                    <option value="2021" <?php if($anoFilter == '2021') echo ' selected="selected"';?>>2021</option>
                    <option value="2022" <?php if($anoFilter == '2022') echo ' selected="selected"';?>>2022</option>
                    <option value="2023" <?php if($anoFilter == '2023') echo ' selected="selected"';?>>2023</option>
                    <option value="2024" <?php if($anoFilter == '2024') echo ' selected="selected"';?>>2024</option>
                    <option value="2025" <?php if($anoFilter == '2025') echo ' selected="selected"';?>>2025</option>
                    <option value="2026" <?php if($anoFilter == '2026') echo ' selected="selected"';?>>2026</option>
                    <option value="2027" <?php if($anoFilter == '2027') echo ' selected="selected"';?>>2027</option>
                    <option value="2028" <?php if($anoFilter == '2028') echo ' selected="selected"';?>>2028</option>
                    <option value="2029" <?php if($anoFilter == '2029') echo ' selected="selected"';?>>2029</option>
                    <option value="2030" <?php if($anoFilter == '2030') echo ' selected="selected"';?>>2030</option>
                </select>
                <select class="combo" id="mesVendasPorSemana" onchange="buscarVendasPorSemana();">
                    <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                    <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                    <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                    <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                    <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                    <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                    <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                    <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                    <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                    <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                    <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                    <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                </select>
                <select class="combo" id="semanaVendasPorSemana" onchange="buscarVendasPorSemana();">
                    <option value="" <?php if($semanaFilterRelatorioVendaHora == '') echo ' selected="selected"';?>>Todos</option>
                    <option value="" <?php if($semanaFilterRelatorioVendaHora == 'at') echo ' selected="selected"';?>>---</option>
                    <option value="1" <?php if($semanaFilterRelatorioVendaHora == '1') echo ' selected="selected"';?>>1° Semana do mês</option>
                    <option value="2" <?php if($semanaFilterRelatorioVendaHora == '2') echo ' selected="selected"';?>>2° Semana do mês</option>
                    <option value="3" <?php if($semanaFilterRelatorioVendaHora == '3') echo ' selected="selected"';?>>3° Semana do mês</option>
                    <option value="4" <?php if($semanaFilterRelatorioVendaHora == '4') echo ' selected="selected"';?>>4° Semana do mês</option>
                    <option value="4" <?php if($semanaFilterRelatorioVendaHora == '5') echo ' selected="selected"';?>>5° Semana do mês</option>
                </select>
            </div>
        </div>

        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="vds-chart" style="width:100%; height:450px;"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="box" style="margin-bottom: 15px;display: none;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('grafico_fluxo_caixa_anual'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="fca-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="row" style="margin-bottom: 15px;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-tasks"></i> <?= lang('latest_five') ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="dbTab" class="nav nav-tabs">
                            <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                                <li class=""><a href="#sales"><?= lang('sales_faturada') ?></a></li>
                                <li class=""><a href="#sales-cotacao"><?= lang('quotes') ?></a></li>
                                <li class=""><a href="#sales-espera"><?= lang('sales_lista_espera') ?></a></li>
                                <!--
                                <li class=""><a href="#cobrancas"><?= lang('ultimas_cobrancas') ?></a></li>
                                <li class=""><a href="#cobrancas-pagar"><?= lang('ultimas_cobrancas_pagas') ?></a></li>
                                <li class=""><a href="#cobrancas-atrasadas"><?= lang('ultimas_cobrancas_atrasadas') ?></a></li>
                                !-->

                            <?php } if ($Owner || $Admin || $GP['quotes-index']) { ?>
                                <!--<li class=""><a href="#quotes"><?= lang('quotes') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['purchases-index']) { ?>
                               <!-- <li class=""><a href="#purchases"><?= lang('purchases') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['transfers-index']) { ?>
                                <!--<li class=""><a href="#transfers"><?= lang('transfers') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['customers-index']) { ?>
                                <li class=""><a href="#customers"><?= lang('customers') ?></a></li>
                            <?php } if ($Owner || $Admin || $GP['suppliers-index']) { ?>
                                <li class=""><a href="#suppliers"><?= lang('suppliers') ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php if ($Owner || $Admin || $GP['sales-index']) { ?>

                                <div id="sales" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesFaturadas)) {
                                                        $r = 1;
                                                        foreach ($salesFaturadas as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sales-cotacao" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesOrcamentos)) {
                                                        $r = 1;
                                                        foreach ($salesOrcamentos as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sales-espera" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesListaEspera)) {
                                                        $r = 1;
                                                        foreach ($salesListaEspera as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } if ($Owner || $Admin || $GP['customers-index']) { ?>
                                <div id="customers" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table id="customers-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("company"); ?></th>
                                                        <th><?= $this->lang->line("name"); ?></th>
                                                        <th><?= $this->lang->line("email"); ?></th>
                                                        <th><?= $this->lang->line("phone"); ?></th>
                                                        <th><?= $this->lang->line("address"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($customers)) {
                                                        $r = 1;
                                                        foreach ($customers as $customer) {
                                                            echo '<tr id="' . $customer->id . '" class="customer_link pointer"><td>' . $r . '</td>
                                                                    <td>' . $customer->company . '</td>
                                                                    <td>' . $customer->name . '</td>
                                                                    <td>' . $customer->email . '</td>
                                                                    <td>' . $customer->phone . '</td>
                                                                    <td>' . $customer->address . '</td>
                                                                </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="6"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } if ($Owner || $Admin || $GP['suppliers-index']) { ?>

                                <div id="suppliers" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table id="suppliers-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("company"); ?></th>
                                                        <th><?= $this->lang->line("name"); ?></th>
                                                        <th><?= $this->lang->line("email"); ?></th>
                                                        <th><?= $this->lang->line("phone"); ?></th>
                                                        <th><?= $this->lang->line("address"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($suppliers)) {
                                                        $r = 1;
                                                        foreach ($suppliers as $supplier) {
                                                            echo '<tr id="' . $supplier->id . '" class="supplier_link pointer"><td>' . $r . '</td>
                                                                    <td>' . $supplier->company . '</td>
                                                                    <td>' . $supplier->name . '</td>
                                                                    <td>' . $supplier->email . '</td>
                                                                    <td>' . $supplier->phone . '</td>
                                                                    <td>' . $supplier->address . '</td>
                                                                </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="6"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.order').click(function () {
            window.location.href = '<?=site_url()?>orders/view/' + $(this).attr('id') + '#comments';
        });
        $('.invoice').click(function () {
            window.location.href = '<?=site_url()?>orders/view/' + $(this).attr('id');
        });
        $('.quote').click(function () {
            window.location.href = '<?=site_url()?>quotes/view/' + $(this).attr('id');
        });
    });
</script>

<?php if (($Owner || $Admin) && ($relatorioVendasPorDespesas || $relatorioVendasPorHora)) { ?>


    <script type="text/javascript">
        $(function () {

            $('#ov-chart').highcharts({
                chart: {},
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($months); ?>},
                yAxis: {min: 0, title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#428BCA'],
                series: [
                    {
                        type: 'areaspline',
                        name: '<?= lang("sales"); ?>',
                        data: [<?php
                            echo implode(', ', $msales);
                            ?>]
                    }
                    ]
            });


            $('#vh-chart').highcharts({
                chart: {
                    type: 'spline' // Gráfico de barras
                },
                credits: { enabled: false },
                title: { text: 'Vendas por Hora' },
                xAxis: {
                    categories: <?= json_encode($formattedLabels); ?>, // Exibindo o ano, mês e hora no formato correto
                    title: { text: 'Hora - Mês - Ano' },
                    labels: {
                        rotation: -45, // Rotacionando para as labels não ficarem sobrepostas
                        style: { fontSize: '10px' }
                    }
                },
                yAxis: {
                    min: 0,
                    title: { text: 'Total de Vendas' },
                    plotLines: [{
                        color: '#FF0000',
                        value: <?= number_format(max($formattedSales) / 2, 2, '.', ''); ?>,  // Linha representando a metade do maior valor de vendas
                        width: 2,
                        zIndex: 5
                    }]
                },
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        return '<b>' + this.x + '</b><br>' + 'Vendas: ' + currencyFormat(this.y);
                    },
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: { fontSize: '14px', padding: '0', color: '#000000' }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: '<?= lang("sales"); ?>',
                    data: <?= json_encode($formattedSales); ?>,
                    //colorByPoint: true, // Variar as cores das barras
                    //colors: ['#428BCA', '#f43e61', '#1C7430', '#FFA500', '#8E44AD', '#D35400', '#16A085'] // Cores variadas
                }]
            });


            $('#vds-chart').highcharts({
                chart: {
                    type: 'column'
                },
                credits: { enabled: false },
                title: { text: 'Vendas por Dia da Semana' },
                xAxis: {
                    categories: <?= json_encode($relatorioVendasPorDiaSemana['labels']); ?>,
                    title: { text: 'Dia da Semana' },
                    labels: {
                        rotation: -45,
                        style: { fontSize: '10px' }
                    }
                },
                yAxis: {
                    min: 0,
                    title: { text: 'Total de Vendas' },
                    plotLines: [{
                        color: '#FF0000',
                        value: <?= number_format(max($relatorioVendasPorDiaSemana['weeklySales']) / 2, 2, '.', ''); ?>,
                        width: 2,
                        zIndex: 5
                    }]
                },
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        return '<b>' + this.x + '</b><br>' + 'Vendas: ' + currencyFormat(this.y);
                    },
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: { fontSize: '14px', padding: '0', color: '#000000' }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: '<?= lang("sales"); ?>',
                    data: <?= json_encode($relatorioVendasPorDiaSemana['weeklySales']); ?>,
                    colorByPoint: true, // Variar as cores das barras
                    colors: ['#428BCA', '#f43e61', '#1C7430', '#FFA500', '#8E44AD', '#D35400', '#16A085'] // Cores variadas
                }]
            });
        });

        function verSaldoAsaas() {
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/saldoAsaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.retorno) {
                        $('#saldo-asaas').html(retorno.saldo);

                        consultar_webhook_cobranca_asaas();
                    }
                }
            });
        }

        function consultar_webhook_cobranca_asaas() {
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/consultar_webhook_cobranca_asaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {

                    if (retorno != null) {
                        $('#webhook-asaas').show();

                        if (retorno.interrupted) {
                            $('#webhook-asaas').html('<br/>INTEGRAÇÃO: DESATIVADO');
                            $('#webhook-asaas').css('color', 'red');
                            $('#webhook-habilitar-asaas').show();
                        } else {
                            $('#webhook-asaas').html('<br/>INTEGRAÇÃO: ATIVO');
                            $('#webhook-asaas').css('color', '#4ab858');
                            $('#webhook-habilitar-asaas').hide();
                        }
                    }
                }
            });
        }

        function habilitar_webhook_asaas() {
            $('#webhook-habilitar-asaas').html('');
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/habilitar_webhook_asaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.errors.length) {
                        for (var i = 0; i < retorno.errors.length; i++) {
                            var errorDescription = retorno.errors[i].description;
                            $('#webhook-habilitar-asaas').append('<br/>' + errorDescription);
                            $('#webhook-habilitar-asaas').css('color', 'red');
                        }
                    } else {
                        location.reload();
                    }
                }
            });
        }

        function buscarVendasPorHora() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>welcome/buscarVendasPorHora",
                data: {
                    mes: $('#mesVendasPorHora').val(),
                    ano: $('#anoVendasPorHora').val(),
                    dia: $('#diaVendasPorHora').val(),
                },
                dataType: 'json',
                success: function (dados) {

                    var chart = $('#vh-chart').highcharts();
                    var horas = dados.labels;
                    var vendas = dados.sales;

                    chart.xAxis[0].setCategories(horas);
                    chart.series[0].setData(vendas);

                    var maxSales = Math.max.apply(null, vendas);
                    var midValue = maxSales / 2;

                    if (chart.yAxis[0].plotLinesAndBands.length > 0) {
                        chart.yAxis[0].removePlotLine(chart.yAxis[0].plotLinesAndBands[0].id);
                    }

                    chart.yAxis[0].addPlotLine({
                        color: '#FF0000',
                        value: midValue,
                        width: 2,
                        zIndex: 5,
                        id: 'half-max-sales-line' // Um id único para a linha de plot
                    });

                    chart.redraw();
                }
            });
        }

        function buscarVendasPorSemana() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>welcome/buscarVendasPorSemana",
                data: {
                    mes: $('#mesVendasPorSemana').val(),
                    ano: $('#anoVendasPorSemana').val(),
                    semana: $('#semanaVendasPorSemana').val(),
                },
                dataType: 'json',
                success: function (dados) {

                    var chart = $('#vds-chart').highcharts();
                    var horas = dados.labels;
                    var vendas = dados.weeklySales;

                    chart.xAxis[0].setCategories(horas);
                    chart.series[0].setData(vendas);

                    var maxSales = Math.max.apply(null, vendas);
                    var midValue = maxSales / 2;

                    if (chart.yAxis[0].plotLinesAndBands.length > 0) {
                        chart.yAxis[0].removePlotLine(chart.yAxis[0].plotLinesAndBands[0].id);
                    }

                    chart.yAxis[0].addPlotLine({
                        color: '#FF0000',
                        value: midValue,
                        width: 2,
                        zIndex: 5,
                        id: 'half-max-sales-line' // Um id único para a linha de plot
                    });

                    chart.redraw();
                }
            });
        }

    </script>

<?php } ?>