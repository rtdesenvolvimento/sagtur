<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Integracaositetrendytravel_model extends CI_Model
{

    private $dbSite;

    public function __construct()
    {
        $this->dbSite = $this->load->database('site', TRUE);

        parent::__construct();
    }

    function getPedidoById($id) {
        $q = $this->dbSite->get('posts');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getAllCupoms($idProduto) {
        $this->dbSite->where('post_parent',$idProduto);
        $q = $this->dbSite->get('posts');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getAllPedidos() {
        $this->dbSite->where('post_type', 'shop_order');
        $q = $this->dbSite->get('posts');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function atualizarStatusPedidoCompleto($id) {
        if ($this->dbSite->update('posts', array('post_status' => 'wc-completed'), array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    function atualizarStatusPedidoAguardando($id) {
        if ($this->dbSite->update('posts', array('post_status' => 'wc-on-hold'), array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    function getInfoPedidoByType($post_id,$type) {
        $q = $this->dbSite->get_where('postmeta', array('post_id' => $post_id, 'meta_key' => $type), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->meta_value;
        }
        return FALSE;
    }

    function getWoocommerceOrderItems($post_id) {
        $q = $this->dbSite->get_where('woocommerce_order_items', array('order_id' => $post_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->order_item_id;
        }
        return FALSE;
    }

    function getWoocommerceOrderItemmetaByType($order_item_id, $type) {
        $q = $this->dbSite->get_where('woocommerce_order_itemmeta', array('order_item_id' => $order_item_id, 'meta_key' => $type), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->meta_value;
        }
        return FALSE;
    }

    function getPassageiro($post_id, $contadorPassageiro=null) {

        if ($contadorPassageiro) {
            if ($contadorPassageiro>1) {
                $contadorPassageiro = '_' . ($contadorPassageiro - 1);
            } else {
                $contadorPassageiro = '';
            }
        } else {
            $contadorPassageiro = '';
        }

        $nameFirst      = $this->getInfoPedidoByType($post_id, '_billing_first_name'.$contadorPassageiro);
        $nameLast       = $this->getInfoPedidoByType($post_id, '_billing_last_name'.$contadorPassageiro);
        $email          = $this->getInfoPedidoByType($post_id, '_billing_email'.$contadorPassageiro);
        $phone          = $this->getInfoPedidoByType($post_id, '_billing_phone'.$contadorPassageiro);
        $rgPassaporte   = $this->getInfoPedidoByType($post_id, '_billing_rg_passaporte'.$contadorPassageiro);
        $tipoDocumento  = $this->getInfoPedidoByType($post_id, '_billing_tipo_documento'.$contadorPassageiro);
        $cpf            = $this->getInfoPedidoByType($post_id, '_billing_cpf'.$contadorPassageiro);
        $sexo           = $this->getInfoPedidoByType($post_id, '_billing_tipo_sexo'.$contadorPassageiro);
        $nascimento     = $this->getInfoPedidoByType($post_id, '_billing_nascimento'.$contadorPassageiro);
        //$city           = $this->getInfoPedidoByType($post_id, '_billing_city');

        $address        = $this->getInfoPedidoByType($post_id, '_billing_rua'.$contadorPassageiro);
        $complemento    = $this->getInfoPedidoByType($post_id, '_billing_address_2'.$contadorPassageiro);
        $numero         = $this->getInfoPedidoByType($post_id, '_billing_numero'.$contadorPassageiro);
        $country        = $this->getInfoPedidoByType($post_id, '_billing_country'.$contadorPassageiro);
        $state          = $this->getInfoPedidoByType($post_id, '_billing_state'.$contadorPassageiro);
        $postal_code    = $this->getInfoPedidoByType($post_id, '_billing_cep'.$contadorPassageiro);
        $cidade         = $this->getInfoPedidoByType($post_id, '_billing_cidade'.$contadorPassageiro);
        $bairro         = $this->getInfoPedidoByType($post_id, '_billing_bairro'.$contadorPassageiro);
        $dtValidadeDoc  = $this->getInfoPedidoByType($post_id, '_billing_dt_validade_doc'.$contadorPassageiro);
        $OrgaoExpedicao = $this->getInfoPedidoByType($post_id, '_billing_orgao_expedicao'.$contadorPassageiro);
        $plano_saude    = $this->getInfoPedidoByType($post_id, '_billing_plano_saude'.$contadorPassageiro);
        $alergia_medicamento    = $this->getInfoPedidoByType($post_id, '_billing_alergia_medicamento'.$contadorPassageiro);
        $doenca_informar        = $this->getInfoPedidoByType($post_id, '_billing_doenca_informar'.$contadorPassageiro);
        $telefone_emergencia    = $this->getInfoPedidoByType($post_id, '_billing_telefone_emergencia'.$contadorPassageiro);

        $name           =  strtoupper($nameFirst.' '.$nameLast);

        if ($sexo == 0) {
            $tipo_sexo = 'MASCULINO';
        } else {
            $tipo_sexo = 'FEMININO';
        }

        if ($tipoDocumento == '0') {
            $tipoDocumento = 'rg';
        } else if ($tipoDocumento == '1') {
            $tipoDocumento = 'passaporte';
        } else if($tipoDocumento == '2') {
            $tipoDocumento = 'rne';
        } else {
            $tipoDocumento = 'nenhum';
        }

        $customers = array(
            'group_id'              => '3',
            'group_name'            => 'customer',
            'customer_group_id'     => 1,
            'customer_group_name'   => 'PASSAGEIROS',
            'name'                  => $name,
            'email'                 => $email,
            'company'               => $name,
            'city'                  => $cidade,
            'phone'                 => $phone,
            'vat_no'                => $cpf,
            'tipo_documento'        => $tipoDocumento,
            'cf1'                   => $rgPassaporte,
            'data_aniversario'      => $nascimento,
            'sexo'                  => $tipo_sexo,
            'cf5'                   => $phone,//whatsApp
            'plano_saude'           => $plano_saude,
            'alergia_medicamento'   => $alergia_medicamento,
            'doenca_informar'       => $doenca_informar,
            'telefone_emergencia'    => $telefone_emergencia,
            'state'                 => $state,
            'postal_code'           => $postal_code,
            'country'               => $country,
            'cf3'                   => $OrgaoExpedicao,
            'validade_rg_passaporte'=> $dtValidadeDoc,
            'address'               => $address.' '.$numero.$complemento.', '.$bairro.' '.$cidade,
        );

        if ($cpf) {
            $customer = $this->getVerificaCustomeByCPF($cpf);
            print_r($customer);
            if (count($customer) > 0 && $cpf) {
                $this->db->update('companies', $customers, array('id' => $customer->id));
                return $customer->id;
            } else {
                $this->db->insert('companies', $customers);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('companies', $customers);
            return $this->db->insert_id();
        }
    }

    public function getVerificaCustomeByCPF($cpf)
    {
        $this->db->where("vat_no",$cpf);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }

    function addNovaViagemSite($idProduto,
                               $quantidadePessoasViagem,
                               $data,
                               $objProducts,
                               $files_original) {

        $name                   = $data['name'];
        $post_name              = str_replace(' ', '_', $name);
        $post_name              = $this->tirarAcentos($post_name);

        $post_id                = $this->addPost($data);

        $price_pacote_seis_onze         = $data['price_pacote_seis_onze'];
        $price_pacote_zero_cinco        = $data['price_pacote_zero_cinco'];
        $price_pacote_acima_sessenta    = $data['price_pacote_acima_sessenta'];

        $promo_price                    = $data['promo_price'];
        $price                          = $data['price'];

        //substitui o valor pela promocao
        if ($promo_price) {
            $price = $promo_price;
        }

        $this->addAllCupomDescontoCrianca($post_name, ($price - $price_pacote_seis_onze), $idProduto, 50);
        $this->addAllCupomDescontoCrianca($post_name, ($price - $price_pacote_acima_sessenta), $idProduto, 150);

        if ($price_pacote_zero_cinco > 0 ) {
            $this->addAllCupomDescontoCrianca($post_name,($price - $price_pacote_zero_cinco), $idProduto, 100);
        } else {
            $this->addAllCupomDescontoCrianca($post_name,$price, $idProduto, 100);
        }

        $this->addProducsSite($post_id, $idProduto);
        $this->addPostmeta($post_id, $idProduto, $quantidadePessoasViagem, $data ) ;
        $this->addTerms($name, $post_name, $post_id, $idProduto);
        $this->addPhoto($post_id, $objProducts, $files_original);
    }

    function addAllCupomDescontoCrianca($post_name, $valor, $idProduto, $desconto) {
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 1, $desconto);//desconto
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 2, $desconto);//desconto
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 3, $desconto);//desconto
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 4, $desconto);//desconto
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 5, $desconto);//desconto
        $this->addCupomDescontoCrianca($post_name,$valor, $idProduto, 6, $desconto);//desconto
    }

    function addCupomDescontoCrianca($post_name,$valor, $idProduto, $qtdCriancas, $desconto) {

        $valorDesconto      = ($valor * $qtdCriancas);

        $nome_desconto  = $post_name.' - DESCONTO '.$desconto.'% PARA '.$qtdCriancas.' CRIANCA(S)';
        $nome_desconto  = str_replace(' ', '_', $nome_desconto);
        $nome_desconto  = $this->tirarAcentos($nome_desconto);

        $item = array(
            'post_author'       => 1,
            'post_content'      => '',
            'post_excerpt'      => $nome_desconto,
            'post_date'         => date('Y-m-d H:m:s'),
            'post_date_gmt'     => date('Y-m-d H:m:s'),
            'post_title'        => strtoupper($nome_desconto),
            'post_status'       => 'publish',
            'comment_status'    => 'open',
            'ping_status'       => 'closed',
            'post_name'         => $nome_desconto,
            'post_modified'     => date('Y-m-d H:m:s'),
            'post_modified_gmt' => date('Y-m-d H:m:s'),
            'post_parent'       => $idProduto,
            'guid'              => 'http://paulovasconcellos.tur.br/?post_type=product&#038;p=4848',
            //'guid'              => 'http://homologacao.phenixvoyages.com.br/?post_type=product&#038;p=4848',
            'menu_order'        => 0,
            'post_type'         => 'shop_coupon',
            'comment_count'     => 0
        );

        $this->dbSite->insert('posts', $item);
        $idCupom = $this->dbSite->insert_id();

        $this->insertPostMeta($idCupom, 'slide_template', 'default');
        $this->insertPostMeta($idCupom, 'customer_email', 'a:0:{}');
        $this->insertPostMeta($idCupom, 'maximum_amount', '');
        $this->insertPostMeta($idCupom, 'minimum_amount', '');
        $this->insertPostMeta($idCupom, 'exclude_sale_items', 'no');
        $this->insertPostMeta($idCupom, 'exclude_product_categories', 'a:0:{}');
        $this->insertPostMeta($idCupom, 'product_categories', 'no');
        $this->insertPostMeta($idCupom, 'free_shipping', 'no');
        $this->insertPostMeta($idCupom, 'expiry_date', '2021-01-31');
        $this->insertPostMeta($idCupom, 'date_expires', '1612058400');
        $this->insertPostMeta($idCupom, 'usage_count', '0');
        $this->insertPostMeta($idCupom, 'limit_usage_to_x_items', '0');
        $this->insertPostMeta($idCupom, 'usage_limit_per_user', '0');
        $this->insertPostMeta($idCupom, 'usage_limit', '0');
        $this->insertPostMeta($idCupom, 'exclude_product_ids', '');
        $this->insertPostMeta($idCupom, 'product_ids', '');
        $this->insertPostMeta($idCupom, 'individual_use', 'no');
        $this->insertPostMeta($idCupom, 'coupon_amount', $valorDesconto);
        $this->insertPostMeta($idCupom, 'discount_type', 'fixed_cart');
        $this->insertPostMeta($idCupom, '_edit_last', '1');
        $this->insertPostMeta($idCupom, '_edit_lock', '1530209424:1');
        $this->insertPostMeta($idCupom, '_dt_enable_builder', '0');
    }

    function tirarAcentos($string){
        $string = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
        $string = str_replace(',', '_', $string);
        $string = str_replace('!', '', $string);
        $string = str_replace('Ç', 'C', $string);
        $string = str_replace('ç', 'c', $string);
        $string = str_replace('/', '_', $string);
        $string = str_replace('°', '_', $string);
        $string = str_replace(':', '_', $string);
        return  $string;
    }

    function editViagemSite($idProduto,
                            $data,
                            $objProducts,
                            $files_original ) {
        
        $product    = $this->site->getProductByID($idProduto);
        $idPost     = $product->products_site_id;
        $idTerm     = $product->term_site_id;
        $name       = $data['name'];
        $post_name  = str_replace(' ', '_', $name);
        $post_name  = $this->tirarAcentos($post_name);

        $price_pacote_seis_onze         = $data['price_pacote_seis_onze'];
        $price_pacote_zero_cinco        = $data['price_pacote_zero_cinco'];
        $price_pacote_acima_sessenta    = $data['price_pacote_acima_sessenta'];

        $promo_price                    =  $data['promo_price'];
        $price                          = $data['price'];

        //substitui o valor da promocao pelo preco
        if ($promo_price) {
            $price = $promo_price;
        }

        $this->editPost($idPost, $data);
        $this->editPostmeta($idPost, $data);
        $this->editTerms($name,$post_name,$idTerm);
        $this->editPhoto($idPost, $objProducts, $files_original);

        $this->deletarAllPostMetaCupom($idProduto);

        $this->addAllCupomDescontoCrianca($post_name, ($price - $price_pacote_seis_onze), $idProduto, 50);
        $this->addAllCupomDescontoCrianca($post_name, ($price - $price_pacote_acima_sessenta), $idProduto, 150);

        if ($price_pacote_zero_cinco > 0) {
            $this->addAllCupomDescontoCrianca($post_name,($price - $price_pacote_zero_cinco), $idProduto, 100);
        } else {
            $this->addAllCupomDescontoCrianca($post_name,$price, $idProduto, 100);
        }
    }

    function deletarAllPostMetaCupom($idProduto) {
        $allCupons = $this->getAllCupoms($idProduto);

        foreach ($allCupons as $cupom) {
            $this->dbSite->delete('postmeta', array('post_id' => $cupom->ID));
            $this->dbSite->delete('posts', array('ID' => $cupom->ID));
        }
    }

    function atualizarEstoque($idProduto, $estoque) {
        $product    = $this->site->getProductByID($idProduto);
        $idPost     = $product->products_site_id;
        $this->editPostmetaEstoque($idPost, $estoque);
    }

    function addPhoto($idPostPai, $objProducts, $files_original) {

        $nomePhoto = $this->publicarFotoWP($objProducts, $files_original);

        $this->publicarGaleriaFotoWP($objProducts,$idPostPai, $files_original);

        $ano = date('Y');
        $mes = date('m');

        if ($nomePhoto) {
            $post_id = $this->addPostImagem($nomePhoto, $idPostPai);
            $this->insertPostMeta($post_id, '_wp_attachment_metadata', $this->getWpAttachmentMetada($ano.'/'.$mes.'/'.$nomePhoto, $nomePhoto));
            $this->insertPostMeta($post_id, '_wp_attached_file', $ano.'/'.$mes.'/'.$nomePhoto);
            $this->insertPostMeta($idPostPai, '_thumbnail_id', $post_id);
        } else {
            $this->insertPostMeta($idPostPai, '_thumbnail_id', 'no');
        }
    }

    function editPhoto($idPostPai, $objProducts, $files_original) {
        $nomePhoto = $this->publicarFotoWP($objProducts, $files_original);

        $this->publicarGaleriaFotoWPEditar($objProducts,$idPostPai, $files_original);

        $ano = date('Y');
        $mes = date('m');

        if ($nomePhoto) {
            $post_id = $this->addPostImagem($nomePhoto, $idPostPai);
            $this->insertPostMeta($post_id, '_wp_attachment_metadata', $this->getWpAttachmentMetada($ano.'/'.$mes.'/'.$nomePhoto, $nomePhoto));
            $this->insertPostMeta($post_id, '_wp_attached_file', $ano.'/'.$mes.'/'.$nomePhoto);
            $this->updatePostMeta($idPostPai, '_thumbnail_id', $post_id);
        }
    }

    function publicarGaleriaFotoWPEditar($objProducts, $idPostPai, $files_original) {

        if ($files_original['userfile']['name'][0] != "") {
            $ano = date('Y');
            $mes = date('m');

            $upload_path = '/home/katatur2/public_html/wp-content/uploads/' . $ano . '/' . $mes . '/';

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0700);
            }

            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = $objProducts->image_types;
            $config['max_size'] = $objProducts->allowed_file_size;
            $config['max_width'] = $objProducts->Settings->iwidth;
            $config['max_height'] = $objProducts->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 100;
            $config['encrypt_name'] = TRUE;
            $files = $files_original;
            $cpt = count($files_original['userfile']['name']);
            $_product_image_gallery = '';

            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                $objProducts->upload->initialize($config);

                if ($objProducts->upload->do_upload('userfile')) {
                    $photo = $objProducts->upload->file_name;
                    $this->publicarAllThumbsWP($objProducts, $upload_path, $photo);

                    if ($photo) {
                        $post_id = $this->addPostImagem($photo, $idPostPai);
                        $this->insertPostMeta($post_id, '_wp_attachment_metadata', $this->getWpAttachmentMetadaGaleria ($ano . '/' . $mes . '/' . $photo, $photo));
                        $this->insertPostMeta($post_id, '_wp_attached_file', $ano . '/' . $mes . '/' . $photo);
                        $_product_image_gallery = $_product_image_gallery . $post_id . ",";
                    }
                }

            }
            $config = NULL;
            //atualizar galeria
            $this->updatePostMeta($idPostPai, '_product_image_gallery', $_product_image_gallery);
        }
    }

    function publicarGaleriaFotoWP($objProducts, $idPostPai, $files_original) {

        $ano            = date('Y');
        $mes            = date('m');

        $upload_path = '/home/katatur2/public_html/wp-content/uploads/' . $ano . '/' . $mes . '/';

        if ( ! file_exists($upload_path)) {
            mkdir($upload_path, 0700);
        }

        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = $objProducts->image_types;
        $config['max_size']         = $objProducts->allowed_file_size;
        $config['max_width']        = $objProducts->Settings->iwidth;
        $config['max_height']       = $objProducts->Settings->iheight;
        $config['overwrite']        = FALSE;
        $config['max_filename']     = 25;
        $config['encrypt_name']     = TRUE;
        $files  = $files_original;
        $cpt    = count($files);
        $_product_image_gallery = '';

        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['userfile']['name']     = $files['userfile']['name'][$i];
            $_FILES['userfile']['type']     = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $files['userfile']['error'][$i];
            $_FILES['userfile']['size']     = $files['userfile']['size'][$i];
            $objProducts->upload->initialize($config);

            if ($objProducts->upload->do_upload('userfile')) {
                $photo = $objProducts->upload->file_name;
                $this->publicarAllThumbsWP($objProducts, $upload_path, $photo);

                if ($photo) {
                    $post_id = $this->addPostImagem($photo, $idPostPai);
                    $this->insertPostMeta($post_id, '_wp_attachment_metadata', $this->getWpAttachmentMetadaGaleria($ano . '/' . $mes . '/' . $photo, $photo));
                    $this->insertPostMeta($post_id, '_wp_attached_file', $ano . '/' . $mes . '/' . $photo);
                    $_product_image_gallery = $_product_image_gallery . $post_id . ",";
                }
            }

        }
        $config = NULL;
        //atualizar galeria
        $this->insertPostMeta($idPostPai, '_product_image_gallery', $_product_image_gallery);
    }

    function getWpAttachmentMetada($nomeImagemOriginal, $nomeOriginalSemPasta=null) {
        $contadorImagemOriginal = strlen($nomeImagemOriginal);
        $nomeImagem = explode  ('.', $nomeOriginalSemPasta);
        $nomeImagem = $nomeImagem[0];
        $contadorImagem = strlen($nomeImagem) + 12;
        return 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:'.$contadorImagemOriginal.':"'.$nomeImagemOriginal.'";s:5:"sizes";a:64:{s:9:"thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"gallery-onecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-940x470.jpg";s:5:"width";i:940;s:6:"height";i:470;s:9:"mime-type";s:10:"image/jpeg";}s:22:"gallery-onecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-685x340.jpg";s:5:"width";i:685;s:6:"height";i:340;s:9:"mime-type";s:10:"image/jpeg";}s:14:"gallery-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-585x400.jpg";s:5:"width";i:585;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:22:"gallery-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x309.jpg";s:5:"width";i:420;s:6:"height";i:309;s:9:"mime-type";s:10:"image/jpeg";}s:26:"gallery-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:16:"gallery-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:24:"gallery-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:28:"gallery-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:15:"gallery-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"gallery-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:27:"gallery-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:11:"blog-onecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-960x732.jpg";s:5:"width";i:960;s:6:"height";i:732;s:9:"mime-type";s:10:"image/jpeg";}s:19:"blog-onecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-670x458.jpg";s:5:"width";i:670;s:6:"height";i:458;s:9:"mime-type";s:10:"image/jpeg";}s:23:"blog-onecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-490x335.jpg";s:5:"width";i:490;s:6:"height";i:335;s:9:"mime-type";s:10:"image/jpeg";}s:11:"blog-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-472x323.jpg";s:5:"width";i:472;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:19:"blog-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"blog-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:13:"blog-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:21:"blog-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:25:"blog-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-510x350.jpg";s:5:"width";i:510;s:6:"height";i:350;s:9:"mime-type";s:10:"image/jpeg";}s:10:"blog-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:18:"blog-thumb-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:22:"blog-thumb-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-510x350.jpg";s:5:"width";i:510;s:6:"height";i:350;s:9:"mime-type";s:10:"image/jpeg";}s:11:"hotel-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x277.jpg";s:5:"width";i:420;s:6:"height";i:277;s:9:"mime-type";s:10:"image/jpeg";}s:19:"hotel-thumb-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x338.jpg";s:5:"width";i:420;s:6:"height";i:338;s:9:"mime-type";s:10:"image/jpeg";}s:23:"hotel-thumb-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x277.jpg";s:5:"width";i:420;s:6:"height";i:277;s:9:"mime-type";s:10:"image/jpeg";}s:13:"places-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-572x418.jpg";s:5:"width";i:572;s:6:"height";i:418;s:9:"mime-type";s:10:"image/jpeg";}s:21:"places-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:25:"places-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:15:"places-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:23:"places-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:27:"places-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:14:"places-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:22:"places-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:26:"places-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:13:"events-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-570x390.jpg";s:5:"width";i:570;s:6:"height";i:390;s:9:"mime-type";s:10:"image/jpeg";}s:21:"events-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:25:"events-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:15:"events-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"events-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:27:"events-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:14:"events-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:22:"events-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:26:"events-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:14:"package-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-569x569.jpg";s:5:"width";i:569;s:6:"height";i:569;s:9:"mime-type";s:10:"image/jpeg";}s:22:"package-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:16:"package-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:24:"package-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:15:"package-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:23:"package-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:10:"best-place";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-572x391.jpg";s:5:"width";i:572;s:6:"height";i:391;s:9:"mime-type";s:10:"image/jpeg";}s:11:"travel-dest";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:12:"tour-package";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:13:"my-post-thumb";a:4:{s:4:"file";s:'.($contadorImagem-1).':"'.$nomeImagem.'-100x80.jpg";s:5:"width";i:100;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:10:"room-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-140x100.jpg";s:5:"width";i:140;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}';
    }

    function getWpAttachmentMetadaGaleria($nomeImagemOriginal, $nomeOriginalSemPasta=null) {
        $contadorImagemOriginal = strlen($nomeImagemOriginal);
        $nomeImagem = explode  ('.', $nomeOriginalSemPasta);
        $nomeImagem = $nomeImagem[0];
        $contadorImagem = strlen($nomeImagem) + 12;
        return 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:'.$contadorImagemOriginal.':"'.$nomeImagemOriginal.'";s:5:"sizes";a:64:{s:9:"thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"gallery-onecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-940x470.jpg";s:5:"width";i:940;s:6:"height";i:470;s:9:"mime-type";s:10:"image/jpeg";}s:22:"gallery-onecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-685x340.jpg";s:5:"width";i:685;s:6:"height";i:340;s:9:"mime-type";s:10:"image/jpeg";}s:14:"gallery-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-585x400.jpg";s:5:"width";i:585;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:22:"gallery-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x309.jpg";s:5:"width";i:420;s:6:"height";i:309;s:9:"mime-type";s:10:"image/jpeg";}s:26:"gallery-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:16:"gallery-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:24:"gallery-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:28:"gallery-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:15:"gallery-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"gallery-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:27:"gallery-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:11:"blog-onecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-960x732.jpg";s:5:"width";i:960;s:6:"height";i:732;s:9:"mime-type";s:10:"image/jpeg";}s:19:"blog-onecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-670x458.jpg";s:5:"width";i:670;s:6:"height";i:458;s:9:"mime-type";s:10:"image/jpeg";}s:23:"blog-onecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-490x335.jpg";s:5:"width";i:490;s:6:"height";i:335;s:9:"mime-type";s:10:"image/jpeg";}s:11:"blog-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-472x323.jpg";s:5:"width";i:472;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:19:"blog-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"blog-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:13:"blog-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:21:"blog-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:25:"blog-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-510x350.jpg";s:5:"width";i:510;s:6:"height";i:350;s:9:"mime-type";s:10:"image/jpeg";}s:10:"blog-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:18:"blog-thumb-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:22:"blog-thumb-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-510x350.jpg";s:5:"width";i:510;s:6:"height";i:350;s:9:"mime-type";s:10:"image/jpeg";}s:11:"hotel-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x277.jpg";s:5:"width";i:420;s:6:"height";i:277;s:9:"mime-type";s:10:"image/jpeg";}s:19:"hotel-thumb-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x338.jpg";s:5:"width";i:420;s:6:"height";i:338;s:9:"mime-type";s:10:"image/jpeg";}s:23:"hotel-thumb-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x277.jpg";s:5:"width";i:420;s:6:"height";i:277;s:9:"mime-type";s:10:"image/jpeg";}s:13:"places-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-572x418.jpg";s:5:"width";i:572;s:6:"height";i:418;s:9:"mime-type";s:10:"image/jpeg";}s:21:"places-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:25:"places-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:15:"places-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:23:"places-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:27:"places-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:14:"places-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:22:"places-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:26:"places-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x307.jpg";s:5:"width";i:420;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:13:"events-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-570x390.jpg";s:5:"width";i:570;s:6:"height";i:390;s:9:"mime-type";s:10:"image/jpeg";}s:21:"events-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:25:"events-twocol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:15:"events-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:23:"events-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:27:"events-threecol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:14:"events-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:22:"events-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:26:"events-fourcol-bothsidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:14:"package-twocol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-569x569.jpg";s:5:"width";i:569;s:6:"height";i:569;s:9:"mime-type";s:10:"image/jpeg";}s:22:"package-twocol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:16:"package-threecol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:24:"package-threecol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:15:"package-fourcol";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:23:"package-fourcol-sidebar";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:10:"best-place";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-572x391.jpg";s:5:"width";i:572;s:6:"height";i:391;s:9:"mime-type";s:10:"image/jpeg";}s:11:"travel-dest";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x287.jpg";s:5:"width";i:420;s:6:"height";i:287;s:9:"mime-type";s:10:"image/jpeg";}s:12:"tour-package";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-420x420.jpg";s:5:"width";i:420;s:6:"height";i:420;s:9:"mime-type";s:10:"image/jpeg";}s:13:"my-post-thumb";a:4:{s:4:"file";s:'.($contadorImagem - 1).':"'.$nomeImagem.'-100x80.jpg";s:5:"width";i:100;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:10:"room-thumb";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-140x100.jpg";s:5:"width";i:140;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:'.$contadorImagem.':"'.$nomeImagem.'-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}';
     }

    function publicarFotoWP($objProducts, $files_original) {

        if ($files_original['product_image']['size'] > 0) {

            $ano = date('Y');
            $mes = date('m');

            $upload_path = '/home/katatur2/public_html/wp-content/uploads/' . $ano . '/' . $mes . '/';

            if ( ! file_exists($upload_path)) {
                mkdir($upload_path, 0700);
            }

            $config['upload_path']      = $upload_path;
            $config['allowed_types']    = $objProducts->image_types;
            $config['max_size']         = $objProducts->allowed_file_size;
            $config['max_width']        = $objProducts->Settings->iwidth;
            $config['max_height']       = $objProducts->Settings->iheight;
            $config['overwrite']        = FALSE;
            $config['max_filename']     = 100;
            $config['encrypt_name']     = TRUE;
            $objProducts->upload->initialize($config);

            if ($objProducts->upload->do_upload('product_image')) {
                $photo = $objProducts->upload->file_name;
                $this->publicarAllThumbsWP($objProducts, $upload_path, $photo);
            }

            $objProducts->image_lib->clear();
            $config = NULL;
            return $photo;
        }
    }

    function publicarAllThumbsWP($objProducts,$upload_path,  $photo) {
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 100, 80);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 100, 100);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 140, 100);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 150, 150);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 300, 300);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 277);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 287);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 307);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 309);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 338);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 420, 420);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 472, 323);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 490, 335);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 510, 350);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 569, 569);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 570, 390);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 572, 391);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 572, 418);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 585, 400);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 600, 600);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 670, 458);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 685, 340);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 768, 768);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 940, 470);
        $this->publicarFotoThumbsWP($objProducts, $upload_path, $photo, 960, 732);
    }

    function publicarFotoThumbsWP($objProducts, $upload_path, $photo, $width, $height) {

        $photo_tumbs    = explode  ('.', $photo);
        $nome_nome      = $photo_tumbs[0].'-'.$width.'x'.$height.'.'.$photo_tumbs[1];

        $objProducts->load->library('image_lib');
        $config['image_library']    = 'gd2';
        $config['source_image']     = $upload_path . $photo;
        $config['new_image']        = $upload_path.$nome_nome;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = $width;
        $config['height']           = $height;
        $objProducts->image_lib->clear();
        $objProducts->image_lib->initialize($config);

        if (!$objProducts->image_lib->resize()) {
            echo $objProducts->image_lib->display_errors();
        }

        if ($objProducts->Settings->watermark) {
            $objProducts->image_lib->clear();
            $wm['source_image'] = $objProducts->upload_path . $photo;
            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $objProducts->Settings->site_name;
            $wm['wm_type'] = 'text';
            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
            $wm['quality'] = '100';
            $wm['wm_font_size'] = '16';
            $wm['wm_font_color'] = '999999';
            $wm['wm_shadow_color'] = 'CCCCCC';
            $wm['wm_vrt_alignment'] = 'top';
            $wm['wm_hor_alignment'] = 'right';
            $wm['wm_padding'] = '10';
            $objProducts->image_lib->initialize($wm);
            $objProducts->image_lib->watermark();
        }

        $extensao = explode('.',$nome_nome);
        rename($upload_path.$nome_nome, $upload_path.$extensao[0].'.jpg');
    }

    function editPost($idPost, $data) {

        $name               = $data['name'];
        $product_details    = $data['product_details'];
        $enviar_site        = $data['enviar_site'];
        $data_saida         = $data['data_saida'];
        $data_retorno       = $data['data_retorno'];
        $destino            = $data['destino'];
        $categorias         = $data['categorias'];
        $tipo_transporte    = $data['tipo_transporte'];
        $apenas_cotacao     = $data['apenas_cotacao'] == '1' ? 'sim' : 'nao';

        $categorias         = $this->getCategorias($categorias);
        $tipo_transporte    = $this->getTipoTransporte($tipo_transporte);

        if ($data_saida) {
            $data_saida = date(('d/m/Y'), strtotime($data_saida));
        }

        if ($data_retorno) {
            $data_retorno = date(('d/m/Y'), strtotime($data_retorno));
        }

        $post_name  = str_replace(' ', '_', $name);
        $post_name  = $this->tirarAcentos($post_name);

        $post_status = 'publish';
        if ($enviar_site == '0') {
            $post_status = 'trash';
        }

        $breve = '<h5>Localização: '.$destino.'</h5>
            <h5>Data da Partida: '.$data_saida.'</h5>
            <h5>Data do Retorno: '.$data_retorno.'</h5>
            <h5>Transporte:  '.$tipo_transporte.'</h5>
            <h5>Categoria: '.$categorias.'</h5>';

        if ($apenas_cotacao == 'sim') {
            $botao_cotacao = '[dt_sc_titled_box type="titled-box" title="Solicite aqui sua cotação"]
                                [contact-form-7 id="6076" title="Cotação de pacotes"]
                           [/dt_sc_titled_box]';
            $breve = $breve.$botao_cotacao;
        }

        $item = array(
            'post_content'      => $product_details,
            'post_title'        => strtoupper($name),
            'post_excerpt'      => $breve,
            'post_status'       => $post_status,
            'post_name'         => $post_name,
            'post_modified'     => date('Y-m-d H:m:s'),
            'post_modified_gmt' => date('Y-m-d H:m:s'),
        );
        $this->dbSite->update('posts', $item, array('ID' => $idPost));
    }

    function addPost($data) {

        $product_details    = $data['product_details'];
        $enviar_site        = $data['enviar_site'];
        $name               = $data['name'];
        $data_saida         = $data['data_saida'];
        $data_retorno       = $data['data_retorno'];
        $destino            = $data['destino'];
        $apenas_cotacao     = $data['apenas_cotacao'];

        $post_name          = str_replace(' ', '_', $name);
        $post_name          = $this->tirarAcentos($post_name);

        if ($data_saida) {
            $data_saida = date(('d/m/Y'), strtotime($data_saida));
        }

        if ($data_retorno) {
            $data_retorno = date(('d/m/Y'), strtotime($data_retorno));
        }

        $post_status = 'publish';
        if ($enviar_site == '0') {
            $post_status = 'trash';
        }

        $categorias         = $data['categorias'];
        $tipo_transporte    = $data['tipo_transporte'];

        $categorias         = $this->getCategorias($categorias);
        $tipo_transporte    = $this->getTipoTransporte($tipo_transporte);

        $breve = '<h5>Localização: '.$destino.'</h5>
            <h5>Data da Partida: '.$data_saida.'</h5>
            <h5>Data do Retorno: '.$data_retorno.'</h5>
            <h5>Transporte:  '.$tipo_transporte.'</h5>
            <h5>Categoria: '.$categorias.'</h5>';

        if ($apenas_cotacao == 'sim') {
            $botao_cotacao = '[dt_sc_titled_box type="titled-box" title="Solicite aqui sua cotação"]
                                [contact-form-7 id="6076" title="Cotação de pacotes"]
                           [/dt_sc_titled_box]';
            $breve = $breve.$botao_cotacao;
        }

        $item = array(
            'post_author'       => 1,
            'post_content'      => $product_details,
            'post_excerpt'      => $breve,
            'post_date'         => date('Y-m-d H:m:s'),
            'post_date_gmt'     => date('Y-m-d H:m:s'),
            'post_title'        => strtoupper($name),
            'post_status'       => $post_status,
            'comment_status'    => 'open',
            'ping_status'       => 'closed',
            'post_name'         => $post_name,
            'post_modified'     => date('Y-m-d H:m:s'),
            'post_modified_gmt' => date('Y-m-d H:m:s'),
            'post_parent'       => 0,
            //'guid'              => 'http://paulovasconcellos.tur.br/?post_type=product&#038;p=4848',
            'guid'              => 'http://wwww.phenixvoyages.com.br/?post_type=product&#038;p=4848',
            'menu_order'        => 0,
            'post_type'         => 'product',
            'comment_count'     => 0,
        );

        $this->dbSite->insert('posts', $item);
        return $this->dbSite->insert_id();
    }

    function getCategorias($categorias) {
        if ($categorias  == 'bate_volta') {
            return lang('Bate Volta');
        } else if ($categorias == 'feriados') {
            return lang('Feriados');
        } else if ($categorias == 'ferias') {
            return lang('Férias');
        } else if ($categorias == 'festivais') {
            return lang('Festivais');
        } else if ($categorias == 'finais_de_semana') {
            return lang('Finais de Semana');
        } else if ($categorias == 'viagens_exclusivas') {
            return lang('Viagem Exclusiva');
        } else {
            return '';
        }
    }

    function getTipoTransporte($tipoTransporte) {
        if ($tipoTransporte == 'sem_aereo') {
            return lang('Sem Aéreo');
        } else if ($tipoTransporte == 'com_aereo') {
            return lang('Com Aéreo');
        } else if ($tipoTransporte == 'rodoviario') {
            return lang('Rodoviário');
        } else if ($tipoTransporte == 'nao_exibir') {
            return lang('nao_exibir');
        } else {
            return '';
        }
    }

    function addPostImagem($nameImagem, $postId) {

        $ano = date('Y');
        $mes = date('m');

        $item = array(
            'post_author'       => 1,
            'post_date'         => date('Y-m-d H:m:s'),
            'post_date_gmt'     => date('Y-m-d H:m:s'),
            'post_title'        => strtoupper($nameImagem),
            'post_status'       => 'inherit',
            'post_parent'       => $postId,
            'comment_status'    => 'open',
            'ping_status'       => 'closed',
            'post_name'         => $nameImagem,
            'post_modified'     => date('Y-m-d H:m:s'),
            'post_modified_gmt' => date('Y-m-d H:m:s'),
            //'guid'              => 'http://paulovasconcellos.tur.br/?post_type=product&#038;p=4848',
            'guid'              => 'http://www.phenixvoyages.com.br/wp-content/uploads/'.$ano.'/'.$mes.'/'.$nameImagem,
            'menu_order'        => 0,
            'post_type'         => 'attachment',
            'post_mime_type'    => 'image/jpeg',
            'comment_count'     => 0,
        );
        $this->dbSite->insert('posts', $item);
        return $this->dbSite->insert_id();
    }

    function addProducsSite($post_id, $idProduto) {
        $product_array = array(
            'products_site_id' => $post_id
        );
        $this->db->update('products', $product_array, array('id' => $idProduto));
    }

    function addTermProducsSite($term_id, $idProduto) {
        $product_array = array(
            'term_site_id' => $term_id
        );
        $this->db->update('products', $product_array, array('id' => $idProduto));
    }

    function addTerms($name, $post_name, $post_id, $idProduto) {
        $terms = array(
            'name'          => $name,
            'slug'          => $post_name,
            'term_group'    => 0
        );
        $this->dbSite->insert('terms', $terms);
        $term_id = $this->dbSite->insert_id();

        $this->addTermProducsSite($term_id, $idProduto);
        $this->addTermmeta($term_id);
        $this->addTerm_taxonomy($term_id, $post_id);
    }

    function editTerms($name, $post_name, $idTerms) {
        $terms = array(
            'name'          => $name,
            'slug'          => $post_name
        );
        $this->dbSite->update('terms', $terms, array('term_id' => $idTerms));
    }

    function addTermmeta($term_id) {
        $termmeta = array(
            'term_id'       => $term_id,
            'meta_key'      => 'product_count_product_cat',
            'meta_value'    => 1
        );
        $this->dbSite->insert('termmeta', $termmeta);

        $termmeta = array(
            'term_id'       => $term_id,
            'meta_key'      => 'order',
            'meta_value'    => 0
        );
        $this->dbSite->insert('termmeta', $termmeta);
    }

    function addTerm_taxonomy($term_id, $post_id) {

        $term_taxonomy = array(
            'term_id'       => $term_id,
            'taxonomy'      => 'product_cat',
            'description'   => '',
            'parent'        => 0,
            'count'         => 1,
        );
        $this->dbSite->insert('term_taxonomy', $term_taxonomy);
        $term_taxonomy_id = $this->dbSite->insert_id();

        $this->addTerm_relationships($term_taxonomy_id, $post_id);
    }

    function addTerm_relationships($term_taxonomy_id, $post_id) {

        $term_relationships = array(
            'object_id'         => $post_id,
            'term_taxonomy_id'  => $term_taxonomy_id,
            'term_order'        => 0
        );
        $this->dbSite->insert('term_relationships', $term_relationships);

        $term_relationships = array(
            'object_id'         => $post_id,
            'term_taxonomy_id'  => 2,
            'term_order'        => 0
        );
        $this->dbSite->insert('term_relationships', $term_relationships);
    }

    function addPostmeta($post_id, $idProduto, $quantidadePessoasViagem, $data ) {

        $alertar_polcas_vagas           = $data['alertar_polcas_vagas'];
        $alertar_ultimas_vagas          = $data['alertar_ultimas_vagas'];
        $percentual_desconto_a_vista    = $data['percentual_desconto_a_vista'];
        $apenas_cotacao                 = $data['apenas_cotacao'];
        $origem                         = $data['origem'];
        $categorias                     = $data['categorias'];
        $tipo_transporte                = $data['tipo_transporte'];
        $simbolo_moeda                  = $data['simbolo_moeda'];
        $unit                           = $data['unit'];

        $valor_pacote                   = $data['valor_pacote'];
        $price                          = $data['price'];
        $promo_price                    = $data['promo_price'];

        //substitui o valor da promocao pelo preco
        if ($promo_price) {
            $price = $promo_price;
        }


        $price_pacote_seis_onze         = $data['price_pacote_seis_onze'];
        $price_pacote_zero_cinco        = $data['price_pacote_zero_cinco'];
        $price_pacote_acima_sessenta    = $data['price_pacote_acima_sessenta'];

        $categorias         = $this->getCategorias($categorias);
        $tipo_transporte    = $this->getTipoTransporte($tipo_transporte);

        $data_saida     = $data['data_saida'];
        $mes_embarque   = date( 'm', strtotime($data_saida) );

        //variaveis_sistema
        $this->insertPostMeta($post_id, 'data_saida',  $data_saida);
        $this->insertPostMeta($post_id, 'data_retorno', $data['data_retorno']);
        $this->insertPostMeta($post_id, 'mes_embarque', $mes_embarque);
        $this->insertPostMeta($post_id, 'simbolo_moeda', $simbolo_moeda);

        $this->insertPostMeta($post_id, 'categoria', $categorias);
        $this->insertPostMeta($post_id, 'tipo_transporte', $tipo_transporte);
        $this->insertPostMeta($post_id, 'apenas_cotacao', $apenas_cotacao);
        $this->insertPostMeta($post_id, 'alertar_polcas_vagas', $alertar_polcas_vagas);
        $this->insertPostMeta($post_id, 'alertar_ultimas_vagas', $alertar_ultimas_vagas);
        $this->insertPostMeta($post_id, 'price_pacote_zero_cinco', $price_pacote_zero_cinco);
        $this->insertPostMeta($post_id, 'price_pacote_seis_onze', $price_pacote_seis_onze);
        $this->insertPostMeta($post_id, 'price_pacote_acima_sessenta', $price_pacote_acima_sessenta);
        $this->insertPostMeta($post_id, 'percentual_desconto_a_vista', $percentual_desconto_a_vista);
        $this->insertPostMeta($post_id, 'origem', $origem);
        $this->insertPostMeta($post_id, 'status_viagem', $unit);

        $this->insertPostMeta($post_id, '_sku', $idProduto);//relacao com o sistema
        $this->insertPostMeta($post_id, '_package_place', $data['destino']);//destinos
        $this->insertPostMeta($post_id, '_stock', $quantidadePessoasViagem);

        if ($valor_pacote) {
            if ($apenas_cotacao == 'sim') {
                $this->updatePostMeta($post_id, '_regular_price', $valor_pacote);
                $this->updatePostMeta($post_id, '_price', $valor_pacote);
            } else {
                $this->updatePostMeta($post_id, '_regular_price', $price);
                $this->updatePostMeta($post_id, '_price', $price);
            }
        } else {
            $this->insertPostMeta($post_id, '_price', $price);
            $this->insertPostMeta($post_id, '_regular_price', $price);
        }

        $this->insertPostMeta($post_id, '_package_days_duration', $data['duracao_pacote']);
        $this->insertPostMeta($post_id, '_package_people',  $data['numero_pessoas']);
        $this->insertPostMeta($post_id, '_product_attributes',  $this->getProductAttributes($data));

        //promocao
        $this->insertPostMeta($post_id, '_sale_price', $promo_price);
        $this->insertPostMeta($post_id, '_sale_price_dates_from', '');
        $this->insertPostMeta($post_id, '_sale_price_dates_to', '');

        $this->insertPostMeta($post_id, '_dt_enable_builder', '0');
        $this->insertPostMeta($post_id, '_wc_review_count', '0');
        $this->insertPostMeta($post_id, '_wc_rating_count', 'a:0:{}');
        $this->insertPostMeta($post_id, '_wc_average_rating', 0);
        $this->insertPostMeta($post_id, '_edit_lock', '1528140538:1');
        $this->insertPostMeta($post_id, '_edit_last', 1);
        $this->insertPostMeta($post_id, 'total_sales', 0);
        $this->insertPostMeta($post_id, '_tax_status', 'taxable');
        $this->insertPostMeta($post_id, '_tax_class', '');
        $this->insertPostMeta($post_id, '_manage_stock', 'yes');
        $this->insertPostMeta($post_id, '_backorders', 'no');
        $this->insertPostMeta($post_id, '_sold_individually', 'no');
        $this->insertPostMeta($post_id, '_weight', '');
        $this->insertPostMeta($post_id, '_length', '');
        $this->insertPostMeta($post_id, '_width', '');
        $this->insertPostMeta($post_id, '_height', '');
        $this->insertPostMeta($post_id, '_upsell_ids', 'a:0:{}');
        $this->insertPostMeta($post_id, '_crosssell_ids', 'a:0:{}');
        $this->insertPostMeta($post_id, '_purchase_note', '');
        $this->insertPostMeta($post_id, '_default_attributes', 'a:0:{}');
        $this->insertPostMeta($post_id, '_virtual', 'no');
        $this->insertPostMeta($post_id, '_downloadable', 'no');
        $this->insertPostMeta($post_id, '_download_limit', '-1');
        $this->insertPostMeta($post_id, '_download_expiry', '-1');
        $this->insertPostMeta($post_id, '_stock_status', 'instock');
        $this->insertPostMeta($post_id, '_product_version', '3.3.5');
        $this->insertPostMeta($post_id, 'slide_template', 'default');
        $this->insertPostMeta($post_id, 'fswp_post_meta', 'a:2:{s:15:"disable_in_cash";s:1:"0";s:20:"disable_installments";s:1:"0";}');
    }

    function getProductAttributes($data) {
        $itinerario         = $data['itinerario'];
        $contadorItinerario = strlen($itinerario);
        $oqueInclui         = $data['oqueInclui'];
        $contadorOqueInclui = strlen($oqueInclui);
        $valores_condicoes  = $data['valores_condicoes'];
        $contadorCondicoes  = strlen($valores_condicoes);
        return 'a:3:{s:10:"itinerario";a:6:{s:4:"name";s:11:"Itinerário";s:5:"value";s:'.$contadorItinerario.':"'.$itinerario.'";s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}s:12:"o-que-inclui";a:6:{s:4:"name";s:13:"O que Inclui?";s:5:"value";s:'.$contadorOqueInclui.':"'.$oqueInclui.'";s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}s:19:"valores-e-condicoes";a:6:{s:4:"name";s:21:"Valores e Condições";s:5:"value";s:'.$contadorCondicoes.':"'.$valores_condicoes.'";s:8:"position";i:2;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}}';
    }

    function editPostmeta($post_id, $data) {

        $alertar_polcas_vagas           = $data['alertar_polcas_vagas'];
        $alertar_ultimas_vagas          = $data['alertar_ultimas_vagas'];
        $price_pacote_zero_cinco        = $data['price_pacote_zero_cinco'];
        $price_pacote_seis_onze         = $data['price_pacote_seis_onze'];
        $price_pacote_acima_sessenta    = $data['price_pacote_acima_sessenta'];
        $percentual_desconto_a_vista    = $data['percentual_desconto_a_vista'];
        $apenas_cotacao                 = $data['apenas_cotacao'];
        $categorias                     = $data['categorias'];
        $tipo_transporte                = $data['tipo_transporte'];
        $origem                         = $data['origem'];
        $simbolo_moeda                  = $data['simbolo_moeda'];
        $unit                           = $data['unit'];

        $valor_pacote                   = $data['valor_pacote'];
        $price                          = $data['price'];

        //tratamento
        $categorias                     = $this->getCategorias($categorias);
        $tipo_transporte                = $this->getTipoTransporte($tipo_transporte);

        $this->updatePostMeta($post_id, 'categoria', $categorias);
        $this->updatePostMeta($post_id, 'tipo_transporte', $tipo_transporte);
        $this->updatePostMeta($post_id, 'alertar_polcas_vagas', $alertar_polcas_vagas);
        $this->updatePostMeta($post_id, 'alertar_ultimas_vagas', $alertar_ultimas_vagas);
        $this->updatePostMeta($post_id, 'price_pacote_zero_cinco', $price_pacote_zero_cinco);
        $this->updatePostMeta($post_id, 'price_pacote_seis_onze', $price_pacote_seis_onze);
        $this->updatePostMeta($post_id, 'price_pacote_acima_sessenta', $price_pacote_acima_sessenta);
        $this->updatePostMeta($post_id, 'percentual_desconto_a_vista', $percentual_desconto_a_vista);
        $this->updatePostMeta($post_id, 'apenas_cotacao', $apenas_cotacao);
        $this->updatePostMeta($post_id, 'origem', $origem);
        $this->updatePostMeta($post_id, 'simbolo_moeda', $simbolo_moeda);
        $this->updatePostMeta($post_id, 'status_viagem', $unit);

        $data_saida         =  $data['data_saida'];
        $mes_embarque       = date( 'm', strtotime($data_saida) );

        //datas
        $this->updatePostMeta($post_id, 'data_saida', $data_saida);
        $this->updatePostMeta($post_id, 'data_retorno', $data['data_retorno']);
        $this->updatePostMeta($post_id, 'mes_embarque', $mes_embarque);

        //informacoes do pacote
        $this->updatePostMeta($post_id, '_package_place', $data['destino']);
        $this->updatePostMeta($post_id, '_package_days_duration', $data['duracao_pacote']);
        $this->updatePostMeta($post_id, '_package_people', $data['numero_pessoas']);

        //preco
        if ($valor_pacote) {

            if ($apenas_cotacao == 'sim') {
                $this->updatePostMeta($post_id, '_regular_price', $valor_pacote);
                $this->updatePostMeta($post_id, '_price', $valor_pacote);
            } else {
                $this->updatePostMeta($post_id, '_regular_price', $price);
                $this->updatePostMeta($post_id, '_price', $price);
            }
        } else {
            $this->updatePostMeta($post_id, '_regular_price', $price);
            $this->updatePostMeta($post_id, '_price', $price);
        }

        //promocao
        $this->updatePostMeta($post_id, '_sale_price', $data['promo_price']);
        $this->updatePostMeta($post_id, '_sale_price_dates_from', '');
        $this->updatePostMeta($post_id, '_sale_price_dates_to', '');

        //atributos
        $this->updatePostMeta($post_id, '_product_attributes', $this->getProductAttributes($data));
    }

    function editPostmetaEstoque($post_id, $estoque) {
        if ($estoque <= 0) {
            $this->updatePostMeta($post_id, '_stock_status', 'outofstock');
            $this->updatePostMeta($post_id, '_stock', 0);
        } else {
            $this->updatePostMeta($post_id, '_stock', $estoque);
            $this->updatePostMeta($post_id, '_stock_status', 'instock');
        }
    }

    function insertPostMeta($post_id, $meta_key, $meta_value) {
        $data = array(
            'post_id'       => $post_id,
            'meta_key'      => $meta_key,
            'meta_value'    => $meta_value
        );
        $this->dbSite->insert('postmeta', $data);
    }

    function updatePostMeta($post_id, $meta_key, $meta_value) {
        $this->dbSite->delete('postmeta', $data = array('post_id'=> $post_id, 'meta_key'=> $meta_key) );
        $this->insertPostMeta($post_id, $meta_key, $meta_value);
    }

}
