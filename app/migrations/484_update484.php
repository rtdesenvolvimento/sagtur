<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update484 extends CI_Migration {

    public function up() {
        $this->db->update('shop_settings',  array('number_packages_per_line' => 4), array('id' => 1));
        $this->db->update('settings',  array('version' => '4.8.4'), array('setting_id' => 1));
    }

    public function down() {}
}
