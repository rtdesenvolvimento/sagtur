<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update432 extends CI_Migration {

    public function up() {

        $this->alter_table_gift_cards();
        $this->alter_table_tipo_cobranca();
        $this->alter_table_settings();
        $this->alter_table_conta_pagar();
        $this->alter_table_sales();
        $this->alter_table_payments();

        $this->insert_dados_para_controle_credito();
        $this->insert_despesa();

        $this->db->update('settings',  array('version' => '4.3.2'), array('setting_id' => 1));
    }

    public function alter_table_settings()
    {
        $fields = array(
            'tipo_cobranca_credito_cliente_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_conta_pagar()
    {
        $fields = array(
            'gift_cards_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('conta_pagar', $fields);
    }

    public function alter_table_payments()
    {
        $fields = array(
            'origin_payment_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('payments', $fields);
    }

    public function alter_table_sales()
    {
        $fields = array(
            'credited_sale' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'refunded_sale' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_gift_cards() {
        $fields = array(
            'origin_sale_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' , 'default' => 'due'),//todo status padrao pagar
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('gift_cards', $fields);
    }

    public function alter_table_tipo_cobranca()
    {
        $fields = array(
            'credito_cliente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'reembolso' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('tipo_cobranca', $fields);
    }

    public function insert_dados_para_controle_credito()
    {
        $conta_credito = array(
            'user_id'       => 1,
            'name'          => 'CAIXA - CRÉDITO DE CLIENTES',
            'status'        => 'open',
            'date'          => date('Y-m-d H:i:s'),
            'cash_in_hand'  => 0,
            'caixa'         => 0,
        );
        $this->db->insert('pos_register', $conta_credito);
        $conta_credito_id = $this->db->insert_id();


        $forma_pagamento_credito = array(
            'name'          => 'CRÉDITO DE CLIENTE',
            'status'        => 'Ativo',
        );
        $this->db->insert('forma_pagamento', $forma_pagamento_credito);
        $forma_pagmento_id = $this->db->insert_id();

        $tipo_cobranca = array(
            'name' => 'CRÉDITO DE CLIENTE',
            'status' => 'Ativo',
            'note' => '',
            'tipo' => 'nenhuma',
            'tipoExibir' => 'despesa',
            'integracao' => 'nenhuma',
            'faturar_automatico' => 'sim',
            'formapagamento' => $forma_pagmento_id,
            'conta' => $conta_credito_id,
            'faturarVenda' => 1,
            'automatic_cancellation_sale' => 0,
            'numero_dias_cancelamento' => 0,
            'exibirLinkCompras' => TRUE,
            'credito_cliente' => 1,
        );

        $this->db->insert('tipo_cobranca', $tipo_cobranca);
        $tipo_cobranca_id = $this->db->insert_id();

        $this->db->update('settings',  array('tipo_cobranca_credito_cliente_id' => $tipo_cobranca_id), array('setting_id' => 1));

        $this->db->update('motivo_cancelamento',  array('gerar_credito' => 1));
    }

    public function insert_despesa() {
        $data_despesa = array(
            'name' => 'CRÉDITO DE CLIENTE',
            'code' => 'CRÉDITO DE CLIENTE',
            'tipo' => 'subgrupo',
            'despesasuperior' => 18,
        );

        $this->db->insert('despesa', $data_despesa);
        $despesa_id = $this->db->insert_id();

        $fields = array(
            'despesa_padrao_credito_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('despesa_padrao_credito_id' => $despesa_id), array('setting_id' => 1));

    }

    public function down() {}
}
