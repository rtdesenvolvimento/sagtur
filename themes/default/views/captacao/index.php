<style type="text/css" media="screen">
    #captacoesTable td:nth-child(1) {display: none;}

    #captacoesTable td:nth-child(2) {width: 12%;text-align: center;}

    #captacoesTable td:nth-child(6) {width: 5%;text-align: center;}

    #captacoesTable td:nth-child(8) {width: 1%;text-align: center;}
    #captacoesTable td:nth-child(9) {width: 10%;text-align: center;}
    #captacoesTable td:nth-child(11) {width: 5%;text-align: center;}
    #captacoesTable td:nth-child(13) {width: 5%;text-align: center;}
    #captacoesTable td:nth-child(14) {width: 5%;text-align: center;}


    .badge {
        font-size: 11px;
        font-weight: 700;
        color: #404040;
        border: 1px solid #d9d9d9;
        margin: 0 2px;
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        line-height: 1;
        vertical-align: middle;
        white-space: nowrap;
        text-align: center;
        background-color: #FDFDFD;
        border-radius: 3px;
    }

    .badge_novo {
        border-color: #fff;
        background-color: #ffdc77;
        font-size: 11px;
        font-weight: 700;
        color: #404040;
        margin: 0 2px;
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        line-height: 1;
        vertical-align: middle;
        white-space: nowrap;
        text-align: center;
        border-radius: 3px;
    }

    .badge_lido {
        border-color: #fff;
        background-color: #daf196;
        font-size: 11px;
        font-weight: 700;
        color: #404040;
        margin: 0 2px;
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        line-height: 1;
        vertical-align: middle;
        white-space: nowrap;
        text-align: center;
        border-radius: 3px;
    }

    .badge_n_lido {
        border-color: #fff;
        background-color: #f53a3669;
        font-size: 11px;
        font-weight: 700;
        color: #404040;
        margin: 0 2px;
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        line-height: 1;
        vertical-align: middle;
        white-space: nowrap;
        text-align: center;
        border-radius: 3px;
    }


</style>

<script>
    $(document).ready(function () {

        $('#billerFilter').change(function(event){
            $('#captacoesTable').DataTable().fnClearTable()
        });

        $('#filterRead').change(function(event){
            $('#captacoesTable').DataTable().fnClearTable()
        });

        $('#filteInteresseCaptacao').change(function(event){
            $('#captacoesTable').DataTable().fnClearTable()
        });

        function st_origem(x) {
            return '<div class="text-center"><span class="label badge">'+x+'</span></div>'
        }

        function st_status(x) {
            if (x === 'ABERTO') {
                return '<div class="text-center"><span class="label badge_novo">'+x+'</span></div>'
            }
        }

        function st_lido(x) {
            if (x === '0') {
                return '<div class="text-center"><span class="label badge_n_lido"><i class="fa fa-envelope"></i></span></div>'
            } else {
                return '<div class="text-center"><span class="label badge_lido"><i class="fa fa-eye"></i></span></div>'
            }
        }

        $('#captacoesTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('captacao/getCaptacoes') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                aoData.push({ "name": "filterRead", "value":  $('#filterRead').val() });
                aoData.push({ "name": "filteInteresseCaptacao", "value":  $('#filteInteresseCaptacao').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                {"mRender": st_origem},
                {"mRender": st_origem},
                null,
                {"bSortable": false, "mRender": st_status},
                null,
                {"mRender": st_lido},
                {"bSortable": false}
            ]
        });
    });
</script>
<?= form_open('captacao/expense_captacoes_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-leaf"></i><?= lang('captacoes'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-lg-6">
                <?= lang("biller", "billerFilter"); ?>
                <?php
                $bl[""] = lang("select") . ' ' . lang("biller") ;
                foreach ($billers as $biller) {
                    $bl[$biller->id] = $biller->name;
                }
                echo form_dropdown('billerFilter', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
            </div>
            <div class="col-lg-4">
                <?= lang("interesse_captacao", "interesse_captacao"); ?>
                <?php
                $cbInteresse[""] = lang("select") . ' ' . lang("interesse_captacao") ;
                foreach ($interesses as $interesse) {
                    $cbInteresse[$interesse->id] = $interesse->name;
                }
                echo form_dropdown('filteInteresseCaptacao', $cbInteresse, '', 'id="filteInteresseCaptacao" name="filteInteresseCaptacao" data-placeholder="' . lang("select") . ' ' . lang("interesse_captacao") . '" class="form-control input-tip select" style="width:100%;"'); ?>
            </div>
            <div class="col-lg-2">
                <?= lang("read", "read") ?>
                <?php
                $opts = array(
                    1 => lang('yes'),
                    0 => lang('no')
                );
                echo form_dropdown('filterRead', $opts, 0 , 'class="form-control" id="filterRead"');
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="captacoesTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= $this->lang->line("date"); ?></th>
                            <th><?= $this->lang->line("reference_no"); ?></th>
                            <th><?= $this->lang->line("name"); ?></th>
                            <th><?= $this->lang->line("telefone"); ?></th>
                            <th><?= $this->lang->line("motivo"); ?></th>
                            <th><?= $this->lang->line("product_interest"); ?></th>
                            <th><?= $this->lang->line("origem"); ?></th>
                            <th><?= $this->lang->line("forma_atendimento"); ?></th>
                            <th><?= $this->lang->line("biller"); ?></th>
                            <th><?= $this->lang->line("captacao_status"); ?></th>
                            <th><?= $this->lang->line("department"); ?></th>
                            <th><?= $this->lang->line("read"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="13" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th><?= $this->lang->line("date"); ?></th>
                            <th><?= $this->lang->line("reference_no"); ?></th>
                            <th><?= $this->lang->line("name"); ?></th>
                            <th><?= $this->lang->line("telefone"); ?></th>
                            <th><?= $this->lang->line("motivo"); ?></th>
                            <th><?= $this->lang->line("product_interest"); ?></th>
                            <th><?= $this->lang->line("forma_atendimento"); ?></th>
                            <th><?= $this->lang->line("origem"); ?></th>
                            <th><?= $this->lang->line("biller"); ?></th>
                            <th><?= $this->lang->line("captacao_status"); ?></th>
                            <th><?= $this->lang->line("department"); ?></th>
                            <th><?= $this->lang->line("read"); ?></th>
                            <th style="width:100px;text-align: center;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {
        $(document).on('click', '.cp-read', function(e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({type: "get", url: link,
                success: function(data) {
                    if (data === 'Captacao Lida Com Sucesso!') {
                        addAlert(data, 'success');
                    } else {
                        addAlert(data, 'danger');
                    }

                    $('#captacoesTable').DataTable().fnClearTable()
                },
                error: function(data) { addAlert('Failed', 'danger'); }
            });
            return false;
        });
    });
</script>

