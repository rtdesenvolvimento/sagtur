<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');



$lang['checkin'] = 'Check-in';
$lang['entry']  = 'Entrada';
$lang['info_checkin'] = 'Informações do Check-in';
$lang['fazer_checkin'] = 'Fazer check-in';
$lang['situation'] = 'Situação';
$lang['entrance'] = 'Fazer Check-in';
$lang['tipo_transporte'] = "Tipo de transporte";
$lang['local_embarque'] = 'Local de Embarque';

$lang['vendas'] = 'Vendas';
$lang['confirmados'] = 'Confirmados';
$lang['pendentes'] = 'Pendentes';
$lang['data_saida'] = 'Data Saída';
$lang['ano'] = 'Ano';
$lang['mes'] = 'Mês';
$lang['end_date'] = 'Até';
$lang['produto_ativo'] = "Pacotes Ativo";
$lang['produto_inativo'] = "Pacotes Inativo";
$lang['arquivado']  = "Arquivados";
