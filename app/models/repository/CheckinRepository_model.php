<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CheckinRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id)
    {
        $q = $this->db->get_where('checkin', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function checked($sale_id, $customer_id)
    {
        $q = $this->db->get_where('checkin', array('sale_id' => $sale_id, 'customer_id' => $customer_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItem($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInvoiceByID($id)
    {
        $q = $this->db->get_where('sales', array('sales.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllInvoiceItems($sale_id)
    {
        $this->db->select('sale_items.*, companies.name as customer, companies.telefone_emergencia, companies.social_name, companies.vat_no, companies.cf1, companies.cf3, companies.cf5, companies.tipo_documento, companies.doenca_informar, companies.data_aniversario')
            ->join('companies', 'companies.id=sale_items.customerClient', 'left')
            ->order_by('sale_items.customerClientName');

        $this->db->where('adicional', false);

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProducts()
    {
        $this->db->where('viagem', 1);
        $this->db->where('unit', 'Confirmado');

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}