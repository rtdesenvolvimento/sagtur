<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update357 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.5.7'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'site_width' => array('type' => 'INT', 'constraint' => 1 , 'default' => 350),
            'site_height' => array('type' => 'INT', 'constraint' => 1 , 'default' => 350),
            'quantity_thumbs' => array('type' => 'INT', 'constraint' => 1 , 'default' => 5),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
