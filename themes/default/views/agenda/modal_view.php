<?php
function row_status($x)
{
    if($x == null) {
        return '';
    } else if($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    } else if($x == 'partial' || $x == 'transferring' || $x == 'ordered') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if($x == 'due' || $x == 'returned') {
        return '<div class="text-center"><span class="label label-danger">'.lang($x).'</span></div>';
    } else if ($x == 'cancel') {
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'reembolso'){
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'orcamento') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if ($x == 'lista_espera') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if ($x == 'faturada'){
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    }
}
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;"
                    onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>

            <?php
            $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
            $nomeViagem = strtoupper($product->name).' '.$data;
            ?>
            <h4 class="modal-title" id="myModalLabel"><?= $nomeViagem; ?> </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive" id="divImprimir">
                        <table id="tbFinanceiroGeralNivel2"
                               style="cursor: pointer;"
                               class="table table-bordered table-hover table-striped table-condensed reports-table">
                            <thead>
                            <tr>
                                <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                                <th style="text-align: center;width: 1%;"><?= lang("reference_no"); ?></th>
                                <th style="text-align: left;width: 80%;"><?= lang("customer"); ?></th>
                                <th style="text-align: center;width: 2%;"><?= lang("poltrona"); ?></th>
                                <th style="text-align: center;width: 2%;"><?= lang("status"); ?></th>
                                <th style="text-align: center;"><?= lang("payment_status"); ?></th>
                                <th style="text-align: center;"><?= lang("actions"); ?></th>
                            </tr>
                            </thead>
                            <thead>
                            </thead>
                            <tbody>
                            <?php

                            $contadoColunas = 0;
                            $totalValor = 0;

                            foreach ($sales as $sale) {

                                $contadoColunas = $contadoColunas + (int) $sale->quantity;

                                $cliente = $this->site->getCompanyByID($sale->customerClient);

                                list($ano, $mes, $dia) = explode('-', $cliente->data_aniversario);

                                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                                $faturas = $this->site->getParcelasFaturaByContaReceber($sale->id);
                                $fatura = null;

                                $subTotal += $sale->subtotal;
                                foreach ($faturas as $faturaObj) $fatura = $faturaObj;
                                ?>
                                <tr class="invoice_link" id="<?php echo $sale->id;?>">
                                    <td style="text-align: center;"><?php echo $contadoColunas;?></td>
                                    <td style="text-align: center;"><?php echo $sale->reference_no;?></td>
                                    <td style="text-align: left;<?php if (!$sale->descontarVaga) echo 'color: #0044cc;font-weight: 500;';?>"><?php echo $cliente->name.'<small> idade '.$idade.' ['.lang($sale->faixaNome).']'.' '. (int) $sale->quantity.'un</small>';?></td>
                                    <td style="text-align: center;"><?php echo $sale->poltronaClient;?></td>
                                    <td style="text-align: center;" ><?php echo row_status($sale->sale_status) ;?></td>
                                    <td style="text-align: center;"><?php echo row_status($sale->payment_status) ;?></td>
                                    <td class="acoes">
                                        <div class="text-center">
                                            <div class="btn-group text-left">
                                                <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li><a href="<?php echo base_url();?>sales/view/<?php echo $sale->id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Passagem</a></li>
                                                    <li><a href="<?php echo base_url();?>sales/emitir_contrato/<?php echo $sale->id;?>"><i class="fa fa-book"></i> Baixar Contrato</a></li>
                                                    <?php if ($fatura != null){?>
                                                        <li><a href="<?php echo base_url();?>financeiro/historico/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i><?php echo lang('Ver Parcelas / Adicionar Pagamento')?></a></li>
                                                    <?php }?>
                                                    <li><a href="<?php echo base_url();?>sales/edit/<?php echo $sale->id;?>" class="sledit" target="_blank"><i class="fa fa-edit"></i> Editar Venda</a></li>
                                                    <li><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $sale->id;?>"><i class="fa fa-file-pdf-o"></i> Baixar Voucher</a></li>
                                                    <li><a href="<?php echo base_url();?>sales/email/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal2"><i class="fa fa-envelope"></i> E-mail de Passagem</a></li>
                                                    <li class="divider"></li>
                                                    <li>

                                                        <?php
                                                        $phone = str_replace('(', '', str_replace(')', '', $cliente->cf5));
                                                        $phone = str_replace('-', '', $phone);
                                                        $phone = str_replace(' ', '', $phone);
                                                        ?>
                                                        <a href="https://api.whatsapp.com/send?phone=55<?php echo $phone;?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('whatsapp_send')?></a>
                                                    </li>
                                                    <li><a href="<?php echo base_url().'appcompra/whatsapp/'.$sale->id.'?token='.$this->session->userdata('cnpjempresa');?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('compartilhar_dados_venda_whatsapp');?></a></li>
                                                    <li><a href="<?php echo base_url().'appcompra/whatsapp_pdf_atualizado/'.$sale->id.'?token='.$this->session->userdata('cnpjempresa');?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('compartilhar_voucher_whatsapp');?></a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo base_url();?>customers/edit/<?php echo $sale->customerClient;?>" data-toggle="modal" data-target="#myModal2"><i class="fa fa-edit"></i> Visualizar Dados Passageiro</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="1" style="background: #0044cc;">
                                    <div></div>
                                </td>
                                <td colspan="6">
                                    Nome Destacados Nesta Cor Não Descontam Vagas.
                                </td>
                            </tr>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12">
                    <table id="tbDatas" class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;margin-bottom: 30px;">
                        <thead>
                        <tr><th colspan="6" style="text-align: center;color: #ffffff;font-weight: bold;">DADOS DAS VENDAS</th></tr>
                        </thead>
                        <thead>
                        <tr class="active">
                            <th class="col-md-8" style="text-align: left;"><?= lang('tipo') ?></th>
                            <th class="col-md-1" style="text-align: center;"><?= lang('cotacoes') ?></th>
                            <th class="col-md-1" style="text-align: center;"><?= lang('vendas') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalOrcamentoContaVagas = 0;
                        $totalFaturasContaVagas = 0;

                        $totalOrcamentoNaoContaVagas = 0;
                        $totalFaturasNaoContaVagas = 0;

                        foreach ($valorFaixas as $valoresFaixa) {
                            $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaByFaixaId($product->id, $valoresFaixa->id);
                            if ($faixa) {?>
                            <?php
                                $ativo = $faixa->status == 'ATIVO' ? true  : false; ?>
                                <?php if ($ativo){

                                    $itemFatura = $this->site->getItensByFaixaValores($product->id, $programacao->id,  $valoresFaixa->id, 'faturada');
                                    $itemOrcamento = $this->site->getItensByFaixaValores($product->id, $programacao->id,  $valoresFaixa->id, 'orcamento');

                                    $qtdOrcamento = $itemOrcamento->qtd ? $itemOrcamento->qtd : 0;
                                    $qtdFaturas = $itemFatura->qtd ? $itemFatura->qtd : 0;

                                    if ($faixa->descontarVaga) {
                                        $totalOrcamentoContaVagas += $qtdOrcamento;
                                        $totalFaturasContaVagas += $qtdFaturas;
                                    } else {
                                        $totalOrcamentoNaoContaVagas += $qtdOrcamento;
                                        $totalFaturasNaoContaVagas += $qtdFaturas;
                                    }
                                    
                                    ?>
                                    <tr>
                                        <?php if ($faixa->descontarVaga) {?>
                                            <td><?php echo $faixa->name;?></td>
                                        <?php } else { ?>
                                            <td><?php echo $faixa->name;?><small> (Não desconta vagas)</small></td>
                                        <?php } ?>
                                        <td style="text-align: center;"><?php echo $qtdOrcamento;?></td>
                                        <td style="text-align: center;"><?php echo $qtdFaturas;?></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>


                        <?php
                        foreach ($tiposQuarto as $tipoQuarto){?>
                            <?php
                            foreach ($valorFaixas as $valoresFaixa) {
                                $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemRodoviarioByTipoHospedagemTipoFaixa($product->id, $tipoQuarto->id, $valoresFaixa->id);

                                $quarto = $programacao->getQuartos()[(int)$tipoQuarto->id];
                                $tipoEspedagemSemEstoque = false;
                                $nomeTipoQuartoEstoque = '';
                                $nomeTipoQuarto = $tipoQuarto->name;

                                if ($quarto && $product->controle_estoque_hospedagem)  {
                                    $nomeTipoQuarto = $nomeTipoQuarto.' - Acomoda '.$quarto->acomoda.' pessoa(s) por quarto';
                                    $nomeTipoQuartoEstoque = 'Quarto(s) Disponíveis: ' . $quarto->qtd_quartos_disponivel . ' quarto(s) <br/>Vagas Disponíveis: '.$quarto->estoque_atual.' pessoa(s)';
                                }

                                if ($faixa) {
                                    $ativo = $faixa->status == 'ATIVO' ? true  : false; ?>
                                    <?php if ($ativo){

                                        $itemFatura = $this->site->getItensByFaixaValores($product->id, $programacao->id,  $valoresFaixa->id, 'faturada', $tipoQuarto->id);
                                        $itemOrcamento = $this->site->getItensByFaixaValores($product->id, $programacao->id,  $valoresFaixa->id, 'orcamento', $tipoQuarto->id);

                                        $qtdOrcamento = $itemOrcamento->qtd ?: 0;
                                        $qtdFaturas = $itemFatura->qtd ?: 0;

                                        if ($faixa->descontarVaga) {
                                            $totalOrcamentoContaVagas += $qtdOrcamento;
                                            $totalFaturasContaVagas += $qtdFaturas;
                                        } else {
                                            $totalOrcamentoNaoContaVagas += $qtdOrcamento;
                                            $totalFaturasNaoContaVagas += $qtdFaturas;
                                        }

                                        ?>
                                        <?php if ($tipoQuartoId != $tipoQuarto->id) {
                                            $tipoQuartoId = $tipoQuarto->id;
                                            ?>
                                            <tr style="text-align: center;">
                                                <td colspan="3" class="warning" style="font-weight: 500;"><h4><?php echo $nomeTipoQuarto;?></h4><?php echo $nomeTipoQuartoEstoque;?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <?php if ($faixa->descontarVaga) {?>
                                                <td><?php echo $faixa->name;?></td>
                                            <?php } else { ?>
                                                <td><?php echo $faixa->name;?><small> (Não desconta vagas)</small></td>
                                            <?php } ?>
                                            <td style="text-align: center;"><?php echo $qtdOrcamento;?></td>
                                            <td style="text-align: center;"><?php echo $qtdFaturas;?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td style="text-align:right; font-weight:bold;">Total de Vendas Infantil</td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalOrcamentoNaoContaVagas;?></td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalFaturasNaoContaVagas;?></td>
                        </tr>
                        <tr>
                            <td style="text-align:right; font-weight:bold;">Total de Vendas</td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalOrcamentoContaVagas;?></td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalFaturasContaVagas;?></td>
                        </tr>
                        <tr>
                            <td style="text-align:right; font-weight:bold;">Total</td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalOrcamentoContaVagas + $totalOrcamentoNaoContaVagas ;?></td>
                            <td style="text-align:center; font-weight:bold;"><?php echo $totalFaturasContaVagas + $totalFaturasNaoContaVagas;?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>