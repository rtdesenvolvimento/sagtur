<style>
    .form-control_custom {
        display: block;
        width: 100%;
        height: 25px;
        padding: 0px 0px;
        font-size: 11px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .form-control_data {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom: none;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_agenda'); ?></h4>
        </div>
        <div class="modal-body">
            <?php echo form_open_multipart("agenda/agendar", array('data-toggle' => 'validator', 'role' => 'form')) ?>
            <div class="row">
                <div class="col-lg-12" style="margin-bottom: 25px;">
                    <?= lang("product", "product") ?>
                    <?php
                    $prod[''] = lang("select") . " " . lang("product");
                    foreach ($produtos as $produts) {
                        $prod[$produts->id] = $produts->name;
                    }
                    echo form_dropdown('produto', $prod, (isset($_POST['produto']) ? $_POST['produto'] : ($produto ? $produto->id : '')), 'class="form-control" id="produto" placeholder="' . lang("select") . " " . lang("product") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-md-3">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("data_do_dia", "doDia") ?>*
                            <?= form_input('doDia[]', '', 'class="form-control_data tip" required="required"', 'date') ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("hora_saida", "horaSaida") ?>
                            <?= form_input('horaSaida[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("data_ao_dia", "aoDia") ?>
                            <?= form_input('aoDia[]', '', 'class="form-control_data tip"', 'date') ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("hora_retorno", "horaRetorno") ?>
                            <?= form_input('horaRetorno[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <?= lang("vagas", "vagas") ?>
                        <?= form_input('vagas[]', (isset($_POST['vagas']) ? $_POST['vagas'] : ($produto ? $produto->quantidadePessoasViagem : '')), 'class="form-control_data tip mask_integer" required="required" ') ?>
                    </div>
                    <div class="col-md-1" style="float: left;margin-top: 25px;font-size: 20px;">
                        <i class="fa fa-plus addNewData" style="cursor: pointer;"></i>
                    </div>
                    <span id="new-linha-data"></span>

                    <div class="modal-footer">
                        <div style="text-align: right;margin-top: 20px;">
                            <?php echo form_submit('add_agenda_pacote', lang("save_date"), 'id="add_agenda_pacote" class="btn btn-primary" style="padding: 6px 15px; margin:25px 0;"'); ?>
                        </div>
                    </div>
                <?php }?>
            </div>
            <?= form_close(); ?>
            <div id="line-programacao"></div>
        </div>
    </div>

    <?= $modal_js ?>

    <script type="text/javascript">
        $(document).ready(function () {

            $('select, .select').select2({minimumResultsForSearch: 7});

            $('#produto').change(function (event) {
                $('#line-programacao').empty();

                if ($(this).val() !== '') {
                    $.ajax({
                        url: site.base_url + "agenda/abrir_modal_programacao/"+$(this).val(),
                        dataType: 'html',
                        type: 'get',
                    }).done(function (html) {
                        $('#line-programacao').append(html);
                    });
                }
            });

            $('.delAttr').click(function (event) {
                if (confirm('Deseja realmente excluir o lançamento?')) {
                    let id = $(this).attr('id');
                    let produto = $(this).attr('produto');

                    window.location = site.base = 'agenda/excluir/' + id + '/' + produto;
                }
            });

            $('.addNewData').click(function (event){
                $.ajax({
                    url: site.base_url + "agenda/view_lancamento_agenda_data",
                    dataType: 'html',
                    type: 'get',
                }).done(function (html) {
                    $('#new-linha-data').append(html);

                    $('.removeItemData').click(function (event) {
                        $(this).parent().parent().remove();
                    });

                    adicionarCamposObrigatorioAoFormulario();
                });
            });
        });

        function adicionarCamposObrigatorioAoFormulario() {

            $('form[data-toggle="validator"]').data('bootstrapValidator', null);
            $('form[data-toggle="validator"]').bootstrapValidator();
            $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

            let fields = $('.form-control');
            $.each(fields, function() {

                var id = $(this).attr('id');
                var iname = $(this).attr('name');
                var iid = '#'+id;

                if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                    if ($("label[for='" + id + "']").html() !== undefined) {
                        let label =  $("label[for='" + id + "']").html().replace('*', '');
                        $("label[for='" + id + "']").html(label + ' *');
                        $(document).on('change', iid, function () {
                            $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                        });
                    }
                }
            });
        }
    </script>

