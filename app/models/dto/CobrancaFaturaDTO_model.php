<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CobrancaFaturaDTO_model extends CI_Model
{
    public $faturas = [];

    //field cc
    public $senderHash=null;

    public $cardName;
    public $cardNumber;
    public $cardExpiryMonth;
    public $cardExpiryYear;
    public $cvc;

    public $deviceId;
    public $creditCardToken;
    public $numeroParcelas;

    public $cpfTitularCartao;
    public $celularTitularCartao;
    public $cepTitularCartao;
    public $enderecoTitularCartao;
    public $numeroEnderecoTitularCartao;
    public $complementoEnderecoTitularCartao;
    public $bairroTitularCartao;
    public $cidadeTitularCartao;
    public $estadoTitularCartao;

    //pagseguro cc
    public $totalParcelaPagSeguro;

    public $venda_manual;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarFatura($faturaId) {
        $this->faturas[count($this->faturas)] = $faturaId;
    }
}
