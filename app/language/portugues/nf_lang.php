<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['descricao_cobranca'] = 'Descrição da cobrança';
$lang['emissao'] = 'Emissão';
$lang['valor_nf'] = 'Valor NF';
$lang['status'] = 'Situação';
$lang['nf_agendamento'] = 'Emissão de Nota Fiscal Asaas (Agendamento -> Emissão)';

$lang['emitir_agendamento'] = 'Emitir Nota Fiscal';
$lang['excluir_agendamento'] = 'Excluir Agendamento';
$lang['nf_emitida'] = 'Nota Fiscal Emitida';
$lang['nf_agendamento_excluida'] = 'Nota Fiscal de Agendamento Excluída';
$lang['nf_cancelada'] = 'Nota Fiscal Cancelada';
$lang['cancelar_notafiscal'] = 'Cancelar Nota Fiscal';
$lang['no_nf_selecao_selected'] = 'Nenhuma Nota Fiscal Selecionada';
$lang['emitir_notasfiscal'] = 'Emitir Notas Fiscais';