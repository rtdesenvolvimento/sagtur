<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calendar</title>

    <link href='<?= $assets ?>fullcalendar/css/fullcalendar.min.css' rel='stylesheet' />
    <link href='<?= $assets ?>fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link href="<?= $assets ?>fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

</head>
<body>

<h1>ESCLOLHA A MELHOR DATA</h1>

<div id="calendar"></div>

<h1> ESCOLHA O MELHOR HORÁRIO</h1>
<select id="horarios"></select>

<script src='<?= $assets ?>fullcalendar/js/moment.min.js'></script>
<script src="<?= $assets ?>fullcalendar/js/fullcalendar.min.js"></script>
<script src="<?= $assets ?>fullcalendar/js/lang-all.js"></script>
<script src='<?= $assets ?>fullcalendar/js/bootstrap-colorpicker.min.js'></script>

<script type="application/javascript">
    $(document).ready(function(){

        let currentLangCode = 'pt-br';
        let events = [];

        <?php foreach ($programacoes as $programacao) {?>
            events.push(
                {
                    "id": <?=$programacao->id;?>,
                    "color":"#1d7010",
                    "title":"DISPONÍVEL",
                    "start": '<?=$programacao->getDataSaida();?>',
                }
            )
        <?php } ?>

        $('#calendar').fullCalendar(
            {
                lang: currentLangCode,
                events: events,
                // Handle Existing Event Click
                eventClick: function(calEvent, jsEvent, view) {
                    console.log(calEvent);

                    $('#horarios').append(new Option('das 08h até 12h', '', false, false));
                    $('#horarios').append(new Option('das 14h até 16h', '', false, false));

                }
            },
        );

        $('.fc-widget-content').css('height', '15px');
        $('.fc-event').css('cursor', 'pointer');
        $('.fc-toolbar').css('background', '#615cf5');
        $('.fc-toolbar').css('padding', '25px');
        $('.fc-toolbar').css('color', '#ffffff');


    });
</script>

</body>
</html>