<style type="text/css" media="screen">
    #BusTable td:nth-child(1) {display: none;}
    #BusTable td:nth-child(2) {width: 80%;}
    #BusTable td:nth-child(3) {text-align: center;}
    #BusTable td:nth-child(4) {text-align: center;}
</style>

<script>

    function row_action(x, y, z) {

        let planta = z[1];

        if (planta === 'MAPA BASE ÔNIBUS 1 ANDAR' || planta === 'MAPA BASE ÔNIBUS 2 ANDARES' || planta === 'MAPA BASE ÔNIBUS 1 ANDAR *Não excluir' || planta === 'MAPA BASE ÔNIBUS 2 ANDARES *Não excluir') {
            return '';
        }

        return x;
    }

    $(document).ready(function () {

        $('#BusTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('bus/getMapas') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                null,
                {"bSortable": false, "mRender": row_action},
            ]
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('bus_plans'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('bus/addBus'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('adicionar_bus') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="BusTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("name"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("andares"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("total_assentos"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="3" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="javascript">
    $(document).ready(function () {});
</script>