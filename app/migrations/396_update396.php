<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update396 extends CI_Migration {

    public function up() {

        $this->alter_table_assento_bloqueio();
        $this->alter_table_companies();

        $this->db->update('settings',  array('version' => '3.9.6'), array('setting_id' => 1));
    }

    public function alter_table_companies() {
        $fields = array(
            'ultimo_assento' => array('type' => 'VARCHAR', 'constraint' => '30' ),
        );

        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_assento_bloqueio() {
        $fields = array(
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),

            'cart_lock' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'cart_time_blocked' => array('type' => 'TIMESTAMP', 'null' => TRUE ),//todo quando foi bloqueado no link essa variavel destrava o assento depois de 10 minutos

        );

        $this->dbforge->add_column('assento_bloqueio', $fields);
    }

    public function down() {

    }
}
