<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update377 extends CI_Migration {

    public function up() {

        //$this->create_index();

        $this->db->update('settings',  array('version' => '3.7.7'), array('setting_id' => 1));
    }

    public function create_index() {

        try {

            //sma_companies
            $this->db->query('CREATE INDEX indxs_customer_cpf1	 ON sma_companies (vat_no)');
            $this->db->query('CREATE INDEX indxs_companies_group_name1	 ON sma_companies (group_name)');

            //sma_sales
            $this->db->query('CREATE INDEX indxs_sales_created_by1	 ON sma_sales (created_by)');
            $this->db->query('CREATE INDEX indxs_sales_payment_status1	 ON sma_sales (payment_status)');
            $this->db->query('CREATE INDEX indxs_sales_sale_status1 ON sma_sales (sale_status)');
            $this->db->query('CREATE INDEX indxs_sales_date1 ON sma_sales (date)');
            $this->db->query('CREATE INDEX indxs_sales_customer1 ON sma_sales (customer)');

            //sma_sale_items
            $this->db->query('CREATE INDEX indxs_sales_items_poltrona1 ON sma_sale_items (tipoTransporte, programacaoId)');
            $this->db->query('CREATE INDEX indxs_sales_item_sale_id_customer_id1 ON sma_sale_items (sale_id, customerClient)');
            $this->db->query('CREATE INDEX indxs_sales_item_quantity1 ON sma_sale_items (quantity)');
            $this->db->query('CREATE INDEX indxs_sale_items_programacaoId1	 ON sma_sale_items (programacaoId)');
            $this->db->query('CREATE INDEX indxs_sale_items_sale_id1 ON sma_sale_items (sale_id)');
            $this->db->query('CREATE INDEX indxs_sale_items_customer_id1 ON sma_sale_items (customerClient)');
            $this->db->query('CREATE INDEX indxs_sale_items_programacaoId_descontarVaga1 ON sma_sale_items (programacaoId, descontarVaga)');
            $this->db->query('CREATE INDEX indxs_sale_items_descontarVaga1 ON sma_sale_items (descontarVaga)');
            $this->db->query('CREATE INDEX indxs_sale_items_adicional1 ON sma_sale_items (adicional)');

            //sma_parcela_fatura
            $this->db->query('CREATE INDEX indxs_sma_parcela_fatura__parcela_fatura1 ON sma_parcela_fatura (parcela, fatura)');

            //sma_fatura_cobranca
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_fatura1 ON sma_fatura_cobranca (fatura)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_status1 ON sma_fatura_cobranca (status)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_code1 ON sma_fatura_cobranca (code)');

            //sma_fatura
            $this->db->query('CREATE INDEX indxs_sma_fatura_pessoa1 ON sma_fatura (pessoa)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_programacaoId1 ON sma_fatura (programacaoId)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_tipooperacao1 ON sma_fatura (tipooperacao)');
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }



    public function down() {}
}
