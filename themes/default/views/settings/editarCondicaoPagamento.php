<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_condicao_pagamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("system_settings/editarCondicaoPagamento/" . $category->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'Ativo' => lang('ativo'),
                    'Inativo' => lang('inativo')
                );
                echo form_dropdown('status', $opts, $category->status , 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('condicao_pagamento_name', 'name'); ?>
                <?= form_input('name', $category->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("intervalo", "intervalo") ?>
                <?php
                $opts = array(
                    'dia' => lang('dia'),
                    'mes' => lang('mes')
                );
                echo form_dropdown('intervalo', $opts, $category->intervalo , 'class="form-control" id="intervalo" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("parcelas", "parcelas"); ?>
                <?php echo form_input('parcelas', $category->parcelas , 'class="form-control input-tip mask_integer" id="sldiscount"'); ?>
            </div>
            <div class="form-group">
                <?= lang('intervalo_em_dias', 'dias'); ?>
                <?= form_input('dias', $category->dias , 'class="form-control" id="dias"'); ?>
            </div>
            <?php echo form_hidden('id', $category->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarCondicaoPagamento', lang('editar_condicao_pagamento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>