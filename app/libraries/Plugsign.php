<?php

use GuzzleHttp\Client;

class Plugsign
{
    private $client;
    private $url = 'https://app.plugsign.com.br/api';
    private $headers;

    public $apiKey;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @throws Exception
     */
    public function createFolder($data)
    {
        return $this->sendRequest('/folders', 'POST', $data, Folder::class);
    }

    /**
     * @throws Exception
     */
    public function editFolder($data, $id)
    {
        return $this->sendRequest('/folders/'.$id, 'PUT', $data, Folder::class);
    }

    /**
     * @throws Exception
     */
    public function deleteFolder($id)
    {
        return $this->sendRequest('/folders/'.$id, 'DELETE');
    }

    /**
     * @throws Exception
     */
    public function createDocument($data)
    {
        return $this->sendRequest('/files/upload', 'POST', $data, Document::class);
    }


    /**
     * @throws Exception
     */
    public function createDocumentSend($data)
    {
        return $this->sendRequest('/files/upload/requests', 'POST', $data, Document::class);
    }

    /**
     * @throws Exception
     */
    public function protectDocument($data)
    {
        return $this->sendRequest('/files/protect', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function sign($data)
    {
        return $this->sendRequest('/files/sign', 'POST', $data);
    }


    /**
     * @throws Exception
     */
    public function sendDocumentTo($data)
    {
        return $this->sendRequest('/files/send', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function downloadDocument($document_key)
    {
        return $this->sendRequest('/files/download/'.$document_key, 'GET');
    }

    /**
     * @throws Exception
     */
    public function toSign($data)
    {
        return $this->sendRequest('/requests/documents', 'POST', $data, Signatures::class);
    }

    /**
     * @throws Exception
     */
    public function cancelSignatory($data)
    {
        return $this->sendRequest('/requests/cancelled', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function sign_key($data)
    {
        return $this->sendRequest('/requests/sign', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function delete($data)
    {
        return $this->sendDelete('/docs', $data);
    }

    /**
     * @throws Exception
     */
    public function editDocument($data, $document_key)
    {
        return $this->sendRequest('/docs/'.$document_key, 'PUT', $data);
    }

    /**
     * @throws Exception
     */
    public function resendWhatsapp($data)
    {
        return $this->sendRequest('/reminders/whatsapp', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function resendEmail($data)
    {
        return $this->sendRequest('/reminders/email', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function createCompany($data)
    {
        return $this->sendRequest('/companies', 'POST', $data, Company::class);
    }

    /**
     * @throws Exception
     */
    public function updateCompany($data, $id)
    {
        return $this->sendRequest('/companies/'.$id, 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function getTotalDocs($id)
    {
        return $this->sendRequest('/companies/signed', 'GET');
    }

    /**
     * @throws Exception
     */
    public function createCustomer($data)
    {
        return $this->sendRequest('/customers', 'POST', $data, Customer::class);
    }

    /**
     * @throws Exception
     */
    public function updateCustomer($data, $email)
    {
        return $this->sendRequest('/customers/'.$email, 'POST', $data, Customer::class);
    }

    /**
     * @throws Exception
     */
    public function searchCustomer($email)
    {
        return $this->sendRequest('/customers?email=' . urlencode($email), 'GET', array(), CustomerSearch::class);
    }

    /**
     * @throws Exception
     */
    public function deleteCustomer($email)
    {
        return $this->sendRequest('/customers?email=' . urlencode($email), 'DELETE');
    }

    /**
     * @throws Exception
     */
    public function createWebhook($data)
    {
        return $this->sendRequest('/webhooks', 'POST', $data);
    }

    /**
     * @throws Exception
     */
    public function updateWebhook($data, $id)
    {
        return $this->sendRequest('/webhooks/'.$id, 'PUT', $data);
    }

    /**
     * @throws Exception
     */
    public function getWebhooks()
    {
        return $this->sendRequest('/webhooks', 'GET', array(), Webhook::class);
    }

    private function sendDelete($endpoint, $params = [])
    {
        try {

            $this->headers = [
                'Authorization' => 'Bearer ' . $this->apiKey,
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ];

            $options = [
                'headers' => $this->headers,
            ];

            if (!empty($params)) {
                $options['json'] = $params;
            }

            $response = $this->client->delete($this->url . $endpoint, $options);
            $body = $response->getBody();
            $data = json_decode((string)$body, true);

            if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
                return $data;
            } else {
                throw new \Exception("Erro: " . $response->getReasonPhrase());
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {

            $responseBody = json_decode($e->getResponse()->getBody(), true);
            $errorMessage = $this->mapErrorMessage($responseBody['message']) ?? 'Erro desconhecido';
            $errors = $responseBody['errors'] ?? [];

            $detailedErrors = '';
            foreach ($errors as $field => $messages) {
                $translatedField = $this->mapFieldName($field);
                foreach ($messages as $message) {
                    $translatedMessage = $this->mapErrorMessage($message);
                    $detailedErrors .= $translatedField . ': ' . $translatedMessage . "<br/>";
                }
            }

            if ($errorMessage) {
                throw new \Exception("Mensagem de Erro: " . $errorMessage . "<br/>Detalhes:<br/>" . $detailedErrors);
            } else {
                throw new \Exception("Erro: ". $this->mapErrorMessage($responseBody['error']) . ' Code ' . $e->getResponse()->getReasonPhrase());
            }

        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new \Exception("Server error: " . $e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new \Exception("Error : " . $e->getMessage());
        } catch (\Exception $e) {
            throw new \Exception("Error : " . $e->getMessage());
        }
    }

    private function sendRequest($endpoint, $method = 'POST', $params = [], $responseClass = null)
    {
        try {

            $this->headers = [
                'Authorization' => 'Bearer ' . $this->apiKey,
                'Accept' => 'application/json',
            ];

            $options = [
                'headers' => $this->headers,
            ];

            if (!empty($params)) {
                if ($this->hasFile($params)) {
                    $options['multipart'] = $this->prepareMultipartData($params);
                } else {
                    $options['headers']['Content-Type'] = 'application/json';
                    $options['json'] = $params;
                }
            }

            $response = $this->client->request($method, $this->url . $endpoint, $options);
            $body = $response->getBody();

            // Verifica se é um tipo de arquivo binário (PDF, Word, Imagem, etc.)
            $contentType = $response->getHeaderLine('Content-Type');
            if (preg_match('/application\/(pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)/', $contentType) ||
                preg_match('/image\/(jpeg|png|gif)/', $contentType)) {

                // Define o nome do arquivo para download ou exibição
                $fileName = "file" . $this->getFileExtensionFromContentType($contentType);

                // Define os cabeçalhos apropriados para exibir ou baixar o arquivo
                header('Content-Type: ' . $contentType);
                header('Content-Disposition: inline; filename="' . basename($fileName) . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                echo $body; // Exibe o conteúdo do arquivo
                exit;
            }

            $data = json_decode((string)$body, true);

            if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
                if ($responseClass && class_exists($responseClass)) {
                    return new $responseClass($data);
                }
                return $data;
            } else {
                throw new \Exception("Erro: " . $response->getReasonPhrase());
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseBody = json_decode($e->getResponse()->getBody(), true);
            $errorMessage = $this->mapErrorMessage($responseBody['message']) ?? 'Erro desconhecido';
            $errors = $responseBody['errors'] ?? [];

            $detailedErrors = '';
            foreach ($errors as $field => $messages) {
                $translatedField = $this->mapFieldName($field);
                foreach ($messages as $message) {
                    $translatedMessage = $this->mapErrorMessage($message);
                    $detailedErrors .= $translatedField . ': ' . $translatedMessage . "<br/>";
                }
            }

            if ($errorMessage) {
                throw new \Exception("Mensagem de Erro: " . $errorMessage . "<br/>Detalhes:<br/>" . $detailedErrors);
            } else {
                throw new \Exception("Erro: ". $this->mapErrorMessage($responseBody['error']) . ' Code ' . $e->getResponse()->getReasonPhrase());
            }

        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new \Exception("Server error: " . $e->getResponse()->getBody());
        } catch (\Exception $e) {
            throw new \Exception("Error : " . $e->getMessage());
        }
    }

    private function getFileExtensionFromContentType($contentType): string
    {
        $map = [
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'image/gif' => '.gif',
        ];

        return $map[$contentType] ?? '';
    }

    private function hasFile($params): bool
    {
        foreach ($params as $key => $value) {
            if (is_resource($value) || (is_array($value) && isset($value['file'])) && isset($value['company_brand'])) {
                return true;
            }
        }
        return false;
    }

    private function prepareMultipartData($params): array
    {
        $multipartData = [];
        foreach ($params as $key => $value) {
            if (is_resource($value) || (is_array($value) && isset($value['file']))) {
                $multipartData[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            } else {
                $multipartData[] = [
                    'name' => $key,
                    'contents' => (string)$value,
                ];
            }
        }
        return $multipartData;
    }

    private function mapFieldName($field): string
    {
        $fieldMap = [
            'name' => 'Nome',
            // Adicione outros campos conforme necessário
        ];

        return $fieldMap[$field] ?? ucfirst($field);
    }

    private function mapErrorMessage($message): string
    {
        if ($message === null) {
            return '';
        }

        $messageMap = [
            'The name has already been taken.' => 'O nome já foi escolhido.',
            'The given data was invalid.' => 'Os dados fornecidos eram inválidos.',
            'The name field is required.' => 'O campo nome é obrigatório.',
            'The file field is required.' => 'O campo arquivo é obrigatório.',
            'Token not found!' => 'Token Inválido',
            'Status not pending' => 'Status não pendente',
            'file not found!' => 'Arquivo não encontrado',
            'Name exists' => 'Nome já existe',
            'doc not found' => 'Documento não encontrado',
            'The page field is required.' => 'O campo página é obrigatório.',
            'The company brand must be an image.' => 'A marca da empresa deve ser uma imagem.',
            'The company brand must be a file of type: png, jpg, jpeg.' => 'A marca da empresa deve ser um arquivo do tipo: png, jpg, jpeg.',
            'Too Many Attempts.' => 'Muitas tentativas.',
            'Folder not found' => 'Pasta não encontrada',
            'The email has already been taken.' => 'O email já foi escolhido.',
            'The cert password must be a string.' => 'A senha do certificado deve ser uma string.',
        ];

        return $messageMap[$message] ?? $message;
    }
}

class Folder
{
    public $id;
    public $folder;
    public $accessibility;
    public $subFolders;
    public $createdOn;
    public $message;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->folder = $data['folder'];
        $this->accessibility = $data['accessibility'];
        $this->subFolders = $data['sub_folders'];
        $this->createdOn = $data['created_on'];
        $this->message = $data['message'];
    }
}

class Document
{
    public $id;
    public $name;
    public $file_name;
    public $extension;
    public $size;
    public $status;
    public $document_key;
    public $editted;
    public $is_template;
    public $template_fields;
    public $accessibility;
    public $departments;
    public $folder;
    public $uploaded_by;
    public $uploaded_on;
    public $download;

    public function __construct($data)
    {
        $data = $data['data'] ?? $data;

        $this->id = $data['id'] ?? null;
        $this->name = $data['name'] ?? null;
        $this->file_name = $data['file_name'] ?? null;
        $this->extension = $data['extension'] ?? null;
        $this->size = $data['size'] ?? null;
        $this->status = $data['status'] ?? null;
        $this->document_key = $data['document_key'] ?? null;
        $this->editted = $data['editted'] ?? null;
        $this->is_template = $data['is_template'] ?? null;
        $this->template_fields = $data['template_fields'] ?? null;
        $this->accessibility = $data['accessibility'] ?? null;
        $this->departments = $data['departments'] ?? null;
        $this->folder = $data['folder'] ?? null;
        $this->uploaded_by = $data['uploaded_by'] ?? null;
        $this->uploaded_on = $data['uploaded_on'] ?? null;
        $this->download = $data['download'] ?? null;
    }
}

class Sender
{
    public $id;
    public $name;
    public $lastName;
    public $email;
    public $permissions;

    public function __construct($data)
    {
        $this->id = $data['id'] ?? null;
        $this->name = $data['name'] ?? null;
        $this->lastName = $data['last_name'] ?? null;
        $this->email = $data['email'] ?? null;
        $this->permissions = $data['permissions'] ?? [];
    }
}

class Signatures
{
    public array $signatures = [];
    public $apiMessage;
    public $error;

    public function __construct($data, $message = null, $error = null)
    {
        $data = $data['data'] ?? $data;

        if ($data) {
            foreach ($data as $signature) {
                $this->addSignature(new Signature($signature));
            }
        }
        $this->apiMessage = $message;
        $this->error = $error;
    }

    public function addSignature(Signature $signature): void
    {
        $this->signatures[] = $signature;
    }

    public function getSignatures(): array
    {
        return $this->signatures;
    }

    public function getSignature(int $index): ?Signature
    {
        return $this->signatures[$index] ?? null;
    }
}


class Signature
{
    public $id;
    public $signingKey;
    public $document;
    public $email;
    public $message;
    public ?Sender $sender;
    public $expireDate;
    public $sendTime;
    public $updateTime;
    public $createdAt;
    public $status;
    public $observers;

    public function __construct($data)
    {

        $data = $data['data'] ?? $data;

        if ($data) {
            $this->id = $data['id'] ?? null;
            $this->signingKey = $data['signing_key'] ?? null;
            $this->document = $data['document'] ?? null;
            $this->email = $data['email'] ?? null;
            $this->message = $data['message'] ?? null;
            $this->sender = isset($data['sender']) ? new Sender($data['sender']) : null;
            $this->expireDate = $data['expire_date'] ?? null;
            $this->sendTime = $data['send_time'] ?? null;
            $this->updateTime = $data['update_time'] ?? null;
            $this->createdAt = $data['creted_at'] ?? null;
            $this->status = $data['status'] ?? null;
            $this->observers = $data['observer'] ?? [];
        }
    }
}


class Company
{
    public $id;
    public $token;

    // Construtor
    public function __construct($data)
    {
        $data = $data['data'] ?? $data;

        $this->id = $data['company']['id'] ?? null;
        $this->token = $data['token'] ?? null;
    }
}

class Customer {

    public $id;
    public $name;
    public $last_name;
    public $phone;
    public $address;
    public $email;
    public $cpf;

    public function __construct($data) {

        $data = $data['data'] ?? $data;

        $this->id = $data['id'] ?? null;
        $this->name = $data['name'] ?? null;
        $this->last_name = $data['last_name'] ?? null;
        $this->phone = $data['phone'] ?? null;
        $this->address = $data['address'] ?? null;
        $this->email = $data['email'] ?? null;
        $this->cpf = $data['cpf'] ?? null;
    }
}

class CustomerSearch {

    public $id;
    public $name;
    public $last_name;
    public $phone;
    public $address;
    public $email;
    public $cpf;

    public function __construct($data) {

        $data = $data['data'] ?? $data;

        if (is_array($data)) {
            $data = $data[0];
        }

        $this->id = $data['id'] ?? null;
        $this->name = $data['name'] ?? null;
        $this->last_name = $data['last_name'] ?? null;
        $this->phone = $data['phone'] ?? null;
        $this->address = $data['address'] ?? null;
        $this->email = $data['email'] ?? null;
        $this->cpf = $data['cpf'] ?? null;

    }

}

class Webhook
{

    public $webhooks;

    public function __construct($data) {
        $this->webhooks = $data['data'] ?? $data;
    }

}