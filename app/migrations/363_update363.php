<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update363 extends CI_Migration {

    public function up() {

        $this->alter_table_room_list_hospedagem_hospede();
        $this->db->update('settings',  array('version' => '3.6.3'), array('setting_id' => 1));
    }

    public function alter_table_room_list_hospedagem_hospede() {
        $fields = array(
            'sale_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );
        $this->dbforge->add_column('room_list_hospedagem_hospede', $fields);
    }

    public function down() {}
}
