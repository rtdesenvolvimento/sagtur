<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('storage_url')) {
    function storage_url($path = ''): string
    {
        $storageBaseUrl = 'https://files.suareservaonline.com.br';

        return rtrim($storageBaseUrl, '/') . '/' . ltrim($path, '/');
    }
}

if (!function_exists('storage_docs_url')) {
    function storage_docs_url($path = ''): string
    {
        $storageBaseUrl = 'https://docs.sagtur.com.br';

        return rtrim($storageBaseUrl, '/') . '/' . ltrim($path, '/');
    }
}


if (!function_exists('storage_img_url')) {
    function storage_img_url($path = ''): string
    {
        $storageBaseUrl = 'https://img.suareservaonline.com.br';
        //$storageBaseUrl = 'https://pub-ff317fe9b82f49768175a113cc335a3e.r2.dev';
        //$storageBaseUrl = 'https://11652bc094dc1f514aaafb293ce84bd9.r2.cloudflarestorage.com';

        return rtrim($storageBaseUrl, '/') . '/' . ltrim($path, '/');
    }
}

if (!function_exists('storage_img_thumbs_url')) {
    function storage_img_thumbs_url($path = ''): string
    {
        $storageBaseUrl = 'https://img.suareservaonline.com.br';

        $pathParts = explode('/', $path);
        $fileName = array_pop($pathParts);

        $pathParts[] = 'thumbs';
        $pathParts[] = $fileName;

        // Reconstruir o caminho modificado
        $modifiedPath = implode('/', $pathParts);

        return rtrim($storageBaseUrl, '/') . '/' . ltrim($modifiedPath, '/');
    }
}
