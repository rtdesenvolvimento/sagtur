<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update344 extends CI_Migration {

    public function up() {

        //$this->create_index();
        $this->create_index_2();

        $this->db->update('settings',  array('version' => '3.22.08.31.2'), array('setting_id' => 1));
    }

    public function create_index() {

        try {

            //sma_companies
            $this->db->query('CREATE INDEX indxs_customer_cpf	 ON sma_companies (vat_no)');
            $this->db->query('CREATE INDEX indxs_companies_group_name	 ON sma_companies (group_name)');

            //sma_sales
            $this->db->query('CREATE INDEX indxs_sales_created_by	 ON sma_sales (created_by)');
            $this->db->query('CREATE INDEX indxs_sales_payment_status	 ON sma_sales (payment_status)');
            $this->db->query('CREATE INDEX indxs_sales_sale_status ON sma_sales (sale_status)');
            $this->db->query('CREATE INDEX indxs_sales_date ON sma_sales (date)');
            $this->db->query('CREATE INDEX indxs_sales_customer ON sma_sales (customer)');

            //sma_sale_items
            $this->db->query('CREATE INDEX indxs_sales_items_poltrona ON sma_sale_items (tipoTransporte, programacaoId)');
            $this->db->query('CREATE INDEX indxs_sales_item_sale_id_customer_id ON sma_sale_items (sale_id, customerClient)');
            $this->db->query('CREATE INDEX indxs_sales_item_quantity ON sma_sale_items (quantity)');
            $this->db->query('CREATE INDEX indxs_sale_items_programacaoId	 ON sma_sale_items (programacaoId)');
            $this->db->query('CREATE INDEX indxs_sale_items_sale_id ON sma_sale_items (sale_id)');
            $this->db->query('CREATE INDEX indxs_sale_items_customer_id ON sma_sale_items (customerClient)');
            $this->db->query('CREATE INDEX indxs_sale_items_programacaoId_descontarVaga ON sma_sale_items (programacaoId, descontarVaga)');
            $this->db->query('CREATE INDEX indxs_sale_items_descontarVaga ON sma_sale_items (descontarVaga)');
            $this->db->query('CREATE INDEX indxs_sale_items_adicional ON sma_sale_items (adicional)');

            //sma_parcela_fatura
            $this->db->query('CREATE INDEX indxs_sma_parcela_fatura__parcela_fatura ON sma_parcela_fatura (parcela, fatura)');

            //sma_fatura_cobranca
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_fatura ON sma_fatura_cobranca (fatura)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_status ON sma_fatura_cobranca (status)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_cobranca_code ON sma_fatura_cobranca (code)');

            //sma_fatura
            $this->db->query('CREATE INDEX indxs_sma_fatura_pessoa ON sma_fatura (pessoa)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_programacaoId ON sma_fatura (programacaoId)');
            $this->db->query('CREATE INDEX indxs_sma_fatura_tipooperacao ON sma_fatura (tipooperacao)');
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public function create_index_2() {
        $this->db->query('CREATE INDEX indxs_sma_fatura_tipo_cobranca ON sma_fatura (tipoCobranca)');


        $this->db->query('CREATE INDEX indxs_sma_fatura_status ON sma_fatura (status)');

        //sma_agenda_viagem
        $this->db->query('CREATE INDEX indxs_sma_agenda_viagem_produto ON sma_agenda_viagem (produto)');

        //sma_payments
        $this->db->query('CREATE INDEX indxs_sma_sma_payments_fatura ON sma_payments (fatura)');
        $this->db->query('CREATE INDEX indxs_sma_sma_payments_pessoa ON sma_payments (pessoa)');
        $this->db->query('CREATE INDEX indxs_sma_sma_payments_status ON sma_payments (status)');

        //sma_products
        $this->db->query('CREATE INDEX indxs_sma_products_unit ON sma_products (unit)');

        //sma_sale_items
        $this->db->query('CREATE INDEX indxs_sale_items_faixaId ON sma_sale_items (faixaId)');
        $this->db->query('CREATE INDEX indxs_sale_items_product_id ON sma_sale_items (product_id)');
        $this->db->query('CREATE INDEX indxs_sale_items_product_id_programacaoId ON sma_sale_items (product_id, programacaoId)');
        $this->db->query('CREATE INDEX indxs_sale_items_tipoHospedagem ON sma_sale_items (tipoHospedagem)');

    }

    public function down() {}
}
