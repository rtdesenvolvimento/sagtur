<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendamentoServico_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('repository/AgendamentoRepository_model', 'AgendamentoRepository_model');

        $this->load->model('service/HorarioTrabalhoService_model', 'HorarioTrabalhoService_model');
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');

        $this->load->model('model/Agendamento_model', 'Agendamento_model');
        $this->load->model('model/ContaReceber_model', 'ContaReceber_model');

        $this->load->model('companies_model', 'companies_model');
        $this->load->model('financeiro_model', 'financeiro_model');
    }

    public function salvar($agendamento, $definirHoraFinalAutomaticamente=true) {
        //$agendamento = new Agendamento_model();

        try {

            $this->__iniciaTransacao();

            $dateDe = $agendamento->getData();
            $dateAte = $agendamento->getDataAte();

            $horaDe = $agendamento->getHoraInicio();
            $horaATe = $agendamento->getHoraTermino();

            $agendamentosEncontratos = $this->HorarioTrabalhoService_model->agendamentosEncontrado($dateDe, $dateAte, $horaDe, $horaATe);

            if ($agendamentosEncontratos) {
                throw new Exception('Ops! Já encontramos um agendamento para o mesmo dia e horário selecionado! Tente um outro periodo para confirmr seu agendamento!');
            }

            $agendamento->setDataReserva(date('Y-m-d'));

            $agendamento = $this->preencherAgendamentoAntesSalvar($agendamento, $definirHoraFinalAutomaticamente);

            $agendamento->setCompromisso($this->salvarNoCalendario($agendamento));

            $agendamentoId = $this->__save('agendamento', $agendamento->__toArray());

            $this->__confirmaTransacao();

            return $agendamentoId;
        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function editar($id, $agendamento) {
        try {
            $this->__iniciaTransacao();

            $agendamentoOpen =  $this->openAgendamentoModel($id);

            $this->excluirEventoNoCalendario($agendamentoOpen);

            $agendamento = $this->preencherAgendamentoAntesSalvar($agendamento, false);

            $agendamento->setCompromisso($this->salvarNoCalendario($agendamento));

            $isHorarioAlterado = $this->alteracaoDeHorarioDoAgendaento($agendamentoOpen, $agendamento);

            $this->__edit('agendamento',$agendamento->__toArray() , 'id', $id);

            $this->__confirmaTransacao();

            return $isHorarioAlterado;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function alteracaoDeHorarioDoAgendaento($agendamentoAntesSalvar, $agendamentoAposSalvar) {

        $dataDeAntesAlterar = $agendamentoAntesSalvar->getData();
        $dataAteAntesAlterar = $agendamentoAntesSalvar->getDataAte();
        $horaInicioAntesAlterar = $agendamentoAntesSalvar->getHoraInicio();
        $horaTerminoAntesAlterar = $agendamentoAntesSalvar->getHoraTermino();

        $dataDeDepoisAlter = $agendamentoAposSalvar->getData();
        $dataAteDepoisAlter = $agendamentoAposSalvar->getDataAte();

        $horaInicioDepoisAlter = $agendamentoAposSalvar->getHoraInicio();
        $horaTerminoDepoisAlter = $agendamentoAposSalvar->getHoraTermino();

        $hasAntes = ($dataDeAntesAlterar.$dataAteAntesAlterar.$horaInicioAntesAlterar.$horaTerminoAntesAlterar);
        $hasDepois = ($dataDeDepoisAlter.$dataAteDepoisAlter.$horaInicioDepoisAlter.$horaTerminoDepoisAlter);

        if ($hasAntes != $hasDepois) return true;
        else return false;

    }

    private function preencherAgendamentoAntesSalvar($agendamento, $definirHoraFinalAutomaticamente=true) {

        if ($definirHoraFinalAutomaticamente) $agendamento->setHoraTermino($this->preencherHoraFinal($agendamento));

        if ($agendamento->getIsBloqueado()) {
            $agendamento->setEmail('');
            $agendamento->setCelular('');
            return $agendamento;
        }

        $produto = $this->site->getProductByID($agendamento->getProduto());
        $categoria = $this->__getById('categories', $produto->category_id);
        $tipoCobranca = $this->__getById('tipo_cobranca', $agendamento->getTipoCobranca());

        if ($agendamento->getValor() == null) $agendamento->setValor($produto->price);
        if ($agendamento->getDataAte() == null)  $agendamento->setDataAte($agendamento->getData());

        $agendamento->setNomeProduto($produto->name);
        $agendamento->setNomeCategoria($categoria->name);
        $agendamento->setNomeTipoCobranca($tipoCobranca->name);

        $agendamento->setStatus(Agendamento_model::STATUS_AGUARDANDO_PAGAMENTO);
        $agendamento->setCliente($this->salvarCliente($agendamento));
        $agendamento->setContaReceber($this->gerarContaReceber($agendamento));

        return $agendamento;
    }

    public function openAgendamentoModel($id) {
        return $this->__toModel($this->__getById('agendamento', $id), new Agendamento_model());
    }

    public function cancelarByContaReceber($contaReceberId) {
        $agendamento = $this->buscarAgendamentoByContaReceber($contaReceberId);

        $this->cancelar($agendamento->id, false);

        $agendamentoModel = $this->AgendamentoServico_model->openAgendamentoModel($agendamento->id);

        if ($agendamentoModel->getEmail() == null) return false;

        $arquivo = 'cancelamento_gw.html';
        $subject = '📷 ⚠️CANCELAMENTO DO ENSAIO ⚠️' . $this->Settings->site_name;

        $message = $this->converterTemplateToMessage($agendamento->id, $agendamentoModel, $arquivo);

        $to = $agendamentoModel->getEmail().','.$this->Settings->default_email;

        $this->sma->send_email($to, $subject, $message, null, null, [], '', '');
    }

    private function converterTemplateToMessage($id, $agendamentoModel , $arquivo)  {
        $this->load->library('parser');

        $parse_data = array(
            'reference_number' => $id,
            'contact_person' => $agendamentoModel->getNomeCompleto(),
            'company' => $agendamentoModel->getNomeCompleto(),
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/logo.png" alt="' . $this->Settings->site_name . '"/>',
        );

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/'.$arquivo)) {
            $temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/'.$arquivo);
        } else {
            $temp = file_get_contents('./themes/default/views/email_templates/'.$arquivo);
        }

        return $this->parser->parse_string($temp, $parse_data);
    }

    public function cancelar($id, $cancelarCobranca=true) {

        try {

            $this->__iniciaTransacao();

            $agendamento = $this->__toModel($this->__getById('agendamento', $id), new Agendamento_model());

            if ($this->isCancelada($agendamento)) throw new Exception("Agendamento já encontra-se Cancelado e não pode ser cancelado uma segunda vez.");

            $this->cancelarFatura($agendamento, $cancelarCobranca);

            $this->excluirEventoNoCalendario($agendamento);

            $this->__edit('agendamento', array('status' => Agendamento_model::STATUS_CANCELADA), 'id', $id);

            $this->__confirmaTransacao();
        } catch(Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function getPagamentosByFatura($agendamentoId) {
        $agendamento = $this->__toModel($this->__getById('agendamento', $agendamentoId), new Agendamento_model());
        $fatura = $this->buscarFatura($agendamento);

        return $this->financeiro_model->getPagamentosByFatura($fatura->id);
    }

    public function adicionarPagamento($agendamentoId) {
        $agendamento = $this->__toModel($this->__getById('agendamento', $agendamentoId), new Agendamento_model());
        $fatura = $this->buscarFatura($agendamento);

        return $fatura->id;
    }

    private function cancelarFatura($agendamento, $cancelarCobranca=true) {
        $fatura = $this->buscarFatura($agendamento);

        $this->FinanceiroService_model->cancelar($fatura->id, '', $cancelarCobranca);
    }

    private function excluirEventoNoCalendario($agendamento) {
        $this->companies_model->delete_event($agendamento->getCompromisso());
    }

    private function isCancelada($agendamento) {
        return $agendamento->getStatus() == Agendamento_model::STATUS_CANCELADA;
    }

    private function buscarFatura($agendamento) {
        $parcela = $this->financeiro_model->getParcelaByIdContaReceber($agendamento->getContaReceber());
        $faturaParcela = $this->financeiro_model->getParcelaFaturaByParcela($parcela->id);

        return $faturaParcela;
    }

    public function buscarCobranca($agendamento) {
        $faturaParcela = $this->buscarFatura($agendamento);
        $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($faturaParcela->id);

        return $cobranca;
    }

    private function preencherHoraFinal($agendamento) {
        $horarioComercial = $this->HorarioTrabalhoService_model->getHorarioComercial();

        $dataAgendamento = $agendamento->getData();
        $horaInicio = $agendamento->getHoraInicio();

        $duracao = $horarioComercial->getDuracao();
        $intervalo = ' +'.$duracao.' minutes';
        $data = date('Y-m-d H:i', strtotime($dataAgendamento.' '.$horaInicio));

        $data = date('Y-m-d H:i', strtotime($data.$intervalo));
        $horaFinal = date('H:i', strtotime($data));

        return $horaFinal;
    }

    private function salvarCliente($agendamento) {

        $data = array(
            'group_id'              => 3,//TODO VIA CONFIGURACOA
            'customer_group_id'     => 1,//TODO VIA CONFIGURACAO
            'group_name'            => 'customer',
            'customer_group_name'   => 'PASSAGEIROS',
            'company'               => strtoupper($agendamento->getNomeCompleto()),
            'name'                  => strtoupper($agendamento->getNomeCompleto()),
            'email'                 => $agendamento->getEmail(),
            'vat_no'                => $agendamento->getCpf(),
            'cf5'                   => $agendamento->getCelular(),
            'phone'                 => $agendamento->getCelular(),
            'data_aniversario'      => $agendamento->getDataNascimento(),

            'postal_code'           => '88135432',
            'address'               => 'Rua Giovanni Pisano',
            'numero'                => '221',
            'complemento'           => 'Ap 201 Residencial Monza',
            'bairro'                => 'Aririú',
            'city'                  => 'Palhoça',
            'state'                 => 'SC',
        );

        $customer = $this->companies_model->getVerificaCustomeByCPF($agendamento->getCpf());

        if (count($customer) > 0 && $agendamento->getCpf()) {
            $this->companies_model->updateCompanyWith($customer->id, $data, NULL);
            return $customer->id;
        } else {
            return $this->companies_model->addCompanyWith($data, NULL);
        }
    }

    private function gerarContaReceber($agendamento) {
        //$agendamento = new Agendamento_model();

        $condicaoPagamentoId = 5;//TODO A VISTA VIA CONFIGURACAO
        $movimentador = 6;
        $user = 6;
        $filial = 55;
        $receita = 13;

        $dataContaReceber = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'reference'             => $this->site->getReference('ex'),
            'receita'               => $receita,
            'created_by'            => $user,
            'condicaopagamento'     => $condicaoPagamentoId,
            'warehouse'             => $filial,
            'pessoa'                => $agendamento->getCliente(),
            'tipocobranca'          => $agendamento->getTipoCobranca(),
            'dtvencimento'          => $agendamento->getData(),
            'dtcompetencia'         => $agendamento->getData(),
            'valor'                 => $agendamento->getValor(),
            'note'                  => '',
        );

        $contaReceber = new ContaReceber_model();
        $contaReceber->data = $dataContaReceber;
        $contaReceber->valores = [$agendamento->getValor()];
        $contaReceber->vencimentos = [$agendamento->getData()];
        $contaReceber->cobrancas = [$agendamento->getTipoCobranca()];
        $contaReceber->movimentadores =  [$movimentador];
        $contaReceber->senderHash = $agendamento->getSenderHash();
        $contaReceber->cardName = $agendamento->getCardName();
        $contaReceber->creditCardToken = $agendamento->getCreditCardToken();
        $contaReceber->numeroParcelas = $agendamento->parcelas;
        $contaReceber->descontos = [0];

        $contaReceberId = $this->financeiro_model->adicionarReceita($contaReceber);

        return $contaReceberId;
    }

    public function pagar($contareceberId) {
        $agendamento = $this->buscarAgendamentoByContaReceber($contareceberId);

        if ($agendamento) $this->__edit('agendamento', array('status' => Agendamento_model::STATUS_PAGO), 'id', $agendamento->id);
    }

    public function reabrirPagamentoAgendamento($contareceberId) {
        $agendamento = $this->buscarAgendamentoByContaReceber($contareceberId);

        if ($agendamento) $this->__edit('agendamento', array('status' => Agendamento_model::STATUS_AGUARDANDO_PAGAMENTO), 'id', $agendamento->id);
    }

    function buscarAgendamentoByContaReceber($contareceberId) {

        if (!$this->__existByField('agendamento', 'contaReceber', $contareceberId)) return false;

        return $this->AgendamentoRepository_model->getAgendamentoByContaReceber($contareceberId);
    }

    private function salvarNoCalendario($agendamento) {
        $data_event = array(
            'title' 		=> $agendamento->getNomeCompleto().' - '.$agendamento->getNomeProduto(),
            //'customers' 	=> $id,
            'start'	 		=> $agendamento->getData().' '.$agendamento->getHoraInicio(),
            'end' 			=> $agendamento->getDataAte().' '.$agendamento->getHoraTermino(),
            'description' 	=> $agendamento->getNomeCompleto().'<br/>'.
                ' <b>Ensaio:</b> '.$agendamento->getNomeProduto().'<br/>'.
                ' <b>Celular:</b> '.$agendamento->getCelular().'<br/>'.
                ' <b>E-mail:</b> '.$agendamento->getEmail().'<br/>'.
                ' <b>CPF:</b> '.$agendamento->getCpf().'<br/>',
            'color' 		=>  '#1d7010',//TODO COR DOS DESTINOS VIR DA CATEGORIA
            'user_id' 	    => 5
        );
        return $this->companies_model->addEvent($data_event, true);
    }
}