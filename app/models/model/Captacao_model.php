<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Captacao_model extends CI_Model
{

    //dados
    public $status = 'ABERTO';
    public $reference_no;
    public $date;

    public $biller;
    public $biller_id;

    //interesse
    public $programacao_id;
    public $product_id;

    //dados
    public $name;
    public $plantao_id;
    public $meio_divulgacao_id;
    public $forma_atendimento_id;
    public $interesse_id;

    public $department_id;

    public $customer_id;

    public $cell_phone;
    public $phone = '';
    public $commercial_phone = '';
    public $additional_telephone = '';
    public $email = '';
    public $city = '';
    public $note = '';
    public $url = '';

    public $read = false;

    //log
    public $created_by;//usuario
    public $created_at;
    public $update_at;
    public $update_by;//usuario

    public function __construct() {
        parent::__construct();
    }


}