<?php defined('BASEPATH') or exit('No direct script access allowed');

class Commissions extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->library('form_validation');

        $this->lang->load('commission', $this->Settings->user_language);

        //repository
        $this->load->model('repository/CommissionsRepository_model', 'CommissionsRepository_model');
        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');

        //service
        $this->load->model('service/CommissionsService_model', 'CommissionsService_model');
        $this->load->model('service/FechamentoComissaoService_model', 'FechamentoComissaoService_model');

        //model
        $this->load->model('model/Commission_model', 'Commission_model');

        //dto
        $this->load->model('dto/FechamentoComissaoDTO_model', 'FechamentoComissaoDTO_model');
    }

    function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('commissions')));
        $meta = array('page_title' => lang('commissions'), 'bc' => $bc);

        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaReceita();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('commissions/index', $meta, $this->data);
    }

    public function getPrevissaoComissao()
    {
        $this->load->library('datatables');

        $historico_link = anchor('commissions/historico_commission_item/$1', '<i class="fa fa-archive"></i> ' . lang('historico_commission_item'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_link = anchor('commissions/edit_commission_item/$1', '<i class="fa fa-edit"></i> ' . lang('edit_commission_item'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $cancel_link = anchor('commissions/cancel_commission_item/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancel_commission_item'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');


        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {

            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $historico_link . '</li>
                </ul>
            </div></div>';

        } else {
            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $historico_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $cancel_link . '</li>
                </ul>
            </div></div>';
        }


        $this->datatables
            ->select("commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    commission_items.biller,
                    commission_items.customer,
                    commission_items.base_value,
                    commission_items.commission_percentage,
                    commission_items.commission, 
                    commission_items.sale_id")
            ->from('commission_items')
            ->join('sales', 'sales.id = commission_items.sale_id');

        if ($this->input->post('flStatusCommission')) {
            $this->datatables->where('commission_items.status', $this->input->post('flStatusCommission'));
        } else {
            $this->datatables->where('commission_items.status', 'Pendente');
        }

        if ($this->input->post('programacaoFilter')) {
            $this->datatables->where('commission_items.programacao_id =', $this->input->post('programacaoFilter'));
        }

        if ($this->input->post('flDataVendaDe')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $this->input->post('flDataVendaDe'));
        }

        if ($this->input->post('flDataVendaDeAte')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $this->input->post('flDataVendaDeAte'));
        }

        if ($this->input->post('billerFilter')) {
            $this->datatables->where('commission_items.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('flDataParcelaDe') ||
            $this->input->post('flStatusParcela') ||
            $this->input->post('flTipoCobranca') ||
            $this->input->post('flLiberacaoComissao')) {

            $subqueryf = '(SELECT sma_fatura .sale_id
               FROM sma_fatura WHERE ';

            if ($this->input->post('flStatusParcela')) {

                if ($this->input->post('flStatusParcela') == 'VENCIDA') {
                    $subqueryf .= ' DATEDIFF (sma_fatura.dtVencimento, NOW() ) < 0';
                    $subqueryf .= ' AND sma_fatura.status in ("ABERTA", "PARCIAL")';
                } else {
                    $subqueryf .= ' sma_fatura.status  = "' . $this->input->post('flStatusParcela') . '"';
                }

            } else {
                $subqueryf .= ' sma_fatura.status in ("ABERTA", "PARCIAL", "QUITADA")';
            }

            if ($this->input->post('flDataParcelaDe')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") >= "' . $this->input->post('flDataParcelaDe') . '"';
            }

            if ($this->input->post('flDataParcelaAte')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") <= "' . $this->input->post('flDataParcelaAte') . '"';
            }

            if ($this->input->post('flTipoCobranca')) {
                $subqueryf .= ' AND sma_fatura.tipoCobranca = "' . $this->input->post('flTipoCobranca') . '"';
            }

            if ($this->input->post('flLiberacaoComissao') == 'primeira_parcela_paga') {
                $subqueryf .= ' AND (SELECT COUNT(*) FROM sma_fatura WHERE sma_fatura.sale_id = sma_sales.id AND sma_fatura.status = "QUITADA") > 0';
            }

            $subqueryf .= ')';

            $this->datatables->where("sma_commission_items.sale_id IN $subqueryf");

            if ($this->input->post('flLiberacaoComissao') == 'ultima_parcela_paga') {
                $this->datatables->where(' (sma_sales.grand_total - sma_sales.paid) <= ',0);
            }
        }

        $this->datatables->where("sales.payment_status in ('due','partial','paid') ");

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('commission_items.biller_id', $this->session->userdata('biller_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function approved()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('commissions')));
        $meta = array('page_title' => lang('list_commissions_approved'), 'bc' => $bc);

        $this->data['flStatusCommission'] = 'Aprovada_Parcial';

        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaReceita();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('commissions/approved', $meta, $this->data);
    }

    public function getComissoesAprovadas()
    {
        $this->load->library('datatables');

        $historico_link = anchor('commissions/historico_commission_item_fechamento/$1', '<i class="fa fa-archive"></i> ' . lang('historico_commission_item'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $delete_link_fechamento_id = "<a href='#' class='po' title='<b>" . lang("excluir_commission_fechamento_item") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('commissions/excluir_commission_fechamento_item/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('excluir_commission_fechamento_item') . "</a>";

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {

            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $historico_link . '</li>
                </ul>
            </div></div>';


        } else {
            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $historico_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link_fechamento_id . '</li>
                </ul>
            </div></div>';
        }

        $this->datatables
            ->select("close_commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    close_commission_items.biller,
                    close_commission_items.customer,
                    close_commission_items.base_value,
                    close_commission_items.commission_percentage,
                    close_commission_items.commission,
                    close_commission_items.paid,
                    (sma_close_commission_items.commission - sma_close_commission_items.paid) as balance,
                    close_commission_items.status,
                    close_commission_items.sale_id")
            ->from('close_commission_items')
            ->join('sales', 'sales.id = close_commission_items.sale_id');

        if ($this->input->post('flStatusCommission') == 'Aprovada_Parcial') {
            $this->datatables->where("close_commission_items.status in ('Aprovada','Parcial') ");
        } else if ($this->input->post('flStatusCommission')) {
            $this->datatables->where('close_commission_items.status', $this->input->post('flStatusCommission'));
        }

        if ($this->input->post('programacaoFilter')) {
            $this->datatables->where('close_commission_items.programacao_id =', $this->input->post('programacaoFilter'));
        }

        if ($this->input->post('flDataVendaDe')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $this->input->post('flDataVendaDe'));
        }

        if ($this->input->post('flDataVendaDeAte')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $this->input->post('flDataVendaDeAte'));
        }

        if ($this->input->post('billerFilter')) {
            $this->datatables->where('close_commission_items.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('flDataParcelaDe') ||
            $this->input->post('flStatusParcela') ||
            $this->input->post('flTipoCobranca') ||
            $this->input->post('flLiberacaoComissao')) {

            $subqueryf = '(SELECT sma_fatura .sale_id
               FROM sma_fatura WHERE ';

            if ($this->input->post('flStatusParcela')) {

                if ($this->input->post('flStatusParcela') == 'VENCIDA') {
                    $subqueryf .= ' DATEDIFF (sma_fatura.dtVencimento, NOW() ) < 0';
                    $subqueryf .= ' AND sma_fatura.status in ("ABERTA", "PARCIAL")';
                } else {
                    $subqueryf .= ' sma_fatura.status  = "' . $this->input->post('flStatusParcela') . '"';
                }

            } else {
                $subqueryf .= ' sma_fatura.status in ("ABERTA", "PARCIAL", "QUITADA")';
            }

            if ($this->input->post('flDataParcelaDe')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") >= "' . $this->input->post('flDataParcelaDe') . '"';
            }

            if ($this->input->post('flDataParcelaAte')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") <= "' . $this->input->post('flDataParcelaAte') . '"';
            }

            if ($this->input->post('flTipoCobranca')) {
                $subqueryf .= ' AND sma_fatura.tipoCobranca = "' . $this->input->post('flTipoCobranca') . '"';
            }

            if ($this->input->post('flLiberacaoComissao') == 'primeira_parcela_paga') {
                $subqueryf .= ' AND (SELECT COUNT(*) FROM sma_fatura WHERE sma_fatura.sale_id = sma_sales.id AND sma_fatura.status = "QUITADA") > 0';
            }

            $subqueryf .= ')';

            $this->datatables->where("close_commission_items.sale_id IN $subqueryf");

            if ($this->input->post('flLiberacaoComissao') == 'ultima_parcela_paga') {
                $this->datatables->where(' (sma_sales.grand_total - sma_sales.paid) <= ',0);
            }
        }

        $this->datatables->where("sales.payment_status in ('due','partial','paid') ");

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('close_commission_items.biller_id', $this->session->userdata('biller_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function edit_commission_item($comission_item_id)
    {
        try {

            $this->form_validation->set_rules('new_commission', lang("new_commission"), 'required');
            $this->form_validation->set_rules('note', lang("note"), 'required');

            if ($this->form_validation->run() == true) {

                $note = $this->input->post('note');

                $data = array(
                    'commission' => $this->input->post('new_commission'),
                    'commission_percentage' => $this->input->post('new_commission_percentage'),
                    'updated_by' => $this->session->userdata('user_id'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
            }

            if ($this->form_validation->run() == true && $this->CommissionsService_model->editCommissionItem($data, $note, $comission_item_id)) {
                $this->session->set_flashdata('message', lang("comissison_item_edited"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {

                $this->data['error'] = validation_errors();
                $this->session->set_flashdata('error', validation_errors());
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['item'] = $this->CommissionsRepository_model->getComissionsItemByID($comission_item_id);

                $this->load->view($this->theme . 'commissions/edit_commission_item', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function cancel_commission_item($comission_item_id)
    {
        try {

            $this->form_validation->set_rules('note', lang("note"), 'required');

            if ($this->form_validation->run() == true) {
                $note = $this->input->post('note');
            }

            if ($this->form_validation->run() == true && $this->CommissionsService_model->cancelCommissionItem($note, $comission_item_id)) {
                $this->session->set_flashdata('message', lang("comissison_item_cancelled"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {

                $this->data['error'] = validation_errors();
                $this->session->set_flashdata('error', validation_errors());

                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['item'] = $this->CommissionsRepository_model->getComissionsItemByID($comission_item_id);

                $this->load->view($this->theme . 'commissions/cancel_commission_item', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function historico_commission_item($comission_item_id)
    {
        $this->data['item'] = $this->CommissionsRepository_model->getComissionsItemByID($comission_item_id);
        $this->data['events'] = $this->CommissionsRepository_model->getAllHistoricoComissao($comission_item_id);

        $this->load->view($this->theme . 'commissions/historico_commission_item', $this->data);
    }

    function historico_commission_item_fechamento($comission_item_fechamento_id)
    {

        $itemFechamento = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($comission_item_fechamento_id);

        $this->data['item'] = $this->CommissionsRepository_model->getComissionsItemByID($itemFechamento->commission_item_id);
        $this->data['events'] = $this->CommissionsRepository_model->getAllHistoricoComissao($itemFechamento->commission_item_id);

        $this->load->view($this->theme . 'commissions/historico_commission_item', $this->data);
    }

    public function payment_commissions()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('payment_commissions_billers')));
        $meta = array('page_title' => lang('payment_commissions_billers'), 'bc' => $bc);

        $this->page_construct('commissions/billers', $meta, $this->data);

    }

    function getBillers()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("companies.id as id, 
            companies.company, 
            companies.name, 
            companies.vat_no, 
            companies.phone, 
            companies.email,
            (select sum(cci.commission - cci.paid) as commission from sma_close_commission_items cci where cci.biller_id = sma_companies.id and cci.status in ('Aprovada', 'Parcial') ) as commission")
            ->from("companies")
            ->join('users', 'users.biller_id = companies.id')
            ->where('users.active', 1)
            ->where('companies.is_biller', 1)
            ->where('group_name', 'biller')
            ->add_column("Actions", "<div class=\"text-center\"> <a class=\"tip\" title='" . lang("payment_commission_biller") . "' href='" . site_url('commissions/payment_commission_biller/$1') . "'><i class=\"fa fa-money\"></i></a> </div>", "id");

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('companies.id', $this->session->userdata('biller_id'));
        }

        echo $this->datatables->generate();
    }

    public function payment_commission_biller($biller_id)
    {
        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaDespesa();
        $this->data['biller']       = $this->site->getCompanyByID($biller_id);

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('commissions'), 'page' => lang('commissions')),
            array('link' => '#', 'page' => lang('payment_commission_biller')));
        $meta = array('page_title' => lang('payment_commission_biller'), 'bc' => $bc);

        $this->page_construct('commissions/payment_commission_biller', $meta, $this->data);
    }

    function getPaymentCommissionBiller($billerID) {

        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                {$this->db->dbprefix('fatura')}.product_name,
                {$this->db->dbprefix('fatura')}.strDespesas as despesa,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.valorpago, 
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->join('sales', 'sales.id = fatura.sale_id', 'left')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'DEBITO');

        $this->datatables->where("{$this->db->dbprefix('fatura')}.pessoa", $billerID);
        $this->datatables->where("{$this->db->dbprefix('fatura')}.despesa", $this->Settings->despesa_fechamento_id);

        if ($this->input->post('filterProgramacao')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        }

        if ($this->input->post('start_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$this->input->post('start_date')}' ");
        }

        if ($this->input->post('end_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$this->input->post('end_date')}' ");
        }

        if ($this->input->post('filterTipoCobranca')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        }

        $this->datatables->where("{$this->db->dbprefix('fatura')}.status IN ('ABERTA', 'PARCIAL')");

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function fechamentos()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('commissions')));
        $meta = array('page_title' => lang('fechamento_comissao'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('commissions/fechamentos', $meta, $this->data);
    }

    function getFechamentosComissao()
    {
        $this->load->library('datatables');

        $mount_link     = anchor('commissions/montar_fechamento/$1', '<i class="fa fa-cogs"></i> ' . lang('montar_fechamento'), '');
        $edit_link      = anchor('commissions/edit_fechamento/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fechamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //$report_link_landscape    = anchor('commissions/report/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('report_commissions_landscape'), 'target="_blank"');
        $report_link_portrait    = anchor('commissions/report/$1/0/0/portrait', '<i class="fa fa-file-pdf-o"></i> ' . lang('report_commissions_portrait'), 'target="_blank"');

        $delete_link = "<a href='#' class='po' title='<b>" . lang("cancel_fechamento") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('commissions/cancel_fechamento/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('cancel_fechamento') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $mount_link . '</li>
                    <li class="divider"></li>
                    <li>' . $edit_link . '</li>
                    <li>' . $report_link_portrait . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("close_commissions.id as id,
                    reference_no,
                    dt_competencia as date, 
                    title,
                    total_commission,
                    paid,
                    (total_commission - paid) as balance,
                    status")
            ->from('close_commissions');

        if ($this->input->post('filter_biller')) {
            $this->datatables->join('commission_billers', 'commission_billers.commission_id = close_commissions.id');
            $this->datatables->where('commission_billers.biller_id', $this->input->post('filter_biller'));
            $this->datatables->where('commission_billers.active', true);
        }

        if ($this->input->post('filter_date_de')) {
            $this->datatables->where('close_commissions.dt_competencia>=', $this->input->post('filter_date_de'));
        }

        if ($this->input->post('filter_date_ate')) {
            $this->datatables->where('close_commissions.dt_competencia<=', $this->input->post('filter_date_ate'));
        }

        if ($this->input->post('filter_status')) {
            $this->datatables->where('close_commissions.status', $this->input->post('filter_status'));
        } else {
            $this->datatables->where("close_commissions.status in ('Pendente','Confirmada') ");
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function add_fechamento()
    {
        $this->form_validation->set_rules('dt_competencia', lang("dt_competencia"), 'required');
        $this->form_validation->set_rules('title', lang("title"), 'required');

        if ($this->form_validation->run() == true) {

            $reference  = $this->site->getReference('fc');

            $data = array(
                'reference_no'      => $reference,
                'dt_competencia'    => $this->input->post('dt_competencia'),
                'title'             => $this->input->post('title'),
                'note'              => $this->input->post('note'),
                'total_commission'  => 0.00,
                'created_by'        => $this->session->userdata('user_id'),
                'updated_by'        => $this->session->userdata('user_id'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            );

            $billers = $this->adicionarBillers();

            if (empty($billers)) {
                $this->session->set_flashdata('error', lang('biller_required'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } elseif ($this->input->post('add_fechamento')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == true && $id = $this->FechamentoComissaoService_model->addFechamentoComissao($data, $billers)) {
            $this->session->set_flashdata('message', lang("fechamento_added"));
            redirect('commissions/montar_fechamento/'.$id);
        } else {
            $this->data['error']        = validation_errors();
            $this->session->set_flashdata('error', validation_errors());

            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['billers']      = $this->site->getAllCompanies('biller');

            $this->load->view($this->theme . 'commissions/fechamento_add', $this->data);
        }
    }

    function edit_fechamento($id)
    {
        try {

            $fechamento = $this->FechamentoComissaoRepository_model->getByID($id);

            $this->form_validation->set_rules('dt_competencia', lang("dt_competencia"), 'required');
            $this->form_validation->set_rules('title', lang("title"), 'required');

            if ($this->form_validation->run() == true) {

                $data = array(
                    'dt_competencia'    => $this->input->post('dt_competencia'),
                    'title'             => $this->input->post('title'),
                    'note'              => $this->input->post('note'),
                    'updated_by'        => $this->session->userdata('user_id'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                );

                $billers = $this->adicionarBillers();

                if (empty($billers)) {
                    $this->session->set_flashdata('error', lang('biller_required'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

            } elseif ($this->input->post('edit_fechamento')) {
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->form_validation->run() == true && $id = $this->FechamentoComissaoService_model->editFechamentoComissao($data, $billers, $id)) {
                $this->session->set_flashdata('message', lang("fechamento_editdd"));
                redirect('commissions/montar_fechamento/'.$id);
            } else {
                $this->data['error']        = validation_errors();

                $this->data['modal_js']     = $this->site->modal_js();
                $this->data['billers']      = $this->site->getAllCompanies('biller');
                $this->data['fechamento']   = $fechamento;

                $this->load->view($this->theme . 'commissions/fechamento_edit', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function adicionarBillers(): array
    {
        $a = sizeof($_POST ['biller_id']);

        $billers = array();

        for ($r = 0; $r <= $a; $r++) {
            if (isset ($_POST ['biller_id'] [$r])) {
                $active = $this->adicionarBilleActive($_POST['biller_id'] [$r]);
                if ($active) {
                    $billers [] = array(
                        'active'    => $active,
                        'biller_id' => $_POST['biller_id'] [$r],
                    );
                }
            }
        }

        return $billers;
    }

    private function adicionarBilleActive($billerID): bool
    {
        $a = sizeof($_POST ['biller_id']);
        $active = false;

        for($r = 0; $r <= $a; $r ++) {
            $activeBillerID = $_POST ['ativarBiller'] [$r];

            if ($activeBillerID == $billerID) {
                $active = true;
            }
        }

        return $active;
    }

    public function cancel_fechamento($id)
    {
        try {

            if ($this->FechamentoComissaoService_model->cancel($id)) {

                if ($this->input->is_ajax_request()) {
                    echo lang("Fechamento Cancelado Com Sucesso!");
                    die();
                }

                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) {
                echo  $exception->getMessage();
                die();
            }
        }
    }

    public function modal_view($id = null)
    {

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $fechamento =  $this->FechamentoComissaoRepository_model->getByID($id);

        $this->data['fechamento']       = $fechamento;
        $this->data['created_by']       = $this->site->getUser($fechamento->created_by);
        $this->data['updated_by']       = $fechamento->updated_by ? $this->site->getUser($fechamento->updated_by) : null;
        $this->data['biller']           = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->data['itensFechamento']  = $this->FechamentoComissaoRepository_model->getAllItensFechamento($id);
        $this->data['faturas']          = $this->FechamentoComissaoRepository_model->getFaturasByFechamento($id);
        $this->data['payments']         = $this->FechamentoComissaoRepository_model->getPaymentsByFechamento($id);

        $this->load->view($this->theme . 'commissions/modal_view', $this->data);
    }

    public function report($id = null, $view = null, $save_bufffer = null, $orientation = NULL)
    {
        $fechamento =  $this->FechamentoComissaoRepository_model->getByID($id);

        $name = 'RELATÓRIO DE COMISSÃO ' .$fechamento->title. '.pdf';

        $this->data['fechamento']       = $fechamento;
        $this->data['created_by']       = $this->site->getUser($fechamento->created_by);
        $this->data['updated_by']       = $fechamento->updated_by ? $this->site->getUser($fechamento->updated_by) : null;
        $this->data['biller']           = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->data['itensFechamento']  = $this->FechamentoComissaoRepository_model->getAllItensFechamento($id);
        $this->data['faturas']          = $this->FechamentoComissaoRepository_model->getFaturasByFechamento($id);
        $this->data['payments']         = $this->FechamentoComissaoRepository_model->getPaymentsByFechamento($id, null, true);

        $html = $this->load->view($this->theme . 'commissions/report', $this->data, TRUE);

        if ($view) {
            $this->load->view($this->theme . 'commissions/report', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else if ($orientation == 'portrait') {
            $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
        } else {
            $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
        }
    }

    public function report_biller($id = null, $biller_id = null,  $view = null, $save_bufffer = null)
    {
        $fechamento =  $this->FechamentoComissaoRepository_model->getByID($id);
        $biller = $this->site->getCompanyByID($biller_id);

        $name = 'RELATÓRIO DE COMISSÃO ' .$fechamento->title. ' Vendedor '. $biller->name .'.pdf';

        $this->data['fechamento']       = $fechamento;
        $this->data['created_by']       = $this->site->getUser($fechamento->created_by);
        $this->data['updated_by']       = $fechamento->updated_by ? $this->site->getUser($fechamento->updated_by) : null;
        $this->data['biller']           =  $biller;

        $this->data['itensFechamento']  = $this->FechamentoComissaoRepository_model->getAllItensFechamento($id, $biller_id);
        $this->data['faturas']          = $this->FechamentoComissaoRepository_model->getFaturasByFechamento($id, $biller_id);
        $this->data['payments']         = $this->FechamentoComissaoRepository_model->getPaymentsByFechamento($id, $biller_id, true);

        $html = $this->load->view($this->theme . 'commissions/report_biller', $this->data, TRUE);

        if ($view) {
            $this->load->view($this->theme . 'commissions/report_biller', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
        } else {
            $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Data de Emissão do Documento '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
        }
    }

    public function montar_fechamento($id) {

        $this->data['fechamento']       = $this->FechamentoComissaoRepository_model->getByID($id);
        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['tiposCobranca']    = $this->site->getAllTiposCobrancaReceita();

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('commissions'), 'page' => lang('commissions')),
            array('link' => '#', 'page' => lang('montar_fechamento'))
        );

        $meta = array('page_title' => lang('montar_fechamento'), 'bc' => $bc);
        $this->page_construct('commissions/montar_fechamento', $meta, $this->data);
    }

    public function getComissoesAbertas($id)
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    commission_items.biller,
                    commission_items.customer,
                    commission_items.base_value,
                    commission_items.commission_percentage,
                    commission_items.commission, 
                    commission_items.sale_id")
            ->from('commission_items')
            ->join('sales', 'sales.id = commission_items.sale_id');

        $subquery = '(SELECT sma_commission_billers.biller_id 
                      FROM sma_commission_billers 
                      WHERE sma_commission_billers.commission_id = ' . $id . ' 
                        AND sma_commission_billers.active = true)';

        $this->datatables->where("sma_commission_items.biller_id IN $subquery");

        $this->datatables->where('commission_items.status', 'Pendente');

        if ($this->input->post('programacaoFilter')) {
            $this->datatables->where('commission_items.programacao_id =', $this->input->post('programacaoFilter'));
        }

        if ($this->input->post('flDataVendaDe')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $this->input->post('flDataVendaDe'));
        }

        if ($this->input->post('flDataVendaDeAte')) {
            $this->datatables->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $this->input->post('flDataVendaDeAte'));
        }

        if ($this->input->post('billerFilter')) {
            $this->datatables->where('commission_items.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('flDataParcelaDe') ||
            $this->input->post('flStatusParcela') ||
            $this->input->post('flTipoCobranca') ||
            $this->input->post('flLiberacaoComissao')) {

            $subqueryf = '(SELECT sma_fatura .sale_id
               FROM sma_fatura WHERE ';

            if ($this->input->post('flStatusParcela')) {

                if ($this->input->post('flStatusParcela') == 'VENCIDA') {
                    $subqueryf .= ' DATEDIFF (sma_fatura.dtVencimento, NOW() ) < 0';
                    $subqueryf .= ' AND sma_fatura.status in ("ABERTA", "PARCIAL")';
                } else {
                    $subqueryf .= ' sma_fatura.status  = "' . $this->input->post('flStatusParcela') . '"';
                }

            } else {
                $subqueryf .= ' sma_fatura.status in ("ABERTA", "PARCIAL", "QUITADA")';
            }

            if ($this->input->post('flDataParcelaDe')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") >= "' . $this->input->post('flDataParcelaDe') . '"';
            }

            if ($this->input->post('flDataParcelaAte')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") <= "' . $this->input->post('flDataParcelaAte') . '"';
            }

            if ($this->input->post('flTipoCobranca')) {
                $subqueryf .= ' AND sma_fatura.tipoCobranca = "' . $this->input->post('flTipoCobranca') . '"';
            }

            if ($this->input->post('flLiberacaoComissao') == 'primeira_parcela_paga') {
                $subqueryf .= ' AND (SELECT COUNT(*) FROM sma_fatura WHERE sma_fatura.sale_id = sma_sales.id AND sma_fatura.status = "QUITADA") > 0';
            }

            $subqueryf .= ')';

            $this->datatables->where("sma_commission_items.sale_id IN $subqueryf");


            if ($this->input->post('flLiberacaoComissao') == 'ultima_parcela_paga') {
                $this->datatables->where(' (sma_sales.grand_total - sma_sales.paid) <= ',0);
            }
        }

        $this->datatables->where("sales.payment_status in ('due','partial','paid') ");

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a onclick=\"addItemFechamento('$1');\" style='cursor: pointer;'><i class=\"fa fa-plus\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    public function getComissoesFechamento($id)
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("close_commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    close_commission_items.biller,
                    close_commission_items.customer,
                    close_commission_items.base_value,
                    close_commission_items.commission_percentage,
                    close_commission_items.commission,
                    commission_items.sale_id")
            ->from('close_commission_items')
            ->join('commission_items', 'commission_items.id = close_commission_items.commission_item_id')
            ->join('sales', 'sales.id = commission_items.sale_id');

        $this->datatables->where('close_commission_items.close_commission_id', $id);

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a onclick=\"removeItemFechamento('$1');\" style='cursor: pointer;'><i class=\"fa fa-trash-o\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    public function commissions_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'insert_commissions') {

                    try {

                        $fechamentoID = $this->input->post('fechamento_id');

                        foreach ($_POST['val'] as $id) {
                            $this->addItemFechamento($id, $fechamentoID);
                        }

                        $this->session->set_flashdata('message', lang("commissions_added"));
                        redirect($_SERVER["HTTP_REFERER"]);

                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }

                } elseif ($this->input->post('form_action') == 'delete_commissions') {

                    foreach ($_POST['val'] as $id) {
                        $item = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($id);
                        $this->FechamentoComissaoService_model->removeItemFechamento($item->id, $item->commission_item_id, $item->close_commission_id);
                    }

                    $this->session->set_flashdata('message', lang("commissions_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_commission_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function add_item_fechamento() {
        return $this->addItemFechamento($this->input->get('commission_item_id'), $this->input->get('close_commission_id'));
    }

    public function remove_item_fechamento()
    {
        $item = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($this->input->get('fechamento_item_id'));

        return $this->FechamentoComissaoService_model->removeItemFechamento($item->id, $item->commission_item_id, $item->close_commission_id);
    }

    public function excluir_commission_fechamento_item($item_fechamento_id)
    {
        try {

            $item = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($item_fechamento_id);

            if ($this->FechamentoComissaoService_model->removeItemFechamento($item->id, $item->commission_item_id, $item->close_commission_id)) {
                if ($this->input->is_ajax_request()) {
                    echo lang("item_comissao_fechamento_deleted");
                }
                die();
            } else {
                if ($this->input->is_ajax_request()) {
                    echo lang("item_comissao_fechamento_not_deleted");
                }
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) echo  $exception->getMessage();
            die();
        }
    }

    private function addItemFechamento($commission_item_id, $close_commission_id): bool
    {
        $item = $this->CommissionsRepository_model->getComissionsItemByID($commission_item_id);

        $data_item = array(

            'status'                => 'Em Revisão',
            'commission_item_id'    => $item->id,
            'close_commission_id'   => $close_commission_id,

            'sale_id'               => $item->sale_id,
            'programacao_id'        => $item->programacao_id,

            'product_id'            => $item->product_id,
            'product_name'          => $item->product_name,

            'base_value'            => $item->base_value,
            'commission_percentage' => $item->commission_percentage,
            'commission'            => $item->commission,

            'biller'                => $item->biller,
            'biller_id'             => $item->biller_id,

            'customer'              => $item->customer,
            'customer_id'           => $item->customer_id,

            'created_by'            => $this->session->userdata('user_id'),
            'updated_by'            => $this->session->userdata('user_id'),
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s'),
        );

        return $this->FechamentoComissaoService_model->addItemFechamento($close_commission_id, $item->id, $data_item);
    }

    public function fechar_comissao($id)
    {

        $fechamento = $this->FechamentoComissaoRepository_model->getByID($id);

        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');

        if ($this->form_validation->run() == true) {

            $fechamentoDTO = new FechamentoComissaoDTO_model();

            $fechamentoDTO->dtVencimento            = $this->input->post('dtvencimento');
            $fechamentoDTO->condicao_pagamento_id   = $this->input->post('condicaopagamento');
            $fechamentoDTO->tipo_cobranca_id        = $this->input->post('tipocobranca');
            $fechamentoDTO->note                    = $this->input->post('note', true);

            //lista
            $fechamentoDTO->vencimentos     = $this->input->post('dtVencimentoParcela');
            $fechamentoDTO->tiposCobranca   = $this->input->post('tipocobrancaParcela');
            $fechamentoDTO->movimentadores  = $this->input->post('movimentadorParcela');

        } elseif ($this->input->post('fechar_comissao')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->FechamentoComissaoService_model->fechar($fechamentoDTO, $fechamento->id);
                $this->session->set_flashdata('message', lang("financeiro_conta_pagar_adicionada_com_sucesso"));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['modal_js']             = $this->site->modal_js();

            $this->data['fechamento']           = $fechamento;
            $this->data['tiposCobranca']        = $this->site->getAllTiposCobranca(true, true);
            $this->data['condicoesPagamento']   = $this->site->getAllCondicoesPagamento();
            $this->data['movimentadores']       = $this->site->getAllContas();

            $this->load->view($this->theme . 'commissions/modal_fechamento', $this->data);
        }
    }

    public function save($sale_id)
    {
        $this->CommissionsService_model->save($sale_id);
    }

    public function getParcelasCommission()
    {
        $condicaoPagamentoId = $this->input->get('condicaoPagamentoId');
        $dtvencimento = $this->input->get('dtvencimento');
        $tipoCobrancaId = $this->input->get('tipoCobrancaId');

        $tiposCobranca      = $this->site->getAllTiposCobranca();
        $condicaoPagamento  = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
        $tipoCobranca       = $this->site->getTipoCobrancaById($tipoCobrancaId);
        $movimentadores     = $this->site->getAllContas();
        $totalParcelas      = $condicaoPagamento->parcelas;

        $html = '';

        if ($dtvencimento == null) {
            $dtvencimento = date('Y-m-d');
        } else {
            $dtvencimento = date('Y-m-d', strtotime($dtvencimento));
        }

        $tipo       = $tipoCobranca->tipo;
        $integracoa = $tipoCobranca->integracao;
        $conta      = $tipoCobranca->conta;
        $readOnly   = '';

        if ($tipo == 'carne' ||
            $tipo == 'carne_boleto' ||
            $tipo == 'carne_cartao' ||
            $tipo == 'carne_boleto_cartao' &&
            $integracoa != '') {
            $readOnly = 'readonly';
        }

        for ($i = 0; $i < $totalParcelas; $i++) {

            $html .= '<tr>
                    <td>' . ($i + 1) . '/' . $totalParcelas . '</td>
                    <td><input type="date" ' . $readOnly . ' name="dtVencimentoParcela[]" required="required" class="form-control" value="' . $dtvencimento . '" /></td>
                    <td>
                        <select class="form-control" ' . $readOnly . ' name="tipocobrancaParcela[]" required="required">';

            foreach ($tiposCobranca as $tipoCobranca) {
                if ($tipoCobrancaId == $tipoCobranca->id) {
                    $html .= '      <option selected="selected" value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                }
            }

            $html .= ' </select> 
                      </td>';

            $html .= '<td>
                        <select class="form-control" ' . $readOnly . ' name="movimentadorParcela[]" required="required">';

            foreach ($movimentadores as $movimentador) {
                if ($movimentador->id == $conta) {
                    $html .= '      <option selected="selected" value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                }
            }

            $html .= '    </select>
                    </td>
                </tr>';

            $dtvencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtvencimento)));
        }
        echo $html;
    }

    public function report_commissions_paid()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) {
            $start_date = date('Y-m-d', $data_incio);
        }

        if (!$end_date) {
            $end_date = date('Y-m-d', $data_fim);
        }

        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaDespesa();
        $this->data['planoDespesas'] = $this->site->getAllDespesasSuperiores();
        $this->data['billers']       = $this->site->getAllCompanies('biller');

        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['ano'] = date('Y');
        $this->data['mes'] = date('m');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('commissions'), 'page' => lang('commissions')),
            array('link' => '#', 'page' => lang('report_commissions_paid')));
        $meta = array('page_title' => lang('report_commissions_paid'), 'bc' => $bc);

        $this->page_construct('commissions/report_commissions_paid', $meta, $this->data);
    }

    function getComissoesPagas() {

        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_fatura_link = anchor('faturas/editarFatura/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //<li class="divider"></li>
        //<li>'.$edit_fatura_link.'</li>


        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li class="divider"></li>
                </ul>
            </div></div>';
        } else {
            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';
        }

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                {$this->db->dbprefix('fatura')}.product_name,
                {$this->db->dbprefix('fatura')}.strDespesas as despesa,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.valorpago, 
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'DEBITO');

        $this->datatables->where("{$this->db->dbprefix('fatura')}.despesa", $this->Settings->despesa_fechamento_id);

        if ($this->input->post('start_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$this->input->post('start_date')}' ");
        }

        if ($this->input->post('end_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$this->input->post('end_date')}' ");
        }

        if ($this->input->post('filterTipoCobranca')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        }

        if ($this->input->post('filterProgramacao')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        }

        if ($this->input->post('filterDataPagamentoDe')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento >= '{$this->input->post('filterDataPagamentoDe')}' ");
        }

        if ($this->input->post('filterDataPagamentoAte')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento <= '{$this->input->post('filterDataPagamentoAte')}' ");
        }

        if ($this->input->post('filterBiller') || $this->input->post('filterCustomer')) {

            $this->datatables->join('sales', 'sales.id = fatura.sale_id', 'left');

            if ($this->input->post('filterCustomer')) {

                $this->datatables->join('sale_items', 'sale_items.sale_id = sales.id', 'left');

                $this->datatables->where("{$this->db->dbprefix('sale_items')}.customerClient", $this->input->post('filterCustomer'));

                $this->db->group_by('sales.id');
            }

            if ($this->input->post('filterBiller')) {
                $this->datatables->where("{$this->db->dbprefix('sales')}.biller_id", $this->input->post('filterBiller'));
            }

        }

        if ($this->input->post('filterStatus')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status", $this->input->post('filterStatus'));
        } else {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status IN ('PARCIAL', 'QUITADA')");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('fatura.pessoa', $this->session->userdata('biller_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function commissions_actions_list_approved()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'export_excel') {

                    try {

                        $this->export_commmissions_approved_item_all_actions($_POST['val']);

                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function commissions_actions_list()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'export_excel') {

                    try {

                        $this->export_commmissions_item_all_actions($_POST['val']);

                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function export_commmissions_item_all_actions($commissionsItemID)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('companies'));

        $query  = $this->CommissionsRepository_model->getCommissionsToExportExcel($commissionsItemID);

        $fields = $query->list_fields(); // Obtém os nomes das colunas

        $col = 0;
        foreach ($fields as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, lang($field));
            $col++;
        }

        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }

        foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $filename = 'customers_' . date('Y_m_d_H_i_s') . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    function export_commmissions_approved_item_all_actions($commissionsFechamentoItemID)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('companies'));

        $query  = $this->FechamentoComissaoRepository_model->getCommissionsFechametoItemToExportExcel($commissionsFechamentoItemID);

        $fields = $query->list_fields(); // Obtém os nomes das colunas

        $col = 0;
        foreach ($fields as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, lang($field));
            $col++;
        }

        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }

        foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $filename = 'customers_' . date('Y_m_d_H_i_s') . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    public function commissions_close_actions()
    {

        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'export_excel') {

                    try {

                        $this->export_commmissions_close_all_actions($_POST['val']);

                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function export_commmissions_close_all_actions($commissionsFechamentoID)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('companies'));

        $query  = $this->FechamentoComissaoRepository_model->getCommissionsFechametoToExportExcel($commissionsFechamentoID);

        $fields = $query->list_fields(); // Obtém os nomes das colunas

        $col = 0;
        foreach ($fields as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, lang($field));
            $col++;
        }

        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }

        foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $filename = 'customers_' . date('Y_m_d_H_i_s') . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

}?>