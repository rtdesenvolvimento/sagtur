<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update387 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '3.8.7'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'filtar_local_embarque' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
