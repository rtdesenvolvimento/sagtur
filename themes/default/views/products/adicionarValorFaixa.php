<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_valor_faixa'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("products/adicionarValorFaixa", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('descontarVaga', '1', TRUE, ''); ?>
                <?= lang('descontarVaga', 'descontarVaga'); ?>
            </div>
            <h3><?= lang('captar_dados_link'); ?></h3>
            <div class="form-group">
                <?php echo form_checkbox('captarNomeSocial', '1', FALSE, ''); ?>
                <?= lang('captarNomeSocial', 'captarNomeSocial'); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("formato_captacao_cpf_cnpj", "formato_cpf") ?>
                <?php
                $opts = array(
                    '1' => lang('formato_cpf'),
                    '0' => lang('formato_cnpj'),
                );
                echo form_dropdown('formato_cpf', $opts, 1, 'class="form-control" id="formato_cpf" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarCPF', '1', TRUE, ''); ?>
                <?= lang('captarCPF', 'captarCPF'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('obrigaCPF', '1', TRUE, ''); ?>
                <?= lang('obrigaCPF', 'obrigaCPF'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarDataNascimento', '1', TRUE, ''); ?>
                <?= lang('captarDataNascimento', 'captarDataNascimento'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarDocumento', '1', TRUE, ''); ?>
                <?= lang('captarDocumento', 'captarDocumento'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarOrgaoEmissor', '1', TRUE, ''); ?>
                <?= lang('captarOrgaoEmissor', 'captarOrgaoEmissor'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('obrigaOrgaoEmissor', '1', TRUE, ''); ?>
                <?= lang('obrigaOrgaoEmissor', 'obrigaOrgaoEmissor'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarEmail', '1', TRUE, ''); ?>
                <?= lang('captarEmail', 'captarEmail'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarProfissao', '1', FALSE, ''); ?>
                <?= lang('captarProfissao', 'captarProfissao'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarInformacoesMedicas', '1', FALSE, ''); ?>
                <?= lang('captarInformacoesMedicas', 'captarInformacoesMedicas'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('faixaDependente', '1', FALSE, ''); ?>
                <?= lang('faixaDependente', 'faixaDependente'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captar_observacao', '1', FALSE, ''); ?>
                <?= lang('captar_observacao', 'captar_observacao'); ?>
            </div>
            <div class="form-group">
                <?= lang('placeholder_observacao', 'placeholder_observacao'); ?>
                <?= form_input('placeholder_observacao', '', 'class="form-control" placeholder="Exemplo: Com Quem deseja compartilhar o assento ou Hospedagem." id="placeholder_observacao"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('staff', '1', FALSE, ''); ?>
                <?= lang('staff', 'staff'); ?>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarValorFaixa', lang('adicionar_valor_faixa'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
