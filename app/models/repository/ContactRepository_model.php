<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContactRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function addContact($data)
    {
        if ($this->db->insert("contact_website", $data)) {
            return true;
        }
        return false;
    }

}