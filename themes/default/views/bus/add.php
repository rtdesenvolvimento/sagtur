<style>
    .busMapAppContainer_planta {
        align-items: center;
        display: flex;
        flex-direction: column;
        max-width: 100%;
        width: 100%;
    }
</style>
<link href="<?= $assets ?>styles/bus/css/bus-map-min-v1.css" rel="stylesheet" />
<?php echo form_open("bus/addBus", $attrib = array('data-toggle' => 'validator', 'role' => 'form')); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_bus'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="busMapAppContainer_planta">
                        <div class="busMapForm">
                            <input type="hidden" name="mapa" id="map_all" value="">
                            <div class="col-md-6">
                                <div class="busMapNameContainer form-group">
                                    <label for="name_veiculo">Nome do mapa</label>
                                    <input name="name" type="text" class="form-control" value="" required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="busMapModelContainer form-group">
                                    <label for="modelo">Usar como base o mapa</label>
                                    <select id="modelo" class="modelo form-control" required="required">
                                        <option value="">Selecione</option>
                                        <?php foreach ($automoveis as $automovel) {?>
                                            <option andares="<?=$automovel->andares;?>" value="<?=$automovel->id;?>"><?=$automovel->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 map-view" style="display: none;">
                                <div class="form-group floorsNumberContainer">
                                    <p><b>Quantos andares:</b></p>
                                    <span class="radio-input-label">
                                        <input class="change-floors-button" type="radio" name="numero-andares" value="1" id="andar1" checked="checked">
                                        <label for="one-floor-radio">1 andar</label>
                                    </span>
                                    <span class="radio-input-label">
                                        <input class="change-floors-button" type="radio" name="numero-andares" value="2" id="andar2">
                                        <label for="two-floor-radio">2 andares</label>
                                    </span>

                                </div>
                                <div class="andares" style="display: none;">
                                    <p>Alterne entre os andares:</p>
                                    <button type="button" class="btn btn-primary" id="andar-inferior"> <i class="fa fa-sort-desc"></i> Andar inferior (1° Andar)</button>
                                    <button type="button" class="btn btn-secondary" id="andar-superior"><i class="fa fa-sort-asc"></i>Andar superior (2º Andar)</button>
                                </div>
                            </div>
                            <div class="col-md-12 map-view" style="display: none;">
                                <span class="radio-input-label">
                                    Clique nos assentos para modificar a sua numeração ou habilitar/desabilitar o assento.
                                </span>
                            </div>
                        </div>
                        <div class="busMapApp map-view" style="max-width: 665px;display: none;">
                            <div class="scaffold-bus-container bus-map">
                                <div class="seat-map">
                                    <div class="scaffold-bus">
                                        <ul class="bus-seats superior-floor active" id="mapa"></ul>
                                    </div>
                                    <ul class="seats-subtitles">
                                        <li>
                                            <span class="seat-icon seat-enabled"></span>
                                            <span class="subtitle">Habilitado</span>
                                        </li>
                                        <li>
                                            <span class="seat-icon seat-disabled"></span>
                                            <span class="subtitle">Desabilitado</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12" style="display: none;">
                    <div class="form-group">
                        <?= lang("note", "note"); ?>
                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('addBus', lang('adicionar_bus'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
</div>
<?= form_close(); ?>
<div class="modal" id="axisMapModal" tabindex="-1" aria-labelledby="axisMapModalLabel" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="fecharModal"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabelEditAssento"><?= lang('edit_assento'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Modificar N<sup>o</sup> do Assento:</label>
                    <input type="tel" class="form-control" id="assento">
                </div>
                <div class="form-group" style="display: none;">
                    <?= lang("note", "note"); ?>
                    <?php echo form_textarea('note_assento', '', 'class="form-control skip" id="note_assento" style="margin-top: 10px; height: 100px;"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_radio('activeAssento', '1', TRUE, 'id="activeAssento"'); ?>
                    <label for="attributes" class="padding05"><?= lang('habilitado'); ?></label>
                </div>
                <div class="form-group">
                    <?php echo form_radio('activeAssento', '0', FALSE, 'id="disabledAssento"'); ?>
                    <label for="attributes" class="padding05"><?= lang('desabilitado'); ?></label>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_submit('edit_assento', lang('edit_assento'), 'id="editar_assento" class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
</div>

<?= $modal_js ?>
<script type="text/javascript">

    var assento_selecionado = null;
    var mapa = [];
    var mapa_base = null;

    $(document).ready(function () {
        $('#axisMapModal').on('shown.bs.modal', function (e) {

            let assento_name = e.relatedTarget.getAttribute('data-position-order_name')
            let status = e.relatedTarget.getAttribute('data-status');
            let note = e.relatedTarget.getAttribute('data-note-assento');

            if (status === '1') {
                $('#activeAssento').attr('checked', true);
                $('#disabledAssento').attr('checked', false);
            } else {
                $('#activeAssento').attr('checked', false);
                $('#disabledAssento').attr('checked', true);
            }

            $('input[type="checkbox"],[type="radio"]').iCheck('update');
            assento_selecionado = e.relatedTarget;

            $('#note_assento').val(note);
            $('#assento').val(assento_name);
        });

        $('#editar_assento').click(function (event){

            let assento_sel = $('#assento').val();
            let isAssentoActive = $('#activeAssento').prop('checked');
            let note = $('#note_assento').val();

            assento_selecionado.setAttribute('data-position-order_name', assento_sel);
            assento_selecionado.setAttribute('data-note-assento', note);
            assento_selecionado.querySelector('span').innerHTML = assento_sel;

            if (isAssentoActive) {
                assento_selecionado.classList.add('seat-enabled');
                assento_selecionado.classList.remove('seat-disabled');
                assento_selecionado.setAttribute('data-status', '1');
            } else {
                assento_selecionado.classList.remove('seat-enabled');
                assento_selecionado.classList.add('seat-disabled');
                assento_selecionado.setAttribute('data-status', '0');
            }

            assento_selecionado = null;
            $('#assento').val('');

            montar_mapa();

            $('#axisMapModal').appendTo("body").modal('hide');
        });

        $('#fecharModal').click(function (event){
            $('#axisMapModal').appendTo("body").modal('hide');
        });

        $('#modelo').change(function (event){
            if ($(this).val() !== '') {
                if (mapa_base !== null && mapa_base !== $(this).val()) {
                    if(confirm('Deseja realmente alterar a planta base? Você perderá qualquer edição feita até agora!')) {
                        montar_mapa_base($(this).val());
                    }
                } else {
                    montar_mapa_base($(this).val());
                }
                mapa_base = $(this).val();
            } else {
                $('.map-view').hide();
            }
        });

        $('.change-floors-button').on('ifChecked', function (e) {
            mostar_segundo_andar();
        });
        $('.change-floors-button').on('ifUnchecked', function (e) {
            mostar_segundo_andar();
        });

        $('#andar-inferior').click(function (event){
            habilitar_andar(1);
        });

        $('#andar-superior').click(function (event){
            habilitar_andar(2);
        });
    });

    function habilitar_andar(andar) {

        $('#andar-inferior').removeClass('btn-primary');
        $('#andar-inferior').removeClass('btn-secondary');
        $('#andar-superior').removeClass('btn-primary');
        $('#andar-superior').removeClass('btn-secondary');

        if (andar === 1) {
            $('#andar-inferior').addClass('btn-primary');
            $('#andar-superior').addClass('btn-secondary');
        } else if (andar === 2) {
            $('#andar-inferior').addClass('btn-secondary');
            $('#andar-superior').addClass('btn-primary');
        }

        $('.andar-1').hide();
        $('.andar-2').hide();

        $('.andar-' + andar).show();
    }

    function montar_mapa_base(automovel_id) {
        $.ajax({
            type: "get",
            async: false,
            data: {
                automel_id: automovel_id,
            },
            url: site.base_url + "bus/bus_base",
            dataType: "html",
            success: function (data) {
                $('#mapa').html(data);
                $('.map-view').show(100);

                habilitar_andar(1);
                montar_mapa();

                if ($('#modelo').find(":selected").attr('andares') === '2') {
                    $('#andar1').attr('checked', false);
                    $('#andar2').attr('checked', true);
                } else {
                    $('#andar1').attr('checked', true);
                    $('#andar2').attr('checked', false);
                }

                $('input[type="checkbox"],[type="radio"]').iCheck('update');

                mostar_segundo_andar();
            }
        });
    }
    function mostar_segundo_andar() {
        let andar2 = $('#andar2').prop('checked');

        if (andar2) {
            $('.andares').show();
        } else {
            $('.andares').hide();
        }
    }

    function montar_mapa() {
        mapa = [];

        const lista = $('#mapa');

        lista.find('li').each(function() {

            let assento_order = $(this).attr('data-position-order');
            let assento_name = $(this).attr('data-position-order_name');
            let assento_assento =  $(this).find('span').html();
            let note = $(this).attr('data-note-assento');
            let assento_status =  $(this).attr('data-status');
            let andar =  $(this).attr('data-andar');
            let x =  $(this).attr('data-x');
            let y =  $(this).attr('data-y');
            let z =  $(this).attr('data-z');

            var assento = {
                assento_order : assento_order,
                assento_name: assento_name,
                assento_assento : assento_assento,
                assento_status: assento_status,
                andar: andar,
                x: x,
                y: y,
                z: z,
                assento_note: note,
            };
            mapa.push(assento);
        });

        const mapa_all = JSON.stringify(mapa);

        $('#map_all').val(mapa_all);
    }
</script>