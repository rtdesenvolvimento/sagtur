<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update457 extends CI_Migration {

    public function up() {

        $this->create_commission_item_events();

        $this->db->update('settings',  array('version' => '4.5.7'), array('setting_id' => 1));
    }

    public function create_table_regras_pagamento_comissao() {
        $fields = array(
            'id'   => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('regras_pagamento_comissao', TRUE, $attributes);
    }

    public function create_commission_item_events() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'date' => array('type' => 'timestamp', 'null' => FALSE ),
            'commission_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'commission_item_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE),
            'event' => array('type' => 'LONGTEXT', 'default' => '', 'null' => FALSE),
            'status' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => FALSE),
            'log' => array('type' => 'LONGTEXT', 'default' => '', 'null' => TRUE),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('commission_item_events', TRUE, $attributes);
    }
    public function down() {}
}
