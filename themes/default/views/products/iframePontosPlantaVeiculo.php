<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
    #special {
        width: <?php echo $visao->width;?>px;
        height: <?php echo $visao->height;?>px;
        border:1px ridge #0e90d2;
        background: #0e90d2;
    }
</style>
<body style="text-align: center;">
<h2 id="status2" style="display: none;">0, 0</h2>
<canvas id="special" style="background-image: url('<?php echo base_url().'/plantas/'.$visao->planta; ?>');">Not supported</canvas>
<img id="ocupada" style="display: none;" src="<?php echo base_url();?>themes/default/assets/poltronas_arquivos/images/poltrona-ocupada.png">
<img id="livre" style="display: none;" src="<?php echo base_url();?>themes/default/assets/poltronas_arquivos/images/poltrona-livre.png">
<script>

    var contador = 0;

    jQuery(document).ready(function(){

        $('#special').attr('height', $('#special').css('height'));
        $('#special').attr('width', $('#special').css('width'));

        $("#special").click(function(e){

            if (contador == 0) {

                var x = e.pageX - this.offsetLeft;
                var y = e.pageY - this.offsetTop;
                var onibus      = document.getElementById("special");

                var posicao     = onibus.getContext("2d");
                var poltrona    = onibus.getContext("2d");

                var tipo = window.parent.document.getElementById('tipo').value;
                var name = window.parent.document.getElementById('name').value;

                if (tipo === 'acento') {
                    var img         = document.getElementById("livre");
                    poltrona.drawImage(img, x-10, y-15);
                    posicao.fillStyle="white";
                } else {
                    posicao.fillStyle="black";
                }

                posicao.font="12px sans-serif";
                posicao.fillText( name , x, y);

                window.parent.atribuirXY(x, y);

                $('#status2').html(x + ', ' + y);

                contador = contador + 1;
            } else {
                alert("Só é possível selecionar UM ponto na imagem referente a esse automovel! Caso tenha errado o ponto, cliente no botão Limpar para iniciar o posicionamento.");
            }

        });
        
        $('#posicionamento').blur(function (event) {
            event.preventDefault();
            var id = $(this).val();

            if (id) {
                if (confirm("Deseja realmente excluir a posição?")) {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url();?>products/excluirPosicaoPlanta/" +id,
                        success: function (result) {
                            window.parent.recarregar();
                        }
                    });
                }
            }
        })


        <?php foreach($posicoes as $posicao){ ?>

            var onibus      = document.getElementById("special");

            var posicao     = onibus.getContext("2d");
            var poltrona    = onibus.getContext("2d");
            var img         = document.getElementById("livre");

            <?php if  ($posicao->tipo == 'acento') {?>
                poltrona.drawImage(img, <?php echo $posicao->client_x; ?>,  <?php echo $posicao->client_y; ?>);
                posicao.fillStyle="white";
            <?php } else { ?>
                posicao.fillStyle="black";
            <?php } ?>

            posicao.font="12px sans-serif";
            posicao.fillText('<?php echo $posicao->name; ?>',<?php echo $posicao->client_x + 10; ?>,  <?php echo $posicao->client_y + 15; ?>);
    <?php  }?>
    })
</script>
<br/>
<select id="posicionamento" style="width: <?php echo $visao->width;?>px;">
    <option value="">selecione para excluir</option>
    <?php foreach($posicoes as $posicao){ ?>
        <option value="<?php echo $posicao->id;?>"><?php echo $posicao->name;?></option>
    <?php }?>
</select>
</body>