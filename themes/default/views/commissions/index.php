<style media="screen">

    #TBPrevissaoComissao td:nth-child(1) {text-align: center;width: 2%;}
    #TBPrevissaoComissao td:nth-child(2) {text-align: center;width: 10%;}
    #TBPrevissaoComissao td:nth-child(3) {text-align: center;width: 12%;}
    #TBPrevissaoComissao td:nth-child(6) {text-align: right;width: 10%}
    #TBPrevissaoComissao td:nth-child(7) {text-align: right;width: 5%}
    #TBPrevissaoComissao td:nth-child(8) {text-align: right;width: 10%}
    #TBPrevissaoComissao td:nth-child(9) {display: none;}
    #TBPrevissaoComissao td:nth-child(10) {text-align: center;width: 2%;}

</style>

<script>

    function status(x) {

        if(x == null) {
            return '';
        } else if(x === 'ABERTO') {
            return '<div class="text-center"><span class="label label-success">Aberta</span></div>';
        } else {
            return '<div class="text-center"><span class="label label-warning">x</span></div>';
        }
    }

    function currentFormatDecimal(x, d) {
        return parseFloat(x).toFixed(2) + '%';
    }

    function currencyFormatTotal(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(x)+'</div>';
        } else {
            return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(0)+'</div>';
        }
    }

    $(document).ready(function () {

        $('#TBPrevissaoComissao').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('commissions/getPrevissaoComissao')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[8];
                nRow.className = "invoice_link_fechamento";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });
                aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });
                aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                aoData.push({ "name": "programacaoFilter", "value":  $('#filter_programacao_id').val() });
                aoData.push({ "name": "flDataParcelaDe", "value":  $('#flDataParcelaDe').val() });
                aoData.push({ "name": "flDataParcelaAte", "value":  $('#flDataParcelaAte').val() });
                aoData.push({ "name": "flStatusParcela", "value":  $('#flStatusParcela').val() });
                aoData.push({ "name": "flLiberacaoComissao", "value":  $('#flLiberacaoComissao').val() });
                aoData.push({ "name": "flTipoCobranca", "value":  $('#flTipoCobranca').val() });
                aoData.push({ "name": "flStatusCommission", "value":  $('#flStatusCommission').val() });
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, total_comissao = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                    total_comissao += parseFloat(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');
                var percentual_comissao = (total_comissao / gtotal) * 100;

                nCells[5].innerHTML = currencyFormatTotal(parseFloat(gtotal));
                nCells[6].innerHTML = percentual_comissao.toFixed(2) + '%';
                nCells[7].innerHTML = currencyFormatTotal(parseFloat(total_comissao));
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                {"mRender": fld},
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currentFormatDecimal},
                {"mRender": currencyFormat},
                null,
                {"bSortable": false}
            ],
        });
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('commissions/commissions_actions_list', 'id="action-form"');
}
?>

<div class="row">

    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue">
                    <i class="fa-fw fa fa-cogs"></i><?=lang('list_commissions')?>
                </h2>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                            </a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a href="#" id="excel" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> <?=lang('export_commissions_to_excel')?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                    <div style="margin-bottom: 20px;" class="divfilters">
                        <div class="row">
                            <div  class="col-sm-3">
                                <?= lang("status_comission", "flStatusCommission") ?>
                                <?php
                                $cbStatus = array(
                                    '' => lang('select'),
                                    'Pendente' => lang('pendente'),
                                    'Em Revisão' => lang('em_revisao'),
                                    'Aprovada' => lang('aprovada'),
                                    'Cancelada' => lang('cancelada'),
                                );
                                echo form_dropdown('flStatusCommission', $cbStatus,  '', 'class="form-control" id="flStatusCommission"'); ?>
                            </div>
                            <div class="col-sm-7">
                                <?= lang("product", "product"); ?>
                                <?php
                                $pgs[""] = lang('select').' '.lang('product');
                                echo form_dropdown('filter_programacao_id', $pgs,  '', 'class="form-control" id="filter_programacao_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary" style="width: 100%;margin-top: 25px;" data-toggle="modal" data-target="#filterModal"><i class="fa fa-search"></i> <?=$this->lang->line("filter_product");?></button>
                            </div>

                            <div class="col-sm-3">
                                <?= lang("data_venda_de", "data_venda_de"); ?>
                                <?php echo form_input('flDataVendaDe', '', 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?= lang("data_venda_ate", "data_venda_ate"); ?>
                                <?php echo form_input('flDataVendaAte', '', 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                            </div>
                            <div class="col-sm-6">
                                <?= lang("biller", "billerFilter"); ?>
                                <?php
                                $bl[""] = lang("select") . ' ' . lang("biller") ;
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->name;
                                }
                                echo form_dropdown('flBiller', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?= lang("data_parcela_de", "data_parcela_de"); ?>
                                <?php echo form_input('flDataParcelaDe', '', 'type="date" class="form-control" id="flDataParcelaDe"', 'date'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?= lang("data_venda_ate", "data_venda_ate"); ?>
                                <?php echo form_input('flDataParcelaAte', '', 'type="date" class="form-control" id="flDataParcelaAte"', 'date'); ?>
                            </div>
                            <div  class="col-sm-2">
                                <?= lang("status_parcela", "status_parcela") ?>
                                <?php
                                $cbStatus = array(
                                    '' => lang('select'),
                                    'ABERTA' => lang('aberta'),
                                    'PARCIAL' => lang('parcial'),
                                    'QUITADA' => lang('quitada'),
                                    'VENCIDA' => lang('vencida'),
                                );
                                echo form_dropdown('flStatusParcela', $cbStatus,  '', 'class="form-control" id="flStatusParcela"'); ?>
                            </div>
                            <div  class="col-sm-2">
                                <?= lang("liberacao_comissao", "liberacao_comissao") ?>
                                <?php
                                $cbStatus = array(
                                    '' => lang('select'),
                                    'primeira_parcela_paga' => lang('primeira_parcela_paga'),
                                    'ultima_parcela_paga' => lang('ultima_parcela_paga'),
                                );
                                echo form_dropdown('flLiberacaoComissao', $cbStatus,  '', 'class="form-control" id="flLiberacaoComissao"'); ?>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang("tipo_cobranca", "tipo_cobranca") ?>
                                    <?php
                                    $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('flTipoCobranca', $tcs, '', 'class="form-control" id="flTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="controls">
                                    <input type="button" onclick="limparFormulario();" name="submit_fin" value="<?=lang('clear')?>" class="btn btn-primary input-xs">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="TBPrevissaoComissao" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                <thead>
                                <tr>
                                    <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th><?php echo $this->lang->line("date"); ?></th>
                                    <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                    <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                    <th><?php echo $this->lang->line("base_value"); ?></th>
                                    <th><?php echo $this->lang->line("commission_percentage"); ?></th>
                                    <th><?php echo $this->lang->line("commission"); ?></th>
                                    <th style="display: none;"></th>
                                    <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr><td colspan="10" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                    <th><?php echo $this->lang->line("reference_no"); ?></th>
                                    <th><?php echo $this->lang->line("date"); ?></th>
                                    <th><?php echo $this->lang->line("biller"); ?></th>
                                    <th><?php echo $this->lang->line("customer"); ?></th>
                                    <th style="text-align: right;"><?php echo $this->lang->line("base_value"); ?></th>
                                    <th style="text-align: right;"><?php echo $this->lang->line("commission_percentage"); ?></th>
                                    <th style="text-align: right;"><?php echo $this->lang->line("commission"); ?></th>
                                    <th style="display: none;"></th>
                                    <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php } ?>

<!-- Modal Filter Pacote-->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filtros de Pacote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('filter_status_pacote', $opts,  'Confirmado', 'class="form-control" id="filter_status_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('filter_ano_pacote', $opts,  date('Y'), 'class="form-control" id="filter_ano_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('filter_mes_pacote', $opts,  date('m'), 'class="form-control" id="filter_mes_pacote"'); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button"  id="applyFilters" class="btn btn-primary">Aplicar Filtros</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        createFilter('filters');
        buscarProdutos();

        $('#flDataVendaDe').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flDataVendaAte').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#billerFilter').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flDataParcelaDe').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flDataParcelaAte').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flStatusParcela').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flLiberacaoComissao').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flTipoCobranca').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#filter_programacao_id').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('#flStatusCommission').change(function() {
            $('#TBPrevissaoComissao').dataTable().fnDraw();
        });

        $('body').on('click', '.invoice_link_fechamento td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'salesutil/modal_view/' + $(this).parent('.invoice_link_fechamento').attr('id')});
            $('#myModal').modal('show');
        });

        $('#applyFilters').click(function() {
            buscarProdutos();
        });
    });

    function createFilter(nameClasse) {
        $('.'+nameClasse).click(function() {
            if ($('.div'+nameClasse).is(':visible')) {
                $('.div'+nameClasse).hide(300);
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            } else {
                $('.div'+nameClasse).show(300).fadeIn();
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
            }
        });
    }

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#filter_status_pacote').val(),
                ano: $('#filter_ano_pacote').val(),
                mes: $('#filter_mes_pacote').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {

                $('#filter_programacao_id').empty();
                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#filter_programacao_id').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#filter_programacao_id').append(option);
                });

                $('#filter_programacao_id').select2({minimumResultsForSearch: 7});
                $('#filterModal').modal('hide');
            }
        });
    }

    function limparFormulario() {
        $('#flDataVendaDe').val('');
        $('#flDataVendaAte').val('');
        $('#billerFilter').val('');
        $('#flDataParcelaDe').val('');
        $('#flDataParcelaAte').val('');
        $('#flStatusParcela').val('');
        $('#flLiberacaoComissao').val('');
        $('#flTipoCobranca').val('');
        $('#flStatusCommission').val('');
        $('#filter_programacao_id').val('').select2()

        $('#TBPrevissaoComissao').dataTable().fnDraw();
    }
</script>
