<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->library('form_validation');

        $this->load->model('repository/WelcomeRepository_model', 'WelcomeRepository_model');

        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/signature/PlugsignService_model');

        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');


        $this->load->model('db_model');
        $this->load->model('settings_model');
        
        //$this->output->cache(5);
    }

    public function index()
    {
        if ($this->Settings->version == '2.3') {
            $this->session->set_flashdata('warning', 'Please complete your update by synchronizing your database.');
            redirect('sync');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

        $this->data['valepaySetting'] = $this->settings_model->getValepaySettings();
        $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();
        $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();
        $this->data['asaasSetting'] = $this->settings_model->getAsaasSettings();
        $this->data['plugsignSetting'] = $this->settings_model->getPlugsignSettings();

        $this->data['relatorioVendasPorDespesas'] = $this->WelcomeRepository_model->getRelatorioVendasPorDespesas();

        $this->data['salesFaturadas'] = $this->db_model->getLatestSales('faturada');
        $this->data['salesOrcamentos'] = $this->db_model->getLatestSales('orcamento');
        $this->data['salesListaEspera'] = $this->db_model->getLatestSales('lista_espera');

        $this->data['customers'] = $this->db_model->getLatestCustomers();
        $this->data['suppliers'] = $this->db_model->getLatestSuppliers();

        $this->data['count_unread_captacoes'] = $this->db_model->count_unread_captacoes();

        $this->data['totalRecebimentoMes'] = $this->WelcomeRepository_model->getTotalRecebimentoMes()->totalRecebimentoMes;
        $this->data['totalReceberMes'] = $this->WelcomeRepository_model->getTotalReceberMes()->totalReceberMes;
        $this->data['totalPagamentosMes'] = $this->WelcomeRepository_model->getTotalPagamentoMes()->totalPagamentoMes;
        $this->data['totalPagarMes'] = $this->WelcomeRepository_model->getTotalPagarMes()->totalPagarMes;

        $ano = date('Y');
        $mes = date('m');

        if ($this->Settings->receptive) {
            $dia = date('d');
        }

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->mes = $mes;
        $filter->ano = $ano;
        $filter->dia = $dia;
        $filter->enviar_site = false;

        $this->data['programacoes']  = $this->AgendaViagemService_model->getAllProgramacao($filter);

        $this->data['mesFilter'] = $mes;
        $this->data['anoFilter'] = $ano;
        $this->data['diaFilter'] = $dia;
        $this->data['diaFilterRelatorioVendaHora'] = date('d');
        $this->data['semanaFilterRelatorioVendaHora'] = 'at';

        $fatuaEmAbertoSistema = $this->getCobrancaEmAbertoDoSistema();

        $this->data['faturaEmAbertoSistema']            =  $fatuaEmAbertoSistema;
        $this->data['faturaCobrancaEmAbertoSistema']    =  $this->getCobrancaFatura($fatuaEmAbertoSistema->cobranca);


        $startDate = date('Y-m-d'). ' 00:00:00';
        $endDate = date('Y-m-d') . ' 23:59:59';

        $this->data['relatorioVendasPorHora'] = $this->WelcomeRepository_model->getRelatorioVendasPorHora($startDate, $endDate);



        $hoje = new DateTime(); // Data atual
        $hoje->setISODate((int) $hoje->format("o"), (int) $hoje->format("W")); // Define para a segunda-feira da semana atual

        $startDate = $hoje->format("Y-m-d") . ' 00:00:00'; // Segunda-feira 00:00:00

        $endDate = clone $hoje;
        $endDate->modify("+6 days"); // Define para domingo
        $endDate = $endDate->format("Y-m-d") . ' 23:59:59'; // Domingo 23:59:59

        $this->data['relatorioVendasPorDiaSemana'] = $this->WelcomeRepository_model->getRelatorioVendasPorDiaSemana($startDate, $endDate);

        $bc = array(array('link' => '#', 'page' => lang('dashboard')));
        $meta = array('page_title' => lang('dashboard'), 'bc' => $bc);

        $this->page_construct('dashboard', $meta, $this->data);
    }

    function saldoAsaas() {

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $consultaSaldo = $this->CobrancaService_model->consultaSaldoAsaas();

        if ($consultaSaldo) {
            $this->sma->send_json(array('retorno' => true, 'saldo' => $this->sma->formatMoney($consultaSaldo->balance)));
        }

        return  $this->sma->send_json([]);
    }

    function consultar_webhook_cobranca_asaas() {

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $consultaSaldo = $this->CobrancaService_model->consultar_webhook_cobranca_asaas();

        if ($consultaSaldo) {
            $this->sma->send_json($consultaSaldo);
        }

        return  $this->sma->send_json([]);
    }

    function habilitar_webhook_asaas()
    {
        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $consultaSaldo = $this->CobrancaService_model->habilitar_webhook_asaas();

        if ($consultaSaldo) {
            $this->sma->send_json($consultaSaldo);
        }

        return  $this->sma->send_json([]);

    }

    public function getCobrancaEmAbertoDoSistema()
    {

        $db = $this->load->database('sagtur_administrativo', TRUE);

        $db->select('DATEDIFF(min(sma_fatura.dtvencimento), NOW() ) as  quantidade_dias_vencimento, min(sma_fatura.dtvencimento) as dtVencimento, min(sma_fatura_cobranca.id) as cobranca', false);
        $db->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left');
        $db->join('companies', 'companies.id = fatura.pessoa');

        $db->where("companies.vat_no", $this->formatar_cpf_cnpj($this->getCnpj_origem()));
        $db->where('fatura.status', 'ABERTA');

        $q = $db->get('fatura', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCobrancaFatura($id) {
        $db = $this->load->database('sagtur_administrativo', TRUE);
        $q = $db->get_where('fatura_cobranca', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function getCnpj_origem() {
        $cnpj_origem = $this->session->userdata('cnpj_origem');
        return $cnpj_origem;
    }

    private function formatar_cpf_cnpj($doc) {

        $doc = preg_replace("/[^0-9]/", "", $doc);
        $doc = str_replace("-", "", $doc);
        $doc = str_replace(".", "", $doc);
        $doc = trim($doc);

        $qtd = strlen($doc);

        if($qtd >= 11) {

            if($qtd === 11 ) {

                $docFormatado = substr($doc, 0, 3) . '.' .
                    substr($doc, 3, 3) . '.' .
                    substr($doc, 6, 3) . '-' .
                    substr($doc, 9, 2);
            } else {
                $docFormatado = substr($doc, 0, 2) . '.' .
                    substr($doc, 2, 3) . '.' .
                    substr($doc, 5, 3) . '/' .
                    substr($doc, 8, 4) . '-' .
                    substr($doc, -2);
            }

            return $docFormatado;

        } else {
            return $doc;
        }
    }

    function buscarPainelVagas() {

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->ocultarViagensPassadas = false;

        $mes = $this->input->get('mes') == null && $this->input->get('mes') != 'PROXIMAS' ? date('m') : ($this->input->get('mes') != 'all' ? $this->input->get('mes') : null);
        $ano = $this->input->get('ano') != null ? $this->input->get('ano')  : date('Y');
        $dia = $this->input->get('dia') != null ? $this->input->get('dia')  : null;

        if ($mes == 'PROXIMAS') {
            $filter->ocultarViagensPassadas = true;
            $mes = null;
        }

        $filter->ano = $ano;
        $filter->mes = $mes;
        $filter->dia = $dia;

        $filter->verificarConfiguracaoOcultarProdutosSemEstoque = false;
        $filter->enviar_site = false;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

        foreach ($programacoes as $programacao) {
            $programacao->faturadas  = $programacao->getTotalVendasFaturas();
            $programacao->disponivel = $programacao->getTotalDisponvel();
            $programacao->nome = $programacao->name.'<br/>'.$this->sma->dataDeHojePorExtensoRetorno($programacao->getDataSaida());
        }

        $this->sma->send_json($programacoes);
    }

    public function buscarVendasPorHora()
    {

        $ano = $this->input->get('ano') != null ? $this->input->get('ano')  : date('Y');
        $mes = $this->input->get('mes') != null ? $this->input->get('mes') : date('m');
        $dia = $this->input->get('dia') != null ? $this->input->get('dia')  : null;

        if ($dia === null) {
            $startDate = $ano . '-' . str_pad($mes, 2, '0', STR_PAD_LEFT) . '-01 00:00:00'; // Primeiro dia do mês
            $endDate = date('Y-m-t', strtotime($ano . '-' . str_pad($mes, 2, '0', STR_PAD_LEFT))) . ' 23:59:59'; // Último dia do mês
        } else {
            $startDate = $ano . '-' . str_pad($mes, 2, '0', STR_PAD_LEFT) . '-' . str_pad($dia, 2, '0', STR_PAD_LEFT) . ' 00:00:00';
            $endDate = $ano . '-' . str_pad($mes, 2, '0', STR_PAD_LEFT) . '-' . str_pad($dia, 2, '0', STR_PAD_LEFT) . ' 23:59:59';
        }

        $relatorioVendasPorHora = $this->WelcomeRepository_model->getRelatorioVendasPorHora($startDate, $endDate);

        $hours = $relatorioVendasPorHora['hours'];
        $hourlySales = $relatorioVendasPorHora['hourlySales'];

        foreach ($hourlySales as $year => $months) {
            foreach ($months as $month => $days) {
                foreach ($days as $day => $hours) {
                    foreach ($hours as $hour => $sales) {
                        // Criar a label como "YYYY-MM-DD HH:00"
                        $formattedLabels[] = $year . '/' . str_pad($month, 2, '0', STR_PAD_LEFT) . '/' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' ' . str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00';
                        $formattedSales[] = $sales;
                    }
                }
            }
        }

        $data = array(
            'labels' => $formattedLabels,
            'sales' => $formattedSales
        );

        return $this->sma->send_json($data);

    }

    public function buscarVendasPorSemana()
    {
        $ano = $this->input->get('ano') ?? date('Y');
        $mes = $this->input->get('mes') ?? date('m');
        $semana = $this->input->get('semana') ?? null;

        if ($semana !== null && $semana >= 1 && $semana <= 4) {
            // Calcula o primeiro dia do intervalo
            $startDay = (($semana - 1) * 7) + 1; // Ex: Semana 1: 1, Semana 2: 8, etc.
            $dataInicioSemana = new DateTime("$ano-$mes-$startDay");
            $dataFimSemana = clone $dataInicioSemana;
            $dataFimSemana->modify("+6 days");

            // Ajusta a data fim se ultrapassar o último dia do mês
            $ultimoDiaMes = new DateTime("$ano-$mes-" . date('t', strtotime("$ano-$mes-01")));
            if ($dataFimSemana > $ultimoDiaMes) {
                $dataFimSemana = $ultimoDiaMes;
            }

            // Formata as datas com horário
            $startDate = $dataInicioSemana->format('Y-m-d') . ' 00:00:00';
            $endDate   = $dataFimSemana->format('Y-m-d') . ' 23:59:59';
        } else {
            // Se não tiver filtro de semana, pega o mês inteiro
            $startDate = "$ano-$mes-01 00:00:00";
            $endDate   = date("Y-m-t", strtotime($startDate)) . ' 23:59:59';
        }

        $relatorioVendasPorHora = $this->WelcomeRepository_model->getRelatorioVendasPorDiaSemana($startDate, $endDate);

        return $this->sma->send_json($relatorioVendasPorHora);
    }

    function saldoJuno() {

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $junoSetting = $this->settings_model->getJunoSettings();

        if (!$junoSetting->active) return null;

        $consultaSaldo = $this->CobrancaService_model->consultaSaldo();

        if ($consultaSaldo != null) {
            $this->data['junoDisponivel'] = json_decode($consultaSaldo)->data->transferableBalance;
            $this->data['junoLiberar'] = json_decode($consultaSaldo)->data->withheldBalance;
            $this->data['junoTotal'] = json_decode($consultaSaldo)->data->balance;
        }
    }

    function promotions()
    {
        $this->load->view($this->theme . 'promotions', $this->data);
    }

    function image_upload()
    {
        if (DEMO) {
            $error = array('error' => $this->lang->line('disabled_in_demo'));
            $this->sma->send_json($error);
            exit;
        }
        $this->security->csrf_verify();
        if (isset($_FILES['file'])) {
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '500';
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $error = array('error' => $error);
                $this->sma->send_json($error);
                exit;
            }
            $photo = $this->upload->file_name;
            $array = array(
                'filelink' => base_url() . 'assets/uploads/images/' . $photo
            );
            echo stripslashes(json_encode($array));
            exit;

        } else {
            $error = array('error' => 'No file selected to upload!');
            $this->sma->send_json($error);
            exit;
        }
    }

    function set_data($ud, $value)
    {
        $this->session->set_userdata($ud, $value);
        echo true;
    }

    function hideNotification($id = NULL)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    function language($lang = false)
    {
        if ($this->input->get('lang')) {
            $lang = $this->input->get('lang');
        }
        //$this->load->helper('cookie');
        $folder = 'sma/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            $cookie = array(
                'name' => 'language',
                'value' => $lang,
                'expire' => '31536000',
                'prefix' => 'sma_',
                'secure' => false
            );
            $this->input->set_cookie($cookie);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function toggle_rtl()
    {
        $cookie = array(
            'name' => 'rtl_support',
            'value' => $this->Settings->user_rtl == 1 ? 0 : 1,
            'expire' => '31536000',
            'prefix' => 'sma_',
            'secure' => false
        );
        $this->input->set_cookie($cookie);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function download($file)
    {
        $this->load->helper('download');
        force_download('./assets/uploads/'.$file, NULL);
        exit();
    }

    public function slug()
    {
        echo $this->sma->slug($this->input->get('title', true));
        exit();
    }

}
