<script type="text/javascript">

    var count = 1,
        an = 1,
        product_variant = 0,
        DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0,
        invoice_tax = 0,
        product_discount = 0,
        order_discount = 0,
        total_discount = 0,
        total = 0,
        allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;

    var audio_success = new Audio('<?=$assets?>sounds/sound2.mp3');
    var audio_error = new Audio('<?=$assets?>sounds/sound3.mp3');
    
	$(document).ready(function () {

        if (localStorage.getItem('remove_slls')) {

            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }

            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }

            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }

            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }

            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }

            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }

            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }

            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }

            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }

            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }

            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }

            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }

            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }

            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }

            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }

            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }


            localStorage.removeItem('previsao_pagamento');
            localStorage.removeItem('valor');
            localStorage.removeItem('tipoCobrancaId');
            localStorage.removeItem('condicaopagamentoId');

            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }

            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }

            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }

            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }

            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }

            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }

            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }

            if (localStorage.getItem('payment_note_1')) {
                localStorage.removeItem('payment_note_1');
            }

            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }

            localStorage.removeItem('remove_slls');
        }

        <?php if($quote_id) { ?>
			localStorage.setItem('sldate', '<?= $this->sma->hrld($quote->date) ?>');
			localStorage.setItem('slcustomer', '<?= $quote->customer_id ?>');
			localStorage.setItem('slbiller', '<?= $quote->biller_id ?>');
			localStorage.setItem('slwarehouse', '<?= $quote->warehouse_id ?>');
			localStorage.setItem('slnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($quote->note)); ?>');
			localStorage.setItem('sldiscount', '<?= $quote->order_discount_id ?>');
			localStorage.setItem('sltax2', '<?= $quote->order_tax_id ?>');
			localStorage.setItem('slshipping', '<?= $quote->shipping ?>');
			localStorage.setItem('slitems', JSON.stringify(<?= $quote_items; ?>));
        <?php } ?>
		
        <?php if($this->input->get('customer')) { ?>
			if (!localStorage.getItem('slitems')) {
			    localStorage.setItem('slcustomer', <?=$this->input->get('customer');?>);
			}
        <?php } ?>
		
        <?php if ($Owner || $Admin) { ?>

			if (!localStorage.getItem('sldate')) {
				$("#sldate").datetimepicker({
					format: site.dateFormats.js_ldate,
					fontAwesome: true,
					language: 'sma',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					forceParse: 0
				}).datetimepicker('update', new Date());
			}

			$(document).on('change', '#sldate', function (e) {
				localStorage.setItem('sldate', $(this).val());
			});

            $(document).on('change', '#slbiller', function (e) {
                localStorage.setItem('slbiller', $(this).val());
            });

			if (sldate = localStorage.getItem('sldate')) {
				$('#sldate').val(sldate);
			}

			if (slbiller = localStorage.getItem('slbiller')) {
				$('#slbiller').val(slbiller);
			}
        <?php } ?>
		
        if (!localStorage.getItem('slref')) {
            localStorage.setItem('slref', '<?=$slnumber?>');
        }

        if (!localStorage.getItem('sltax2')) {
            localStorage.setItem('sltax2', <?=$Settings->default_tax_rate2;?>);
        }

        ItemnTotals();

        $('.bootbox').on('hidden.bs.modal', function (e) {
            $('#add_item').focus();
        });

        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#slcustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= site_url('sales/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#slwarehouse").val(),
                        customer_id: $("#slcustomer").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    //var row = add_invoice_item(ui.item);
                    var row = controllerSalesPacote.adicionarNovoItem(ui.item);

                    if (row) $(this).val('');

                } else {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#slcustomer').val()) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            $('#gc_details').html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });

        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });
    });
</script>

<div class="box">
    <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('header.label.venda.pacotes.servicos.turisticos'); ?></h2></div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id=' => 'formSales');
                echo form_open_multipart("sales/add", $attrib);

                if ($quote_id) echo form_hidden('quote_id', $quote_id); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("warehouse", "slwarehouse"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $warehouse_input = array(
                                'type' => 'hidden',
                                'name' => 'warehouse',
                                'id' => 'slwarehouse',
                                'value' => $this->session->userdata('warehouse_id'),
                            );

                            echo form_input($warehouse_input);
                            ?>
                        <?php } ?>

                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "sldate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>
						
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "slbiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="slbiller" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'slbiller',
                                'value' => $this->session->userdata('biller_id'),
                            );
                            echo form_input($biller_input);
                        } ?>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("sale_status", "slsale_status"); ?>
                                        <?php $sst = array('completed' => lang('completed'), 'pending' => lang('pending'), 'lista_espera' => lang('lista_espera'));
                                        echo form_dropdown('sale_status', $sst, '', 'class="form-control input-tip" required="required" id="slsale_status"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <?= lang("customer", "slcustomer"); ?>
                                        <div class="input-group">
                                            <?php
                                            echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                            ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-pencil" style="font-size: 1.2em;"></i>
                                            </div>
                                            <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                <a href="#" id="view-customer" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <a href="<?= site_url('customers/add'); ?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-plus-circle" id="addIcon"  style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <?php } ?>

                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <a href="#" id="addNovoItem" class="tip" title="<?= lang('Adiconar novo item') ?>">
                                                    <i class="fa fa-plus addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;display: none;">
                                                <a href="#" id="addHotelManually" class="tip" title="<?= lang('add_hotel_manually') ?>">
                                                    <i class="fa fa-bed addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="sticker" style="display: none;">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                     </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <style>
                                    .total_itens_contador {
                                        background: #fff;
                                        color: #3fb29d;
                                        padding: 0 7px;
                                        min-width: 9px;
                                        border-radius: 12px;
                                        font-weight: 700;
                                        border-bottom: 1px solid #dbdee0;
                                    }
                                </style>
                                <li class=""><a href="#meus-pacotes" class="tab-grey" style="text-align: center;"><i class="fa fa-briefcase" style="font-size: 20px;"></i><span class="number blightOrange black total_itens_contador"><span id="total_meus_pacotes">0</span></span><br/> Pacotes / Passeios e Serviços Turísticos</a></li>

                                <li class="" style="display: none;"><a href="#passeios" class="tab-grey" style="text-align: center;"><i class="fa fa-map-signs" style="font-size: 20px;"></i><br/> Passeios</a></li>
                                <li class="" style="display: none;"><a href="#hospedagem" class="tab-grey" style="text-align: center;"><i class="fa fa-bed" style="font-size: 20px;"></i><br/> Hospedagem</a></li>
                                <li class="" style="display: none;"><a href="#passagem-aerea" class="tab-grey" style="text-align: center;"><i class="fa fa-plane" style="font-size: 20px;"></i><br/> Passagens Áereas</a></li>
                                <li class="" style="display: none;"><a href="#transfer" class="tab-grey" style="text-align: center;"><i class="fa fa-exchange" style="font-size: 20px;"></i><br/> Transfer</a></li>
                                <li class="" style="display: none;"><a href="#cruzeiro" class="tab-grey" style="text-align: center;"><i class="fa fa-ship" style="font-size: 20px;"></i><br/> Cruzeiro</a></li>
                                <li class="" style="display: none;"><a href="#aluguel-veiculo" class="tab-grey" style="text-align: center;"><i class="fa fa-car" style="font-size: 20px;"></i><br/> Aluguel de Veículo</a></li>
                                <li class="" style="display: none;"><a href="#seguro-viagem" class="tab-grey" style="text-align: center;"><i class="fa fa-ambulance" style="font-size: 20px;"></i><br/> Seguro Viagem</a></li>
                                <li class="" style="display: none;"><a href="#outros-servicos" class="tab-grey" style="text-align: center;"><i class="fa fa-heart" style="font-size: 20px;"></i><br/> Outros serviços</a></li>
                            </ul>
                            <div class="tab-content">



                                <!-- ################# !-->
                                <!-- ## CRUZEIRO # !-->
                                <!-- ################# !-->
                                <div id="cruzeiro" class="tab-pane fade in">
                                    <div class="box">
                                        <div class="box-header">
                                            <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i><?= lang('header.label.adicionar.cruzeiro'); ?></h2>
                                            <div class="box-icon">
                                                <ul class="btn-tasks">
                                                    <li class="dropdown">
                                                        <a id="btn-exibir-meu-pacote" style="cursor: pointer;" class="toggle_up tip" title="<?= lang('adicinar_novo_pacote') ?>">
                                                            <i class="icon fa fa-plus" style="font-size: 25px;"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12" id="form-meu-pacote#">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("navio", "slNavio"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-ship" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('navio', (isset($_POST['navio']) ? $_POST['navio'] : ""), 'class="form-control" id="slNavio"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("emissaoCruzeiro", "slEmissaoCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('emissaoCruzeiro', (isset($_POST['emissaoCruzeiro']) ? $_POST['emissaoCruzeiro'] : ""), 'class="form-control input-tip date" id="slEmissaoCruzeiro"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckinCruzeiro", "slDateCheckinCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckinCruzeiro', (isset($_POST['dateCheckinCruzeiro']) ? $_POST['dateCheckinCruzeiro'] : ""), 'class="form-control input-tip date" id="slDateCheckinCruzeiro"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckoutCruzeiro", "slDateCheckoutCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckoutCruzeiro', (isset($_POST['dateCheckoutCruzeiro']) ? $_POST['dateCheckoutCruzeiro'] : ""), 'class="form-control input-tip date" id="slDateCheckoutCruzeiro"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("reservaCruzeiro", "slReservaCruzeiro"); ?>
                                                                <?php echo form_input('reservaCruzeiro', (isset($_POST['reservaCruzeiro']) ? $_POST['reservaCruzeiro'] : ""), 'class="form-control" id="slReservaCruzeiro"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("numeroCabineCruzeiro", "slNumeroCabineCruzeiro"); ?>
                                                                <?php echo form_input('numeroCabineCruzeiro', (isset($_POST['numeroCabineCruzeiro']) ? $_POST['numeroCabineCruzeiro'] : ""), 'class="form-control" id="slNumeroCabineCruzeiro"'); ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("acomodacaoCruzeiro", "slAcomodacaoCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbAcomodcao[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('acomodacaoCruzeiro', $cbAcomodcao, (isset($_POST['acomodacaoCruzeiro']) ? $_POST['acomodacaoCruzeiro'] : ''), 'id="slAcomodacaoCruzeiro" data-placeholder="' . lang("select") . ' ' . lang("acomodacao") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("categoriaAcomodacaoCruzeiro", "slCategoriaAcomodacaoCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('categoriaAcomodacaoCruzeiro', $cbCategoria, (isset($_POST['categoriaAcomodacaoCruzeiro']) ? $_POST['categoriaAcomodacaoCruzeiro'] : ''), 'id="slCategoriaAcomodacaoCruzeiro" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("acomodacaoRegimeCategoriaCruzeiro", "slAcomodacaoRegimeCategoriaCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('acomodacaoRegimeCategoriaCruzeiro', $cbCategoria, (isset($_POST['acomodacaoRegimeCategoriaCruzeiro']) ? $_POST['acomodacaoRegimeCategoriaCruzeiro'] : ''), 'id="slAcomodacaoRegimeCategoriaCruzeiro" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang("destinoCruzeiro", "slDestinoCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('destinoCruzeiro', $cbCategoria, (isset($_POST['destinoCruzeiro']) ? $_POST['destinoCruzeiro'] : ''), 'id="slDestinoCruzeiro" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang("supplierCruzeiro", "slSupplierCruzeiro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-building-o" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('supplierCruzeiro', (isset($_POST['supplierCruzeiro']) ? $_POST['supplierCruzeiro'] : ""), 'class="form-control" id="slSupplierCruzeiro"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ####################### !-->
                                <!-- ## ALUGUEL DE VEICULO # !-->
                                <!-- ####################### !-->
                                <div id="aluguel-veiculo" class="tab-pane fade in">
                                    <div class="box">
                                        <div class="box-header">
                                            <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i><?= lang('header.label.cadastro.aluguel.veiculo'); ?></h2>
                                            <div class="box-icon">
                                                <ul class="btn-tasks">
                                                    <li class="dropdown">
                                                        <a id="btn-exibir-meu-pacote" style="cursor: pointer;" class="toggle_up tip" title="<?= lang('adicinar_novo_pacote') ?>">
                                                            <i class="icon fa fa-plus" style="font-size: 25px;"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12" id="form-meu-pacote#">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("categoriaVeiculo", "slCategoriaVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('categoriaVeiculo', $cbCategoria, (isset($_POST['categoriaVeiculo']) ? $_POST['categoriaVeiculo'] : ''), 'id="slCategoriaVeiculo" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("emissaoAluguelVeiculo", "slEmissaoAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('emissaoAluguelVeiculo', (isset($_POST['emissaoAluguelVeiculo']) ? $_POST['emissaoAluguelVeiculo'] : ""), 'class="form-control input-tip date" id="slEmissaoAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("reservaAluguelVeiculo", "slReservaAluguelVeiculo"); ?>
                                                                <?php echo form_input('reservaAluguelVeiculo', (isset($_POST['reservaAluguelVeiculo']) ? $_POST['reservaAluguelVeiculo'] : ""), 'class="form-control" id="slReservaAluguelVeiculo"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("localRetiradaAluguelVeiculo", "slLocalRetiradaAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('localRetiradaAluguelVeiculo', $cbCategoria, (isset($_POST['localRetiradaAluguelVeiculo']) ? $_POST['localRetiradaAluguelVeiculo'] : ''), 'id="slLocalRetiradaAluguelVeiculo" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dataRetiradaAluguelVeiculo", "slDataRetiradaAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dataRetiradaAluguelVeiculo', (isset($_POST['dataRetiradaAluguelVeiculo']) ? $_POST['dataRetiradaAluguelVeiculo'] : ""), 'class="form-control input-tip date" id="slDataRetiradaAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("horaRetiradaAluguelVeiculo", "slHoraRetiradaAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('horaRetiradaAluguelVeiculo', (isset($_POST['horaRetiradaAluguelVeiculo']) ? $_POST['horaRetiradaAluguelVeiculo'] : ""), 'class="form-control input-tip time" id="slHoraRetiradaAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("localDevolucaoAluguelVeiculo", "slLocalDevolucaoAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('localDevolucaoAluguelVeiculo', $cbCategoria, (isset($_POST['localDevolucaoAluguelVeiculo']) ? $_POST['localDevolucaoAluguelVeiculo'] : ''), 'id="slLocalDevolucaoAluguelVeiculo" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dataDevolucaoAluguelVeiculo", "slDataDevolucaoAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dataDevolucaoAluguelVeiculo', (isset($_POST['dataDevolucaoAluguelVeiculo']) ? $_POST['dataDevolucaoAluguelVeiculo'] : ""), 'class="form-control input-tip date" id="slDataDevolucaoAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("horaDevolucaoAluguelVeiculo", "slHoraDevolucaoAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('horaDevolucaoAluguelVeiculo', (isset($_POST['horaDevolucaoAluguelVeiculo']) ? $_POST['horaDevolucaoAluguelVeiculo'] : ""), 'class="form-control input-tip time" id="slHoraDevolucaoAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("supplierAluguelVeiculo", "slSupplierAluguelVeiculo"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-building-o" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('supplierAluguelVeiculo', (isset($_POST['supplierAluguelVeiculo']) ? $_POST['supplierAluguelVeiculo'] : ""), 'class="form-control" id="slSupplierAluguelVeiculo"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ####################### !-->
                                <!-- #### SEGURO VIAGEM #### !-->
                                <!-- ####################### !-->
                                <div id="seguro-viagem" class="tab-pane fade in">
                                    <div class="box">
                                        <div class="box-header">
                                            <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i><?= lang('header.label.cadastro.seguro.viagem'); ?></h2>
                                            <div class="box-icon">
                                                <ul class="btn-tasks">
                                                    <li class="dropdown">
                                                        <a id="btn-exibir-meu-pacote" style="cursor: pointer;" class="toggle_up tip" title="<?= lang('adicinar_novo_pacote') ?>">
                                                            <i class="icon fa fa-plus" style="font-size: 25px;"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12" id="form-meu-pacote#">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("nomeSeguro", "slNomeSeguro"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-map" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('nomeSeguro', (isset($_POST['nomeSeguro']) ? $_POST['nomeSeguro'] : ""), 'class="form-control" id="slNomeSeguro"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("emissaoSeguroViagem", "slEmissaoSeguroViagem"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('emissaoSeguroViagem', (isset($_POST['emissaoSeguroViagem']) ? $_POST['emissaoSeguroViagem'] : ""), 'class="form-control input-tip date" id="slEmissaoSeguroViagem"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckinSeguroViagem", "slDateCheckinSeguroViagem"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckinSeguroViagem', (isset($_POST['dateCheckinSeguroViagem']) ? $_POST['dateCheckinSeguroViagem'] : ""), 'class="form-control input-tip date" id="slDateCheckinSeguroViagem"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckoutSeguroViagem", "slDateCheckoutSeguroViagem"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckoutSeguroViagem', (isset($_POST['dateCheckoutSeguroViagem']) ? $_POST['dateCheckoutSeguroViagem'] : ""), 'class="form-control input-tip date" id="slDateCheckoutSeguroViagem"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("reservaSeguroViagem", "slReservaSeguroViagem"); ?>
                                                                <?php echo form_input('reservaSeguroViagem', (isset($_POST['reservaSeguroViagem']) ? $_POST['reservaSeguroViagem'] : ""), 'class="form-control" id="slReservaSeguroViagem"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <?= lang("supplierSeguroViagem", "slSupplierSeguroViagem"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-building-o" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('supplierSeguroViagem', (isset($_POST['supplierSeguroViagem']) ? $_POST['supplierSeguroViagem'] : ""), 'class="form-control" id="slSupplierSeguroViagem"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ####################### !-->
                                <!-- ### OUTROS SERVICOS ### !-->
                                <!-- ####################### !-->
                                <div id="outros-servicos" class="tab-pane fade in">
                                    <div class="box">
                                        <div class="box-header">
                                            <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i><?= lang('header.label.cadastro.outros.servicos'); ?></h2>
                                            <div class="box-icon">
                                                <ul class="btn-tasks">
                                                    <li class="dropdown">
                                                        <a id="btn-exibir-meu-pacote" style="cursor: pointer;" class="toggle_up tip" title="<?= lang('adicinar_novo_pacote') ?>">
                                                            <i class="icon fa fa-plus" style="font-size: 25px;"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12" id="form-meu-pacote#">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("descricaoOutrosServicos", "slDescricaoOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-map" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('descricaoOutrosServicos', (isset($_POST['descricaoOutrosServicos']) ? $_POST['descricaoOutrosServicos'] : ""), 'class="form-control" id="slDescricaoOutrosServicos"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("tipoOutrosServicos", "slTipoOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php
                                                                    $cbCategoria[""] = "Vila Olimbia";
                                                                    foreach ($condicoesPagamento as $le) {
                                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                                    }
                                                                    echo form_dropdown('tipoOutrosServicos', $cbCategoria, (isset($_POST['tipoOutrosServicos']) ? $_POST['tipoOutrosServicos'] : ''), 'id="slTipoOutrosServicos" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("reservaOutrosServicos", "slReservaOutrosServicos"); ?>
                                                                <?php echo form_input('reservaOutrosServicos', (isset($_POST['reservaOutrosServicos']) ? $_POST['reservaOutrosServicos'] : ""), 'class="form-control" id="slReservaOutrosServicos"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("emissaoOutrosServicos", "slEmissaoOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('emissaoOutrosServicos', (isset($_POST['emissaoOutrosServicos']) ? $_POST['emissaoOutrosServicos'] : ""), 'class="form-control input-tip date" id="slEmissaoOutrosServicos"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckinOutrosServicos", "slDateCheckinOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckinOutrosServicos', (isset($_POST['dateCheckinOutrosServicos']) ? $_POST['dateCheckinOutrosServicos'] : ""), 'class="form-control input-tip date" id="slDateCheckinOutrosServicos"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckoutOutrosServicos", "slDateCheckoutOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('dateCheckoutOutrosServicos', (isset($_POST['dateCheckoutOutrosServicos']) ? $_POST['dateCheckoutOutrosServicos'] : ""), 'class="form-control input-tip date" id="slDateCheckoutOutrosServicos"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("supplierOutrosServicos", "slSupplierOutrosServicos"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-building-o" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('supplierOutrosServicos', (isset($_POST['supplierOutrosServicos']) ? $_POST['supplierOutrosServicos'] : ""), 'class="form-control" id="slSupplierOutrosServicos"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php if ($Settings->tax2) { ?>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <?= lang("order_tax", "sltax2"); ?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="sltax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("order_discount", "sldiscount"); ?>
                                    <?php echo form_input('order_discount', '0.00', 'class="form-control input-tip mask_money" id="sldiscount"'); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("acrescimo", "slshipping"); ?>
                                <?php echo form_input('shipping', '0.00', 'class="form-control input-tip mask_money" id="slshipping"'); ?>
                            </div>
                        </div>

                        <div class="col-md-12" style="display: none;">
                            <div class="form-group">
                                <?= lang("Vendedor", "Vendedor"); ?>
                                <?php echo form_input('vendedor', (isset($_POST['vendedor']) ? $_POST['vendedor'] : ''), 'class="form-control input-tip" id="vendedor"' ); ?>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                            <ul id="myTab3" class="nav nav-tabs">
                                <li class=""><a href="#pagamento-cliente" class="tab-grey" style="text-align: center;"><i class="fa fa-money" style="font-size: 25px;"></i><br/>Faturar Pagamento Cliente</span></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="pagamento-cliente" class="tab-pane fade in" style="padding: 1px;">
                                    <div class="box">
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row" id="form-fatura-cliente">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang('previsao_pagamento', 'previsao_pagamento'); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('previsao_pagamento', (isset($_POST['previsao_pagamento']) ? $_POST['previsao_pagamento'] : ""), 'class="form-control input-tip date" id="previsao_pagamento"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("valor_vencimento", "valor"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <input type="text" name="valor" id="valor" value="0.00" readonly style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("tipo_cobranca", "tipoCobrancaId"); ?>
                                                                <?php
                                                                $cbTipoCobranca[""] = "";
                                                                foreach ($tiposCobranca as $tc) {
                                                                    $cbTipoCobranca[$tc->id] = $tc->name;
                                                                }
                                                                echo form_dropdown('tipoCobrancaId', $cbTipoCobranca, (isset($_POST['tipoCobrancaId']) ? $_POST['tipoCobrancaId'] : $Settings->default_biller), 'id="tipoCobrancaId" name="tipoCobrancaId" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("condicao_pagamento", "condicaopagamentoId"); ?>
                                                                <?php
                                                                $cbCondicaoPagamento[""] = "";
                                                                foreach ($condicoesPagamento as $cp) {
                                                                    $cbCondicaoPagamento[$cp->id] = $cp->name;
                                                                }
                                                                echo form_dropdown('condicaopagamentoId', $cbCondicaoPagamento, (isset($_POST['condicaopagamentoId']) ? $_POST['condicaopagamentoId'] : $Settings->default_biller), 'id="condicaopagamentoId" name="condicaopagamentoId" data-placeholder="' . lang("select") . ' ' . lang("condicao_pagamento") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <?php if ($Owner || $Admin || $GP['sales-payments']) { ?>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <?= lang("payment_status", "slpayment_status"); ?>
                                                                    <?php $pst = array('pending' => lang('pending'), 'due' => lang('due'), 'partial' => lang('partial'), 'paid' => lang('paid'));
                                                                    echo form_dropdown('payment_status', $pst, '', 'class="form-control input-tip" required="required" id="slpayment_status"'); ?>
                                                                </div>
                                                            </div>
                                                        <?php } else {
                                                            echo form_hidden('payment_status', 'pending');
                                                        } ?>
                                                        <div class="col-md-6" style="display: none;">
                                                            <div class="form-group">
                                                                <?= lang("reference_no", "slref"); ?>
                                                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $slnumber), 'class="form-control input-tip" id="slref"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="controls table-controls slTableParcelas">
                                                                <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="text-align: left;">#</th>
                                                                        <th class="col-md-4" style="text-align: left;">Valor</th>
                                                                        <th class="col-md-1" style="text-align: left;">Desc.</th>
                                                                        <th class="col-md-4" style="text-align: left;">Vencimento</th>
                                                                        <th class="col-md-6" style="text-align: left;">Tipo</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div id="payments" style="display: none;">
                                                            <div class="col-md-12">
                                                                <div class="well well-sm well_1">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                                                    <?= form_input('payment_reference_no', (isset($_POST['payment_reference_no']) ? $_POST['payment_reference_no'] : $payment_ref), 'class="form-control tip" id="payment_reference_no"'); ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="payment">
                                                                                    <div class="form-group ngc">
                                                                                        <?= lang("amount", "amount_1"); ?>
                                                                                        <input name="amount-paid" type="text" id="amount_1" required="required" value="0" class="pa form-control kb-pad amount"/>
                                                                                    </div>
                                                                                    <div class="form-group gc" style="display: none;">
                                                                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                                        <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>
                                                                                        <div id="gc_details"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <?= lang("paying_by", "paid_by_1"); ?>
                                                                                    <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                                                        <?= $this->sma->paid_opts(); ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="pcc_1" style="display:none;">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input name="pcc_no" type="text" id="pcc_no_1"
                                                                                               class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control"
                                                                                               placeholder="<?= lang('cc_holder') ?>"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <select name="pcc_type" id="pcc_type_1"
                                                                                                class="form-control pcc_type"
                                                                                                placeholder="<?= lang('card_type') ?>">
                                                                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                                                                            <option
                                                                                                    value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                                                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                                                                        </select>
                                                                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input name="pcc_month" type="text" id="pcc_month_1"
                                                                                               class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">

                                                                                        <input name="pcc_year" type="text" id="pcc_year_1"
                                                                                               class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">

                                                                                        <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                                                               class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pcheque_1" style="display:none;">
                                                                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                                                <input name="cheque_no" type="text" id="cheque_no_1"
                                                                                       class="form-control cheque_no"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <?= lang('payment_note', 'payment_note_1'); ?>
                                                                            <textarea name="payment_note" id="payment_note_1"
                                                                                      class="pa form-control kb-text payment_note"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                        <div class="row" id="bt">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("sale_note", "slnote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="slnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("staff_note", "slinnote"); ?>
                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="slinnote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="from-group">
                                <?php echo form_submit('add_sale', lang("submit"), 'id="add_sale" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                    <style>
                        .info-view {
                            background-color: #428bca;
                            color: #ffffff;
                            font-size: 14px;
                        }
                    </style>
                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                        <tr class="info-view">
                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                            <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                            <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                            <?php }?>
                            <?php if ($Settings->tax2) { ?>
                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                            <?php } ?>
                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                        </tr>
                    </table>
                </div>
                <input type="hidden" name="totalHoteis" value="" id="totalHoteis">
                <input type="hidden" name="supplier_id" value="" id="supplier_id">
                <input type="hidden" name="supplier_id2" value="" id="supplier_id2">
                <input type="hidden" name="supplier_id3" value="" id="supplier_id3">
                <input type="hidden" name="supplier_id4" value="" id="supplier_id4">
                <input type="hidden" name="supplier_id5" value="" id="supplier_id5">
                <input type="hidden" name="supplier_id6" value="" id="supplier_id6">
                <input type="hidden" name="supplier_id7" value="" id="supplier_id7">
                <input type="hidden" name="form_slcustomer_1" value="" id="form_slcustomer_1"/>
				<input type="hidden" name="form_slcustomer_2" value="" id="form_slcustomer_2"/>
				<input type="hidden" name="form_slcustomer_3" value="" id="form_slcustomer_3"/>
				<input type="hidden" name="form_slcustomer_4" value="" id="form_slcustomer_4"/>
				<input type="hidden" name="form_slcustomer_5" value="" id="form_slcustomer_5"/>
				<input type="hidden" name="form_slnote_hotel" value="" id="form_slnote_hotel"/>
				<input type="hidden" name="form_sltipo_quarto" value="" id="form_sltipo_quarto"/>
				<input type="hidden" name="reference_no_variacao" value="" id="reference_no_variacao"/>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity_item') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('customer') ?></label>

                        <div class="col-sm-8">
                            <?php
                            echo form_input('customerClient', (isset($_POST['customerClient']) ? $_POST['customerClient'] : ""), 'id="customerClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                            ?>
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####     ONIBUS     ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('Onibus') ?></label>
                        <div class="col-sm-8">
                            <input type="hidden" name="reference_no_variacao_onibus" value="" id="reference_no_variacao_onibus" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("onibus") ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('poltrona') ?></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <?php
                                echo form_input('poltronaClient', (isset($_POST['poltronaClient']) ? $_POST['poltronaClient'] : ""), 'id="poltronaClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" required="required" style="width:100%;"');
                                ?>
                                 <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                    <a href="#" id="addBusManuallyClient" class="tip" title="<?= lang('add_poltrona_manually') ?>">
                                        <i class="fa fa-bus  addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('valor') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                          </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('descricao') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity_item') ?> *</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('valor') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<!--#################### INICIO MODAL ONIBUS ######################### -->
<div class="modal" id="mModal2" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
        <div class="modal-content">
		<center>
            <div class="modal-header">
                <button type="button" class="close" id="close_bus" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_poltrona') ?></h4>
            </div>
			<iframe src="#" width="100%"  id="iframe_onibus" scrolling="auto" height="900px"></iframe>
		</center>
        </div>
    </div>
</div>
<!--######################## FINAL MODAL ONIBUS ################################### -->

<!-- ################################### MODAL HOTEL ##########################################################-->
<div class="modal" id="mModal3" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
							class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
				<h4 class="modal-title" id="mModalLabel"><?= lang('add_hotel_manually') ?></h4>
			</div>
		
			<div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">

                    <!--########################!-->
                    <!--#### QT HOTEIS ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('qts_hoteis') ?> *</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="qts_hoteis" id="qts_hoteis" required="required">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                        </div>
                    </div>

					<!--########################!-->
					<!--####   FORNECEDORES   ####!-->
					<!--#########################-->
					<div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= '1º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier" value="" id="posupplier" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 2  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier2">
                        <label for="mtax" class="col-sm-4 control-label"><?= '2º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier2" value="" id="posupplier2" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 3  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier3">
                        <label for="mtax" class="col-sm-4 control-label"><?= '3º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier3" value="" id="posupplier3" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 4  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier4">
                        <label for="mtax" class="col-sm-4 control-label"><?= '4º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier4" value="" id="posupplier4" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 5  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier5">
                        <label for="mtax" class="col-sm-4 control-label"><?= '5º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier5" value="" id="posupplier5" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 6  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier6">
                        <label for="mtax" class="col-sm-4 control-label"><?= '6º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier6" value="" id="posupplier6" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 7  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier7">
                        <label for="mtax" class="col-sm-4 control-label"><?= '7º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier7" value="" id="posupplier7" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

					<!--########################!-->
					<!--#### TIPO DE QUARTO ####!-->
					<!--#########################-->
					<div class="form-group">
						<label for="mtax" class="col-sm-4 control-label"><?= lang('tipo_quarto') ?> *</label>
						<div class="col-sm-8">
                            <select class="form-control" name="tipo_quarto" id="tipo_quarto" required="required">
                                <option value="8"><?php echo lang('single');?></option>
                                <option value="1"><?php echo lang('duplo_casal');?></option>
                                <option value="2"><?php echo lang('duplo_casal_um_adulto');?></option>
                                <option value="3"><?php echo lang('duplo_casal_dois_adulto');?></option>
                                <option value="4"><?php echo lang('duplo_casal_tres_adulto');?></option>
                                <option value="6"><?php echo lang('duplo_solteiro');?></option>
                                <option value="7"><?php echo lang('tripo_solteiro');?></option>
                            </select>
						</div>
					</div>
 				
					<!--#########################!-->
					<!--#### 1 pessoa do quarto #!-->
					<!--##########################-->
					<div class="form-group" id="div_slcustomer_1">
						<label for="mtax" class="col-sm-4 control-label"><?= '1º '.lang('Hospede') ?> *</label>
						<div class="col-sm-8">
							<?php
							echo form_input('customer_1', (isset($_POST['customer_1']) ? $_POST['customer_1'] : ""), 'id="slcustomer_1" data-placeholder="' . lang("select") . ' ' . lang("customer_1") . '" required="required" class="form-control input-tip"');
							?> 
						</div>
					</div>
 
					
					<!--#########################!-->
					<!--#### 2 pessoa do quarto #!-->
					<!--##########################-->
					<div class="form-group" id="div_slcustomer_2" style="display:none">
						<label for="mtax" class="col-sm-4 control-label"><?= '2º '.lang('Hospede') ?> *</label>
						<div class="col-sm-8">
							<?php
							echo form_input('customer_2', (isset($_POST['customer_2']) ? $_POST['customer_2'] : ""), 'id="slcustomer_2" data-placeholder="' . lang("select") . ' ' . lang("customer_2") . '" required="required" class="form-control input-tip"');
							?> 
						</div>
					</div>
 
					
					<!--#########################!-->
					<!--#### 3 pessoa do quarto #!-->
					<!--##########################-->
					<div class="form-group" id="div_slcustomer_3" style="display:none">
						<label for="mtax" class="col-sm-4 control-label"><?= '3º '.lang('Hospede') ?> *</label>
						<div class="col-sm-8">
							<?php
							echo form_input('customer_3', (isset($_POST['customer_3']) ? $_POST['customer_3'] : ""), 'id="slcustomer_3" data-placeholder="' . lang("select") . ' ' . lang("customer_3") . '" required="required" class="form-control input-tip"');
							?> 
						</div>
					</div>
 
					<!--#########################!-->
					<!--#### 4 pessoa do quarto #!-->
					<!--##########################-->
					<div class="form-group" id="div_slcustomer_4" style="display:none">
						<label for="mtax" class="col-sm-4 control-label"><?= '4º '.lang('Hospede') ?> *</label>
						<div class="col-sm-8">
							<?php
							echo form_input('customer_4', (isset($_POST['customer_4']) ? $_POST['customer_4'] : ""), 'id="slcustomer_4" data-placeholder="' . lang("select") . ' ' . lang("customer_4") . '" required="required" class="form-control input-tip"');
							?> 
						</div>
					</div>

					
					<!--#########################!-->
					<!--#### 5 pessoa do quarto #!-->
					<!--##########################-->
					<div class="form-group" id="div_slcustomer_5" style="display:none">
						<label for="mtax" class="col-sm-4 control-label"><?= '5º '.lang('Hospede') ?> *</label>
						<div class="col-sm-8">
							<?php
							echo form_input('customer_5', (isset($_POST['customer_5']) ? $_POST['customer_5'] : ""), 'id="slcustomer_5" data-placeholder="' . lang("select") . ' ' . lang("customer_5") . '" required="required" class="form-control input-tip"');
							?> 
						</div>
					</div>
 
					<!--########################!-->
					<!--#### NOTA DO HOTEL  ####!-->
					<!--#########################-->
					<div class="form-group">
						<label for="note_hotel" class="col-sm-4 control-label"><?= lang('sale_note_hotel') ?></label>
						<div class="col-sm-8">
							<?php echo form_textarea('note_hotel', (isset($_POST['note_hotel']) ? $_POST['note_hotel'] : ""), 'class="form-control" id="note_hotel" style="margin-top: 10px; height: 100px;"'); ?>
						</div>
					</div>
					 
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="addItemHotelManually"><?= lang('submit') ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--####################################################### FINAL DO MODAL HOTEL #####################################--->


<div class="modal" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('sell_gift_card'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>

                <div class="alert alert-danger gcerror-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="gcerror"></span>
                </div>
                <div class="form-group">
                    <?= lang("card_no", "gccard_no"); ?> *
                    <div class="input-group">
                        <?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#"
                                                                                                           id="genNo"><i
                                    class="fa fa-cogs"></i></a></div>
                    </div>
                </div>
                <input type="hidden" name="gcname" value="<?= lang('gift_card') ?>" id="gcname"/>

                <div class="form-group">
                    <?= lang("value", "gcvalue"); ?> *
                    <?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("price", "gcprice"); ?> *
                    <?php echo form_input('gcprice', '', 'class="form-control" id="gcprice"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("customer", "gccustomer"); ?>
                    <?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("expiry_date", "gcexpiry"); ?>
                    <?php echo form_input('gcexpiry', $this->sma->hrsd(date("Y-m-d", strtotime("+2 year"))), 'class="form-control date" id="gcexpiry"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="addGiftCard" class="btn btn-primary"><?= lang('sell_gift_card') ?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#gccustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
		
        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });
    });
</script>
