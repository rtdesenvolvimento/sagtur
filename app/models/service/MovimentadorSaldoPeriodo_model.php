<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MovimentadorSaldoPeriodo_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function atualizarMovimentadorSaldoNoPeriodo($fatura, $pagamento) {

        $valorPagamento = $pagamento->amount;
        $dataPagamento = $pagamento->date;
        $formaPagamento = $pagamento->formapagamento;
        $movimentador = $pagamento->movimentador;
        $bloqueado = 0;

        if ($dataPagamento != '0000-00-00 00:00:00') {
            $movimentadorSaldoPeriodo = $this->getMovimentadorSaldoPeriodo($formaPagamento, $movimentador, $dataPagamento);

            //$saldoAnterior = $movimentadorSaldoPeriodo->saldo;
            $entradas = $movimentadorSaldoPeriodo->entradas;
            $saidas = $movimentadorSaldoPeriodo->saidas;

            if ($this->isFaturaDeCredito($fatura)) $entradas = $entradas - $valorPagamento;
            else $saidas = $saidas - $valorPagamento;

            $saldo = ($entradas - $saidas);
            $diferena = $saldo - $bloqueado;

            $dadosAtualizadosMovimentadorSaldo = array(
                'entradas' => $entradas,
                'saidas' => $saidas,
                'saldo' => $saldo,
                'bloqueado' => $bloqueado,
                'diferenca' => $diferena,
                'formapagamento' => $formaPagamento,
                'movimentador' => $movimentador,
            );

            $this->__edit('movimentador_saldo_periodo', $dadosAtualizadosMovimentadorSaldo, 'id', $movimentadorSaldoPeriodo->id);
        }
    }

    private function isFaturaDeCredito($fatura) {
        return $fatura->tipooperacao == Financeiro_model::TIPO_OPERACAO_CREDITO;
    }

    private function getMovimentadorSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo) {

        $periodo = date('Y-m-d', strtotime($periodo));

        $this->db->where('formapagamento',$formaPagamentoId);
        $this->db->where('movimentador',$movimentadorId);
        $this->db->where('dtmovimento',$periodo);
        $this->db->limit(1);
        $movimentadoSaldo = $this->db->get('movimentador_saldo_periodo')->row();

        if (count($movimentadoSaldo) > 0) {
            return $movimentadoSaldo;
        } else {
            $this->adicionarMovimentadoSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo);
        }

        return $this->getMovimentadorSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo);
    }

    function adicionarMovimentadoSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo) {

        $dadosNovoMovimentadorSaldo = array (
            'entradas' => 0,
            'saidas' => 0,
            'saldo' => 0,
            'bloqueado' => 0,
            'diferenca' => 0,
            'formapagamento' => $formaPagamentoId,
            'movimentador' => $movimentadorId,
            'dtmovimento' => $periodo
        );

        $this->db->insert('movimentador_saldo_periodo', $dadosNovoMovimentadorSaldo);

        if ($this->db->affected_rows() == '1') return $this->db->insert_id('movimentador_saldo_periodo');

        return FALSE;
    }
}