<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('commission_settings'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open("commission_settings"); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang('despesas', 'despesa_fechamento_id'); ?>
                            <select class="form-control tip" name="despesa_fechamento_id" id="despesa_fechamento_id" required="required">
                                <option value=""><?php echo lang('select').' '.lang('despesa');?></option>
                                <?php
                                foreach ($despesas as $despesa) {?>
                                    <optgroup label="<?php echo $despesa->name;?>">
                                        <?php
                                        $despesasFilhas = $this->site->getReceitaByDespesaSuperiorId($despesa->id);
                                        foreach ($despesasFilhas as $item) {?>
                                            <?php if ($this->Settings->despesa_fechamento_id == $item->id ) {?>
                                                <option  selected="selected" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                            <?php } else {?>
                                                <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                            <?php } ?>
                                        <?php }?>
                                    </optgroup>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang('tipo_cobranca', 'tipo_cobranca_fechamento_id'); ?>
                            <?php
                            $cbTipoDeCobranca[''] = '';
                            foreach ($tiposCobranca as $tipoCobranca) {
                                $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                            }
                            echo form_dropdown('tipo_cobranca_fechamento_id', $cbTipoDeCobranca, $this->Settings->tipo_cobranca_fechamento_id, 'id="tipo_cobranca_fechamento_id" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca") . '" required="required" style="width:100%;" ');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both; height: 10px;"></div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="controls">
                        <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
