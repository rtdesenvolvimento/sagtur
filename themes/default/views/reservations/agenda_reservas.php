
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_blue.css">

<style>
    :root {
        --fc-small-font-size: 1em;
        --fc-list-event-dot-width: 10px;
        --fc-list-event-hover-bg-color: #428BCA;
    }

    .cart_destaque {
        border: 1px solid #dcdcdc;
        padding: 25px 15px 15px 15px;
        margin-bottom: 0;
        background: #ffff;
        border-radius: 5px;
    }

    .fc .fc-button-primary {
        background-color: #428BCA;
        border-color: #428BCA;
    }
</style>
<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('reservations/search_reservations_actions', 'id="action-form" target="_blank');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('search_reservations'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-4 cart_destaque">
                <div class="col-md-12" style='margin-top: 15px;'>
                    <input class="flatpickr" type="text" name="flatpickr_date" style="display: none;">
                </div>
                <div class="col-lg-12" style="margin-top: 15px;">
                    <?= lang("tours", "tours") ?>
                    <select id="tours" name="tours" class="form-control select" style="width: 100%;">
                        <option value=""><?= lang("select") . " " . lang("tours");?></option>
                        <?php foreach ($products as $product) {?>
                            <option image="<?=$product->image;?>" value="<?=$product->id;?>"><?=$product->name;?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("start_time", "start_time"); ?>
                        <?php echo form_input('start_time', (isset($_POST['start_time']) ? $_POST['start_time'] : ''), 'class="form-control" id="start_time"', 'time'); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("end_time", "end_time"); ?>
                        <?php echo form_input('end_time', (isset($_POST['end_time']) ? $_POST['end_time'] : ''), 'type="date" class="form-control" id="end_time"', 'time'); ?>
                    </div>
                </div>
                <div  class="col-sm-6">
                    <?= lang("Status da Venda", "filterStatus") ?>
                    <?php
                    $cbStatus = array(
                        '' => lang('select'),
                        'orcamento' => lang('orcamento'),
                        'faturada' => lang('faturada'),
                    );
                    echo form_dropdown('filterStatus', $cbStatus,  $ano, 'class="form-control" id="filterStatus"'); ?>
                </div>
                <div  class="col-sm-6">
                    <?= lang("situacao", "situacao") ?>
                    <?php
                    $cbSituacao = array(
                        '' => lang('select'),
                        'due' => lang('due'),
                        'partial' => lang('partial'),
                        'paid' => lang('paid'),
                        'cancel' => lang('cancel'),
                    );
                    echo form_dropdown('situacao', $cbSituacao,  $ano, 'class="form-control" id="situacao"'); ?>
                </div>
                <div class="col-sm-6">
                    <?= lang("biller", "billerFilter"); ?>
                    <?php
                    $bl[""] = lang("select") . ' ' . lang("biller") ;
                    foreach ($billers as $biller) {
                        $bl[$biller->id] = $biller->name;
                    }
                    echo form_dropdown('billerFilter', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("customer_filter", "customer_filter"); ?>
                        <?php
                        echo form_input('customer_filter', (isset($_POST['customer_filter']) ? $_POST['customer_filter'] : ""), 'id="customer_filter" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("meio_divulgacao", "fdivulgacao"); ?>
                        <?php
                        $md[""] = lang("select") . ' ' . lang("meio_divulgacao") ;
                        foreach ($meiosDivulgacao as $divulgacao) {
                            $md[$divulgacao->id] = $divulgacao->name;
                        }
                        echo form_dropdown('fdivulgacao', $md, $inv->meio_divulgacao, 'id="fdivulgacao" name="fdivulgacao" data-placeholder="' . lang("select") . ' ' . lang("meio_divulgacao") . '"class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?= lang("data_venda_de", "data_venda_de"); ?>
                    <?php echo form_input('flDataVendaDe', (isset($_POST['flDataVendaDe']) ? $_POST['flDataVendaDe'] : $flDataVendaDe), 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                </div>
                <div class="col-sm-6">
                    <?= lang("data_venda_ate", "data_venda_ate"); ?>
                    <?php echo form_input('flDataVendaAte', (isset($_POST['flDataVendaAte']) ? $_POST['flDataVendaAte'] : $flDataVendaAte), 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                </div>
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success" id="pdf" style="margin-top: 16px;"><?= lang('export_to_pdf') ?></button>
                    <button type="button" class="btn btn-danger" id="reset" style="margin-top: 16px;"><?= lang('reset') ?></button>
                </div>
            </div>
            <div class="col-md-8">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php } ?>

<div class="modal" id="cal_modal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="close_bus" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_venda') ?></h4>
            </div>
            <iframe id="iframe_new_sale" width="598px" height="600px;" frameborder="0" scrolling="no"></iframe>
        </div>
    </div>
</div>

<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr@4.6.13/dist/l10n/pt.js"></script>

<script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>
<script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>

<script type="text/javascript">

    var calendar = null;
    var calendarEl = document.getElementById('calendar');

    var optional_config = {
        enableTime: false,
        dateFormat: "Y-m-d",
        locale : "pt",
        inline: true,
        minDate: "today",
        weekNumbers: false,
        onChange: function(selectedDates, dateStr, instance) {
            carregar_agenda('listDay', dateStr);
        },
    };

    var fp = flatpickr(".flatpickr",  optional_config);

    $(document).ready(function () {

        carregar_agenda('listMonth');

        setTimeout(function (){carregarEstilo();}, 200);

        $('#customer_filter').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            carregar_agenda('listWeek');
        });

        $('#tours').change(function (){
            carregar_agenda('listWeek');
        });

        $('#start_time').change(function (){
            carregar_agenda('listWeek');
        });

        $('#end_time').change(function (){
            carregar_agenda('listWeek');
        });

        $('#filterStatus').change(function (){
            carregar_agenda('listWeek');
        });

        $('#situacao').change(function (){
            carregar_agenda('listWeek');
        });

        $('#billerFilter').change(function (){
            carregar_agenda('listWeek');
        });

        $('#fdivulgacao').change(function (){
            carregar_agenda('listWeek');
        });

        $('#flDataVendaDe').change(function (){
            carregar_agenda('listWeek');
        });

        $('#flDataVendaAte').change(function (){
            carregar_agenda('listWeek');
        });

        $('body').on('click', '#pdf', function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#reset').click(function (evet){
            location.reload();
        });

        $('body').on('click', '#pdf', function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });
    });

    function carregarEstilo() {
        $('.flatpickr-monthDropdown-months').select2('destroy');
        $('.flatpickr-calendar.inline').css('margin-left', '15px');
        $('.fc-toolbar-title').css('text-transform', 'uppercase');
    }

    function carregar_agenda(initialView, default_date) {

        if (calendar !== null) {
            calendar.destroy();
        }

        if (default_date === undefined) {
            default_date = '<?=date('Y-m-d');?>'
        }

        calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: initialView,
            initialDate: default_date,
            timeZone: 'local',
            showNonCurrentDates: false,
            nowIndicator: true,
            businessHours: false,
            locale: 'pt-br',
            views: {
                listDay: { buttonText: '<?=lang('list_by_day');?>' },
                listWeek: { buttonText: '<?=lang('list_by_week');?>' },
                listMonth: { buttonText: '<?=lang('list_by_month');?>' },
                listYear: { buttonText: '<?=lang('list_by_year');?>' }
            },
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'listDay,listWeek,listMonth,timeGridWeek,timeGridDay,dayGridMonth'

            },
            eventClick: function(info) {
                info.jsEvent.preventDefault();
                if (info.event.url) {
                    $('#myModal').modal({remote: info.event.url});
                    $('#myModal').modal('show');
                }
            },
            eventDidMount: function(info) {
                var tooltip = new Tooltip(info.el, {
                    title: info.event.extendedProps.description,
                    //placement: 'top',
                    trigger: 'hover',
                    container: 'body'
                });
            },
            events: getLoadEventsByFilter(),
        });

        calendar.render();

        carregar_calendario();
    }

    function carregar_calendario() {

        optional_config.enable = [];

        $.ajax({
            type: "GET",
            url:  getLoadEventsByFilter(),
            data: {},
            dataType: 'json',
            success: function (events) {

                $(events).each(function(index, event) {
                    optional_config.enable.push({from: event.start, to: event.end})
                });

                fp.destroy();
                fp = flatpickr(".flatpickr",  optional_config);

                $('.flatpickr-calendar.inline').css('margin-left', '15px');
                $('.fc-toolbar-title').css('text-transform', 'uppercase');
            }
        });
    }

    function getLoadEventsByFilter() {

        let productsID = $('#tours').val();
        let start_time = $('#start_time').val();
        let end_time = $('#end_time').val();
        let biller = $('#billerFilter').val();
        let customer = $('#customer_filter').val();
        let meio_divulgacao = $('#fdivulgacao').val();
        let status_sale = $('#filterStatus').val();
        let status_payment = $('#situacao').val();
        let dataReservaDe = $('#flDataVendaDe').val();
        let dataReservaAte = $('#flDataVendaAte').val();

        let url = site.base_url+'reservations/search_reservations?produto_id='+productsID;

        if (start_time !== '') url += '&start_time='+start_time;
        if (end_time !== '') url += '&end_time='+end_time;
        if (biller !== '') url += '&biller='+biller;
        if (customer !== '') url += '&customer='+customer;
        if (meio_divulgacao !== '') url += '&meio_divulgacao='+meio_divulgacao;
        if (status_sale !== '') url += '&status_sale='+status_sale;
        if (status_payment !== '') url += '&status_payment='+status_payment;
        if (dataReservaDe !== '') url += '&data_venda_de='+dataReservaDe;
        if (dataReservaAte !== '') url += '&data_venda_ate='+dataReservaAte;

        return url;
    }
</script>

