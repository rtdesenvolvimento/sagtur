<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaProgramacaoRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id) {
        $q = $this->db->get_where('agenda_programacao', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function getAll($productID) {
        $this->db->where("produto", $productID);

        $this->db->order_by('dataSaida, horaSaida, dataRetorno, horaRetorno', 'asc');

        $q = $this->db->get('agenda_programacao');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllByModel($productID) {
        $this->db->where("produto", $productID);

        $q = $this->db->get('agenda_programacao');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $resultado) {

                $agenda = new AgendaProgramacao_model();

                $agenda->produto = $resultado->produto;
                $agenda->dataSaida = $resultado->dataSaida;
                $agenda->dataRetorno = $resultado->dataRetorno;
                $agenda->horaSaida = $resultado->horaSaida;
                $agenda->horaRetorno = $resultado->horaRetorno;
                $agenda->titulo = $resultado->titulo;
                $agenda->note = $resultado->note;
                $agenda->isPromocao = $resultado->isPromocao;
                $agenda->isUsarPreco = $resultado->isUsarPreco;
                $agenda->preco = $resultado->preco;
                $agenda->tipoDisponibilidade = $resultado->tipoDisponibilidade;
                $agenda->vagas = $resultado->vagas;
                $agenda->segunda = $resultado->segunda;
                $agenda->terca = $resultado->terca;
                $agenda->quarta = $resultado->quarta;
                $agenda->quinta = $resultado->quinta;
                $agenda->sexta = $resultado->sexta;
                $agenda->sabado = $resultado->sabado;
                $agenda->domingo = $resultado->domingo;

                $data[] = $agenda;

            }
            return $data;
        }
        return FALSE;
    }

    public function delete($productID)
    {
        return $this->db->delete('agenda_programacao', array('produto' => $productID));
    }

}