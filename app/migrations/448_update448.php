<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update448 extends CI_Migration {

    public function up() {

        $this->alter_table_rating_questions();

        $this->db->update('settings',  array('version' => '4.4.7'), array('setting_id' => 1));
    }

    public function alter_table_rating_questions() {
        $fields = array(
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('rating_questions', $fields);
    }

    public function down() {}
}
