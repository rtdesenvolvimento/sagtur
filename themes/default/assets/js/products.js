$(document).ready(function (e) {

    $('#addAttributesHospedagem').click(function (e) {
        e.preventDefault();
        preencherTabeladaDeHospedagem();
    });

    $('#save').click(function (event){$('#add_product').click();});

    $(document).on('click', '.delAttrFaixaEtariaServicoOpcional', function () {
        //var id = $(this).attr('id');
        $(this).closest("tr").remove();//TODO REMOVER OBJETO
    });

    $(document).on('click', '.delAttrFaixaEtaria', function () {
        $(this).closest("tr").remove();
    });

    $('#isTransporteTuristico').on('ifChecked', function (e) {$('#tbTransporte').slideDown();});
    $('#isTransporteTuristico').on('ifUnchecked', function (e) {$('#tbTransporte').slideUp();});

    $('#isEmbarque').on('ifChecked', function (e) {$('#tbEmbarques').slideDown();});
    $('#isEmbarque').on('ifUnchecked', function (e) {$('#tbEmbarques').slideUp();});

    $('#isComissao').on('ifChecked', function (e) {$('#tbComissao').slideDown();});
    $('#isComissao').on('ifUnchecked', function (e) {$('#tbComissao').slideUp();});

    $('#isServicosAdicionais').on('ifChecked', function (e) {$('#tbServicosAdicionais').slideDown();});
    $('#isServicosAdicionais').on('ifUnchecked', function (e) {$('#tbServicosAdicionais').slideUp();});

    $('#isTaxasComissao').on('ifChecked', function (e) {$('#tbTaxasComissao').slideDown();});
    $('#isTaxasComissao').on('ifUnchecked', function (e) {$('#tbTaxasComissao').slideUp();});

    $('#comissaoDiferenciada').on('ifChecked', function (e) {$('.divComissaoFaixaEtaria').slideDown();});
    $('#comissaoDiferenciada').on('ifUnchecked', function (e) {$('.divComissaoFaixaEtaria').slideUp();});

    $('#comissaoDiferenciadaServicoAdicional').on('ifChecked', function (e) {$('.divComissaoFaixaEtariaServicoAdicional').slideDown();});
    $('#comissaoDiferenciadaServicoAdicional').on('ifUnchecked', function (e) {$('.divComissaoFaixaEtariaServicoAdicional').slideUp();});


    $('#isServicoOnline').on('ifChecked', function (e) {$('#tbIntegracaoSite').slideDown();});
    $('#isServicoOnline').on('ifUnchecked', function (e) {$('#tbIntegracaoSite').slideUp();});

    $('#addAttributes').click(function (e) {

        e.preventDefault();
        var attrs_val = $('#attributesInput').val();
        var attrs = attrs_val.split(',');

        for (var i in attrs) {
            if (attrs[i] !== '') {
                var tableNew  = '';
                tableNew = tableNew + '<tr class="attr">';
                tableNew = tableNew + '	<td><input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>' + attrs[i] + '</span></td>';
                tableNew = tableNew + '	<td class="fornecedor text-left" style="display: none;"><input type="hidden" name="attr_fornecedor[]" value=""><span></span></td>';
                tableNew = tableNew + '	<td class="totalPoltrona text-center" style="display:none;"><input type="hidden" name="att_totalPoltrona[]" value=""><span></span></td>';
                tableNew = tableNew + '	<td class="totalPessoasConsiderar text-center" style="display:none;"><input type="hidden" name="att_totalPessoasConsiderar[]" value=""><span></span></td>';
                tableNew = tableNew + '	<td class="localizacao text-center" style="display:none;"><input type="hidden" name="attr_localizacao[]" value=""><span></span></span></td>';
                tableNew = tableNew + '	<td class="cost text-center" style="display:none;"><input type="hidden" name="attr_cost[]" value="0"><span>0</span></td>';
                tableNew = tableNew + '	<td class="price text-right" style="display: none;"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td>';
                tableNew = tableNew + '	<td class="totalPreco text-right" style="display: none;"><input type="hidden" name="attr_totalPreco[]" value="0"><span>0</span></span></td>';
                tableNew = tableNew + '	<td class="text-center"><i class="fa fa-times delAttr"></i></td>';
                tableNew = tableNew + '</tr>';
                $('#attrTable').show().append(tableNew);
            }
        }
        $('.select2-search-choice-close').click();
        $('#attributesInput').select2().val('').change();
    });

    $(document).on('click', '.attr td:not(:last-child)', function () {

        row = $(this).closest("tr");

        var nomeVariacao = row.children().eq(0).find('span').text();
        var calculo_type = 0;
        var categoria_fornecedor = 0;

        $.each(transportes, function () {
            if (this.name === nomeVariacao) {
                calculo_type = this.calculo_type;
                categoria_fornecedor = this.group_id;
            }
        });

        preencherFornecedorVariacao();

        $('#typeCalculo').val(calculo_type);
        $('#aModalLabel').text(nomeVariacao);
        $('#aTotalPoltronas').val(row.children().eq(3).find('input').val());
        $('#aLocalizacao').val(row.children().eq(5).find('span').text());
        $('#aprice').val(row.children().eq(7).find('span').text());
        $('#aCalculoPreco').val(row.children().eq(8).find('span').text());
        $('#aModal').appendTo('body').modal('show');
    });

    $(document).on('click', '.attrHospedagem', function () {

        row = $(this).closest("tr");

        var nomeVariacao = row.children().children('.attr_name').val();
        var tipoQuarto = row.children().children('.rhospedagem').val();

        let rValorBebe = row.children().children('.rValorBebe').val();
        let rValorCrianca  = row.children().children('.rValorCrianca').val();
        let rValorAdulto = row.children().children('.rValorAdulto').val();
        let rValorPNE  = row.children().children('.rValorPNE').val();
        let rValorIdoso  = row.children().children('.rValorIdoso').val();
        let rValorFree  = row.children().children('.rValorFree').val();

        let isAtivarBebe = getBoolean(row.children().children('.isAtivarBebe').val());
        let isAtivarCrianca  = getBoolean(row.children().children('.isAtivarCrianca').val());
        let isAtivarAdulto  = getBoolean(row.children().children('.isAtivarAdulto').val());
        let isAtivaPNE  = getBoolean(row.children().children('.isAtivaPNE').val());
        let isAtivarIdoso  = getBoolean(row.children().children('.isAtivarIdoso').val());
        let isAtivarFree  = getBoolean(row.children().children('.isAtivarFree').val());

        if (isAtivarBebe) $('#ativarValorHospedagemBebe').attr('checked', true);
        else $('#ativarValorHospedagemBebe').attr('checked', false);

        if (isAtivarCrianca) $('#ativarValorHospedagemCrianca').attr('checked', true);
        else $('#ativarValorHospedagemCrianca').attr('checked', false);

        if (isAtivarAdulto) $('#ativarValorHospedagemAdulto').attr('checked', true);
        else $('#ativarValorHospedagemAdulto').attr('checked', false);

        if (isAtivaPNE) $('#ativarValorHospedagemPNE').attr('checked', true);
        else $('#ativarValorHospedagemPNE').attr('checked', false);

        if (isAtivarIdoso) $('#ativarValorHospedagemIdoso').attr('checked', true);
        else $('#ativarValorHospedagemIdoso').attr('checked', false);

        if (isAtivarFree) $('#ativarValorHospedagemFree').attr('checked', true);
        else $('#ativarValorHospedagemFree').attr('checked', false);

        $('input[type="checkbox"],[type="radio"]').iCheck('update');

        if (rValorBebe === undefined) rValorBebe = '0.00';
        if (rValorCrianca === undefined) rValorCrianca = '0.00';
        if (rValorAdulto === undefined) rValorAdulto = '0.00';
        if (rValorPNE === undefined) rValorPNE = '0.00';
        if (rValorIdoso === undefined) rValorIdoso = '0.00';
        if (rValorFree === undefined) rValorFree = '0.00';

        var calculo_type = 0;
        var categoria_fornecedor = 0;

        $.each(hospedagens, function () {
            if (this.name === nomeVariacao) {
                calculo_type = this.calculo_type;
                categoria_fornecedor = this.group_id;
            }
        });

        preencherFornecedorVariacao(categoria_fornecedor);

        $('#typeCalculo').val(calculo_type);
        $('#aModalLabel').text(nomeVariacao);
        $('#aTotalPoltronas').val(row.children().eq(3).find('input').val());
        $('#aLocalizacao').val(row.children().eq(5).find('span').text());
        $('#aprice').val(row.children().eq(7).find('span').text());
        $('#aCalculoPreco').val(row.children().eq(8).find('span').text());
        $('#aHospedagemId').val(tipoQuarto);
        $('#precoBB').val(rValorBebe);
        $('#precoCrianca').val(rValorCrianca);
        $('#precoAdulto').val(rValorAdulto);
        $('#precoPne').val(rValorPNE);
        $('#precoIdoso').val(rValorIdoso);
        $('#precoFree').val(rValorFree);

        $('#aModal').appendTo('body').modal('show');
    });

    $(document).on('click', '.updateAttr', function () {
        let faixaId = $(this).attr('faixaId');
        $('#aModalHospedagem'+faixaId).modal('hide');
    });
});

function getBoolean(tipo) {
    if ("true" === tipo) return true;
    else return false;
}

function preencherFornecedorVariacao(categoria_fornecedor) {

    var aFornecedor = row.children().eq(2).find('input').val() != null ?  row.children().eq(2).find('input').val()  : '';

    if (aFornecedor !== ''){
        $('#aFornecedor').select2({
            minimumInputLength: 0,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + aFornecedor,
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        group: categoria_fornecedor,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#aFornecedor').val(aFornecedor).trigger('change');

    } else {
        $('#aFornecedor').select2({
            minimumInputLength: 0,
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        group: categoria_fornecedor,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    }
}

function preencherTabeladaDeHospedagem() {

    var attrs_val = $('#attributesInputHospedagem').val();
    var attrs = attrs_val.split(',');

    for (var i in attrs) {
        if (attrs[i] !== '') {

            let tableNew  = '';
            let atributo = attrs[i];

            tableNew = tableNew + '<tr class="attr">';
            tableNew = tableNew + '	<td><input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>'+atributo+'</span></td>';
            tableNew = tableNew + '	<td class="fornecedor text-left" style="display:none;"><input type="hidden" name="attr_fornecedor[]" value=""><span></span></td>';
            tableNew = tableNew + '	<td class="totalPoltrona text-center" style="display:none;"><input type="hidden" name="att_totalPoltrona[]" value=""><span></span></td>';
            tableNew = tableNew + '	<td class="totalPessoasConsiderar text-center" style="display:none;"><input type="hidden" name="att_totalPessoasConsiderar[]" value=""><span></span></td>';
            tableNew = tableNew + '	<td class="localizacao text-center" style="display:none;"><input type="hidden" name="attr_localizacao[]" value=""><span></span></span></td>';
            tableNew = tableNew + '	<td class="cost text-center" style="display:none;"><input type="hidden" name="attr_cost[]" value="0"><span>0</span></td>';
            tableNew = tableNew + '	<td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td>';
            tableNew = tableNew + '	<td class="totalPreco text-right" style="display: none;"><input type="hidden" name="attr_totalPreco[]" value="0"><span>0</span></span></td>';
            tableNew = tableNew + '	<td class="text-center"><i class="fa fa-times delAttr"></i></td>';
            tableNew = tableNew + '</tr>';

            $('#attrTableHospedagem').show().append(tableNew);
        }
    }

    $('.select2-search-choice-close').click();
}

function getRow(str) {
    return str !== '' && str  !== undefined ? str : '';
}