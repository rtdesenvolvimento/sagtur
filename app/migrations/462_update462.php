<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update462 extends CI_Migration {

    public function up() {

        $this->alter_table_local_embarque();

        $this->db->update('settings',  array('version' => '4.6.2'), array('setting_id' => 1));
    }

    public function alter_table_local_embarque() {
        $fields = array(
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('local_embarque', $fields);
    }

    public function down() {}
}
