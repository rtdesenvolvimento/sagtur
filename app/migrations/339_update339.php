<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update339 extends CI_Migration {

    public function up() {

        $this->create_table_valor_faixa();
        $this->create_table_shop_settings();

        $this->alter_table_products();
        $this->alter_table_valor_faixa_etaria();
        $this->alter_table_local_embarque_viagem();
        $this->alter_table_sale_items();
        $this->alter_table_sales();
        $this->alter_table_companies();
        $this->alter_table_faixa_etaria_tipo_hospedagem();
        $this->alter_table_tipo_hospedagem();
        $this->alter_table_faixa_etaria_adicional();
        $this->alter_cupom_desconto_item();
        $this->alter_cupom_desconto();

        $this->db->update('settings',  array('version' => '2.2022.07.30'), array('setting_id' => 1));
    }

    public function alter_cupom_desconto_item() {
        $fields = array(
            'status' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => ''),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11, 'null' => FALSE  ),
        );
        $this->dbforge->add_column('cupom_desconto_item', $fields);
    }

    public function alter_cupom_desconto() {
        $fields = array(
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
        );
        $this->dbforge->add_column('cupom_desconto', $fields);
    }

    public function alter_table_products() {

        $fields = array(
            'isApenasColetarPagador' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'isEmbarque' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'isComissao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'permitirListaEmpera' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//produto ativo para todos
            'captarEnderecoLink' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function alter_table_sales() {
        $fields = array(
            'desconto_cupom' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' , 'default' => 0),
            'cupom_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE ),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_sale_items() {
        $fields = array(
            'faixaId' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE  ),
            'faixaNome' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'descontarVaga' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//todo copia da faixa
            'cupom_id' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE ),
        );
        $this->dbforge->add_column('sale_items', $fields);
    }

    public function alter_table_valor_faixa_etaria() {
        $fields = array(
            'faixaId' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE  ),
        );
        $this->dbforge->add_column('valor_faixa_etaria', $fields);
    }

    public function alter_table_faixa_etaria_tipo_hospedagem() {

        $fields = array(
            'faixaId' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE  ),
        );
        $this->dbforge->add_column('faixa_etaria_tipo_hospedagem', $fields);
    }

    public function alter_table_faixa_etaria_adicional() {
        $fields = array(
            'faixaId' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE  ),
        );
        $this->dbforge->add_column('faixa_etaria_adicional', $fields);
    }

    public function alter_table_tipo_hospedagem() {
        $fields = array(
            'note' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
        );
        $this->dbforge->add_column('tipo_hospedagem', $fields);
    }

    public function alter_table_local_embarque_viagem() {
        $fields = array(
            'dataEmbarque' => array('type' => 'DATE', 'null' => TRUE ),
            'note' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
        );
        $this->dbforge->add_column('local_embarque_viagem', $fields);
    }

    public function alter_table_companies() {
        $fields = array(
            'social_name' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'profession' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    function create_table_valor_faixa() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'note' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => ''),
            'descontarVaga' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'faixaDependente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => ''),

            'obrigaCPF' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarCPF' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarDataNascimento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarDocumento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarOrgaoEmissor' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'obrigaOrgaoEmissor' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarEmail' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captarProfissao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'captarInformacoesMedicas' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'captarNomeSocial' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('valor_faixa', TRUE, $attributes);

        $tipo_faixa_padrao_adulto = array(
            'name' => 'Adulto',
            'descontarVaga' => 1,
            'faixaDependente' => 0,
        );
        $this->db->insert('valor_faixa', $tipo_faixa_padrao_adulto);

        $tipo_faixa_padrao_crianca = array(
            'name' => 'Criança',
            'descontarVaga' => 1,
            'faixaDependente' => 1,
        );
        $this->db->insert('valor_faixa', $tipo_faixa_padrao_crianca);

        $tipo_faixa_padrao_bebe = array(
            'name' => 'Bebê de colo',
            'descontarVaga' => 0,
            'faixaDependente' => 1,
            'obrigaCPF' => 0,
            'captarEmail' => 0,
        );
        $this->db->insert('valor_faixa', $tipo_faixa_padrao_bebe);

    }

    function create_table_shop_settings() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'shop_name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'version' => array('type' => 'VARCHAR', 'constraint' => '30' , 'default' => '1.0.0'),
            'logo' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'logo_header' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'all_products' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('shop_settings', TRUE, $attributes);
    }
    public function down() {}
}
