<?php defined('BASEPATH') or exit('No direct script access allowed');

class Schedules extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('schedules', $this->Settings->user_language);
    }

    public function add_schedule() {
        $dia_semana = $this->input->get('dia_semana');

        $this->data['dia_semana'] = $dia_semana;

        $this->load->view($this->theme . 'schedules/add_schedule', $this->data);
    }

    public function punctual_dates() {
        $this->load->view($this->theme . 'schedules/punctual_dates', $this->data);
    }

    public function period_dates() {
        $this->load->view($this->theme . 'schedules/period_dates', $this->data);
    }
}