<?php defined('BASEPATH') OR exit('No direct script access allowed');

class VendaFilterDTO_model extends CI_Model
{
    public $programacaoId;
    public $statusPagamento;
    public $clienteId;

    public $order_by;

    public $sale_status;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getProgramacaoId()
    {
        return $this->programacaoId;
    }

    /**
     * @param mixed $programacaoId
     */
    public function setProgramacaoId($programacaoId)
    {
        $this->programacaoId = $programacaoId;
    }

    /**
     * @return mixed
     */
    public function getStatusPagamento()
    {
        return $this->statusPagamento;
    }

    /**
     * @param mixed $statusPagamento
     */
    public function setStatusPagamento($statusPagamento)
    {
        $this->statusPagamento = $statusPagamento;
    }

    /**
     * @return mixed
     */
    public function getClienteId()
    {
        return $this->clienteId;
    }

    /**
     * @param mixed $clienteId
     */
    public function setClienteId($clienteId)
    {
        $this->clienteId = $clienteId;
    }

}
