<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update409 extends CI_Migration {

    public function up() {

        $this->alter_table_tipo_cobranca_produto();
        $this->alter_table_tipo_cobranca_condicao_produto();

        $this->db->update('settings',  array('version' => '4.0.9'), array('setting_id' => 1));
    }

    public function alter_table_tipo_cobranca_produto() {

        $this->db->query('DROP TABLE sma_tipo_cobranca_produto');

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'tipoCobrancaId' => array('type' => 'INT', 'constraint' => 11 ),
            'produtoId' => array('type' => 'INT', 'constraint' => 11 ),
            'diasAvancaPrimeiroVencimento' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'diasMaximoPagamentoAntesViagem' => array('type' => 'INT', 'constraint' => 11, 'default' => 0 ),
            'taxaIntermediacao' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'taxaFixaIntermediacao' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'exibirNaSemanaDaViagem' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'usarTaxasPagSeguro' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'numero_max_parcelas' => array('type' => 'INT', 'constraint' => 1 , 'default' => 12),
            'numero_max_parcelas_sem_juros' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'assumir_juros_parcelamento' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'status' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_cobranca_produto', TRUE, $attributes);
    }

    public function alter_table_tipo_cobranca_condicao_produto() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'tipoCobrancaId' => array('type' => 'INT', 'constraint' => 11 ),
            'condicaoPagamentoId' => array('type' => 'INT', 'constraint' => 11 ),
            'produtoId' => array('type' => 'INT', 'constraint' => 11 ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'acrescimoDescontoTipo' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'ativo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_cobranca_condicao_produto', TRUE, $attributes);
    }

    public function down() {}
}
