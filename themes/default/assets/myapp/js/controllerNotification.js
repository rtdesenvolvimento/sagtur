var ControllerNotificacao = function () {

    this.showAlert = showAlert;
    this.showConfirm = showConfirm;
    this.beep = beep;
    this.vibrate = vibrate
    this.promptMessage = promptMessage;

    function showAlert(message, callback, title, buttonName) {

        if (callback === undefined) { alert('callback obrigatório!');return;}

        if (navigator.notification !== undefined) notificationAlerta(message, callback, title, buttonName);
        else alertaInner(message, callback);
    }

    function showConfirm(message, callback, title, buttonLabels) {

        if (callback === undefined) { alert('callback obrigatório!');return;}

        if (navigator.notification !== undefined) notificationShowConfirm(message, callback,title,buttonLabels);
        else showConfirmInner(message, callback);
    }

    function promptMessage(mensagem, mensagemPadrao, callback) {
        if(mensagemPadrao === undefined) mensagemPadrao = '';
        var digitado = prompt(mensagem, mensagemPadrao);

        var callbacks = $.Callbacks();
        callbacks.add(callback);
        callbacks.fire(digitado);

    }

    function beep(numeroBeep) {
        if (navigator.notification === undefined) return;
        navigator.notification.beep(numeroBeep);
    }
    
    function vibrate(milliseconds) {
        if (navigator.notification === undefined) return;
        navigator.vibrate(milliseconds)
    }

    function showConfirmInner(message, callback) {
        var callbacks = $.Callbacks();
        callbacks.add(callback);

        var confimacao = confirm(message);
        var retorno;

        if (confimacao) retorno = 1;
        else retorno = 2;

        callbacks.fire(retorno);
    }

    function notificationShowConfirm(message, callback, title, buttonLabels) {

        if (buttonLabels === undefined) buttonLabels = 'Sim, Não';
        if (title === undefined) title = 'Confirmar?';

        navigator.notification.confirm(
            message,        // message
            callback,       // callback to invoke with index of button pressed
            title,          // title
            buttonLabels    // buttonLabels
        );
    }

    function alertaInner(message, callback) {
        alert(message);
        var callbacks = $.Callbacks();
        callbacks.add(callback);
        callbacks.fire(true);
    }

    function notificationAlerta(message, callback, title, buttonName) {

        if (title === undefined) title = 'Alerta!';
        if (buttonName === undefined) buttonName = '';

        navigator.notification.alert(
            message,  // message
            callback, // callback
            title,    // title
            buttonName // buttonName
        );
    }
}