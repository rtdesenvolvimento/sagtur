<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        .assinatura {
            border: 1px solid #0b0b0b;
            padding: 8px;
        }
    </style>
<body>
<table style="width: 100%;">
    <thead>
    <tr>
        <th colspan="1">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
        </th>
        <th style="text-align: left;" colspan="3">
            <h4 style="text-transform: uppercase;"><b>Termo de autorização de imagem</b></h4>
        </th>
    </tr>
    <tr>
        <?php
        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;
        ?>
        <th style="text-align: center;" colspan="4">
            <b><?php echo $nomeViagem; ?></b>
        </th>
    </tr>
    <tr>
        <th style="text-align: justify;" colspan="4">
            "Autorizo o uso da minha imagem em fotografias, som e vídeos para fins diversos, como publicitários, promocionais e educacionais. Esta autorização é concedida de forma gratuita e irrestrita, por tempo indeterminado. Declaro que sou o titular dos direitos da minha imagem e renuncio a qualquer compensação. Concordo em isentar de responsabilidade por quaisquer reclamações decorrentes do uso autorizado. Posso revogar esta autorização por escrito."
        </th>
    </tr>
    </thead>
    <thead>
     <tr>
         <th align="left" width="35%">PASSAGEIRO</th>
         <th style="text-align: center;" width="20%">RG</th>
         <th align="center" width="15%">CPF</th>
        <th align="center" width="30%">&nbsp;ASSINATURA&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $contador = 1;
    $isColorBackground = false;

    foreach ($itens as $row) {

            $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);

            $customer               = $objCustomer->name;
            $cpf                    = $objCustomer->vat_no;
            $rg                     = $objCustomer->cf1;
            $orgaoEmissor           = $objCustomer->cf3;
            $data_aniversario       = $objCustomer->data_aniversario;
            $tipoFaixaEtaria        = $row->tipoFaixaEtaria;

            if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

            $background = "#eee";

            if ($isColorBackground) {
                $background = "#eee";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }

            ?>
            <tr style="background: <?=$background;?>;">
                <td align="left" class="assinatura">&nbsp;<?php echo $customer;?></td>
                <td align="center" class="assinatura"><?php echo $rg;?></td>
                <td align="center" class="assinatura"><?php echo $cpf;?></td>
                <td align="center" class="assinatura"></td>
            </tr>

    <?php }?>
    </tbody>
</table>

</body>
</html>
