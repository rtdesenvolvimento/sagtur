<html>
<head>
    <title>403 Forbidden</title>
    <script src="https://sdk.mercadopago.com/js/v2"></script>
</head>
</div>
<p><?php echo $qr_code_token ?></p>
<img src="data:image/jpeg;base64,<?php echo $qr_code_base64 ?>" alt="QRCode" style="width: 300px;">

<div class="cho-container"></div>

<div>URL DO BOLETO: <a href="<?php echo $urlBoleto;?>" target="_blank">Link do boleto bancário</div>

<script>
    // Adicione as credenciais do SDK
    const mp = new MercadoPago('TEST-98b6e96e-1a6f-4d5e-9f74-24af6eec9b67', {
        locale: 'pt-BR'
    });

    // Inicialize o checkout
    mp.checkout({
        preference: {
            id: '<?php echo $identificador;?>',
        },
        render: {
            container: '.cho-container', // Indique o nome da class onde será exibido o botão de pagamento
            label: 'Pagar Agora', // Muda o texto do botão de pagamento (opcional)
        }
    });
</script>
</body>
</html>