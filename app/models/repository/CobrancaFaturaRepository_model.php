<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CobrancaFaturaRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCobrancaFaturaByQrCode($qrCode)
    {
        $qrCode = str_replace('%20', ' ', $qrCode);

        $this->db->where('(status = "ABERTA" OR status = "PENDENTE" OR status = "QUITADA" OR status = "CANCELADA")');

        $this->db->order_by('fatura_cobranca.id desc');

        $q = $this->db->get_where('fatura_cobranca', array('qr_code' => $qrCode), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCobrancaFaturaCode($code)
    {
        if ($code == null) return false;
        if ($code == 'null') return false;

        $this->db->where('(status = "ABERTA" OR status = "PENDENTE" OR status = "QUITADA" OR status = "CANCELADA")');

        $this->db->order_by('fatura_cobranca.id desc');

        $q = $this->db->get_where('fatura_cobranca', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCobrancaFaturaByFatura($fatura)
    {
        if ($fatura == null) return false;
        if ($fatura == 'null') return false;

        $this->db->where('(status = "ABERTA" OR status = "PENDENTE" OR status = "QUITADA" OR status = "CANCELADA")');

        $this->db->order_by('fatura_cobranca.id desc');

        $q = $this->db->get_where('fatura_cobranca', array('fatura' => $fatura), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}