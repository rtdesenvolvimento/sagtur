<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ratings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('ratings', $this->Settings->user_language);

        //service
        $this->load->model('service/RatingsService_model', 'RatingsService_model');

        //model
        $this->load->model('model/RatingResponse_model', 'RatingResponse_model');

        //repository
        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/RatingRepository_model', 'RatingRepository_model');

        //model
        $this->load->model('products_model');
    }

    function index() {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('ratings')));
        $meta = array('page_title' => lang('ratings'), 'bc' => $bc);

        $this->data['products'] = $this->products_model->getAllTours();

        $this->page_construct('ratings/index', $meta, $this->data);
    }

    public function questions() {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('questions')));
        $meta = array('page_title' => lang('questions'), 'bc' => $bc);

        $this->page_construct('ratings/questions', $meta, $this->data);
    }

    function getQuestions()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("rating_questions.id as id, 
                      LEFT(sma_rating_questions.question, 600) as question,")
            ->from("rating_questions")
            ->order_by('rating_questions.ordem');

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('ratings/editQuestion/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_question") . "'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    function addQuestion()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('question', lang("question"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active'        => $this->input->post('active'),
                'question'      => $this->input->post('question'),
                'type_question' => $this->input->post('type_question'),
                'ordem'         => $this->input->post('ordem'),
            );
        } elseif ($this->input->post('addQuestion')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("ratings/questions");
        }

        if ($this->form_validation->run() == true && $this->RatingRepository_model->addQuestion($data)) {
            $this->session->set_flashdata('message', lang("pergunta_adicionada_com_sucesso"));
            redirect("ratings/questions");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'ratings/addQuestion', $this->data);
        }
    }

    function editQuestion($id = NULL)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('question', lang("question"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active'        => $this->input->post('active'),
                'question'      => $this->input->post('question'),
                'type_question' => $this->input->post('type_question'),
                'ordem'         => $this->input->post('ordem'),
            );
        } elseif ($this->input->post('editQuestion')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("ratings/questions");
        }

        if ($this->form_validation->run() == true && $this->RatingRepository_model->updateQuestion($id, $data)) {
            $this->session->set_flashdata('message', lang("pergunta_atualizada_com_sucesso"));
            redirect("ratings/questions");
        } else {

            $this->data['error']    = validation_errors() ? validation_errors() : $this->session->flashdata('error');

            $this->data['question'] = $this->RatingRepository_model->getQuestionByID($id);
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'ratings/editQuestion', $this->data);
        }
    }

    function getRatings()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("ratings.id as id, 
                      ratings.created_at as date,
                      ratings.customer,
                      products.name as product_name, 
                      ratings.average")
            ->join('products', 'products.id=ratings.product_id')
            ->from("ratings");

        if ($this->input->post('filterProducts')) {
            $this->datatables->where('ratings.product_id', $this->input->post('filterProducts'));
        }

        if ($this->input->post('filterStar')) {
            if ($this->input->post('filterStar') == 'zero') {
                $this->datatables->where('ratings.average', 0);
            } else {
                $this->datatables->where('ratings.average', $this->input->post('filterStar'));
            }
        }

        $this->datatables->where('ratings.answered', true);

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('ratings/view/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("rating_view") . "'><i class=\"fa fa-eye\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    function responses()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('responses')));
        $meta = array('page_title' => lang('responses'), 'bc' => $bc);

        $this->data['products']     = $this->products_model->getAllTours();
        $this->data['questions']    = $this->RatingRepository_model->getQuestions();//TODAS INCLUINDO INATIVAS

        $this->page_construct('ratings/responses', $meta, $this->data);
    }

    function getRatingResponses()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("ratings.id as id, 
                      rating_responses.created_at as date,
                      ratings.customer,
                      products.name as product_name, 
                      rating_responses.question as question, 
                      IF(sma_rating_responses.type_question = 'note', sma_rating_responses.response, sma_rating_responses.rating) as rating")
            ->join('ratings', 'ratings.id=rating_responses.rating_id')
            ->join('products', 'products.id=ratings.product_id')
            ->from("rating_responses");

        if ($this->input->post('filterProducts')) {
            $this->datatables->where('ratings.product_id', $this->input->post('filterProducts'));
        }

        if ($this->input->post('filterQuestion')) {
            $this->datatables->where('sma_rating_responses.question_id', $this->input->post('filterQuestion'));
        }

        if ($this->input->post('filterStar')) {
            if ($this->input->post('filterStar') == 'zero') {
                $this->datatables->where('sma_rating_responses.rating', 0);
            } else {
                $this->datatables->where('sma_rating_responses.rating', $this->input->post('filterStar'));
            }
        }

        $this->datatables->where('ratings.answered', true);

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('ratings/view/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("rating_view") . "'><i class=\"fa fa-eye\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    function save_rating()
    {
        try {
            $this->RatingsService_model->saveAll();
        } catch (Exception $exception) {}
    }

    function rating($uuid)
    {
        if (!$uuid) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $rating = $this->RatingRepository_model->getByuuid($uuid);

        if (!$rating) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $this->data['rating']       = $rating;
        $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
        $this->data['questions']    = $this->RatingRepository_model->getQuestionsActive();

        if ($rating->answered) {
            $this->load->view($this->theme . 'ratings/answered', $this->data);
        } else {
            $this->load->view($this->theme . 'ratings/rating', $this->data);
        }
    }

    public function rate($programacaoID = NULL)
    {
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        if (!$programacaoID) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $programacao = $this->AgendaViagemService_model->getProgramacaoById($programacaoID);

        if (!$programacao) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('programacao_id', lang("programacao_id"), 'required');
        $this->form_validation->set_rules('produto_id', lang("produto_id"), 'required');

        if ($this->form_validation->run() == true) {

            $questions  = $this->RatingRepository_model->getQuestionsActive();
            $responses  = array();

            $rating_private = $this->input->post("rating_private") == 'on';
            $productID      = $this->input->post("produto_id");

            if ($rating_private) {
                $customer   = 'Avaliação Anônima';
            } else {
                $customer   = $this->input->post("customer");
            }

            $rating_id = $this->RatingsService_model->save_rate($customer, $productID, $programacaoID);

            foreach ($questions as $question) {

                $ratingResponse                 = new RatingResponse_model();
                $ratingResponse->question_id    = $question->id;
                $ratingResponse->rating_id      = $rating_id;
                $ratingResponse->rating         = $this->input->post("rating".$question->id)  != null ? $this->input->post("rating".$question->id) : 0;//nota
                $ratingResponse->response       = $this->input->post("message".$question->id) != null ? $this->input->post("message".$question->id)  : '';//nota
                $ratingResponse->question       = $question->question;
                $ratingResponse->type_question  = $question->type_question;
                $ratingResponse->created_at     = date('Y-m-d H:i:s');

                $responses[] = $ratingResponse;
            }

            $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
            $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['questions']    = $this->RatingRepository_model->getQuestionsActive();

            if ($this->RatingsService_model->save_responses($responses, $rating_private, $rating_id) ){
                redirect('ratings/answered');
            }

        } else {
            $this->data['produto']      = $this->site->getProductByID($programacao->getProduto());;
            $this->data['programacao']  = $programacao;
            $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
            $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['questions']    = $this->RatingRepository_model->getQuestionsActive();

            $this->load->view($this->theme . 'ratings/rate', $this->data);
        }

    }

    function answered()
    {
        $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
        $this->load->view($this->theme . 'ratings/answered', $this->data);
    }

    function view($id)
    {
        $this->data['rating']       = $this->RatingRepository_model->getByID($id);
        $this->data['responses']    = $this->RatingRepository_model->getAnswers($id);

        $this->load->view($this->theme.'ratings/view', $this->data);
    }

    function add($uuid)
    {
        if (!$uuid) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $rating = $this->RatingRepository_model->getByuuid($uuid);

        if (!$rating) {
            $this->show_page_error('Avaliação não encontrado', 'Avaliação não encontrado');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rating_id', lang("rating_id"), 'required');

        if ($this->form_validation->run() == true && !$rating->answered) {

            $questions  = $this->RatingRepository_model->getQuestionsActive();
            $responses  = array();

            $rating_private = $this->input->post("rating_private") == 'on';

            foreach ($questions as $question) {

                $ratingResponse                 = new RatingResponse_model();
                $ratingResponse->rating_id      = $rating->id;
                $ratingResponse->question_id    = $question->id;
                $ratingResponse->rating         = $this->input->post("rating".$question->id)  != null ? $this->input->post("rating".$question->id) : 0;//nota
                $ratingResponse->response       = $this->input->post("message".$question->id) != null ? $this->input->post("message".$question->id)  : '';//nota
                $ratingResponse->question       = $question->question;
                $ratingResponse->type_question  = $question->type_question;
                $ratingResponse->created_at     = date('Y-m-d H:i:s');

                $responses[] = $ratingResponse;
            }

            $this->data['rating']       = $rating;
            $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
            $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['questions']    = $this->RatingRepository_model->getQuestionsActive();

            if ($this->RatingsService_model->save_responses($responses, $rating_private, $rating->id) ) {
                redirect('ratings/answered');
            }

        } else {

            $this->data['rating']       = $rating;
            $this->data['vendedor']     = $this->site->getCompanyByID($this->Settings->default_biller);
            $this->data['menusPai']     = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['questions']    = $this->RatingRepository_model->getQuestionsActive();

            if ($rating->answered) {
                $this->load->view($this->theme . 'ratings/answered', $this->data);
            } else {
                $this->load->view($this->theme . 'ratings/rating', $this->data);
            }
        }

    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

}?>